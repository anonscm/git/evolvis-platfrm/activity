<div id="main-content" class="columns-3 lfr-grid">

	<div class="portlet-layout">
		<!-- sitemenu -->
		<div id="column-1" class="portlet-column">  
			<div id="layout-column_column-1" class="portlet-dropzone portlet-column-content portlet-column-content-first">                
				$processor.processColumn("column-1")
			</div>
		</div>                           
		<!-- sitemenu Ende -->
		
		<!-- sitecontent -->
		<div id="column-2" class="portlet-column">   
			<div id="layout-column_column-2" class="portlet-dropzone portlet-column-content">
				$processor.processColumn("column-2")
			</div>
		</div>
	
		<div id="column-3" class="portlet-column">   
			<div id="layout-column_column-3" class="portlet-dropzone portlet-column-content portlet-column-content-last">
				$processor.processColumn("column-3")
			</div>
	    </div>
		<!-- sitecontent Ende-->
	</div>
</div>
                           
		    
