<div id="main-content" class="columns-2">
	
	<div class="portlet-layout">

		<!-- sitemenu -->
		<div id="column-1" class="portlet-column">                 
			$processor.processColumn("column-1")
		</div>                           
		<!-- sitemenu Ende -->
		
		<!-- sitecontent -->         
		<div id="column-2" class="portlet-column">   
			$processor.processColumn("column-2")
		</div>
		<!-- sitecontent Ende-->
	</div>
	 
</div>
                           
		    
