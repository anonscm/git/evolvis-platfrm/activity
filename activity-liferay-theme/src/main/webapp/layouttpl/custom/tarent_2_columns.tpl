<div class="columns-2 lfr-grid">

	<!-- sitemenu -->
	<div id="column-1" class="lfr-column">                 
		$processor.processColumn("column-1")
	</div>                           
	<!-- sitemenu Ende -->
	
	<!-- sitecontent -->         
	<div id="column-2" class="lfr-column">   
	
		<div class="gs-dyn">
			<div class="c5 lfr-column">$processor.processColumn("column-3")</div>
			<div class="c4 lfr-column">$processor.processColumn("column-4")</div>
		</div>
		
		<div class="gs960 lfr-column">
			$processor.processColumn("column-2")
		</div>
	</div>
	<!-- sitecontent Ende-->
	 
</div>
                           
		    
