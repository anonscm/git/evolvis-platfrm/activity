/*de.tarent namespace*/
YAHOO.namespace("de.tarent");

YAHOO.de.tarent.goToUrl = function(renderUrl) {
	document.location.href = renderUrl;
};

YAHOO.de.tarent.createBreadcrumb = function(htmlValue) {
	document.getElementById('titlearea').innerHTML = htmlValue;
};

YAHOO.de.tarent.setOrUnsetAllSelectBoxValues = function(selectBoxToChange, valueToSet){
    if( selectBoxToChange != null) {
        for (var i = 0; i < selectBoxToChange.length; i++)
            selectBoxToChange[i].checked = valueToSet ;
    }
};

YAHOO.de.tarent.hoursIsNotEmptyAndContainsAValidNumber = function(hoursValue){
    if(hoursValue.contains(",")){
        hoursValue = hoursValue.replace(",", ".");
    }

    if(hoursValue == "" ||
        isNaN(hoursValue)){
        return false;
    }else{
        return true;
    }

};

YAHOO.de.tarent.history = YAHOO.util.History;
YAHOO.de.tarent.portletHistoryFlag;

handleBrowserHistory = function (action, namespace) {
	YAHOO.de.tarent.portletHistoryFlag = false;
	YAHOO.util.History.navigate(namespace, action);
};

/*
 * date format selDate - javascript Date object
 */
YAHOO.de.tarent.dateFormat = function(selDate, myFormat) {
	if (selDate.getMonth() >= 0 && selDate.getMonth() <= 11) {
		return YAHOO.util.Date.format(selDate, {
			format : myFormat
		});
	} else {
		return "";
	}
};

/*
 * default formatter, escape html unused now, yui 2.9 includes this in datatable
 */
YAHOO.widget.DataTable.Formatter.defaultFormatter = function(el, oRecord,
		oColumn, oData, oDataTable) {
	var value = (YAHOO.lang.isValue(oData)) ? oData : "";
	el.innerHTML = YAHOO.lang.escapeHTML(value.toString());
};

/*
 * removes any formatting, useful to remove default yui 2.9 html escaping in
 * datatable
 */
YAHOO.widget.DataTable.Formatter.simpleFormat = function(elLiner, oRecord,
		oColumn, oData) {
	elLiner.innerHTML = oData;
};

/*
 * ymd date formatter sample table column definition {key :"date",label :"Date",
 * formatter:"ymdDate", sortable :true}
 */
YAHOO.widget.DataTable.Formatter.ymdDate = function(elLiner, oRecord, oColumn,
		oData) {
	elLiner.innerHTML = (YAHOO.lang.isValue(oData)) ? YAHOO.de.tarent
			.dateFormat(oData, "%Y-%m-%d") : "";
};
/*
 * mdy date formatter sample table column definition {key :"date",label :"Date",
 * formatter:"mdyDate", sortable :true}
 */
YAHOO.widget.DataTable.Formatter.mdyDate = function(elLiner, oRecord, oColumn,
		oData) {
	elLiner.innerHTML = (YAHOO.lang.isValue(oData)) ? YAHOO.de.tarent
			.dateFormat(oData, "%d.%m.%Y") : "";
};

/* json datasource */
YAHOO.de.tarent.jsonDataSource = function(dsUrl, dsFields) {
	var myDataSource = new YAHOO.util.DataSource(dsUrl);
    myDataSource.connMgr = YAHOO.util.Connect;
    myDataSource.connMgr.initHeader('Content-Type', 'application/json; charset=utf-8', true);
	myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
	myDataSource.responseSchema = {
		resultsList : "records",
		fields : dsFields,
		metaFields : {
			totalRecords : "totalRecords",
			columnSum : "columnSum"
		}
	};
	return myDataSource;
};

/*
 *von Uli:
/***********************************/
///***WIP****/
YAHOO.de.tarent.filteredDataTableWithPagingConfig = function(dataTableName, dsUrl, dsFields,
		myColumnDefs, filterObject, columnSumStatus, defaultSort, defaultDir,
		sessionLimit, sessionOffset, sessionSortBy, sessionSortDir, showAll) {
	
	if (typeof showAll === "undefined" || showAll === null){
		showAll = false;
	}
	
	/* DataSource instance */
	var myDataSource = YAHOO.de.tarent.jsonDataSource(dsUrl, dsFields);

	var myDataTableSum = "";

	/* URL builder */
	baseRequestBuilder = function(oState, oSelf) {
		var newSort, newDir, newOffset, newLimit;
		oState = oState || {
			pagination : null,
			sortedBy : null
		};

		defaultSort = (defaultSort) ? defaultSort
				: oSelf.getColumnSet().keys[0].getKey();

		if (oState.sortedBy == null) {
			defaultDir = (defaultDir) ? defaultDir : "asc";
		} else {
			defaultDir = "asc";
		}
		
		newSort = (oState.sortedBy) ? oState.sortedBy.key : (sessionSortBy ? sessionSortBy : defaultSort);
		newDir = (oState.sortedBy && oState.sortedBy.dir) ? (oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC ? "desc" : "asc") 
				: (sessionSortDir ? sessionSortDir : defaultDir);

		newOffset = 0;
		newLimit = null;
		
		//in case of returning back to overview
		//set paging configuration from session
		if(oState.pagination){
			newLimit = oState.pagination.rowsPerPage;
			//be careful: oState.sortedBy initial immer null?!
			if(sessionOffset >= 0){
				oState.pagination.recordOffset = sessionOffset;
				newOffset = sessionOffset;
				sessionOffset = -1;
			}else{
				newOffset = oState.pagination.recordOffset;
			}
		}
		
		ret = "&sort=" + newSort + "&dir=" + newDir + "&limit=" + newLimit
				+ "&offset=" + newOffset;
		for (f in oSelf.myFilter) {
			ret += "&" + f + "=" + encodeURIComponent(oSelf.myFilter[f]);
		}
		return ret;
	};

	/* DataTable configuration */
	myConfigs = {
		selectionMode : "single",
		/*
		 * scrollable:true, width:tableWidth, height: "200px", This
		 * configuration item is what builds the query string passed to the
		 * DataSource.
		 */
		generateRequest : baseRequestBuilder,
		initialLoad : false,
		dynamicData : true, /* Enables dynamic server-driven data */
		paginator : new YAHOO.widget.Paginator(
				{
					rowsPerPage : sessionLimit,
					template : "{FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown} {CurrentPageReport}",
					rowsPerPageOptions : [ 10, 20, 50, 100], /*
																 * configure the
																 * RowsPerPageDropdown
																 * UI Component
																 */
					pageReportValueGenerator : function(paginator) {
						var totalPageDisplay = "";
						var sumToDisplay = "";
						if (paginator.getPageRecords() != null) {
							var recs = paginator.getPageRecords();
							totalPageDisplay = (recs[0] + 1) + "-"
									+ (recs[1] + 1) + "/"
									+ paginator.getTotalRecords();
						}

						if (columnSumStatus != undefined
								&& columnSumStatus.status) {
							sumToDisplay = "<span class='spanTotalTable'>"
									+ columnSumStatus.displayMsg + ": "
									+ myDataTableSum.toFixed(2) + "</span>";
						}
						return {
							totalPageDisplay : totalPageDisplay,
							sumToDisplay : sumToDisplay
						};
					},
					pageReportTemplate : '{totalPageDisplay} &nbsp {sumToDisplay}'
				/*
				 * pageReportTemplate : 'Showing {start} To {end} out of
				 * {totalRecords}'
				 */
				})
	};

	/* DataTable instance */

	var myDataTable = new YAHOO.widget.DataTable(dataTableName, myColumnDefs,
			myDataSource, myConfigs);

	myDataTable.myFilter = filterObject;

	/* Subscribe to events for row selection */
	myDataTable.on("rowMouseoverEvent", myDataTable.onEventHighlightRow);
	myDataTable.on("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
	myDataTable.on("rowClickEvent", myDataTable.onEventSelectRow);

	/* Update totalRecords on the fly with value from server */
	myDataTable.handleDataReturnPayload = function(oRequest, oResponse,
			oPayload) {
		oPayload.totalRecords = oResponse.meta.totalRecords;
		myDataTableSum = (oResponse.meta.columnSum == null) ? ""
				: oResponse.meta.columnSum;
		if(showAll){
			var totalRows = oResponse.meta.totalRecords;
			var rowsPerPageOptions = [10, 20, 50, 100, {value: totalRows, text: 'Alle'}];
			myConfigs.paginator.set('rowsPerPageOptions', rowsPerPageOptions);
		}
		return oPayload;
	};
	
	myDataTable.reloadData = function() {
		var oState = this.getState(), request, oCallback;

		/* return to first page on filter change */
		oState.pagination.recordOffset = 0;

		/*
		 * This example uses onDataReturnSetRows because that method will clear
		 * out the old data in the DataTable, making way for the new data.
		 */
		oCallback = {
			success : this.onDataReturnSetRows,
			failure : this.onDataReturnSetRows,
			argument : oState,
			scope : this
		};

		/* Generate a query string */
		request = this.get("generateRequest")(oState, this);
		/* Fire off a request for new data. */
		this.getDataSource().sendRequest(request, oCallback);

	};

	myDataTable.reloadData();
	return myDataTable;
};


/*
 * Ende von uli
 * */





/*
 * defines a filterable and sortable dataTable sample usage: /${ns}myDataTable =
 * YAHOO.de.tarent.filteredDataTable("${ns}form-activities","${table}&",
 * ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter); dataTableName -
 * id of the div where the dataTable will be rendered dsUrl - datasource URL
 * dsFields - datasource model definition myColumnDefs - dataTable columns
 * definition filterObject - custom query filter parameters object
 */
YAHOO.de.tarent.filteredDataTable = function(dataTableName, dsUrl, dsFields,
		myColumnDefs, filterObject, columnSumStatus, defaultSort, defaultDir, rowsOnOnePage) {

    rowsOnOnePage = (rowsOnOnePage) ? rowsOnOnePage : 10;
	/* DataSource instance */
	var myDataSource = YAHOO.de.tarent.jsonDataSource(dsUrl, dsFields);

	var myDataTableSum = "";

	/* URL builder */
	baseRequestBuilder = function(oState, oSelf) {
		var sort, dir, startIndex, results;
		oState = oState || {
			pagination : null,
			sortedBy : null
		};

		defaultSort = (defaultSort) ? defaultSort
				: oSelf.getColumnSet().keys[0].getKey();

		if (oState.sortedBy == null) {
			defaultDir = (defaultDir) ? defaultDir : "asc";
		} else {
			defaultDir = "asc";
		}

		sort = (oState.sortedBy) ? oState.sortedBy.key : defaultSort;
		dir = (oState.sortedBy && oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC) ? "desc"
				: defaultDir;

		startIndex = (oState.pagination) ? oState.pagination.recordOffset : 0;
		results = (oState.pagination) ? oState.pagination.rowsPerPage : null;

		ret = "&sort=" + sort + "&dir=" + dir + "&results=" + results
				+ "&startIndex=" + startIndex;
		for (f in oSelf.myFilter) {
			ret += "&" + f + "=" + encodeURIComponent(oSelf.myFilter[f]);
		}
		return ret;
	};

	/* DataTable configuration */
	myConfigs = {
		selectionMode : "single",
		/*
		 * scrollable:true, width:tableWidth, height: "200px", This
		 * configuration item is what builds the query string passed to the
		 * DataSource.
		 */
		generateRequest : baseRequestBuilder,
		initialLoad : false,
		dynamicData : true, /* Enables dynamic server-driven data */
		paginator : new YAHOO.widget.Paginator(
				{
					rowsPerPage : rowsOnOnePage,
					template : "{FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown} {CurrentPageReport}",
					rowsPerPageOptions : [ 10, 20, 50, 100 ], /*
																 * configure the
																 * RowsPerPageDropdown
																 * UI Component
																 */
					pageReportValueGenerator : function(paginator) {
						var totalPageDisplay = "";
						var sumToDisplay = "";
						if (paginator.getPageRecords() != null) {
							var recs = paginator.getPageRecords();
							totalPageDisplay = (recs[0] + 1) + "-"
									+ (recs[1] + 1) + "/"
									+ paginator.getTotalRecords();
						}

						if (columnSumStatus != undefined
								&& columnSumStatus.status) {
							sumToDisplay = "<span class='spanTotalTable'>"
									+ columnSumStatus.displayMsg + ": "
									+ myDataTableSum.toFixed(2) + "</span>";
						}
						return {
							totalPageDisplay : totalPageDisplay,
							sumToDisplay : sumToDisplay
						};
					},
					pageReportTemplate : '{totalPageDisplay} &nbsp {sumToDisplay}'
				/*
				 * pageReportTemplate : 'Showing {start} To {end} out of
				 * {totalRecords}'
				 */
				})
	};

	/* DataTable instance */
	var myDataTable = new YAHOO.widget.DataTable(dataTableName, myColumnDefs,
			myDataSource, myConfigs);

	myDataTable.myFilter = filterObject;

	/* Subscribe to events for row selection */
	myDataTable.on("rowMouseoverEvent", myDataTable.onEventHighlightRow);
	myDataTable.on("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
	myDataTable.on("rowClickEvent", myDataTable.onEventSelectRow);

	/* Update totalRecords on the fly with value from server */
	myDataTable.handleDataReturnPayload = function(oRequest, oResponse,
			oPayload) {
		oPayload.totalRecords = oResponse.meta.totalRecords;
		myDataTableSum = (oResponse.meta.columnSum == null) ? ""
				: oResponse.meta.columnSum;
		return oPayload;
	};
	
	myDataTable.reloadData = function() {
		var oState = this.getState(), request, oCallback;

		/* return to first page on filter change */
		oState.pagination.recordOffset = 0;

		/*
		 * This example uses onDataReturnSetRows because that method will clear
		 * out the old data in the DataTable, making way for the new data.
		 */
		oCallback = {
			success : this.onDataReturnSetRows,
			failure : this.onDataReturnSetRows,
			argument : oState,
			scope : this
		};

		/* Generate a query string */
		request = this.get("generateRequest")(oState, this);
		/* Fire off a request for new data. */
		this.getDataSource().sendRequest(request, oCallback);

	};

	myDataTable.reloadData();
	return myDataTable;
};

/* validate if start date is before end date */
YAHOO.de.tarent.validateStartBeforEnd = function(formId, startDateId, endDateId) {
	var form = document.getElementById(formId);
	var startDate = form.elements[startDateId];
	var endDate = form.elements[endDateId];

	/* create date object */
	var objStartDate = new Date(startDate.value);
	var objEndDate = new Date(endDate.value);

	var valid = true;
	if (objStartDate > objEndDate) {
		/* display error message */
		YAHOO.util.Dom.removeClass(formId + "-" + endDateId
				+ "-validateStartBeforEnd", "yui-pe-content");
		valid = false;
	} else {
		/* hide error message */
		YAHOO.util.Dom.addClass(formId + "-" + endDateId
				+ "-validateStartBeforEnd", "yui-pe-content");
	}

	return valid;
};

/* define generic validator */
YAHOO.de.tarent.validate = function(formId, cfg) {

	var form = document.getElementById(formId);
	var valid = true;

	for ( var item in cfg) {
		if (YAHOO.de.tarent.validators[cfg[item].validator](form, cfg[item])) {
			YAHOO.util.Dom.addClass(formId + "-" + cfg[item].id + "-"
					+ cfg[item].validator, "yui-pe-content");
		} else {
			YAHOO.util.Dom.removeClass(formId + "-" + cfg[item].id + "-"
					+ cfg[item].validator, "yui-pe-content");
			valid = false;
		}
	}
	return valid;
};

YAHOO.de.tarent.resetValidate = function(formId, cfg) {
	var form = document.getElementById(formId);

	for ( var item in cfg) {
		YAHOO.util.Dom.addClass(formId + "-" + cfg[item].id + "-"
				+ cfg[item].validator, "yui-pe-content");
	}
};

/* de.tarent.validators namespace */
YAHOO.namespace("de.tarent.validators");

/* required validation */
YAHOO.de.tarent.validators.required = function(form, cfg) {
	var value = form.elements[cfg.id];
	return (value.value == "") ? false : true;
};

/* number validation */
YAHOO.de.tarent.validators.number = function(form, cfg) {
	var value = form.elements[cfg.id];
	return (isNaN(value.value)) ? false : true;
};

/* positive number validation */
YAHOO.de.tarent.validators.positiveNumber = function(form, cfg) {
	var value = form.elements[cfg.id];

	return (isNaN(value.value) && value.value.match(/[^\d]/) || (value.value < 0)) ? false
			: true;
};

/* positive integer validation */
YAHOO.de.tarent.validators.positiveInteger = function(form, cfg) {
	var value = form.elements[cfg.id];
	if (value.value == null || value.value == "") {
		return true;
	}
	if (!isNaN(value.value) && value.value == parseInt(value.value)
			&& value.value >= 0) {
		return true;
	} else {
		return false;
	}
};

/*
 * evolvis id validation can be empty or a number
 */
YAHOO.de.tarent.validators.evolvisId = function(form, cfg) {
	var value = form.elements[cfg.id];
	if (value == '') {
		return true;
	}

	var result = ((isNaN(value.value) && value.value.match(/[^\d]/))) ? false
			: true;
	return result;
};

/*
 * activity hours validation can be a number between 0 and 25 can have the next
 * format : *.0, *.00, *.25, *.5, *.50, *.75 if the value don't have the
 * specific format the number is rounded
 */
YAHOO.de.tarent.validators.activityHours = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /^\d*((\.25)|(\.50)|(\.5)|(\.75)|(\.0)|(\.00))?$/;
	/* verify if the value is a number and have value form 0 to 24 */
	var isValid = (!isNaN(value.value) && value.value > 0 && value.value <= 24) ? true
			: false;
	if (isValid) {
		/* verify the number formate */
		if (!regexp.test(value.value)) {
			/* rounding hours */
			form.elements[cfg.id].value = YAHOO.de.tarent
					.roundingHours(value.value);
			return true;
		} else {
			return true;
		}
	} else {
		return false;
	}
};

YAHOO.de.tarent.validators.roundingNumber = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /^\d*((\.25)|(\.50)|(\.5)|(\.75)|(\.0)|(\.00))?$/;
	var isValid = (!isNaN(value.value) && value.value > 0) ? true : false;
	if (isValid) {
		/* verify the number formate */
		if (!regexp.test(value.value)) {
			/* rounding hours */
			form.elements[cfg.id].value = YAHOO.de.tarent
					.roundingHours(value.value);
			return true;
		} else {
			return true;
		}
	} else {
		return true;
	}
};

/* rounding hours */
YAHOO.de.tarent.roundingHours = function(hours) {
	var no = hours.split('.')[0];
	var fract = "0." + hours.split('.')[1];
	var newnumber;
	if (fract > 0 && fract < 0.25) {
		newnumber = no + ".25";
	} else if (fract > 0.25 && fract < 0.5) {
		newnumber = no + ".5";
	} else if (fract > 0.5 && fract < 0.75) {
		newnumber = no + ".75";
	} else if (fract > 0.75 && fract < 1) {
		if (parseInt(no) < 23) {
			newnumber = parseInt(no) + 1;
		} else {
			newnumber = parseInt(no) + 1;
		}
	}
	return newnumber;
};

/*
 * activity hours validation can be a number between 0 and 25 can have the next
 * format : *.0, *.00, *.25, *.5, *.50, *.75 if the value don't have the
 * specific format the number is rounded
 */
YAHOO.de.tarent.validators.overtimeHours = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /^\d*((\.25)|(\.50)|(\.5)|(\.75)|(\.0)|(\.00))?$/;
	/* verify if the value is a number and have value form 0 to 24 */
	var isValid = (!isNaN(value.value) && value.value > 0 && value.value <= 24) ? true
			: false;
	if (isValid) {
		/* verify the number formate */
		if (!regexp.test(value.value)) {
			/* rounding hours */
			form.elements[cfg.id].value = YAHOO.de.tarent
					.roundingHours(value.value);
			return true;
		} else {
			return true;
		}
	} else {
		return false;
	}
};

/* description validation - the length can't be greater than 4000 */
YAHOO.de.tarent.validators.description = function(form, cfg) {
	var value = form.elements[cfg.id];

	return (value.value.length > 4000) ? false : true;
};

/* link validation */
YAHOO.de.tarent.validators.link = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	if (value.value != "") {
		return (regexp.test(value.value)) ? true : false;
	} else {
		return true;
	}
};

/* link and webdav validation */
YAHOO.de.tarent.validators.linkWebdav = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /(ftp|http|https|webdav|webdav):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	if (value.value != "") {
		return (regexp.test(value.value)) ? true : false;
	} else {
		return true;
	}
};

/* email validation */
YAHOO.de.tarent.validators.email = function(form, cfg) {
	var value = form.elements[cfg.id];
	var regexp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (value.value != "") {
		return (regexp.test(value.value)) ? true : false;
	} else {
		return true;
	}
};

/* date validation */
YAHOO.de.tarent.validators.date = function(form, cfg) {
	var value = form.elements[cfg.id];
	if (value.value != "") {
		var parsedDate = value.value.split("-");
		if (parsedDate.length != 3)
			return false;
		var day, month, year;
		year = parsedDate[0];
		month = parsedDate[1];
		day = parsedDate[2];
		var objDate = new Date(value.value);
		if (month != objDate.getMonth() + 1)
			return false;
		if (day != objDate.getDate())
			return false;
		if (year != objDate.getFullYear())
			return false;
	}
	return true;
};

/* data in the past validation */
YAHOO.de.tarent.validators.dateInThePast = function(form, cfg) {
	var value = form.elements[cfg.id];
	var objDate = new Date(value.value);
	var today = new Date();
	return (objDate > today) ? false : true;

};

YAHOO.de.tarent.validators.dateInOrder = function(form, cfg) {
	var start = form.elements[cfg.id];
	var end = form.elements[cfg.compareDate];
	var startDate = new Date(start.value);
	var endDate = new Date(end.value);
	return (startDate >= endDate) ? false : true;

};

/* max lenght validation */
YAHOO.de.tarent.validators.maxLength = function(form, cfg) {
	var elem = form.elements[cfg.id];
	var maxSize = cfg.maxLength;
	if (elem == null || elem.value == null) {
		return true;
	}
	return (elem.value.length > maxSize) ? false : true;
};

/* jquery modifications */
jQuery.datepicker._gotoToday = function(id) {
    var target = jQuery(id);
    var inst = this._getInst(target[0]);
    if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
            inst.selectedDay = inst.currentDay;
            inst.drawMonth = inst.selectedMonth = inst.currentMonth;
            inst.drawYear = inst.selectedYear = inst.currentYear;
    }
    else {
            var date = new Date();
            inst.selectedDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = date.getFullYear();
            // the below two lines are new
            this._setDateDatepicker(target, date);
            this._selectDate(id, this._getDateDatepicker(target));
    }
    this._notifyChange(inst);
    this._adjustDate(target);
};
