Feature: Fehlgeschlagene Anmelden


Scenario: Failed Login for mitarbeiter role

Given I go to activity login page
When I try to login into activity as a user with "mitarbeiter" role with wrong credentials
Then I remain on the activity login screen with error message

Scenario: Failed Login for projectleiter role

Given I go to activity login page
When I try to login into activity as a user with "projectleiter" role with wrong credentials
Then I remain on the activity login screen with error message


Scenario: Failed Login for controller role

Given I go to activity login page
When I try to login into activity as a user with "controller" role with wrong credentials
Then I remain on the activity login screen with error message