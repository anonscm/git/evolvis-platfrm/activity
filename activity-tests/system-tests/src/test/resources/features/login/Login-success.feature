Feature: Erfolgreicher Anmeldung


Scenario: Successfull Login for mitarbeiter role

Given I go to activity login page
When I try to login into activity as a user with "mitarbeiter" role with correct credentials
Then I see the logged in view for "mitarbeiter" role

Scenario: Successfull Login for projectleiter role

Given I go to activity login page
When I try to login into activity as a user with "projectleiter" role with correct credentials
Then I see the logged in view for "projectleiter" role

Scenario: Successfull Login for controller role

Given I go to activity login page
When I try to login into activity as a user with "controller" role with correct credentials
Then I see the logged in view for "controller" role

