Feature: Meine Leistungen Sehen

Scenario: Mitarbeiter should see his list of activities

Given I go to activity login page
When  I try to login into activity as a user with "mitarbeiter" role with correct credentials
Then I see a list of my activites

Scenario: ProjectLeiter should see his list of activities

Given I go to activity login page
When I try to login into activity as a user with "projectleiter" role with correct credentials
Then I see a list of my activites

Scenario: Controller should see his list of activities

Given I go to activity login page
When  I try to login into activity as a user with "controller" role with correct credentials
Then I see a list of my activites
