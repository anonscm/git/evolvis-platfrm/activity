#!/bin/bash
#

USER=$1
HOST_NAME=$2
DB_USER=$3
DB_PASS=$4
DATABASE=$5
SQL_FILE=$6

echo -e " SSH user: '$1'\n Target host:'$2'\n DB user:'$3'\n DB password:'$4'\n Database name:'$5'\n SQL file path:'$6'"

if [ $# -ne 6 ]
  then
    echo "Not all required arguments supplied"
    echo "usage './remote_mysql_execution.sh <ssh username> <target host> <db user> <db password> <database name> <sql file path>'"
    exit 1
fi

SQL_STATEMENTS=`cat $SQL_FILE`
export PGPASSWORD=activity
ssh -l $USER $HOST_NAME "export PGPASSWORD=activity && psql -U activity -d activityportal << 'EOF'
cat $SQL_STATEMENTS;
EOF"