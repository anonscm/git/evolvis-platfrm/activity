/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.env;

import java.util.concurrent.TimeUnit;

public class GlobalConfig {
	private static GlobalConfig instance;
	
	private String serverHost;
	private int port;
	private String baseURL;
	private String browserMode;
	private String htmlPath;
	private boolean screenshotEmbedded = true;
	private String screenshotPath;
	private String resultContentPath;
	private String resultContentZipPath;
	private long sleepTime;
	private TimeUnit sleepUnit;
	private boolean newSessionPerFeature = false;
    private String dbName;
    private String dbHost;
    private String dbUsername;
    private String dbPassword;

    public GlobalConfig(){
		instance = this;
	}
	
	public static GlobalConfig getInstance(){
		return instance;
	}
	
	public String getServerHost() {
		return serverHost;
	}
	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getBaseURL() {
		return baseURL;
	}
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
	public String getBrowserMode() {
		return browserMode;
	}
	public void setBrowserMode(String browserMode) {
		this.browserMode = browserMode;
	}
	public String getResultContentPath() {
		return resultContentPath;
	}
	public void setResultContentPath(String resultContentPath) {
		this.resultContentPath = resultContentPath;
	}
	public String getResultContentZipPath() {
		return resultContentZipPath;
	}
	public void setResultContentZipPath(String resultContentZipPath) {
		this.resultContentZipPath = resultContentZipPath;
	}
	public String getHtmlPath() {
		return htmlPath;
	}
	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}
	public String getScreenshotPath() {
		return screenshotPath;
	}
	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}
	public long getSleepTime() {
		return sleepTime;
	}
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
	public TimeUnit getSleepUnit() {
		return sleepUnit;
	}
	public void setSleepUnit(TimeUnit sleepUnit) {
		this.sleepUnit = sleepUnit;
	}
	public boolean isScreenshotEmbedded() {
		return screenshotEmbedded;
	}
	public void setScreenshotEmbedded(boolean screenshotEmbedded) {
		this.screenshotEmbedded = screenshotEmbedded;
	}
	public boolean isNewSessionPerFeature() {
		return newSessionPerFeature;
	}
	public void setNewSessionPerFeature(boolean newSessionPerFeature) {
		this.newSessionPerFeature = newSessionPerFeature;
	}

    public String getDbHost() {
        return dbHost;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
