/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.selenium;

import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * The standart RemoteWebDriver doesn't support taking screenshots.
 * (because they is not an instance of {@link TakesScreenshot})
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class RemoteWebDriverWithScreenshotFunctionality extends RemoteWebDriver
		implements TakesScreenshot {
	
	public RemoteWebDriverWithScreenshotFunctionality() {
		super();
	}

	public RemoteWebDriverWithScreenshotFunctionality(
			Capabilities desiredCapabilities) {
		super(desiredCapabilities);
	}

	public RemoteWebDriverWithScreenshotFunctionality(CommandExecutor executor,
			Capabilities desiredCapabilities) {
		super(executor, desiredCapabilities);
	}

	public RemoteWebDriverWithScreenshotFunctionality(URL remoteAddress,
			Capabilities desiredCapabilities) {
		super(remoteAddress, desiredCapabilities);
	}

	public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
		if ((Boolean) getCapabilities().getCapability(CapabilityType.TAKES_SCREENSHOT)) {
			String base64Str = execute(DriverCommand.SCREENSHOT).getValue()
					.toString();
			return target.convertFromBase64Png(base64Str);
		}
		return null;
	}

}
