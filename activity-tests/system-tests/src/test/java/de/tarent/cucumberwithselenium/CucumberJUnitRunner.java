/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.runners.model.InitializationError;

import cucumber.api.junit.Cucumber;

/**
 * A seperate Cucumber-Runner, so that we can get
 * the used Option-Annotation.
 * 
 * @author Sven Schumann, <s.schumann@tarent.de>
 *
 */
public class CucumberJUnitRunner extends Cucumber {
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.TYPE})
	public static @interface ExtendedOptions{
		/**
		 * @return false if the hooks should not be executed.
		 */
		boolean executeHooks() default true;
	}
	
	@Cucumber.Options
	@ExtendedOptions
	class AnnotationHost{}
	
	private static Cucumber.Options usedOptions;
	private static ExtendedOptions usedExtOptions;
	
	public CucumberJUnitRunner(Class<?> clazz) throws InitializationError, IOException {
		super(clazz);
		
		usedOptions = (Cucumber.Options)clazz.getAnnotation(Cucumber.Options.class);
		if(usedOptions == null){
			usedOptions = (Cucumber.Options)AnnotationHost.class.getAnnotation(Cucumber.Options.class);
		}
		
		usedExtOptions = (ExtendedOptions)clazz.getAnnotation(ExtendedOptions.class);
		if(usedExtOptions == null){
			usedExtOptions = (ExtendedOptions)AnnotationHost.class.getAnnotation(ExtendedOptions.class);
		}
	}
	
	/**
	 * If cucumber will be executed via console, this
	 * class wont be instantiated. This behavior can
	 * be used to check if we are in a JUnit test.
	 * 
	 * @return
	 */
	public static boolean isJunitTest(){
		if(usedOptions == null) return false;
		else return true;
	}
	
	public static boolean isExecuteHooks(){
		return usedExtOptions.executeHooks();
	}
	
	public static boolean isDryRun(){
		return usedOptions.dryRun();
	}
	
	public static boolean isStrict(){
		return usedOptions.strict();
	}
	
	public static String[] getFeatures(){
		return usedOptions.features();
	}
	
	public static String[] getGlue(){
		return usedOptions.glue();
	}

	public static String[] getTags(){
		return usedOptions.tags();
	}
	
	public static String[] getFormat(){
		return usedOptions.format();
	}
	
	public static boolean isMonochrome(){
		return usedOptions.monochrome();
	}
	
	public static String[] getOptionName(){
		return usedOptions.name();
	}
	
}