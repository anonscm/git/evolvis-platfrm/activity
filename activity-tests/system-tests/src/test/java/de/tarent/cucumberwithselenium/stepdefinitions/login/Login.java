/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.stepdefinitions.login;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.tarent.cucumberwithselenium.stepdefinitions.AbstractStepDefinition;
import de.tarent.cucumberwithselenium.util.Browser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Login extends AbstractStepDefinition {

    public Browser browser;

    @Before
    public void before(){
        browser = new Browser(webDriver);
    }

    @After
    public void after(){
    }

    @Given("^I go to activity login page$")
    public void I_go_to_activity_login_page() throws Throwable {
        browser.gotoPage("http://activity-ci.lan.tarent.de:8080");
    }

    @When("^I try to login into activity as a user with \"([^\"]*)\" role with correct credentials$")
    public void I_try_to_login_into_activity_as_a_user_with_role_with_correct_credentials(String userRole) throws Throwable {

        if(userRole.equals("mitarbeiter")){
            enterUsernameAndPassword("mittest", "test");
        }else if(userRole.equals("projectleiter")){
            enterUsernameAndPassword("protest", "test");
        }else if(userRole.equals("controller")){
            enterUsernameAndPassword("contest", "test");
        }else{
            assertFalse(true);
        }
        browser.clickButton("login_btn");

    }

    private void enterUsernameAndPassword(String username, String password) {
        browser.setTextField("field-username", username);
        browser.setTextField("field-password", password);
    }

    @When("^I try to login into activity as a user with \"([^\"]*)\" role with wrong credentials$")
    public void I_try_to_login_into_activity_as_a_user_with_role_with_wrong_credentials(String userRole) throws Throwable {
        if(userRole.equals("mitarbeiter")){
            enterUsernameAndPassword("mittest", "wrong");
        }else if(userRole.equals("projectleiter")){
            enterUsernameAndPassword("protest", "wrong");
        }else if(userRole.equals("controller")){
            enterUsernameAndPassword("contest", "wrong");
        }else{
            assertFalse(true);
        }
        browser.clickButton("login_btn");
    }

    @Then("^I see the logged in view for \"([^\"]*)\" role$")
    public void I_see_the_logged_in_view_for(String userRole) throws Throwable {
        if(userRole.equals("mitarbeiter")){
            assertTrue( mitarbeiterSpecificLoggedInView("mittest") );
        }else if(userRole.equals("projectleiter")){
            assertTrue(mitarbeiterSpecificLoggedInView("protest") && projectLeiterSpecificLoggedInView());
        }else if(userRole.equals("controller")){
            assertTrue(mitarbeiterSpecificLoggedInView("contest") && projectLeiterSpecificLoggedInView() && controllerSpecificLoggedInView());
        }else{
            assertFalse(true);
        }
    }


    private boolean mitarbeiterSpecificLoggedInView(String user) {
        return  browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li/a[contains(text(), 'Meine Leistungen')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li/a[contains(text(), 'Meine Überstunden')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li/a[contains(text(), 'Meine Abwesenheit')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li/a[contains(text(), 'Meine Löschanfragen')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[2]/ul/li/a[contains(text(), 'Meine aktiven Projekte')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_loginportlet_WAR_activityportlets_loginForm']/h3").getText().equals("Eingeloggt als: " + user);

    }

    private boolean projectLeiterSpecificLoggedInView() {
        return browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li/a[contains(text(), 'Meine Leistungen')]").isDisplayed();
    }

    private boolean controllerSpecificLoggedInView() {
        return  browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Kunden')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Projekte')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Aufträge')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Positionen')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Benutzerverwaltung')]").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='_activitymenuportlet_WAR_activityportlets_mainDiv']/div[3]/ul/li/a[contains(text(), 'Mitarbeiterzuordnung')]").isDisplayed();
    }


    @Then("^I remain on the activity login screen with error message$")
    public void I_remain_on_the_activity_login_screen_with_error_message() throws Throwable {
        assertTrue(loginPageWithErrorMessage());
    }

    private boolean loginPageWithErrorMessage() {
        return browser.findElementByXpathString(".//*[@id='_loginportlet_WAR_activityportlets_mainDiv']/h2").getText().equals("Login") &&
                browser.findElementByXpathString(".//*[@id='_loginportlet_WAR_activityportlets_loginForm']/p").getText().equals("Sie sind nicht eingeloggt") &&
                browser.findElementByXpathString(".//*[@id='login_btn']").isDisplayed() &&
                browser.findElementByXpathString(".//*[@id='errorMsg']").getText().equals("Falscher Benutzername oder Passwort.");
    }


}
