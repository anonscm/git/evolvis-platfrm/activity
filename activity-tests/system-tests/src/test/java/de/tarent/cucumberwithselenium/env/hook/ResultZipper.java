/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.env.hook;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import de.tarent.cucumberwithselenium.env.GlobalConfig;
import de.tarent.cucumberwithselenium.env.HookRegistry;
import de.tarent.cucumberwithselenium.env.Utils;
import de.tarent.cucumberwithselenium.env.event.HandlerStop;

/**
 * This class represents a hook.
 * They zip all test results to a separate zip-file.
 */
public class ResultZipper implements HandlerStop {
	@Autowired GlobalConfig config;
	
	public ResultZipper(){
		//register me
		HookRegistry.addOnStopHandler(this);
	}
	
	public void handleStop() {
		if(	config.getResultContentPath() != null &&
			!config.getResultContentPath().equals("") &&
			config.getResultContentZipPath() != null &&
			!config.getResultContentZipPath().equals("")){
			
			System.err.println("=============== Zip result-files ===============");
			try {
				Utils.zipDirectory(config.getResultContentPath(), config.getResultContentZipPath());
			} catch (IOException e) {
				System.err.println("Error on creating zip-file");
				e.printStackTrace();
			}
		}
	}

}
