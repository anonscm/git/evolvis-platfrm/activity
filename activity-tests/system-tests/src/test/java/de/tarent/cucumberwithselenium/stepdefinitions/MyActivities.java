/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.stepdefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.tarent.cucumberwithselenium.util.Browser;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MyActivities extends AbstractStepDefinition {

    public Browser browser;

    @Before
    public void before(){
        browser = new Browser(webDriver);
    }

    @After
    public void after(){
        logout();
    }

    private void logout() {
        if( browser.findElementsByXpathString(".//*[@id='logout_btn']").size() != 0){
            browser.clickButton("logout_btn");
        }
    }



    @Then("^I see a list of my activites$")
    public void I_see_a_list_of_my_activites() throws Throwable {
        int rowsCount = browser.countRowsInTable(".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_activities']/table/tbody[2]/tr");
        assertTrue(rowsCount > 0 );
    }

}
