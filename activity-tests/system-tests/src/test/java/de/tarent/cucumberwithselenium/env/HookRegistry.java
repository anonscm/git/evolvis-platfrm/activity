/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.env;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import de.tarent.cucumberwithselenium.CucumberJUnitRunner;
import de.tarent.cucumberwithselenium.env.event.HandlerAfter;
import de.tarent.cucumberwithselenium.env.event.HandlerBefore;
import de.tarent.cucumberwithselenium.env.event.HandlerStart;
import de.tarent.cucumberwithselenium.env.event.HandlerStop;
import de.tarent.cucumberwithselenium.stepdefinitions.AbstractStepDefinition;

/**
 * This class contains all important (cucumber-)hooks.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class HookRegistry {
	
	private static List<HandlerAfter> afterHandler = new ArrayList<HandlerAfter>();
	private static List<HandlerBefore> beforeHandler = new ArrayList<HandlerBefore>();
	private static List<HandlerStart> startHandler = new ArrayList<HandlerStart>();
	private static List<HandlerStop> stopHandler = new ArrayList<HandlerStop>();
	
	private static boolean initialised = false;
	
	protected static boolean executeHooks;
	
	/**
	 * This constructor will be invoked by spring.
	 */
	public HookRegistry(String sExecuteHooks){
		if(CucumberJUnitRunner.isJunitTest()){
			executeHooks = CucumberJUnitRunner.isExecuteHooks();
		}else{
			executeHooks = Boolean.valueOf(sExecuteHooks);
		}
	}
	
	/**
	 * This hook will be called before cucumber starts
	 * to process features.
	 */
	@PostConstruct
	public void postConstruct() throws Exception{
		if(!initialised) initialised = true;
		else return;
		
		//initialise shutdown-hook
		if(executeHooks){
			Runtime.getRuntime().addShutdownHook(new Thread() {
			    public void run() {
		    		preDestroy();
			    }
			});
		}
		
		//forward event
		if(executeHooks){
			for(HandlerStart handler : startHandler){
				handler.handleStart();
			}
		}
	}
	
	public static class CucumberHookHost extends AbstractStepDefinition {
		/**
		 * This hook will be called before each scenario.
		 */
		@Before
		@org.junit.Before
		public void beforeScenario(){
			//forward event
			if(executeHooks){
				for(HandlerBefore handler : beforeHandler){
					handler.handleBeforeScenario();
				}
			}
		}
		
		/**
		 * This hook will be called after each scenario.
		 */
		@After
		@org.junit.After
		public void afterScenario(Scenario result){
			//forward event
			if(executeHooks){
				for(HandlerAfter handler : afterHandler){
					handler.handleAfterScenario(result);
				}
			}
		}
	}
	
	/**
	 * This hook will be called before the program will exit.
	 */
	private static void preDestroy(){
		//forward event
		for(HandlerStop handler : stopHandler){
			handler.handleStop();
		}
	}
	
	//event handler functions
	public static void addOnStartHandler(HandlerStart handler){
		startHandler.add(handler);
	}
	
	public static void addOnStopHandler(HandlerStop handler){
		stopHandler.add(handler);
	}
	
	public static void addOnAfterHandler(HandlerAfter handler){
		afterHandler.add(handler);
	}
	
	public static void addOnBeforeHandler(HandlerBefore handler){
		beforeHandler.add(handler);
	}
}
