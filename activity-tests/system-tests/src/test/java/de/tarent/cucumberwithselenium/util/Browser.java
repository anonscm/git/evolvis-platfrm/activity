/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.util;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Browser {
    private WebDriver webDriver;

    public Browser(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void clickButton(String buttonId) {
        webDriver.findElement(By.id(buttonId)).click();
    }

    public void gotoPage(String page) {
        webDriver.get(page);
    }

    public WebElement findElementByXpathString(String xpathString) {
        return webDriver.findElement(By.xpath(xpathString));
    }

    public List<WebElement> findElementsByXpathString(String tableId) {
        return webDriver.findElements(By.xpath(tableId));
    }

    public void setTextField(String textFieldId, String textFieldValue) {
        WebElement element = this.findElementByXpathString("//input[@id='" + textFieldId + "']");
        element.clear();
        element.sendKeys(textFieldValue);
    }

    public int countRowsInTable(String tableId) {
        return this.findElementsByXpathString(tableId).size();
    }
}
