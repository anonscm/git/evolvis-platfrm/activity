/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.world;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.apache.commons.io.IOUtils;

public class IOWrapper {
    final Process proc;
    final ByteArrayOutputStream errBuffer = new ByteArrayOutputStream();
    final ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
    private PrintStream stdin;
    public IOWrapper(Process p) {
        this.proc = p;
        stdin = new PrintStream(proc.getOutputStream());
    }

    public byte[] stdErr(){
        return errBuffer.toByteArray();
    }
    
    public byte[] stdOut(){
        return errBuffer.toByteArray();
    }
    
    public IOWrapper write(String input){
        stdin.println(input);
        stdin.flush();
        return this;
    }
    
    public int waitFor() throws InterruptedException {
        Thread errCopier = copyThread(errBuffer, proc.getErrorStream());
        Thread outCopier = copyThread(outBuffer, proc.getInputStream());
        int retVal = proc.waitFor();
        errCopier.start();
        outCopier.start();
        errCopier.join();
        outCopier.join();
        return retVal;
    }

    private Thread copyThread(final ByteArrayOutputStream buffer, final InputStream input) {
        return new Thread() {
            public void run() {
                try {
                    IOUtils.copy(input, buffer);
                    buffer.flush();                    
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            };
        };
    }

    public IOWrapper closeStdIn() {
        stdin.close();
        return this;
    }
}
