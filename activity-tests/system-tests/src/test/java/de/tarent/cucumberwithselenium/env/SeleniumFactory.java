/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.env;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import de.tarent.cucumberwithselenium.selenium.RemoteWebDriverWithScreenshotFunctionality;
import de.tarent.cucumberwithselenium.CucumberJUnitRunner;

public class SeleniumFactory {
	private static Map<String, WebDriver> alwaysCreated = new HashMap<String, WebDriver>();
	
	protected final boolean executeHooks;
	
	/**
	 * This constructor will be invoked by spring.
	 */
	public SeleniumFactory(String executeHooks){
		if(CucumberJUnitRunner.isJunitTest()){
			this.executeHooks = CucumberJUnitRunner.isExecuteHooks();
		}else{
			this.executeHooks = Boolean.valueOf(executeHooks);
		}
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode){
		return buildRemote(host, port, browserMode, 10, TimeUnit.SECONDS);
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode, long sleepTime, TimeUnit sleepUnit){
		return buildRemote(host, port, browserMode, sleepTime, sleepUnit, true);
	}
	
	public WebDriver buildRemote(String host, int port, String browserMode, long sleepTime, TimeUnit sleepUnit, boolean loadFromCache){
		if(!executeHooks) return null;
		
		DesiredCapabilities capabilities = null;
		
		String uniqueId = generateUniqueString(host, port, browserMode);
		if(loadFromCache){
			for(String curKey : alwaysCreated.keySet()){
				if(curKey.startsWith(uniqueId)){
					return alwaysCreated.get(curKey);
				}
			}
		}
		
		if(browserMode.toLowerCase().startsWith("ff")){
			//firefox
			capabilities = DesiredCapabilities.firefox();
		}else if(browserMode.toLowerCase().startsWith("ie")){
			//InternetExplorer
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		}else if(browserMode.toLowerCase().startsWith("htmlunit")){
			//HTML-Unit (with javascript)
			capabilities = DesiredCapabilities.htmlUnit();
		}else{
			return null;
		}

		capabilities.setJavascriptEnabled(true);

		WebDriver driver = null;
		try {
			driver = new RemoteWebDriverWithScreenshotFunctionality(
					new URL("http://" + host + ":" + String.valueOf(port) + "/wd/hub"),
					capabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		
		driver.manage().timeouts().implicitlyWait(sleepTime, sleepUnit);

		//put into cache-map
		String key = uniqueId;
		for(int i=0; alwaysCreated.containsKey(key); i++){
			key = uniqueId + "_" + String.valueOf(i);
		}
		alwaysCreated.put(key, driver);
		
		return driver;
	}
	
	private static String generateUniqueString(String host, int port, String browserMode){
		return String.valueOf(host) + "_" +  String.valueOf(port) + "_" + String.valueOf(browserMode);
	}
	
	public static void closeAll(){
		for(WebDriver driver : alwaysCreated.values()){
			try{
				driver.close();
			}catch(Exception e){}
		}
	}
}
