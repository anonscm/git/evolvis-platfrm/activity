/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.formatter;

import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import cucumber.runtime.CucumberException;
import cucumber.runtime.formatter.FormatterFactory;


/**
 * This formatter extends the cucumber HTMFormatter (Version 1.1.1).
 * The cucumber-formatter doesn't support the nice features
 * such like &gt;summary of the results&lt; or &gt;collapse/expand one or
 * all scenarios&lt; but this features be sorely missed! 
 * 
 * This formatter delegates all to the cucumber-formatter. At the least
 * the output HTML-File is edited.
 * 
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class HTMLFormatter implements Reporter, Formatter {
	private static final String EXTENDED_JS_FILE	=	"/htmlformatter/extended.js";
	private static final String FORMATTER_JS_FILE	=	"/htmlformatter/formatter.js";
	private static final String STYLE_CSS_FILE		=	"/htmlformatter/style.css";
	
	protected File htmlReportDir;
	private Formatter baseFormatter;
	private Reporter baseReporter;
	
	public HTMLFormatter(File htmlReportDir) {
		baseFormatter = new FormatterFactory().create("html:" + htmlReportDir.getPath());
		baseReporter = (Reporter)baseFormatter;
		
		this.htmlReportDir = htmlReportDir;
	}

	public void done() {
		baseFormatter.done();
		
		replaceStyleCSSFile();
		replaceFormatterJSFile();
		mergeReportHtml();
	}
	
	private void replaceStyleCSSFile(){
		replaceFile(
				htmlReportDir.getPath() + "/style.css", 
				STYLE_CSS_FILE);
	}

	private void replaceFormatterJSFile(){
		replaceFile(
				htmlReportDir.getPath() + "/formatter.js", 
				FORMATTER_JS_FILE);
	}
	
	private void replaceFile(final String filePath, final String resourcePath) {
		new File(filePath).delete();
		
		InputStream is = null;
		FileOutputStream fw = null;
		try{
			fw = new FileOutputStream(filePath, false);
			is = getClass().getResourceAsStream(resourcePath);
			
			int read;
			byte[] buffer = new byte[4096];
			
			while((read = is.read(buffer)) != -1){
				fw.write(buffer, 0, read);
			}
		}catch(IOException e){
			throw new CucumberException("Unable to replace " + filePath, e);
		}finally{
			if(is != null) try{ is.close(); }catch(IOException e){}
			if(fw != null) try{ fw.close(); }catch(IOException e){}
		}
	}

	private void mergeReportHtml() {
		//after super-method invocation the index.html is stored
		File oldHTML = new File(htmlReportDir.getPath() + "/index.html");
		File newHTML = mergeHTML(oldHTML);

		//replace html-files
		oldHTML.delete();
		newHTML.renameTo(new File(htmlReportDir.getPath() + "/cucumber_report.html"));
	}
	
	private File mergeHTML(File oldHTML){
		File newHTML = new File(htmlReportDir.getPath() + "/index.html_new");
		
		BufferedReader oldHTMLReader = null;
		BufferedWriter newHTMLWriter = null;
		InputStream extendedIS = null;
		
		try {
			oldHTMLReader = new BufferedReader(new FileReader(oldHTML));
			newHTMLWriter = new BufferedWriter(new FileWriter(newHTML));
			extendedIS = getClass().getResourceAsStream(EXTENDED_JS_FILE);
			
			String oldLine = oldHTMLReader.readLine();
			
			while(oldLine != null){
				String newLine = oldLine;
				
				if(oldLine.toLowerCase().contains("</body>")){
					newHTMLWriter.write("\t\t\t<script lang=\"text/javascript\">\n");
					
					//copy content from js-file into new html-file
					byte[] buffer = new byte[16 * 1024];
					int readed = 0;
					while((readed = extendedIS.read(buffer)) != -1){
						newHTMLWriter.write(new String(buffer, 0, readed));
					}
					
					newHTMLWriter.write("\t\t\t</script>\n");
				}
				
				newHTMLWriter.write(newLine + "\n");
				oldLine = oldHTMLReader.readLine();
			}		
		} catch (IOException e) {
			throw new CucumberException("Unable to edit index.html: ", e);
		}finally{
			try{
				if(oldHTMLReader != null) oldHTMLReader.close();
				if(newHTMLWriter != null) newHTMLWriter.close();
				if(extendedIS != null) extendedIS.close();
			}catch(IOException e){}
		}
		
		return newHTML;
	}
	
	public void background(Background background) {
		baseFormatter.background(background);
	}

	public void eof() {
		baseFormatter.eof();
	}

	public void close() {
		baseFormatter.close();
	}

	public void before(Match match, Result result) {
		baseReporter.before(match, result);
	}

	public void after(Match match, Result result) {
		baseReporter.after(match, result);
	}

	public void embedding(String mimeType, byte[] data) {
		baseReporter.embedding(mimeType, data);
	}

	public void feature(Feature feature) {
		baseFormatter.feature(feature);
	}

	public void examples(Examples examples) {
		baseFormatter.examples(examples);
	}

	public void scenario(Scenario scenario) {
		baseFormatter.scenario(scenario);
	}

	public void scenarioOutline(ScenarioOutline scenarioOutline) {
		baseFormatter.scenarioOutline(scenarioOutline);
	}

	public void step(Step step) {
		baseFormatter.step(step);
	}

	public void syntaxError(String s, String s1, List<String> list, String s2,
			Integer integer) {
		baseFormatter.syntaxError(s, s1, list, s2, integer);
	}

	public void result(Result result) {
		baseReporter.result(result);
	}

	public void match(Match match) {
		baseReporter.match(match);
	}

	public void uri(String uri) {
		baseFormatter.uri(uri);
	}

	public void write(String text) {
		baseReporter.write(text);
	}
}
