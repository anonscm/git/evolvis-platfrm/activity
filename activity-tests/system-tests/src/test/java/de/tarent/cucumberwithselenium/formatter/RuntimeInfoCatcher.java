/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.formatter;

import java.util.List;

import gherkin.formatter.Formatter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;

/**
 * This class is a workaround for a missing hook!
 * Because the hook which is called after each step
 * doesn't provide information about the current 
 * feature or current scenario and so on. This class
 * represents a formatter that we refer to the 
 * cucumber-cli. 
 *
 * @author Sven Schumann <s.schumann@tarent.de>
 */
public class RuntimeInfoCatcher implements Formatter {
	private static RuntimeInfoCatcher instance;
	
	private Feature currentFeature;
	private Scenario currentScenario;
	private Step currentStep;
	
	private int currentScenarioCount = 0;
	private int currentStepCount = 0;
	
	public RuntimeInfoCatcher(){
		instance = this;
	}
	
	public static RuntimeInfoCatcher getInstance(){
		return instance;
	}
	
	@Override
	public void feature(Feature feature) {
		if(!feature.equals(currentFeature)){
			currentScenarioCount = 0;
			currentStepCount = 0;
		}
		
		this.currentFeature = feature;
	}

	@Override
	public void scenario(Scenario scenario) {
		if(!scenario.equals(currentScenario)){
			currentScenarioCount++;
			currentStepCount = 0;
		}
		
		this.currentScenario = scenario;
	}

	@Override
	public void step(Step step) {
		if(!step.equals(currentStep)){
			currentStepCount++;
		}
		
		this.currentStep = step;
	}

	public Feature getCurrentFeature() {
		return currentFeature;
	}
	public Scenario getCurrentScenario() {
		return currentScenario;
	}
	public Step getCurrentStep() {
		return currentStep;
	}
	public int getCurrentScenarioCount() {
		return currentScenarioCount;
	}
	public int getCurrentStepCount() {
		return currentStepCount;
	}

	@Override
	public void background(Background arg0) {}

	@Override
	public void close() {}

	@Override
	public void done() {}

	@Override
	public void eof() {}

	@Override
	public void examples(Examples arg0) {}

	@Override
	public void scenarioOutline(ScenarioOutline arg0) {}

	@Override
	public void syntaxError(String arg0, String arg1, List<String> arg2, String arg3, Integer arg4) {}

	@Override
	public void uri(String arg0) {}
}
