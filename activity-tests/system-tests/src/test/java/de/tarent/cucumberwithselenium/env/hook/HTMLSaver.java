/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.cucumberwithselenium.env.hook;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.Scenario;
import de.tarent.cucumberwithselenium.env.GlobalConfig;
import de.tarent.cucumberwithselenium.env.HookRegistry;
import de.tarent.cucumberwithselenium.env.Utils;
import de.tarent.cucumberwithselenium.env.event.HandlerAfter;
import de.tarent.cucumberwithselenium.formatter.RuntimeInfoCatcher;

/**
 * This class represents a hook.
 * They take the html-source after a step failed.
 */
public class HTMLSaver implements HandlerAfter {
	@Autowired WebDriver browser;
	@Autowired GlobalConfig config;
	
	public HTMLSaver(){
		//register me
		HookRegistry.addOnAfterHandler(this);
	}
	
	public void handleAfterScenario(Scenario result) {
		RuntimeInfoCatcher catcher = RuntimeInfoCatcher.getInstance();
		if(catcher == null){
			//this can be happen if the formatter is not configured
			//or the test is running in a IDE-JUnit-Test.
			return;
		}
		
		if(result.isFailed()){
			////
			//save html-result
			////
			if(	config.getHtmlPath() != null &&
				!config.getHtmlPath().equals("")){
				
				String htmlPath = Utils.generateOutputPath(
						config.getHtmlPath(), 
						catcher.getCurrentFeature(), 
						catcher.getCurrentScenarioCount(),
						catcher.getCurrentStepCount());
				htmlPath += ".html";
				
				Utils.createDirectoryIfDoesntExist(htmlPath);
				
				String htmlSource = browser.getPageSource();
				try {
					Utils.writeHtmlFile(htmlSource, htmlPath);
				} catch (IOException e) {
					System.err.println("Error on catching html-source.");
					e.printStackTrace();
				}
			}
		}
	}

}
