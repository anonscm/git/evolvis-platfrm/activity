/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.examples.cuke4duke;

import com.thoughtworks.selenium.Selenium;


/**
 * Singleton class for sharing a single selenium driver instance between java and ruby world.
 * <br><br>
 * 
 * I experimented with a couple of solutions and this one seemed to be the most reliable one.
 * Please let me know if you figure out a way to solve this in a more "Springy" manner.
 * 
 * @author lukas
 *
 */
public class SeleniumSingleton {
    private final static SeleniumSingleton instance= new SeleniumSingleton();
    private SeleniumSingleton(){
    }
    public static SeleniumSingleton getInstance(){
	return instance;
    }
    
    private Selenium selenium;
    
    public Selenium getSelenium() {
	if(selenium==null){
	    throw new RuntimeException("Selenium is not yet initialized. ");
	}
        return selenium;
    }
    public void setSelenium(Selenium selenium) {
        this.selenium = selenium;
    }
    

}
