#!/bin/bash
#set -x

XVFB=`which Xvfb`
if [ "$?" -eq 1 ];
then
    echo "Xvfb not found."
    exit 1
fi

$XVFB :99 -ac &    # launch virtual framebuffer into the background
PID_XVFB="$!"      # take the process ID
export DISPLAY=:99 # set display to use that of the xvfb

# run the tests
. ~/.rvm/scripts/rvm
rvm use 1.9.3
file_name=report.$(date "+%Y.%m.%d-%H.%M.%S").html
cucumber features --format html --out $file_name
firefox $file_name


kill $PID_XVFB     # shut down xvfb (firefox will shut down cleanly by JsTestDriver)
echo "Done."