# language: de

Funktionalität: bugverification


@FixThis
Szenario: Projekte über Ressourcenzuordnung sehen (#3868)
  Angenommen ich betrachte Projekte über Ressourcenzuordnung mit folgenden Rollen		 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Projekte über Ressourcenzuordnung sehen			|		|x		|		|

@TestOK
Szenario: Positionen anlegen Pflichtfeldkennzeichnung (#3650) 
  Angenommen ich lege Positionen an mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Positionen anlegen			|		|x		|x		|

@TestOK
Szenario: Benutzerverwaltung Beschriftung (#3473)
  Angenommen ich betrachte die Benutzerverwaltung mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Benutzerverwaltung betrachten			|		|x		|		|
  
@FixThis(FailsBecauseOf4063)
Szenario: Projektdetails letzte Änderungen (#3599) 
  Angenommen ich betrachte letzte Änderungen unter Projektdetails mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Änderungen betrachten			|		|x		|x		|
  

@TestOK
Szenario: Abbruch von Zuweisung einer Ressource zu einer Position (#3865) 
  Angenommen ich breche die Zuweisung einer Ressource zu einer Position ab mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Zuweisung abbrechen			|		|x		|x		|
  
@TestOK
Szenario: Benutzer anlegen ohne Felder zu befüllen (#3670) 
  Angenommen ich lege einen Benutzer an ohne Felder zu befüllen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Benutzer anlegen ohne Felder zu befüllen			|		|x		|		|
  
@TestOK
Szenario: Auftragsdetails Feldbezeichnungen (#3606) 
  Angenommen ich betrachte Auftragsdetails mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Auftragsdetails betrachten			|		|x		|		|
  
@TestOK
Szenario: Leistungen bearbeiten (#3783) 
  Angenommen ich bearbeite Leistungen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Leistungen bearbeiten			|		|x		|		|

@FixThis(FailsBecauseOf3708)
Szenario: Aufträge betrachten, wenn Projekte über Ressourcenzuordnung erreicht werden (#3678)
  Angenommen ich betrachte Aufträge, wenn Projekte über Ressourcenzuordnung erreicht werden mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Leistungen bearbeiten			|		|x		|		|
  
@TestOK
Szenario: Link meine Löschanfragen (#3647)
  Angenommen ich betrachte die Startseite mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Link Meine Löschanfragen vorhanden			|x		|x		|x		|
  
@FixThis(failsBecauseOf4094)
Szenario: Auftrag löschen (#3862)
  Angenommen ich lösche einen Auftrag mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Auftrag löschen			|		|x		|x		|
  
@TestOK
Szenario: Rechnungen von Aufträgen sehen (#4021)
  Angenommen ich betrachte Rechnungen von Aufträgen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Rechnungen von Aufträgen sehen			|		|x		|x		|
  
@TestOK
Szenario: Rechnung bearbeiten (#3804)
  Angenommen ich bearbeite eine Rechnung mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Rechnung bearbeiten			|		|x		|	|
  
@TestOK
Szenario: Navigieren von Auftragsdetails zum zugehörigen Projekt (#3785)
  Angenommen ich navigiere von Auftragsdetails zum zugehörigen Projekt mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Von Auftragsdetails zum zugehörigen Projekt navigieren		|		|x		|x	|
  
@FixThis01
Szenario: Rechnung aus Detailansicht eines Auftrags heraus bearbeiten (#3861)
  Angenommen ich bearbeite eine Rechnung aus er Detailansicht eines Auftrags heraus mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Rechnung aus Detailansicht eines Auftrags heraus bearbeiten		|		|x		|x	|
  
@FixThis01
Szenario: Rechnungsdetails aus Detailansicht eines Auftrags heraus betrachten (#3861)
  Angenommen ich beatrachte Rechnungsdetails aus er Detailansicht eines Auftrags heraus mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|
  |Rechnungsdetails aus Detailansicht eines Auftrags heraus betrachten		|		|x		|x	|
