# encoding: utf-8
require "rubygems"
require "selenium/client"
require "selenium-webdriver"
require "socket"
require "nokogiri"


Before do

  def driver
    if not @webdriver then
      @webdriver = Selenium::WebDriver.for :firefox
    end
    @webdriver
  end

  def startDebugSocket port=51674
    server = TCPServer.open(port)
    $stderr.puts "To resume execution, telnet to port #{port}."
    @client = server.accept
    @client.puts "exit<Enter>: resume execution.\nanything else<Enter> single step"
  end

  def checkDebugSocket scenario = nil
    if @client then
      @client.puts ">"
      @client.puts scenario.line if scenario
      line = @client.gets
      if not line then
        @client.close
        @client=nil
      elsif line.chomp == "exit" then
        @client.close
        @client=nil
      else
        @client.puts "step"
      end
    end
  end

  checkDebugSocket
end

After do
  @webdriver.close if @webdriver
end

#Before('@with_do') login
#  driver.navigate.to "#{ENV['UI_BASE_URL']}/Anmeldung"
#  driver.find_element(:name => "j_username" ).send_keys("selenium")
#  driver.find_element(:name => "j_password" ).send_keys("selenium")
#  driver.find_element(:name => "j_password" ).submit  
#end

AfterStep do |scenario|
  checkDebugSocket scenario
end

# if requested, fail on slow scenarios
if ENV['TIMEOUT'] then
  Around  do |scenario, block|
    Timeout.timeout ENV['TIMEOUT'].to_f  do
      block.call
    end
  end
end
