# language: de

Funktionalität: activity access rights

#Grundlage:
#  Angenommen ich melde mich an
#  Dann erhalte ich die Startseite
#  Dann darf ich auf Position buchen	
 
@TestOK
Szenario: Rechte und Rollenschema Eigene Leistungen sehen
  Angenommen ich sehe eigene Leistungen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Leistungen sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@basic
Szenario: Rechte und Rollenschema Eigene Leistungen eintragen
  Angenommen ich trage eigene Leistungen mit folgenden Rollen ein
    |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
    |Eigene Leistungen eintragen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@TestOK
Szenario: Rechte und Rollenschema Eigene Leistungen bearbeiten
  Angenommen ich bearbeite eigene Leistungen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Leistungen bearbeiten		|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@ignore
Szenario: Rechte und Rollenschema Eigene Leistungen löschen
  Angenommen ich lösche eigene Leistungen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|    
  |Eigene Leistungen löschen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Leistungen umbuchen
  Angenommen ich buche eigene Leistungen mit folgenden Rollen um
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|    
  |Eigene Leistungen umbuchen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@TestOK
Szenario: Rechte und Rollenschema Eigene Leistungen exportieren
  Angenommen ich exportiere eigene Leistungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Leistungen exportieren		|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@basic
Szenario: Rechte und Rollenschema Leistungen eines Projekts sehen
  Angenommen ich sehe Leistungen eines Projekts mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Leistungen eines Projekts sehen		|		|x		|x		|x		|	|x		|x		|x		|		|

@TestOK
Szenario: Rechte und Rollenschema Leistungen eines Bereichs sehen
  Angenommen ich sehe die Leistungen eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Leistungen eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Leistungen sehen
  Angenommen ich betrachte alle Leistungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Leistungen sehen			|		|x		|		|		|	|		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema alle Leistungen löschen
  Angenommen ich lösche alle Leistungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Leistungen löschen			|		|x		|		|		|	|		|		|		|		|

@ignore
Szenario: Rechte und Rollenschema alle Leistungen umbuchen
  Angenommen ich buche alle Leistungen mit folgenden Rollen um 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Leistungen umbuchen			|		|x		|		|		|	|		|		|		|		|
  
@basic
Szenario: Rechte und Rollenschema Alle Leistungen importieren
  Angenommen ich importiere alle Leistungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Leistungen importieren			|		|x		|		|		|	|		|		|		|		|

@ignore
Szenario: Rechte und Rollenschema Alle Leistungen exportieren #fails because of #3780
  Angenommen ich exportiere alle Leistungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Leistungen exportieren			|		|x		|		|		|	|		|		|		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Eigene Überstunden sehen
  Angenommen ich betrachte eigene Überstunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Überstunden sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@TestOK
Szenario: Rechte und Rollenschema Eigene Überstunden eintragen
  Angenommen ich trage eigene Überstunden mit folgenden Rollen ein  
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Überstunden eintragen		|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@basic
Szenario: Rechte und Rollenschema Eigene Überstunden bearbeiten
  Angenommen ich bearbeite eigene Überstunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Überstunden bearbeiten		|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@basic
Szenario: Rechte und Rollenschema Eigene Überstunden löschen
  Angenommen ich lösche eigene Überstunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Überstunden löschen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@TestOK
Szenario: Rechte und Rollenschema Überstunden eines Projekts sehen
  Angenommen ich betrachte die Überstunden eines Projektes mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Überstunden eines Projekts sehen		|		|x		|x		|x		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Überstunden eines Projekts exportieren #fails because functionality not yet implemented
  Angenommen ich exportiere Überstunden eines Projekts mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Überstunden eines Projekts exportieren	|		|x		|		|		|	|x		|		|		|		|

@ignore
Szenario: Rechte und Rollenschema Überstunden eines Bereichs sehen
  Angenommen ich betrachte die Überstunden eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Überstunden eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Überstunden eines Bereichs exportieren #fails because functionality not yet implemented
  Angenommen ich exportiere Überstunden eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Überstunden eines Bereichs exportieren	|		|x		|		|		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Überstunden sehen
  Angenommen ich betrachte alle Überstunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Überstunden sehen			|		|x		|		|		|	|		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Überstunden eintragen - Funktionalität noch nicht vorhanden
  Angenommen ich trage alle Überstunden mit folgenden Rollen ein 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Überstunden eintragen			|		|x		|		|		|	|		|		|x		|		|
   
@ignore
Szenario: Rechte und Rollenschema Alle Überstunden löschen
  Angenommen ich lösche alle Überstunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Überstunden löschen			|		|x		|		|		|	|		|		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Alle Überstunden abfeiern
  Angenommen ich feier alle Überstunden mit folgenden Rollen ab 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Überstunden abfeiern			|		|x		|		|		|	|		|		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Eigene Abwesenheiten sehen
  Angenommen ich betrachte eigene Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Abwesenheiten sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@TestOK
Szenario: Rechte und Rollenschema Eigene Abwesenheiten beantragen
  Angenommen ich beantrage eigene Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Abwesenheiten beantragen		|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@TestOK
Szenario: Rechte und Rollenschema Eigene Abwesenheiten bearbeiten
  Angenommen ich bearbeite eigene Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Abwesenheiten bearbeiten		|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@basic
Szenario: Rechte und Rollenschema Eigene Abwesenheiten löschen #fails because functionality not yet implemented
  Angenommen ich lösche eigene Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Abwesenheiten löschen		|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Abwesenheiten genehmigen #fails because functionality not yet implemented
  Angenommen ich genehmige auf der Startseite alle Abwesehnheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Abwesenheiten genehmigen		|		|		|x		|		|	|x		|		|x		|		|

@ignore
Szenario: Rechte und Rollenschema Abwesenheiten eines Projektes genehmigen #fails because of #3818
  Angenommen ich genehmige auf der Startseite Abwesehnheiten eines Projekts mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Abwesenheiten einess Projekts genehmigen		|		|		|x		|		|	|x		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Abwesenheiten eines Projektes bearbeiten #fails because of #3818 
  Angenommen ich bearbeite auf der Startseite Abwesenheiten eines Projekts mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Abwesenheiten eines Projekts bearbeiten		|		|		|x		|		|	|x		|		|x		|		| 

@ignore
Szenario: Rechte und Rollenschema Abwesenheiten eines Bereichs bearbeiten #fails because of #3818
  Angenommen ich bearbeite auf der Startseite Abwesenheiten eines Bereichs
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Abwesenheiten eines Bereichs bearbeiten		|		|		|		|		|	|x		|		|		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Abwesenheiten eines Projekts sehen
  Angenommen ich betrachte Abwesenheiten eines Projekts mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Abwesenheiten eines Projekts sehen		|		|x		|x		|x		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Abwesenheiten eines Bereichs sehen
  Angenommen ich betrachte Abwesenheiten eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Abwesenheiten eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Alle Abwesenheiten sehen
  Angenommen ich betrachte alle Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Abwesenheiten sehen			|		|x		|		|		|	|		|x		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Alle Abwesenheiten exportieren
  Angenommen ich exportiere alle Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Abwesenheiten exportieren		|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Abwesenheiten löschen
  Angenommen ich lösche alle Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Abwesenheiten löschen			|		|x		|		|		|	|		|		|x		|		|
  
@basic
Szenario: Rechte und Rollenschema Alle Abwesenheiten bearbeiten
  Angenommen ich bearbeite alle Abwesenheiten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Abwesenheiten bearbeiten		|		|x		|		|		|	|		|		|x		|		|

@ignore
Szenario: Rechte und Rollenschema Eigene Skills eintragen
  Angenommen ich trage eigene Skills mit folgenden Rollen ein 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Skills eintragen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Skills sehen
  Angenommen ich betrachte eigene Skills mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Skills sehen				|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Skills bearbeiten
  Angenommen ich bearbeite eigene Skills mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Eigene Skills bearbeiten			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Skills löschen
  Angenommen ich lösche eigene Skills mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Skills löschen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Skills sehen
  Angenommen ich betrachte alle Skills mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Alle Skills sehen				|		|		|		|		|	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Skills der MA eines Bereichs sehen
  Angenommen ich betrachte alle Skills der MA eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Alle Skills der MA eines Bereichs sehen	|		|		|		|		|	|x		|		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Kunden sehen
  Angenommen ich betrachte einen Kunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Kunden sehen					|		|x		|x		|		|	|x		|		|		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Kunden anlegen
  Angenommen ich lege einen Kunden mit folgenden Rollen an 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Kunden anlegen				|		|x		|x		|		|	|x		|		|		|x		|

@TestOK
Szenario: Rechte und Rollenschema Kunden bearbeiten
  Angenommen ich bearbeite einen Kunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Kunden bearbeiten				|		|x		|x		|		|	|x		|		|		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Kunden löschen
  Angenommen ich lösche einen Kunden mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Kunden löschen				|		|x		|x		|		|	|x		|		|		|x		|

@basic
Szenario: Rechte und Rollenschema Projekte anlegen
  Angenommen ich lege ein Projekt mit folgenden Rollen an 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Projekte anlegen				|		|x		|x		|		|	|x		|x		|		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Projekte sehen ohne Rechnung und Finanzen
  Angenommen ich betrachte eigene Projekte ohne Rechnung und Finanzen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Projekte sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Projekte sehen komplett
  Angenommen ich betrachte eigene Projekte komplett mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Projekte sehen			|		|x		|x		|		|	|x		|x		|		|		|

@ignore
Szenario: Rechte und Rollenschema Eigene Projekte bearbeiten
  Angenommen ich bearbeite eigene Projekte mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Projekte bearbeiten			|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Projekte löschen
  Angenommen ich lösche eigene Projekte mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Projekte löschen			|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Projekte eines Bereichs sehen
  Angenommen ich betrachte Projekte eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Projekte eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
 
@ignore
Szenario: Rechte und Rollenschema Projekte eines Bereichs bearbeiten
  Angenommen ich bearbeite Projekte eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Projekte eines Bereichs bearbeiten		|		|x		|		|		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Projekte eines Bereichs löschen
  Angenommen ich lösche Projekte eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Projekte eines Bereichs löschen		|		|x		|		|		|	|x		|		|		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Alle Projekte sehen
  Angenommen ich betrachte alle Projekte mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Projekte sehen				|		|x		|		|		|	|		|x		|x		|x		|
  
@basic
Szenario: Rechte und Rollenschema Alle Projektdetails sehen
  Angenommen ich betrachte alle Projektdetails mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Projektdetails sehen			|		|x		|		|		|	|		|x		|x		|x		|

@ignore
Szenario: Rechte und Rollenschema Alle Projekte bearbeiten
  Angenommen ich bearbeite alle Projekte mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Projekte bearbeiten			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Projekte löschen
  Angenommen ich lösche alle Projekte mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Alle Projekte löschen			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Aufträge anlegen
  Angenommen ich lege Auftraege mit folgenden Rollen an 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Aufträge anlegen				|		|x		|x		|		|	|x		|x		|		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Aufträge ohne Leistungen und Rechnungen und Kosten sehen
  Angenommen ich betrachte eigene Aufträge ohne Leistungen und Rechnungen und Kosten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Eigene Aufträge sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Aufträge komplett sehen
  Angenommen ich betrachte eigene Aufträge komplett mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Eigene Aufträge sehen			|		|x		|x		|		|	|x		|x		|		|		|

@basic
Szenario: Rechte und Rollenschema Eigene Aufträge bearbeiten
  Angenommen ich bearbeite eigene Aufträge mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Eigene Aufträge bearbeiten			|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Aufträge löschen
  Angenommen ich lösche eigene Aufträge mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Eigene Aufträge löschen			|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Aufträge eines Bereichs sehen
  Angenommen ich betrachte Aufträge eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Aufträge eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Aufträge eines Bereichs bearbeiten
  Angenommen ich bearbeite Aufträge eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Aufträge eines Bereichs bearbeiten		|		|x		|		|		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Aufträge eines Bereichs löschen
  Angenommen ich lösche Aufträge eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Aufträge eines Bereichs löschen		|		|x		|		|		|	|x		|		|		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Alle Aufträge sehen
  Angenommen ich betrachte alle Aufträge mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|  
  |Alle Aufträge sehen				|		|x		|		|

@TestOK
Szenario: Rechte und Rollenschema Aufträge von Mitarbeiter unter Verwaltung sehen
  Angenommen ich betrachte Aufträge von Mitarbeiter unter Verwaltung mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|  
  |Alle Aufträge sehen				|		|x		|		|
  
@TestOK
Szenario: Rechte und Rollenschema Aufträge von Projektleiter unter Verwaltung sehen
  Angenommen ich betrachte Aufträge von Projektleiter unter Verwaltung mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter|  
  |Alle Aufträge sehen				|		|x		|x		|
  
@basic
Szenario: Rechte und Rollenschema Alle Aufträge bearbeiten
  Angenommen ich bearbeite alle Aufträge mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Alle Aufträge bearbeiten			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Aufträge löschen
  Angenommen ich lösche alle Aufträge mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	| 
  |Alle Aufträge löschen			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Positionen anlegen
  Angenommen ich lege Positionen mit folgenden Rollen an 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Positionen anlegen				|		|x		|x		|		|	|x		|x		|		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Positionen sehen zugeordnet - alte Verlinkung bis v0.12
  Angenommen ich betrachte eigene zugeordnete Positionen mit alter Verlinkung mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Positionen sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@TestOKFailsBecauseOf#3824
Szenario: Rechte und Rollenschema Eigene Positionen sehen zugeordnet
  Angenommen ich betrachte eigene zugeordnete Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Positionen sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Positionen sehen verantwortlich
  Angenommen ich betrachte eigene verantwortliche Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Positionen sehen			|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Positionen bearbeiten
  Angenommen ich bearbeite eigene Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Positionen bearbeiten		|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Eigene Positionen löschen
  Angenommen ich lösche eigene Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Eigene Positionen löschen			|		|x		|x		|		|	|x		|x		|x		|x		|
  
@ignore
Szenario: Rechte und Rollenschema Positionen eines Bereichs sehen
  Angenommen ich betrachte Positionen eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Positionen eines Bereichs sehen		|		|x		|		|		|	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Positionen eines Bereichs bearbeiten
  Angenommen ich bearbeite Positionen eins Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Positionen eines Bereichs bearbeiten	|		|x		|		|		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Positionen eines Bereichs löschen
  Angenommen ich lösche Positionen eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Positionen eines Bereichs löschen		|		|x		|		|		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Positionen sehen
  Angenommen ich betrachte alle Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Alle Positionen sehen			|		|x		|		|		|	|		|x		|x		|x		|
  
@basic
Szenario: Rechte und Rollenschema Alle Positionen bearbeiten
  Angenommen ich bearbeite alle Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Alle Positionen bearbeiten			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Positionen löschen
  Angenommen ich lösche alle Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Positionen löschen			|		|x		|		|		|	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen sehen
  Angenommen ich betrachte Ressourcen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen sehen				|		|x		|x		|x		|x	|x		|x		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen anlegen
  Angenommen ich lege Ressourcen mit folgenden Rollen an
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen anlegen				|		|x		|		|		|x	|		|		|x		|		|

@ignore
Szenario: Rechte und Rollenschema Ressourcen bearbeiten
  Angenommen ich bearbeite Ressourcen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen bearbeiten			|		|x		|		|		|x	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen sperren
  Angenommen ich sperre Ressourcen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen sperren				|		|x		|		|		|x	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen löschen
  Angenommen ich lösche Ressourcen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen löschen				|		|x		|		|		|x	|		|		|x		|		|
  
@basic
Szenario: Rechte und Rollenschema Ressourcen zu Positionen hinzufügen
  Angenommen ich füge Ressourcen zu Positionen mit folgenden Rollen hinzu
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Ressourcen zu Positionen hinzufügen		|		|x		|x		|x		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen zu Positionen entfernen
  Angenommen ich entferne Ressourcen zu Positionen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen zu Positionen entfernen		|		|x		|x		|x		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Ressourcen Rechte zuteilen
  Angenommen ich teile Ressourcen Rechte mit folgenden Rollen zu 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Ressourcen Rechte zuteilen			|		|x		|		|		|x	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Auftragsrechnungen sehen
  Angenommen ich betrachte Auftragsrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Auftragsrechnungen sehen			|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Auftragsrechnungen einstellen
  Angenommen ich stelle Auftragsrechnungen mit folgenden Rollen ein 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Auftragsrechnungen einstellen		|		|x		|		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Auftragsrechnungen bearbeiten
  Angenommen ich bearbeite Auftragsrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Auftragsrechnungen bearbeiten		|		|x		|		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Auftragsrechnungen löschen
  Angenommen ich lösche Auftragsrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Auftragsrechnungen löschen			|		|x		|		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Projektrechnungen sehen
  Angenommen ich betrachte Projektrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Projektrechnungen sehen			|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Projektrechnungen einstellen
  Angenommen ich stelle Projektrechnungen mit folgenden Rollen ein 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Projektrechnungen einstellen		|		|x		|		|		|	|x		|x		|		|		|

@ignore
Szenario: Rechte und Rollenschema Projektrechnungen bearbeiten
  Angenommen ich bearbeite Projektrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Projektrechnungen bearbeiten		|		|x		|		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Projektrechnungen löschen
  Angenommen ich lösche Projektrechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Projektrechnungen löschen			|		|x		|		|		|	|x		|x		|		|		|
  
@basic
Szenario: Rechte und Rollenschema Alle Rechnungen sehen
  Angenommen ich betrachte alle Rechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Rechnungen sehen			|		|x		|		|		|	|		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Rechnungen eines Bereichs sehen
  Angenommen ich betrachte alle Rechnungen eines Bereichs mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Rechnungen eines Bereichs sehen	|		|x		|		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Rechnungsdetails sehen	
  Angenommen ich betrachte alle Rechnungsdetails mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Rechnungsdetails sehen			|		|x		|		|		|	|x		|x		|		|		|
  
@basic
Szenario: Rechte und Rollenschema Alle Rechnungen bearbeiten	
  Angenommen ich bearbeite alle Rechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Rechnungsdetails sehen			|		|x		|		|		|	|		|x		|		|		|  
  
@ignore
Szenario: Rechte und Rollenschema LDAP Benutzer und Gruppen importieren
  Angenommen ich importiere LDAP Benutzer und Gruppen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |LDAP Benutzer und Gruppen importieren	|		|		|		|		|x	|		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Berechtigungen bearbeiten
  Angenommen ich bearbeite Berechtigungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Berechtigungen bearbeiten			|		|x		|		|		|	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Kosten anlegen
  Angenommen ich lege Kosten mit folgenden Rollen an 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Kosten anlegen				|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Kosten sehen
  Angenommen ich betrachte alle Kosten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Kosten sehen				|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Kostendetails sehen
  Angenommen ich betrachte alle Kostendetails mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Kostendetails sehen			|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Alle Kosten bearbeiten
  Angenommen ich bearbeite alle Kosten mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Alle Kosten bearbeiten			|		|x		|x		|		|	|x		|x		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Löschanfragen stellen
  Angenommen ich stelle Löschanfragen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Löschanfragen stellen			|		|x		|		|		|	|		|		|x		|		|
  
@ignore
Szenario: Rechte und Rollenschema Löschanfragen beantworten
  Angenommen ich beantworte Löschanfragen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Löschanfragen beantworten			|x		|x		|x		|x		|x	|x		|x		|x		|x		|
 
@ignore
Szenario: Rechte und Rollenschema Löschanfragen sehen
  Angenommen ich betrachte Löschanfragen mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|
  |Löschanfragen stellen			|x		|x		|x		|x		|x	|x		|x		|x		|x		| 
 
@basic
Szenario: Rechte und Rollenschema Alle Rechnungen bearbeiten
  Angenommen ich bearbeite Rechnungen mit folgenden Rollen 
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Rechnungen bearbeiten			|		|x		|		|		|	|		|x		|		|		|

@ignore
Szenario: Rechte und Rollenschema Meine Projekte und Aktive Projekte unter meine aktiven Projekte sehen
  Angenommen ich beatrachte Meine Projekte und Aktive Projekte unter meine aktiven Projekte mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Meine Projekte und Aktive Projekte unter meine aktiven Projekte sehen			|x		|x		|x		|x		|x	|x		|x		|x		|x		|

@ignore
Szenario: Rechte und Rollenschema Projektleitung unter meine aktiven Projekte sehen
  Angenommen ich beatrachte Projektleitung unter meine aktiven Projekte mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Projektleitung unter meine aktiven Projekte sehen			|		|		|x		|x		|	|x		|		|		|		|
  
@ignore
Szenario: Rechte und Rollenschema Benutzerverwaltung sehen
  Angenommen ich beatrachte Benutzerverwaltung mit folgenden Rollen
  |Funktion 					|Mitarbeiter	|Controller	|Projektleiter	|Architekt	|Admin	|Bereichsleiter	|Buchhaltung	|Personal	|Vertrieb	|  
  |Benutzerverwaltung sehen			|		|x		|		|		|x	|		|		|x		|		|
  
