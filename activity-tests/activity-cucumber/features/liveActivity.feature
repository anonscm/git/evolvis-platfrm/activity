# language: de

Funktionalität: Activity Basistest

Grundlage:
   Angenommen ich melde mich an
#  Dann erhalte ich die Startseite
#  Dann darf ich auf Position buchen	

@basic
Szenario: Projekt anlegen	
	Angenommen ich betrachte die Ansicht zum Anlegen von Projekten
	Wenn ich folgendes Projekt anlege
	|Name	|Startdatum	|
	|Start	|heute	|
	Dann erscheint unter "Projekt"
	|Projekt	|
	|Start	|
	Und unter dem zugehörigen Kunden wird das Projekt angezeigt
	
@ignore
Szenario: Projekt bearbeiten
  Angenommen ich bearbeite das Projekt "Start"
  Wenn ich den Namen des Projektes in "Anders" ändere
  Dann erscheint unter "Projekt" das Projekt "Anders"
  Und unter dem zugehörigen Kunden wird der Projektname "Anders" angezeigt

@ignore
Szenario: Projekt löschen
  Angenommen ich betrachte die Ansicht zur Löschung von Projekten
  Angenommen unter dem Projekt sind keinerlei Leistungen verbucht worden
  Wenn ich ein Projekt lösche
  Dann wird dieses Projekt nicht länger unter "Projekte" angezeigt
  Und auch unter dem zugehörigen Kunden wird das Projekt nicht länger angezeigt

@ignore
Szenario: Auftrag anlegen
  Angenommen ich betrachte die Ansicht zum Anlegen von Aufträgen
  Wenn ich folgenden Auftrag anlege
  # Pflichtfelder müssen befüllt werden, egal womit.
  |Name		|Typ			|Status		|
  |Auftrag1	|Kundenauftrag		|Beauftragt	|
  Dann erscheint unter "Aufträge"
  |Name		|Status		|Typ			|
  |Auftrag1	| Beauftragt	| Kundenauftrag		|
  Und unter dem zugehörigen Projekt wird der Auftrag angezeigt

@basic
Szenario: Auftrag bearbeiten
  Angenommen ich bearbeite den Auftrag "Auftrag1"
  Wenn ich den Namen des Auftrages in "Auftrag2" ändere
  Dann erscheint unter „Aufträge“ der Auftrag "Auftrag2"
  Und unter dem zugehörigen Projekt wird der Auftragsname "Auftrag2" angezeigt

@ignore
Szenario: Auftrag löschen
  Angenommen ich betrachte die Ansicht zur Löschung von Aufträgen
  Angenommen unter dem Auftrag sind keinerlei Leistungen verbucht worden
  Wenn ich einen Auftrag lösche
  Dann wird dieser Auftrag nicht länger unter "Aufträge" angezeigt
  Und auch unter dem zugehörigen Projekt wird der Auftrag nicht länger angezeigt  

@basic
Szenario: Leistungen eintragen auf der Startseite
	Wenn ich folgende Leistungen auf der Startseite eintrage:
	|Auftrag auswählen		| Position auswählen 	| Stunde 		| Datum |
	|Test Auftrag		   	| Test Position			| 8		| heute |
	Und unter "Meine Stunden im Überblick" wird der Wert "8.0" angezeigt
	
@basic	
Szenario: Leistungen eintragen auf der Leistungen-Seite
	Angenommen ich befinde mich auf der "Meine Leistungen" -Seite
	Wenn ich folgende Leistungen auf der "Meine Leistungen" Seite eintrage:
	|Auftrag auswählen	| Position auswählen 	| Stunde 	| Datum |
	|Test Auftrag	   	| Test Position		| 8		| heute |
	Dann erscheint unter "Meine Leistungen" die Testdaten
	|Auftrag		| Position		| Stunden	| Datum |
	|Test Auftrag		| Test Position		| 8,0		| heute |
	Und unter "Meine Stunden im Überblick" wird der Wert "8.0" angezeigt

@basic
Szenario: Leistungen ändern in der "Leistungen bearbeiten"-Ansicht
	Angenommen ich befinde mich in der "Leistungen bearbeiten"-Ansicht
	Wenn ich die eingetragenen Leistungen folgendermaßen ändere
	|Auftrag auswählen	|Position auswählen	|Stunden	|Datum	|
	|Test Auftrag		|Test Position		|6		|heute	|
	Dann erscheinen unter "Meine Leistungen" die geänderten Daten
	|Auftrag auswählen	|Position auswählen	|Stunden	|Datum	|
	|Test Auftrag		|Test Position		|6		|heute	|
	Und unter "Meine Stunden im Überblick" wird die geänderte Stundenzahl angezeigt
	
@basic
Szenario: Leistungen löschen
	Angenommen ich betrachte die Ansicht zum Löschen von Leistungen
	Wenn ich Löschung der Leistung mit „OK“ bestätige
	Dann ist die gesamte Zeile des ausgewählte Eintrags gelöscht
	Und unter „Meine Stunden im Überblick“ wird die geänderte Stundenzahl angezeigt

@ignore
Szenario: Leistungen als Controller löschen
	  Angenommen ich betrachte die Ansicht zum Erstellen einer Löschanfrage für Leistungen
	  Wenn ich eine Löschanfrage erstelle
	  Dann wird unter "Löschanfragen" die neue Löschanfrage angezeigt
	  Und unter "To dos" wird dem entsprechenden Mitarbeiter die Löschanfrage angezeigt

@basic
Szenario: Leistungen exportieren
	  Angenommen ich habe mithilfe des Filters zwei Leistungen ausgewählt
	  Wenn ich mich in der Leistungen exportieren Ansicht befinde
	  Dann speichere ich die Datei im Excel-Format auf meinem Desktop

	  
@basic
Szenario: Abwesenheit eintragen
	Wenn ich unter "Urlaub" folgende Daten eintrage
	|Typ	|Von	|Bis	|Projektleiter auswählen	|
	|Urlaub	|heute	|morgen	|admin, activity			|
	Dann erscheinen in der "Abwesenheitsliste" die Daten
	|Von	|Bis	|Typ	|Status		|Tage	|Antworten	|
	|heute	|morgen	|Urlaub	|beantragt	|2	|admin, activity	|
	Und in "Abwesende Mitarbeiter" erscheinen die Daten
	|Von	|Bis	|Typ	|Status		|Tage	|Antworten	|
	|heute	|morgen	|Urlaub	|beantragt	|2	|admin, activity	|
	
@basic
Szenario: Abwesenheit ändern in der "Urlaub"-Ansicht
	Wenn ich eine eingetragene Abwesenheit folgendermaßen ändere
	|Typ		|Von	|Bis		|
	|Krankheit	|heute	|übermorgen	|
	Dann erscheinen unter der "Abwesenheitsliste" die Daten
	|Von		|Bis		|Typ		|Status		|Tage	|
	|heute		|übermorgen	|Krankheit	|beantragt	|3	|
	Und unter "Abwesende Mitarbeiter" erscheinen die Daten
	|Von		|Bis		|Typ		|Status		|Tage	|
	|heute		|übermorgen	|Krankheit	|beantragt	|3	|

@ignore
Szenario: Abwesenheiten genehmigen
	Wenn ich unter "Urlaub" folgende Daten eintrage
	|Typ	|Von	|Bis	|Projektleiter auswählen	|
	|Urlaub	|heute	|morgen	|admin, activity			|
	Angenommen ich betrachte die Ansicht zum genehmigen von Abwesenheiten
	Wenn ich folgende Abwesenheit genehmige
	|Name		|Von		|Bis		|Tage	|
	|activity admin	|heute		|morgen		|2	|
	Dann erscheint unter "Abwesende Mitarbeiter" folgender Eintrag
	|Ressource	|Von		|Bis		|Tage	|Status		|
	|activity admin	|heute		|morgen		|2	|genehmigt	|
	Und unter der "Abwesenheitsliste" von "activity admin" wird der genehmigte Urlaub angezeigt

@basic
Szenario: Abwesenheiten bearbeiten
	Wenn ich unter "Urlaub" folgende Daten eintrage
	|Typ	|Von	|Bis	|Projektleiter auswählen	|
	|Urlaub	|heute	|morgen	|admin, activity			|
	Angenommen ich betrachte die Controlleransicht zur Bearbeitung von Abwesenheiten
	Wenn ich folgende Abwesenheit bearbeite
	|Name		|Von		|Bis			|Tage	|
	|activity admin	|heute		|morgen			|2	|
	Und "Bis" auf "übermorgen" setze
	Dann erscheinen unter der "Abwesende Mitarbeiter" die Daten
	|Ressource	|Von		|Bis			|Tage	|
	|activity admin	|heute		|übermorgen		|3	|
	Und unter "Abwesenheitsliste" erscheinen die Daten
	|Von		|Bis			|Tage	|
	|heute		|übermorgen		|3	|
	
@basic

Szenario: Überstunden eintragen
	#es können keine überstunden eingetragen werden
	Wenn ich unter "Meine Überstunden" folgende Daten eintrage
	|Datum		|Stunden	|
	|gestern	|2		|
	Dann erscheinen unter "Meine Überstunde" die Daten
	|Datum		|Stunden	|
	|gestern	|2		|
	Und unter "meine Stunden im Überblick" wird die geänderte Überstundenzahl angezeigt

@ignore

Szenario: Überstunden abfeiern 
	Wenn ich unter „Überstunden abbauen“ folgende Daten eintrage
	|Datum		|Stunden	|
	|heute		|1		|
	Dann erscheinen unter „Meine Überstunden“ die Daten
	|Stunden	|Datum		|
	|1		|heute		|
	Und unter „Meine Stunden im Überblick“ wird die geänderte Überstundenzahl angezeigt

@ignore
Szenario: Überstunden als Controller löschen
	Angenommen ich betrachte die Ansicht zum Erstellen einer Löschanfrage für Überstunden
	Wenn ich die Löschanfrage erstelle
	Dann wird unter „Löschanfragen“ die neue Löschanfrage angezeigt
	Und unter "To dos" wird dem entsprechenden Mitarbeiter die Löschanfrage angezeigt
	
	
@basic
Szenario:Abwesenheiten als Controller bearbeiten
	Angenommen ich betrachte die Ansicht zur Genehmigung von Abwesenheiten
	Wenn ich den Abwesenheits-Antrag genehmige
	Dann wird der Status unter „Abwesende Mitarbeiter“ und unter der „Abwesenheitsliste“als „genehmigt“ angezeigt
	
@ignore
Szenario: Abwesenheit als Controller löschen
	Angenommen ich betrachte die Ansicht zum Löschen von Abwesenheiten
	Wenn ich eine Löschanfrage erstelle
	#Fehlermeldung beim Abschicken der Löschanfrage
	Dann erscheint unter „Abwesende Mitarbeiter“ bei der Abwesenheit der Status „Löschanfrage“
	Und in der Benutzeransicht erscheint eine neue Löschanfrage
	Und unter "To dos" wird dem entsprechenden Mitarbeiter die Löschanfrage angezeigt
	
@ignore
Szenario: Kunden anlegen
	Wenn ich einen Kunden neu anlege
	Dann erscheint unter "Kunden" der neue Eintrag

@basic
Szenario: Position anlegen
  Angenommen ich betrachte die Ansicht zum Anlegen von Positionen
  Wenn ich folgende Position anlege
  |Name		|Status		|
  |Position1	|in Arbeit	|
  Dann erscheint unter "Positionen"
  |Name		|Status		|
  |Position1	|in Arbeit	|
  Und unter dem zugehörigen Auftrag wird die Position angezeigt

@basic
Szenario: Position bearbeiten
  Angenommen ich bearbeite die Position "Position1"
  Wenn ich den Namen der Position in "Position2" ändere
  Dann erscheint unter "Positionen" die Position "Position2"
  Und unter dem zugehörigen Auftrag wird der Positionsname "Position2" angezeigt

@ignore
Szenario: Position löschen
  Angenommen ich betrachte die Ansicht zur Löschung von Positionen
  Angenommen unter der Position sind keinerlei Leistungen verbucht worden
  Wenn ich eine Position lösche
  Dann wird diese Position nicht länger unter "Positionen" angezeigt
  Und auch unter dem zugehörigen Auftrag wird die Position nicht länger angezeigt

@basic
Szenario: Ressource zuordnen
  Angenommen ich betrachte die Ansicht zum Zuordnen von Ressourcen
  Wenn ich der Position eine Ressource mit dem Status "aktiv" zuordne
  Dann erscheint diese Ressource unter "Mitarbeiter" mit dem ausgewählten Status
  Und auch unter "Ressourcenzuordnung" erscheint die Ressource mit einer Verknüpfung zu dem Projekt, zu dem die Position gehört

@ignore
Szenario: Mitarbeiter anlegen
  Angenommen ich betrachte die Ansicht zum Hinzufügen von Stammdaten
  Wenn ich den folgenden Mitarbeiter anlege
  |Vorname	|Nachname	|Benutzername		|
  |Lutz		|Lurch		|llurch			|
  Dann erscheint unter "Benutzer"
  |Nachname	|Vorname	|
  |Lurch	|Lutz		|

@ignore
Szenario: Bearbeiten von Mitarbeitern
  Angenommen ich bearbeite den Mitarbeiter "Lutz Lurch"
  Wenn ich den Nachnamen des Mitarbeiters auf "Lerche" ändere
  Dann erscheint unter "Benutzer"
  |Nachname		|Vorname	|
  |Lerche		|Lutz		|


@ignore 
Szenario: muss noch geklärt werden
  Dann erscheint unter "Deinen letzten fünf Leistungen" die Testdaten
  |Auftrag		| Position		| Stunden	| Datum |
  |Test Auftrag		| Test Position		| 8,0		| heute |
  
  
@ignore
Szenario: test
	Angenommen ich trage Urlaub ein
	Und ich dann den Urlaubstag ändere
	Dann kann ich den selben Projektleiter wieder auswählen
	