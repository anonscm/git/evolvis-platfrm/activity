# encoding: utf-8

Angenommen /^ich lege Positionen mit folgenden Rollen an$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div-top']/input[@value='Neue Position']")
		tmp3 = suche(:xpath=>"//h2[contains(text(),'Position hinzufügen/bearbeiten')]")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene zugeordnete Positionen mit alter Verlinkung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a[1]/span")
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
		tmp4 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp5 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[1]/a")
		tmp6 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[2]/a")
		tmp = auswertung_sechs(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene zugeordnete Positionen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Meine Positionen')]")
		tmp3 = sucheUndKlicken(:xpath=>"//a[text()='FIX AND COMPLETE THIS, NOT YET IMPLEMENTED IN ACTIVITY']")
		tmp = auswertung_sechs(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene verantwortliche Positionen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		tmp2 = suche(:xpath=>"//a[text()='ID']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end 

Angenommen /^ich bearbeite eigene Positionen mit folgenden Rollen$/ do |table|
	
end

Angenommen /^ich betrachte alle Positionen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		tmp2 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form-positions']")		
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite alle Positionen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[9]/div/a")
		tmp3 = suche(:xpath=>"//h2[contains(text(),'Position hinzufügen/bearbeiten')]")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lege Positionen an mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/div[1]/input")
		tmp3 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/fieldset[1]/div[1]/div/div[1]/label[contains(text(),'Position*:')]")
		tmp4 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/fieldset[1]/div[1]/div/div[2]/label[contains(text(),'Auftrag auswählen*:')]")
		tmp5 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/fieldset[1]/div[1]/div/div[3]/label[contains(text(),'Status*:')]")
		tmp6 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/fieldset[3]/div/div/div[1]/label[contains(text(),'Preis (Netto)*:')]")
		tmp7 = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/fieldset[3]/div/div/div[2]/label[contains(text(),'Geschätzter Arbeitsaufwand*:')]")
		tmp = auswertung_sieben(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

