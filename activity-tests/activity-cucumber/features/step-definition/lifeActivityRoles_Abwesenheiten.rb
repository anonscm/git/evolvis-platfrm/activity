# encoding: utf-8
Angenommen /^ich genehmige auf der Startseite alle Abwesehnheiten mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[9]/div/a")
		tmp3 = genehmigen
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end 

Angenommen /^ich betrachte Abwesenheiten eines Projekts mit folgenden Rollen$/ do |table|

end

Angenommen /^ich exportiere alle Abwesenheiten mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_holidayportlet_WAR_activityportlets_form']/div[4]/input")
		tmp = auswertung_zwei(tmp1, tmp2)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche alle Abwesenheiten mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[10]/div/a")
		tmp3 = suche(:xpath=>"//div[@class='button-div']/input[@value='Senden']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite alle Abwesenheiten mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = suche(:xpath=>"//tr[1]/td[9]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte alle Abwesenheiten mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = suche(:xpath=>".//*[@id='_holidayportlet_WAR_activityportlets_form-loss']")
		tmp = auswertung_zwei(tmp1, tmp2) 
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich betrachte Abwesenheiten eines Bereichs mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Abwesenheiten']")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'Alle Abwesenheiten')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich genehmige auf der Startseite Abwesehnheiten eines Projekts mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>".//*[@id='_countmyrequestsportlet_WAR_activityportlets_mainDiv']/div/ul/li[2]/a")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'FIX THIS, FUNCTIONALITY NOT YET IN ACTIVITY')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich bearbeite auf der Startseite Abwesenheiten eines Projekts mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>".//*[@id='_countmyrequestsportlet_WAR_activityportlets_mainDiv']/div/ul/li[2]/a")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'FIX THIS, FUNCTIONALITY NOT YET IN ACTIVITY')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich bearbeite auf der Startseite Abwesenheiten eines Bereichs$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>".//*[@id='_countmyrequestsportlet_WAR_activityportlets_mainDiv']/div/ul/li[2]/a")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'FIX THIS, FUNCTIONALITY NOT YET IN ACTIVITY')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end