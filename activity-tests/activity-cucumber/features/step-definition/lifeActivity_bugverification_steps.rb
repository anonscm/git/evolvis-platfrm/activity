# encoding: utf-8

Angenommen /^ich betrachte letzte Änderungen unter Projektdetails mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Kunden']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_clientsportlet_WAR_activityportlets_form-clients']/table/tbody[2]/tr[1]/td[5]/div/a")
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_clientsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[1]/td[5]/div/a")
		tmp4 = sucheUndKlicken(:xpath=>".//*[@id='_clientsportlet_WAR_activityportlets_form']/div[1]/input[3]")
		tmp5 = suche(:xpath=>".//*[@id='_clientsportlet_WAR_activityportlets_form']/h2[contains(.,'Letzte Änderungen')]")
		tmp = auswertung_fuenf(tmp1, tmp2, tmp3, tmp4, tmp5)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich breche die Zuweisung einer Ressource zu einer Position ab mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		clickOnPositionen = false
        clickDetailButtonOfAPosition = false
        clickResourceZuweisenButton = false
        clickCancelFromResourceZuweisenScreen = false
        checkIfWirtschaftsdatenIsPresent = false

		clickOnPositionen = sucheUndKlicken(:xpath=>"//a[text()='Positionen']")
		if clickOnPositionen
		    clickDetailButtonOfAPosition = sucheUndKlicken(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form-positions']/table/tbody[2]/tr[1]/td[10]/div/a")
		    if clickDetailButtonOfAPosition
		        clickResourceZuweisenButton = sucheUndKlicken(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/div[1]/input[2]")
                if clickResourceZuweisenButton
                    clickCancelFromResourceZuweisenScreen = sucheUndKlicken(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/div[5]/input[2]")
                    if clickCancelFromResourceZuweisenScreen
                        checkIfWirtschaftsdatenIsPresent = suche(:xpath=>".//*[@id='_positionsportlet_WAR_activityportlets_form']/h3[contains(.,'Wirtschaftsdaten')]")
                    end
                end
            end
		end


		result = auswertung_fuenf(clickOnPositionen, clickDetailButtonOfAPosition, clickResourceZuweisenButton, clickCancelFromResourceZuweisenScreen, checkIfWirtschaftsdatenIsPresent)
		fehlermeldung+=werteTabZuordnen(result, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich lege einen Benutzer an ohne Felder zu befüllen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='addUserButton']")
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_form']/div[2]/input[1]")
		tmp4 = sucheUndKlicken(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_form']/fieldset[2]/div/div/div/span[contains(.,'Bitte geben Sie das Anstellungsverhältnis an.')]")
		tmp = auswertung_vier(tmp1, tmp2, tmp3, tmp4)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich betrachte Auftragsdetails mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[1]/td[7]/div/a")
		tmp3 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[7]/div[1]/div[1]/span[contains(.,'Geschätzter Aufwand')]")
		tmp4 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[7]/div[2]/div[1]/span[contains(.,'Tatsächlicher Aufwand')]")
		tmp5 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[7]/div[3]/div[1]/span[contains(.,'Kommunizierter Aufwand')]")
		tmp6 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[7]/div[2]/div[2]/span[contains(.,'Kosten aus Leistungen')]")
		tmp7 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[7]/div[3]/div[2]/span[contains(.,'Sonstige Kosten')]")
		tmp = auswertung_sieben(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich bearbeite Leistungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_activitiesportlet_WAR_activityportlets_form-activities']/table/tbody[2]/tr[1]/td[8]/div/a")
		tmp3 = suche(:xpath=>".//*[@id='_activitiesportlet_WAR_activityportlets_form']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich betrachte die Startseite mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Löschanfragen']")
		tmp2 = suche(:xpath=>".//*[@id='_myrequestsportlet_WAR_activityportlets_mainDiv']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich lösche einen Auftrag mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[1]/td[7/div/a]")
		tmp3 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[3]/a")
		tmp3 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[3]/a")
		tmp3 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-activities']/table/tbody[2]/tr[1]/td[7]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich betrachte Rechnungen von Aufträgen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[1]/td[7]/div/a")
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[4]/a")
		tmp4 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-bills']")
		tmp = auswertung_vier(tmp1, tmp2, tmp3, tmp4)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich bearbeite eine Rechnung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_invoiceportlet_WAR_activityportlets_form-bills']/table/tbody[2]/tr[1]/td[11]/div/a")
		tmp3 = suche(:xpath=>".//*[@id='_invoiceportlet_WAR_activityportlets_mainDiv']/h2[contains(.,'Rechnungsdetails')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich navigiere von Auftragsdetails zum zugehörigen Projekt mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[1]/td[7]/div/a")
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_mainDiv']/div[3]/div[2]/div[2]/a[contains(.,'Projekt')]")
		tmp4 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_detailsTab']/h2[contains(.,'Projekt')]")
		tmp = auswertung_vier(tmp1, tmp2, tmp3, tmp4)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end