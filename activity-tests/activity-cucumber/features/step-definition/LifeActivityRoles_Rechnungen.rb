# encoding: utf-8

Angenommen /^ich betrachte Auftragsrechnungen mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich stelle Auftragsrechnungen mit folgenden Rollen ein$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div-top']/input[@value='Neue Rechnung']")
		tmp3 = suche(:xpath=>"//div[@class='button-div']/input[@value='Speichern']")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite Auftragsrechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[10]/div/a")
		tmp3 = suche(:xpath=>"//div[@class='button-div']/input[@value='Speichern']")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche Auftragsrechnungen mit folgenden Rollen$/ do |table|
	
end

Angenommen /^ich betrachte Projektrechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp3 = suche(:xpath=>"//li/a[contains(text(),'Rechnung')]")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich stelle Projektrechnungen mit folgenden Rollen ein$/ do |table|
	
end

Angenommen /^ich bearbeite Projektrechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp3 = suche(:xpath=>"//li/a[contains(text(),'Rechnung')]")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte alle Rechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		tmp2 = suche(:xpath=>"//div[@class='button-div-top']/input[@value='Neue Rechnung']")		
		tmp = auswertung_zwei(tmp1, tmp2)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte alle Rechnungsdetails mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[11]/div/a")
		tmp3 = suche(:xpath=>"//div[@class='button-div-top']/input[@value='Bearbeiten']")		
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche Projektrechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp3 = sucheUndKlicken(:xpath=>"//li/a[contains(text(),'Rechnung')]")
		tmp4 =	suche(löschenButton)#muss noch eingefügt werden	
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite Rechnungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Rechnungen']")		
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[10]/div//a")
		tmp3 = suche(:xpath=>".//*[@id='_invoiceportlet_WAR_activityportlets_form']/h2[contains(text(),'Rechnung hinzufügen/bearbeiten')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)	
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

