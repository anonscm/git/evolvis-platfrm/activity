# encoding: utf-8


Angenommen /^ich betrachte eigene Überstunden mit folgenden Rollen$/ do |table|
    verbinden
	fehlermeldung=""
	tableOut=rausAusTabelle(table)
	for i in 0..alleRollen.length-1
	    testResult = false
		anmeldung(alleRollen[i])
		if sucheUndKlicken(:xpath=>"//a[text()='Meine Überstunden']") == true
		   if countNumerOfRowsInTable(:xpath=>".//*[@id='_myovertimeportlet_WAR_activityportlets_form-overtime']/table/tbody[2]/tr") > 0
		        testResult = true
		   end
		end
		fehlermeldung+=werteTabZuordnen(testResult, tableOut[i], alleRollen[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich trage eigene Überstunden mit folgenden Rollen ein$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Überstunden']")
		tmp2 = ueberstunden_eintragen(projekt_array[i])
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich exportiere Überstunden eines Projekts mit folgenden Rollen$/ do |table|

end

Angenommen /^ich betrachte alle Überstunden mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Überstunden']")
		tmp2 = suche(:xpath=>"//div[@id='_overtimeportlet_WAR_activityportlets_mainDiv']/h2[contains(text(),'Überstundenliste')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche alle Überstunden mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Überstunden']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp3 = suche(:xpath=>"//div[@id='_overtimeportlet_WAR_activityportlets_mainDiv']/h2[contains(text(),'Löschanfrage senden')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich feier alle Überstunden mit folgenden Rollen ab$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Überstunden']")
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div']/input[@value='Überstunden abbauen']")
		tmp3 = suche(:xpath=>"//div[@id='_overtimeportlet_WAR_activityportlets_mainDiv']/form/h2[contains(text(),'Überstunden abbauen')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite eigene Überstunden mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Überstunden']")
		tmp2 = sucheUndKlicken(:xpath=>"//tbody/tr[1]/td[6]/div/a")
		tmp3 = suche(:xpath=>"//div[@id='_myovertimeportlet_WAR_activityportlets_mainDiv']/form/h2[contains(text(),'Überstunden eintragen/abbauen')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

 Angenommen /^ich lösche eigene Überstunden mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Überstunden']")
		tmp2 = suche(:xpath=>"//tbody/tr[1]/td[7]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte die Überstunden eines Projektes mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Überstunden']")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'Überstundenliste')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte die Überstunden eines Bereichs mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	projekt_array = projekte
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Überstunden']")
		tmp2 = suche(:xpath=>"//h2[contains(text(),'Überstundenliste')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end