# encoding: utf-8

Angenommen /^ich importiere LDAP Benutzer und Gruppen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp = suche(:xpath=>"//a[text()='LDAP Benutzer importieren']")
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich beatrachte Benutzerverwaltung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = suche(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_form-users']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end