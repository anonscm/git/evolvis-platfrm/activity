# encoding: utf-8

Angenommen /^ich lege ein Projekt mit folgenden Rollen an$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")
		tmp2 = suche(:xpath=>"//div[@class='button-div top-margin']/input[@value='Neues Projekt']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene Projekte ohne Rechnung und Finanzen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		if ! suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
			tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a[1]/span")
		end
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
		tmp4 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[1]/a")
		tmp5 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[2]/a")
		tmp = auswertung_fuenf(tmp1, tmp2, tmp3, tmp4, tmp5)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene Projekte komplett mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		if ! suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
			tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a[1]/span")
		end
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
		tmp4 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[1]/a")
		tmp5 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[2]/a")
		tmp6 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[3]/a")
		tmp7 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_form']/ul/li[4]/a")
		tmp = auswertung_sieben(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite eigene Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp = bearbeiteEigenesProjekt(projekt)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich lösche eigene Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp = löscheEigenesProjekt(projekt)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich betrachte Projekte eines Bereichs mit folgenden Rollen$/ do |table|
	
end

Angenommen /^ich betrachte alle Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")
		tmp2 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Admin')]")
		tmp3 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Architekt')]")
		tmp4 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Bereichsleiter')]")
		tmp5 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Buchhaltung')]")
		tmp6 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Controller')]")
		tmp7 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Mitarbeiter')]")
		tmp8 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Personal')]")
		tmp9 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Projektleiter')]")
		tmp10 = sucheUndKlicken(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/div[3]/a[contains(.,'next')]")
		tmp11 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-projects']/table/tbody[2]/tr[contains(.,'Projekt Vertrieb')]")
		tmp = auswertung_elf(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11)  
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte alle Projektdetails mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")
		if rolleOut[i]=="controller"
			tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		else
			tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[5]/div/a")
		end
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite alle Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[5]/div/a")
		tmp3 = suche(:xpath=>"//div[@class='button-div']/input[@value='Speichern']")	
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich beatrachte Meine Projekte und Aktive Projekte unter meine aktiven Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		tmp2 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Meine Projekte')]")
		tmp3 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Aktive Projekte')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich beatrachte Projektleitung unter meine aktiven Projekte mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		tmp2 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Projektleitung')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte Projekte über Ressourcenzuordnung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Ressourcenzuordnung']")
		tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_resourceallocationportlet_WAR_activityportlets_form-allocations']/table/tbody[2]/tr[7]/td[4]/div/a")
		tmp3 = suche(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_form-jobs']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end