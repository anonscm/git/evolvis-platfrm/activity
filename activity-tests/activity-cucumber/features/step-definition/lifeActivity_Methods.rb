# encoding: utf-8

#Daten zum Anlegen anlegen
def auftraege
	auftrag_Mitarbeiter="Job Mitarbeiter"
	auftrag_Controller="Job Controller"
	auftrag_Projektleiter="Job Projektleiter"
	auftrag_Architekt="Job Architekt"
	auftrag_Admin="Job Admin"
	auftrag_Bereichsleiter="Job Bereichsleiter"
	auftrag_Buchhaltung="Job Buchhaltung"
	auftrag_Personal="Job Personal"
	auftrag_Vertrieb="Job Vertrieb"
	auftrag_array=[auftrag_Mitarbeiter, auftrag_Controller, auftrag_Projektleiter, auftrag_Architekt, auftrag_Admin, auftrag_Bereichsleiter, auftrag_Buchhaltung, auftrag_Personal, auftrag_Vertrieb]
	return auftrag_array
end

def ressource
	res_mittest="Mitarbeiter Test"
	res_contest="Controller Test"
	res_protest="Projektleiter Test"
	#res_arctest="Architekt Test" #ROLLENREDUZIERUNG
	#res_admtest="Admin Test"
	#res_bertest="Bereichsleiter Test"
	#res_buctest="Buchhaltung Test"
	#res_pertest="Personal Test"
	#res_vertest="Vertrieb Test"
	#res_array=[res_mittest, res_contest, res_protest, res_arctest, res_admtest, res_bertest, res_buctest, res_pertest, res_vertest]
	res_array=[res_mittest, res_contest, res_protest]
	return res_array
end

def positionen
	position_Mitarbeiter="Position Mitarbeiter"
	position_Controller="Position Controller"
	position_Projektleiter="Position Projektleiter"
	position_Architekt="Position Architekt"
	position_Admin="Position Admin"
	position_Bereichsleiter="Position Bereichsleiter"
	position_Buchhaltung="Position Buchhaltung"
	position_Personal="Position Personal"
	position_Vertrieb="Position Vertrieb"
	position_array=[position_Mitarbeiter, position_Controller, position_Projektleiter, position_Architekt, position_Admin, position_Bereichsleiter, position_Buchhaltung, position_Personal, position_Vertrieb]
	return position_array
end

def projekte
	projekt_Mitarbeiter="Projekt Mitarbeiter"
	projekt_Controller="Projekt Controller"
	projekt_Projektleiter="Projekt Projektleiter"
	projekt_Architekt="Projekt Architekt"
	projekt_Admin="Projekt Admin"
	projekt_Bereichsleiter="Projekt Bereichsleiter"
	projekt_Buchhaltung="Projekt Buchhaltung"
	projekt_Personal="Projekt Personal"
	projekt_Vertrieb="Projekt Vertrieb"
	projekt_array=[projekt_Mitarbeiter, projekt_Controller, projekt_Projektleiter, projekt_Architekt, projekt_Admin, projekt_Bereichsleiter, projekt_Buchhaltung, projekt_Personal, projekt_Vertrieb]
	return projekt_array
end

def verbinden_slow
	#base=ENV['UI_BASE_URL']
	#driver.navigate.to("#{base}")
	driver.navigate.to("activity-ci:8080")
end

def verbinden
	driver.navigate.to("activity-ci:8080")
end


def anmeldung(rolle)
	wait = Selenium::WebDriver::Wait.new(:timeout => 20)
	if rolle == "mitarbeiter"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "mittest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
	end
	if rolle == "controller"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "contest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
	end
	if rolle == "projektleiter"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "protest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
	end
end

def anmeldung_slow(rolle)
	wait = Selenium::WebDriver::Wait.new(:timeout => 20)
	if rolle == "mitarbeiter"
		#wait.until { driver.find_element(:id => "field-username") }
		sleep 5
		driver.find_element(:id =>"field-username").send_keys "mittest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 5
	#	driver.navigate.refresh
	end
	if rolle == "controller"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "contest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "projektleiter"
		#wait.until { driver.find_element(:id => "field-username") }
		sleep 1
		driver.find_element(:id =>"field-username").send_keys "protest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "architekt"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "arctest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "admin"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "admtest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "bereichsleiter"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "bertest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "buchhaltung"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "buctest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "personal"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "pertest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end
	if rolle == "vertrieb"
		wait.until { driver.find_element(:id => "field-username") }
		driver.find_element(:id =>"field-username").send_keys "vertest"
		driver.find_element(:id =>"field-password").send_keys "test"
		driver.find_element(:id => "login_btn").click
		sleep 1
		#driver.navigate.refresh
	end

end

def rausAusTabelle(table)
	table.hashes.each do |row|
		mitarbeiterx=row['Mitarbeiter']
		controllerx=row['Controller']
		projektleiterx=row['Projektleiter']
		#architektx=row['Architekt'] #ROLLENREDUZIERUNG
		#adminx=row['Admin']
		#bereichsleiterx=row['Bereichsleiter']
		#buchhaltungx=row['Buchhaltung']
		#personalx=row['Personal']
		#vertriebx=row['Vertrieb']
		#table_array = [mitarbeiterx, controllerx, projektleiterx, architektx, adminx, bereichsleiterx, buchhaltungx, personalx, vertriebx]
		table_array = [mitarbeiterx, controllerx, projektleiterx]
		return table_array
	end
end

def alleRollen
	mitarbeiter="mitarbeiter"
	controller="controller"
	projektleiter="projektleiter"
	#architekt="architekt"
	#admin="admin" #ROLLENREDUZIERUNG
	#bereichsleiter="bereichsleiter"
	#buchhaltung="buchhaltung"
	#personal="personal"
	#vertrieb="vertrieb"
	#rollen_array = [mitarbeiter, controller, projektleiter, architekt, admin, bereichsleiter, buchhaltung, personal, vertrieb]
	rollen_array = [mitarbeiter, controller, projektleiter]
	return rollen_array
end

def suche(arg)
	test = driver.find_elements(arg).size
	if test==1
		bool=true
    else
        bool=false
	end
	return bool
end

def sucheUndKlicken(arg)
	test = driver.find_elements(arg).size
	if test==1
		driver.find_element(arg).click
		sleep 2
		bool=true
    else
        bool=false
    end
	return bool
end

def auswertung_elf(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6 and arg7 and arg8 and arg9 and arg10 and arg11
		return true
	else
		return false
	end
end

def auswertung_zehn(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6 and arg7 and arg8 and arg9 and arg10
		return true
	else
		return false
	end
end

def auswertung_neun(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6 and arg7 and arg8 and arg9
		return true
	else
		return false
	end
end

def auswertung_acht(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6 and arg7 and arg8
		return true
	else
		return false
	end
end

def auswertung_sieben(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6 and arg7
		return true
	else
		return false
	end
end

def auswertung_sechs(arg1, arg2, arg3, arg4, arg5, arg6)
	if arg1 and arg2 and arg3 and arg4 and arg5 and arg6
		return true
	else
		return false
	end
end

def auswertung_fuenf(arg1, arg2, arg3, arg4, arg5)
	if arg1 and arg2 and arg3 and arg4 and arg5
		return true
	else
		return false
	end
end

def auswertung_vier(arg1, arg2, arg3, arg4)
	if arg1 and arg2 and arg3 and arg4
		return true
	else
		return false
	end
end

def auswertung_drei(arg1, arg2, arg3)
	if arg1 and arg2 and arg3
		return true
	else
		return false
	end
end

def auswertung_zwei(arg1, arg2)
	if arg1 and arg2
		return true
	else
		return false
	end
end


def werteTabZuordnen(bool, tableOut, rolleOut)
	fehlermeldung=""
	if tableOut=="x"
		if bool==false
			#startDebugSocket
			fehlermeldung << "Fehler bei folgender Rolle: " + rolleOut + " \n"
		end
	end
	if tableOut==""
		if bool==true
			#startDebugSocket
			fehlermeldung << "Fehler bei folgender Rolle: " + rolleOut + " \n"
		end
	end
	return fehlermeldung
end

def loggout
	loggout_btn=driver.find_elements(:xpath => "//form/div[@class='button-div']/input[@id='logout_btn']")
	if loggout_btn.size==0
		raise"Der Loggout Button wurde nicht gefunden"
	end
	if loggout_btn.size==1
		driver.find_element(:xpath => "//form/div[@class='button-div']/input[@id='logout_btn']").click
        driver.manage.timeouts.implicit_wait = 2
        driver.find_element(:xpath => "//div[@id='logoarea']/a/img").click
	end
end

def ausgabe(i, exception)
	#if i==8 #ROLLENREDUZIERUNG
	if i==2
		if not exception.empty?
			raise exception
		end
	end
end

def ueberstunden_eintragen(arg)
	select_box = driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-projectId")
	dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
	box_bef = dropdown_box.select_by(:text, "#{arg}")

	driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-date").click
	driver.find_element(:xpath=>"//div/button[contains(text(),'Heute')]").click

	#driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-hours").send_keys "1"
	driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-description").send_keys "test desc"

    driver.find_element(:xpath=>"//input[@value='Speichern']").click
    return true

end

def abwesenheiten_eintragen
	sleep(3)
	tmp = driver.find_elements(:xpath=>"//div[@class='button-div-top']/input")
	if tmp.size==0
		bool = false
	else
		driver.manage.timeouts.implicit_wait = 1
		driver.find_element(:xpath=>"//div[@class='button-div-top']/input").click
		driver.manage.timeouts.implicit_wait = 1
		select_box = driver.find_element(:xpath=>"//div[@class='layout-cell cell-fifty']/select")
		dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
		box_bef = dropdown_box.select_by(:text, "Urlaub")

		driver.find_element(:id=>"_myholidayportlet_WAR_activityportlets_form-startDate").click

		driver.find_element(:xpath=>"//button[@class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all']").click

		driver.find_element(:id=>"_myholidayportlet_WAR_activityportlets_form-endDate").click

		driver.find_element(:xpath=>"//button[@class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all']").click

		driver.find_element(:xpath=>"//input[@value='Speichern']").click
		bool = true
	end
	return bool
end

def genehmigen
	sleep 1
	tmp=driver.find_elements(:xpath=>"//div[@class='layout-cell cell-thirty-three']/select")
	if tmp.size==0
		bool=false
	end
	if tmp.size==1
		select_box = driver.find_element(:xpath=>"//div[@class='layout-cell cell-thirty-three']/select")
		dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
		box_bef = dropdown_box.select_by(:text, "genehmigt")
		bool=true
	end
	return bool
end

def skillHinzu
	sleep 1
	tmp = driver.find_elements(:id=>"_myskillsportlet_WAR_activityportlets_form-skillsDefId")
	if tmp.size==0
		bool=false
	end
	if tmp.size==1
		select_box = driver.find_element(:id=>"_myskillsportlet_WAR_activityportlets_form-skillsDefId")
		dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
		box_bef = dropdown_box.select_by(:text, "java")
		bool=true
	end
	tmpz = driver.find_elements(:xpath=>"//div[@class='layout-cell cell-fifty']/select")
	if tmpz.size==0
		bool=false
	end
	if tmpz.size==1
		select_box = driver.find_element(:xpath=>"//div[@class='layout-cell cell-fifty']/select")
		dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
		box_bef = dropdown_box.select_by(:text, "1-Professional")
		driver.find_element(:xpath=>"//div[@class='button-div']/input[@class='save']").click
		bool=true
	end
	return bool
end

def betrachteEigenesProjekt(projekt)
	tmp = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
	if tmp.size==0
		driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Meine Projekte')]").click
		sleep 1
		tmpz = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
		if tmpz.size==0
			bool=false
		end
		if tmpz.size==1
			bool=true
		end
	end
	if tmp.size==1
		bool=true
	end
	return bool

end

def bearbeiteEigenesProjekt(projekt)
	tmp = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
	if tmp.size==0
		driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Meine Projekte')]").click
		sleep 1
		tmpz = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
		if tmpz.size==0
			bool=false
		end
		if tmpz.size==1
			driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]").click
			sleep 1
			neuTmp=driver.find_elements(:xpath=>"//div[@class='button-div-top']/input[@value='Bearbeiten']")
			if neuTmp.size==0
				bool=false
			end
			if neuTmp.size==1
				bool=true
			end
		end
	end
	if tmp.size==1
		driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]").click
		sleep 1
		neuTmp=driver.find_elements(:xpath=>"//div[@class='button-div-top']/input[@value='Bearbeiten']")
		if neuTmp.size==0
			bool=false
		end
		if neuTmp.size==1
			bool=true
		end
	end
	return bool
end

def betrachteEigeneAuftraege(projekt)
	tmp = driver.find_elements(:xpath=>"//ul/li/a[contains(text(),'#{projekt}')]")
	if tmp.size==0
		driver.find_element(:xpath=>"//a/span[contains(text(),'Aktive Projekte')]").click
		tmpz = driver.find_elements(:xpath=>"//ul/li/a[contains(text(),'#{projekt}')]")
		if tmpz.size==0
			bool=false
		end
		if tmpz.size==1
			driver.find_element(:xpath=>"//ul/li/a[contains(text(),'#{projekt}')]").click
			sleep 1
			tmpx = driver.find_elements(:xpath=>"//tr[1]/td[6]/div/a")
			if tmpx.size==0
				bool=false
			end
			if tmpx.size==1
				driver.find_element(:xpath=>"//tr[1]/td[6]/div/a").click
				bool=true
			end
		end
	end
	if tmp.size==1
		driver.find_element(:xpath=>"//ul/li/a[contains(text(),'#{projekt}')]").click
		sleep 1
		tmpx = driver.find_elements(:xpath=>"//tr[1]/td[6]/div/a")
		if tmpx.size==0
			bool=false
		end
		if tmpx.size==1
			driver.find_element(:xpath=>"//tr[1]/td[6]/div/a").click
			bool=true
		end
	end
end

def loeschanfragen
	res=ressource
	anzahl=res.size
	for i in 0..anzahl-1
		sleep 1
		tmp = driver.find_elements(:xpath=>"//a[text()='Abwesenheiten']")
		if tmp.size==0
			bool=false
		end
		if tmp.size==1
			driver.find_element(:xpath=>"//a[text()='Abwesenheiten']").click
		end
		sleep 1
		test = driver.find_elements(:xpath=>"//tr[td[1]/div[contains(text(),'#{res[i]}')]]/td[10]/div/a")
		if test.size==0
			test2 = driver.find_elements(:xpath=>"//div[@class='yui-dt-paginator yui-pg-container']/a[@class='yui-pg-next']")
			if test2.size==0
				bool=false
			else
				driver.find_element(:xpath=>"//div[@class='yui-dt-paginator yui-pg-container']/a[@class='yui-pg-next']").click
				sleep 1
				test3 = driver.find_elements(:xpath=>"//tr[td[1]/div[contains(text(),'#{res[i]}')]]/td[10]/div/a")
				if test3.size==0
					bool=false
				end
				if test3.size==1
					driver.find_element(:xpath=>"//tr[td[1]/div[contains(text(),'#{res[i]}')]]/td[10]/div/a").click
					sleep 1
					tmp_senden = driver.find_elements(:xpath=>"//div[@class='button-div']/input[@value='Senden']")
					if tmp_senden.size==1
						driver.find_element(:xpath=>"//div[@class='button-div']/input[@value='Senden']").click
						bool=true
					end
					if tmp_senden.size==0
						bool = false
					end
				end
			end
		end
		if test.size==1
			driver.find_element(:xpath=>"//tr[td[1]/div[contains(text(),'#{res[i]}')]]/td[10]/div/a").click
			sleep 1
			#driver.find_element(:xpath=>"//div[@class='button-div']/input[@value='Senden']").click
			tmp_senden2 = driver.find_elements(:xpath=>"//div[@class='button-div']/input[@value='Senden']")
					if tmp_senden2.size==1
						driver.find_element(:xpath=>"//div[@class='button-div']/input[@value='Senden']").click
						bool=true
					end
					if tmp_senden2.size==0
						bool = false
					end
			bool=true
		end
	end
	return bool
#	res=ressource
#	anzahl=res.size
#	for i in 0..anzahl-1
		#test = driver.find_elements(:xpath=>"//tr[td[1]/div[contains(text(),'#{res[i]}')]]/td[10]/div/a")
		#if test.size==0
		#	driver.find_element(:xpath=>"//div[@class='yui-dt-paginator yui-pg-container']/a[@class='yui-pg-next']").click
		#else
		#	puts("klappt")
		#end
#		puts(res[i])
#	end


end

#def loescheEigenesProjekt(projekt)
#	tmp = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
#	if tmp.size==0
#		driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a/span[contains(text(),'Meine Projekte')]").click
#		sleep 1
#		tmpz = driver.find_elements(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]")
#		if tmpz.size==0
#			bool=false
#		end
#		if tmpz.size==1
#			driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]").click
#			sleep 1
#			neuTmp=driver.find_elements("LÖSCHENBUTTON")
#			if neuTmp.size==0
#				bool=false
#			end
#			if neuTmp.size==1
#				bool=true
#			end
#		end
#	end
#	if tmp.size==1
#		driver.find_element(:xpath=>"//div[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul/li/a[contains(text(),'#{projekt}')]").click
#		sleep 1
#		neuTmp=driver.find_elements("LÖSCHENBUTTON")
#		if neuTmp.size==0
#			bool=false
#		end
#		if neuTmp.size==1
#			bool=true
#		end
#	end
#	return bool
#end

def selectHundredRowsPerSeite(selectBoxPath)
    test = 0
    wait = Selenium::WebDriver::Wait.new(:timeout => 3)
    wait.until { test = driver.find_elements(selectBoxPath).size }

   	if test==0
   		return false
   	else
   		select_box = driver.find_element(selectBoxPath)
   		dropdown_box = Selenium::WebDriver::Support::Select.new(select_box)
        box_bef = dropdown_box.select_by(:text, "100")
   	end
    return true
end

def countNumerOfRowsInTable(dataTable)
    return driver.find_elements(dataTable).size
end