# encoding: utf-8

Angenommen /^ich trage eigene Skills mit folgenden Rollen ein$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Skills']")
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div']/input[@id='addSkillsButton']")
		tmp3 = skillHinzu
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end 

Angenommen /^ich betrachte eigene Skills mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp = sucheUndKlicken(:xpath=>"//a[text()='Meine Skills']")		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite eigene Skills mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Skills']")		
		tmp2 = suche(:xpath=>"//tr[1]/td[3]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche eigene Skills mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine Skills']")		
		tmp2 = suche(:xpath=>"//tr[1]/td[4]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end
