# encoding: utf-8

#################################Szenario: Rechte und Rollenschema !!!EIGENE LEISTUNGEN!!!#################################

Angenommen /^ich trage eigene Leistungen mit folgenden Rollen ein$/ do |table|	
	verbinden
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..alleRollen.length-1
	    testResult = false
		anmeldung(alleRollen[i])
		if ( clickLeistungEintragenButton() )
            randomBeschreibung = "Ich habe heute gearbeitet: random letter ist "+ rand(100..110).chr
            populateLeistungEintragenMask(randomBeschreibung)
            clickSaveButton(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form']/div[4]/input[1]")
            if weFindTheRandomBeschreibungInTheTableFirstRow(randomBeschreibung)
                testResult = true
            end
		end

        fehlermeldung+=werteTabZuordnen(testResult, tableOut[i], alleRollen[i])
        loggout
        ausgabe(i, fehlermeldung)

	end		
end

Angenommen /^ich sehe eigene Leistungen mit folgenden Rollen$/ do |table|
	verbinden
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..alleRollen.length-1
	    testResult = false
	    anmeldung(alleRollen[i])
	    if( sucheUndKlicken(:xpath=>"(//a[contains(text(), 'Meine Leistungen')])[1]") )
            if countNumerOfRowsInTable(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_activities']/table/tbody/tr") > 0
                testResult = true
            end
        end
		fehlermeldung+=werteTabZuordnen(testResult, tableOut[i], alleRollen[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite eigene Leistungen mit folgenden Rollen$/ do |table|
	verbinden
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..alleRollen.length-1
	    testResult = false
		anmeldung(alleRollen[i])
		if sucheUndKlicken(:xpath=>"(//a[contains(text(), 'Meine Leistungen')])[1]")
		    if clickErsteLeistungEditButton()
                uniqueDesc = "Edited Description "+ rand(100..110).chr
                clearAndSetTextField("_myactivitiesportlet_WAR_activityportlets_form_add-description", uniqueDesc)
                sucheUndKlicken(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_add']/div[4]/input[@value='Speichern']")  #click save button
                sleep 5
                if suche(:xpath=>".//*[@id='successMsg']")
                    if leistungBearbeitenPruefen(uniqueDesc)
                        testResult = true;
                    end
                end
		    end

            fehlermeldung+=werteTabZuordnen(testResult, tableOut[i], alleRollen[i])
            loggout
            ausgabe(i, fehlermeldung)

		end
	end
end

Angenommen /^ich lösche eigene Leistungen mit folgenden Rollen$/ do |table|
	verbinden
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"(//a[contains(text(), 'Meine Leistungen')])[1]")
		tmp2 = suche(:xpath => "//tbody/tr[1]/td[7]/div/a")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich exportiere eigene Leistungen mit folgenden Rollen$/ do |table|
	verbinden
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"(//a[contains(text(), 'Meine Leistungen')])[1]")
		tmp2 = suche(:xpath=>"//div[@class='button-div']/input[@value='Als Excel Tabelle exportieren']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich buche eigene Leistungen mit folgenden Rollen um$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"(//a[contains(text(), 'Meine Leistungen')])[1]")
		tmp2 = sucheUndKlicken(:xpath=>"//tbody/tr[1]/td[7]/div/a")
		tmp3 = suche(:xpath=>"//form/h2[contains(text(),'Leistung bearbeiten')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

### Helper methods ###

def getRandomNumberWithRange(range)
    return rand(range)
end

def clearAndSetTextField(textFieldPath, textToSet)
    driver.find_element(:id =>textFieldPath).clear
	driver.find_element(:id =>textFieldPath).send_keys textToSet
end

def clickSaveButton(buttonLink)
    driver.find_element(buttonLink).click
    sleep 2
end

def populateLeistungEintragenMask(randomBeschreibung)
    driver.find_element(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-jobId']/optgroup[1]/option").click
    sleep 2
    #sucheUndKlicken(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-positionId']")
    driver.find_element(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-positionId']/option[2]").click

    sucheUndKlicken(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-date']")
    driver.find_element(:xpath=>".//*[@id='ui-datepicker-div']/div[2]/button[1]").click
    driver.find_element(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-evolvisTaskId']").send_keys 2
    clearAndSetTextField("_myactivitiesportlet_WAR_activityportlets_form-hours", 8)
    driver.find_element(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-description']").send_keys randomBeschreibung
end

def displayCountOfRows()
   driver.find_element(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-activities']/span[4]")

end

def clickLeistungEintragenButton()
   return sucheUndKlicken(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-activities']/table/tbody[2]/tr[1]/td[8]")
end


def weFindTheRandomBeschreibungInTheTableFirstRow(randomBeschreibung)
    return suche(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form-activities']/table/tbody/tr[1]/td[5]/div[contains(text(), '"+randomBeschreibung+"')]")
end


def clickErsteLeistungEditButton()
    return sucheUndKlicken(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_activities']/table/tbody/tr[1]/td[8]/div/a")
end

def leistungBearbeitenPruefen(uniqueDesc)
    return suche(:xpath=>".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_activities']/table/tbody/tr[1]/td[5]/div[contains(text(), '"+uniqueDesc+"')]")
end