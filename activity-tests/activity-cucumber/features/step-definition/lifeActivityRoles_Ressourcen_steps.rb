# encoding: utf-8

Angenommen /^ich betrachte Ressourcen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp = suche(:xpath=>"//a[text()='Benutzerverwaltung']")
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich lege Ressourcen mit folgenden Rollen an$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = suche(:xpath=>"//div[@class='button-div']/input[@id='addUserButton']")
		tmp = auswertung_zwei(tmp1, tmp2)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite Ressourcen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[5]/div/a")
		tmp3 = suche(:xpath=>"//input[@value='Speichern']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich sperre Ressourcen mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[5]/div/a")
		tmp3 = suche(:xpath=>"//div[@class='top-margin']/input[@id='active']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche Ressourcen mit folgenden Rollen$/ do |table|

end

Angenommen /^ich füge Ressourcen zu Positionen mit folgenden Rollen hinzu$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Ressourcenzuordnung']")
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div']/input[@value='Ressource zuweisen']")
		tmp3 = suche(:xpath=>"//div[@class='button-div']/input[@value='Speichern']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich entferne Ressourcen zu Positionen mit folgenden Rollen$/ do |table|

end

Angenommen /^ich teile Ressourcen Rechte mit folgenden Rollen zu$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Ressourcenzuordnung']")
		tmp2 = sucheUndKlicken(:xpath=>"//div[@class='button-div']/input[@value='Ressource zuweisen']")
		tmp3 = suche(:xpath=>"//input[@value='Speichern']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)		
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

