# encoding: utf-8

Angenommen /^ich lege Auftraege mit folgenden Rollen an$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = suche(:xpath=>"//div[@class='button-div']/input[@id='addUserButton']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene Aufträge ohne Leistungen und Rechnungen und Kosten mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		if ! suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
			tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a[1]/span")
		end
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
		tmp4 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp5 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[1]/a")
		tmp6 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[2]/a")
		tmp = auswertung_sechs(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte eigene Aufträge komplett mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	projekt_array=projekte
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Meine aktiven Projekte']")
		if ! suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
			tmp2 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/a[1]/span")
		end
		tmp3 = sucheUndKlicken(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/ul[1]/li[1]/a")
		tmp4 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp5 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[1]/a")
		tmp6 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[2]/a")
		tmp7 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[3]/a")
		tmp8 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[4]/a")
		tmp9 = suche(:xpath=>".//*[@id='_myactiveprojectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[5]/a")
		tmp = auswertung_neun(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite eigene Aufträge mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = betrachteEigeneAuftraege(projekt)
		tmp2 = suche(:xpath=>"//div[@class='button-div-top']/input[@value='Bearbeiten']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche eigene Aufträge mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = betrachteEigeneAuftraege(projekt)
		tmp2 = suche(:xpath=>"//div[@class='button-div-top']/input[@value='Löschen']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte alle Aufträge mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]

		if sucheUndKlicken(:xpath=>"//a[text()='Aufträge']") and
		    selectHundredRowsPerSeite(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/div[3]/select") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Mitarbeiter')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Controller')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Architekt')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Admin')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Bereichsleiter')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Buchhaltung')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Personal')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Vertrieb')]") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Projektleiter')]")

                tmp = true
            else
                tmp = false
        end
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte Aufträge von Mitarbeiter unter Verwaltung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Mitarbeiter')]")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich betrachte Aufträge von Projektleiter unter Verwaltung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]

		if  sucheUndKlicken(:xpath=>"//a[text()='Aufträge']") and
		    selectHundredRowsPerSeite(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/div[3]/select") and
		    suche(:xpath=>".//*[@id='_jobsportlet_WAR_activityportlets_form-jobs']/table/tbody[2]/tr[contains(.,'Job Projektleiter')]")

                tmp = true
            else
                tmp = false
        end
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich bearbeite alle Aufträge mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[7]/div/a")
		tmp3 = suche(:xpath=>"//input[@value='Speichern']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche alle Aufträge mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt_array=projekte	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		projekt = projekt_array[i]
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Aufträge']")
		tmp2 = sucheUndKlicken(:xpath=>"//tr[1]/td[8]/div/a")
		tmp3 = suche(:xpath=>"//input[@value='Löschen']")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end