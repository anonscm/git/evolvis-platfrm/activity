# encoding: utf-8

Angenommen /^ich betrachte die Benutzerverwaltung mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Benutzerverwaltung']")
		tmp2 = suche(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_mainDiv']/h2[contains(text(),'Benutzer')]")
		tmp3 = suche(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_form']/div[2]/label[contains(text(),'nur aktive Benutzer anzeigen')]")
		tmp4 = suche(:xpath=>".//*[@id='_usersportlet_WAR_activityportlets_form-users']/table/thead/tr/th[2]/div/span/a[contains(text(),'Benutzername')]")
		tmp = auswertung_vier(tmp1, tmp2, tmp3, tmp4)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end