# encoding: utf-8

Angenommen /^ich sehe Leistungen eines Projekts mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt=projekte #array	
	for i in 0..anzahl-1
		verbinden
		projekt_tmp=projekt[i]
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Projekte']")
		if rolleOut[i]=="controller" || rolleOut[i]=="bereichsleiter" 
			tmp2 = sucheUndKlicken(:xpath=>"//tbody/tr[td[2]/div[contains(text(),'#{projekt_tmp}')] ]/td[6]/div/a")
		else
			tmp2 = sucheUndKlicken(:xpath=>"//tbody/tr[td[2]/div[contains(text(),'#{projekt_tmp}')] ]/td[5]/div/a")
			puts ("projekt_tmp = " + projekt_tmp)
		end
		tmp3 = sucheUndKlicken(:xpath=>"//tr[1]/td[6]/div/a")
		tmp4 = sucheUndKlicken(:xpath=>".//*[@id='_projectsportlet_WAR_activityportlets_mainDiv']/div[1]/ul/li[3]/a[contains(text(),'Leistungen')]")
		tmp = auswertung_vier(tmp1, tmp2, tmp3, tmp4)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end	
end

Angenommen /^ich sehe die Leistungen eines Bereichs mit folgenden Rollen$/ do |table|

end 

Angenommen /^ich betrachte alle Leistungen mit folgenden Rollen$/ do |table| 
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		tmp2 = suche(:xpath=>".//*[@id='_activitiesportlet_WAR_activityportlets_form-activities']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich lösche alle Leistungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		if rolleOut[i]=="buchhaltung"
			tmp2 = suche(:xpath=>"//tr[1]/td[7]/div/a")
		else
			tmp2 = suche(:xpath=>"//tr[1]/td[8]/div/a")
		end
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich importiere alle Leistungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		tmp2 = suche(:xpath=>"//div[@class='button-div']/input[@value='importieren']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end 

Angenommen /^ich exportiere alle Leistungen mit folgenden Rollen$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		tmp2 = suche(:xpath=>"//div[@class='button-div']/input[@type='submit']")
		tmp = auswertung_zwei(tmp1, tmp2)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end

Angenommen /^ich buche alle Leistungen mit folgenden Rollen um$/ do |table|
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array	
	for i in 0..anzahl-1
		verbinden
		anmeldung(rolleOut[i])
		tmp1 = sucheUndKlicken(:xpath=>"//a[text()='Leistungen']")
		tmp2 = sucheUndKlicken(:xpath=>"//tbody/tr[1]/td[7]/div/a")
		tmp3 = suche(:xpath=>"//form/h2[contains(text(),'Leistung bearbeiten')]")
		tmp = auswertung_drei(tmp1, tmp2, tmp3)
		fehlermeldung+=werteTabZuordnen(tmp, tableOut[i], rolleOut[i])
		loggout
		ausgabe(i, fehlermeldung)
	end
end