# encoding: utf-8
#################################ANMELDUNG#################################

Wenn /^ich mich anmelde$/ do
  step "ich bin auf der Startseite"
  step "ich das Feld für den Benutzernamen mit \"admin\" befülle"
  step "ich das Feld für das Passwort mit \"admin\" befülle"
  step "ich den Submitbutton klicke"  
  step "ich die Seite aktualisiere" #später ist dieser step überflüssig
end

Angenommen /^ich bin auf der Startseite$/ do
    base=ENV['UI_BASE_URL']
    #driver.manage.timeouts.implicit_wait = 3 # seconds
    driver.navigate.to("#{base}")
end

Angenommen /^ich betrachte die URL "(.*?)"$/ do |url|
	base=ENV['UI_BASE_URL']
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.navigate.to("#{base}")
end

Wenn /^ich die Seite aktualisiere$/ do
	#driver.manage.timeouts.implicit_wait = 3 # seconds  
	driver.navigate.refresh
end

Wenn /^ich das Feld für den Benutzernamen mit "(.*?)" befülle$/ do |username|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,"field-username").send_keys "#{username}"
end

Wenn /^ich das Feld für das Passwort mit "(.*?)" befülle$/ do |password|
  	driver.find_element(:id,"field-password").send_keys "#{password}"
end

Und /^ich den Submitbutton klicke$/ do
  	driver.find_element(:id,"login_btn").click
end

Dann /^sollte da stehen "(.*?)"$/ do |text|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[@class='mainMenuCat']/h2[contains(text(),'#{text}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Gesuchtes Element wird nicht gefunden"
	end	  	

end

Angenommen /^ich melde mich an$/ do
	#driver.manage.timeouts.implicit_wait = 3 # seconds  
	step "ich mich anmelde"
end

Angenommen /^ich befinde mich auf der "Meine Leistungen" -Seite$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	eigene_leistungen = driver.find_element(:xpath => "//a[text()='Meine Leistungen']")
  	eigene_leistungen.click
end

Dann /^erhalte ich die Startseite$/ do
  step "sollte da stehen \"Meine Aktivitäten\"" 
end

#################################Szenario: Projekt anlegen#################################
Angenommen /^ich betrachte die Ansicht zum Anlegen von Projekten$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div top-margin']//input[@type='submit']").click	
end

Wenn /^ich folgendes Projekt anlege$/ do |table|
	table.hashes.each do |row|    					
		name=row['Name']
		start=row['Startdatum']
		#Kunde muss existieren		
		kunde = "Marian"
		#Verantwortlicher muss existieren
		verantwortlicher = "activity admin"
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id, "_projectsportlet_WAR_activityportlets_form-projectName").send_keys "#{name}"
		if start == "heute"
			start=DateTime.now.strftime('%Y-%m-%d')		
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id, "_projectsportlet_WAR_activityportlets_form-startDate").send_keys "#{start}"
		#Selectbox Kunde		
		select_kunde = driver.find_element(:id, "_projectsportlet_WAR_activityportlets_form-clientId") 
		dropdown_kunde = Selenium::WebDriver::Support::Select.new(select_kunde)		
		kunde_bef = dropdown_kunde.select_by(:text, "#{kunde}")
		#Selectbox Verantwortlicher 
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_verantwortlicher = driver.find_element(:id, "_projectsportlet_WAR_activityportlets_form-responsibleId") 
		dropdown_verantwortlicher = Selenium::WebDriver::Support::Select.new(select_verantwortlicher)
		driver.manage.timeouts.implicit_wait = 3 # seconds
		verantwortlicher_bef = dropdown_verantwortlicher.select_by(:text, "#{verantwortlicher}")

		driver.find_element(:class, "save").click
	end	
end

Dann /^erscheint unter "Projekt"$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	table.hashes.each do |row|    					
		projekt=row['Projekt']
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => "//tbody[@class='yui-dt-data']//tr//td//div[contains(text(),'#{projekt}')]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Projekt ist nicht vorhanden"
		end			
	end
end

Und /^unter dem zugehörigen Kunden wird das Projekt angezeigt$/ do
	driver.find_element(:xpath => "//a[text()='Kunden']").click
	#Kunde muss der Selbe sein wie in step "wenn ich folgendes projekt anlege"	
	kunde = "Marian"
	#Projekt muss auch bekannt sein
	projekt = "Start"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(), '#{kunde}')]]//td[5]//div//a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[@class='layout-cell']//a[contains(text(),'#{projekt}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Projekt nicht vorhanden"
	end		
end

#################################Szenario: Projekt anlegen#################################
Angenommen /^ich bearbeite das Projekt "(.*?)"$/ do |projekt| 
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(), '#{projekt}')]]//td[5]//div/a").click

end

Wenn /^ich den Namen des Projektes in "(.*?)" ändere$/ do |neu|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	element1 = driver.find_element(:id,"_projectsportlet_WAR_activityportlets_form-projectName")
	driver.find_element(:id,"_projectsportlet_WAR_activityportlets_form-projectName").clear
	driver.find_element(:id,"_projectsportlet_WAR_activityportlets_form-projectName").send_keys "#{neu}"	
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
end

Dann /^erscheint unter "Projekt" das Projekt "(.*?)"$/ do |neu|
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tbody/tr/td/div[contains(text(),'#{neu}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Projekt wurde nicht korrekt geändert"
	end	
end

Und /^unter dem zugehörigen Kunden wird der Projektname "(.*?)" angezeigt$/ do |neu|
	driver.find_element(:xpath => "//a[text()='Kunden']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	kunde = "Marian"
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(), '#{kunde}')]]//td[5]//div//a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[@class='layout-cell']/a[contains(text(),'#{neu}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Projektname wird nicht angezeigt"
	end	
end


#################################Szenario: Projekt löschen#################################
Angenommen /^ich betrachte die Ansicht zur Löschung von Projekten$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'Anders')]]//td[6]//div//a").click
end

Angenommen /^unter dem Projekt sind keinerlei Leistungen verbucht worden$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td/div[contains(text(),'No records found')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Es besteht die Möglichkeit, dass Leistungen verbucht wurden"
	end		
end

Wenn /^ich ein Projekt lösche$/ do
	driver.find_element(:xpath => "//div[@class='button-div-top']/input[@value='Löschen']").click
	a = driver.switch_to.alert
	if a.text == 'OK'
		a.dismiss
	else
		a.accept
	end		
end

Dann /^wird dieses Projekt nicht länger unter "Projekte" angezeigt$/ do
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	elementfound = true
	begin	
		driver.find_element(:xpath => "//tbody[@class='yui-dt-data']/tr/td[2]/div[contains(text(),'Anders')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end
end

Und /^auch unter dem zugehörigen Kunden wird das Projekt nicht länger angezeigt$/ do
	driver.find_element(:xpath => "//a[text()='Kunden']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'Marian')]]//td[5]/div/a").click
	elementfound = true
	begin	
		driver.find_element(:xpath => "//div[@class='layout-table']/div/div/a[contains(text(),'Anders')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	
	
end

#################################Szenario: Auftrag anlegen#################################
Angenommen /^ich betrachte die Ansicht zum Anlegen von Aufträgen$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,"addUserButton").click
end

Wenn /^ich folgenden Auftrag anlege$/ do |table|
	table.hashes.each do |row|    					
		name=row['Name']
		typ=row['Typ']
		status=row['Status']
		#Pflichtfelder
		auftragsnummer = 1
		kostenstelle = 2
		projekt = "Test Projekt"
		verantwortlicher = "activity admin"
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-name").send_keys "#{name}"
		driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-nr").send_keys "#{auftragsnummer}"		
		driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-accounts").send_keys "#{kostenstelle}"
		
		select_projekt = driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-projectId") 
		dropdown_projekt = Selenium::WebDriver::Support::Select.new(select_projekt)		
		projekt_bef = dropdown_projekt.select_by(:text, "#{projekt}") 

		select_verantwortlicher = driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-managerId") 
		dropdown_verantwortlicher = Selenium::WebDriver::Support::Select.new(select_verantwortlicher)		
		verantwortlicher_bef = dropdown_verantwortlicher.select_by(:text, "#{verantwortlicher}")

		select_typ = driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-jobTypeId") 
		dropdown_typ = Selenium::WebDriver::Support::Select.new(select_typ)		
		typ_bef = dropdown_typ.select_by(:text, "#{typ}")

		select_status = driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-jobStatusId") 
		dropdown_status = Selenium::WebDriver::Support::Select.new(select_status)		
		status_bef = dropdown_status.select_by(:text, "#{status}")

		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
	end
end

Dann /^erscheint unter "Aufträge"$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	table.hashes.each do |row|    					
		name=row['Name']
		typ=row['Typ']
		status=row['Status']
		begin	
			driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{name}')] and td[4]/div[contains(text(),'#{status}')] and td[5]/div[contains(text(),'#{typ}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Auftrag erscheint nicht unter Aufträge"
		end		
	end
end

Und /^unter dem zugehörigen Projekt wird der Auftrag angezeigt$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	projekt = "Test Projekt"
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{projekt}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[2]/div[contains(text(),'Auftrag1')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Unter dem zugehörigen Projekt wird der Auftrag nicht angezeigt"
	end	
end

#################################Szenario: Auftrag bearbeiten#################################

Angenommen /^ich bearbeite den Auftrag "(.*?)"$/ do |auftrag|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[6]/div/a").click
end

Wenn /^ich den Namen des Auftrages in "(.*?)" ändere$/ do |neu|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-name").clear
	driver.find_element(:id, "_jobsportlet_WAR_activityportlets_form-name").send_keys "#{neu}"
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click	
end

Dann /^erscheint unter „Aufträge“ der Auftrag "(.*?)"$/ do |neu|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[2]/div[contains(text(),'#{neu}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Geänderter Auftrag erscheint nicht"
	end		
end

Und /^unter dem zugehörigen Projekt wird der Auftragsname "(.*?)" angezeigt$/ do |neu|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	projekt = "Test Projekt"
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{projekt}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[2]/div[contains(text(),'#{neu}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Unter dem zugehörigen Projekt wird der Auftrag nicht angezeigt"
	end		
end

#################################Szenario: Auftrag löschen#################################

Angenommen /^ich betrachte die Ansicht zur Löschung von Aufträgen$/ do
	projekt = "Test Projekt"
	auftrag = "Auftrag2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{projekt}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[7]/div/a").click	
end

Angenommen /^unter dem Auftrag sind keinerlei Leistungen verbucht worden$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//ul[@class='tabs']/li/a[contains(text(),'Leistungen')]").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'No records found')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Es wurden schon Leistungen für dieses Projekt verbucht"
	end		
end

Wenn /^ich einen Auftrag lösche$/ do
	step "ich betrachte die Ansicht zur Löschung von Aufträgen"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div-top']/input[@value='Löschen']").click
	a = driver.switch_to.alert
	if a.text == 'OK'
		a.dismiss
	else
		a.accept
	end		
end

Dann /^wird dieser Auftrag nicht länger unter "Aufträge" angezeigt$/ do
	auftrag = "Auftrag2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[2]/div[contains(text(),'#{auftrag}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	


end

Und /^auch unter dem zugehörigen Projekt wird der Auftrag nicht länger angezeigt$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Projekte']").click
	projekt = "Test"
	auftrag = "Auftrag2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{projekt}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	begin	
		driver.find_element(:xpath => "//tr/td[2]/div[contains(text(),'#{auftrag}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	


end	

#################################Szenario: Leistungen eintragen auf der Startseite#################################

Wenn /^ich folgende Leistungen auf der Startseite eintrage:$/ do |table|

  	#sicherstellen dass ich auf der Startseite bin
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => ".//*[@id='logoarea']/a[1]/img").click	
	table.hashes.each do |row|
	    	auftrag=row['Auftrag auswählen']
	    	position=row['Position auswählen']
	    	stunde=row['Stunde']
	    	datum=row['Datum']

		#SelectBox auswahl Auftrag
		select_auftrag = driver.find_element(:xpath => ".//*[@id='_myactivitiesportlet_WAR_activityportlets_form_add-jobId']")                                                    	
		dropdown_leistung = Selenium::WebDriver::Support::Select.new(select_auftrag)
		leistung = dropdown_leistung.select_by(:text, "#{auftrag}")
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#Selectbox auswahl Position
		select_position = driver.find_element(:xpath => ".//*[@id='_addactivitiesportlet_WAR_activityportlets_form-positionId']") 
		dropdown_position = Selenium::WebDriver::Support::Select.new(select_position)
		position = dropdown_position.select_by(:text, "#{position}")
		#Eintragen in Stunden
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-hours").send_keys "#{stunde}"
		#Eintragen in Datum
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-date").click
		driver.manage.timeouts.implicit_wait = 3 # seconds		
		driver.find_element(:xpath => ".//*[@id='ui-datepicker-div']/div[2]/button[1]").click
		#Eintragen in Beschreibung
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-description").send_keys "blaaaa"
		#Leistungen abschicken (hinzufügen)
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Hinzufügen']").click
  	end
end

Und /^unter \"Meine Stunden im Überblick\" wird der Wert "(.*?)" angezeigt$/ do |stunde|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[contains(@class,'layout-cell cell-fifty')]//span[contains(text(),'#{stunde}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Stunden stimmen nicht überein"
	end	
end

#################################Szenario: Leistungen eintragen auf der Leistungen-Seite#################################

Wenn /^ich folgende Leistungen auf der "Meine Leistungen" Seite eintrage:$/ do |table|

	
	table.hashes.each do |row|
	    	auftrag=row['Auftrag auswählen']
	    	position=row['Position auswählen']
	    	stunde=row['Stunde']
	    	datum=row['Datum']

		#SelectBox auswahl Auftrag
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_auftrag = driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-jobId")                                                    	
		dropdown_auftrag = Selenium::WebDriver::Support::Select.new(select_auftrag)
		auftrag_bef = dropdown_auftrag.select_by(:text, "#{auftrag}")
		#Selectbox auswahl Position
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_position = driver.find_element(:xpath => ".//*[@id='_addactivitiesportlet_WAR_activityportlets_form-positionId']") 
		dropdown_position = Selenium::WebDriver::Support::Select.new(select_position)
		position = dropdown_position.select_by(:text, "#{position}")
		#Eintragen in Stunden
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-hours").send_keys "#{stunde}"
		#Eintragen in Datum
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-date").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => ".//*[@id='ui-datepicker-div']/div[2]/button[1]").click
		#Eintragen in Beschreibung
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_addactivitiesportlet_WAR_activityportlets_form-description").send_keys "blaaaa"
		#Leistungen abschicken (hinzufügen)
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Hinzufügen']").click
  	end
end

Dann /^erscheint unter "Meine Leistungen" die Testdaten$/ do |table|
	 
	step "ich befinde mich auf der \"Meine Leistungen\" -Seite"
	#feature Tabelle auslesen
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	table.hashes.each do |row|
		projekt = "(Test Projekt)"    	
		auftrag=row['Auftrag']
		auftrag1 = projekt+auftrag
	    	position=row['Position auswählen']
	    	stunde=row['Stunde']
	    	datum=row['Datum']
		beschreibung="blaa"
		
	
		#feature Tabelle mit "Deine letzten fünf Leistungen" Portlet vergleichen
		#Elemente aus Portlet Tabelle holen
		
		if datum=="heute"
			datum = DateTime.now.strftime('%m/%d/%Y')		
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => ".//tr[td[2]/div[contains(text(), '#{auftrag1}')] and td[3]/div[contains(text(), '#{position}')] and td[4]/div[contains(text(), '#{beschreibung}')] and td[5]/div[contains(text(), '#{stunde}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Leistungen stimmen nicht überein"
		end		
	end
		
end

#################################Szenario: Leistungen ändern in der "Leistungen bearbeiten"-Ansicht#################################
Angenommen /^ich befinde mich in der "Leistungen bearbeiten"-Ansicht$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	step "ich befinde mich auf der \"Meine Leistungen\" -Seite"
	auftrag = "(Test Projekt)Test Auftrag"
	position = "Test Position"
	beschreibung = "blaaa"
	datum = DateTime.now.strftime('%m/%d/%Y')
	#xpath für button 
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => %Q{.//tr[td[2]/div[contains(text(), '#{auftrag}')] and td[3]/div[contains(text(), '#{position}')] and td[4]/div[contains(text(), '#{beschreibung}')]and td[6]/div[contains(text(), '#{datum}')]]//td[7]/div/a}).click


end

Wenn /^ich die eingetragenen Leistungen folgendermaßen ändere$/ do |table|
	#Daten aus feature Tabelle holen
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	table.hashes.each do |row|
	    	stunde=row['Stunden']
		#Stunde eintragen
		driver.find_element(:id,"_myactivitiesportlet_WAR_activityportlets_form-hours").clear
		driver.find_element(:id,"_myactivitiesportlet_WAR_activityportlets_form-hours").send_keys "#{stunde}"
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click	
	end
end

Dann /^erscheinen unter "Meine Leistungen" die geänderten Daten$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	step "ich befinde mich auf der \"Meine Leistungen\" -Seite"
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	#feature Tabelle auslesen	
	table.hashes.each do |row|
		projekt = "(Test Projekt)"    	
		auftrag=row['Auftrag auswählen']
		auftrag1 = projekt+auftrag
	    	position=row['Position auswählen']
	    	stunde=row['Stunde']
	    	datum=row['Datum']
		beschreibung="blaa"

		#feature Tabelle mit "Meine Leistungen" Portlet vergleichen
		#Elemente aus Portlet Tabelle holen und vergleichen jetzt richtig
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#Abfrage wenn in der feature Tabelle datum==heute dann date=getDate() oder so sonst gegebenes Datum einfügen
		if datum=="heute"
			datum = DateTime.now.strftime('%m/%d/%Y')		
		end		
		begin	
			driver.find_element(:xpath => ".//tr[td[2]/div[contains(text(), '#{auftrag1}')] and td[3]/div[contains(text(), '#{position}')] and td[4]/div[contains(text(), '#{beschreibung}')] and td[5]/div[contains(text(), '#{stunde}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Die geänderten Daten stimmen nicht"
		end		
	end
end

Und /^unter "Meine Stunden im Überblick" wird die geänderte Stundenzahl angezeigt$/ do  
	
	#Refreshen
	step "ich die Seite aktualisiere"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	# var stunde sollte von anderen step übergeben werden
	stunde = 6.0
	#Stunden aus "Meine Stunden im Überblick holen"
	begin	
		driver.find_element(:xpath => "//div//span[contains(text(),'#{stunde}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Stunden stimmen nicht überein"
	end	
end


#################################Szenario: Leistungen löschen#################################

Angenommen /^ich betrachte die Ansicht zum Löschen von Leistungen$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	step "ich befinde mich auf der \"Meine Leistungen\" -Seite"
	
end

Wenn /^ich Löschung der Leistung mit „OK“ bestätige$/ do
	auftrag = "(Test Projekt)Test Auftrag"
	position = "Test Position"	
	datum = DateTime.now.strftime('%m/%d/%Y')
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')] and td[3]/div[contains(text(),'#{position}')] and td[6]/div[contains(text(),'#{datum}')]]/td[8]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	a = driver.switch_to.alert
	if a.text == 'OK'
		a.dismiss
	else
		a.accept
	end		
end

Dann /^ist die gesamte Zeile des ausgewählte Eintrags gelöscht$/ do
	step "ich befinde mich auf der \"Meine Leistungen\" -Seite"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	auftrag = "(Test Projekt)Test Auftrag"
	position = "Test Position"	
	datum = DateTime.now.strftime('%m/%d/%Y')
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')] and td[3]/div[contains(text(),'#{position}')] and td[6]/div[contains(text(),'#{datum}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Leistung wird noch immer noch angezeigt"
	end		
end

Und /^unter „Meine Stunden im Überblick“ wird die geänderte Stundenzahl angezeigt$/ do
	#kann ich nicht testen
end

#################################Szenario: Leistungen als Controller löschen#################################

Angenommen /^ich betrachte die Ansicht zum Erstellen einer Löschanfrage für Leistungen$/ do
	auftrag = "(Test Projekt)Test Auftrag"
	position = "Test Position"
	stunden = 6
	datum = DateTime.now.strftime('%m/%d/%Y')
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Leistungen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')] and td[3]/div[contains(text(),'#{position}')] and td[5]/div[contains(text(),'#{stunden}')] and td[6]/div[contains(text(),'#{datum}')]]/td[8]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
end

Wenn /^ich eine Löschanfrage erstelle$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Senden']").click
end

Dann /^wird unter "Löschanfragen" die neue Löschanfrage angezeigt$/ do
	#LÖSCHANFRAGE KANN NICHT ERSTELLT WERDEN
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Löschanfragen']").click
	user = "admin activity"
	status = "open"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{user}')] and td[6]/div[contains(text(),'#{status}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Die Löschanfrage wird nicht angezeigt"
	end	
end

Und /^unter "To dos" wird dem entsprechenden Mitarbeiter die Löschanfrage angezeigt$/ do
	begin	
		driver.find_element(:xpath => "//div[@class='portlet-body']/div/div[contains(text(),'Es gibt 1 Löschanfrage')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Die Löschanfrage wird nicht angezeigt"
	end	
end

#################################Szenario: Leistungen exportieren#################################

Angenommen /^ich habe mithilfe des Filters zwei Leistungen ausgewählt$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Leistungen allgemein']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	res = "activity admin"
	auftrag = "Fanta"
	
	select_res = driver.find_element(:id, "_allactivitiesportlet_WAR_activityportlets_form-resourceId") 
	dropdown_res = Selenium::WebDriver::Support::Select.new(select_res)		
	res_bef = dropdown_res.select_by(:text, "#{res}") 
	
	select_auftrag = driver.find_element(:id, "_allactivitiesportlet_WAR_activityportlets_form-jobId") 
	dropdown_auftrag = Selenium::WebDriver::Support::Select.new(select_auftrag)		
	auftrag_bef = dropdown_auftrag.select_by(:text, "#{auftrag}") 

	driver.find_element(:xpath => "//div[@class='button-div rightColumn']/input[@value='Als Excel Tabelle exportieren']").click
	
end

Wenn /^ich mich in der Leistungen exportieren Ansicht befinde$/ do
	#Absprache
end

Dann /^speichere ich die Datei im Excel-Format auf meinem Desktop$/ do
	#Absprache
end

#################################Szenario: Abwesenheiten eintragen#################################

Wenn /^ich unter "Urlaub" folgende Daten eintrage$/ do |table|
	#feature Tabelle auslesen	
	table.hashes.each do |row|    	
		typ=row['Typ']
		von=row['Von']
	    	bis=row['Bis']
	    	leiter=row['Projektleiter auswählen']
		
		#Zu Abwesenheit wechseln
		driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#"Abwesenheit eintragen" Button drücken
		driver.find_element(:xpath => "//div[@class='button-div-top']/input[@value='Abwesenheit eintragen'] ").click	
	    	
		#Felder befüllen
		driver.manage.timeouts.implicit_wait = 3 # seconds		
		select_typ = driver.find_element(:xpath => "//div[@class='layout-cell cell-fifty']/select[@name='typeId']") 
		dropdown_typ = Selenium::WebDriver::Support::Select.new(select_typ)
		typ_bef = dropdown_typ.select_by(:text, "#{typ}")
		
		if von == "heute"
			driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-startDate").click
			driver.find_element(:xpath => ".//*[@id='ui-datepicker-div']/div[2]/button[1]").click
		else
			driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-startDate").send_keys "#{von}"	

		end

		if bis == "morgen"
			datum = DateTime.now.strftime('%Y-%m-%d')
			bis = Date.parse(datum)
			bis +=1
			driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-endDate").send_keys "#{bis}"
		else
			driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-endDate").send_keys "#{bis}"
		end	
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_leiter = driver.find_element(:id, "_myholidayportlet_WAR_activityportlets_form-resourceList" ) 
		dropdown_leiter = Selenium::WebDriver::Support::Select.new(select_leiter)
		leiter_bef = dropdown_leiter.select_by(:text, "#{leiter}")
		
		#"speichern" Button drücken
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
		
		
	end
	
end

Dann /^erscheinen in der "Abwesenheitsliste" die Daten$/ do |table|
	table.hashes.each do |row|    			
		von=row['Von']
	    	bis=row['Bis']
		typ=row['Typ']
	    	status=row['Status']
		tage=row['Tage']
		antworten=row['Antworten']
		#Auf abwesenheit navigieren
		driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click
		#vergleichen
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => ".//tr[td[1]/div[contains(text(), '#{von}')] and td[2]/div[contains(text(), '#{bis}')] and td[3]/div[contains(text(), '#{typ}')] and td[5]/div[contains(text(), '#{status}')] and td[6]/div[contains(text(), '#{tage}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{position}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	
end

Und /^auch unter dem zugehörigen Auftrag wird die Position nicht länger angezeigt$/ do 
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	auftrag = "Test Auftrag"
	position = "Position2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[7]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//ul[@class='tabs']/li/a[contains(text(),'Positionen')]").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
			elementfound = true
		end
		if elementfound
			raise "Daten stimmen nicht überein"
		end			
	end
end

Und /^in "Abwesende Mitarbeiter" erscheinen die Daten$/ do |table|
	table.hashes.each do |row|    			
		von=row['Von']
	    	bis=row['Bis']
		typ=row['Typ']
	    	status=row['Status']
		tage=row['Tage']
		antworten=row['Antworten']
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#Auf abwesenheiten allgemein navigieren
		driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
		#vergleichen
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end
		begin	
			driver.find_element(:xpath => ".//tr[td[2]/div[contains(text(), '#{von}')] and td[3]/div[contains(text(), '#{bis}')] and td[4]/div[contains(text(), '#{typ}')] and td[6]/div[contains(text(), '#{status}')] and td[7]/div[contains(text(), '#{tage}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Daten stimmen nicht überein"
		end	
	end	
end

#################################Szenario: Abwesenheit ändern in der "Urlaub"-Ansicht#################################

Wenn /^ich eine eingetragene Abwesenheit folgendermaßen ändere$/ do |table|
	#step "ich eine Abwesenheit eintrage"	
	table.hashes.each do |row|    			
		typ=row['Typ']
		von=row['Von']
	    	bis=row['Bis']
		#Auf abwesenheit navigieren
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#Klickt auf spezifischen bearbeiten button DATUM MUSS ANGEPASST WERDEN!!!
		von_tmp=DateTime.now.strftime('%m/%d/%Y')

		datum_tmp = DateTime.now.strftime('%d-%m-%Y')
		bis_tmp = Date.parse(datum_tmp)
		bis_tmp +=1
		bis_tmp = bis_tmp.strftime('%m/%d/%Y')

		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => ".//tr[td[1]/div[contains(text(), '#{von_tmp}')] and td[2]/div[contains(text(), '#{bis_tmp}')] and td[3]/div[contains(text(), 'Urlaub')] ]//td[8]/div/a ").click
		#Felder füllen
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_typ = driver.find_element(:xpath => "//div[@class='layout-cell cell-fifty']/select[@name='typeId']") 
		dropdown_typ = Selenium::WebDriver::Support::Select.new(select_typ)
		#Anstelle von Abwesendheit sollte typ drinne stehen doch wegen rechtschreibfelher nicht möglich		
		typ_bef = dropdown_typ.select_by(:text, "#{typ}")
		
		if von == "heute"
			von=DateTime.now.strftime('%Y-%m-%d')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%Y-%m-%d')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%Y-%m-%d')				
		end
		
		driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-startDate").clear
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-startDate").send_keys "#{von}"
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-endDate").clear
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_myholidayportlet_WAR_activityportlets_form-endDate").send_keys "#{bis}"
		#Speichern drücken
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		
	end
end

Dann /^erscheinen unter der "Abwesenheitsliste" die Daten$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click
	table.hashes.each do |row|    					
		von=row['Von']
	    	bis=row['Bis']
		typ=row['Typ']
		staus=row['Status']
		tage=row['Tage']
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#sollte typ anstelle von "Abwesendheit" stehen doch rechtschreibfehler
		begin	
			driver.find_element(:xpath => ".//tr[td[1]/div[contains(text(), '#{von}')] and td[2]/div[contains(text(), '#{bis}')] and td[3]/div[contains(text(), '#{typ}')]and td[6]/div[contains(text(), '#{tage}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Daten stimmen nicht überein"
		end	
	end
end

Und /^unter "Abwesende Mitarbeiter" erscheinen die Daten$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	table.hashes.each do |row|    					
		von=row['Von']
	    	bis=row['Bis']
		typ=row['Typ']
		staus=row['Status']
		tage=row['Tage']
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		#Typ muss angepasst werden wegen rechtschreibfehler
		begin	
			driver.find_element(:xpath => ".//tr[td[2]/div[contains(text(), '#{von}')] and td[3]/div[contains(text(), '#{bis}')] and td[4]/div[contains(text(), '#{typ}')]and td[7]/div[contains(text(), '#{tage}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Daten stimmen nicht überein"
		end
	end
end

#################################Szenario: Abwesenheiten genehmigen#################################

Angenommen /^ich betrachte die Ansicht zum genehmigen von Abwesenheiten$/ do
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
		
		
end

Wenn /^ich folgende Abwesenheit genehmige$/ do |table|
	table.hashes.each do |row|    					
		name=row['Name']
		von=row['Von']
		bis=row['Bis']
		tage=row['Tage']
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end		

		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{name}')] and td[2]/div[contains(text(),'#{von}')] and td[3]/div[contains(text(),'#{bis}')] and td[6]/div[contains(text(),'beantragt')]]/td[9]/div/a").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_status = driver.find_element(:xpath => "//div[@class='layout-row']/div/select[@name='statusId']") 
		dropdown_status = Selenium::WebDriver::Support::Select.new(select_status)		
		status_bef = dropdown_status.select_by(:text, "genehmigt") 
	
		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click				
	end	
end

Dann /^erscheint unter "Abwesende Mitarbeiter" folgender Eintrag$/ do |table|
	table.hashes.each do |row|    					
		name=row['Name']
		von=row['Von']
		bis=row['Bis']
		tage=row['Tage']
		status=row['Status']
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{name}')] and td[2]/div[contains(text(),'#{von}')] and td[3]/div[contains(text(),'#{bis}')] and td[7]/div[contains(text(),'#{tage}')] and td[6]/div[contains(text(),'#{status}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Eintrag erscheint nicht unter Abwesende Mitarbeiter"
		end	
	end
end

Und /^unter der "Abwesenheitsliste" von "activity admin" wird der genehmigte Urlaub angezeigt$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	von = DateTime.now.strftime('%m/%d/%Y')
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=1
	bis = bis.strftime('%m/%d/%Y')
	tage = 2
	status = "genehmigt"
	#Zeigt mehr einträge an
	select_rpp = driver.find_element(:xpath => "//select[@title='Rows per page']") 
	dropdown_rpp = Selenium::WebDriver::Support::Select.new(select_rpp)		
	rpp_bef = dropdown_rpp.select_by(:text, "50")
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{von}')] and td[2]/div[contains(text(),'#{bis}')] and td[5]/div[contains(text(),'#{status}')] and td[6]/div[contains(text(),'#{tage}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Genehmigter Urlaub wird nicht angezeigt"
	end				
end

#################################Szenario: Abwesenheiten bearbeiten#################################

Angenommen /^ich betrachte die Controlleransicht zur Bearbeitung von Abwesenheiten$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
end

Wenn /^ich folgende Abwesenheit bearbeite$/ do |table|
	table.hashes.each do |row|	
		name=row['Name']
		von=row['Von']
		bis=row['Bis']
		tage=row['Tage']
		typ = "Urlaub"
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		if bis == "überübermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=3
			bis = bis.strftime('%m/%d/%Y')				
		end
		driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{name}')] and td[2]/div[contains(text(),'#{von}')] and td[3]/div[contains(text(),'#{bis}')] and td[4]/div[contains(text(),'#{typ}')] and td[7]/div[contains(text(),'#{tage}')]]/td[9]/div/a").click		
	end
end

Und /^"Bis" auf "übermorgen" setze$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=2
	bis = bis.strftime('%Y-%m-%d')
	driver.find_element(:id, "_allholidayportlet_WAR_activityportlets_form-endDate").clear
	driver.find_element(:id, "_allholidayportlet_WAR_activityportlets_form-endDate").send_keys "#{bis}"
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
end

Dann /^erscheinen unter der "Abwesende Mitarbeiter" die Daten$/ do |table|
	table.hashes.each do |row|	
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click		
		driver.manage.timeouts.implicit_wait = 3 # seconds		
		name=row['Ressource']
		von=row['Von']
		bis=row['Bis']
		tage=row['Tage']
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		if bis == "überübermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=3
			bis = bis.strftime('%m/%d/%Y')				
		end
		#rpp		
		select_rpp = driver.find_element(:xpath => "//select[@title='Rows per page']") 
		dropdown_rpp = Selenium::WebDriver::Support::Select.new(select_rpp)		
		rpp_bef = dropdown_rpp.select_by(:text, "50")
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{name}')] and td[2]/div[contains(text(),'#{von}')] and td[3]/div[contains(text(),'#{bis}')]  and td[4]/div[contains(text(),'#{tage}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Die Daten erscheinen nicht"
		end	
	end
end

Und /^unter "Abwesenheitsliste" erscheinen die Daten$/ do |table|
	table.hashes.each do |row|	
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Meine Abwesenheit']").click		
		driver.manage.timeouts.implicit_wait = 3 # seconds		
		von=row['Von']
		bis=row['Bis']
		tage=row['Tage']
		if von == "heute"
			von=DateTime.now.strftime('%m/%d/%Y')		
		end
		
		if bis == "morgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=1
			bis = bis.strftime('%m/%d/%Y')				
		end

		if bis == "übermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=2
			bis = bis.strftime('%m/%d/%Y')				
		end
		if bis == "überübermorgen"
			datum = DateTime.now.strftime('%d-%m-%Y')
			bis = Date.parse(datum)
			bis +=3
			bis = bis.strftime('%m/%d/%Y')				
		end
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{von}')] and td[2]/div[contains(text(),'#{bis}')] and td[6]/div[contains(text(),'#{tage}')] ]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Daten erscheinen nicht"
		end	
	end
end

#################################Szenario: Überstunden eintragen#################################

Wenn /^ich unter "Meine Überstunden" folgende Daten eintrage$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Meine Überstunden']").click
	table.hashes.each do |row|    					
		datum=row['Datum']
	    	stunden=row['Stunden']
		projekt = "Test Projekt"
		typ = "Eintragen"
		
		if datum == "heute"
			datum=DateTime.now.strftime('%Y-%m-%d')		
		end
		if datum == "gestern"
			datum_tmp = DateTime.now.strftime('%d-%m-%Y')
			datum = Date.parse(datum_tmp)
			datum -=1
			datum = datum.strftime('%Y-%m-%d')	
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		select_projekt = driver.find_element(:xpath => "//div[@class='layout-table ']/div/select[@name='projectId']") 
		dropdown_projekt = Selenium::WebDriver::Support::Select.new(select_projekt)		
		projekt_bef = dropdown_projekt.select_by(:text, "#{projekt}")
		
		
		driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-date").send_keys "#{datum}"
		driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-hours").send_keys "#{stunden}"
		driver.find_element(:class, "save").click
	end
end	

Dann /^erscheinen unter "Meine Überstunde" die Daten$/ do |table|
	table.hashes.each do |row|    					
		datum=row['Datum']
		stunden=row['Stunden']	
		projekt = "Test Projekt"
		if datum == "gestern"
			datum_tmp = DateTime.now.strftime('%d-%m-%Y')
			datum = Date.parse(datum_tmp)
			datum -=1
			datum = datum.strftime('%m/%d/%Y')	
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds		
		driver.find_element(:xpath => "//a[text()='Meine Überstunden']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		vergleich = driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{projekt}')] and td[3]/div[contains(text(),'#{stunden}')] and td[4]/div[contains(text(),'#{datum}')]]")
	end
end

Und /^unter "meine Stunden im Überblick" wird die geänderte Überstundenzahl angezeigt$/ do
	driver.find_element(:xpath => "//div[@id='logoarea']/a/img").click
	stunden = 2
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[@class='layout-row']/div/span[contains(text(),'#{stunden}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Die geänderte Stundenzahl wird nicht angezeigt"
	end		
end

#################################Szenario: Überstunden abfeiern#################################

Wenn /^ich unter „Überstunden abbauen“ folgende Daten eintrage$/ do |table|
	driver.find_element(:xpath => "//a[text()='Meine Überstunden']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	table.hashes.each do |row|    					
		datum=row['Datum']
	    	stunden=row['Stunden']
		if datum == "heute"
			datum=DateTime.now.strftime('%Y-%m-%d')		
		end
	
		driver.find_element(:id, "reduce").click

		driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-date").send_keys "#{datum}"
		driver.find_element(:id, "_addovertimeportlet_WAR_activityportlets_form-hours").send_keys "#{stunden}"
		driver.find_element(:xpath => "//div[@class='button-div']//input[@value='Speichern']").click
	end	
end

Dann /^erscheinen unter „Meine Überstunden“ die Daten$/ do |table|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Meine Überstunden']").click
	table.hashes.each do |row|    					
		datum=row['Datum']
	    	stunden=row['Stunden']
		projekt = "Test Projekt"
		if datum == "heute"
			datum=DateTime.now.strftime('%m/%d/%Y')		
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{projekt}')] and td[3]/div[contains(text(),'#{stunden}')] and td[4]/div[contains(text(),'#{datum}')]]") 
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Überstunden stimmen nicht"
		end	
	end	
end

Und /^unter „Meine Stunden im Überblick“ wird die geänderte Überstundenzahl angezeigt$/ do
	driver.find_element(:xpath => "//div[@id='logoarea']/a/img").click
	stunden = 1
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//div[@class='layout-row']/div/span[contains(text(),'#{stunden}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Die geänderte Überstundenzahl wird nicht angezeigt"
	end		
end

#################################Szenario: Überstunden als Controller löschen#################################

Angenommen /^ich betrachte die Ansicht zum Erstellen einer Löschanfrage für Überstunden$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Überstunden']").click
end

Wenn /^ich die Löschanfrage erstelle$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	projekt = "Test Projekt"
	stunden = 2
	datum = DateTime.now.strftime('%m/%d/%Y')
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{projekt}')] and td[4]/div[contains(text(),'#{stunden}')] and td[5]/div[contains(text(),'#{datum}')]]/td[6]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	a = driver.switch_to.alert
	if a.text == 'OK'
		a.dismiss
	else
		a.accept
	end		
end

Dann /^wird unter „Löschanfragen“ die neue Löschanfrage angezeigt$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Löschanfragen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	user = "admin activity"
	typ = "overtime"
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{user}')] and td[3]/div[contains(text(),'#{typ}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Löschanfrage wird nicht angezeigt"
	end			
end

#################################Szenario: Abwesenheiten als Controller bearbeiten#################################

Angenommen /^ich betrachte die Ansicht zur Genehmigung von Abwesenheiten$/ do 	
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	#Start und enddatum des vorhandenen und zu löschenden antrages bestimmen
	von = DateTime.now.strftime('%m/%d/%Y')
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=2
	bis = bis.strftime('%m/%d/%Y')
	
	status = "beantragt"
	#den spezifischen bearbeitenbutton drücken
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => %Q{//tr[td[2]/div[contains(text(), '#{von}')] and td[3]/div[contains(text(), '#{bis}')] and td[6]/div[contains(text(), '#{status}')]]//td[9]//div//a}).click	
end


Wenn /^ich den Abwesenheits-Antrag genehmige$/ do
	#step "ich betrachte die Ansicht zur Genehmigung von Abwesenheiten"
	driver.manage.timeouts.implicit_wait = 3 # seconds 	
	select_status = driver.find_element(:xpath => %Q{.//div[@class='layout-cell cell-thirty-three']//select[@name='statusId']})
	driver.manage.timeouts.implicit_wait = 3 # seconds 
	dropdown_status = Selenium::WebDriver::Support::Select.new(select_status)		
	status_bef = dropdown_status.select_by(:text, "genehmigt")
	#speichern drücken
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
end

Dann /^wird der Status unter „Abwesende Mitarbeiter“ und unter der „Abwesenheitsliste“als „genehmigt“ angezeigt$/ do
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	von = DateTime.now.strftime('%m/%d/%Y')
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=2
	bis = bis.strftime('%m/%d/%Y')
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => ".//tr[td[2]/div[contains(text(), '#{von}')] and td[3]/div[contains(text(), '#{bis}')] and td[6]/div[contains(text(), 'genehmigt')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Status wurde nicht verändert"
	end	
end

#################################Szenario: Abwesenheit als Controller löschen#################################

Angenommen /^ich betrachte die Ansicht zum Löschen von Abwesenheiten$/ do
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	von = DateTime.now.strftime('%m/%d/%Y')
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=2
	bis = bis.strftime('%m/%d/%Y')
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(), '#{von}')] and td[3]/div[contains(text(), '#{bis}')]]/td[10]/div/a").click
end

Dann /^erscheint unter „Abwesende Mitarbeiter“ bei der Abwesenheit der Status „Löschanfrage“$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Abwesenheiten']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	von = DateTime.now.strftime('%m/%d/%Y')
	datum = DateTime.now.strftime('%d-%m-%Y')
	bis = Date.parse(datum)
	bis +=2
	bis = bis.strftime('%m/%d/%Y')
	status = "Löschanfrage"
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{von}')] and td[3]/div[contains(text(),'#{bis}')] and td[6]/div[contains(text(),'#{status}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Änderung wurde nicht übernommen"
	end	
end

Und /^in der Benutzeransicht erscheint eine neue Löschanfrage$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Löschanfragen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'admin activity')] and td[3]/div[contains(text(),'loss')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Löschanfrage wird nicht angezeigt"
	end	
end

#################################Szenario: Kunden anlegen#################################

Wenn /^ich einen Kunden neu anlege/ do 
	driver.find_element(:xpath => "//a[text()='Kunden']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div-top']//input[@type='submit']").click
	#Daten des Kunden
	name = "Torben Mustermann"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,'_allclientsportlet_WAR_activityportlets_form-name').send_keys "#{name}"
	driver.find_element(:class,"save").click
end

Dann /^erscheint unter "Kunden" der neue Eintrag$/ do
	name = "Torben Mustermann"
	driver.find_element(:xpath => "//a[text()='Kunden']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => %Q{.//tbody[@class='yui-dt-data']//tr//td//div[contains(text(), '#{name}')]})
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Kunde wurde nicht erstellt"
	end		
	driver.manage.timeouts.implicit_wait = 3 # seconds
end

#################################Szenario: Position anlegen#################################

Angenommen /^ich betrachte die Ansicht zum Anlegen von Positionen$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Positionen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div-top']/input[@value='Neue Position']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
end

Wenn /^ich folgende Position anlege$/ do |table|
	table.hashes.each do |row|	
		name=row['Name']
		status=row['Status']
		auftrag = "Test Auftrag"
		preis = 12
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id,"_allpositionsportlet_WAR_activityportlets_form-positionName").send_keys "#{name}"
	
		select_auftrag = driver.find_element(:id,"_allpositionsportlet_WAR_activityportlets_form-jobId") 
		dropdown_auftrag = Selenium::WebDriver::Support::Select.new(select_auftrag)		
		auftrag_bef = dropdown_auftrag.select_by(:text, "#{auftrag}")
		
		select_status = driver.find_element(:id,"positionStatusId") 
		dropdown_status = Selenium::WebDriver::Support::Select.new(select_status)		
		status_bef = dropdown_status.select_by(:text, "#{status}")

		driver.find_element(:id,"fixedPrice").send_keys "#{preis}"		

		driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Speichern']").click
	end 
end

Dann /^erscheint unter "Positionen"$/ do |table|
	table.hashes.each do |row|
		name=row['Name']
		status=row['Status']
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Positionen']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{name}')] and td[3]/div[contains(text(),'#{status}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Daten erscheinen nicht unter Positionen"
		end		
	end
end 

Und /^unter dem zugehörigen Auftrag wird die Position angezeigt$/ do
	name="Position1"
	status="in Arbeit"
	auftrag = "Test"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[7]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//ul[@class='tabs']/li/a[contains(text(),'Positionen')]").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{name}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Im zugehörigen Auftrag wird die Position nicht angezeigt"
	end	
end

#################################Szenario: Position bearbeiten#################################

Angenommen /^ich bearbeite die Position "(.*?)"$/ do |position|
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Positionen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{position}')]]/td[9]/div/a").click		
end
	
Wenn /^ich den Namen der Position in "(.*?)" ändere$/ do |position|
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,"_allpositionsportlet_WAR_activityportlets_form-positionName").clear
	driver.find_element(:id,"_allpositionsportlet_WAR_activityportlets_form-positionName").send_keys "#{position}"
	driver.find_element(:class,"save").click
end

Dann /^erscheint unter "Positionen" die Position "(.*?)"$/ do |position|
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Positionen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{position}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Geänderte Position wird nicht angezeigt"
	end		
end

Und /^unter dem zugehörigen Auftrag wird der Positionsname "(.*?)" angezeigt$/ do |position|
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	auftrag = "Test Auftrag"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[7]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//ul[@class='tabs']/li/a[contains(text(),'Positionen')]").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{position}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Im zugehörigen Auftrag wird die Position nicht angezeigt"
	end		
end

#################################Szenario: Position löschen#################################

Angenommen /^ich betrachte die Ansicht zur Löschung von Positionen$/ do
	position = "Position2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Positionen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{position}')]]/td[10]/div/a").click	
end

Angenommen /^unter der Position sind keinerlei Leistungen verbucht worden$/ do
	#kann leistungen noch nicht abfragen
end

Wenn /^ich eine Position lösche$/ do
	position = "Position2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//input[@value='Löschen']").click
	a = driver.switch_to.alert
	if a.text == 'OK'
		a.dismiss
	else
		a.accept
	end		
end

Dann /^wird diese Position nicht länger unter "Positionen" angezeigt$/ do
	position = "Position2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Positionen']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{position}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	
end

Und /^auch unter dem zugehörigen Auftrag wird die Position nicht länger angezeigt$/ do 
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Aufträge']").click
	auftrag = "Test Auftrag"
	position = "Position2"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{auftrag}')]]/td[7]/div/a").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//ul[@class='tabs']/li/a[contains(text(),'Positionen')]").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr/td[1]/div[contains(text(),'#{position}')]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = false
	end
	if elementfound
		raise "Projekt wird noch immer noch angezeigt"
	end	
end

#################################Szenario: Ressource zuordnen#################################

Angenommen /^ich betrachte die Ansicht zum Zuordnen von Ressourcen$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Ressourcenzuordnung']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//div[@class='button-div']/input[@value='Ressource zuweisen']").click
end

Wenn /^ich der Position eine Ressource mit dem Status "aktiv" zuordne$/ do
	auftrag = "Test Auftrag"
	position = "Test Position"
	res = "activity admin"
	status = "aktiv"
	driver.manage.timeouts.implicit_wait = 3 # seconds
	#Auftrag auswählen
	select_auftrag = driver.find_element(:id,"_resourceallocationportlet_WAR_activityportlets_form-jobId") 
	dropdown_auftrag = Selenium::WebDriver::Support::Select.new(select_auftrag)		
	auftrag_bef = dropdown_auftrag.select_by(:text, "#{auftrag}")

	#Position auswählen
	select_position = driver.find_element(:id,"_resourceallocationportlet_WAR_activityportlets_form-positionId") 
	dropdown_position = Selenium::WebDriver::Support::Select.new(select_position)		
	position_bef = dropdown_position.select_by(:text, "#{position}")
	
	#Ressource auswählen 
	select_res = driver.find_element(:id,"resourceId") 
	dropdown_res = Selenium::WebDriver::Support::Select.new(select_res)		
	res_bef = dropdown_res.select_by(:text, "#{res}")

	#Status auswählen
	select_status = driver.find_element(:id,"statusId") 
	dropdown_status = Selenium::WebDriver::Support::Select.new(select_status)		
	status_bef = dropdown_status.select_by(:text, "#{status}")

	driver.find_element(:class,"save").click
end

Dann /^erscheint diese Ressource unter "Mitarbeiter" mit dem ausgewählten Status$/ do
	nachname = "admin"
	vorname = "activity"
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Benutzerverwaltung']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,"checkActive").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	begin	
		driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{nachname}')] and td[2]/div[contains(text(),'#{vorname}')]]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Mitarbeiter erscheint mit dem ausgewählten Status"
	end	
end

Und /^auch unter "Ressourcenzuordnung" erscheint die Ressource mit einer Verknüpfung zu dem Projekt, zu dem die Position gehört$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Ressourcenzuordnung']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	nachname = "admin"
	vorname = "activity"
	projekt = "Test Projekt"
	begin	
		driver.find_element(:xpath => "//tr[td[2]/div[contains(text(),'#{vorname}')] and td[3]/div[contains(text(),'#{nachname}')]  and td[5]/div/a[contains(text(),'#{projekt}')]  ]")
	rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
		elementfound = true
	end
	if elementfound
		raise "Ressource erscheint nicht mit der jeweiligen Verknüpfung"
	end	
end

#################################Szenario: Mitarbeiter anlegen#################################

Angenommen /^ich betrachte die Ansicht zum Hinzufügen von Stammdaten$/ do
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//a[text()='Benutzerverwaltung']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id, "addUserButton").click
end 

Wenn /^ich den folgenden Mitarbeiter anlege$/ do |table|
	table.hashes.each do |row|    					
		vorname=row['Vorname']
		nachname=row['Nachname']
		username=row['Benutzername']
		res_typ = "int. Person"
		filiale = "Bonn"
		auftragstyp = "Azubi"
		urlaubstage = 20
		resturlaub = 10
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:id, "_allusersportlet_WAR_activityportlets_form-firstName").send_keys "#{vorname}"
		driver.find_element(:id, "_allusersportlet_WAR_activityportlets_form-lastName").send_keys "#{nachname}"
		driver.find_element(:id, "_allusersportlet_WAR_activityportlets_form-username").send_keys "#{username}"

		select_res = driver.find_element(:id, "resourceTypeId") 
		dropdown_res = Selenium::WebDriver::Support::Select.new(select_res)		
		res_bef = dropdown_res.select_by(:text, "#{res_typ}") 

		select_filiale = driver.find_element(:id, "branchOfficeId") 
		dropdown_filiale = Selenium::WebDriver::Support::Select.new(select_filiale)		
		filiale_bef = dropdown_filiale.select_by(:text, "#{filiale}") 

		select_auftragstyp = driver.find_element(:id, "employmentId") 
		dropdown_auftragstyp = Selenium::WebDriver::Support::Select.new(select_auftragstyp)		
		auftragstyp_bef = dropdown_auftragstyp.select_by(:text, "#{auftragstyp}") 

		driver.find_element(:id, "_allusersportlet_WAR_activityportlets_form-holidays").send_keys "#{urlaubstage}"
		driver.find_element(:id, "_allusersportlet_WAR_activityportlets_form-remainingDays").send_keys "#{resturlaub}"
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:class, "save").click
	end

end

Dann /^erscheint unter "Benutzer"$/ do |table|
	table.hashes.each do |row|    					
		vorname=row['Vorname']
		nachname=row['Nachname']
		driver.manage.timeouts.implicit_wait = 3 # seconds
		driver.find_element(:xpath => "//a[text()='Benutzerverwaltung']").click
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{nachname}')] and td[2]/div[contains(text(),'#{vorname}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Benutzer erscheint nicht"
		end		
	end
end

#################################Szenario: Bearbeiten von Mitarbeitern#################################

Angenommen /^ich bearbeite den Mitarbeiter "Lutz Lurch"$/ do 
	vorname = "Lutz"
	nachname = "Lurch"	
	driver.manage.timeouts.implicit_wait = 3 # seconds	
	driver.find_element(:xpath => "//a[text()='Benutzerverwaltung']").click
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:xpath => "//tr[td[1]/div[contains(text(),'#{nachname}')] and td[2]/div[contains(text(),'#{vorname}')]]/td[5]/div/a").click
end

Wenn /^ich den Nachnamen des Mitarbeiters auf "(.*?)" ändere$/ do |nachname|
	username = "llurch1"	
	driver.manage.timeouts.implicit_wait = 3 # seconds
	driver.find_element(:id,"_allusersportlet_WAR_activityportlets_form-lastName").clear
	driver.find_element(:id,"_allusersportlet_WAR_activityportlets_form-lastName").send_keys "#{nachname}"
	
	driver.find_element(:id,"_allusersportlet_WAR_activityportlets_form-username").clear
	driver.find_element(:id,"_allusersportlet_WAR_activityportlets_form-username").send_keys "#{username}"	

	driver.find_element(:class, "save").click
end

#################################Szenario: muss noch geklärt werden#################################
Dann /^erscheint unter "Deinen letzten fünf Leistungen" die Testdaten$/ do |table|
	
	#Zur Startseite wechseln
	driver.manage.timeouts.implicit_wait = 3 # seconds 	
	driver.find_element(:xpath => ".//*[@id='logoarea']/a/img" ).click
	#feature Tabelle auslesen	
	table.hashes.each do |row|
		projekt = "(Fantestisch)"    	
		auftrag=row['Auftrag']
		auftrag1 = projekt+auftrag
	    	position=row['Position auswählen']
	    	stunde=row['Stunde']
	    	datum=row['Datum']
		beschreibung="blaa"
		
	
		#feature Tabelle mit "Deine letzten fünf Leistungen" Portlet vergleichen
		#Elemente aus Portlet Tabelle holen
	
		if datum=="heute"
			datum = DateTime.now.strftime('%Y-%m-%d')		
		end
		driver.manage.timeouts.implicit_wait = 3 # seconds
		begin	
			driver.find_element(:xpath => ".//tr[td[1]/div[contains(text(), '#{auftrag1}')] and td[2]/div[contains(text(), '#{position}')] and td[4]/div[contains(text(), '#{beschreibung}')] and td[5]/div[contains(text(), '#{stunde}')]]")
		rescue (Selenium::WebDriver::Error::NoSuchElementError)=>e
			elementfound = true
		end
		if elementfound
			raise "Aufträge stimmen nicht überein"
		end		
	end
end

Angenommen /^ich trage Urlaub ein$/ do
	verbinden
	anmeldung("projektleiter")
	tmp = sucheUndKlicken(:xpath=>"//a[text()='Meine Abwesenheit']")
	tmp1 = sucheUndKlicken(:xpath=>"//div[@class='button-div-top']/input[@value='Abwesenheit eintragen']")
	select_urlaub = driver.find_element(:xpath=>"//div[@class='layout-cell cell-fifty']/select[@name='typeId']")
	dropdown_urlaub = Selenium::WebDriver::Support::Select.new(select_urlaub)		
	urlaub_bef = dropdown_urlaub.select_by(:text, "Urlaub")
	
	von = DateTime.now.strftime('%d.%m.%Y')
	von_datum = driver.find_element(:id, "_myholidayportlet_WAR_activityportlets_form-startDate")
	von_datum.send_keys "#{von}"
	
	bis = DateTime.now.strftime('%d.%m.%Y')
	bis_datum = driver.find_element(:id, "_myholidayportlet_WAR_activityportlets_form-endDate")
	bis_datum.send_keys "#{bis}"
	 
	select_proleiter = driver.find_element(:id=>"_myholidayportlet_WAR_activityportlets_form-resourceList") 
	dropdown_proleiter = Selenium::WebDriver::Support::Select.new(select_proleiter)		
	proleiter_bef = dropdown_proleiter.select_by(:text, "admin, activity")
	
	driver.find_element(:class,"save").click
end

Und /^ich dann den Urlaubstag ändere$/ do
	verbinden
	anmelden("projektleiter")
	
end

#################################Szenario: TEST-SZENARIO#################################


Angenommen /^ich teste1$/ do
	base=ENV['UI_BASE_URL']
    	driver.navigate.to("#{base}")
    	testbool=suche(:id=>"hashdhhash")
	if testbool==true
		puts"bool ist true"
	end  
	if testbool==false
		puts"bool ist false"
	end
end

Angenommen /^ich teste2$/ do |table|
	verbinden
	#Alle Rollen im Array speichern	
	rolleOut=alleRollen #array
	#Zähler var für Schleife
	anzahl=alleRollen.length
	fehlermeldung=""
	tableOut=rausAusTabelle(table) #array
	projekt=projekte #array	
	for i in 0..anzahl-1
		driver.find_element()
	end		
end


