#!/bin/bash

unset LANGUAGE
export LC_ALL=C.UTF-8

#set -x
set -e

# goto correct directory
cd "$(dirname "$0")"

# debugging
(wget -O - http://activity-ci.lan.tarent.de:8080/ || :)

# use existing VNC server
export DISPLAY=:1

# run tests
if test -e ~/.rvm/scripts/rvm; then
	. ~/.rvm/scripts/rvm
else
	. /etc/profile.d/rvm.sh
fi
rvm use 1.9.3
file_name=report.$(date "+%Y.%m.%d-%H.%M.%S").html
rm -rf reports
mkdir reports
set +e
startTime=$(date +"%s")
cucumber features --format html --out reports/$file_name
rv=$?
endTime=$(date +"%s")
timeDiff=$(($endTime-$startTime))
echo "######Cucumber Tests took: $(($timeDiff / 60)) minutes and $(($timeDiff % 60)) seconds to complete.######"


# teardown
scp reports/$file_name upload@tpick-dev-storage.lan.tarent.de:~/public_html/activity || exit $?
echo "Done with error code: $rv"
echo "Reported to: http://tpick-dev-storage.lan.tarent.de/~upload/activity/$file_name"
exit $rv
