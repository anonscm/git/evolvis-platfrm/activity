#!/bin/bash
. ~/.rvm/scripts/rvm
rvm use 1.9.3
file_name=report.$(date "+%Y.%m.%d-%H.%M.%S").html
startTime=$(date +"%s")
cucumber features --format html --out $file_name
endTime=$(date +"%s")
timeDiff=$(($endTime-$startTime))
echo "######Cucumber Tests took: $(($timeDiff / 60)) minutes and $(($timeDiff % 60)) seconds to complete.######"
firefox $file_name
