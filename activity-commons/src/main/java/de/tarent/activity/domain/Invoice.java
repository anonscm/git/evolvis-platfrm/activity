/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * @author This class stores all Invoice properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tinvoice", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Invoice.GET_BY_JOB", query = "SELECT i FROM Invoice i WHERE i.job.pk = :jobId"),
		@NamedQuery(name = "Invoice.GET_COUNT_BY_JOB", query = "SELECT count(i.pk) FROM Invoice i WHERE i.job.pk = :jobId"),
		@NamedQuery(name = "Invoice.GET_INVOICES_BY_PROJECT", query = "SELECT i FROM Invoice i WHERE i.project.pk = :projectId"),
		@NamedQuery(name = "Invoice.GET_COUNT_BY_PROJECT", query = "SELECT count(i.pk) FROM Invoice i WHERE i.project.pk = :projectId") 
		})
public class Invoice implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 2614690316241131239L;

	@Id
	@GeneratedValue(generator = "tinvoice_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tinvoice_pk_gen", sequenceName = "activity.tinvoice_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_job")
	private Job job;

	@ManyToOne
	@JoinColumn(name = "fk_project")
	private Project project;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "nr", length = 255)
	private String number;

	@Column(name = "amount", nullable = false, precision = 15, scale = 2)
	private BigDecimal amount;

	@Temporal(TemporalType.DATE)
	@Column(name = "invoiced", length = 29)
	private Date invoiced;

	@Temporal(TemporalType.DATE)
	@Column(name = "payed", length = 29)
	private Date payed;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public Invoice() {
	}

	/**
	 * Constructor with required fields.
	 *
	 * @param pk
	 *            pk
	 * @param job
	 *            job
	 * @param amount
	 *            amount
	 */
	public Invoice(Long pk, Job job, BigDecimal amount) {
		this.pk = pk;
		this.job = job;
		this.amount = amount;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            pk
	 * @param job
	 *            job
	 * @param name
	 *            name
	 * @param number
	 *            number
	 * @param amount
	 *            amount
	 * @param invoiced
	 *            invoiced
	 * @param payed
	 *            payed
	 * @param note
	 *            note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Invoice(Long pk, Job job, String name, String number, BigDecimal amount, Date invoiced, Date payed,
			String note, String crUser, Date crDate, String updUser, Date updDate) {
		this.pk = pk;
		this.job = job;
		this.name = name;
		this.number = number;
		this.amount = amount;
		this.invoiced = invoiced;
		this.payed = payed;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getInvoiced() {
		return invoiced;
	}

	public void setInvoiced(Date invoiced) {
		this.invoiced = invoiced;
	}

	public Date getPayed() {
		return payed;
	}

	public void setPayed(Date payed) {
		this.payed = payed;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
