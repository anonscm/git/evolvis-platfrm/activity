/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Unique Constraint.
 * 
 */
public class UniqueConstraint implements Serializable{

    private static final long serialVersionUID = 5738761228918524083L;

    private Serializable currentEntityId;

    private List<UniqueProperty> properties = new ArrayList<UniqueProperty>();

    /**
     * Add another property to this unique constraint.
     * 
     * @see UniqueProperty
     * @param property
     *            Unique property to add
     * @return this (for method chaining)
     */
    public UniqueConstraint addProperty(UniqueProperty property) {
        this.properties.add(property);

        return this;
    }

    public Serializable getCurrentEntityId() {
        return currentEntityId;
    }

    public List<UniqueProperty> getProperties() {
        return properties;
    }

    public void setCurrentEntityId(Serializable currentEntityId) {
        this.currentEntityId = currentEntityId;
    }

    public void setProperties(List<UniqueProperty> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (Iterator<UniqueProperty> it = properties.iterator(); it.hasNext();) {
            UniqueProperty property = it.next();
            res.append(property.toString());
            if (it.hasNext()) {
                res.append(", ");
            }
        }

        return res.toString();
    }
}
