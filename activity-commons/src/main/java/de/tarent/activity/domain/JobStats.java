/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;

/**
 * @author This class contains aggregate values for the Job entity
 */
public class JobStats implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6260746188507454934L;

	private BigDecimal expectedWork;

	private BigDecimal realWork;

	private BigDecimal percentDone;

	private BigDecimal communicatedWork;
	
	private BigDecimal income;

	private BigDecimal fixedPrice;

	private BigDecimal serviceCosts;

	private BigDecimal otherCosts;

	private BigDecimal invoiceTotal;

	private BigDecimal invoicePaidTotal;

	/**
	 * This is default and null constructor.
	 */
	public JobStats() {
	}

	public BigDecimal getExpectedWork() {
		if (expectedWork == null){
			return BigDecimal.ZERO;
        }
		return expectedWork;
	}

	public void setExpectedWork(BigDecimal expectedWork) {
		this.expectedWork = expectedWork;
	}

	public BigDecimal getRealWork() {
		if (realWork == null){
			return BigDecimal.ZERO;
        }
		return realWork;
	}

	public void setRealWork(BigDecimal realWork) {
		this.realWork = realWork;
	}

	public BigDecimal getPercentDone() {
		if (percentDone == null){
			return BigDecimal.ZERO;
        }
		return percentDone;
	}

	public void setPercentDone(BigDecimal percentDone) {
		this.percentDone = percentDone;
	}

	public BigDecimal getCommunicatedWork() {
		if (communicatedWork == null){
			return BigDecimal.ZERO;
        }
		return communicatedWork;
	}

	public void setCommunicatedWork(BigDecimal communicatedWork) {
		this.communicatedWork = communicatedWork;
	}
	
	public BigDecimal getIncome() {
		if (income == null){
			return BigDecimal.ZERO;
        }
		return income;
	}

	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	public BigDecimal getFixedPrice() {
		if (fixedPrice == null){
			return BigDecimal.ZERO;
        }
		return fixedPrice;
	}

	public void setFixedPrice(BigDecimal fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	public BigDecimal getServiceCosts() {
		if (serviceCosts == null){
			return BigDecimal.ZERO;
        }
		return serviceCosts;
	}

	public void setServiceCosts(BigDecimal serviceCosts) {
		this.serviceCosts = serviceCosts;
	}

	public BigDecimal getOtherCosts() {
		if (otherCosts == null){
			return BigDecimal.ZERO;
        }
		return otherCosts;
	}

	public void setOtherCosts(BigDecimal otherCosts) {
		this.otherCosts = otherCosts;
	}

	public BigDecimal getInvoiceTotal() {
		if (invoiceTotal == null){
			return BigDecimal.ZERO;
        }
		return invoiceTotal;
	}

	public void setInvoiceTotal(BigDecimal invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public BigDecimal getInvoicePaidTotal() {
		if (invoicePaidTotal == null){
			return BigDecimal.ZERO;
        }
		return invoicePaidTotal;
	}

	public void setInvoicePaidTotal(BigDecimal invoicePaidTotal) {
		this.invoicePaidTotal = invoicePaidTotal;
	}

}
