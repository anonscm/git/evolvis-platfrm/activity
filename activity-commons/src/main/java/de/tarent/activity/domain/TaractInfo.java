/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import de.tarent.activity.domain.util.DomainObject;

@Entity
@Table(name = "taract_info", schema = "activity")
@NamedQueries({ @NamedQuery(name = "TaractInfo.GET_ALL", query = "select ti from TaractInfo ti order by ti.pk desc") })
public class TaractInfo implements java.io.Serializable, DomainObject {

	private static final long serialVersionUID = 665243333976575337L;

	@Id
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "dbversion_string", length = 255)
	private String dbversion;

	public TaractInfo() {
	}

	public TaractInfo(Long pk, String dbversion) {
		super();
		this.pk = pk;
		this.dbversion = dbversion;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getDbversion() {
		return dbversion;
	}

	public void setDbversion(String dbversion) {
		this.dbversion = dbversion;
	}

}
