/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import de.tarent.activity.domain.util.DomainObject;

/**
 * Entity to hold action based permissions.
 */
@Entity
@Table(name = "tpermission", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Permission.GET_BY_RESOURCE_AND_ACTION_ID", query = "SELECT DISTINCT perm FROM Permission perm JOIN perm.resources res WHERE res.pk=:resourceId AND perm.actionId=:actionId"),
		@NamedQuery(name = "Permission.GET_BY_ACTION_ID", query = "SELECT perm FROM Permission perm WHERE perm.actionId=:actionId") })
public class Permission implements Serializable, DomainObject {

	private static final long serialVersionUID = -2306553556374865247L;

	@Id
	@GeneratedValue(generator = "tpermission_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tpermission_pk_gen", sequenceName = "activity.tpermission_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "actionid", unique = true, nullable = false, length = 255)
	private String actionId;

	@Column(name = "description", length = 255)
	private String description;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "permissions")
	private Set<Role> roles;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "permissions")
	private Set<Resource> resources;

	public Permission() {
	}

	public Permission(Long pk) {
		this.pk = pk;
	}

	public Permission(String actionId, String description) {
		this.actionId = actionId;
		this.description = description;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public String getActionId() {
		return actionId;
	}

	public String getDescription() {
		return description;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionId == null) ? 0 : actionId.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
        }
		if (obj == null){
			return false;
        }
		if (getClass() != obj.getClass()){
			return false;
        }

		Permission other = (Permission) obj;
		if (actionId == null) {
			if (other.actionId != null){
				return false;
            }
		} else if (!actionId.equals(other.actionId)){
			return false;
        }
		if (description == null) {
			if (other.description != null){
				return false;
            }
		} else if (!description.equals(other.description)){
			return false;
        }
		if (pk == null) {
			if (other.pk != null){
				return false;
            }
		} else if (!pk.equals(other.pk)){
			return false;
        }

		return true;
	}
}
