/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;

public final class LdapUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8733927722999653048L;
	private String username;
	private String firstname;
	private String lastname;

	private String groups;
	private String birth;
	private String mail;
	private long rights;// On default 1;
	private long resourceType;
	private long branchOffice;
	private long employment;
	private long id;

	public LdapUser() {

	}

	public LdapUser(long id, String username) {
		this.setId(id);
		this.setUsername(username);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public long getRights() {
		return rights;
	}

	public void setRights(long rights) {
		this.rights = rights;
	}

	public long getResourceType() {
		return resourceType;
	}

	public void setResourceType(long resourceType) {
		this.resourceType = resourceType;
	}

	public long getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(long branchOffice) {
		this.branchOffice = branchOffice;
	}

	public long getEmployment() {
		return employment;
	}

	public void setEmployment(long employment) {
		this.employment = employment;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}
