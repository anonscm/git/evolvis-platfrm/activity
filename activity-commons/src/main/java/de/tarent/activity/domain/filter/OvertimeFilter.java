/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

import java.util.Date;

/**
 * This class stores properties for Overtime filter.
 * 
 */
public class OvertimeFilter extends BaseFilter {

	private static final long serialVersionUID = 6154952682485918584L;

	private Long projectId;
	private Long resourceId;
	private Long type;
	private Date startDate;
	private Date endDate;

	/**
	 * Constructor.
	 * 
	 * @param prjId - prjId
	 * @param resId - resId
	 * @param type - type
	 * @param startDate - startDate
	 * @param endDate - endDate
	 * @param offset - offset
	 * @param pageItems - pageItems
	 * @param sortColumn - sortColumn
	 * @param sortOrder - sortOrder
	 */
	public OvertimeFilter(Long prjId, Long resId, Long type, Date startDate, Date endDate, Long offset, Long pageItems,
			String sortColumn, String sortOrder) {
		super(offset, pageItems, sortColumn, sortOrder);
		this.projectId = prjId;
		this.resourceId = resId;
		this.type = type;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long prjId) {
		this.projectId = prjId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resId) {
		this.resourceId = resId;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
