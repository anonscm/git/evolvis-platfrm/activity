/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

/**
 * This class stores Project filter properties.
 * 
 */
public class ProjectFilter extends BaseFilter {

	private static final long serialVersionUID = -1957154853390684404L;
	
	private Long status;
	private Long type;
	private Long resourceId;
	private Long posResStatus;
	private Long customerId;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * Constructor.
	 * 
	 * @param status
	 *            - status
	 * @param type
	 *            - type
	 * @param resourceId
	 *            - resourceId
	 * @param posResStatus
	 *            - posResStatus
	 * @param offset
	 *            - offset
	 * @param pageItems
	 *            - pageItems
	 * @param sortColumn
	 *            - sortColumn
	 * @param sortOrder
	 *            - sortOrder
	 */
	public ProjectFilter(Long status, Long type, Long resourceId, Long posResStatus, Long customerId, Long offset,
			Long pageItems, String sortColumn, String sortOrder) {
		super(offset, pageItems, sortColumn, sortOrder);
		this.status = status;
		this.type = type;
		this.resourceId = resourceId;
		this.posResStatus = posResStatus;
		this.customerId = customerId;

	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public Long getPosResStatus() {
		return posResStatus;
	}

	public void setPosResStatus(Long posResStatus) {
		this.posResStatus = posResStatus;
	}
}
