/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all Loss properties.
 */
@Entity
@Audited
@Table(name = "tloss", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Loss.GET_CURRENT_BY_PROJRCT", query = "select distinct los FROM Loss los "
				+ "join los.resourceByFkResource as res join res.posResourceMappings as mapp "
				+ "join mapp.posResourceStatus as mappStatus "
				+ "join mapp.position as pos join pos.job as j join j.project as proj "
				+ "left outer join los.lossType as ltype left outer join los.lossStatusByFkLossStatus as status "
				+ "where proj.pk = :projectId and res.active like 't' and mappStatus.pk = 1 "
				+ " and los.startDate <= :date and los.endDate >= :date "
				+ "and ((ltype.pk=1 AND status.pk=2) OR (ltype.pk=1 AND status.pk=4) OR (ltype.pk=2))"),
		@NamedQuery(name = "Loss.GET_FUTURE_BY_PROJRCT", query = "select distinct los FROM Loss los "
				+ "join los.resourceByFkResource as res join res.posResourceMappings as mapp "
				+ "join mapp.posResourceStatus as mappStatus "
				+ "join mapp.position as pos join pos.job as j join j.project as proj "
				+ "left outer join los.lossType as ltype left outer join los.lossStatusByFkLossStatus as status "
				+ "where proj.pk = :projectId and res.active like 't' and mappStatus.pk = 1 and los.startDate > :date and "
				+ "((ltype.pk=1 AND status.pk=2) OR (ltype.pk=1 AND status.pk=4) OR (ltype.pk=2))"),
		@NamedQuery(name = "Loss.GET_LOSS_DAYS", query = "select sum(los.days) FROM Loss los "
				+ "join los.resourceByFkResource as res "
				+ "left outer join los.lossType as ltype left outer join los.lossStatusByFkLossStatus as status "
				+ "where res.pk =:resId and ltype.pk=1 and status.pk in (2, 4) and los.year = :year ") })
public class Loss implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -2095498092165178498L;

	@Id
	@GeneratedValue(generator = "tloss_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tloss_pk_gen", sequenceName = "activity.tloss_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl5")
	private Resource resourceByFkPl5;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_losstype", nullable = false)
	private LossType lossType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl3_status")
	private LossStatus lossStatusByFkPl3Status;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_lossstatus", nullable = false)
	private LossStatus lossStatusByFkLossStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl4")
	private Resource resourceByFkPl4;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl3")
	private Resource resourceByFkPl3;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resourceByFkResource;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl2")
	private Resource resourceByFkPl2;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl4_status")
	private LossStatus lossStatusByFkPl4Status;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl1")
	private Resource resourceByFkPl1;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl2_status")
	private LossStatus lossStatusByFkPl2Status;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl1_status")
	private LossStatus lossStatusByFkPl1Status;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_pl5_status")
	private LossStatus lossStatusByFkPl5Status;

	@Temporal(TemporalType.DATE)
	@Column(name = "startdate", length = 29)
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "enddate", length = 29)
	private Date endDate;

	@Column(name = "year")
	private Long year;

	@Column(name = "days", precision = 13, scale = 5)
	private BigDecimal days;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public Loss() {
	}

	/**
	 * Constructor with required fields.
	 *
	 * @param pk
	 *            pk
	 * @param lossType
	 *            lossType
	 * @param lossStatusByFkLossStatus
	 *            lossStatusByFkLossStatus
	 * @param resourceByFkResource
	 *            resourceByFkResource
	 */
	public Loss(Long pk, LossType lossType, LossStatus lossStatusByFkLossStatus, Resource resourceByFkResource) {
		this.pk = pk;
		this.lossType = lossType;
		this.lossStatusByFkLossStatus = lossStatusByFkLossStatus;
		this.resourceByFkResource = resourceByFkResource;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            pk
	 * @param resourceByFkPl5
	 *            resourceByFkPl5
	 * @param lossType
	 *            lossType
	 * @param lossStatusByFkPl3Status
	 *            lossStatusByFkPl3Status
	 * @param lossStatusByFkLossStatus
	 *            lossStatusByFkLossStatus
	 * @param resourceByFkPl4
	 *            resourceByFkPl4
	 * @param resourceByFkPl3
	 *            resourceByFkPl3
	 * @param resourceByFkResource
	 *            resourceByFkResource
	 * @param resourceByFkPl2
	 *            resourceByFkPl2
	 * @param lossStatusByFkPl4Status
	 *            lossStatusByFkPl4Status
	 * @param resourceByFkPl1
	 *            resourceByFkPl1
	 * @param lossStatusByFkPl2Status
	 *            lossStatusByFkPl2Status
	 * @param lossStatusByFkPl1Status
	 *            lossStatusByFkPl1Status
	 * @param lossStatusByFkPl5Status
	 *            lossStatusByFkPl5Status
	 * @param startDate
	 *            startDate
	 * @param endDate
	 *            endDate
	 * @param year
	 *            year
	 * @param days
	 *            days
	 * @param note
	 *            note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Loss(Long pk, Resource resourceByFkPl5, LossType lossType, LossStatus lossStatusByFkPl3Status,
			LossStatus lossStatusByFkLossStatus, Resource resourceByFkPl4, Resource resourceByFkPl3,
			Resource resourceByFkResource, Resource resourceByFkPl2, LossStatus lossStatusByFkPl4Status,
			Resource resourceByFkPl1, LossStatus lossStatusByFkPl2Status, LossStatus lossStatusByFkPl1Status,
			LossStatus lossStatusByFkPl5Status, Date startDate, Date endDate, Long year, BigDecimal days, String note,
			String crUser, Date crDate, String updUser, Date updDate) {
		this.pk = pk;
		this.lossType = lossType;
		this.resourceByFkResource = resourceByFkResource;
		this.resourceByFkPl1 = resourceByFkPl1;
		this.resourceByFkPl2 = resourceByFkPl2;
		this.resourceByFkPl3 = resourceByFkPl3;
		this.resourceByFkPl4 = resourceByFkPl4;
		this.resourceByFkPl5 = resourceByFkPl5;
		this.lossStatusByFkLossStatus = lossStatusByFkLossStatus;
		this.lossStatusByFkPl1Status = lossStatusByFkPl1Status;
		this.lossStatusByFkPl2Status = lossStatusByFkPl2Status;
		this.lossStatusByFkPl3Status = lossStatusByFkPl3Status;
		this.lossStatusByFkPl4Status = lossStatusByFkPl4Status;
		this.lossStatusByFkPl5Status = lossStatusByFkPl5Status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.year = year;
		this.days = days;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Resource getResourceByFkPl5() {
		return resourceByFkPl5;
	}

	public void setResourceByFkPl5(Resource resourceByFkPl5) {
		this.resourceByFkPl5 = resourceByFkPl5;
	}

	public LossType getLossType() {
		return lossType;
	}

	public void setLossType(LossType lossType) {
		this.lossType = lossType;
	}

	public LossStatus getLossStatusByFkPl3Status() {
		return lossStatusByFkPl3Status;
	}

	public void setLossStatusByFkPl3Status(LossStatus lossStatusByFkPl3Status) {
		this.lossStatusByFkPl3Status = lossStatusByFkPl3Status;
	}

	public LossStatus getLossStatusByFkLossStatus() {
		return lossStatusByFkLossStatus;
	}

	public void setLossStatusByFkLossStatus(LossStatus lossStatusByFkLossStatus) {
		this.lossStatusByFkLossStatus = lossStatusByFkLossStatus;
	}

	public Resource getResourceByFkPl4() {
		return resourceByFkPl4;
	}

	public void setResourceByFkPl4(Resource resourceByFkPl4) {
		this.resourceByFkPl4 = resourceByFkPl4;
	}

	public Resource getResourceByFkPl3() {
		return resourceByFkPl3;
	}

	public void setResourceByFkPl3(Resource resourceByFkPl3) {
		this.resourceByFkPl3 = resourceByFkPl3;
	}

	public Resource getResourceByFkResource() {
		return resourceByFkResource;
	}

	public void setResourceByFkResource(Resource resourceByFkResource) {
		this.resourceByFkResource = resourceByFkResource;
	}

	public Resource getResourceByFkPl2() {
		return resourceByFkPl2;
	}

	public void setResourceByFkPl2(Resource resourceByFkPl2) {
		this.resourceByFkPl2 = resourceByFkPl2;
	}

	public LossStatus getLossStatusByFkPl4Status() {
		return lossStatusByFkPl4Status;
	}

	public void setLossStatusByFkPl4Status(LossStatus lossStatusByFkPl4Status) {
		this.lossStatusByFkPl4Status = lossStatusByFkPl4Status;
	}

	public Resource getResourceByFkPl1() {
		return resourceByFkPl1;
	}

	public void setResourceByFkPl1(Resource resourceByFkPl1) {
		this.resourceByFkPl1 = resourceByFkPl1;
	}

	public LossStatus getLossStatusByFkPl2Status() {
		return lossStatusByFkPl2Status;
	}

	public void setLossStatusByFkPl2Status(LossStatus lossStatusByFkPl2Status) {
		this.lossStatusByFkPl2Status = lossStatusByFkPl2Status;
	}

	public LossStatus getLossStatusByFkPl1Status() {
		return lossStatusByFkPl1Status;
	}

	public void setLossStatusByFkPl1Status(LossStatus lossStatusByFkPl1Status) {
		this.lossStatusByFkPl1Status = lossStatusByFkPl1Status;
	}

	public LossStatus getLossStatusByFkPl5Status() {
		return lossStatusByFkPl5Status;
	}

	public void setLossStatusByFkPl5Status(LossStatus lossStatusByFkPl5Status) {
		this.lossStatusByFkPl5Status = lossStatusByFkPl5Status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public BigDecimal getDays() {
		return days;
	}

	public void setDays(BigDecimal days) {
		this.days = days;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

}
