/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

import java.io.Serializable;

/**
 * This class stores common properties for all other filters.
 * 
 */
public class BaseFilter implements Serializable {

	private static final long serialVersionUID = 922262215404696126L;

	public static final int VIEW_ALL = 1;
	public static final int VIEW_OWN = 2;
	public static final int VIEW_BY_DIVISION = 3;
	public static final int VIEW_BY_MANAGER = 4;
	
	//default settings
	private Long offset = -1l;
	private Long pageItems = 10l;
	private String sortColumn = null;
	private String sortOrder = null;
	
	private int viewBy;
	
	public BaseFilter(){
		
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param offset
	 *            offset index
	 * @param pageItems
	 *            number of rows per page
	 * @param sortColumn
	 *            column to sort
	 * @param sortOrder
	 *            sort order
	 */
	public BaseFilter(Long offset, Long pageItems, String sortColumn, String sortOrder) {
		this.offset = offset;
		this.pageItems = pageItems;
		this.sortColumn = sortColumn;
		this.sortOrder = sortOrder;
	}
	
	public Long getOffset() {
		return offset;
	}

	public void setOffset(Long offset) {
		this.offset = offset;
	}

	public Long getPageItems() {
		return pageItems;
	}

	public void setPageItems(Long pageItems) {
		this.pageItems = pageItems;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	

    public int getViewBy() {
        return viewBy;
    }

    public void setViewBy(int viewBy) {
        this.viewBy = viewBy;
    }

}
