/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all Position properties.
 */
@Entity
@Audited
@Table(name = "tposition", schema = "activity")
@NamedQueries({
	@NamedQuery(name = "Position.SUM_TOTAL_HOURS_SPENT", query = "SELECT SUM(a.hours) FROM Position p JOIN p.activities a WHERE p.pk = :positionId")
})
public class Position implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -7818847360556471345L;

	@Id
	@GeneratedValue(generator = "tposition_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tposition_pk_gen", sequenceName = "activity.tposition_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_positionstatus", nullable = false)
	private PositionStatus positionStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_job", nullable = false)
	private Job job;

	@Column(name = "name", length = 255)
	@AuditCompareField
	private String name;

	@Column(name = "fixedprice", precision = 10, scale = 2)
	@AuditCompareField
	private BigDecimal fixedPrice;

	@Column(name = "hourlyrate", precision = 10, scale = 2)
	@AuditCompareField
	private BigDecimal hourlyRate;

	@Column(name = "costs", precision = 10)
	@AuditCompareField
	private BigDecimal cost;

	@Column(name = "income", precision = 10, scale = 2)
	@AuditCompareField
	private BigDecimal income;

	@Column(name = "note", length = 4000)
	@AuditCompareField
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Column(name = "expectedwork", precision = 13, scale = 5)
	@AuditCompareField
	private BigDecimal expectedWork;

	@Column(name = "communicatedwork", precision = 13, scale = 5)
	@AuditCompareField
	private BigDecimal communicatedWork;

	@Temporal(TemporalType.DATE)
	@Column(name = "positionstartdate", length = 29)
	@AuditCompareField
	private Date positionStartDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "positionenddate", length = 29)
	@AuditCompareField
	private Date positionEndDate;

	@Column(name = "evolvisurl", length = 255)
	@AuditCompareField
	private String evolvisurl;

	@Column(name = "evolvisprojektid")
	@AuditCompareField
	private Long evolvisprojektid;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "position")
	private List<Timer> timers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "position")
	private List<PosResourceMapping> posResourceMappings;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "position")
	private List<Activity> activities;

	/**
	 * This is default and null constructor.
	 */
	public Position() {
	}

	/**
	 * Constructor with primary key field.
	 *
	 * @param pk
	 *            Position primary key
	 */
	public Position(Long pk) {
		this.pk = pk;
	}

	/**
	 * Constructor with required fields.
	 *
	 * @param pk
	 *            Position primary key
	 * @param positionStatus
	 *            PositionStatus
	 * @param job
	 *            Job
	 */
	public Position(Long pk, PositionStatus positionStatus, Job job) {
		this.pk = pk;
		this.positionStatus = positionStatus;
		this.job = job;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            pk
	 * @param positionStatus
	 *            positionStatus
	 * @param tjob
	 *            tjob
	 * @param name
	 *            name
	 * @param fixedPrice
	 *            fixedPrice
	 * @param hourlyRate
	 *            hourlyRate
	 * @param cost
	 *            cost
	 * @param income
	 *            income
	 * @param note
	 *            note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param expectedwork
	 *            expectedwork
	 * @param realwork
	 *            realwork
	 * @param communicatedwork
	 *            communicatedwork
	 * @param positionstartdate
	 *            positionstartdate
	 * @param positionenddate
	 *            positionenddate
	 * @param percentdone
	 *            percentdone
	 * @param evolvisurl
	 *            evolvisurl
	 * @param evolvisprojektid
	 *            evolvisprojektid
	 * @param timers
	 *            timers
	 * @param posResourceMappings
	 *            posResourceMappings
	 * @param activities
	 *            activities
	 */
	public Position(Long pk, PositionStatus positionStatus, Job tjob, String name, BigDecimal fixedPrice,
			BigDecimal hourlyRate, BigDecimal cost, BigDecimal income, String note, String crUser, Date crDate,
			String updUser, Date updDate, BigDecimal expectedwork, BigDecimal realwork, BigDecimal communicatedwork,
			Date positionstartdate, Date positionenddate, BigDecimal percentdone, String evolvisurl,
			Long evolvisprojektid, List<Timer> timers, List<PosResourceMapping> posResourceMappings,
			List<Activity> activities) {
		this.pk = pk;
		this.positionStatus = positionStatus;
		this.job = tjob;
		this.name = name;
		this.fixedPrice = fixedPrice;
		this.hourlyRate = hourlyRate;
		this.cost = cost;
		this.income = income;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.expectedWork = expectedwork;
		this.communicatedWork = communicatedwork;
		this.positionStartDate = positionstartdate;
		this.positionEndDate = positionenddate;
		this.evolvisurl = evolvisurl;
		this.evolvisprojektid = evolvisprojektid;
		this.timers = timers;
		this.posResourceMappings = posResourceMappings;
		this.activities = activities;
	}

	/**
	 * Constructor: Used to duplicate a position (in a duplicated job).
	 *
	 * @param p
	 *            p
	 */
	public Position(Position p) {
		this.cost = BigDecimal.ZERO;
		this.evolvisprojektid = p.evolvisprojektid;
		this.evolvisurl = p.evolvisurl;
		this.fixedPrice = BigDecimal.ZERO;
		this.hourlyRate = BigDecimal.ZERO;
		this.income = BigDecimal.ZERO;
		this.name = p.name;
		this.note = p.note;
		this.positionEndDate = p.positionEndDate;
		this.positionStartDate = p.positionStartDate;
		this.positionStatus = p.positionStatus;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public PositionStatus getPositionStatus() {
		return positionStatus;
	}

	public void setPositionStatus(PositionStatus positionStatus) {
		this.positionStatus = positionStatus;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getFixedPrice() {
		return fixedPrice;
	}

	public void setFixedPrice(BigDecimal fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	public BigDecimal getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(BigDecimal hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public BigDecimal getIncome() {
		return income;
	}

	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public BigDecimal getExpectedWork() {
		return expectedWork;
	}

	public void setExpectedWork(BigDecimal expectedWork) {
		this.expectedWork = expectedWork;
	}

	public BigDecimal getCommunicatedWork() {
		return communicatedWork;
	}

	public void setCommunicatedWork(BigDecimal communicatedWork) {
		this.communicatedWork = communicatedWork;
	}

	public Date getPositionStartDate() {
		return positionStartDate;
	}

	public void setPositionStartDate(Date positionStartDate) {
		this.positionStartDate = positionStartDate;
	}

	public Date getPositionEndDate() {
		return positionEndDate;
	}

	public void setPositionEndDate(Date positionEndDate) {
		this.positionEndDate = positionEndDate;
	}

	public String getEvolvisurl() {
		return evolvisurl;
	}

	public void setEvolvisurl(String evolvisurl) {
		this.evolvisurl = evolvisurl;
	}

	public Long getEvolvisprojektid() {
		return evolvisprojektid;
	}

	public void setEvolvisprojektid(Long evolvisprojektid) {
		this.evolvisprojektid = evolvisprojektid;
	}

	public List<Timer> getTimers() {
		return timers;
	}

	public void setTimers(List<Timer> timers) {
		this.timers = timers;
	}

	public List<PosResourceMapping> getPosResourceMappings() {
		return posResourceMappings;
	}

	public void setPosResourceMappings(List<PosResourceMapping> posResourceMappings) {
		this.posResourceMappings = posResourceMappings;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
}
