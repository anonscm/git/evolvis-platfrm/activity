/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;

/**
 * Information of a change operation between two {@link Entity}s.
 * 
 * @author Hendrik Helwich
 */
public class ChangeInfo implements java.io.Serializable {

	/**
	 * Date at which the change was done.
	 */
	public final Date changeDate;

	/**
	 * User which initiated the change
	 */
	public final String changeUser;
	public final List<ChangeField> changeFields = new ArrayList<ChangeInfo.ChangeField>();

	public ChangeInfo(Date changeDate, String changeUser) {
		this.changeDate = changeDate;
		this.changeUser = changeUser;
	}

	/**
	 * Add information of a field which was adapted in the change.
	 * 
	 * @param fieldName
	 *            name of the {@link Entity} field which was changed
	 * @param oldValue
	 *            Value of the field before the change
	 * @param newValue
	 *            Value of the field after the change
	 */
	public void addChangeField(String fieldName, Object oldValue, Object newValue) {
		changeFields.add(new ChangeField(fieldName, oldValue, newValue));
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public String getChangeUser() {
		return changeUser;
	}

	public List<ChangeField> getChangeFields() {
		return changeFields;
	}

	public int getChangeFieldSize() {
		return changeFields.size();
	}

	@Override
	public String toString() {
		return "ChangeInfo [changeDate=" + changeDate + ", changeUser=" + changeUser + ", changeFields=" + changeFields
				+ "]";
	}

	public static class ChangeField implements java.io.Serializable {

		public final String fieldName;
		public final Object oldValue;
		public final Object newValue;

		public ChangeField(String fieldName, Object oldValue, Object newValue) {
			this.fieldName = fieldName;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		public String getFieldName() {
			return fieldName;
		}

		public Object getOldValue() {
			return oldValue;
		}

		public Object getNewValue() {
			return newValue;
		}

		@Override
		public String toString() {
			return "ChangeField [fieldName=" + fieldName + ", oldValue=" + oldValue + ", newValue=" + newValue + "]";
		}

	}

}
