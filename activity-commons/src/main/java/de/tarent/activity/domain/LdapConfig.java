/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;

public final class LdapConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4519817408434266337L;
	private String ldapUrl;
	private String ldapBaseDn;
	private boolean ldapEnabled;
	private String userFilter;
	private String roleFilter;
	private String linkFilter;
	private String userGroupCn;
	private String groupDn;

	public LdapConfig() {

	}

	public String getLdapBaseDn() {
		return this.ldapBaseDn;
	}

	public void setLdapBaseDn(String ldapBaseDn) {
		this.ldapBaseDn = ldapBaseDn;
	}

	public boolean isLdapEnabled() {
		return this.ldapEnabled;
	}

	public void setLdapEnabled(boolean ldapEnabled) {
		this.ldapEnabled = ldapEnabled;
	}

	public String getUserFilter() {
		return userFilter;
	}

	public void setUserFilter(String userFilter) {
		this.userFilter = userFilter;
	}

	public String getRoleFilter() {
		return roleFilter;
	}

	public void setRoleFilter(String roleFilter) {
		this.roleFilter = roleFilter;
	}

	public String getLinkFilter() {
		return linkFilter;
	}

	public void setLinkFilter(String linkFilter) {
		this.linkFilter = linkFilter;
	}

	public String getUserGroupCn() {
		return userGroupCn;
	}

	public void setUserGroupCn(String userGroupCn) {
		this.userGroupCn = userGroupCn;
	}

	public String getGroupDn() {
		return groupDn;
	}

	public void setGroupDn(String groupDn) {
		this.groupDn = groupDn;
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}
}
