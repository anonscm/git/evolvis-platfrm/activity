/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueConstraint;
import de.tarent.activity.domain.util.UniqueProperty;
import de.tarent.activity.domain.util.UniqueValidationAware;

/**
 * This class stores Activity properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tactivity", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Activity.GET_BY_POSITION", query = "select a from Activity a where a.position.pk = :positionId"),
		@NamedQuery(name = "Activity.GET_COUNT_BY_POSITION", query = "select count(a.pk) from Activity a where a.position.pk = :positionId"),
		@NamedQuery(name = "Activity.GET_COUNT_BY_JOB", query = "select count(a.pk) from Activity a join a.position p join p.job j where j.pk = :jobId"),
		@NamedQuery(name = "Activity.GET_COUNT_BY_PROJECT", query = "select count(a.pk) from Activity a JOIN a.position p JOIN p.job j JOIN j.project pr where pr.pk = :projectId") })
public class Activity implements java.io.Serializable, DomainObject, Auditable, UniqueValidationAware {

	private static final long serialVersionUID = -5285928874990823675L;

	@Id
	@GeneratedValue(generator = "tactivity_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tactivity_pk_gen", sequenceName = "activity.tactivity_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_position", nullable = false)
	private Position position;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@Column(name = "name", length = 255)
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "date", length = 29)
	private Date date;

	@Column(name = "hours", precision = 13, scale = 5)
	private BigDecimal hours;

	@Column(name = "costperhour", precision = 10, scale = 2)
	private BigDecimal costPerHour;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Column(name = "evolvis_task_id")
	private Long evolvisTaskId;

	/**
	 * This is default and null constructor.
	 */
	public Activity() {
	}

	/**
	 * This is constructor with required fields.
	 *
	 * @param pk
	 *            primary key
	 * @param position
	 *            position foreign key
	 * @param resource
	 *            resource foreign key
	 *
	 */
	public Activity(Long pk, Position position, Resource resource) {
		this.pk = pk;
		this.position = position;
		this.resource = resource;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            primary key
	 * @param position
	 *            position foreign key
	 * @param resource
	 *            resource foreign key
	 * @param name
	 *            activity name
	 * @param date
	 *            activity date
	 * @param hours
	 *            activity hours
	 * @param costPerHour
	 *            activity cost per hour
	 * @param note
	 *            activity note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param evolvisTaskId
	 *            evolvisTaskId
	 *
	 */
	public Activity(Long pk, Position position, Resource resource, String name, Date date, BigDecimal hours,
			BigDecimal costPerHour, String note, String crUser, Date crDate, String updUser, Date updDate,
			Long evolvisTaskId) {
		this.pk = pk;
		this.position = position;
		this.resource = resource;
		this.name = name;
		this.date = date;
		this.hours = hours;
		this.costPerHour = costPerHour;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.evolvisTaskId = evolvisTaskId;
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getHours() {
		return hours;
	}

	public void setHours(BigDecimal hours) {
		this.hours = hours;
	}

	public BigDecimal getCostPerHour() {
		return costPerHour;
	}

	public void setCostPerHour(BigDecimal costPerHour) {
		this.costPerHour = costPerHour;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCrUser() {
		return crUser;
	}

	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public Long getEvolvisTaskId() {
		return evolvisTaskId;
	}

	public void setEvolvisTaskId(Long evolvisTaskId) {
		this.evolvisTaskId = evolvisTaskId;
	}

	@Override
	public List<UniqueConstraint> getUniqueConstraints() {
		List<UniqueConstraint> constraints = new ArrayList<UniqueConstraint>();

		UniqueConstraint constraint = new UniqueConstraint();
		constraint.setCurrentEntityId(this.pk);
		constraint.addProperty(new UniqueProperty("resource.id", this.resource.getPk()))
				.addProperty(new UniqueProperty("position.id", this.position.getPk()))
				.addProperty(new UniqueProperty("name", this.name)).addProperty(new UniqueProperty("date", this.date))
				.addProperty(new UniqueProperty("hours", this.hours));

		if (this.evolvisTaskId != null) {
			constraint.addProperty(new UniqueProperty("evolvisTaskId", this.evolvisTaskId));
		}

		constraints.add(constraint);

		return constraints;
	}
}
