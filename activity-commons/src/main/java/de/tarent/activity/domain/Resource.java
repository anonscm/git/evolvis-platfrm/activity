/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueConstraint;
import de.tarent.activity.domain.util.UniqueProperty;
import de.tarent.activity.domain.util.UniqueValidationAware;

/**
 * This class stores all Resource properties.
 *
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tresource", schema = "activity", uniqueConstraints = @javax.persistence.UniqueConstraint(columnNames = "username"))
@NamedQueries({
		@NamedQuery(name = "Resource.GET_ALL", query = "SELECT res FROM Resource res order by res.lastname, res.firstname"),
		@NamedQuery(name = "Resource.GET_RESOURCE_BY_USERNAME", query = "SELECT res FROM Resource res where res.username=:username"),
		@NamedQuery(name = "Resource.GET_RESOURCE_BY_PROJECT", query = "SELECT distinct res FROM Resource res "
				+ "join res.posResourceMappings mapp join mapp.posResourceStatus mappStatus "
				+ "join mapp.position pos join pos.job jo join jo.project proj "
				+ "where proj.pk = :projectId and res.active like 't' and mappStatus.pk = 1 "),
		@NamedQuery(name = "Resource.GET_ACTIVE_RESOURCES", query = "SELECT res FROM Resource res WHERE res.active like 't' order by res.lastname, res.firstname"),
		@NamedQuery(name = "Resource.GET_RESOURCE_BY_POSITION", query = "SELECT res FROM Resource res join res.posResourceMappings "
				+ "mapp where mapp.position.pk = :positionId"),
		@NamedQuery(name = "Resource.GET_RESOURCE_BY_PERMISSION", query = "SELECT r FROM Resource r join r.permissions p where p.actionId =:actionId"),
		@NamedQuery(name = "Resource.GET_PROJECT_MANAGER", query = "SELECT distinct res FROM Resource res "
				+ "where exists(select proj from Project proj where proj.fkResource = res.pk) order by res.lastname, res.firstname") })
public class Resource implements java.io.Serializable, DomainObject, Auditable, UniqueValidationAware,
		Comparable<Resource> {

	private static final long serialVersionUID = -4739237277592150535L;

	@Id
	@GeneratedValue(generator = "tresource_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tresource_pk_gen", sequenceName = "activity.tresource_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resourcetype", nullable = false)
	private ResourceType resourceType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_employment")
	private Employment employment;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_branchoffice")
	private BranchOffice branchOffice;

	@Column(name = "firstname", length = 255)
	private String firstname;

	@Column(name = "lastname", length = 255)
	private String lastname;

	@Temporal(TemporalType.DATE)
	@Column(name = "birth", length = 29)
	private Date birth;

	@Temporal(TemporalType.DATE)
	@Column(name = "entered", length = 29)
	private Date entered;

	@Temporal(TemporalType.DATE)
	@Column(name = "exit", length = 29)
	private Date exit;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "username", unique = true, length = 10)
	private String username;

	@Column(name = "passwd", length = 32)
	private String password;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Column(name = "mail", length = 255)
	private String mail;

	@Column(name = "salery", precision = 10, scale = 2)
	private BigDecimal salery;

	@Column(name = "costperhour", precision = 10, scale = 2)
	private BigDecimal costPerHour;

	@Column(name = "fk_tcid")
	private Long fkTcid;

	@Column(name = "available_hours")
	private Long availableHours;

	// here can be an enumeration
	@Column(name = "active", length = 1)
	private Character active;

	// maybe a BigDecimal 3.5 days
	@Column(name = "remain_holiday", columnDefinition = "numeric(5, 2) default 0.00")
	private BigDecimal remainHoliday;

	@Column(name = "holiday", columnDefinition = "numeric(5, 2)")
	private BigDecimal holiday;

	@Column(name = "groups", length = 4000)
	private String groups;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkPl1")
	private List<Loss> lossesForFkPl1;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private List<Timer> timers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private List<Skills> skills;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkPl4")
	private List<Loss> lossesForFkPl4;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkPl5")
	private List<Loss> lossesForFkPl5;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkPl2")
	private List<Loss> lossesForFkPl2;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkPl3")
	private List<Loss> lossesForFkPl3;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceByFkResource")
	private List<Loss> lossesForFkResource;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private List<Activity> activities;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private List<PosResourceMapping> posResourceMappings;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
	private List<Cost> costs;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "resource")
	private List<ResourceFTypeMapping> resourceFTypeMappings;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "resources")
	private Set<Role> roles;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(schema = "activity", name = "tresources_tpermissions")
	private Set<Permission> permissions;

	/**
	 * Default and null constructor.
	 */
	public Resource() {
	}

	/**
	 * Constructor using primary key field.
	 *
	 * @param pk
	 *            Resource primary key
	 */
	public Resource(Long pk) {
		this.pk = pk;
	}

	/**
	 * Constructor using required fields.
	 *
	 * @param pk
	 *            Resource primary key
	 * @param resourceType
	 *            ResourceType
	 */
	public Resource(Long pk, ResourceType resourceType) {
		this.pk = pk;
		this.resourceType = resourceType;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	public Employment getEmployment() {
		return employment;
	}

	public void setEmployment(Employment employment) {
		this.employment = employment;
	}

	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(BranchOffice branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Date getEntered() {
		return entered;
	}

	public void setEntered(Date entered) {
		this.entered = entered;
	}

	public Date getExit() {
		return exit;
	}

	public void setExit(Date exit) {
		this.exit = exit;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public BigDecimal getSalery() {
		return salery;
	}

	public void setSalery(BigDecimal salery) {
		this.salery = salery;
	}

	public BigDecimal getCostPerHour() {
		return costPerHour;
	}

	public void setCostPerHour(BigDecimal costPerHour) {
		this.costPerHour = costPerHour;
	}

	public Long getFkTcid() {
		return fkTcid;
	}

	public void setFkTcid(Long fkTcid) {
		this.fkTcid = fkTcid;
	}

	public Long getAvailableHours() {
		return availableHours;
	}

	public void setAvailableHours(Long availableHours) {
		this.availableHours = availableHours;
	}

	public Character getActive() {
		return active;
	}

	public void setActive(Character active) {
		this.active = active;
	}

	public BigDecimal getRemainHoliday() {
		return remainHoliday;
	}

	public void setRemainHoliday(BigDecimal remainHoliday) {
		this.remainHoliday = remainHoliday;
	}

	public BigDecimal getHoliday() {
		return holiday;
	}

	public void setHoliday(BigDecimal holiday) {
		this.holiday = holiday;
	}

	public List<Loss> getLossesForFkPl1() {
		return lossesForFkPl1;
	}

	public void setLossesForFkPl1(List<Loss> lossesForFkPl1) {
		this.lossesForFkPl1 = lossesForFkPl1;
	}

	public List<Timer> getTimers() {
		return timers;
	}

	public void setTimers(List<Timer> timers) {
		this.timers = timers;
	}

	public List<Skills> getSkills() {
		return skills;
	}

	public void setSkills(List<Skills> skills) {
		this.skills = skills;
	}

	public List<Loss> getLossesForFkPl4() {
		return lossesForFkPl4;
	}

	public void setLossesForFkPl4(List<Loss> lossesForFkPl4) {
		this.lossesForFkPl4 = lossesForFkPl4;
	}

	public List<Loss> getLossesForFkPl5() {
		return lossesForFkPl5;
	}

	public void setLossesForFkPl5(List<Loss> lossesForFkPl5) {
		this.lossesForFkPl5 = lossesForFkPl5;
	}

	public List<Loss> getLossesForFkPl2() {
		return lossesForFkPl2;
	}

	public void setLossesForFkPl2(List<Loss> lossesForFkPl2) {
		this.lossesForFkPl2 = lossesForFkPl2;
	}

	public List<Loss> getLossesForFkPl3() {
		return lossesForFkPl3;
	}

	public void setLossesForFkPl3(List<Loss> lossesForFkPl3) {
		this.lossesForFkPl3 = lossesForFkPl3;
	}

	// public List<Overtime> getOvertimesForDecidedBy() {
	// return overtimesForDecidedBy;
	// }
	//
	// public void setOvertimesForDecidedBy(List<Overtime> overtimesForDecidedBy) {
	// this.overtimesForDecidedBy = overtimesForDecidedBy;
	// }

	public List<Loss> getLossesForFkResource() {
		return lossesForFkResource;
	}

	public void setLossesForFkResource(List<Loss> lossesForFkResource) {
		this.lossesForFkResource = lossesForFkResource;
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	public List<PosResourceMapping> getPosResourceMappings() {
		return posResourceMappings;
	}

	public void setPosResourceMappings(List<PosResourceMapping> posResourceMappings) {
		this.posResourceMappings = posResourceMappings;
	}

	public List<Cost> getCosts() {
		return costs;
	}

	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}

	public List<ResourceFTypeMapping> getResourceFTypeMappings() {
		return resourceFTypeMappings;
	}

	public void setResourceFTypeMappings(List<ResourceFTypeMapping> resourceFTypeMappings) {
		this.resourceFTypeMappings = resourceFTypeMappings;
	}

	public String getName() {
		return firstname + " " + lastname;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	@Override
	public List<UniqueConstraint> getUniqueConstraints() {
		List<UniqueConstraint> constraints = new ArrayList<UniqueConstraint>();

		UniqueConstraint constraint = new UniqueConstraint();
		constraint.setCurrentEntityId(this.pk);
		constraint.addProperty(new UniqueProperty("username", this.username));

		constraints.add(constraint);

		return constraints;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permisssions) {
		this.permissions = permisssions;
	}

	@Override
	public int compareTo(Resource arg) {
		int result = 0;

		Collator collator = Collator.getInstance(java.util.Locale.GERMAN);
		collator.setStrength(Collator.SECONDARY);

		result = collator.compare(this.username, arg.username);

		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birth == null) ? 0 : birth.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
        }
		if (obj == null){
			return false;
        }
		if (getClass() != obj.getClass()){
			return false;
        }

		Resource other = (Resource) obj;
		if (birth == null) {
			if (other.birth != null){
				return false;
            }
		} else if (!birth.equals(other.birth)){
			return false;
        }
		if (firstname == null) {
			if (other.firstname != null){
				return false;
            }
		} else if (!firstname.equals(other.firstname)){
			return false;
        }
		if (lastname == null) {
			if (other.lastname != null){
				return false;
            }
		} else if (!lastname.equals(other.lastname)){
			return false;
        }
		if (pk == null) {
			if (other.pk != null){
				return false;
            }
		} else if (!pk.equals(other.pk)){
			return false;
        }
		if (username == null) {
			if (other.username != null){
				return false;
            }
		} else if (!username.equals(other.username)){
			return false;
        }

		return true;
	}

}
