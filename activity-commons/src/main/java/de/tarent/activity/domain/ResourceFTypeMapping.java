/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores ResourceFTypeMapping properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tres_ftype_mapping", schema = "activity")
@NamedQueries({ @NamedQuery(name = "ResourceFTypeMapping.GET_BY_RESOURCE", query = "select mapp from ResourceFTypeMapping mapp where mapp.resource.pk = :resourceId") })
public class ResourceFTypeMapping implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 3503539484854834152L;

	@Id
	@GeneratedValue(generator = "tres_ftype_mapping_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tres_ftype_mapping_pk_gen", sequenceName = "activity.tres_ftype_mapping_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_functiontype", nullable = false)
	private FunctionType functionType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public ResourceFTypeMapping() {
	}

	/**
	 * Constructor using required fields.
	 * 
	 * @param pk
	 *            pk
	 * @param functionType
	 *            functionType
	 * @param resource
	 *            resource
	 */
	public ResourceFTypeMapping(Long pk, FunctionType functionType, Resource resource) {
		this.pk = pk;
		this.functionType = functionType;
		this.resource = resource;
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param pk
	 *            pk
	 * @param functionType
	 *            functionType
	 * @param resource
	 *            resource
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public ResourceFTypeMapping(Long pk, FunctionType functionType, Resource resource, String crUser, Date crDate,
			String updUser, Date updDate) {
		this.pk = pk;
		this.functionType = functionType;
		this.resource = resource;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public FunctionType getFunctionType() {
		return functionType;
	}

	public void setFunctionType(FunctionType functionType) {
		this.functionType = functionType;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
