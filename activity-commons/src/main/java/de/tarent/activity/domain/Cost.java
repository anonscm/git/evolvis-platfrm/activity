/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores Cost properties.
 *
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tcost", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Cost.GET_BY_JOB", query = "SELECT c FROM Cost c inner join c.job j where j.pk = :jobId"),
		@NamedQuery(name = "Cost.GET_COUNT_BY_JOB", query = "SELECT count(c.pk) FROM Cost c join c.job j where j.pk = :jobId") })
public class Cost implements Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 3495091957811180374L;

	@Id
	@GeneratedValue(generator = "tcost_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tcost_pk_gen", sequenceName = "activity.tcost_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_costtype", nullable = false)
	private CostType costType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_job", nullable = false)
	private Job job;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource")
	private Resource resource;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "cost", nullable = false, precision = 10, scale = 2)
	private BigDecimal cost;

	@Temporal(TemporalType.DATE)
	@Column(name = "date", length = 29)
	private Date date;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public Cost() {
	}

	/**
	 * This is constructor with required fields.
	 *
	 * @param pk
	 *            pk
	 * @param costType
	 *            costType
	 * @param job
	 *            job
	 * @param cost
	 *            cost
	 */
	public Cost(Long pk, CostType costType, Job job, BigDecimal cost) {
		this.pk = pk;
		this.costType = costType;
		this.job = job;
		this.cost = cost;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            - primary key
	 * @param tcosttype
	 *            - CostType foreign key
	 * @param job
	 *            - Job foreign key
	 * @param resource
	 *            - Resource foreign key
	 * @param name
	 *            - cost name
	 * @param cost
	 *            - cost value
	 * @param date
	 *            - cost date
	 * @param note
	 *            - cost note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Cost(Long pk, CostType tcosttype, Job job, Resource resource, String name, BigDecimal cost, Date date,
			String note, String crUser, Date crDate, String updUser, Date updDate) {
		this.pk = pk;
		this.costType = tcosttype;
		this.job = job;
		this.resource = resource;
		this.name = name;
		this.cost = cost;
		this.date = date;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public CostType getCostType() {
		return costType;
	}

	public void setCostType(CostType costType) {
		this.costType = costType;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
