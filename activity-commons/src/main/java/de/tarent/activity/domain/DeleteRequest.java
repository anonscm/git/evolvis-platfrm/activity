/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all DeleteRequest properties.
 * 
 */
@Entity
@Table(name = "tdelete_request", schema = "activity")
@NamedQueries({ @NamedQuery(name = "DeleteRequest.GET_BY_TYPE_AND_ID", query = "select req from DeleteRequest req where "
		+ "req.status = 'open' and req.pk = :entityId and req.type = :entityType") })
public class DeleteRequest implements Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -4529678133115377316L;

	@Id
	@GeneratedValue(generator = "tdelete_request_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tdelete_request_pk_gen", sequenceName = "activity.tdelete_request_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_controller", nullable = false)
	private Resource controller;

	@Column(name = "note")
	private String description;

	/**
	 * Values for status: open, deleted, cancelled .
	 */
	@Column(name = "status")
	private String status;

	@Column(name = "cr_user")
	private String crUser;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date")
	private Date crDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date")
	private Date updDate;

	/**
	 * The type of the delete request. Values for type : Activity, Overtime , Loss.
	 */
	@Column(name = "fk_type")
	private String type;

	/**
	 * The id of the deleted object.
	 */
	@Column(name = "fk_id")
	private Long id;

	/**
	 * Constructor
	 */
	public DeleteRequest() {

	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param resource
	 *            resource
	 * @param controller
	 *            controller
	 * @param description
	 *            description
	 * @param status
	 *            status
	 * @param type
	 *            type
	 */
	public DeleteRequest(Long pk, Resource resource, Resource controller, String description, String status, String type) {
		this.pk = pk;
		this.resource = resource;
		this.controller = controller;
		this.description = description;
		this.status = status;
		this.type = type;
	}

	/**
	 * @param pk
	 *            pk
	 * @param resource
	 *            resource
	 * @param controller
	 *            controller
	 * @param description
	 *            description
	 * @param status
	 *            status
	 * @param type
	 *            type
	 * @param crUser
	 *            crUser
	 * @param updUser
	 *            updUser
	 * @param crDate
	 *            crDate
	 * @param updDate
	 *            updDate
	 */
	public DeleteRequest(Long pk, Resource resource, Resource controller, String description, String status,
			String type, String crUser, String updUser, Date crDate, Date updDate) {
		this(pk, resource, controller, description, status, type);
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	/**
	 * Create delete request from given overtime.
	 * 
	 * @param overtime
	 *            overtime
	 */
	public DeleteRequest(Overtime overtime) {
		this.resource = overtime.getResource();
		this.status = "open";
		this.type = "overtime";
		this.id = overtime.getPk();
	}

	/**
	 * Create delete request from give activity.
	 * 
	 * @param activity
	 *            activity
	 */
	public DeleteRequest(Activity activity) {
		this.resource = activity.getResource();
		this.status = "open";
		this.type = "activity";
		this.id = activity.getPk();
	}

	/**
	 * Create delete request from give loss.
	 * 
	 * @param loss
	 *            loss
	 */
	public DeleteRequest(Loss loss) {
		this.resource = loss.getResourceByFkResource();
		this.status = "open";
		this.type = "loss";
		this.id = loss.getPk();
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Resource getController() {
		return controller;
	}

	public void setController(Resource controller) {
		this.controller = controller;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
