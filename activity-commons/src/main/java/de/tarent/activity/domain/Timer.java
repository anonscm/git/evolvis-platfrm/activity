/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * Timer entity.
 * 
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "ttimer", schema = "activity", uniqueConstraints = @UniqueConstraint(columnNames = "fk_resource"))
@NamedQueries({ @NamedQuery(name = "Timer.GET_BY_RESOURCE", query = "select t from Timer t join t.resource res join t.position pos where res.pk = :resId") })
public class Timer implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -3582173981692903301L;

	@Id
	@GeneratedValue(generator = "ttimer_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ttimer_pk_gen", sequenceName = "activity.ttimer_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_position")
	private Position position;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", unique = true, nullable = false)
	private Resource resource;

	@Temporal(TemporalType.DATE)
	@Column(name = "time", length = 29)
	private Date time;

	@Temporal(TemporalType.DATE)
	@Column(name = "time_diff", length = 29)
	private Date timeDiff;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * Constructor.
	 */
	public Timer() {
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param resource
	 *            resource
	 */
	public Timer(Long pk, Resource resource) {
		this.pk = pk;
		this.resource = resource;
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param position
	 *            position
	 * @param resource
	 *            resource
	 * @param time
	 *            time
	 * @param timeDiff
	 *            timeDiff
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Timer(Long pk, Position position, Resource resource, Date time, Date timeDiff, String crUser, Date crDate,
			String updUser, Date updDate) {
		this.pk = pk;
		this.position = position;
		this.resource = resource;
		this.time = time;
		this.timeDiff = timeDiff;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getTimeDiff() {
		return timeDiff;
	}

	public void setTimeDiff(Date timeDiff) {
		this.timeDiff = timeDiff;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
