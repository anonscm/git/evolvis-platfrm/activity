/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 *
 * This class stores all Overtime properties.
 */
@Entity
@Audited
@Table(name = "tovertime", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Overtime.GET_BY_PK", query = "select distinct o from Overtime as o "
				+ "left join fetch o.project join fetch o.resource where o.pk=:overtimeId"),
		@NamedQuery(name = "Overtime.GET_SUM_HOURS", query = "select sum(o.hours) from Overtime as o "
				+ "join o.resource as res where res.pk=:resId"),
		@NamedQuery(name = "Overtime.GET_COUNT_BY_PROJECT", query = "SELECT count(o) FROM Overtime as o where o.project.pk=:projectId") })
public class Overtime implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -3650786264938342606L;

	@Id
	@GeneratedValue(generator = "tovertime_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tovertime_pk_gen", sequenceName = "activity.tovertime_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_overtimestatus", nullable = false)
	private OvertimeStatus overtimeStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "fk_project")
	private Project project;

	@Temporal(TemporalType.DATE)
	@Column(name = "date", length = 29)
	private Date date;

	@Column(name = "hours", precision = 13, scale = 5)
	private BigDecimal hours;

	@Column(name = "year")
	private Long year;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public Overtime() {
	}

	/**
	 * Constructor with required fields.
	 *
	 * @param pk
	 *            Overtime primary key
	 * @param overtimeStatus
	 *            OvertimeStatus
	 * @param resource
	 *            Resource
	 */
	public Overtime(Long pk, OvertimeStatus overtimeStatus, Resource resource) {
		this.pk = pk;
		this.overtimeStatus = overtimeStatus;
		this.resource = resource;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            Overtime primary key
	 * @param resourceByDecidedBy
	 *            resource
	 * @param overtimeStatus
	 *            OvertimeStatus
	 * @param resource
	 *            Resource
	 * @param project
	 *            Project
	 * @param date
	 *            Overtime date
	 * @param hours
	 *            Overtime hours
	 * @param year
	 *            Overtime year
	 * @param note
	 *            Overtime note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Overtime(Long pk, OvertimeStatus overtimeStatus, Resource resource, Project project, Date date,
			BigDecimal hours, Long year, String note, String crUser, Date crDate, String updUser, Date updDate) {
		this.pk = pk;
		this.overtimeStatus = overtimeStatus;
		this.resource = resource;
		this.project = project;
		this.date = date;
		this.hours = hours;
		this.year = year;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public OvertimeStatus getOvertimeStatus() {
		return overtimeStatus;
	}

	public void setOvertimeStatus(OvertimeStatus overtimeStatus) {
		this.overtimeStatus = overtimeStatus;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resourceByFkResource) {
		this.resource = resourceByFkResource;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getHours() {
		return hours;
	}

	public void setHours(BigDecimal hours) {
		this.hours = hours;
	}

	public Long getYear() {
		return year;
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
