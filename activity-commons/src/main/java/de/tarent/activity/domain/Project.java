/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueConstraint;
import de.tarent.activity.domain.util.UniqueProperty;
import de.tarent.activity.domain.util.UniqueValidationAware;

/**
 * @author This class stores all Project fields.
 */
@Entity
@Audited
@Table(name = "tproject", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Project.GET_ALL", query = "SELECT proj FROM Project proj order by proj.name"),
		@NamedQuery(name = "Project.GET_BY_RESOURCE", query = "SELECT distinct proj FROM Project proj inner join proj.jobs as jo "
				+ "inner join jo.positions as pos inner join pos.posResourceMappings as prm join prm.posResourceStatus as mappStatus "
				+ "where prm.resource.pk=:resId and mappStatus.pk = 1 order by proj.name"),
		@NamedQuery(name = "Project.GET_ALL_ACTIVE", query = "SELECT distinct proj FROM Project proj inner join proj.jobs as jo "
				+ "inner join jo.positions as pos inner join pos.posResourceMappings as prm join prm.posResourceStatus as mappStatus "
				+ "where mappStatus.pk = 1 order by proj.name"),
		@NamedQuery(name = "Project.GET_BY_RESPONSIBLE_RESOURCE", query = "SELECT proj FROM Project proj where proj.fkResource=:resId order by proj.name "),
		@NamedQuery(name = "Project.GET_BY_CUSTOMER", query = "SELECT proj from Project proj where proj.customer.pk = :customerId order by proj.name") })
public class Project implements java.io.Serializable, DomainObject, Auditable, UniqueValidationAware {

	private static final long serialVersionUID = 1960818723624605555L;

	@Id
	@GeneratedValue(generator = "tproject_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tproject_pk_gen", sequenceName = "activity.tproject_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_customer", nullable = false)
	private Customer customer;

	@Column(name = "name", length = 255)
	@AuditCompareField
	private String name;

	@Column(name = "note", length = 4000)
	@AuditCompareField
	private String note;

	@Column(name = "dayrate", precision = 10, scale = 2)
	@AuditCompareField
	private BigDecimal dayRate;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_target", length = 29)
	private Date paymentTarget;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Column(name = "wiki", length = 255)
	@AuditCompareField
	private String wiki;

	@Column(name = "test_server", length = 255)
	@AuditCompareField
	private String testServer;

	@Column(name = "sprint_backlog", length = 255)
	@AuditCompareField
	private String sprintBacklog;

	@Column(name = "product_backlog", length = 255)
	@AuditCompareField
	private String productBacklog;

	@Column(name = "specification", length = 255)
	@AuditCompareField
	private String specification;

	@Column(name = "source_control", length = 255)
	@AuditCompareField
	private String sourceControl;

	@Column(name = "protocolls", length = 255)
	@AuditCompareField
	private String protocols;

	@Column(name = "project_site", length = 255)
	@AuditCompareField
	private String projectSite;

	@Column(name = "project_blog", length = 255)
	@AuditCompareField
	private String projectBlog;

	@Column(name = "project_plan", length = 255)
	@AuditCompareField
	private String projectPlan;

	@Column(name = "mailinglists", length = 255)
	@AuditCompareField
	private String mailingLists;

	@Column(name = "issue_tracking", length = 255)
	@AuditCompareField
	private String issueTracking;

	@Column(name = "dms", length = 255)
	@AuditCompareField
	private String dms;

	@Column(name = "crm", length = 255)
	@AuditCompareField
	private String crm;

	@Column(name = "sonar", length = 255)
	@AuditCompareField
	private String sonar;

	@Column(name = "riskAnalysis", length = 255)
	@AuditCompareField
	private String riskAnalysis;

	@Column(name = "offer", length = 255)
	@AuditCompareField
	private String offer;

	@Column(name = "contact_person", length = 255)
	@AuditCompareField
	private String contactPerson;

	@Column(name = "page_auftrag", length = 255)
	@AuditCompareField
	private String pageAuftrag;

	@Column(name = "test_protokoll", length = 255)
	@AuditCompareField
	private String testProtokoll;

	@Column(name = "process_wiki", length = 255)
	@AuditCompareField
	private String processWiki;

	// i think here is missing a foreign key
	@Column(name = "fk_resource")
	@AuditCompareField
	private Long fkResource;

	@Column(name = "accounts")
	@AuditCompareField
	private Long accounts;

	@Temporal(TemporalType.DATE)
	@Column(name = "enddate", length = 13)
	private Date endDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "startdate", length = 13)
	private Date startDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<Overtime> overtimes;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<Job> jobs;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<ProjectFieldsMapping> projectFieldsMappings;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	private List<ProjectSites> projectSites;

	/**
	 * This is default and null constructor.
	 */
	public Project() {
	}

	/**
	 * Constructor using primary key field.
	 *
	 * @param pk
	 *            Project primary key
	 */
	public Project(Long pk) {
		this.pk = pk;
	}

	/**
	 * Constructor using all fields.
	 *
	 * @param pk
	 *            pk
	 * @param customer
	 *            customer
	 * @param name
	 *            name
	 * @param note
	 *            note
	 * @param dayRate
	 *            dayRate
	 * @param paymentTarget
	 *            paymentTarget
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param wiki
	 *            wiki
	 * @param testServer
	 *            testServer
	 * @param sprintBacklog
	 *            sprintBacklog
	 * @param productBacklog
	 *            productBacklog
	 * @param specification
	 *            specification
	 * @param sourceControl
	 *            sourceControl
	 * @param protocols
	 *            protocols
	 * @param projectSite
	 *            projectSite
	 * @param projectBlog
	 *            projectBlog
	 * @param projectPlan
	 *            projectPlan
	 * @param mailingLists
	 *            mailingLists
	 * @param issueTracking
	 *            issueTracking
	 * @param dms
	 *            dms
	 * @param offer
	 *            offer
	 * @param contactPerson
	 *            contactPerson
	 * @param pageAuftrag
	 *            pageAuftrag
	 * @param testProtokoll
	 *            testProtokoll
	 * @param processWiki
	 *            processWiki
	 * @param fkResource
	 *            fkResource
	 * @param accounts
	 *            accounts
	 * @param enddate
	 *            enddate
	 * @param startdate
	 *            startdate
	 * @param overtimes
	 *            overtimes
	 * @param jobs
	 *            jobs
	 * @param projectFieldsMappings
	 *            projectFieldsMappings
	 * @param projectSites
	 *            projectSites
	 */
	public Project(Long pk, Customer customer, String name, String note, BigDecimal dayRate, Date paymentTarget,
			String crUser, Date crDate, String updUser, Date updDate, String wiki, String testServer,
			String sprintBacklog, String productBacklog, String specification, String sourceControl, String protocols,
			String projectSite, String projectBlog, String projectPlan, String mailingLists, String issueTracking,
			String dms, String offer, String contactPerson, String pageAuftrag, String testProtokoll,
			String processWiki, Long fkResource, Long accounts, Date enddate, Date startdate, List<Overtime> overtimes,
			List<Job> jobs, List<ProjectFieldsMapping> projectFieldsMappings, List<ProjectSites> projectSites) {
		this.pk = pk;
		this.customer = customer;
		this.name = name;
		this.note = note;
		this.dayRate = dayRate;
		this.paymentTarget = paymentTarget;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.wiki = wiki;
		this.testServer = testServer;
		this.sprintBacklog = sprintBacklog;
		this.productBacklog = productBacklog;
		this.specification = specification;
		this.sourceControl = sourceControl;
		this.protocols = protocols;
		this.projectSite = projectSite;
		this.projectBlog = projectBlog;
		this.projectPlan = projectPlan;
		this.mailingLists = mailingLists;
		this.issueTracking = issueTracking;
		this.dms = dms;
		this.offer = offer;
		this.contactPerson = contactPerson;
		this.pageAuftrag = pageAuftrag;
		this.testProtokoll = testProtokoll;
		this.processWiki = processWiki;
		this.fkResource = fkResource;
		this.accounts = accounts;
		this.endDate = enddate;
		this.startDate = startdate;
		this.overtimes = overtimes;
		this.jobs = jobs;
		this.projectFieldsMappings = projectFieldsMappings;
		this.projectSites = projectSites;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public BigDecimal getDayRate() {
		return dayRate;
	}

	public void setDayRate(BigDecimal dayRate) {
		this.dayRate = dayRate;
	}

	public Date getPaymentTarget() {
		return paymentTarget;
	}

	public void setPaymentTarget(Date paymentTarget) {
		this.paymentTarget = paymentTarget;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public String getWiki() {
		return wiki;
	}

	public void setWiki(String wiki) {
		this.wiki = wiki;
	}

	public String getTestServer() {
		return testServer;
	}

	public void setTestServer(String testServer) {
		this.testServer = testServer;
	}

	public String getSprintBacklog() {
		return sprintBacklog;
	}

	public void setSprintBacklog(String sprintBacklog) {
		this.sprintBacklog = sprintBacklog;
	}

	public String getProductBacklog() {
		return productBacklog;
	}

	public void setProductBacklog(String productBacklog) {
		this.productBacklog = productBacklog;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getSourceControl() {
		return sourceControl;
	}

	public void setSourceControl(String sourceControl) {
		this.sourceControl = sourceControl;
	}

	public String getProtocols() {
		return protocols;
	}

	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}

	public String getProjectSite() {
		return projectSite;
	}

	public void setProjectSite(String projectSite) {
		this.projectSite = projectSite;
	}

	public String getProjectBlog() {
		return projectBlog;
	}

	public void setProjectBlog(String projectBlog) {
		this.projectBlog = projectBlog;
	}

	public String getProjectPlan() {
		return projectPlan;
	}

	public void setProjectPlan(String projectPlan) {
		this.projectPlan = projectPlan;
	}

	public String getMailingLists() {
		return mailingLists;
	}

	public void setMailingLists(String mailingLists) {
		this.mailingLists = mailingLists;
	}

	public String getIssueTracking() {
		return issueTracking;
	}

	public void setIssueTracking(String issueTracking) {
		this.issueTracking = issueTracking;
	}

	public String getDms() {
		return dms;
	}

	public void setDms(String dms) {
		this.dms = dms;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getSonar() {
		return sonar;
	}

	public void setSonar(String sonar) {
		this.sonar = sonar;
	}

	public String getRiskAnalysis() {
		return riskAnalysis;
	}

	public void setRiskAnalysis(String riskAnalysis) {
		this.riskAnalysis = riskAnalysis;
	}

	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPageAuftrag() {
		return pageAuftrag;
	}

	public void setPageAuftrag(String pageAuftrag) {
		this.pageAuftrag = pageAuftrag;
	}

	public String getTestProtokoll() {
		return testProtokoll;
	}

	public void setTestProtokoll(String testProtokoll) {
		this.testProtokoll = testProtokoll;
	}

	public String getProcessWiki() {
		return processWiki;
	}

	public void setProcessWiki(String processWiki) {
		this.processWiki = processWiki;
	}

	public Long getFkResource() {
		return fkResource;
	}

	public void setFkResource(Long fkResource) {
		this.fkResource = fkResource;
	}

	public Long getAccounts() {
		return accounts;
	}

	public void setAccounts(Long accounts) {
		this.accounts = accounts;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public List<Overtime> getOvertimes() {
		return overtimes;
	}

	public void setOvertimes(List<Overtime> overtimes) {
		this.overtimes = overtimes;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public List<ProjectFieldsMapping> getProjectFieldsMappings() {
		return projectFieldsMappings;
	}

	public void setProjectFieldsMappings(List<ProjectFieldsMapping> projectFieldsMappings) {
		this.projectFieldsMappings = projectFieldsMappings;
	}

	public List<ProjectSites> getProjectSites() {
		return projectSites;
	}

	public void setProjectSites(List<ProjectSites> projectSites) {
		this.projectSites = projectSites;
	}

	@Override
	public List<UniqueConstraint> getUniqueConstraints() {
		List<UniqueConstraint> constraints = new ArrayList<UniqueConstraint>();

		UniqueConstraint constraint = new UniqueConstraint();
		constraint.setCurrentEntityId(this.pk);
		constraint.addProperty(new UniqueProperty("name", this.name)).addProperty(
				new UniqueProperty("customer.id", customer.getPk()));

		constraints.add(constraint);

		return constraints;
	}
}
