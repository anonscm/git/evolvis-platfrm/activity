/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

import java.util.List;

import de.tarent.activity.domain.Permission;

/**
 * PermissionFilter specify criteria to search for {@link Permission}s.
 */
public class PermissionFilter extends BaseFilter {

    private static final long serialVersionUID = 7876843948083826327L;

    private List<Long> roleIds;

    private List<Long> resourceIds;

    private List<Long> permissionIds;
    
    /**
     * Creates a new PermissionFilter with empty filter criteria.
     */
    public PermissionFilter() {
        super(null, null, null, null);
    }

    /**
     * Creates a new PermissionFilter with filter criteria to search permissions by roles or resources. Each
     * criteria may be null.
     * 
     * @param roleIds
     *            list of role id's, triggers the search to find only permissions that are assigned to given roles
     * @param resourceIds
     *            list of resource id's, triggers the search to find only permissions that are assigned (direct, not by
     *            role) to given resources
     * @param offset
     *            offset index
     * @param pageItems
     *            number of rows per page
     * @param sortColumn
     *            column to sort
     * @param sortOrder
     *            sort order
     */
    public PermissionFilter(List<Long> roleIds, List<Long> resourceIds, Long offset, Long pageItems, String sortColumn,
            String sortOrder) {
        super(offset, pageItems, sortColumn, sortOrder);
        this.roleIds = roleIds;
        this.resourceIds = resourceIds;
    }

    /**
     * Creates a new PermissionFilter with filter criteria to search permissions by roles or resources. Additional
     * to that criteria you can define that the searched permissions must be within a given set of permission id's. This
     * is useful if you want to perform a search over a result of a previous search.
     * <p>
     * Because the permission id's criteria already defines the range of permissions to search through you usually
     * should not have to set an offset index or a number of items per page. If you need an offset or number of items
     * anyway you must control that this criteria are within given permission id's. For example if permission id's
     * contains the last ten permissions but your offset and number of page items are set to find only the first ten
     * permissions then the search will never return any item.
     * 
     * @param roleIds
     *            list of role id's, triggers the search to find only permissions that are assigned to given roles
     * @param resourceIds
     *            list of resource id's, triggers the search to find only permissions that are assigned (direct, not by
     *            role) to given resources
     * @param permissionIds
     *            list of permission id's, triggers the search to find only permissions that have given id's
     * @param sortColumn
     *            column to sort
     * @param sortOrder
     *            sort order
     */
    public PermissionFilter(List<Long> roleIds, List<Long> resourceIds, List<Long> permissionIds, String sortColumn,
            String sortOrder) {
        this(roleIds, resourceIds, null, null, sortColumn, sortOrder);
        this.permissionIds = permissionIds;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    public List<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }

}
