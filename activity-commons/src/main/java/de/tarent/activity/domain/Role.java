/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import de.tarent.activity.domain.util.DomainObject;

/**
 * Role entity
 * 
 */
@Entity
@Table(name = "trole", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Role.GET_ALL", query = "select r from Role r order by r.name"),
		@NamedQuery(name = "Role.GET_BY_NAME", query = "select r from Role r where r.name = :name order by r.name"),
		@NamedQuery(name = "Role.GET_BY_PERMISSION", query = "select r from Role r join r.permissions perms where perms.actionId = :actionId"),
		@NamedQuery(name = "Role.GET_BY_PERMISSION_AND_RESOURCE", query = "select r from Role r join r.permissions perms join r.resources res where perms.actionId = :actionId and res.pk = :resourceId"),
		@NamedQuery(name = "Role.GET_BY_RESOURCE", query = "select r from Role r join r.resources res where res.pk = :resourceId") })
public class Role implements Serializable, DomainObject {

	private static final long serialVersionUID = 3745642154623454395L;

	@Id
	@GeneratedValue(generator = "trole_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "trole_pk_gen", sequenceName = "activity.trole_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "name", unique = true, length = 255, nullable = false)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@ManyToMany
	@JoinTable(schema = "activity", name = "troles_tpermissions")
	private Set<Permission> permissions;

	@ManyToMany
	@JoinTable(schema = "activity", name = "troles_tresources")
	private Set<Resource> resources;

	public Role() {
	}

	public Role(Long roleId) {
		this.pk = roleId;
	}

	public Role(String name) {
		this.name = name;
	}

	/**
	 * @return name of this role
	 * @see #setName(String)
	 */
	public String getName() {
		return name;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	/**
	 * @return list of assigned permissions
	 * @see #setPermissions(List)
	 */
	public Set<Permission> getPermissions() {
		return permissions;
	}

	/**
	 * @return list of assigned resources
	 * @see #setResources(List)
	 */
	public Set<Resource> getResources() {
		return resources;
	}

	/**
	 * Sets the name of this role. The name may have a maximal length of 255 character and must not be null.
	 * 
	 * @param name
	 *            new name of this role
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the permissions that are assigned to this role.
	 * 
	 * @param permissions
	 *            list of permissions that are assigned to this role
	 */
	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Sets the primary key of this role.
	 * 
	 * @param pk
	 *            primary key
	 */
	public void setPk(Long pk) {
		this.pk = pk;
	}

	/**
	 * Sets the resources that are assigned to this role.
	 * 
	 * @param resources
	 *            list of resources that are assigned to this role
	 */
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
