/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all ProjectFieldsMapping properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tproject_fields_mapping", schema = "activity")
public class ProjectFieldsMapping implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 1688169127815632598L;

	@Id
	@GeneratedValue(generator = "tproject_fields_mapping_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tproject_fields_mapping_pk_gen", sequenceName = "activity.tproject_fields_mapping_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_projectfield", nullable = false)
	private ProjectFields projectFields;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_project", nullable = false)
	private Project project;

	@Column(name = "fieldcontent", length = 255)
	private String fieldContent;

	@Column(name = "sort")
	private Long sort;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * This is default and null constructor.
	 */
	public ProjectFieldsMapping() {
	}

	/**
	 * Constructor using required fields.
	 * 
	 * @param pk
	 *            pk
	 * @param projectFields
	 *            projectFields
	 * @param project
	 *            project
	 */
	public ProjectFieldsMapping(Long pk, ProjectFields projectFields, Project project) {
		this.pk = pk;
		this.projectFields = projectFields;
		this.project = project;
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param pk
	 *            pk
	 * @param projectFields
	 *            projectFields
	 * @param project
	 *            project
	 * @param fieldContent
	 *            fieldContent
	 * @param sort
	 *            sort
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public ProjectFieldsMapping(Long pk, ProjectFields projectFields, Project project, String fieldContent, Long sort,
			String crUser, Date crDate, String updUser, Date updDate) {
		this.pk = pk;
		this.projectFields = projectFields;
		this.project = project;
		this.fieldContent = fieldContent;
		this.sort = sort;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public ProjectFields getProjectFields() {
		return projectFields;
	}

	public void setProjectFields(ProjectFields projectFields) {
		this.projectFields = projectFields;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getCrUser() {
		return crUser;
	}

	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
