/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

/**
 * This class stores Skills filter properties.
 * 
 */
public class SkillsFilter extends BaseFilter {

	private static final long serialVersionUID = -4519128629621484115L;

	private Long resourceId;

	private Long skillsDefId;

	/**
	 * Constructor.
	 * 
	 * @param resId - resId
	 * @param skillsDefId - skillsDefId
	 * @param offset - offset
	 * @param pageItems - pageItems
	 * @param sortColumn - sortColumn
	 * @param sortOrder - sortOrder
	 */
	public SkillsFilter(Long resId, Long skillsDefId, Long offset, Long pageItems, String sortColumn, String sortOrder) {
		super(offset, pageItems, sortColumn, sortOrder);
		this.resourceId = resId;
		this.skillsDefId = skillsDefId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resId) {
		this.resourceId = resId;
	}

	public Long getSkillsDefId() {
		return skillsDefId;
	}

	public void setSkillsDefId(Long skillsDefId) {
		this.skillsDefId = skillsDefId;
	}

}
