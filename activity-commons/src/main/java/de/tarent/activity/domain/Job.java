/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueConstraint;
import de.tarent.activity.domain.util.UniqueProperty;
import de.tarent.activity.domain.util.UniqueValidationAware;

/**
 * @author This class stores all Job properties.
 */
@Entity
@Audited
@Table(name = "tjob", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Job.GET_ALL", query = "SELECT j FROM Job j join fetch j.project order by j.project.name, j.name"),
		@NamedQuery(name = "Job.GET_ACTIVE_BY_RESOURCE", query = "SELECT distinct j FROM Job j join fetch j.project proj join j.positions pos "
				+ "join pos.posResourceMappings prm where prm.resource.pk=:resId and prm.posResourceStatus.pk=:posStatusId and (prm.startDate is null or prm.startDate <=:endDate)"
				+ " and (prm.endDate is null or prm.endDate >=:startDate) and (j.jobStatus.pk=:jobStatusId1 or j.jobStatus.pk=:jobStatusId2) order by proj.name, j.name"),
		@NamedQuery(name = "Job.GET_NOT_MAINTENANCE", query = "SELECT j FROM Job j join fetch j.project where j.isMaintenance <> 't' order by j.project.name, j.name"),
		@NamedQuery(name = "Job.GET_BY_PROJECT", query = "SELECT j FROM Job j where j.project.pk=:projectId order by j.name"),
		@NamedQuery(name = "Job.GET_ORDERED", query = "SELECT j FROM Job j where j.jobStatus.pk=2 order by j.project.name, j.name"),
		@NamedQuery(name = "Job.GET_BY_RESOURCE", query = "SELECT j FROM Job j join fetch j.project join j.positions pos "
				+ "join pos.posResourceMappings prm where prm.resource.pk=:resId order by j.project.name, j.name"),
		@NamedQuery(name = "Job.SUM_TOTAL_HOURS_SPENT", query = "SELECT SUM(a.hours) FROM Job j JOIN j.positions p JOIN p.activities a WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_EXPECTED_WORK", query = "SELECT SUM(p.expectedWork) FROM Job j JOIN j.positions p WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_COMMUNICATED_WORK", query = "SELECT SUM(p.communicatedWork) FROM Job j JOIN j.positions p WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_SERVICE_COSTS", query = "SELECT SUM(p.cost) FROM Job j JOIN j.positions p WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_FIXED_PRICE", query = "SELECT SUM(p.fixedPrice) FROM Job j JOIN j.positions p WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_INVOICE", query = "SELECT SUM(i.amount) FROM Job j JOIN j.invoices i WHERE j.pk = :jobId"),
		@NamedQuery(name = "Job.SUM_INVOICE_PAID", query = "SELECT SUM(i.amount) FROM Job j JOIN j.invoices i WHERE j.pk = :jobId AND i.payed IS NOT NULL"),
		@NamedQuery(name = "Job.SUM_OTHER_COSTS", query = "SELECT SUM(c.cost) FROM Job j JOIN j.costs c WHERE j.pk = :jobId")
})
public class Job implements java.io.Serializable, DomainObject, Auditable, UniqueValidationAware {

	private static final long serialVersionUID = -3349380267868102281L;

	@Id
	@GeneratedValue(generator = "tjob_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tjob_pk_gen", sequenceName = "activity.tjob_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_jobtype", nullable = false)
	private JobType jobType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_manager", nullable = false)
	private Resource manager;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_project", nullable = false)
	private Project project;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_jobstatus", nullable = false)
	private JobStatus jobStatus;

	@Column(name = "name", length = 255)
	@AuditCompareField
	private String name;

	// maybe an enumeration here
	@Column(name = "billable", length = 1)
	@AuditCompareField
	private Character billable;

	@Temporal(TemporalType.DATE)
	@Column(name = "expectedbilling", length = 29)
	private Date expectedBilling;

	@Temporal(TemporalType.DATE)
	@Column(name = "realbilling", length = 29)
	private Date realBilling;

	@Column(name = "note", length = 4000)
	@AuditCompareField
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "payed", length = 29)
	private Date payed;

	@Column(name = "nr", length = 255)
	@AuditCompareField
	private String nr;

	// enumeration here or boolean
	@Column(name = "ismaintenance", length = 1)
	private String isMaintenance;

	@Temporal(TemporalType.DATE)
	@Column(name = "jobstartdate", length = 29)
	private Date jobStartDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "jobenddate", length = 29)
	private Date jobEndDate;

	@Column(name = "accounts")
	@AuditCompareField
	private Long accounts;

	@Column(name = "crm", length = 255)
	@AuditCompareField
	private String crm;

	@Column(name = "offer", length = 255)
	@AuditCompareField
	private String offer;

	@Column(name = "request", length = 255)
	@AuditCompareField
	private String request;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "job")
	private List<Invoice> invoices;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "job")
	private List<Position> positions;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "job")
	private List<Cost> costs;

	/**
	 * This is default and null constructor.
	 */
	public Job() {
	}

	/**
	 * Constructor with required fields.
	 * 
	 * @param pk
	 *            Job primary key
	 * @param name
	 *            name
	 * @param jobType
	 *            JobType
	 * @param resource
	 *            Resource
	 * @param project
	 *            Project
	 * @param jobStatus
	 *            JobStatus
	 */
	public Job(Long pk, String name, JobType jobType, Resource resource, Project project, JobStatus jobStatus) {
		this.pk = pk;
		this.jobType = jobType;
		this.manager = resource;
		this.project = project;
		this.jobStatus = jobStatus;
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param pk
	 *            - Job primary key
	 * @param jobType
	 *            jobType
	 * @param resource
	 *            resource
	 * @param project
	 *            project
	 * @param jobStatus
	 *            jobStatus
	 * @param name
	 *            name
	 * @param billable
	 *            billable
	 * @param expectedBilling
	 *            expectedBilling
	 * @param realBilling
	 *            realBilling
	 * @param note
	 *            note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param payed
	 *            payed
	 * @param nr
	 *            nr
	 * @param isMaintenance
	 *            isMaintenance
	 * @param jobStartDate
	 *            jobStartDate
	 * @param jobEndDate
	 *            jobEndDate
	 * @param accounts
	 *            accounts
	 * @param crm
	 *            crm
	 * @param offer
	 *            offer
	 * @param request
	 *            request
	 * @param invoices
	 *            - List<Invoice>
	 * @param positions
	 *            - List<Position>
	 * @param costs
	 *            - List<Cost>
	 */
	public Job(Long pk, JobType jobType, Resource resource, Project project, JobStatus jobStatus, String name,
			Character billable, Date expectedBilling, Date realBilling, String note, String crUser, Date crDate,
			String updUser, Date updDate, Date payed, String nr, String isMaintenance, Date jobStartDate,
			Date jobEndDate, Long accounts, String crm, String offer, String request, List<Invoice> invoices,
			List<Position> positions, List<Cost> costs) {
		this.pk = pk;
		this.jobType = jobType;
		this.manager = resource;
		this.project = project;
		this.jobStatus = jobStatus;
		this.name = name;
		this.billable = billable;
		this.expectedBilling = expectedBilling;
		this.realBilling = realBilling;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.payed = payed;
		this.nr = nr;
		this.isMaintenance = isMaintenance;
		this.jobStartDate = jobStartDate;
		this.jobEndDate = jobEndDate;
		this.accounts = accounts;
		this.crm = crm;
		this.offer = offer;
		this.request = request;
		this.invoices = invoices;
		this.positions = positions;
		this.costs = costs;
	}

	/**
	 * Constructor: Used to duplicate a job
	 * 
	 * @param job
	 *            job
	 */
	public Job(Job job) {
		this.accounts = job.accounts;
		this.billable = job.billable;
		this.crm = job.crm;
		this.isMaintenance = job.isMaintenance;
		this.jobEndDate = job.jobEndDate;
		this.jobStartDate = job.jobStartDate;
		this.jobStatus = job.jobStatus;
		this.jobType = job.jobType;
		this.manager = job.manager;
		this.name = job.name + " Kopie";
		this.note = job.note;
		this.nr = job.nr;
		this.offer = job.offer;
		this.project = job.project;
		this.request = job.request;
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 */
	public Job(Long pk) {
		this.pk = pk;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public JobType getJobType() {
		return jobType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	public Resource getManager() {
		return manager;
	}

	public void setManager(Resource manager) {
		this.manager = manager;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public JobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Character getBillable() {
		return billable;
	}

	public void setBillable(Character billable) {
		this.billable = billable;
	}

	public Date getExpectedBilling() {
		return expectedBilling;
	}

	public void setExpectedBilling(Date expectedBilling) {
		this.expectedBilling = expectedBilling;
	}

	public Date getRealBilling() {
		return realBilling;
	}

	public void setRealBilling(Date realBilling) {
		this.realBilling = realBilling;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public Date getPayed() {
		return payed;
	}

	public void setPayed(Date payed) {
		this.payed = payed;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public String getIsMaintenance() {
		return isMaintenance;
	}

	public void setIsMaintenance(String isMaintenance) {
		this.isMaintenance = isMaintenance;
	}

	public Date getJobStartDate() {
		return jobStartDate;
	}

	public void setJobStartDate(Date jobStartDate) {
		this.jobStartDate = jobStartDate;
	}

	public Date getJobEndDate() {
		return jobEndDate;
	}

	public void setJobEndDate(Date jobEndDate) {
		this.jobEndDate = jobEndDate;
	}

	public Long getAccounts() {
		return accounts;
	}

	public void setAccounts(Long accounts) {
		this.accounts = accounts;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public List<Cost> getCosts() {
		return costs;
	}

	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}

	@Override
	public List<UniqueConstraint> getUniqueConstraints() {
		List<UniqueConstraint> constraints = new ArrayList<UniqueConstraint>();

		UniqueConstraint constraint = new UniqueConstraint();
		constraint.setCurrentEntityId(this.pk);
		constraint.addProperty(new UniqueProperty("name", this.name)).addProperty(
				new UniqueProperty("project.pk", project.getPk()));

		constraints.add(constraint);

		return constraints;
	}

}
