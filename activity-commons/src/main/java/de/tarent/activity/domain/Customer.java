/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores Customer properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tcustomer", schema = "activity")
@NamedQueries({ @NamedQuery(name = "Customer.GET_ALL", query = "SELECT c FROM Customer c order by c.name") })
public class Customer implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 266159177233831354L;

	@Id
	@GeneratedValue(generator = "tcustomer_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tcustomer_pk_gen", sequenceName = "activity.tcustomer_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "note", length = 4000)
	private String note;

	@Column(name = "dayrate", precision = 10, scale = 2)
	private BigDecimal dayRate;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_target", length = 29)
	private Date paymentTarget;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
	private List<Project> projects;

	/**
	 * This is default and null constructor.
	 */
	public Customer() {
	}

	/**
	 * Constructor with primary key field.
	 *
	 * @param pk
	 *            pk
	 */
	public Customer(Long pk) {
		this.pk = pk;
	}

	/**
	 * Constructor with all fields.
	 *
	 * @param pk
	 *            pk
	 * @param name
	 *            name
	 * @param note
	 *            note
	 * @param dayRate
	 *            dayRate
	 * @param paymentTarget
	 *            paymentTarget
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param projects
	 *            projects
	 */
	public Customer(Long pk, String name, String note, BigDecimal dayRate, Date paymentTarget, String crUser,
			Date crDate, String updUser, Date updDate, List<Project> projects) {
		this.pk = pk;
		this.name = name;
		this.note = note;
		this.dayRate = dayRate;
		this.paymentTarget = paymentTarget;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.projects = projects;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public BigDecimal getDayRate() {
		return dayRate;
	}

	public void setDayRate(BigDecimal dayRate) {
		this.dayRate = dayRate;
	}

	public Date getPaymentTarget() {
		return paymentTarget;
	}

	public void setPaymentTarget(Date paymentTarget) {
		this.paymentTarget = paymentTarget;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

}
