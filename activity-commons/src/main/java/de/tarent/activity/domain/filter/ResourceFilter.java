/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

import java.util.List;

import de.tarent.activity.domain.Resource;

/**
 * ResourceFilter specify criteria to search for {@link Resource}s
 * 
 */
public class ResourceFilter extends BaseFilter {

    private static final long serialVersionUID = 512225872581602197L;

    private Character active;

    private List<Long> resourceIds;
    
    private List<Long> roleIds;
    
    private Long jobId;
    
    private Long positionId;
    
    public ResourceFilter(){
        super(null, null, null, null);
    }

    /**
     * Creates a new ResourceFilter with filter criteria to search by active or inactive resources and by roles.
     * 
     * @param active
     *            must be 't' for true or 'f' for false
     * @param roleIds
     *            list of role id's, triggers the search to find only resources that are assigned to given roles
     * @param offset
     *            offset index
     * @param pageItems
     *            number of rows per page
     * @param sortColumn
     *            column to sort
     * @param sortOrder
     *            sort order
     */
    public ResourceFilter(Character active, List<Long> roleIds, Long offset, Long pageItems, String sortColumn,
            String sortOrder) {
        super(offset, pageItems, sortColumn, sortOrder);
        this.active = active;
        this.setRoleIds(roleIds);
    }
    
    /**
     * Creates a new ResourceFilter with filter criteria to search by active or inactive resources. Additional to that
     * criteria you can define that the searched resources must be within a given set of resource id's. This is useful
     * if you want to perform a search over a result of a previous search.
     * <p>
     * Because the resource id's criteria already defines the range of resources to search through you usually should
     * not have to set an offset index or a number of items per page. If you need an offset or number of items anyway
     * you must control that this criteria are within given resource id's. For example if resource id's contains the
     * last ten resources but your offset and number of page items are set to find only the first ten resources then the
     * search will never return any item.
     * 
     * @param active
     *            must be 't' for true or 'f' for false
     * @param roleIds
     *            list of role id's, triggers the search to find only resources that are assigned to given roles
     * @param resourceIds
     *            list of resource id's, triggers the search to find only resources that have given id's
     * @param sortColumn
     *            column to sort
     * @param sortOrder
     *            sort order
     */
    public ResourceFilter(Character active, List<Long> roleIds, List<Long> resourceIds, String sortColumn,
            String sortOrder) {
        this(active, roleIds, null, null, sortColumn, sortOrder);
        this.setResourceIds(resourceIds);
    }

    public ResourceFilter(Long startIndex, Long results, String sortColumn, String sortOrder) {
		super(startIndex, results, sortColumn, sortOrder);
	}

	public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public List<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}
