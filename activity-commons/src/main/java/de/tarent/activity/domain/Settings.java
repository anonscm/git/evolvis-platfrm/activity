/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * Settings entity.
 * 
 */
@Entity
@Table(name = "tsettings", schema = "activity")
@NamedQueries({ @NamedQuery(name = "Settings.GET_ALL", query = "SELECT s FROM Settings s "),
		@NamedQuery(name = "Settings.GET_BY_KEY", query = "SELECT s FROM Settings s where s.key like :key ") })
public class Settings implements Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -8378389465138845008L;

	@Id
	@GeneratedValue(generator = "tsettings_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tsettings_pk_gen", sequenceName = "activity.tsettings_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "key", length = 255)
	private String key;

	@Column(name = "value", length = 255)
	private String value;

	@Column(name = "cr_user")
	private String crUser;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date")
	private Date crDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date")
	private Date updDate;

	/**
	 * Constructor.
	 */
	public Settings() {

	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param key
	 *            key
	 * @param value
	 *            value
	 */
	public Settings(Long pk, String key, String value) {
		this.pk = pk;
		this.key = key;
		this.value = value;
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param key
	 *            key
	 * @param value
	 *            value
	 * @param crUser
	 *            crUser
	 * @param updUser
	 *            updUser
	 * @param crDate
	 *            crDate
	 * @param updDate
	 *            updDate
	 */
	public Settings(Long pk, String key, String value, String crUser, String updUser, Date crDate, Date updDate) {
		this(pk, key, value);
		this.crUser = crUser;
		this.updUser = updUser;
		this.crDate = crDate;
		this.updDate = updDate;
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCrUser() {
		return crUser;
	}

	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

}
