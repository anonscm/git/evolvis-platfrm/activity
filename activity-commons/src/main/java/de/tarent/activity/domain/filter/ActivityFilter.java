/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

import java.util.Date;

/**
 * This class stores Activity filter properties.
 * 
 */
public class ActivityFilter extends BaseFilter {

	private static final long serialVersionUID = 8938637273017414649L;

	private Long resourceId;
	private Long jobId;
	private Long positionId;
	private Date startDate;
	private Date endDate;
	private Long projectId;

	/**
	 * Constructor using all fields.
	 * 
	 * @param resourceId
	 *            Resource id
	 * @param jobId
	 *            Job id
	 * @param positionId
	 *            Position id
	 * @param startDate
	 *            Activity start date
	 * @param endDate
	 *            Activity end date
	 * @param projectId
	 *            - project Id
	 * @param offset
	 *            offset index
	 * @param pageItems
	 *            number of rows per page
	 * @param sortColumn
	 *            the column to sort
	 * @param sortOrder
	 *            sort order
	 */
	public ActivityFilter(Long resourceId, Long jobId, Long positionId, Date startDate, Date endDate, Long projectId,
			Long offset, Long pageItems, String sortColumn, String sortOrder) {
		super(offset, pageItems, sortColumn, sortOrder);
		this.resourceId = resourceId;
		this.jobId = jobId;
		this.positionId = positionId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.projectId = projectId;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Long getJobId() {
		return jobId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
