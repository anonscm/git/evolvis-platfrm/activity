/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all PosResourceMapping properties.
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tpos_res_mapping", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "PosResourceMapping.GET_BY_POSITION", query = "SELECT posRes FROM PosResourceMapping posRes"
				+ " join posRes.position pos where pos.pk = :posId"),
		@NamedQuery(name = "PosResourceMapping.GET_BY_POSITION_AND_RESOURCE", query = "SELECT posRes FROM PosResourceMapping"
				+ " posRes where posRes.position.pk = :posId and posRes.resource.pk = :resourceId") })
public class PosResourceMapping implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -6306987503645768654L;

	@Id
	@GeneratedValue(generator = "tpos_res_mapping_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tpos_res_mapping_pk_gen", sequenceName = "activity.tpos_res_mapping_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_position", nullable = false)
	private Position position;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_status", nullable = false)
	private PosResourceStatus posResourceStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@Column(name = "percent")
	private Long percent;

	@Temporal(TemporalType.DATE)
	@Column(name = "startdate", length = 29)
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "enddate", length = 29)
	private Date endDate;

	/**
	 * This is default and null constructor.
	 */
	public PosResourceMapping() {
	}

	/**
	 * Constructor using required fields.
	 * 
	 * @param pk
	 *            PosResourceMapping primary key
	 * @param position
	 *            Position
	 * @param posResourceStatus
	 *            PosResourceStatus
	 * @param resource
	 *            Resource
	 */
	public PosResourceMapping(Long pk, Position position, PosResourceStatus posResourceStatus, Resource resource) {
		this.pk = pk;
		this.position = position;
		this.posResourceStatus = posResourceStatus;
		this.resource = resource;
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param pk
	 *            PosResourceMapping primary key
	 * @param tposition
	 *            Position
	 * @param posResourceStatus
	 *            PosResourceStatus
	 * @param resource
	 *            Resource
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param percent
	 *            PosResourceMapping percent
	 * @param startDate
	 *            PosResourceMapping start date
	 * @param endDate
	 *            PosResourceMapping end date
	 */
	public PosResourceMapping(Long pk, Position tposition, PosResourceStatus posResourceStatus, Resource resource,
			String crUser, Date crDate, String updUser, Date updDate, Long percent, Date startDate, Date endDate) {
		this.pk = pk;
		this.position = tposition;
		this.posResourceStatus = posResourceStatus;
		this.resource = resource;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.percent = percent;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * Constructor.
	 * 
	 * @param posRes
	 *            posRes
	 */
	public PosResourceMapping(PosResourceMapping posRes) {
		this.percent = posRes.getPercent();
		this.posResourceStatus = posRes.getPosResourceStatus();
		this.resource = posRes.getResource();
		this.startDate = posRes.getStartDate();
		this.endDate = posRes.getEndDate();
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public PosResourceStatus getPosResourceStatus() {
		return posResourceStatus;
	}

	public void setPosResourceStatus(PosResourceStatus posResourceStatus) {
		this.posResourceStatus = posResourceStatus;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public Long getPercent() {
		return percent;
	}

	public void setPercent(Long percent) {
		this.percent = percent;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
