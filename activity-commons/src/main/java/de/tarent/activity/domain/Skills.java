/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * Skills entity.
 * 
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tskills", schema = "activity")
@NamedQueries({
		@NamedQuery(name = "Skills.GET_BY_RESOURCE", query = "select s from Skills s join fetch s.skillsDef where s.resource=:resId"),
		@NamedQuery(name = "Skills.GET_BY_RESOURCE_AND_SKILL_DEF", query = "select s from Skills s where s.resource.pk=:resId and s.skillsDef.pk=:skillDefId") })
public class Skills implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = 6839910403996815767L;

	@Id
	@GeneratedValue(generator = "tskills_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tskills_pk_gen", sequenceName = "activity.tskills_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_skills_def", nullable = false)
	private SkillsDef skillsDef;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false)
	private Resource resource;

	@Column(name = "value", nullable = false)
	private int value;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	/**
	 * Constructor.
	 */
	public Skills() {
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param skillsDef
	 *            skillsDef
	 * @param resource
	 *            resource
	 * @param value
	 *            value
	 */
	public Skills(Long pk, SkillsDef skillsDef, Resource resource, int value) {
		this.pk = pk;
		this.skillsDef = skillsDef;
		this.resource = resource;
		this.value = value;
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param skillsDef
	 *            skillsDef
	 * @param resource
	 *            resource
	 * @param value
	 *            value
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 */
	public Skills(Long pk, SkillsDef skillsDef, Resource resource, int value, String crUser, Date crDate,
			String updUser, Date updDate) {
		this.pk = pk;
		this.skillsDef = skillsDef;
		this.resource = resource;
		this.value = value;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
	}

	@Override
	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public SkillsDef getSkillsDef() {
		return skillsDef;
	}

	public void setSkillsDef(SkillsDef skillsDef) {
		this.skillsDef = skillsDef;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	@Override
	public String getUpdUser() {
		return updUser;
	}

	@Override
	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	@Override
	public Date getUpdDate() {
		return updDate;
	}

	@Override
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
}
