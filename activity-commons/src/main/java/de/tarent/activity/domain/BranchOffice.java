/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * BranchOffice entity.
 * 
 */
@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "tbranchoffice", schema = "activity")
@NamedQueries({ 
	@NamedQuery(name = "BranchOffice.GET_ALL", query = "select bo from BranchOffice bo") 
})
public class BranchOffice implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -7869313085465915100L;

	@Id
	@GeneratedValue(generator = "tbranchoffice_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tbranchoffice_pk_gen", sequenceName = "activity.ttbranchoffice_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "branchOffice")
	private List<Resource> resources;

	/**
	 * Constuctor.
	 */
	public BranchOffice() {
	}

	/**
	 * Constructor.
	 * 
	 * @param pk
	 *            pk
	 * @param name
	 *            name
	 */
	public BranchOffice(Long pk, String name) {
		this.pk = pk;
		this.name = name;
	}

	/**
	 * Constructor with params.
	 * 
	 * @param pk
	 *            pk
	 * @param name
	 *            name
	 * @param currentUser
	 *            currentUser
	 * @param currentDate
	 *            currentDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param resources
	 *            resources
	 */
	public BranchOffice(Long pk, String name, String currentUser, Date currentDate, String updUser, Date updDate,
			List<Resource> resources) {
		this.pk = pk;
		this.name = name;
		this.crUser = currentUser;
		this.crDate = currentDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.resources = resources;
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrentUser() {
		return crUser;
	}

	public void setCurrentUser(String currentUser) {
		this.crUser = currentUser;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	@Override
	public String getCrUser() {
		return crUser;
	}

	@Override
	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	@Override
	public Date getCrDate() {
		return crDate;
	}

	@Override
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
}
