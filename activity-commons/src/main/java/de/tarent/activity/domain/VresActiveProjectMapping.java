/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "vres_project_mapping", schema = "activity")
@NamedQueries({ @NamedQuery(name = "VresActiveProjectMapping.GET_BY_RESOURCE_AND_STATUS", query = "SELECT vres from VresActiveProjectMapping vres where vres.resource.pk = :resId and vres.status = :status") })
public class VresActiveProjectMapping implements java.io.Serializable {

	private static final long serialVersionUID = 1695520548720585848L;

	@EmbeddedId
	private VresActiveProjectMappingId embeddedId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_resource", nullable = false, insertable = false, updatable = false)
	private Resource resource;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_project", nullable = false, insertable = false, updatable = false)
	private Project project;

	@Column(name = "fk_status")
	private Long status;

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public VresActiveProjectMappingId getEmbeddedId() {
		return embeddedId;
	}

	public void setEmbeddedId(VresActiveProjectMappingId embeddedId) {
		this.embeddedId = embeddedId;
	}

}
