/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.util;

import java.io.Serializable;
import java.util.List;
import java.util.Arrays;

/**
 * Table model.
 * 
 */
public class TableModel implements Serializable {

	private static final long serialVersionUID = 3199293479756022267L;

	private List<Object[]> itemsDescription;
	private String[] headerCellList;
	private List<Object[]> items;
	private int numberOfColumns;
	private String tableCaption;
	private String tableFooter;

	/**
	 * Constructor.
	 * 
	 * @param headerCellList
	 *            - headerCellList
	 * @param items
	 *            - items
	 * @param tableCaption
	 *            - tableCaption
	 * @param tableFooter
	 *            - tableFooter
	 */
	public TableModel(String[] headerCellList, List<Object[]> items, String tableCaption, String tableFooter) {

		if ((headerCellList == null) || (items == null)){
			throw new IllegalArgumentException();
        }

		this.headerCellList = Arrays.copyOf(headerCellList, headerCellList.length);
        this.items = items;
		this.tableCaption = tableCaption;
		this.tableFooter = tableFooter;
		this.numberOfColumns = headerCellList.length;
	}

	public String[] getHeaderCellList() {
		return headerCellList;
	}

	public List<Object[]> getItems() {
		return items;
	}

	public Integer getNumberOfColumns() {
		return numberOfColumns;
	}

	public String getTableCaption() {
		return tableCaption;
	}

	public String getTableFooter() {
		return tableFooter;
	}

	public void setHeaderCellList(String[] headerCellList) {
		this.headerCellList = headerCellList != null ? Arrays.copyOf(headerCellList, headerCellList.length) : null;
	}

	public void setItems(List<Object[]> items) {
		this.items = items;
	}

	public void setTableCaption(String tableCaption) {
		this.tableCaption = tableCaption;
	}

	public void setTableFooter(String tableFooter) {
		this.tableFooter = tableFooter;
	}

	public List<Object[]> getItemsDescription() {
		return itemsDescription;
	}

	public void setItemsDescription(List<Object[]> itemsDescription) {
		this.itemsDescription = itemsDescription;
	}
	
}
