/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;

/**
 * This class stores all PosResourceStatus properties.
 */
@Entity
@Table(name = "tpos_res_status", schema = "activity")
@NamedQueries({ @NamedQuery(name = "PosResourceStatus.GET_ALL", query = "select status from PosResourceStatus status") })
public class PosResourceStatus implements java.io.Serializable, DomainObject, Auditable {

	private static final long serialVersionUID = -3305061838653648824L;

	@Id
	@GeneratedValue(generator = "tpos_res_status_pk_gen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tpos_res_status_pk_gen", sequenceName = "activity.tpos_res_status_pk_seq", allocationSize = 1)
	@Column(name = "pk", unique = true, nullable = false)
	private Long pk;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "note", length = 1000)
	private String note;

	@Column(name = "cr_user")
	private String crUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cr_date", length = 29)
	private Date crDate;

	@Column(name = "upd_user")
	private String updUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upd_date", length = 29)
	private Date updDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "posResourceStatus")
	private List<PosResourceMapping> firstPosResMappings;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "posResourceStatus")
	private List<PosResourceMapping> secondPosResMappings;

	/**
	 * This is default and null constructor.
	 */
	public PosResourceStatus() {
	}

	/**
	 * Constructor with primary key field.
	 * 
	 * @param pk
	 *            pk
	 */
	public PosResourceStatus(Long pk) {
		this.pk = pk;
	}

	/**
	 * Constructor using all fields.
	 * 
	 * @param pk
	 *            pk
	 * @param name
	 *            name
	 * @param note
	 *            note
	 * @param crUser
	 *            crUser
	 * @param crDate
	 *            crDate
	 * @param updUser
	 *            updUser
	 * @param updDate
	 *            updDate
	 * @param firstPosResMappings
	 *            firstPosResMappings
	 * @param secondPosResMappings
	 *            secondPosResMappings
	 */
	public PosResourceStatus(Long pk, String name, String note, String crUser, Date crDate, String updUser,
			Date updDate, List<PosResourceMapping> firstPosResMappings, List<PosResourceMapping> secondPosResMappings) {
		this.pk = pk;
		this.name = name;
		this.note = note;
		this.crUser = crUser;
		this.crDate = crDate;
		this.updUser = updUser;
		this.updDate = updDate;
		this.firstPosResMappings = firstPosResMappings;
		this.secondPosResMappings = secondPosResMappings;
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCrUser() {
		return crUser;
	}

	public void setCrUser(String crUser) {
		this.crUser = crUser;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public String getUpdUser() {
		return updUser;
	}

	public void setUpdUser(String updUser) {
		this.updUser = updUser;
	}

	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}

	public List<PosResourceMapping> getFirstPosResMappings() {
		return firstPosResMappings;
	}

	public void setFirstPosResMappings(List<PosResourceMapping> firstPosResMappings) {
		this.firstPosResMappings = firstPosResMappings;
	}

	public List<PosResourceMapping> getSecondPosResMappings() {
		return secondPosResMappings;
	}

	public void setSecondPosResMappings(List<PosResourceMapping> secondPosResMappings) {
		this.secondPosResMappings = secondPosResMappings;
	}
}
