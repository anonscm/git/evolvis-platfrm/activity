/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.domain.filter;

/**
 * This class stores all Invoice filter properties.
 * 
 */
public class InvoiceFilter extends BaseFilter {

	private static final long serialVersionUID = 5732035884918300200L;

	private Long customerId;
	private Long projectId;
	private Long jobId;
	private Integer type;

	/**
	 * Constructor.
	 * 
	 * @param customerId
	 *            - customerId
	 * @param projectId
	 *            - projectId
	 * @param jobId
	 *            - jobId
	 * @param type
	 *            - type
	 * @param offset
	 *            - offset
	 * @param pageItems
	 *            - pageItems
	 * @param sortColumn
	 *            - sortColumn
	 * @param sortOrder
	 *            - sortOrder
	 */
	public InvoiceFilter(Long customerId, Long projectId, Long jobId, Integer type, Long offset, Long pageItems,
			String sortColumn, String sortOrder) {
		super(offset, pageItems, sortColumn, sortOrder);
		this.customerId = customerId;
		this.projectId = projectId;
		this.jobId = jobId;
		this.type = type;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public Long getJobId() {
		return jobId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public Integer getType() {
		return type;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
