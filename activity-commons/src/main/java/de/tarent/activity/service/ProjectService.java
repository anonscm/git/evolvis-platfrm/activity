/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.*;
import de.tarent.activity.exception.UniqueValidationException;

import java.util.List;

/**
 * ProjectService.
 * 
 */
public interface ProjectService {
	// Job types
	static final Long JOBTYPE_CUSTOMERORDER = 1L; // Kundenauftrag
	static final Long JOBTYPE_BODYLEASING = 2L; // Bodyleasing
	static final Long JOBTYPE_INTERNALJOB = 3L; // Interner Auftrag

	// Job statuses
    static final Long JOBSTATUS_ORDERED = 2L; // beauftragt
    static final Long JOBSTATUS_FINISHED = 4L; // fertiggestellt

    // Todo: cleanup these Job statuses which are no longer used in activity now
	static final Long JOBSTATUS_OFFER = 1L; // Angebot
    static final Long JOBSTATUS_CHARGED = 5L; // abgerechnet
    static final Long JOBSTATUS_REJECTED = 3L; // abgelehnt
    static final Long JOBSTATUS_PAID = 6L; // bezahlt


	// Position statuses
	static final Long POSITIONSTATUS_OPEN = 1L; // offen
	static final Long POSITIONSTATUS_ATWORK = 2L; // in arbeit
	static final Long POSITIONSTATUS_FINISHED = 3L; // fertiggestellt

	// Position resource mapping statuses
	static final Long POS_RES_STATUS_INACTIVE = 0L; // inaktiv
	static final Long POS_RES_STATUS_ACTIVE = 1L; // aktiv
	static final Long POS_RES_STATUS_PLANNED = 2L; // geplant

	// Invoice types
	static final Integer INVOICE_TYPE_NEW = 0;
	static final Integer INVOICE_TYPE_INVOICED = 1;
	static final Integer INVOICE_TYPE_PAID = 2;
	static final Integer INVOICE_TYPE_COMPLETED = 3;

	/**
	 * Add a new {@link Customer}.
	 * 
	 * @param customer
	 *            - customer to be added
	 * @return customer id
	 */
	Long addCustomer(Customer customer);

	/**
	 * Add a new {@link Job}.
	 * 
	 * @param job
	 *            - job to be added
	 * @return job id
	 * @throws UniqueValidationException
	 */
	Long addJob(Job job) throws UniqueValidationException;

	/**
	 * Add a new {@link Position}.
	 * 
	 * @param sosition
	 *            - position to be added
	 * @return Position id
	 */
	Long addPosition(Position position);

	/**
	 * Create a {@link PosResourceMapping}.
	 * 
	 * @param posRes
	 *            the object to persist
	 */
	void addPosResourceMapping(PosResourceMapping posRes);

	/**
	 * Add a new {@link Project}.
	 * 
	 * @param project
	 *            - project to be added
	 * @return project id
	 * @throws UniqueValidationException
	 */
	Long addProject(Project project) throws UniqueValidationException;

	/**
	 * Delete a {@link Customer}.
	 * 
	 * @param id
	 *            customer id
	 */
	void deleteCustomer(Long id);

	/**
     * Delete the specified {@link Job} and all associated {@link Position}s
     * (with {@link Timer}s and {@link PosResourceMapping}s).
     * If {@link Activity}s are associated to a {@link Position} or
     * {@link Cost}s or {@link Invoice}s are associated to the {@link Job}, the
     * operation will fail. Use {@link #isDeletableJob(Long)} to prevent
     * an exception.
     * 
     * @param jobId
     */
    void deleteJob(Long jobId);

	/**
     * Delete the specified position and all associated {@link Timer}s and
     * {@link PosResourceMapping}s.
     * If {@link Activity}s are associated to the {@link Position}, the
     * operation will fail. Use {@link #isDeletablePosition(Long)} to prevent
     * an exception.
     * 
     * @param positionId
     */
    void deletePosition(Long positionId);

	/**
	 * Delete a {@link PosResourceMapping}.
	 * 
	 * @param id
	 *            PosResourceMapping id
	 */
	void deletePosResourceMapping(Long id);

	/**
     * Delete the specified {@link Project} and all associated
     * {@link ProjectFieldsMapping}s, {@link ProjectSites} and {@link Job}s
     * (with {@link Position}s, {@link Timer}s and {@link PosResourceMapping}s).
     * If {@link Activity}s are associated to a {@link Position} or
     * {@link Cost}s or {@link Invoice}s are associated to a {@link Job} or
     * {@link Overtime}s are assocoated to the {@link Project}, the
     * operation will fail. Use {@link #isDeletableProject(Long)} to prevent
     * an exception.
     * 
     * @param projectId
     */
    void deleteProject(Long projectId);
	
	
	/**
	 * Duplicates a job object along with its positions and with its positions'
	 * position request mappings.
	 * 
	 * @param job
	 *            the job to duplicate
	 * @return the new job object
	 * @throws UniqueValidationException
	 */
	Job duplicateJob(Job job) throws UniqueValidationException;

	/**
	 * Gets all active jobs for resource.
	 * 
	 * @param resourceId
	 *            resource id
	 * @return A list of jobs that are active (in the last 24h)
	 */
	List<Job> getActiveJobsByResource(Long resourceId);

	/**
	 * List all active projects.
	 * 
	 * @return project list
	 */
	List<Project> getAllActiveProjectList();

	/**
	 * @param projectId
	 * @return 	{@code true} 	if a {@link Project} has min. one {@link Job} in {@link JobStatus} 'beauftragt'
	 * 							or min. one {@link Position} with an active resource ({@link PosResourceStatus})
	 * 			{@code false} 	otherwise
	 */
	boolean isProjectActive(Long projectId);
	
	/**
	 * List all positions for job.
	 * 
	 * @param jobId
	 *            - job id
	 * @return a list of {@link Position}
	 */
	List<Position> getAllPositionsByJob(Long jobId);

	/**
	 * Lists all costs for job.
	 * 
	 * @param jobId
	 *            jobId
	 * @return a list of {@link Cost}
	 */
	List<Cost> getCostsByJob(Long jobId);

	/**
	 * Gets a {@link Customer} by given id.
	 * 
	 * @param id
	 *            - customer id
	 * @return customer
	 */
	Customer getCustomer(Long id);

	/**
	 * List all customers.
	 * 
	 * @return a customer list
	 */
	List<Customer> getCustomerList();

	/**
	 * Gets a {@link Job} by id.
	 * 
	 * @param id
	 *            - job Id
	 * @return job
	 */
	Job getJob(Long id);

	/**
     * Returns all recorded changes of the {@link Job} with the given id.
     * 
     * @param jobPk
     * @return
     */
    List<ChangeInfo> getJobChangeInfo(Long jobPk);

	/**
	 * List all jobs.
	 * 
	 * @return job list
	 */
	List<Job> getJobList();

	/**
	 * List jobs to manage.
	 * 
	 * @return list of jobs to manage
	 */
	List<Job> getJobListToManage();

	/**
	 * Gets all jobs for selected project.
	 * 
	 * @param projectId
	 *            Project Id
	 * @return A list of jobs for the project.
	 */
	List<Job> getJobsByProject(Long projectId);

	/**
	 * Lists all jobs for a selected resource.
	 * 
	 * @param resourceId
	 *            resource id
	 * @return A list of jobs
	 */
	List<Job> getJobsByResource(Long resourceId);

	/**
	 * Get JobStatus for given Id.
	 * 
	 * @param id
	 *            id
	 * @return a {@link JobStatus}
	 */
	JobStatus getJobStatus(Long id);

	/**
	 * List of {@link JobStatus}.
	 * 
	 * @return job status list
	 */
	List<JobStatus> getJobStatusList();

	/**
	 * Get JobType for given Id.
	 * 
	 * @param id
	 *            id
	 * @return a {@link JobType}
	 */
	JobType getJobType(Long id);

	/**
	 * List of {@link JobType}.
	 * 
	 * @return job type list
	 */
	List<JobType> getJobTypeList();

	/**
	 * Gets a position by position Id.
	 * 
	 * @param id
	 *            POsition Id.
	 * @return Position object.
	 */
	Position getPosition(Long id);

	/**
     * Returns all recorded changes of the {@link Position} with the given id.
     * 
     * @param positionId
     * @return
     */
    List<ChangeInfo> getPositionChangeInfo(Long positionId);

	/**
	 * Returns positions for job, status or/and resource (position-resource mapping has active status, 
	 * and was active in the last 24h)
	 * 
	 * @param jobId			jobId
	 * @param statusId		statusId
	 * @param resourceId	resourceId
	 * 
	 * @return a list of {@link Position}
	 */
	List<Position> getPositionsByJob(Long jobId, Long statusId, Long resourceId);

	/**
	 * Gets {@link PositionStatus}.
	 * 
	 * @param id
	 *            position id
	 * @return {@link PositionStatus}
	 */
	PositionStatus getPositionStatus(Long id);

	/**
	 * Gets {@link PosResourceMapping} by id.
	 * 
	 * @param id
	 *            PosResourceMapping id
	 * @return PosResourceMapping object
	 */
	PosResourceMapping getPosResourceMapping(Long id);

	/**
	 * Get position resource mappings by position id.
	 * 
	 * @param positionId
	 *            positionId
	 * @return a list of position mappings
	 */
	List<PosResourceMapping> getPosResourceMappingsByPosition(Long positionId);

	/**
	 * Gets {@link PosResourceMapping} by Position id and Resource id.
	 * 
	 * @param positionId
	 *            {@link Position} id
	 * @param resourceId
	 *            {@link Resource} id
	 * @return {@link PosResourceMapping} object
	 */
	PosResourceMapping getPosResourceMappingsByPositionAndResource(Long positionId, Long resourceId);

	/**
	 * Gets a {@link Project} by given id.
	 * 
	 * @param id
	 *            - project id
	 * @return Project
	 */
	Project getProject(Long id);

	/**
     * Returns all recorded changes of the {@link Project} with the given id.
     * 
     * @param id
     * @return
     */
    List<ChangeInfo> getProjectChangeInfo(Long id);

	/**
	 * List all projects
	 * 
	 * @return project list
	 */
	List<Project> getProjectList();

	/**
	 * Gets projects for a customer.
	 * 
	 * @param customerId
	 *            customer id
	 * @return a list of projects
	 */
	List<Project> getProjectsByCustomer(Long customerId);

	/**
     * Returns {@code true} if the job is deletable.
     * 
     * @param  jobId
     * @return
     */
    boolean isDeletableJob(Long jobId);

	/**
     * Returns {@code true} if the position is deletable.
     * 
     * @param  positionId
     * @return
     */
    boolean isDeletablePosition(Long positionId);

	/**
     * Returns {@code true} if the project is deletable.
     * 
     * @param  projectId
     * @return
     */
    boolean isDeletableProject(Long projectId);

	/**
	 * List jobs with jobStatus ordered.
	 * 
	 * @return List of Jobs
	 */
	List<Job> listOrderedJobs();

	/**
	 * List projects by Resource id.
	 * 
	 * @param resId
	 *            Resource id
	 * @return List<Project>
	 */
	List<Project> listProjectByResource(Long resId);
	
	/**
	 * List of project where the user is responsible.
	 * 
	 * @param resId
	 *            - resource id
	 * @return projects list
	 */
	List<Project> listProjectByResponsibleResource(Long resId);
	
	/**
	 * List of {@link PositionStatus}.
	 * 
	 * @return position status list
	 */
	List<PositionStatus> positionStatusList();

	/**
	 * Get Position Resource Status List.
	 * 
	 * @return List<PosResourceStatus>
	 */
	List<PosResourceStatus> posResourceStatusList();

	/**
	 * List filtered {@link Customer}.
	 * 
	 * @param filter
	 *            CustomerFilter
	 * @return List<Customer>
	 */
	FilterResult<Customer> searchCustomers(BaseFilter filter);

	/**
	 * List filtered {@link Job}.
	 * 
	 * @param filter
	 *            JobFilter
	 * @return List<Job>
	 */
	FilterResult<Job> searchJob(JobFilter filter);

    /**
	 * @param filter
	 *            filter
	 * @return
	 */
	FilterResult<Position> searchMyPosition(PositionFilter filter);

    /**
	 * List filtered {@link Position}.
	 * 
	 * @param filter
	 *            PositionFilter
	 * @return List<Position>
	 */
	FilterResult<Position> searchPosition(PositionFilter filter);

    /**
	 * List filtered {@link PosResourceMapping}.
	 * 
	 * @param filter
	 *            PosResourceMappingFilter
	 * @return FilterResult object
	 */
	FilterResult<PosResourceMapping> searchPosResourceMapping(PosResourceMappingFilter filter);

    /**
	 * List filtered {@link Project}.
	 * 
	 * @param filter
	 *            ProjectFilter
	 * @return List<Project>
	 */
	FilterResult<Project> searchProject(ProjectFilter filter);

    /**
	 * Update a{@link Customer}.
	 * 
	 * @param customer
	 *            - customer to update
	 */
	void updateCustomer(Customer customer);

    /**
	 * Update a {@link Job}.
	 * 
	 * @param job
	 *            - job to update
	 * @throws UniqueValidationException
	 */
	void updateJob(Job job) throws UniqueValidationException;

    /**
	 * Update a {@link Position}.
	 * 
	 * @param position
	 *            - position to update
	 */
	void updatePosition(Position position);
    
    /**
	 * Updates a {@link PosResourceMapping}.
	 * 
	 * @param posResourceMapping
	 *            {@link PosResourceMapping} object
	 */
	void updatePosResourceMapping(PosResourceMapping posResourceMapping);
    
    
    /**
	 * Update a {@link Project}.
	 * 
	 * @param project
	 *            - project to update
	 * @throws UniqueValidationException
	 */
	void updateProject(Project project) throws UniqueValidationException;

	/**
	 * Returns various aggregates / stats about the job
	 * 
	 * @param jobId
	 * @return
	 */
	JobStats getJobStats(Long jobId);

	/**
	 * Returns various aggregates / stats about the positions
	 * 
	 * @param positionId
	 * @return
	 */
	JobStats getPositionStats(Long positionId);
    
}