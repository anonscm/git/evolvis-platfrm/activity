/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Activity Error Model.
 *
 */
public class ImportResultModel implements Serializable {

	private static final long serialVersionUID = -6418103570376222285L;

	private Integer row;

	private static Integer lines;

	private static Integer linesSuccess;

	private static Integer linesFailure;

	private List<String> errorMessages = new ArrayList<String>();

	/**
	 * Constructor.
	 */
	public ImportResultModel() {

	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public static Integer getLines() {
		return ImportResultModel.lines;
	}

	public static void setLines(Integer lines) {
        ImportResultModel.lines = lines;
	}

	public static Integer getLinesSuccess() {
		return ImportResultModel.linesSuccess;
	}

	public static void setLinesSuccess(Integer linesSuccess) {
        ImportResultModel.linesSuccess = linesSuccess;
	}

	public static Integer getLinesFailure() {
        return ImportResultModel.linesFailure;
	}

	public static void setLinesFailure(Integer linesFailure) {
        ImportResultModel.linesFailure = linesFailure;
	}

}
