/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.exception.PermissionException;

/**
 * PermissionService provides CRUD methods for {@link Permission} and {@link Role} entities. Furthermore this service
 * class has methods to handle and to check permissions of {@link Resource}'s.
 */
public interface PermissionService {

    /**
     * Assigns an array of permissions to a resource.
     * 
     * @param resId
     *            primary key of resource to assign the permissions
     * @param permissionIds
     *            primary keys of permissions to assign
     * @return a resource object with all of his assigned permissions
     */
    Resource addPermissionsToResource(@NotNull Long resId, @NotNull Long[] permissionIds);

    /**
     * Assigns an array of permissions to a role.
     * 
     * @param roleId
     *            primary key of role to assign the permissions
     * @param permissionIds
     *            primary keys of permissions to assign
     * @return a role object with all of his assigned permissions
     */
    Role addPermissionsToRole(@NotNull Long roleId, @NotNull Long[] permissionIds);

    /**
     * Assigns an array of resources to a role.
     * 
     * @param roleId
     *            primary key of role to assign the resources
     * @param resIds
     *            primary keys of resources to assign
     * @return a role object with all of his assigned resources
     */
    Role addResourcesToRole(@NotNull Long roleId, @NotNull Long[] resIds);

    /**
     * Assigns a resource to a role.
     * 
     * @param roleId
     *            primary key of role to assign the resource
     * @param userId
     *            primary keys of resource to assign
     * @return a role object with all of his assigned resources
     */
    Role addResourceToRole(@NotNull Long roleId, @NotNull Long userId);

    /**
     * Checks if the resource with given primary key has the permission with given action identifier assigned. The
     * permission may be assigned either by direct-assignment or by role-assignment.
     * 
     * @param resourceId
     *            primary key of resource
     * @param actionId
     *            action identifier of permission
     * @throws PermissionException
     *             when resource has no permission with given action identifier
     */
    void checkAndThrowPermission(@NotNull Long resourceId, @NotNull String... actionId);

    /**
     * Checks if the resource with given primary key has at least one permission with given action identifier(s)
     * assigned. The permission may be assigned either by direct-assignment or by role-assignment.
     * 
     * @param resourceId
     *            primary key of resource
     * @param actionId
     *            action identifier of permission(s)
     * @return <code>true</code> if user has at least one of given permissions
     */
    boolean checkPermission(@NotNull Long resourceId, @NotNull String... actionId);

    /**
     * Returns a list of all existence roles.
     * 
     * @return List of all roles
     */
    List<Role> getAllRoles();

    /**
     * Get resources with a certain permission
     * 
     * @param permisisonId
     *            {@link de.tarent.activity.service.util.PermissionIds}
     * @return
     */
    List<Resource> getResourcesByPermission(@NotNull String permissionId);

    /**
     * Returns a role with given primary key from database.
     * 
     * @param pk
     *            primary key of role
     * @return Role with given primary key, or <code>null</code> if no role with key exist
     */
    Role getRole(@NotNull Long pk);

    /**
     * Detaches an array of permissions from a resource.
     * 
     * @param resId
     *            primary key of resource to detach
     * @param permissionIds
     *            primary keys of permissions to detach from resource
     * @return a resource object with all of his assigned permissions
     */
    Resource removePermissionsFromResource(@NotNull Long resId, @NotNull Long[] permissionIds);

    /**
     * Detaches an array of permissions from a role.
     * 
     * @param roleId
     *            primary key of role to detach
     * @param permissionIds
     *            primary keys of permissions to detach from role
     * @return a role object with all of his assigned permissions
     */
    Role removePermissionsFromRole(@NotNull Long roleId, @NotNull Long[] permissionIds);

    /**
     * Detaches a resource from a role.
     * 
     * @param roleId
     *            primary key of role to detach
     * @param resId
     *            primary key of resource to detach from role
     * @return
     */
    Role removeResourceFromRole(@NotNull Long roleId, @NotNull Long resId);

    /**
     * Detaches an array of resources from a role.
     * 
     * @param roleId
     *            primary key of role to detach
     * @param resIds
     *            primary keys of resources to detach from role
     * @return a role object with all of his assigned resources
     */
    Role removeResourcesFromRole(@NotNull Long roleId, @NotNull Long[] resIds);

    /**
     * Search permissions by filter
     * 
     * @param filter
     * @return
     */
    FilterResult<Permission> searchPermissions(@NotNull PermissionFilter filter);
}
