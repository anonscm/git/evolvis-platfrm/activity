/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.util.List;

import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.CostType;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.InvoiceFilter;

/**
 * Service for Cost.
 *
 */
public interface CostService {
	
	/**
	 * Gets a cost by cost ID.
	 * 
	 * @param costId Cost ID
	 * @return Cost object
	 */
	Cost getCost(Long costId);
	
	
	/**
	 * List filtered costs. 
	 * 
	 * @param filter CostFilter
	 * @return FilterResult<Cost>
	 */
	FilterResult<Cost> searchCost(CostFilter filter);
	
	/**
	 * List CostType.
	 * 
	 * @return List<CostType>
	 */
	List<CostType> getCostTypeList();
	
	/**
	 * Add a cost.
	 * 
	 * @param cost cost
	 * @return Cost ID
	 */
	Long addCost(Cost cost);
	
	/**
	 * Cost update.
	 * 
	 * @param cost cost
	 */
	void updateCost(Cost cost);
	
	/**
	 * List filtered {@link Invoice}.
	 * 
	 * @param invoiceFilter
	 *            InvoiceFilter
	 * @return List<Invoice>
	 */
	FilterResult<Invoice> searchInvoice(InvoiceFilter invoiceFilter);
	
	/**
	 * Gets a {@link Invoice} by given id.
	 * 
	 * @param id
	 *            - invoice id
	 * @return Invoice
	 */
	Invoice getInvoice(Long id);

	/**
	 * Updates an {@link Invoice}.
	 * 
	 * @param invoice invoice to update
	 */
	void updateInvoice(Invoice invoice);

	/**
	 * Adds an {@link Invoice}.
	 * 
	 * @param invoice invoice to add
	 * @return id of created invoice
	 */
	Long addInvoice(Invoice invoice);
	
	/**
	 * Lists all invoices for job.
	 * 
	 * @param jobId jobId
	 * @return a list of {@link Invoice}
	 */
	List<Invoice> getInvoicesByJob(Long jobId);
	
	/**
	 * Invoice list by given project id.
	 * 
	 * @param projectId - project id
	 * @return list of invoices
	 */
	List<Invoice> getProjectInvoices(Long projectId);
	
	
	/**
	 * List filtered {@link Invoice}.
	 * 
	 * @param invoiceFilter
	 *            InvoiceFilter
	 * @return List<Invoice>
	 */
	FilterResult<Invoice> searchProjectInvoice(InvoiceFilter invoiceFilter);
	
	/**
	 * Gets a {@link ProjectInvoice} by given id.
	 * 
	 * @param id
	 *            - invoice id
	 * @return Invoice
	 */
	Invoice getProjectInvoice(Long id);
	
	void deleteInvoice(Long id);

}
