/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service.util;

import de.tarent.activity.domain.Permission;

/**
 * Class PermissionIds mainly consists of constants which contains String values
 * for {@link Permission} action id's.
 */
public class PermissionIds {

	public static final String ACTIVITY_ADD = "Activity.ADD";

	public static final String ACTIVITY_DELETE = "Activity.DELETE";

	public static final String ACTIVITY_DELETE_ALL = "Activity.DELETE_ALL";

	public static final String ACTIVITY_EDIT = "Activity.EDIT";
	
	public static final String ACTIVITY_EXPORT = "Activity.EXPORT";

	public static final String ACTIVITY_EXPORT_ALL = "Activity.EXPORT_ALL";

	public static final String ACTIVITY_IMPORT_ALL = "Activity.IMPORT_ALL";

	public static final String ACTIVITY_REBOOK = "Activity.REBOOK";

	public static final String ACTIVITY_REBOOK_ALL = "Activity.REBOOK_ALL";

	public static final String ACTIVITY_VIEW = "Activity.VIEW";

	public static final String ACTIVITY_VIEW_ALL = "Activity.VIEW_ALL";

	public static final String ACTIVITY_VIEW_BY_DIVISION = "Activity.VIEW_BY_DIVISION";

	public static final String ACTIVITY_VIEW_BY_PROJECT = "Activity.VIEW_BY_PROJECT";

	public static final String ADMIN_EDIT_SETTINGS = "Admin.EDIT_SETTINGS";

	public static final String ADMIN_IMPORT_LDAP = "Admin.IMPORT_LDAP";

	public static final String ADMIN_VIEW_SETTINGS = "Admin.VIEW_SETTINGS";
	
	public static final String ADMIN_EDIT_PERMISSIONS = "Admin.EDIT_PERMISSIONS";

	public static final String INVOICE_ADD_BY_JOB = "Invoice.ADD_BY_JOB";

	public static final String INVOICE_ADD_BY_PROJECT = "Invoice.ADD_BY_PROJECT";

	public static final String INVOICE_DELETE_BY_JOB = "Invoice.DELETE_BY_JOB";

	public static final String INVOICE_DELETE_BY_PROJECT = "Invoice.DELETE_BY_PROJECT";

	public static final String INVOICE_EDIT_ALL = "Invoice.EDIT_ALL";
	
	public static final String INVOICE_EDIT_BY_JOB = "Invoice.EDIT_BY_JOB";

	public static final String INVOICE_EDIT_BY_PROJECT = "Invoice.EDIT_BY_PROJECT";

	public static final String INVOICE_VIEW_ALL = "Invoice.VIEW_ALL";
	
	public static final String INVOICE_VIEW_ALL_DETAIL = "Invoice.VIEW_ALL_DETAIL";
	
	public static final String INVOICE_VIEW_BY_JOB = "Invoice.VIEW_BY_JOB";

	public static final String INVOICE_VIEW_BY_PROJECT = "Invoice.VIEW_BY_PROJECT";
	
	public static final String COST_ADD_ALL = "Cost.ADD_ALL";
	
	public static final String COST_EDIT_ALL = "Cost.EDIT_ALL";
	
	public static final String COST_VIEW_ALL = "Cost.VIEW_ALL";
	
	public static final String COST_VIEW_ALL_DETAIL = "Cost.VIEW_ALL_DETAIL";
	
	public static final String COST_VIEW_BY_PROJECT = "Cost.VIEW_BY_PROJECT";
	
	public static final String COST_EDIT_BY_PROJECT = "Cost.EDIT_BY_PROJECT";

	public static final String CUSTOMER_ADD_ALL = "Customer.ADD_ALL";

	public static final String CUSTOMER_DELETE_ALL = "Customer.DELETE_ALL";

	public static final String CUSTOMER_EDIT_ALL = "Customer.EDIT_ALL";

	public static final String CUSTOMER_VIEW_ALL = "Customer.VIEW_ALL";

	public static final String JOB_ADD = "Job.ADD";

	public static final String JOB_DELETE = "Job.DELETE";

	public static final String JOB_DELETE_ALL = "Job.DELETE_ALL";

	public static final String JOB_DELETE_BY_DIVISION = "Job.DELETE_BY_DIVISION";

	public static final String JOB_EDIT = "Job.EDIT";

	public static final String JOB_EDIT_ALL = "Job.EDIT_ALL";

	public static final String JOB_EDIT_BY_DIVISION = "Job.EDIT_BY_DIVISION";

	public static final String JOB_VIEW = "Job.VIEW";

	public static final String JOB_VIEW_ALL = "Job.VIEW_ALL";

	public static final String JOB_VIEW_BY_DIVISION = "Job.VIEW_BY_DIVISION";
	
	public static final String JOB_VIEW_MANAGER = "Job.VIEW_MANAGER";

	public static final String LOSS_ADD = "Loss.ADD";

	public static final String LOSS_AUTHORIZE = "Loss.AUTHORIZE";

	public static final String LOSS_DELETE = "Loss.DELETE";

	public static final String LOSS_DELETE_ALL = "Loss.DELETE_ALL";

	public static final String LOSS_EDIT = "Loss.EDIT";

	public static final String LOSS_EDIT_ALL = "Loss.EDIT_ALL";

	public static final String LOSS_EXPORT_ALL = "Loss.EXPORT_ALL";

	public static final String LOSS_VIEW = "Loss.VIEW";

	public static final String LOSS_VIEW_ALL = "Loss.VIEW_ALL";

	public static final String LOSS_VIEW_BY_DIVISION = "Loss.VIEW_BY_DIVISION";

	public static final String LOSS_VIEW_BY_PROJECT = "Loss.VIEW_BY_PROJECT";

	public static final String OVERTIME_ADD = "Overtime.ADD";

	public static final String OVERTIME_ADD_ALL = "Overtime.ADD_ALL";

	public static final String OVERTIME_DELETE = "Overtime.DELETE";

	public static final String OVERTIME_DELETE_ALL = "Overtime.DELETE_ALL";

	public static final String OVERTIME_EDIT = "Overtime.EDIT";

	public static final String OVERTIME_EDIT_ALL = "Overtime.EDIT_ALL";

	public static final String OVERTIME_EXPORT_BY_DIVISION = "Overtime.EXPORT_BY_DIVISION";

	public static final String OVERTIME_EXPORT_BY_PROJECT = "Overtime.EXPORT_BY_PROJECT";

	public static final String OVERTIME_VIEW = "Overtime.VIEW";

	public static final String OVERTIME_VIEW_ALL = "Overtime.VIEW_ALL";

	public static final String OVERTIME_VIEW_BY_DIVISION = "Overtime.VIEW_BY_DIVISION";

	public static final String OVERTIME_VIEW_BY_PROJECT = "Overtime.VIEW_BY_PROJECT";

	public static final String POSITION_ADD = "Position.ADD";

	public static final String POSITION_DELETE = "Position.DELETE";

	public static final String POSITION_DELETE_ALL = "Position.DELETE_ALL";

	public static final String POSITION_DELETE_BY_DIVISION = "Position.DELETE_BY_DIVISION";

	public static final String POSITION_EDIT = "Position.EDIT";

	public static final String POSITION_EDIT_ALL = "Position.EDIT_ALL";

	public static final String POSITION_EDIT_BY_DIVISION = "Position.EDIT_BY_DIVISION";

	public static final String POSITION_VIEW = "Position.VIEW";

	public static final String POSITION_VIEW_ALL = "Position.VIEW_ALL";

	public static final String POSITION_VIEW_BY_DIVISION = "Position.VIEW_BY_DIVISION";
	
	public static final String POSITION_VIEW_MANAGER = "Position.VIEW_MANAGER";

	public static final String PROJECT_ADD = "Project.ADD";

	public static final String PROJECT_DELETE = "Project.DELETE";

	public static final String PROJECT_DELETE_ALL = "Project.DELETE_ALL";

	public static final String PROJECT_DELETE_BY_DIVISION = "Project.DELETE_BY_DIVISION";

	public static final String PROJECT_EDIT = "Project.EDIT";

	public static final String PROJECT_EDIT_ALL = "Project.EDIT_ALL";

	public static final String PROJECT_EDIT_BY_DIVISION = "Project.EDIT_BY_DIVISION";

	public static final String PROJECT_VIEW = "Project.VIEW";

	public static final String PROJECT_VIEW_ALL = "Project.VIEW_ALL";

	public static final String PROJECT_VIEW_BY_DIVISION = "Project.VIEW_BY_DIVISION";
	
	public static final String PROJECT_VIEW_MANAGER = "Project.VIEW_MANAGER";

	public static final String RESOURCE_ADD_ALL = "Resource.ADD_ALL";

	public static final String RESOURCE_APPEND_ALL = "Resource.APPEND_ALL";

	public static final String RESOURCE_CONTROLLER = "Resource.CONTROLLER";
	
	public static final String RESOURCE_DELETE_ALL = "Resource.DELETE_ALL";

	public static final String RESOURCE_DETACH_ALL = "Resource.DETACH_ALL";

	public static final String RESOURCE_EDIT_ALL = "Resource.EDIT_ALL";

	public static final String RESOURCE_LOCK_ALL = "Resource.LOCK_ALL";

	public static final String RESOURCE_VIEW_ALL = "Resource.VIEW_ALL";

	public static final String SKILL_ADD = "Skill.ADD";

	public static final String SKILL_DELETE = "Skill.DELETE";

	public static final String SKILL_EDIT = "Skill.EDIT";

	public static final String SKILL_VIEW = "Skill.VIEW";

	public static final String SKILL_VIEW_ALL = "Skill.VIEW_ALL";
}
