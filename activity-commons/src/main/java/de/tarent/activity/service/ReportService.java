/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import de.tarent.activity.domain.Report;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.exception.UniqueValidationException;

/**
 * 
 * Service for Report entity.
 *
 */
public interface ReportService {

	/**
	 * 
	 * @param report report
	 * @return id of the new added report
	 * @throws UniqueValidationException UniqueValidationException
	 */
	Long addReport(Report report) throws UniqueValidationException;

	/**
	 * 
	 * @param id id
	 * @return report with the given id
	 */
	Report getReport(Long id);

	/**
	 * This method updates the given report.
	 * 
	 * @param report report
	 * @throws UniqueValidationException UniqueValidationException
	 */
	void updateReport(Report report) throws UniqueValidationException;

	/**
	 * This method deletes the report with the given id.
	 * 
	 * @param id id
	 */
	void deleteReport(Long id);

	/**
	 * 
	 * @param filter filter
	 * @return a FilterResult<Report> containg the list of reports
	 */
	FilterResult<Report> searchReports(BaseFilter filter);

}
