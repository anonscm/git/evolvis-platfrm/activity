/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.LossStatus;
import de.tarent.activity.domain.LossType;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.Timer;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.domain.filter.OvertimeFilter;
import de.tarent.activity.exception.UniqueValidationException;

/**
 * Service interface to define service contracts.
 */
public interface ActivityService {

	/**
	 * Add a new {@link Activity}.
	 *
	 * @param activity
	 *            - activity to be added
	 * @return activity id
	 * @throws UniqueValidationException
	 *             when an unique constraint is violated
	 */
	Long addActivity(Activity activity) throws UniqueValidationException;

	/**
	 * Gets an {@link Activity} by given id.
	 *
	 * @param id
	 *            - activity id
	 * @return activity
	 */
	Activity getActivity(Long id);

	/**
	 * Update an {@link Activity}.
	 *
	 * @param activity
	 *            - activity to update
	 * @throws UniqueValidationException
	 *             when an unique constraint is violated
	 */
	void updateActivity(Activity activity) throws UniqueValidationException;

	/**
	 * Delete an {@link Activity}.
	 *
	 * @param id
	 *            Activity id
	 */
	void deleteActivity(Long id);

	/**
	 * Delete an {@link Activity} and the associated delete request (if any).
	 *
	 * @param id
	 *            Activity id
	 */
	void deleteActivityAndRequest(Long id);

	/**
	 * List filtered activity.
	 *
	 * @param filter
	 *            ActivityFilter
	 * @return List<Activity>
	 */
	FilterResult<Activity> searchActivities(ActivityFilter filter);

	/**
	 * Get total activity hours.
	 *
	 * @param filter
	 *            ActivityFilter
	 * @return total activity hours
	 */
	BigDecimal getActivitiesHoursSum(ActivityFilter filter);

	/**
	 * Add a new {@link Overtime}.
	 *
	 * @param overtime
	 *            - overtime to be added
	 * @return overtime id
	 */
	Long addOvertime(Overtime overtime);

	/**
	 * Get {@link Overtime} by overtime id.
	 *
	 * @param id
	 *            - overtime id
	 * @return overtime
	 */
	Overtime getOvertime(Long id);

/**
	 * Update an {@link Overtime.
	 *
	 * @param overtime overtime
	 *
	 */
	void updateOvertime(Overtime overtime);

	/**
	 * Delete an {@link Overtime}.
	 *
	 * @param id
	 *            - overtime id
	 */
	void deleteOvertime(Long id);

	/**
	 * Delete an {@link Overtime} and the associated delete request (if any).
	 *
	 * @param id
	 *            - overtime id
	 */
	void deleteOvertimeAndRequest(Long id);

	/**
	 * List filtered {@link Overtime}.
	 *
	 * @param filter
	 *            - OvertimeFilter
	 * @return List<Overtime>
	 */
	FilterResult<Overtime> searchOvertimes(OvertimeFilter filter);

	/**
	 * Get total overtime hours.
	 *
	 * @param filter
	 *            - OvertimeFilter
	 * @return total overtime hour
	 */
	BigDecimal getOvertimeHoursSumByFilter(OvertimeFilter filter);

	/**
	 * Gets timer by resource.
	 *
	 * @param resId
	 *            Resource Id
	 * @return List<Timers>
	 */
	Timer getTimerByResource(Long resId);

    /**
     * Gets timer by id.
     *
     * @param resId
     *            Resource Id
     * @return List<Timers>
     */
    Timer getTimerById(Long id);

	/**
	 * @param id
	 *            id
	 * @return sum of all overtime
	 */
	BigDecimal getAllOvertimeSum(Long id);

	/**
	 * Delete a timer.
	 *
	 * @param id
	 *            Timer Id
	 */
	void deleteTimer(Long id);

	/**
	 * Add a timer.
	 *
	 * @param timer
	 *            Timer object
	 * @return Timer Id.
	 */
	Long addTimer(Timer timer);

	/**
	 * Update a timer.
	 *
	 * @param timer
	 *            Timer to update
	 */
	void updateTimer(Timer timer);

	/**
	 * Add a new {@link Loss}.
	 *
	 * @param loss
	 *            - loss to be added
	 * @return loss id
	 */
	Long addLoss(Loss loss);

	/**
	 * Update {@link Loss}.
	 *
	 * @param loss
	 *            - loss to update
	 */
	void updateLoss(Loss loss);

	/**
	 * Delete a {@link Loss}.
	 *
	 * @param id
	 *            - loss id
	 */
	void deleteLoss(Long id);

	/**
	 * Delete a {@link Loss} and the associated delete request.
	 *
	 * @param id
	 *            - loss id
	 */
	void deleteLossAndRequest(Long id);

	/**
	 * List filtered loss.
	 *
	 * @param filter
	 *            LossFilter
	 * @return List<Loss>
	 */
	FilterResult<Loss> searchLoss(LossFilter filter);

	/**
	 * Gets loss by given id
	 *
	 * @param id
	 *            - loss id
	 * @return loss
	 */
	Loss getLoss(Long id);

	/**
	 * Get current loss of resource from selected project.
	 *
	 * @param date
	 *            - date
	 * @param projectPk
	 *            - project id
	 * @return List<Loss>
	 */
	List<Loss> getCurrentLossByProject(Date date, Long projectPk);

	/**
	 * Get future loss of resource from selected project.
	 *
	 * @param date
	 *            - date
	 * @param projectPk
	 *            - project id
	 * @return List<Loss>
	 */
	List<Loss> getFutureLossByProject(Date date, Long projectPk);

	/**
	 * Get current loss form selected location.
	 *
	 * @param date
	 *            - date
	 * @param location
	 *            - location
	 * @return List<Loss>
	 */
	List<Loss> getCurrentLossByLocation(Date date, String[] location);

	/**
	 * @param resourceId
	 *            resourceId
	 * @param year
	 *            year
	 * @return
	 */
	BigDecimal getLossDaysFromYear(Long resourceId, Integer year);

	/**
	 * List of existing loss type.
	 *
	 * @return - list of loss type
	 */
	List<LossType> lossTypeList();

	/**
	 * Get LossType by given id.
	 *
	 * @param pk
	 *            - loss type id
	 * @return loss type
	 */
	LossType getLossType(Long pk);

	/**
	 * Get LossStatus by given id.
	 *
	 * @param id
	 *            - loss status id
	 * @return loss status
	 */
	LossStatus getLossStatus(Long id);

	/**
	 * List of existing loss status.
	 *
	 * @return list of loss status
	 */
	List<LossStatus> lossStatusList();

	/**
	 * List of absence colleagues for a given resource.
	 *
	 * @param resourceId
	 *            - resource id
	 * @return - loss list
	 */
	List<Loss> colleaguesLoss(Long resourceId);

	/**
	 * List of resources form a given project that are absence now.
	 *
	 * @param projectId
	 *            - project id
	 * @return loss list
	 */
	List<Loss> projectResourceLossNow(Long projectId);

	/**
	 * List of resources form a given project that will be absent in the future.
	 *
	 * @param projectId
	 *            - project id
	 * @return loss list
	 */
	List<Loss> projectResourceLossFuture(Long projectId);

	/**
	 * @param positionId
	 *            positionId
	 * @return List<Activity>
	 */
	List<Activity> listActivitiesByPosition(Long positionId);

	/**
	 * Calculate sum of worked hours between start date and end date.
	 *
	 * @param resourceId
	 *            resource id
	 * @param startDate
	 *            start date
	 * @param endDate
	 *            end date
	 * @return total hours between start date and end date
	 */
	BigDecimal getHoursFromInterval(Long resourceId, Date startDate, Date endDate);
}
