/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.Project;
import javax.mail.MessagingException;

/**
 * MailService.
 * 
 */
public interface MailService {

	/**
	 * @param customer
	 *            customer
	 * @throws MessagingException
	 *             Exception
	 */
	void sendCustomerMail(Customer customer) throws MessagingException;

	/**
	 * @param loss
	 *            loss
	 * @throws MessagingException
	 *             Exception
	 */
	void sendHolidayApprovedMail(Loss loss) throws MessagingException;

	/**
	 * @param loss
	 *            loss
	 * @throws MessagingException
	 *             Exception
	 */
	void sendHolidayMail(Loss loss) throws MessagingException;

	/**
	 * @param loss
	 *            loss
	 * @throws MessagingException
	 *             Exception
	 */
	void sendHolidayRejectedMail(Loss loss) throws MessagingException;

	/**
	 * @param job
	 *            job
	 * @throws MessagingException
	 *             Exception
	 */
	void sendJobMail(Job job) throws MessagingException;

	/**
	 * @param project
	 *            project
	 * @throws MessagingException
	 *             Exception
	 */
	void sendProjectMail(Project project) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableAddHolidayMail(Loss loss) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableApprovedByManagerLossMail(Loss loss) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableApprovedByControllingLossMail(Loss loss) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableDeniedByManagerLossMail(Loss loss) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableDeniedByControllingLossMail(Loss loss) throws MessagingException;
	
	/**
	 * @param loss
	 * @throws MessagingException
	 */
	void sendConfigurableAddAbsenceMail(Loss loss) throws MessagingException;
	
	/**
	 * @param invoice
	 * @throws MessagingException
	 */
	void sendConfigurableAddJobInvoiceMail(Invoice invoice) throws MessagingException;
	
	/**
	 * @param invoice
	 * @throws MessagingException
	 */
	void sendConfigurableEditJobInvoiceMail(Invoice invoice) throws MessagingException;
	
	/**
	 * @param invoice
	 * @throws MessagingException
	 */
	void sendConfigurableAddProjectInvoiceMail(Invoice invoice) throws MessagingException;
	
	/**
	 * @param invoice
	 * @throws MessagingException
	 */
	void sendConfigurableEditProjectInvoiceMail(Invoice invoice) throws MessagingException;
	
	/**
	 * @param customer
	 * @throws MessagingException
	 */
	void sendConfigurableAddCustomerMail(Customer customer) throws MessagingException;
	
	/**
	 * @param job
	 * @throws MessagingException
	 */
	void sendConfigurableAddJobMail(Job job) throws MessagingException;
	
	/**
	 * @param project
	 * @throws MessagingException
	 */
	void sendConfigurableAddProjectMail(Project project) throws MessagingException;
}
