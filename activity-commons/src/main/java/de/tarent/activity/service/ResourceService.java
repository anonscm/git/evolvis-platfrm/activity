/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.util.List;

import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Employment;
import de.tarent.activity.domain.FunctionType;
import de.tarent.activity.domain.PosResourceStatus;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceFTypeMapping;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.SkillsDef;
import de.tarent.activity.domain.VresActiveProjectMapping;
import de.tarent.activity.domain.filter.AllSkillsFilter;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.domain.filter.SkillsFilter;
import de.tarent.activity.exception.UniqueValidationException;

public interface ResourceService {
	
	/**
	 * Delete request status when first created. 
	 */
	static final String DELETE_REQUEST_STATUS_OPEN = "open";
	/**
	 * Delete request status when delete request is not accepted. 
	 */
	static final String DELETE_REQUEST_STATUS_CANCELED = "cancelled";
	/**
	 * Delete request status when delete request is accepted.
	 */
	static final String DELETE_REQUEST_STATUS_DELETED = "deleted";
	/**
	 * Delete request type for activity.
	 */
	static final String DELETE_REQUEST_TYPE_ACTIVITY = "activity";
	/**
	 * Delete request type for loss.
	 */
	static final String DELETE_REQUEST_TYPE_LOSS = "loss";
	/**
	 * Delete request type for overtime.
	 */
	static final String DELETE_REQUEST_TYPE_OVERTIME = "overtime";

	/**
	 * List all resources.
	 * 
	 * @return List<Resource>
	 */
	List<Resource> getResourceList();

	/**
	 * Get a Resource by username.
	 * 
	 * @param username Resource username
	 * @return null if the database does not contains a Resource by that
	 *         username, a Resource if database lookup is
	 *         successful
	 */
	Resource loadResource(String username);
	
	/**Gets a Resource by ID.
	 * @param resourceId Resource ID
	 * @param fetchRoles if true the returning resource has set its assigned roles
	 * @param fetchPermission if true the returning resource has set its assigned permissions
	 * @return Resource
	 */
	Resource getResource(Long resourceId, boolean fetchRoles, boolean fetchPermission);
	
	/**List managers by Resource ID.
	 * @param resourceId Resource ID
	 * @return List<Resource>
	 */
	List<Resource> managerList(Long resourceId);
	
	/**List filtered users.
	 * @param filter UserFilter
	 * @return FilterResult<Resource>
	 */
	FilterResult<Resource> searchResource(ResourceFilter filter);
	
	/**List all PosResourceStatus.
	 * @return List<PosResourceStatus>
	 */
	List<PosResourceStatus> posResourceStatusList();
	
	/**Add a Resource.
	 * @param resource Resource
	 * @return Resource Id
	 */
	Long addResource(Resource resource) throws UniqueValidationException ;
	
	/**Resource update.
	 * @param resource Resource
	 */
	void updateResource(Resource resource) throws UniqueValidationException ;
	
	
	/**
	 * Get resources from project.
	 * 
	 * @param projectPk - project id
	 * @return List<Resource>
	 */
	List<Resource> getResourceFromProject(Long projectPk);
	
		
	/**Gets a skill.
	 * @param id Skills ID
	 * @return Skills
	 */
	Skills getSkills(Long id);
	
	/**List filtered skills.
	 * @param filter SkillsFilter
	 * @return FilterResult
	 */
	FilterResult<Skills> searchSkills(SkillsFilter filter);
	
	/**List filtered skills.
	 * @param filter SkillsFilter
	 * @return FilterResult
	 */
	FilterResult<Skills> searchAllSkills(AllSkillsFilter filter);
	
	/**List all SkillsDef.
	 * @return List<SkillsDef>
	 */
	List<SkillsDef> skillsDefList();
	
	/**Add a SkillsDef.
	 * @param skilldef SkillsDef
	 * @return SkillsDef ID
	 */
	Long addSkillDef(SkillsDef skilldef);
	
	/**Gets SkillsDef by name.
	 * @param name SkillsDef name
	 * @return SkillsDef
	 */
	SkillsDef getSkillsDefByName(String name);
	
	/**Add a skill.
	 * @param skill Skills object
	 * @return Skill ID
	 */
	Long addSkill(Skills skill);
	
	/**Skill update.
	 * @param skill Skills object
	 */
	void updateSkill(Skills skill);
	
	/**Delete a skill.
	 * @param skillId Skill ID
	 */
	void deleteSkill(Long skillId);
	
	/**List filtered DeleteRequest.
	 * @param filter DeleteRequestFilter
	 * @return FilterResult<DeleteRequest>
	 */
	FilterResult<DeleteRequest> searchRequest(DeleteRequestFilter filter);
	
	/**DeleteRequest update.
	 * @param request DeleteRequest
	 */
	void updateRequest(DeleteRequest request);
	
	/**Gets a DeleteRequest.
	 * @param requestId DeleteRequest ID
	 * @return DeleteRequest
	 */
	DeleteRequest getRequest(Long requestId);
	
	
	/**
	 * Gets a DeleteRequest by entity type and entity id.
	 * @param entityType entity type
	 * @param entityId entity id
	 * @return fetched DeleteRequest
	 */
	DeleteRequest getRequestByTypeAndId(String entityType, Long entityId);
	
	/**Delete a DeleteRequest.
	 * @param requestId DeleteRequest ID
	 */
	void deleteRequest(Long requestId);
	
	/**Add a DeleteRequest.
	 * @param request DeleteRequest
	 * @return DeleteRequest ID.
	 */
	Long addRequest(DeleteRequest request);

	/**List all ResourceType.
	 * @return List<ResourceType>
	 */
	List<ResourceType> getResourceTypeList();

	/**List all BranchOffice.
	 * @return List<BranchOffice>
	 */
	List<BranchOffice> getBranchOfficeList();

	/**List all Employments.
	 * @return List
	 */
	List<Employment> getEmploymentList();

	/**
	 * List active users.
	 * @return List with active users
	 */
	List<Resource> listActiveResources();

	/**
	 * Gets ResourceType.
	 * @param resourceTypeId ResourceType ID
	 * @return ResourceType object
	 */
	ResourceType getResourceType(Long resourceTypeId);

	/**
	 * Gets BranchOffice.
	 * @param branchOfficeId BranchOffice ID
	 * @return BranchOffice
	 */
	BranchOffice getBranchOffice(Long branchOfficeId);

	/**
	 * Gets Employment
	 * @param employmentId Employment ID
	 * @return Employment object
	 */
	Employment getEmployment(Long employmentId);

	/**
	 * Get resources by position.
	 * @param selectedPositionId Position id
	 * @return List of Resources
	 */
	List<Resource> getByPosition(Long selectedPositionId);

	/**
	 * @param pk pk
	 * @param skillDefId skillDefId
	 * @return Skills
	 */
	Skills getUserSkillBySkillDef(Long pk, Long skillDefId);

	/**
	 * List all functions.
	 * @return List of functions
	 */
	List<FunctionType> getFunctionTypeList();

	/**
	 * List ResourceFTypeMapping for a Resource.
	 * @param resourceId Resource ID
	 * @return List of ResourceFTypeMapping
	 */
	List<ResourceFTypeMapping> getResourceFTypeMapping(Long resourceId);

	/**
	 * Delete ResourceFTypeMapping by Resource.
	 * @param resourceId Resource ID
	 */
	void deleteResourceFTypeMappingByResource(Long resourceId);

	/**
	 * Add a ResourceFTypeMapping.
	 * @param resourceFunction ResourceFTypeMapping object
	 * @return ResourceFTypeMapping id
	 */
	Long addResourceFTypeMapping(ResourceFTypeMapping resourceFunction);

	/**
	 * Get all project manager.
	 * 
	 * @return - project manager list
	 */
	List<Resource> getAllProjectManager();
	
	/**
	 * @param resourceId
	 * @param status
	 * @return
	 */
	List<VresActiveProjectMapping> getResourceProjectByStatus(Long resourceId, Long status);

	void deleteRessource(Long resourceId);

}
