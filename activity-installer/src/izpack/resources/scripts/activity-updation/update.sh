#!/bin/mksh
#-
# Copyright © 2013
#	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
#	Umer Kayani <u.kayani@tarent.de>
# All rights reserved.
#
# Licenced under the GNU AGPLv3.
#-
# Place the following three files into the same directory as this
# script (update.sh) then execute the script:
# - activity-backend-ear.ear
# - activity-portlets.war
# - activity-theme.war
# If you just built tarent-activity from source, you can do it with
# the following command line:
# $ cp activity-ear/target/activity-backend-ear.ear activity-portlets/target/activity-portlets.war activity-liferay-theme/target/activity-theme.war $INSTALL_PATH/updation/
#
# If the -n option is given, assume WildFly is stopped and should remain so.

if [[ $(id -un) != '$USER_NAME' ]]; then
	print -u2 E: run as wrong user!
	exit 1
fi

set -x
set -e
cd $INSTALL_PATH/updation
# Stopping WildFly, in case it still runs
test x"$1" = x"-n" || $INSTALL_PATH/jboss.sh stop
# Replacing tarent-activity artifacts and the tmp directory
rm -rf $INSTALL_PATH/bundles/jboss-7.1.1/standalone/deployments/activity-*
rm -rf $INSTALL_PATH/bundles/jboss-7.1.1/standalone/tmp/*
cp activity-backend-ear.ear $INSTALL_PATH/bundles/jboss-7.1.1/standalone/deployments/
cp activity-portlets.war $INSTALL_PATH/bundles/deploy/
cp activity-theme.war $INSTALL_PATH/bundles/deploy/
# Bringing LAR file up to date
rm -rf lar-tmp
mkdir lar-tmp
cd lar-tmp
unzip ../activity-portlets.war
mkdir -p $INSTALL_PATH/bundles/lar-files
cp liferay/activity-structure.lar $INSTALL_PATH/bundles/lar-files/activity-structure.lar
cd ..
rm -rf lar-tmp
# Bringing taract database schema up to latest changes
rm -rf flyway-tmp
mkdir flyway-tmp
cd flyway-tmp
unzip ../activity-backend-ear.ear
mkdir another-tmp
cd another-tmp
unzip ../activity-ejb.jar
test -d $INSTALL_PATH/flyway/sql/testdata/. || {
	# do not update testdata if not present initially
	rm -rf sql/testdata
}
find sql -type f -a '!' -name '*.sql' -print0 | xargs -0 rm --
rm -rf $INSTALL_PATH/flyway/sql
mv sql $INSTALL_PATH/flyway/sql
cd ../..
rm -rf flyway-tmp
(cd $INSTALL_PATH/flyway && ./flyway.sh migrate)
# Dropping and recreating liferay database
if (( USER_ID )); then
	PGPASSWORD=$(sed -n '/^jdbc.default.password=/s///p' \
	    $INSTALL_PATH/bundles/jboss-7.1.1/standalone/deployments/ROOT.war/WEB-INF/classes/portal-ext.properties) \
	    psql -h 127.0.0.1 -U liferay liferayportal <<'EOF'
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
EOF
else
	(cd /; exec sudo -u postgres psql) <<'EOF'
DROP DATABASE $liferay.database.dbname;
CREATE DATABASE $liferay.database.dbname OWNER $liferay.database.user;
\c liferayportal
ALTER SCHEMA public OWNER TO $liferay.database.user;
EOF
fi
# Starting WildFly
test x"$1" = x"-n" || $INSTALL_PATH/jboss.sh start
