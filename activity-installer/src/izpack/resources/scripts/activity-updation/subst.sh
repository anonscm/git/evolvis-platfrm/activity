#!/bin/sh

sed \
    -e 's[$]INSTALL_PATH$INSTALL_PATHg' \
    -e 's[$]USER_NAME$USER_NAMEg' \
    -e 's[$]activity.database.dbname$activity.database.dbnameg' \
    -e 's[$]activity.database.user$activity.database.userg' \
    -e 's[$]jboss.configureBindAddress$jboss.configureBindAddressg' \
    -e 's[$]liferay.database.dbname$liferay.database.dbnameg' \
    -e 's[$]liferay.database.user$liferay.database.userg' \

