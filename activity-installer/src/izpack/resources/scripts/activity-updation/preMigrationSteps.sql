-- This is equivalent to
--  ./flyway.sh -target=0.0.2 migrate
-- except that the DB is empty, for taking in our dump.

DROP SCHEMA activity CASCADE;
CREATE SCHEMA activity;
SET search_path = activity, pg_catalog;

--
-- Name: schema_version; Type: TABLE; Schema: activity; Owner: activity; Tablespace: 
--

CREATE TABLE schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);

--
-- Data for Name: schema_version; Type: TABLE DATA; Schema: activity; Owner: activity
--

COPY schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	0.0.1.1	schema	SQL	schema/V0.0.1_1__schema.sql	-71796797	activity	2013-06-27 14:24:46.903689	743	t
2	2	0.0.1.2	defaultdata	SQL	defaultdata/V0.0.1.2__defaultdata.sql	955806347	activity	2013-06-27 14:24:47.802685	31	t
3	3	0.0.1.4	productionDataMigration	SQL	production/V0.0.1_4__productionDataMigration.sql	-867389681	activity	2013-06-27 14:24:47.876551	1	t
\.
