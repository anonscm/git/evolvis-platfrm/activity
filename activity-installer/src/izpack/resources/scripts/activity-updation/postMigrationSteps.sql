BEGIN;

----------------------------------------------------------------------------------------------
-- Tabelle activity.tfunctiontype
-- Warnung: dies löscht alle Rollenzuordnungen (hinterher neu festlegen!)
UPDATE activity.tresource SET fk_functiontype=NULL;
DELETE FROM activity.tres_ftype_mapping;
DELETE FROM activity.tfunctiontype;

----------------------------------------------------------------------------------------------
-- Tabelle activity.tprojectfields
-- Warnung: dies löscht Daten (vorher durchsehen!)
DELETE FROM activity.tproject_fields_mapping;
DELETE FROM activity.tprojectfields;

----------------------------------------------------------------------------------------------
-- Usernamen
UPDATE activity.tresource SET username = concat(username, '2') WHERE pk IN (
WITH duplicates AS (
SELECT pk, username, ROW_NUMBER() OVER (PARTITION BY username) AS rk FROM activity.tresource WHERE username IN
	(SELECT username FROM activity.tresource GROUP BY username HAVING count(*) > 1) ORDER BY pk DESC)
SELECT s.pk FROM duplicates s WHERE s.rk = 1);

----------------------------------------------------------------------------------------------
COMMIT;

ALTER TABLE ONLY tskills DROP CONSTRAINT IF EXISTS tskills_fk_skills_def;
ALTER TABLE ONLY tskills DROP CONSTRAINT IF EXISTS tskills_fk_resource;
ALTER TABLE ONLY ttimer DROP CONSTRAINT IF EXISTS fk_resource;
ALTER TABLE ONLY ttimer DROP CONSTRAINT IF EXISTS fk_position;
ALTER TABLE ONLY tresource DROP CONSTRAINT IF EXISTS fk_branchoffice;
ALTER TABLE ONLY tpos_res_mapping DROP CONSTRAINT IF EXISTS "FK_STATUS";
ALTER TABLE ONLY tresource DROP CONSTRAINT IF EXISTS "FK_RESOURCETYPE";
ALTER TABLE ONLY tres_ftype_mapping DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tovertime DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tactivity DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tpos_res_mapping DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tloss DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tcost DROP CONSTRAINT IF EXISTS "FK_RESOURCE";
ALTER TABLE ONLY tproject_fields_mapping DROP CONSTRAINT IF EXISTS "FK_PROJECTFIELD";
ALTER TABLE ONLY tprojectinvoice DROP CONSTRAINT IF EXISTS "FK_PROJECT";
ALTER TABLE ONLY tovertime DROP CONSTRAINT IF EXISTS "FK_PROJECT";
ALTER TABLE ONLY tprojectsites DROP CONSTRAINT IF EXISTS "FK_PROJECT";
ALTER TABLE ONLY tproject_fields_mapping DROP CONSTRAINT IF EXISTS "FK_PROJECT";
ALTER TABLE ONLY tjob DROP CONSTRAINT IF EXISTS "FK_PROJECT";
ALTER TABLE ONLY tposition DROP CONSTRAINT IF EXISTS "FK_POSTITIONSTATUS";
ALTER TABLE ONLY tactivity DROP CONSTRAINT IF EXISTS "FK_POSITION";
ALTER TABLE ONLY tpos_res_mapping DROP CONSTRAINT IF EXISTS "FK_POSITION";
ALTER TABLE ONLY tovertime DROP CONSTRAINT IF EXISTS "FK_OVERTIMESTATUS";
ALTER TABLE ONLY tjob DROP CONSTRAINT IF EXISTS "FK_MANAGER";
ALTER TABLE ONLY tloss DROP CONSTRAINT IF EXISTS "FK_LOSSTYPE";
ALTER TABLE ONLY tloss DROP CONSTRAINT IF EXISTS "FK_LOSSSTATUS";
ALTER TABLE ONLY tjob DROP CONSTRAINT IF EXISTS "FK_JOBTYPE";
ALTER TABLE ONLY tjob DROP CONSTRAINT IF EXISTS "FK_JOBSTATUS";
ALTER TABLE ONLY tinvoice DROP CONSTRAINT IF EXISTS "FK_JOB";
ALTER TABLE ONLY tcost DROP CONSTRAINT IF EXISTS "FK_JOB";
ALTER TABLE ONLY tposition DROP CONSTRAINT IF EXISTS "FK_JOB";
ALTER TABLE ONLY tres_ftype_mapping DROP CONSTRAINT IF EXISTS "FK_FUNCTIONTYPE";
ALTER TABLE ONLY tresource DROP CONSTRAINT IF EXISTS "FK_FUNCTIONTYPE";
ALTER TABLE ONLY tresource DROP CONSTRAINT IF EXISTS "FK_EMPLOYMENT";
ALTER TABLE ONLY tovertime DROP CONSTRAINT IF EXISTS "FK_DECIDED_BY";
ALTER TABLE ONLY tproject DROP CONSTRAINT IF EXISTS "FK_CUSTOMER";
ALTER TABLE ONLY tcost DROP CONSTRAINT IF EXISTS "FK_COSTTYPE";
ALTER TABLE ONLY ttooltip DROP CONSTRAINT IF EXISTS ttooltip_pkey;
ALTER TABLE ONLY ttimer DROP CONSTRAINT IF EXISTS ttimer_pkey;
ALTER TABLE ONLY ttimer DROP CONSTRAINT IF EXISTS ttimer_fk_resource_key;
ALTER TABLE ONLY tskills DROP CONSTRAINT IF EXISTS tskills_pkey;
ALTER TABLE ONLY tskills_def DROP CONSTRAINT IF EXISTS tskills_def_pk;
ALTER TABLE ONLY tresourcetype DROP CONSTRAINT IF EXISTS tresourcetype_pkey;
ALTER TABLE ONLY tresource DROP CONSTRAINT IF EXISTS tresource_pkey;
ALTER TABLE ONLY tres_ftype_mapping DROP CONSTRAINT IF EXISTS tres_ftype_mapping_pkey;
ALTER TABLE ONLY tprojectsites DROP CONSTRAINT IF EXISTS tprojectsitess_pkey;
ALTER TABLE ONLY tprojectinvoice DROP CONSTRAINT IF EXISTS tprojectinvoice_pkey;
ALTER TABLE ONLY tprojectfields DROP CONSTRAINT IF EXISTS tprojectfields_pkey;
ALTER TABLE ONLY tproject DROP CONSTRAINT IF EXISTS tproject_pkey;
ALTER TABLE ONLY tproject_fields_mapping DROP CONSTRAINT IF EXISTS tproject_fields_mapping_pkey;
ALTER TABLE ONLY tpositionstatus DROP CONSTRAINT IF EXISTS tpositionstatus_pkey;
ALTER TABLE ONLY tposition DROP CONSTRAINT IF EXISTS tposition_pkey;
ALTER TABLE ONLY tpos_res_status DROP CONSTRAINT IF EXISTS tpos_res_status_pkey;
ALTER TABLE ONLY tpos_res_mapping DROP CONSTRAINT IF EXISTS tpos_res_mapping_pkey;
ALTER TABLE ONLY tovertimestatus DROP CONSTRAINT IF EXISTS tovertimestatus_pkey;
ALTER TABLE ONLY tovertime DROP CONSTRAINT IF EXISTS tovertime_pkey;
ALTER TABLE ONLY tlosstype DROP CONSTRAINT IF EXISTS tlosstype_pkey;
ALTER TABLE ONLY tlossstatus DROP CONSTRAINT IF EXISTS tlossstatus_pkey;
ALTER TABLE ONLY tlossdays DROP CONSTRAINT IF EXISTS tlossdays_pkey;
ALTER TABLE ONLY tloss DROP CONSTRAINT IF EXISTS tloss_pkey;
ALTER TABLE ONLY tjobtype DROP CONSTRAINT IF EXISTS tjobtype_pkey;
ALTER TABLE ONLY tjobstatus DROP CONSTRAINT IF EXISTS tjobstatus_pkey;
ALTER TABLE ONLY tjob DROP CONSTRAINT IF EXISTS tjob_pkey;
ALTER TABLE ONLY tinvoice DROP CONSTRAINT IF EXISTS tinvoice_pkey;
ALTER TABLE ONLY tfunctiontype DROP CONSTRAINT IF EXISTS tfunctiontype_pkey;
ALTER TABLE ONLY temployment DROP CONSTRAINT IF EXISTS temployment_pk;
ALTER TABLE ONLY tcustomer DROP CONSTRAINT IF EXISTS tcustomer_pkey;
ALTER TABLE ONLY tcosttype DROP CONSTRAINT IF EXISTS tcosttype_pkey;
ALTER TABLE ONLY tcost DROP CONSTRAINT IF EXISTS tcost_pkey;
ALTER TABLE ONLY tbranchoffice DROP CONSTRAINT IF EXISTS tbranchoffice_pkey;
ALTER TABLE ONLY tactivity DROP CONSTRAINT IF EXISTS tactivity_pkey;
ALTER TABLE ONLY tsettings DROP CONSTRAINT IF EXISTS "PKsettings";
ALTER TABLE ONLY taract_info DROP CONSTRAINT IF EXISTS "PK";

DROP VIEW activity.vformatted_dates;
