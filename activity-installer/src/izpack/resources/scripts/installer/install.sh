#!/bin/mksh
#-
# Copyright © 2013
#       Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
#       Umer Kayani <u.kayani@tarent.de>
# All rights reserved.
#
# Licenced under the GNU AGPLv3.
#-
# Place the following three files into the same directory as this
# script (update.sh) then execute the script:
# - activity-backend-ear.ear
# - activity-portlets.war
# - activity-theme.war
# If you just built tarent-activity from source, you can do it with
# the following command line:
# $ cp activity-ear/target/activity-backend-ear.ear activity-portlets/target/activity-portlets.war activity-liferay-theme/target/activity-theme.war /opt/activity/updation/
#
# If the -n option is given, assume WildFly is stopped and should remain so.

if [[ $(id -un) != 'root' ]]; then
        print -u2 E: run as wrong user!
        exit 1
fi

set -x
set -e
# Stopping WildFly, in case it still runs
test x"$1" = x"-n" || /opt/activity/jboss.sh stop
# Dropping and recreating liferay database
if (( USER_ID )); then
        PGPASSWORD=$(sed -n '/^jdbc.default.password=/s///p' \
            /opt/activity/bundles/jboss-7.1.1/standalone/deployments/ROOT.war/WEB-INF/classes/portal-ext.properties) \
            psql -h 127.0.0.1 -U liferay liferayportal <<'EOF'
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
EOF
else
        (cd /; exec sudo -u postgres psql) <<'EOF'
DROP DATABASE liferayportal;
CREATE DATABASE liferayportal OWNER liferay;
\c liferayportal
ALTER SCHEMA public OWNER TO liferay;
EOF
fi

# remove previous installation
rm -rf /opt/activity/*
# Install activity
cd /root/activity-installer
java -jar activity-installer-standard.jar activity-silent-script.xml

# Starting WildFly
test x"$1" = x"-n" || /opt/activity/jboss.sh start
