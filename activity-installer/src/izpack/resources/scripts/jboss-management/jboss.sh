#!/bin/mksh
#-
# tarent-activity WildFly startup script
# Copyright © 2013, 2014
#	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
#	Umer Kayani <u.kayani@tarent.de>
# All rights reserved.
#
# Licenced under the GNU AGPLv3.

case $USER_ID:$USER_NAME {
(0:root) ;;
(0:*|*:root)
	print -u2 "E: started as $USER_ID but want $USER_NAME"
	exit 1 ;;
}

# Settings (paths replaced by the installer)
JBOSS_HOME='$INSTALL_PATH/bundles/jboss-7.1.1'
JBOSS_BIND_ADDRESS='$jboss.configureBindAddress'

# Settings (to be adjusted by the local administrator if necessary)
export LC_ALL=de_DE.UTF-8
#export LC_ALL=en_GB.UTF-8

# Initialisation code

jbin=$JBOSS_HOME/bin
jlog=$JBOSS_HOME/standalone/log/server.log
jpid=$JBOSS_HOME/standalone/log/jboss.pid
jcli=$jbin/jboss-cli.sh

function usage {
	print 'Usage: ./jboss.sh [log | start | status | stop | trigger_start]'
	exit ${1:-1}
}

function do_trigger {
	local q=0 what=$1
	shift

	if [[ $1 = -q ]]; then
		q=1
		shift
	fi

	(( q )) || print Starting tarent-activity WildFly Application Server...
	(( q )) || print
	(( q )) || case $what {
	(t)
		print Server startup may take a while - \
		    check logfiles for completion.
		;;
	(*)
		print Server startup may take a while.
		;;
	}
	export LAUNCH_JBOSS_IN_BACKGROUND=1 JBOSS_PIDFILE=$jpid jbin
	mkdir -p "${jpid%/*}"
	rm -f "$jpid"	#XXX but the init script handles this
	mksh -T- -c "\"\$jbin\"/standalone.sh -b $JBOSS_BIND_ADDRESS"
	# wait for a maximum of five seconds until the PID file is created
	local i=0
	while (( ++i < 5 )); do
		sleep 1
		[[ -s $jpid ]] && break
	done
	[[ -s $jpid ]] && return 0
	(( q )) || print 'The WildFly standalone.sh failed to create a PID file in time.'
	(( q )) || print 'Startup execution inconclusive.'
	exit 1
}

function do_wait {
	local maxdelay=240 i=0 found=0 deploy deploys
	print -n "Waiting until fully deployed or $maxdelay seconds / CLI calls "
	while (( !found && (++i < maxdelay) )); do
		sleep 1
		print -n .
		deploys=$("$jbin"/jboss-cli.sh --connect \
		    --command='ls /deployment' 2>/dev/null)
		# require successful deployment of *all* jars
		found=1
		for deploy in activity-backend-ear.ear \
		    activity-portlets.war activity-theme.war; do
			[[ $deploys = *"$deploy"* ]] && continue
			found=0
			break
		done
	done
	if (( found )); then
		print \ done
		return 0
	fi
	print \ aborted!
	return 1
}

function do_stop {
	print Stopping tarent-activity WildFly Application Server...
	print
	"$jbin"/jboss-cli.sh --connect :shutdown
	# wait for 5..20 seconds
	local i=0
	while (( ++i < 5 )); do
		sleep 5
		[[ -s $jpid ]] || break
		kill $(<$jpid)
	done
	[[ -s $jpid ]] || return 0
	rm -f "$jpid"
	return 1
}

function do_status {
	local pid

	if [[ ! -e $jpid ]]; then
		print PID file does not exist
		exit 1
	fi
	if [[ ! -s $jpid ]]; then
		print PID file is empty
		exit 1
	fi
	pid=$(<$jpid)
	if [[ $pid != +([0-9]) ]]; then
		print PID file does not contain the PID
		exit 1
	fi
	if ! kill -0 $pid; then
		print Process $pid not running
		rm -f "$jpid"
		exit 1
	fi
	print Process $pid running
	exit 0
}

function do_log {
	local lines

	case $1 {
	(-n|--lines)
		lines=$2
		shift 2
		;;
	(--lines=*)
		lines=${1#*=}
		shift
		;;
	}

	exec tail -F ${lines:+--lines=$lines} "$jlog"
}

case $1 {
(-h|--help)
	usage 0
	;;
(log)
	shift
	do_log "$@"
	;;
(start)
	shift
	do_trigger s "$@"
	do_wait || exit 1
	;;
(status)
	do_status
	;;
(stop)
	do_stop || exit 1
	;;
(trigger_start)
	do_trigger t
	;;
(*)
	usage
	;;
}
exit 0
