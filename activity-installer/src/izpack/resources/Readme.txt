+=================+
| Activity README |
+=================+

Run "$INSTALL_PATH/jboss.sh start" to execute tarent-activity.
"$INSTALL_PATH/initscript/taract" is a nice SYSV init script.

The first run may return a failure to create a PID file.
Until this is investigated, send the java process a SIGTERM
(never a SIGKILL!), wait until it exited by itself then try
to restart it. It may prove useful to wait until it’s fully
deployed first, however.
