/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.CustomerDAOImpl;
import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * Test class for CustomerDAOImpl class.
 * 
 */
public class CustomerDAOTest extends BaseDAOTest<Customer> {

	private CustomerDAOImpl customerDao = new CustomerDAOImpl();

	@Override
	protected BaseDAOWithJPA<Customer> getDao() {
		return customerDao;
	}

	/**
	 * List customers by filter.
	 */
	@Test
	public void testListCustomer() {
		BaseFilter filter = new BaseFilter(1L, 5L, null, null);
		List<Customer> customers = customerDao.listCustomer(filter);
		XMLDebug.printXML(customers);
	}

	/**
	 * Count customers by filter.
	 */
	@Test
	public void testCountCustomer() {
		BaseFilter filter = new BaseFilter(1L, 5L, null, null);
		Long count = customerDao.countCustomer(filter);
		XMLDebug.printXML(count);
	}

	/**
	 * List all customers.
	 */
	@Test
	public void testFindAll() {
		List<Customer> customers = customerDao.findAll();
		XMLDebug.printXML(customers);
	}

	/**
	 * Gets a customer by id.
	 */
	@Test
	public void testGetCustomer() {
		Customer customer = customerDao.find(1L);
		XMLDebug.printXML(customer);
	}

	/**
	 * Add a new customer.
	 */
	@Test
	public void testAddCustomer() {
		Customer customer = new Customer();
		customer.setName("customer");
		Long pk = customerDao.create(customer);
		XMLDebug.printXML(pk);
	}

}
