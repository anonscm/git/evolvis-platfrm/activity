/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import de.tarent.activity.dao.impl.ViewBaseDAOImpl;

public abstract class ViewBaseDAOTest<EntityClass> {

	protected static EntityManager em;

	@BeforeClass
	public static void beforeClass() {
		em = Persistence.createEntityManagerFactory("activityTestPU").createEntityManager();
	}

	@Before
	public void setUp() {
		getDao().setEntityManager(em);
		em.getTransaction().begin();
	}

	@After
	public void tearDown() {
		em.getTransaction().rollback();
		em.clear();
	}

	@AfterClass
	public static void afterClass() {
		em.close();
	}

	protected abstract ViewBaseDAOImpl<EntityClass> getDao();
}
