/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.ReportDAOImpl;
import de.tarent.activity.domain.Report;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is a test class for ActivityDAO.
 *
 */
public class ReportDAOTest extends BaseDAOTest<Report> {
	
	private ReportDAOImpl reportDao = new ReportDAOImpl();

	@Override
	protected BaseDAOWithJPA<Report> getDao() {
		return reportDao;
	}

	@Test
	public void testListReports() {

//		List<Report> reports = reportDAO.listReport(filter);
//		LOGGER.debug("Number of reports : " + reports.size());
//		Long count = reportDAO.countReport(filter);
//		LOGGER.debug("Number of count reports: " + count);
		
		
//		List<Report> reports = reportDao.listReport(new BaseFilter(0l, 5l, "name", "asc"));
		List<Report> reports = reportDao.listReport(new BaseFilter(null, null, null, null));
		XMLDebug.printXML(reports);
	}

	
	@Test
	public void testCountReports() throws ParseException {
		XMLDebug.printXML(reportDao.countReport(new BaseFilter(null, null, null, null)));
		
	}
	
	@Test
	public void testCreateReport()
	{
		Report report = new Report();
		report.setName("testReport");
		report.setData("test data");
		report.setFkResource(20l);
		report.setPermission(0l);
		
		reportDao.create(report);
		em.flush();
		System.out.println(report.getPk());
		em.refresh(report);		
		XMLDebug.printXML(report);
	}
	
	@Test
	public void testGetReport(){
		Report report = reportDao.find(4L);
		XMLDebug.printXML(report);
	}
	
	@Test
	public void testUpdateReport()
	{
		Report report = reportDao.find(5l);
		if(report!=null){
			report.setName("testReport2");
			reportDao.update(report);
			em.flush();
		}
		
		XMLDebug.printXML(report);
	}
	
//	@Test
//	public void testDelete()
//	{
//		getDao().delete(4L);
//	}
}
