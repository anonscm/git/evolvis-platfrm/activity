/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.ProjectDAOImpl;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.filter.ProjectFilter;

/**
 * This is a test class for ProjectDAOTest.
 * 
 */
public class ProjectDAOTest extends BaseDAOTest<Project> {

	private ProjectDAOImpl projectDao = new ProjectDAOImpl();

	@Override
	protected BaseDAOWithJPA<Project> getDao() {
		return projectDao;
	}

	@Test
	public void testListProjectsByResource() {
		List<Project> projects = projectDao.findByResource(2l);
		assertEquals(2, projects.size());
	}

	@Test
	public void testListAllProjects() {
		List<Project> projects = projectDao.findAll();
		assertTrue(4 <= projects.size());
	}

	@Test
	public void testListProjects() {
		ProjectFilter filter = new ProjectFilter(null, null, null, null, null, null, null, null, null);
		List<Project> projects = projectDao.listProjects(filter);
		Long count = projectDao.countProject(filter); 
		
		assertTrue(4 <= projects.size());
		assertTrue(4 <= count.intValue());
	}

	@Test
	public void testListProjectsByResponsibleResource() {
		List<Project> projects = projectDao.findByResponsibleResource(5l);
		assertEquals(0, projects.size());
		
		projects = projectDao.findByResponsibleResource(1l);
        assertEquals(4, projects.size());
	}

	@Test
	public void testFindAllActive(){
	    List<Project> projects = projectDao.findAllActive();
	    assertEquals(2, projects.size());
	}
}
