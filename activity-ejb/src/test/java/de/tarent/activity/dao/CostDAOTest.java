/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.CostDAOImpl;
import de.tarent.activity.dao.impl.CostTypeDAOImpl;
import de.tarent.activity.dao.impl.JobDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is the test class for CostDAOImpl class.
 * 
 */
public class CostDAOTest extends BaseDAOTest<Cost> {

	private CostDAOImpl costDao = new CostDAOImpl();

	@Override
	protected BaseDAOWithJPA<Cost> getDao() {
		return costDao;
	}

	/**
	 * List Costs by filter with project id.
	 */
	@Test
	public void testListCostByProject() {
		CostFilter filter = new CostFilter(1L, null, null, null, null, null);
		List<Cost> list = costDao.listCost(filter);
		XMLDebug.printXML(list);
	}

	/**
	 * List Costs by filter with job id.
	 */
	@Test
	public void testListCostByJob() {
		CostFilter filter = new CostFilter(null, 1L, null, null, null, null);
		List<Cost> list = costDao.listCost(filter);
		XMLDebug.printXML(list);
	}

	/**
	 * List Costs by filter with project id.
	 */
	@Test
	public void testListCostByProjectAndJob() {
		CostFilter filter = new CostFilter(1L, 1L, null, null, null, null);
		List<Cost> list = costDao.listCost(filter);
		XMLDebug.printXML(list);
	}

	/**
	 * Count costs by filter.
	 */
	@Test
	public void testCountCost() {
		CostFilter filter = new CostFilter(1L, null, null, null, null, null);
		Long nr = costDao.countCost(filter);
		XMLDebug.printXML(nr);

	}

	/**
	 * List costs by Job.
	 */
	@Test
	public void testFindByJob() {
		List<Cost> costs = costDao.findByJob(1L);
		XMLDebug.printXML(costs);
	}

	/**
	 * Gets a cost by id.
	 */
	@Test
	public void testGetCost() {
		XMLDebug.printXML(costDao.find(1L));

	}

	/**
	 * Add a new cost.
	 */
	@Test
	public void testAddCost() {
		Cost cost = new Cost();
		JobDAOImpl jobDao = new JobDAOImpl();
		jobDao.setEntityManager(em);
		cost.setJob(jobDao.find(1L));

		CostTypeDAOImpl costTypeDao = new CostTypeDAOImpl();
		costTypeDao.setEntityManager(em);
		cost.setCostType(costTypeDao.find(1L));
		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(em);
		cost.setResource(resourceDao.find(2L));
		cost.setCost(new BigDecimal(10));
		cost.setCrDate(new Date());
		cost.setName("New Cost");
		costDao.create(cost);
		em.flush();
		em.refresh(cost);
		XMLDebug.printXML(cost);
	}
}
