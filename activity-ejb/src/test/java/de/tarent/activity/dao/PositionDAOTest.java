/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.PositionDAOImpl;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is a test class for PositionDAO.
 * 
 */
public class PositionDAOTest extends BaseDAOTest<Position> {

	private PositionDAOImpl positionDao = new PositionDAOImpl();

	@Override
	protected BaseDAOWithJPA<Position> getDao() {
		return positionDao;
	}

	@Test
	public void testListPositionsByJobAndResource() {

		List<Position> positions = positionDao.findByJob(1L, null, 2L);
		XMLDebug.printXML(positions);
	}
	
	@Test
	public void testListPositionsByJob() {

		List<Position> positions = positionDao.findByJob(2L, 2L, null);
		XMLDebug.printXML(positions);
	}
	
	@Test
	public void testListPosition() {
		PositionFilter filter = new PositionFilter(null, 2l, null, null, "position.positionStatus.id", "asc");
		List<Position> positions = positionDao.listPosition(filter);
		System.out.println(positions.size());
		XMLDebug.printXML(positions);
	}
	
	@Test
	public void testCountPosition() {
		PositionFilter filter = new PositionFilter(null, 2l, null, null, null, null);
		Long ret = positionDao.countPosition(filter);
		XMLDebug.printXML(ret);
	}

}
