/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.Date;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.LossDAOImpl;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.LossStatus;
import de.tarent.activity.domain.LossType;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * Test class for LossDAOImpl class.
 * 
 */
public class LossDAOTest extends BaseDAOTest<Loss> {

	private LossDAOImpl lossDao = new LossDAOImpl();

	@Override
	protected BaseDAOWithJPA<Loss> getDao() {
		return lossDao;
	}

	/**
	 * Gets Loss by id.
	 */
	@Test
	public void testGetLossById() {
		Loss loss = lossDao.find(6L);
		XMLDebug.printXML(loss);
	}

	/**
	 * Add a new loss.
	 */
	@Test
	public void testCreateLoss() {

		Loss loss = createALoss();
		lossDao.create(loss);
		em.flush();
		System.out.println(loss.getPk());
		em.refresh(loss);
		XMLDebug.printXML(loss);

	}

	/**
	 * Update loss.
	 */
	public void testUpdateLoss() {
		Loss loss = lossDao.find(1L);
		loss.setUpdDate(new Date());
		lossDao.update(loss);
		em.flush();
		XMLDebug.printXML(loss);

	}

	/**
	 * Create a loss that have minimal informations.
	 * 
	 * @return loss 
	 */
	private Loss createALoss() {
		Loss loss = new Loss();
		loss.setLossType(new LossType(1L));
		loss.setLossStatusByFkLossStatus(new LossStatus(1L));
		loss.setResourceByFkResource(new Resource(1L));
		return loss;

	}

	/**
	 * List and count filtered Loss.
	 */
	@Test
	public void testListByFilter() {

		XMLDebug.printXML(lossDao.listLoss(new LossFilter(null, null, null, null, null, 1L, null, null, null, null)));
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println("count = "
				+ lossDao.countLoss(new LossFilter(null, null, null, null, null, 2L, null, null, null, null)));
	}

	/**
	 * List loss by project.
	 */
	@Test
	public void testListByProject() {

		XMLDebug.printXML(lossDao.getCurrentLossByProject(new Date(), null).size());
		XMLDebug.printXML(lossDao.getFuturetLossByProject(new Date(), 1L).size());

		String[] a = { "1", "2" };

		XMLDebug.printXML(lossDao.getCurrentLossByLocation(new Date(), a).size());
	}

	/**
	 * Gets loss days.
	 */
	@Test
	public void testGetLossDays() {
		XMLDebug.printXML(lossDao.getLossDayFromYear(2L, 2010));
	}

}
