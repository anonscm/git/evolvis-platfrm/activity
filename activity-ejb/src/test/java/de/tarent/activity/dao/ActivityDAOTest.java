/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.ActivityDAOImpl;
import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.PositionDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is a test class for ActivityDAOImpl class.
 *
 */
public class ActivityDAOTest extends BaseDAOTest<Activity> {

	private ActivityDAOImpl activityDao = new ActivityDAOImpl();

	@Override
	protected BaseDAOWithJPA<Activity> getDao() {
		return activityDao;
	}

	/**
	 * List filtered activities.
	 * @throws ParseException throws Parse Exception for date
	 */
	@Test
	public void testListActivitiesByFilter() throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date a = dfm.parse("2007-02-26");

		ActivityFilter f = new ActivityFilter(2l, 1l, 1l, a, new Date(), null, null, 5l, "activity.name", "desc");
		List<Activity> activities = activityDao.listActivity(f);
		XMLDebug.printXML(activities);
	}

	/**
	 * Count filtered activities.
	 * @throws ParseException throws Parse Exception for date
	 */
	@Test
	public void testCountActivityByFilter() throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date a = dfm.parse("2007-02-26");
		ActivityFilter f = new ActivityFilter(2l, 1l, 1l, a, new Date(), null, null, 5l, null, null);
		XMLDebug.printXML(activityDao.countActivity(f));

	}

	/**
	 * List activities by position id.
	 */
	@Test
	public void testListActivitiesByPosition() {
		List<Activity> activities = activityDao.listActivitiesByPosition(1L);
		XMLDebug.printXML(activities);

	}

	/**
	 * Create an activity.
	 */
	@Test
	public void testCreateActivity() {
		PositionDAOImpl posdao = new PositionDAOImpl();
		ResourceDAOImpl resdao = new ResourceDAOImpl();
		posdao.setEntityManager(em);
		resdao.setEntityManager(em);

		Activity activity = new Activity();
		activity.setCrUser("marius");
		activity.setName("activitatea 1");

		activity.setPosition(new Position(1l));
		activity.setResource(new Resource(1l));

		activityDao.create(activity);
		em.flush();
		System.out.println(activity.getPk());
		em.refresh(activity);
		XMLDebug.printXML(activity);
	}

	/**
	 * Update an activity.
	 */
	@Test
	public void testUpdateActivity() {
		Activity activity = activityDao.find(1l);
		activity.setName("testActivity");
		activity.setDate(new Date());
		activityDao.update(activity);
		em.flush();
		XMLDebug.printXML(activity);
	}

	/**
	 * Gets total number of hours from an interval.
	 * @throws ParseException throws Parse Exception for date
	 */
	@Test
	public void testGetHoursFromInterval() throws ParseException {
		// einmal testen mit start und enddatum
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = dfm.parse("2012-10-28");
		Date lastDate = dfm.parse("2012-10-30");
		BigDecimal hoursFromInterval = activityDao.getHoursFromInterval(1l,startDate, lastDate);
		assertNotNull(hoursFromInterval);
		assertEquals(13.75, hoursFromInterval.doubleValue(), 0.0);

		// testen ohne startdatum
		hoursFromInterval = activityDao.getHoursFromInterval(1l, null, lastDate);
		assertNotNull(hoursFromInterval);
		assertEquals(64.5, hoursFromInterval.doubleValue(), 0.0);

		// testen ohne enddatum
		hoursFromInterval = activityDao.getHoursFromInterval(1l, startDate, null);
		assertNotNull(hoursFromInterval);
		assertEquals(13.75, hoursFromInterval.doubleValue(), 0.0);

		// testen ohne datum
		hoursFromInterval = activityDao.getHoursFromInterval(1l, null, null);
		assertNotNull(hoursFromInterval);
		assertEquals(64.5, hoursFromInterval.doubleValue(), 0.0);
	}
}
