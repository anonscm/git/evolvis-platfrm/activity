/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.Date;
import java.util.List;

import org.junit.Test;


import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.SkillsDAOImpl;
import de.tarent.activity.dao.impl.SkillsDefDAOImpl;
import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.SkillsDef;
import de.tarent.activity.domain.filter.SkillsFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This class tests SkillsDAO.
 * 
 */
public class SkillsDAOTest extends BaseDAOTest<Skills> {

	private SkillsDAOImpl skillsDao = new SkillsDAOImpl();

	@Override
	protected BaseDAOWithJPA<Skills> getDao() {
		return skillsDao;

	}

	@Test
	public void testListSkillsDef() {
		SkillsDefDAOImpl sdao = new SkillsDefDAOImpl();
		sdao.setEntityManager(em);
		List<SkillsDef> list = sdao.findAll();
		XMLDebug.printXML(list);

	}

	@Test
	public void testListSkillsByResource() {
		SkillsFilter filter = new SkillsFilter(1L, null, null, null, null, null);
		List<Skills> list = skillsDao.listSkills(filter);
		XMLDebug.printXML(list);

	}
	
	@Test
	public void testCountSkills(){
		SkillsFilter filter = new SkillsFilter(1L, null, null, null, null, null);
		Long nr = skillsDao.countSkills(filter);
		XMLDebug.printXML(nr);
	}
	
	@Test
	public void testAddSkills(){
		Skills skill = new Skills();
		SkillsDefDAOImpl skillsDefDao = new SkillsDefDAOImpl();
		skillsDefDao.setEntityManager(em);
		ResourceDAOImpl resoureDAO = new ResourceDAOImpl();
		resoureDAO.setEntityManager(em);
		skill.setResource(resoureDAO.find(1L));
		skill.setSkillsDef(skillsDefDao.find(1L));
		skill.setCrDate(new Date());
		skillsDao.create(skill);
		em.flush();
		em.refresh(skill);
		XMLDebug.printXML(skill);
	}

}
