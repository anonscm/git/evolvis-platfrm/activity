/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.PermissionDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.RoleDAOImpl;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Role;
import de.tarent.activity.util.TestUserUtilImpl;

/**
 * Tests for all role based DAO methods.
 */
public class RoleDAOTest extends BaseDAOTest<Role> {

	// Class to test
	private RoleDAOImpl dao = new RoleDAOImpl();
	
	private PermissionDAO permDao = new PermissionDAOImpl();
	private ResourceDAO resDao = new ResourceDAOImpl();

	@Override
	protected BaseDAOWithJPA<Role> getDao() {
		return dao;
	}
	
	@Before
	public void init(){
	    ((PermissionDAOImpl)permDao).setEntityManager(em);
	    ((PermissionDAOImpl)permDao).setUserUtil(new TestUserUtilImpl());
	    
	    ((ResourceDAOImpl)resDao).setEntityManager(em);
        ((ResourceDAOImpl)resDao).setUserUtil(new TestUserUtilImpl());
	}

	@Test
	public void testSaveAndFindRole() {
		String roleName = "Unit Testrolle";
		String actionId = "TestActionId" + System.currentTimeMillis();
		
		// helper dao to retrieve resource
		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(em);
		resourceDao.setUserUtil(new TestUserUtilImpl());

		// retrieve user with id 1 from test data
		Resource resource = resourceDao.find(1L);

		// save a new role with an assignment to one permission and one resource
		Role role = new Role();
		role.setName(roleName);
		role.setPermissions(new HashSet<Permission>());
		role.getPermissions().add(new Permission(actionId, "Testaction"));
		role.setResources(new HashSet<Resource>());
		role.getResources().add(resource);

		Long rolePk = dao.create(role);
		
		// now check if we could load recently saved role
		role = dao.find(rolePk);
		assertNotNull(role);
		assertEquals(roleName, role.getName());
	}
	
	@Test
	public void testUpdate(){
		String roleName = "Testrolle";
		
		// retrieve role from test data
		Role role = dao.find(1L);
		assertEquals("Mitarbeiter", role.getName());
		
		role.setName(roleName);
		dao.update(role);
		
		role = dao.find(1L);
		assertEquals(roleName, role.getName());
	}

	@Test
	public void testFindAll() {
		List<Role> roles = dao.findAll();

		assertNotNull(roles);
	}

	@Test
	public void testFindByPermission() {
		String roleName = "Unit Testrolle" + System.currentTimeMillis();
		String actionId = "TestActionId" + System.currentTimeMillis();

		Permission permission = new Permission(actionId, "Testaction");
		Long permPk = permDao.create(permission);
		permission = permDao.find(permPk);

		// a role with above permission should not be present at this moment
		List<Role> roles = dao.findByPermission(permission);
		assertNotNull(roles);
		assertTrue(roles.isEmpty());

		// Save a new Role with Permission
		Role role = new Role();
		role.setName(roleName);

		Set<Permission> permissions = new HashSet<Permission>();
		permissions.add(permission);
		role.setPermissions(permissions);

		Long rolePk = dao.create(role);

		// check if we can find recently saved role by permission
		roles = dao.findByPermission(permission);

		assertEquals(1, roles.size());
		assertEquals(rolePk, roles.get(0).getPk());
	}

	@Test
	public void testFindByResource() {
		// helper dao to retrieve resource
		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(em);
		resourceDao.setUserUtil(new TestUserUtilImpl());

		// retrieve user with id 1 from test data
		Resource resource = resourceDao.find(1L);
		int roleCount = resource.getRoles().size();

		// save a new role with assignment to above resource
		Role role = new Role();
		role.setName("Testrolle");
		role.setResources(new HashSet<Resource>());
		role.getResources().add(resource);
		dao.create(role);

		// check that we find recently saved role
		List<Role> updatedRoles = dao.findByResource(resource);
		assertEquals(roleCount + 1, updatedRoles.size());
	}

	@Test
	public void testFindByPermissionAndResource() {
		String roleName = "Unit Testrolle" + System.currentTimeMillis();
		String actionId = "TestActionId" + System.currentTimeMillis();
		String userName = ("" + System.currentTimeMillis()).substring(0, 9);

		Resource resourceA = new Resource();
		resourceA.setResourceType(new ResourceType(1L));
		resourceA.setUsername(userName);
		resourceA.setRemainHoliday(BigDecimal.ZERO);
		Long resAPk = resDao.create(resourceA);
		resourceA = resDao.find(resAPk);

		Resource resourceB = new Resource();
		resourceB.setResourceType(new ResourceType(1L));
		resourceB.setUsername(userName + "B");
		resourceB.setRemainHoliday(BigDecimal.ZERO);
		Long resBPk = resDao.create(resourceB);
        resourceB = resDao.find(resBPk);

		Permission permissionA = new Permission(actionId, "Testaction");
		Long permAPk = permDao.create(permissionA);
		permissionA = permDao.find(permAPk);
		
		Permission permissionB = new Permission(actionId + "B", "TestactionB");
		Long permBPk = permDao.create(permissionB);
		permissionB = permDao.find(permBPk);

		// save a new role with assignment to permissionA and resourceA
		Role roleA = new Role();
		roleA.setName(roleName);
		roleA.setPermissions(new HashSet<Permission>());
		roleA.getPermissions().add(permissionA);
		roleA.setResources(new HashSet<Resource>());
		roleA.getResources().add(resourceA);

		Long roleAPk = dao.create(roleA);

		// save a new role with assignment to permissionB and resourceB
		Role roleB = new Role();
		roleB.setName(roleName + "B");
		roleB.setPermissions(new HashSet<Permission>());
		roleB.getPermissions().add(permissionB);
		roleB.setResources(new HashSet<Resource>());
		roleB.getResources().add(resourceB);

		Long roleBPk = dao.create(roleB);

		// now check if recently saved roles could be found
		List<Role> roles = dao.findByResourceAndPermission(resourceA.getPk(),
				permissionA.getActionId());
		assertEquals(1, roles.size());
		assertEquals(roleAPk, roles.get(0).getPk());

		roles = dao.findByResourceAndPermission(resourceB.getPk(), permissionB.getActionId());
		assertEquals(1, roles.size());
		assertEquals(roleBPk, roles.get(0).getPk());

		// we have no role with assignment to resourceB and permissionA at one
		// time
		roles = dao.findByResourceAndPermission(resourceB.getPk(), permissionA.getActionId());
		assertEquals(0, roles.size());
	}

}
