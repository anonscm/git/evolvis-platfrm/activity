/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.ResourceTypeDAOImpl;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is a test class for ResourceDAOIml class.
 * 
 */
public class ResourceDAOTest extends BaseDAOTest<Resource> {

	private ResourceDAOImpl resourceDao = new ResourceDAOImpl();

	@Override
	protected BaseDAOWithJPA<Resource> getDao() {
		return resourceDao;
	}

	@Test
	public void testListResources() {
		// XMLDebug.printXML(resourceDao.findAll());
		List<Resource> list = resourceDao.findAll();
		for (Resource resource : list) {
			System.out.println(resource.getFirstname() + " "
					+ resource.getLastname());
		}

	}

	/**
	 * Find resource by username and password, for authentication.
	 */
	@Test
	public void testFindResource() {
		Resource r = resourceDao.loadResource("admin");
		XMLDebug.printXML(r);
	}

	@Test
	public void testGetResource() {
		Resource res = resourceDao.find(1L);
		XMLDebug.printXML(res);
	}

	@Test
	public void testListResource() {
		ResourceFilter filter = new ResourceFilter(null, null, null, null, null, null);
		List<Resource> list = resourceDao.listResource(filter);
		XMLDebug.printXML(list);
	}

	@Test
	public void testCountResource() {
		ResourceFilter filter = new ResourceFilter(null, null, null, null, null, null);
		Long nr = resourceDao.countResource(filter);
		XMLDebug.printXML(nr);

	}

	@Test
	public void testAddResource() {
		Resource resource = new Resource();
		ResourceTypeDAOImpl resourceTypeDao = new ResourceTypeDAOImpl();
		resourceTypeDao.setEntityManager(em);
		ResourceType resType = resourceTypeDao.find(1L);
		resource.setCrDate(new Date());
		resource.setFirstname("new resource");
		resource.setResourceType(resType);
		resource.setRemainHoliday(new BigDecimal(1));
		resource.setHoliday(new BigDecimal(1));

		resourceDao.create(resource);
		em.flush();
		em.refresh(resource);
		XMLDebug.printXML(resource);
	}

	@Test
	public void testListActiveResource() {
		List<Resource> resources = resourceDao.getActiveResources();
		XMLDebug.printXML(resources);
	}

	@Test
	public void testListResourceByProject() {
		XMLDebug.printXML(resourceDao.listResourceFromProject(1L).size());

	}

//	@Test
//	public void testListByRights() {
//		List<Resource> resources = resourceDao
//				.getByRights(RESOURCE_CONTROLLER_PERMISSION);
//		XMLDebug.printXML(resources);
//	}

	@Test
	public void testListProjectManager() {
		XMLDebug.printXML(resourceDao.getAllProjectManager());
	}
	
	@Test
	public void testAddResourceWithRole(){
		String roleName = "Unit Testrolle mit Permissions";
		Long resourceId = 2L;
		
		Resource resource = resourceDao.find(resourceId);
		
		Set<Role> roles = new HashSet<Role>();
		roles.add(new Role(roleName));
		roles.add(new Role(roleName + '2'));
		
		resource.setRoles(roles);
		
		resourceDao.create(resource);
		
		resource = resourceDao.find(resourceId);
		
		assertNotNull(resource.getRoles());
		assertTrue(resource.getRoles().size() > 0);
	}
	
	@Test
	public void testDeleteRessource(){
		Long resourceId = 2L;
		Resource resource = resourceDao.find(resourceId);
		
		assertNotNull(resource);
		
		resourceDao.delete(resourceId);
		
		Resource resource2 = resourceDao.find(resourceId);
		assertNull(resource2);
		
	}
}
