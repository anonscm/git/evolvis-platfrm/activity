/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.JobDAOImpl;
import de.tarent.activity.dao.impl.ProjectDAOImpl;
import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.JobFilter;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.util.XMLDebug;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * This is a test class for JobDAOImpl class.
 * 
 */
public class JobDAOTest extends BaseDAOTest<Job> {

	private JobDAOImpl jobDAO = new JobDAOImpl();

	@Override
	protected BaseDAOWithJPA<Job> getDao() {
		return jobDAO;
	}

	/**
	 * Count filtered jobs.
	 */
	@Test
	public void testCountJob() {
		JobFilter filter = new JobFilter(null, 1L, null, null, null, null, null, null,null);
		Long count = jobDAO.countJob(filter);
		XMLDebug.printXML(count);
	}

	/**
	 * List active jobs by resource.
	 */
	@Test
	public void testFindActiveJobsByResouce() {
		List<Job> jobs = jobDAO.findActiveJobsByResource(1L);
		XMLDebug.printXML(jobs);
	}

	/**
	 * List jobs by filter.
	 */
	@Test
	public void testListJob() {
		JobFilter filter = new JobFilter(null, 1L, null, null, null, null, null, null,null);
		List<Job> jobs = jobDAO.listJob(filter);
		XMLDebug.printXML(jobs);
	}

	/**
	 * List all jobs.
	 */
	@Test
	public void testFindAll() {
		List<Job> jobs = jobDAO.findAll();
		XMLDebug.printXML(jobs);
	}

	/**
	 * List not maintained jobs.
	 */
	@Test
	public void testFindNotMaintenanceJobs() {
		List<Job> jobs = jobDAO.findNotMaintenanceJobs();
		XMLDebug.printXML(jobs);
	}

	/**
	 * List jobs by project.
	 */
	@Test
	public void testFindByProject() {
		List<Job> jobs = jobDAO.findByProject(1L);
		XMLDebug.printXML(jobs);
	}

	/**
	 * List ordered jobs.
	 */
	@Test
	public void testGetOrderedList() {
		List<Job> jobs = jobDAO.getOrderedList();
		XMLDebug.printXML(jobs);
	}

	/**
	 * Add a new job.
	 */
	@Test
	public void testCreateJob() {
		ProjectDAOImpl projDao = new ProjectDAOImpl();
		projDao.setEntityManager(em);

		Job job = new Job();
		job.setName("test job");
		job.setProject(new Project(1l));
		job.setManager(new Resource(1l));
		job.setJobStatus(new JobStatus(ProjectService.JOBSTATUS_ORDERED));
		job.setJobType(new JobType(ProjectService.JOBTYPE_INTERNALJOB));
		job.setCrm("CRM test");
		job.setJobStartDate(new Date());
		job.setJobEndDate(new Date());
		job.setNote("Ana are mere");

		jobDAO.create(job);
		em.flush();
		System.out.println(job.getPk());
		em.refresh(job);
		XMLDebug.printXML(job);
	}

	/**
	 * Update job.
	 */
	@Test
	public void testUpdateJob() {
		Job job = jobDAO.find(1l);
		job.setName("testActivity");
		job.setJobEndDate(new Date());
		jobDAO.update(job);

		em.flush();

		XMLDebug.printXML(job);
	}
	
	@Test
	public void testTotalDays() {
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal h = jobDAO.getTotalDaysSpent(1l);
		assertEquals("7.5", h.toString());
		total = total.add(h);

		h = jobDAO.getTotalDaysSpent(2l);
		assertEquals("5.25", h.toString());
		total = total.add(h);
		
		h = jobDAO.getTotalDaysSpent(3l);
		assertEquals("1.75", h.toString());
		total = total.add(h);
		
		h = jobDAO.getTotalDaysSpent(4l);
		assertEquals("4.5", h.toString());
		total = total.add(h);
		
		assertEquals("19.00", total.toString());
		
		h = jobDAO.getTotalDaysSpent(5l);
		assertEquals("0", h.toString());
	}
}
