/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.PermissionDAOImpl;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.util.TestHelperUtil;

public class PermissionDAOTest extends BaseDAOTest<Permission> {

    private PermissionDAOImpl dao = new PermissionDAOImpl();
    
    private TestHelperUtil helper;

    @Override
    protected BaseDAOWithJPA<Permission> getDao() {
        return dao;
    }
    
    @Before
    public void setUpTestHelper(){
    	helper = new TestHelperUtil(em);
    }

    @Test
    public void testListPermission() {
        PermissionFilter filter = new PermissionFilter();

        // test with empty filter criteria, should find all permissions
        List<Permission> permissions = dao.listPermission(filter);
        assertNotNull(permissions);
        assertEquals(helper.countAllPermissions(null), permissions.size());

        // test with role id's filter criteria (1L = Mitarbeiter, 3L = Projektleiter), should find all distinct
        // permissions which are assigned to at least one of given roles
        List<Long> roleIds = Arrays.asList(new Long[] { 1L, 3L });
        filter = new PermissionFilter();
        filter.setRoleIds(roleIds);
        permissions = dao.listPermission(filter);
        assertNotNull(permissions);
        assertEquals(helper.countPermissionsByRoles(roleIds), permissions.size());

        // test with resource id's filter criteria, should find all distinct permissions which are assigned to at least
        // one of given resources
        List<Long> resIds = Arrays.asList(new Long[] { 1L, 2L, 3L });
        filter = new PermissionFilter();
        filter.setResourceIds(resIds);
        permissions = dao.listPermission(filter);
        assertNotNull(permissions);
        assertEquals(helper.countPermissionsByResources(resIds), permissions.size());

        // test with permission id's filter criteria, should find all permissions which are given in filter
        List<Long> permIds = Arrays.asList(new Long[] { 1L, 2L, 3L });
        filter = new PermissionFilter();
        filter.setPermissionIds(permIds);
        permissions = dao.listPermission(filter);
        assertNotNull(permissions);
        assertEquals(helper.countAllPermissions(permIds), permissions.size());
    }

    @Test
    public void testFindByActionId() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException {

        // test to find all permissions that are given in class PermissionIds
        @SuppressWarnings("rawtypes")
        Class permissionIds = Class.forName(PermissionIds.class.getName());
        Field[] actionIds = permissionIds.getDeclaredFields();
        for (Field actionId : actionIds) {
            try {
                checkPermissionExists((String) actionId.get(null));
            } catch (IllegalAccessException e) {
                // the IllegalAccesException should only be thrown when running test with eclipse EclEmma plug-in
            }
        }

        // test to find a permission that does not exist
        Permission perm = dao.findByActionId("NOT_EXIST");
        assertNull(perm);
    }

    @Test
    public void testCountPermissions() {
        int expectedCount = helper.countAllPermissions(null);
        int count = dao.countPermissions().intValue();

        assertEquals(expectedCount, count);
    }
    
    @Test
    public void testFindByResourceAndPermission(){
    	// we now from testdata.sql that user 2 has some permissions via direct assignment
        Permission perm = dao.findByResourceAndPermission(2L, PermissionIds.ACTIVITY_VIEW_ALL);
        assertNotNull(perm);
        
        perm = dao.findByResourceAndPermission(2L, PermissionIds.CUSTOMER_EDIT_ALL);
        assertNotNull(perm);
        
        perm = dao.findByResourceAndPermission(2L, PermissionIds.SKILL_ADD);
        assertNotNull(perm);
        
        // now test some search where we don't expect a find
        perm = dao.findByResourceAndPermission(2L, PermissionIds.SKILL_VIEW_ALL);
        assertNull(perm);
        
        perm = dao.findByResourceAndPermission(2L, PermissionIds.COST_VIEW_ALL_DETAIL);
        assertNull(perm);
    }

    protected void checkPermissionExists(String actionId) {
        Permission perm = dao.findByActionId(actionId);
        assertNotNull("Could not find Permission with actionId " + actionId, perm);
        assertEquals(actionId, perm.getActionId());
    }
}
