/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.EmploymentDAOImpl;
import de.tarent.activity.domain.Employment;
import de.tarent.activity.util.XMLDebug;

/**
 * Test class for EmploymentDAOImpl class.
 * 
 */
public class EmploymentDAOTest extends BaseDAOTest<Employment> {

	private EmploymentDAOImpl employmentDao = new EmploymentDAOImpl();

	@Override
	protected BaseDAOWithJPA<Employment> getDao() {
		return employmentDao;
	}

	/**
	 * Gets Employment by id.
	 */
	@Test
	public void testGetEmployment() {
		XMLDebug.printXML(employmentDao.find(1L));
	}

	/**
	 * List all Employments.
	 */
	@Test
	public void testGetEmploymentList() {
		XMLDebug.printXML(employmentDao.findAll());
	}

}
