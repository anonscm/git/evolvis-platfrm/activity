/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.Date;

import org.junit.Test;


import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.PositionDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.TimerDAOImpl;
import de.tarent.activity.domain.Timer;
import de.tarent.activity.util.XMLDebug;

/**
 * This is a test class to test TimerDAOImpl class.
 * 
 */
public class TimerDAOTest extends BaseDAOTest<Timer> {

	private TimerDAOImpl timerDao = new TimerDAOImpl();

	@Override
	protected BaseDAOWithJPA<Timer> getDao() {
		return timerDao;
	}

	/**
	 * Gets timer for a specific resource.
	 */
	@Test
	public void testFindByResource() {
		Timer timer = timerDao.findByResource(2L);
		XMLDebug.printXML(timer);
	}

	@Test
	public void testAddTimer() {
		Timer timer = new Timer();
		Long resourceId = 1L;
		ResourceDAOImpl resDao = new ResourceDAOImpl();
		PositionDAOImpl positionDao = new PositionDAOImpl();
		positionDao.setEntityManager(em);
		timer.setPosition(positionDao.find(1L));
		timer.setTime(new Date());
		timerDao.setEntityManager(em);
		while (timerDao.findByResource(resourceId) != null) {
			resourceId++;
		}
		resDao.setEntityManager(em);
		timer.setResource(resDao.find(resourceId));
		timerDao.create(timer);
		em.flush();
		em.refresh(timer);
		XMLDebug.printXML(timer);

	}

}
