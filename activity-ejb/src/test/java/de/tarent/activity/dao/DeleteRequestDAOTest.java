/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.DeleteRequestDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * Test class for DeleteRequestDAOImpl class.
 * 
 */
public class DeleteRequestDAOTest extends BaseDAOTest<DeleteRequest> {

	private DeleteRequestDAOImpl requestDao = new DeleteRequestDAOImpl();

	@Override
	protected BaseDAOWithJPA<DeleteRequest> getDao() {
		return requestDao;
	}

	/**
	 * List DeleteRequests by filter.
	 */
	@Test
	public void testListDeleteRequest() {
		DeleteRequestFilter filter = new DeleteRequestFilter(1L, null, null, null, null, null, null);
		List<DeleteRequest> deleteRequests = requestDao.listDeleteRequests(filter);
		XMLDebug.printXML(deleteRequests);
	}

	/**
	 * Count DeleteRequests by filter.
	 */
	@Test
	public void testCountDeleteRequests() {
		DeleteRequestFilter filter = new DeleteRequestFilter(1L, null, null, null, null, null, null);
		Long count = requestDao.countDeleteRequests(filter);
		XMLDebug.printXML(count);
	}

	/**
	 * Gets a Request by type and id.
	 */
	@Test
	public void testGetRequestByTypeAndId() {
		DeleteRequest deleteRequest = requestDao.getRequestByTypeAndId("activity", 1L);
		XMLDebug.printXML(deleteRequest);
	}

	/**
	 * Add a new DeleteRequest.
	 */
	@Test
	public void testAddDeleteRequest() {
		DeleteRequest deleteRequest = new DeleteRequest();
		deleteRequest.setDescription("description");
		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(em);
		Resource resource = resourceDao.findAll().get(0);
		deleteRequest.setController(resource);
		deleteRequest.setResource(resource);
		Long pk = requestDao.create(deleteRequest);
		XMLDebug.printXML(pk);

	}

	/**
	 * Gets a DeleteRequest by id.
	 */
	@Test
	public void findDeleteRequest() {
		XMLDebug.printXML(requestDao.find(2L));
	}

}
