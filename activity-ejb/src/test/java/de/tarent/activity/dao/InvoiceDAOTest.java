/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.InvoiceDAOImpl;
import de.tarent.activity.dao.impl.JobDAOImpl;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is the test class for InvoiceDAOImpl class.
 * 
 */
public class InvoiceDAOTest extends BaseDAOTest<Invoice> {

	private InvoiceDAOImpl invoiceDao = new InvoiceDAOImpl();

	@Override
	protected BaseDAOWithJPA<Invoice> getDao() {

		return invoiceDao;
	}

	/**
	 * List invoices by filter.
	 */
	@Test
	public void testListInvoice() {
		// assert that some invoices are in database, to test
		long count = helper.countInvoices();
		assertTrue(count >= 8);
		
		// test with empty filter
		InvoiceFilter filter = new InvoiceFilter(null, null, null, null, null, null, null, null);
		List<Invoice> invoices = invoiceDao.listInvoice(filter);
		assertNotNull(invoices);
		assertEquals(count, invoices.size());
	}
	
	/**
	 * List invoices by filter.
	 */
	@Test
	public void testListInvoiceByProject() {
		// assert that some invoices are in database, to test
		long count = helper.countInvoices();
		assertTrue(count >= 9);
		
		// test with empty filter
		InvoiceFilter filter = new InvoiceFilter(null, 1L, null, null, null, null, null, null);
		List<Invoice> invoices = invoiceDao.listInvoice(filter);
		assertNotNull(invoices);
		assertEquals(9, invoices.size());
	}

	/**
	 * Count invoices by filter.
	 */
	@Test
	public void testCountInvoice() {
		InvoiceFilter filter = new InvoiceFilter(1L, null, null, 0, null, null, null, null);
		Long nr = invoiceDao.countInvoice(filter);
		XMLDebug.printXML(nr);
	}

	/**
	 * List invoices by job.
	 */
	@Test
	public void testFindByJob() {
		List<Invoice> invoices = invoiceDao.findByJob(1L);
		XMLDebug.printXML(invoices);
	}

	/**
	 * Gets an invoice by id.
	 */
	@Test
	public void testGetInvoice() {
		XMLDebug.printXML(invoiceDao.find(1L));
	}

	/**
	 * Add a new invoice.
	 */
	@Test
	public void testAddInvoice() {
		Invoice invoice = new Invoice();
		JobDAOImpl jobDao = new JobDAOImpl();
		jobDao.setEntityManager(em);
		invoice.setJob(jobDao.find(1L));
		invoice.setAmount(new BigDecimal(10));
		invoice.setCrDate(new Date());
		invoice.setName("New invoice");
		invoiceDao.create(invoice);
		em.flush();
		em.refresh(invoice);
		XMLDebug.printXML(invoice);
	}

	/**
	 * List invoices by project.
	 */
	@Test
	public void testInvoiceByProject() {

		XMLDebug.printXML(invoiceDao.invoicesByProject(2L).size());
	}

}
