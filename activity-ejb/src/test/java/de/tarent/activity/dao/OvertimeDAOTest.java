/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import de.tarent.activity.dao.impl.BaseDAOWithJPA;
import de.tarent.activity.dao.impl.OvertimeDAOImpl;
import de.tarent.activity.dao.impl.OvertimeStatusDAOImpl;
import de.tarent.activity.dao.impl.ProjectDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.filter.OvertimeFilter;
import de.tarent.activity.util.XMLDebug;

/**
 * This is test class for OvertimeDAOImpl class.
 * 
 */
public class OvertimeDAOTest extends BaseDAOTest<Overtime> {

	private OvertimeDAOImpl overtimeDao = new OvertimeDAOImpl();

	@Override
	protected BaseDAOWithJPA<Overtime> getDao() {
		return overtimeDao;
	}

	/**
	 * List filtered overtimes.
	 * @throws ParseException throws Parse Exception for date
	 */
	@Test
	public void testListOvertimes() throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date start = dfm.parse("2000-02-26");
		OvertimeFilter f = new OvertimeFilter(1l, 2l, -1L, start, null, null, null, "prj.name", "asc");
		List<Overtime> overtimes = overtimeDao.listOvertime(f);
		XMLDebug.printXML(overtimes);
	}
	
	/**
	 * Count filtered overtimes.
	 * @throws ParseException throws Parse Exception for date
	 */
	@Test
	public void testCountOvertimes() throws ParseException {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		Date start = dfm.parse("2000-02-26");
		OvertimeFilter f = new OvertimeFilter(1l, 2l, -1L, start, null, null, null, "prj.name", "asc");
		Long nr = overtimeDao.countOvertime(f);
		XMLDebug.printXML(nr);
	}

	/**
	 * Gets overtime by id.
	 */
	@Test
	public void testOvertimeById() {
		Overtime o = overtimeDao.find(1l);
		XMLDebug.printXML(o);
	}

	/**
	 * Add a new overtime.
	 */
	@Test
	public void testAddOvertime() {
		Overtime overtime = new Overtime();
		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(em);
		overtime.setResource(resourceDao.find(2L));
		ProjectDAOImpl projectDao = new ProjectDAOImpl();
		projectDao.setEntityManager(em);
		overtime.setProject(projectDao.find(1L));
		OvertimeStatusDAOImpl overtimeStatusDao = new OvertimeStatusDAOImpl();
		overtimeStatusDao.setEntityManager(em);
		overtime.setOvertimeStatus(overtimeStatusDao.find(1L));
		overtime.setCrDate(new Date());
		overtime.setNote("new resource");
		overtimeDao.create(overtime);
		em.flush();
		em.refresh(overtime);
		XMLDebug.printXML(overtime);
	}
	
	/**
	 * Get total overtime hours.
	 */
	@Test
	public void testSumAllOvertimeById() {
		XMLDebug.printXML(overtimeDao.getAllOvertimeHours(2L));
		
	}

}
