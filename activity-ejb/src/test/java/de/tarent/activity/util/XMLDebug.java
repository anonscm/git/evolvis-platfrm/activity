/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.util;

import java.io.Reader;
import java.io.Writer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Utility class for xml serialization. Usefull for logging complex objects.
 * 
 */
public final class XMLDebug {

	/**
	 * XMLDebug constructor.
	 */
	private XMLDebug() {
	}

	/**
	 * Internal XStream instance.
	 */
	private static final XStream XSTREAM = init();

	/**
	 * Serialize an object into a xml string.
	 * 
	 * @param o  - input object
	 * @return xml string
	 */
	public static String toXML(Object o) {
		return XSTREAM.toXML(o);
	}

	/**
	 * Serialize an object to the given Writer.
	 * 
	 * @param o
	 *            - input object
	 * @param out
	 *            - Writer
	 * 
	 */
	public static void toXML(Object o, Writer out) {
		XSTREAM.toXML(o, out);
	}

	/**
	 * Deserialize an object from an xml string.
	 * 
	 * @param xml
	 *            input string
	 * @return the deserialized object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T fromXML(String xml) {
		if (xml == null) {
			return null;
		}
		return (T) XSTREAM.fromXML(xml);
	}

	/**
	 * Deserialize an object from an xml string.
	 * 
	 * @param reader
	 *            the reader to read from
	 * @return the deserialized object
	 */
	public static Object fromXML(Reader reader) {
		return XSTREAM.fromXML(reader);
	}

	/**
	 * Print to console an object in xml form.
	 * 
	 * @param o
	 *            the object to print.
	 */
	public static void printXML(Object o) {
		System.out.println(toXML(o));
	}

	/**
	 * Initialize the XStream object. Put here any customization required. For
	 * example add new Converters.
	 * 
	 * @return
	 */
	private static XStream init() {
		XStream x = new XStream(new DomDriver());
		return x;
	}
}
