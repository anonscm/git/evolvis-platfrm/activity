/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class TestHelperUtil {
	
	private EntityManager em;
	
	public TestHelperUtil(EntityManager em){
		this.em = em;
	}

	public int countAllPermissions(List<Long> permIds) {
	    StringBuilder hql = new StringBuilder("select count(p) from Permission p");
	    if(permIds != null){
	        hql.append(" where p.pk in (:resIds)");
	    }
	    
        Query query = em.createQuery(hql.toString());
        if(permIds != null){
            query.setParameter("resIds", permIds);
        }

        return ((Long) query.getSingleResult()).intValue();
    }

	public long countPermissionsByRoles(List<Long> roleIds) {
        Query query = em
                .createQuery("select count(distinct p) from Permission p join p.roles r where r.pk in (:roleIds)");
        query.setParameter("roleIds", roleIds);

        return (Long) query.getSingleResult();
    }

	public long countPermissionsByResources(List<Long> resIds) {
        Query query = em
                .createQuery("select count(distinct p) from Permission p join p.resources r where r.pk in (:resIds)");
        query.setParameter("resIds", resIds);

        return (Long) query.getSingleResult();
    }
	
	public long countInvoices(){
		Query query = em
                .createQuery("select count(distinct i) from Invoice i");

        return (Long) query.getSingleResult();
	}
}
