/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.ReportDAOImpl;
import de.tarent.activity.domain.Report;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ReportService;
import de.tarent.activity.util.TestUserUtilImpl;

public class ReportServiceImplTest extends BaseServiceImplTest{
	
	private ReportService reportService;
	
	@Before
    public void setUp() {
        JavaEEGloss gloss = new JavaEEGloss();
        
        ReportDAOImpl reportDao = new ReportDAOImpl();
        reportDao.setEntityManager(entityManager);
        reportDao.setUserUtil(new TestUserUtilImpl());

        gloss.addEJB(reportDao);
        
        reportService = gloss.make(ReportServiceImpl.class);
    }
	
	@Test
    public void testGetReport() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Report report = new Report();
        report.setName("Test Report");
        report.setData("Test Data");
        report.setFkResource(1l);
        report.setPermission(1l);
        report.setCrDate(startDate);
        long pk = reportService.addReport(report);
        Report report2 = reportService.getReport(pk);
        assertNotNull(report2);
        
        boolean thrown = false;
        try {
        	reportService.getReport(121211221l);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
    }
	
	@Test
    public void testUpdateReport() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Report report = new Report();
        report.setName("Test Report");
        report.setData("Test Data");
        report.setFkResource(1l);
        report.setPermission(1l);
        report.setCrDate(startDate);
        long pk = reportService.addReport(report);
        
        Report report2 = reportService.getReport(pk);
        report2.setName("Neuer Name");
        reportService.updateReport(report2);
        
        assertTrue(report.getName() == report2.getName());
    }
	
	@Test
    public void testDeleteReport() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Report report = new Report();
        report.setName("Test Report");
        report.setData("Test Data");
        report.setFkResource(1l);
        report.setPermission(1l);
        report.setCrDate(startDate);
        long pk = reportService.addReport(report);
        
        reportService.deleteReport(pk);
        
        boolean thrown = false;
        try {
        	reportService.getReport(pk);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);       
    }
	
	@Test
    public void testSearchReports() {
		FilterResult<Report> res = reportService.searchReports(new BaseFilter(null, null, null, null));
		assertNotNull(res);
    }

}
