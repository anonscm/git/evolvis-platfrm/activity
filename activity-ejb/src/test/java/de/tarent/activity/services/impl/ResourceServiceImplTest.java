/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.BranchOfficeDAOImpl;
import de.tarent.activity.dao.impl.DeleteRequestDAOImpl;
import de.tarent.activity.dao.impl.EmploymentDAOImpl;
import de.tarent.activity.dao.impl.FunctionTypeDAOImpl;
import de.tarent.activity.dao.impl.PermissionDAOImpl;
import de.tarent.activity.dao.impl.PosResourceStatusDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.ResourceFTypeMappingDAOImpl;
import de.tarent.activity.dao.impl.ResourceTypeDAOImpl;
import de.tarent.activity.dao.impl.RoleDAOImpl;
import de.tarent.activity.dao.impl.SkillsDAOImpl;
import de.tarent.activity.dao.impl.SkillsDefDAOImpl;
import de.tarent.activity.dao.impl.VresActiveProjectMappingDAOImpl;
import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Employment;
import de.tarent.activity.domain.FunctionType;
import de.tarent.activity.domain.PosResourceStatus;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceFTypeMapping;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.SkillsDef;
import de.tarent.activity.domain.VresActiveProjectMapping;
import de.tarent.activity.domain.filter.AllSkillsFilter;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.domain.filter.SkillsFilter;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.util.TestUserUtilImpl;

/**
 * Tests for PermissionServiceImpl. The tests assumes sometimes that at least some roles and permissions are stored in
 * database, which is the case when you run dafaultdata.sql on database.
 */
public class ResourceServiceImplTest extends BaseServiceImplTest {

    // Class to test
    private ResourceService resourceService;
    

    /**
     * Initialize EasyGloss container and begin transaction.
     */
    @Before
    public void setUp() {
        JavaEEGloss gloss = new JavaEEGloss();
        
        VresActiveProjectMappingDAOImpl vresMappingDao = new VresActiveProjectMappingDAOImpl();
        vresMappingDao.setEntityManager(entityManager);
        
        FunctionTypeDAOImpl functionTypeDao = new FunctionTypeDAOImpl();
        functionTypeDao.setEntityManager(entityManager);
        functionTypeDao.setUserUtil(new TestUserUtilImpl());
        
        EmploymentDAOImpl employmentDao = new EmploymentDAOImpl();
        employmentDao.setEntityManager(entityManager);
        employmentDao.setUserUtil(new TestUserUtilImpl());
        
        ResourceFTypeMappingDAOImpl resourceFTypeMappingDao = new ResourceFTypeMappingDAOImpl();
        resourceFTypeMappingDao.setEntityManager(entityManager);
        resourceFTypeMappingDao.setUserUtil(new TestUserUtilImpl());
        
        BranchOfficeDAOImpl branchOfficeDao = new BranchOfficeDAOImpl();
        branchOfficeDao.setEntityManager(entityManager);
        branchOfficeDao.setUserUtil(new TestUserUtilImpl());
        
        ResourceTypeDAOImpl resourceTypeDao = new ResourceTypeDAOImpl();
        resourceTypeDao.setEntityManager(entityManager);
        resourceTypeDao.setUserUtil(new TestUserUtilImpl());
        
        DeleteRequestDAOImpl deleteRequestDao = new DeleteRequestDAOImpl();
        deleteRequestDao.setEntityManager(entityManager);
        deleteRequestDao.setUserUtil(new TestUserUtilImpl());
        
        RoleDAOImpl roleDao = new RoleDAOImpl();
        roleDao.setEntityManager(entityManager);
        roleDao.setUserUtil(new TestUserUtilImpl());
        
        SkillsDefDAOImpl skillsDefDao =  new SkillsDefDAOImpl();
        skillsDefDao.setEntityManager(entityManager);
        skillsDefDao.setUserUtil(new TestUserUtilImpl());
        
        PosResourceStatusDAOImpl posResourceStatusDao = new PosResourceStatusDAOImpl();
        posResourceStatusDao.setEntityManager(entityManager);
        posResourceStatusDao.setUserUtil(new TestUserUtilImpl());
        
        PermissionDAOImpl permissionDao = new PermissionDAOImpl();
        permissionDao.setEntityManager(entityManager);
        permissionDao.setUserUtil(new TestUserUtilImpl());

        ResourceDAOImpl resourceDao = new ResourceDAOImpl();
        resourceDao.setEntityManager(entityManager);
        resourceDao.setUserUtil(new TestUserUtilImpl());
        
        SkillsDAOImpl skillDao = new SkillsDAOImpl();
        skillDao.setEntityManager(entityManager);
        skillDao.setUserUtil(new TestUserUtilImpl());

        gloss.addEJB(roleDao);
        gloss.addEJB(permissionDao);
        gloss.addEJB(resourceDao);
        gloss.addEJB(skillDao);
        gloss.addEJB(posResourceStatusDao);
        gloss.addEJB(skillsDefDao);
        gloss.addEJB(deleteRequestDao);
        gloss.addEJB(resourceTypeDao);
        gloss.addEJB(branchOfficeDao);
        gloss.addEJB(resourceFTypeMappingDao);
        gloss.addEJB(employmentDao);
        gloss.addEJB(functionTypeDao);
        gloss.addEJB(vresMappingDao);
        
        resourceService = gloss.make(ResourceServiceImpl.class);
    }

    @Test
    public void testAddRequest() {
        long request = resourceService.addRequest(new DeleteRequest());
        DeleteRequest request2 = resourceService.getRequest(request);
        assertNotNull(request2);
    }

    @Test
    public void testAddResource() throws UniqueValidationException {
        Resource resource = new Resource();
        resource.setFirstname("test_firstname");
        resource.setLastname("Test_lastname");
        resource.setPassword("test");
        long pk = resourceService.addResource(resource);
        Resource res = resourceService.getResource(pk, true, true);
        assertNotNull(res);
        
    }

    @Test
    public void testAddResourceFTypeMapping() {
    	Long mapping = resourceService.addResourceFTypeMapping(new ResourceFTypeMapping());
    	
    	assertNotNull(mapping);
    }

    @Test
    public void testAddSkill() {
        Skills skills = new Skills();
        skills.setResource(resourceService.getResource(1l, true, true));
        long pk = resourceService.addSkill(skills);
        Skills skills2 = resourceService.getSkills(pk);
        assertNotNull(skills2);
        
        Skills skills3 = resourceService.getSkills(190000l);
        assertNull(skills3);
    }

    @Test
    public void testAddSkillDef() {
    	Long skill_def = resourceService.addSkillDef(new SkillsDef());
    	assertNotNull(skill_def);
    }

    @Test
    public void testDeleteRequest() {
        //TODO
    }

    @Test
    public void testDeleteResourceFTypeMappingByResource() {
        // TODO
    }

    @Test
    public void testDeleteSkill() {
        Skills skill = new Skills();
        Resource res = new Resource();
        res.setFirstname("Shrek");
        res.setLastname("Test");
        res.setPassword("Test");
        skill.setResource(res);
        SkillsDef def = new SkillsDef();
        def.setName("Dotraki");
        skill.setSkillsDef(def);
        long pk = resourceService.addSkill(skill);
        assertNotNull(skill);
        resourceService.deleteSkill(pk);
        skill = resourceService.getSkills(pk);
        assertNull(skill);
        
        boolean thrown = false;
        try {
            resourceService.deleteSkill(126321L);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
        
    }

    @Test
    public void testGetAllProjectManager() {
    	List<Resource> manager = resourceService.getAllProjectManager();
    	assertFalse(manager.isEmpty());
    }

    @Test
    public void testGetBranchOffice() {
    	BranchOffice office = resourceService.getBranchOffice(1l);
    	assertNotNull(office);
    }

    @Test
    public void testGetBranchOfficeList() {
    	List<BranchOffice> office = resourceService.getBranchOfficeList();
    	assertFalse(office.isEmpty());
    }

    @Test
    public void testGetByPosition() {
    	List<Resource> res = resourceService.getByPosition(1l);
    	assertFalse(res.isEmpty());
    }

    @Test
    public void testGetEmployment() {
    	Employment employ = resourceService.getEmployment(1l);
    	assertNotNull(employ);
    }

    @Test
    public void testGetEmploymentList() {
    	List<Employment> employ = resourceService.getEmploymentList();
    	assertNotNull(employ);
    	assertFalse(employ.isEmpty());
    }

    @Test
    public void testGetFunctionTypeList() {
    	List<FunctionType> type = resourceService.getFunctionTypeList();
    	assertNotNull(type);
    	assertFalse(type.isEmpty());
    }

    @Test
    public void testGetMD5Hash() {
    	//TODO
    }

    @Test
    public void testGetRequest() {
    	DeleteRequest tmp = new DeleteRequest();
    	tmp.setResource(resourceService.getResource(2l, true, true));
    	tmp.setController(resourceService.getResource(1l, true, true));
    	tmp.setDescription("TEST");
    	long pk = resourceService.addRequest(tmp);
    	DeleteRequest request = resourceService.getRequest(pk);
    	assertNotNull(request);
    }

    @Test
    public void testGetRequestByTypeAndId() {
    	DeleteRequest request = resourceService.getRequestByTypeAndId(null, null);
    	assertNull(request);
    }

    @Test
    public void testGetResource() {
        Resource res = resourceService.getResource(1l, true, true);
        assertNotNull(res);
    }

    @Test
    public void testGetResourceFromProject() {
    	List<Resource> res = resourceService.getResourceFromProject(1l);
    	assertFalse(res.isEmpty());
    }

    @Test
    public void testGetResourceFTypeMapping() throws UniqueValidationException {
//    	WIP
//    	Resource res = new Resource();
//    	res.setFirstname("Testvor");
//    	res.setLastname("Testnach");
//    	res.setPassword("test");
//    	long pk = resourceService.addResource(res);
//    	FunctionType type = new FunctionType(1l);
//    	ResourceFTypeMapping mapping = new ResourceFTypeMapping();
//    	mapping.setFunctionType(type);
//    	mapping.setResource(res);
//    	mapping.setCrUser("denge");
//    	resourceService.addResourceFTypeMapping(mapping);
//    	List<ResourceFTypeMapping> res_map = resourceService.getResourceFTypeMapping(pk);
//    	assertNotNull(res_map);
//    	assertFalse(res_map.isEmpty());
    }

    @Test
    public void testGetResourceList() {
        List<Resource> res = resourceService.getResourceList();
        assertFalse(res.isEmpty());
    }

    @Test
    public void testGetResourceProjectByStatus() {
    	List<VresActiveProjectMapping> res_pro = resourceService.getResourceProjectByStatus(1l, 1l);
    	assertNotNull(res_pro);
    	assertFalse(res_pro.isEmpty());
    }

    @Test
    public void testGetResourceType() throws UniqueValidationException {
    	ResourceType resType = resourceService.getResourceType(1l);
    	assertNotNull(resType);
    }

    @Test
    public void testGetResourceTypeList() {
    	List<ResourceType> res_type = resourceService.getResourceTypeList();
    	assertNotNull(res_type);
    	assertFalse(res_type.isEmpty());
    }

    @Test
    public void testGetSkills() {
        Skills skills = resourceService.getSkills(1l);
        assertNotNull(skills);
    }

    @Test
    public void testGetSkillsDefByName() {
    	SkillsDef skillsDef = resourceService.getSkillsDefByName("java");
    	assertNotNull(skillsDef);
    }

    @Test
    public void testGetUserSkillBySkillDef() {
        Skills skills = resourceService.getSkills(1l);
        assertNotNull(skills);
        
    }

    @Test
    public void testListActiveResources() {
    	List<Resource> res = resourceService.listActiveResources();
    	assertFalse(res.isEmpty());
    }

    @Test
    public void testLoadResource() {
        Resource res = resourceService.loadResource("denge");
        assertNotNull(res);
    }

    @Test
    public void testManagerList() {
    	List<Resource> res = resourceService.managerList(1l);
    	assertNotNull(res);
    }

    @Test
    public void testPosResourceStatusList() {
    	List<PosResourceStatus> pos_res = resourceService.posResourceStatusList();
    	assertNotNull(pos_res);
    	assertFalse(pos_res.isEmpty());
    }

    @Test
    public void testSearchAllSkills() {
    	AllSkillsFilter filter = new AllSkillsFilter("denge", null, 1l, 1l, null, null);
    	FilterResult<Skills> result = resourceService.searchAllSkills(filter);
    	assertNotNull(result);
    }

    @Test
    public void testSearchRequest() {
    	DeleteRequestFilter filter = new DeleteRequestFilter(null, null, null, null, null, null, null);
    	FilterResult<DeleteRequest> result = resourceService.searchRequest(filter);
    	assertNotNull(result);
    }

//    @Test
//    public void testDeleteResource() throws UniqueValidationException{
//    	Resource resource = new Resource();
//    	resource.setResourceType(new ResourceType(1L));
//    	resource.setFirstname("jochen");
//    	resource.setLastname("schweitzer");
//    	resource.setPassword("test");
//
//    	resource.setLossesForFkResource(lossesForFkResource);
//    	resource.setMail(mail);
//    	resource.setNote(note);
//    	resource.setOvertimeHours(overtimeHours);
//    	resource.setOvertimesForDecidedBy(overtimesForDecidedBy);
//    	resource.setPosResourceMappings(posResourceMappings);
//    	resource.setRemainHoliday(remainHoliday);
//    	resource.setResourceFTypeMappings(resourceFTypeMappings);
//    	resource.setSalery(salery);
//    	resource.setTimers(timers);
//    	resource.setUpdDate(updDate);
//    	resource.setUpdUser(updUser);
//    	resource.setUsername(username);
//    	
//    	resource.setAvailableHours(4L);
//    	resource.setActive('t');
//    	
//    	List<Activity> activity = new ArrayList<Activity>();
//    	Activity activities = new Activity();
//    	activities.setPk(1L);
//    	activity.add(activities);
//    	resource.setActivities(activity);
//    	
//    	Date date = new Date();
//    	resource.setBirth(date);
//    	
//    	BranchOffice office = new BranchOffice();
//    	office.setPk(1L);
//    	resource.setBranchOffice(office);
//    	
//    	BigDecimal costperhour = new BigDecimal(100);
//    	resource.setCostPerHour(costperhour);
//    	
//    	List<Cost> cost = new ArrayList<Cost>();
//    	Cost costs = new Cost();
//    	BigDecimal costBD = new BigDecimal(100);
//    	costs.setCost(costBD);
//    	cost.add(costs);
//    	resource.setCosts(cost);
//    	
//    	Set<Permission> permissions = new HashSet<Permission>();
//    	Permission permission = new Permission();
//    	permission.setPk(1L);
//    	permissions.add(permission);
//    	resource.setPermissions(permissions);
//
//    	Set<Role> roles = new HashSet<Role>();
//    	Role role = new Role();
//    	role.setPk(1L);
//    	roles.add(role);
//    	resource.setRoles(roles);
//    	
//    	Date enteredDate = new Date();
//    	resource.setEntered(enteredDate);
//    	
//    	Date exitDate = new Date();
//    	resource.setExit(exitDate);
//    	
//    	resource.setFkTcid(50L);
//    	
//    	resource.setGroups("Test Gruppe");
//    	
//    	BigDecimal holidayBD = new BigDecimal(30);
//    	resource.setHoliday(holidayBD);
//    	
//    	
//    	List<Loss> lossesPl1 = new ArrayList<Loss>();
//    	Loss losses1 = new Loss();
//    	losses1.setPk(1L);
//    	lossesPl1.add(losses1);
//    	resource.setLossesForFkPl1(lossesPl1);
//    	
//    	List<Loss> lossesPl2 = new ArrayList<Loss>();
//    	Loss losses2 = new Loss();
//    	losses2.setPk(1L);
//    	lossesPl2.add(losses2);
//    	resource.setLossesForFkPl2(lossesPl2);
//    	
//    	List<Loss> lossesPl3 = new ArrayList<Loss>();
//    	Loss losses3 = new Loss();
//    	losses3.setPk(1L);
//    	lossesPl3.add(losses3);
//    	resource.setLossesForFkPl3(lossesPl3);
//    	
//    	List<Loss> lossesPl4 = new ArrayList<Loss>();
//    	Loss losses4 = new Loss();
//    	losses4.setPk(1L);
//    	lossesPl4.add(losses4);
//    	resource.setLossesForFkPl4(lossesPl4);
//    	
//    	List<Loss> lossesPl5 = new ArrayList<Loss>();
//    	Loss losses5 = new Loss();
//    	losses5.setPk(1L);
//    	lossesPl5.add(losses5);
//    	resource.setLossesForFkPl5(lossesPl5);
//    	
//    	Long pk = resourceService.addResource(resource);
//    	assertNotNull(pk);
//    	tearDownWithCommit();
//    	initEntityManager();
//		
////    	List<Skills> skills = new ArrayList<Skills>();
//    	Skills skill = new Skills();
//    	skill.setValue(1);
//    	skill.setSkillsDef(new SkillsDef(1L));
//    	skill.setResource(resource);
////    	skills.add(skill);
////    	resource.setSkills(skills);
//    	resourceService.addSkill(skill);
//    	tearDownWithCommit();
//    	initEntityManager();
//    	
//    	resource = resourceService.getResource(pk, false, false);
//    	assertTrue(resource.getSkills().size() == 1);
//    	
//    	resourceService.deleteRessource(pk);
//    	tearDownWithCommit();
//    	initEntityManager();
//    	
//    	resource = resourceService.getResource(pk, false, false);
//    	assertNull(resource);
//    	
//    }
    	
    @Test
    public void testSearchResource() {
        // an empty filter should return all resources
        ResourceFilter filter = new ResourceFilter();
        FilterResult<Resource> result = resourceService.searchResource(filter);
        assertNotNull(result);
        //changed size to 19 because of local resourceSize
        assertTrue(result.getRecords().size() == 19);

        // filter by role with primary key 9, should return two resources
        filter = new ResourceFilter();
        filter.setRoleIds(Arrays.asList(new Long[] { 9L }));
        result = resourceService.searchResource(filter);
        assertNotNull(result);
        //changed size to 3 because of local recordSize
        assertTrue(result.getRecords().size() == 3);

        // filter by resource primary keys should return three of search resources
        filter = new ResourceFilter();
        filter.setResourceIds(Arrays.asList(new Long[] { 1L, 2L, 9L, 10000L }));
        result = resourceService.searchResource(filter);
        assertNotNull(result);
        assertTrue(result.getRecords().size() == 3);

        // filter with all possible filter criteria: by role with pk 9 and by set of resource pk's, should return two
        // resources
        filter = new ResourceFilter(null, Arrays.asList(new Long[] { 9L }), Arrays.asList(new Long[] { 1L, 2L, 3L, 4L,
                5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L }), "resource.pk", "asc");
        result = resourceService.searchResource(filter);
        assertNotNull(result);
        assertTrue(result.getRecords().size() == 2);
    }

    @Test
    public void testSearchSkills() {
    	SkillsFilter filter = new SkillsFilter(1l, 1l, 1l, 1l, null, null);
    	FilterResult<Skills> skills = resourceService.searchSkills(filter);
    	assertNotNull(skills);
    }

    @Test
    public void testSkillsDefList() {
    	List<SkillsDef> skills = resourceService.skillsDefList();
    	assertNotNull(skills);
    }

    @Test
    public void testUpdateRequest() {
    	DeleteRequest request = new DeleteRequest();
    	request.setResource(resourceService.getResource(2l, true, true));
    	request.setController(resourceService.getResource(1l, true, true));
    	request.setDescription("TEST");
    	long pk = resourceService.addRequest(request);
    	
    	DeleteRequest request2 = resourceService.getRequest(pk);
    	request.setDescription("Description zum Testen");
    	resourceService.updateRequest(request2);
    	
    	assertTrue(request.getDescription() == request2.getDescription());
    }

    @Test
    public void testUpdateResource() throws UniqueValidationException {
//    	WIP
    	Resource res = resourceService.getResource(1l, true, true);
    	long pk = resourceService.addResource(res);
    	
    	Resource res2 = resourceService.getResource(pk, true, true);
    	res2.setFirstname("Test Firstname");
    	resourceService.updateResource(res2);
    	
    	System.out.println(res.getName());
    	System.out.println(res2.getName());
    	
    	assertTrue(res.getName().equals(res2.getName()));
    }

    @Test
    public void testUpdateSkill() {
        Skills skills = resourceService.getSkills(1l);
        long pk = resourceService.addSkill(skills);
        
        Skills skills2 = resourceService.getSkills(pk);
        skills2.setValue(999);
        resourceService.updateSkill(skills2);
        
        assertTrue(skills.getValue() == skills2.getValue());
    }

}
