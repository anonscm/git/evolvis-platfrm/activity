/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package de.tarent.activity.services.impl;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;
import de.tarent.activity.dao.impl.*;
import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.*;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.util.TestUserUtilImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author tschmi
 *
 */
public class ProjectServiceImplTest extends BaseServiceImplTest {
    
    // class to test
    private ProjectService projectService;
    
    @Before
    public void setUp() {
        JavaEEGloss gloss = new JavaEEGloss();
        
        TestUserUtilImpl userUtil = new TestUserUtilImpl();

        JobDAOImpl jobDAO = new JobDAOImpl();
        jobDAO.setEntityManager(entityManager);
        jobDAO.setUserUtil(userUtil);

        JobStatusDAOImpl jobStatusDAO = new JobStatusDAOImpl();
        jobStatusDAO.setEntityManager(entityManager);
        jobStatusDAO.setUserUtil(userUtil);
        
        JobTypeDAOImpl jobTypeDAO = new JobTypeDAOImpl();
        jobTypeDAO.setEntityManager(entityManager);
        jobTypeDAO.setUserUtil(userUtil);

        PositionDAOImpl positionDAO = new PositionDAOImpl();
        positionDAO.setEntityManager(entityManager);
        positionDAO.setUserUtil(userUtil);
        
        PosResourceMappingDAOImpl posResourceMappingDAO = new PosResourceMappingDAOImpl();
        posResourceMappingDAO.setEntityManager(entityManager);
        posResourceMappingDAO.setUserUtil(userUtil);
        
        PositionStatusDAOImpl positionStatusDAO = new PositionStatusDAOImpl();
        positionStatusDAO.setEntityManager(entityManager);
        positionStatusDAO.setUserUtil(userUtil);
        
        CostDAOImpl costDAO = new CostDAOImpl();
        costDAO.setEntityManager(entityManager);
        costDAO.setUserUtil(userUtil);
        
        ProjectDAOImpl projectDAO = new ProjectDAOImpl();
        projectDAO.setEntityManager(entityManager);
        projectDAO.setUserUtil(userUtil);
        
        CustomerDAOImpl customerDAO = new CustomerDAOImpl();
        customerDAO.setEntityManager(entityManager);
        customerDAO.setUserUtil(new TestUserUtilImpl());
        
        PosResourceStatusDAOImpl posResourceStatusDAO = new PosResourceStatusDAOImpl();
        posResourceStatusDAO.setEntityManager(entityManager);
        
        gloss.addEJB(jobDAO);
        gloss.addEJB(jobStatusDAO);
        gloss.addEJB(jobTypeDAO);
        gloss.addEJB(positionDAO);
        gloss.addEJB(posResourceMappingDAO);
        gloss.addEJB(positionStatusDAO);
        gloss.addEJB(costDAO);
        gloss.addEJB(projectDAO);
        gloss.addEJB(customerDAO);
        gloss.addEJB(posResourceStatusDAO);

        projectService = gloss.make(ProjectServiceImpl.class);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#addCustomer(de.tarent.activity.domain.Customer)}.
     */
    @Test
    public void testAddCustomer() {
        Customer customer = new Customer();
        customer.setName("Test-Customer");
        
        Long pk = projectService.addCustomer(customer);
        assertNotNull(pk);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#addJob(de.tarent.activity.domain.Job)}.
     * @throws UniqueValidationException 
     */
    @Test(expected = UniqueValidationException.class)
    public void testAddJob() throws UniqueValidationException {
        // new reference job
        Job refJob = new Job(null, "Test-Job", new JobType(ProjectService.JOBTYPE_INTERNALJOB),
                new Resource(1L), new Project(1L), new JobStatus(ProjectService.JOBSTATUS_ORDERED));
        
        // as all job required fields are set we expect no problems
        Job job = new Job(refJob);
        Long pk = projectService.addJob(job);
        assertNotNull(pk);
        
        // try to save a job with same name but with another project
        job = new Job(refJob);
        job.setProject(new Project(2L));
        pk = projectService.addJob(job);
        assertNotNull(pk);
        
        // trying to save a job with same name and same project should throw UniqueValidationException 
        job = new Job(refJob);
        pk = projectService.addJob(job);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#addPosition(de.tarent.activity.domain.Position)}.
     */
    @Test
    public void testAddPosition() {
        Position pos = new Position();
        
        Long pk = projectService.addPosition(pos);
        assertNotNull(pk);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#addPosResourceMapping(de.tarent.activity.domain.PosResourceMapping)}.
     */
    @Test
    public void testAddPosResourceMapping() {
        PosResourceMapping posRes = new PosResourceMapping(null, new Position(1L), new PosResourceStatus(1L), new Resource(1L));
        projectService.addPosResourceMapping(posRes);
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#addProject(de.tarent.activity.domain.Project)}.
     * @throws UniqueValidationException 
     */
    @Test
    public void testAddProject() throws UniqueValidationException {
        Project project = new Project();
        project.setName("Unit Test Projekt");
        project.setAccounts(2l);
        project.setContactPerson("Thomas");
        project.setCustomer(new Customer(1l));
        
        Long pk = projectService.addProject(project);
        
        assertNotNull(pk);
    }
    
    @Test(expected = UniqueValidationException.class)
    public void testAddProjectWithUniqueConstraintError() throws UniqueValidationException {
        Project project = new Project();
        project.setName("Unit Test Projekt");
        project.setAccounts(2l);
        project.setContactPerson("Thomas");
        project.setCustomer(new Customer(1l));
        
        Long pk = projectService.addProject(project);
        
        assertNotNull(pk);
        
        project = new Project();
        project.setName("Unit Test Projekt");
        project.setAccounts(2l);
        project.setContactPerson("Thomas");
        project.setCustomer(new Customer(1l));
        
        projectService.addProject(project);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#deleteCustomer(java.lang.Long)}.
     */
    @Test
    public void testDeleteCustomer() {
        Customer custumerDummy = new Customer();
        custumerDummy.setName("Dummy");
        List<Project> projects = projectService.getProjectList();
        custumerDummy.setProjects(projects);
        Long pk = projectService.addCustomer(custumerDummy);
        custumerDummy = projectService.getCustomer(pk);
        assertNotNull(custumerDummy);

        projectService.deleteCustomer(pk);
        custumerDummy = projectService.getCustomer(pk);
        assertNull(custumerDummy);
        
        boolean thrown = false;
        try {
            projectService.deleteCustomer(126321L);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#deleteJob(java.lang.Long)}.
     * @throws UniqueValidationException 
     */
    @Test
    public void testDeleteJob() throws UniqueValidationException {
    	//WIP Fehlermeldung bei projectService.deleteJob(pk)
//        Job job = new Job(null, "Neuer Job", new JobType(1L), new Resource(1L), new Project(1L), new JobStatus(1L));
//        Long pk = projectService.addJob(job);
//        Job job2 = projectService.getJob(pk);
//        assertNotNull(job2);
//        
//        projectService.deleteJob(pk);
//        Job job3 = projectService.getJob(pk);
//        assertNull(job3);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#deletePosition(java.lang.Long)}.
     */
    @Test
    public void testDeletePosition() {
//    	  WIP
//        Position pos1 = new Position();
//        pos1.setName("Position");
//        Job job = new Job(1l);
//        pos1.setJob(job);
//        long pk = projectService.addPosition(pos1);
//        Position pos2 = projectService.getPosition(pk);
//        assertNotNull(pos2);
//        projectService.deletePosition(pk);
//        Position pos3 = projectService.getPosition(pk);
//        assertNull(pos3);
//        
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#deletePosResourceMapping(java.lang.Long)}.
     */
    @Test
    public void testDeletePosResourceMapping() {
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#deleteProject(java.lang.Long)}.
     */
    @Test
    public void testDeleteProject() {
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#duplicateJob(de.tarent.activity.domain.Job)}.
     * @throws UniqueValidationException 
     */
    @Test
    public void testDuplicateJob() throws UniqueValidationException {
    	 Job refJob = new Job(null, "JobToDuplicate", new JobType(ProjectService.JOBTYPE_INTERNALJOB),
                 new Resource(1L), new Project(1L), new JobStatus(ProjectService.JOBSTATUS_ORDERED));
         Job job = new Job(refJob);
         projectService.addJob(job);
         Job newJob = projectService.duplicateJob(job);
       	 projectService.addJob(newJob);
         
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getActiveJobsByResource(java.lang.Long)}.
     */
    @Test
    public void testGetActiveJobsByResource() {
    	List<Job> job = projectService.getActiveJobsByResource(1l);
    	assertNotNull(job);
    	assertFalse(job.isEmpty());
    	
    	List<Job> job2 = projectService.getActiveJobsByResource(193235l);
    	assertTrue(job2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getAllActiveProjectList()}.
     */
    @Test
    public void testGetAllActiveProjectList() {
    	List<Project> project = projectService.getAllActiveProjectList();
    	assertNotNull(project);
    	assertFalse(project.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getAllPositionsByJob(java.lang.Long)}.
     */
    @Test
    public void testGetAllPositionsByJob() {
    	List<Position> position = projectService.getAllPositionsByJob(1l);
    	assertNotNull(position);
    	assertFalse(position.isEmpty());
    	
    	List<Position> position2 = projectService.getAllPositionsByJob(1928212l);
    	assertTrue(position2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getCostsByJob(java.lang.Long)}.
     */
    @Test
    public void testGetCostsByJob() {
    	List<Cost> costs = projectService.getCostsByJob(1l);
    	assertNotNull(costs);
    	assertFalse(costs.isEmpty());
    	
    	List<Cost> costs2 = projectService.getCostsByJob(1948384l);
    	assertTrue(costs2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getCustomer(java.lang.Long)}.
     */
    @Test
    public void testGetCustomer() {    	
    	Customer customer = projectService.getCustomer(1l);
    	assertNotNull(customer);	
    	
    	Customer customer2 = projectService.getCustomer(1832372l);
    	assertNull(customer2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getCustomerList()}.
     */
    @Test
    public void testGetCustomerList() {
    	List<Customer> customer = projectService.getCustomerList();
    	assertNotNull(customer);
    	assertFalse(customer.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJob(java.lang.Long)}.
     */
    @Test
    public void testGetJob() {
        Job job = projectService.getJob(1l);
        assertNotNull(job);
        boolean thrown = false;
        try {
        	projectService.getJob(194893l);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
        
        
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobChangeInfo(java.lang.Long)}.
     */
    @Test
    public void testGetJobChangeInfo() {
    	List<ChangeInfo> info = projectService.getJobChangeInfo(1l);
    	assertNotNull(info);
    	
    	List<ChangeInfo> info2 = projectService.getJobChangeInfo(193838l);
    	assertTrue(info2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobList()}.
     */
    @Test
    public void testGetJobList() {
        List<Job> job = projectService.getJobList();
        assertNotNull(job);
        assertFalse(job.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobListToManage()}.
     */
    @Test
    public void testGetJobListToManage() {
        List<Job> job = projectService.getJobListToManage();
        assertNotNull(job);
        assertFalse(job.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobsByProject(java.lang.Long)}.
     */
    @Test
    public void testGetJobsByProject() {
        List<Job> job = projectService.getJobsByProject(1l);
        assertNotNull(job);
        assertFalse(job.isEmpty());
        
        List<Job> job2 = projectService.getJobsByProject(1938378l);
        assertTrue(job2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobsByResource(java.lang.Long)}.
     */
    @Test
    public void testGetJobsByResource() {
        List<Job> job = projectService.getJobsByResource(1l);
        assertNotNull(job);
        assertFalse(job.isEmpty());
        
        List<Job> job2 = projectService.getJobsByResource(131331l);
        assertTrue(job2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobStatus(java.lang.Long)}.
     */
    @Test
    public void testGetJobStatus() {
        JobStatus jobStatus = projectService.getJobStatus(ProjectService.JOBSTATUS_ORDERED);
        assertNotNull(jobStatus);
        
        JobStatus jobStatus2 = projectService.getJobStatus(185483l);
        assertNull(jobStatus2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobStatusList()}.
     */
    @Test
    public void testGetJobStatusList() {
        List<JobStatus> jobStatus = projectService.getJobStatusList();
        assertNotNull(jobStatus);
        assertFalse(jobStatus.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobType(java.lang.Long)}.
     */
    @Test
    public void testGetJobType() {
        JobType jobType = projectService.getJobType(1l);
        assertNotNull(jobType);
        
        JobType jobType2 = projectService.getJobType(1332425l);
        assertNull(jobType2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getJobTypeList()}.
     */
    @Test
    public void testGetJobTypeList() {
        List<JobType> jobType = projectService.getJobTypeList();
        assertNotNull(jobType);
        assertFalse(jobType.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPosition(java.lang.Long)}.
     */
    @Test
    public void testGetPosition() {
        Position position = projectService.getPosition(1l);
        assertNotNull(position);
        
        boolean thrown = false;
        try {
        	projectService.getPosition(123244l);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPositionChangeInfo(java.lang.Long)}.
     */
    @Test
    public void testGetPositionChangeInfo() {
    	List<ChangeInfo> changeInfo = projectService.getJobChangeInfo(1l);
    	assertNotNull(changeInfo);
    	
    	List<ChangeInfo> changeInfo2 = projectService.getJobChangeInfo(193747l);
    	assertTrue(changeInfo2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPositionsByJob(java.lang.Long)}.
     */
    @Test
    public void testGetPositionsByJob() {
    	
    	//job and status
    	List<Position> position = projectService.getPositionsByJob(1l, 2L, null);
    	assertNotNull(position);
    	assertFalse(position.isEmpty());
    	
    	List<Position> position2 = projectService.getPositionsByJob(112131l, 2L, null);
    	assertTrue(position2.isEmpty());
    	
    	//job and resource
    	List<Position> position3 = projectService.getPositionsByJob(1l, null, 1l);
    	assertNotNull(position3);
    	
    	List<Position> position4 = projectService.getPositionsByJob(192392392l, null, 1l);
    	assertTrue(position4.isEmpty());
    	
    	List<Position> position5 = projectService.getPositionsByJob(1l, null, 1584858L);
    	assertTrue(position5.isEmpty());
    	
    	List<Position> position6 = projectService.getPositionsByJob(13424424l, null, 13213442l);
    	assertTrue(position6.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPositionStatus(java.lang.Long)}.
     */
    @Test
    public void testGetPositionStatus() {
        PositionStatus status = projectService.getPositionStatus(1l);
        assertNotNull(status);
        
        PositionStatus status2 = projectService.getPositionStatus(183723l);
        assertNull(status2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPosResourceMapping(java.lang.Long)}.
     */
    @Test
    public void testGetPosResourceMapping() {
    	PosResourceMapping posresmapping = projectService.getPosResourceMapping(1l);
    	assertNotNull(posresmapping);
    	
    	boolean thrown = false;
        try {
        	projectService.getPosResourceMapping(1656546l);        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
    	
    	
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPosResourceMappingsByPosition(java.lang.Long)}.
     */
    @Test
    public void testGetPosResourceMappingsByPosition() {
    	List<PosResourceMapping> posresmapping = projectService.getPosResourceMappingsByPosition(1l);
    	assertNotNull(posresmapping);
    	
    	List<PosResourceMapping> posresmapping2 = projectService.getPosResourceMappingsByPosition(134424l);
    	assertTrue(posresmapping2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getPosResourceMappingsByPositionAndResource(java.lang.Long, java.lang.Long)}.
     */
    @Test
    public void testGetPosResourceMappingsByPositionAndResource() {
    	PosResourceMapping posresmapping = projectService.getPosResourceMappingsByPositionAndResource(1l, 2l);
    	assertNotNull(posresmapping);
    	
    	PosResourceMapping posresmapping2 = projectService.getPosResourceMappingsByPositionAndResource(13434l, 2l);
    	assertNull(posresmapping2);
    	
    	PosResourceMapping posresmapping3 = projectService.getPosResourceMappingsByPositionAndResource(1l, 25435l);
    	assertNull(posresmapping3);
    	
    	PosResourceMapping posresmapping4 = projectService.getPosResourceMappingsByPositionAndResource(1323424l, 243434l);
    	assertNull(posresmapping4);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getProject(java.lang.Long)}.
     */
    @Test
    public void testGetProject() {
        Project projekt = projectService.getProject(1l);
        assertNotNull(projekt);
        
        Project projekt2 = projectService.getProject(14353l);
        assertNull(projekt2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getProjectChangeInfo(java.lang.Long)}.
     */
    @Test
    public void testGetProjectChangeInfo() {
    	List<ChangeInfo> changeInfo = projectService.getProjectChangeInfo(1l);
    	assertNotNull(changeInfo);
    	List<ChangeInfo> changeInfo2 = projectService.getProjectChangeInfo(1834823l);
    	assertTrue(changeInfo2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getProjectList()}.
     */
    @Test
    public void testGetProjectList() {
    	List<Project> project = projectService.getProjectList();
    	assertNotNull(project);
    	assertFalse(project.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#getProjectsByCustomer(java.lang.Long)}.
     */
    @Test
    public void testGetProjectsByCustomer() {
    	List<Project> project = projectService.getProjectsByCustomer(1l);
    	assertNotNull(project);
    	assertFalse(project.isEmpty());
    	
    	List<Project> project2 = projectService.getProjectsByCustomer(123131l);
    	assertTrue(project2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#isDeletableJob(java.lang.Long)}.
     */
    @Test
    public void testIsDeletableJob() {
        boolean deletable = projectService.isDeletableJob(1l);
        assertFalse(deletable);
        
        boolean deletable2 = projectService.isDeletableJob(18381383l);
        assertTrue(deletable2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#isDeletablePosition(java.lang.Long)}.
     */
    @Test
    public void testIsDeletablePosition() {
    	//Darf gelöscht werden
    	Position position1 = new Position();
    	position1.setName("Teeeest");
    	long pk = projectService.addPosition(position1);
    	boolean deletable = projectService.isDeletablePosition(pk);
    	assertTrue(deletable);
    	
    	boolean deletable2 = projectService.isDeletablePosition(1l);
    	assertFalse(deletable2);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#isDeletableProject(java.lang.Long)}.
     * @throws UniqueValidationException 
     */
    @Test
    public void testIsDeletableProject() throws UniqueValidationException {
        //WIP
    	boolean project = projectService.isDeletableProject(1l);
    	assertFalse(project);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#listOrderedJobs()}.
     */
    @Test
    public void testListOrderedJobs() {
    	List<Job> jobs = projectService.listOrderedJobs();
    	assertNotNull(jobs);
    	assertFalse(jobs.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#listProjectByResource(java.lang.Long)}.
     */
    @Test
    public void testListProjectByResource() {
    	List<Project> projects = projectService.listProjectByResource(1l);
    	assertNotNull(projects);
    	assertFalse(projects.isEmpty());
    	
    	List<Project> projects2 = projectService.listProjectByResource(13254l);
    	assertTrue(projects2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#listProjectByResponsibleResource(java.lang.Long)}.
     */
    @Test
    public void testListProjectByResponsibleResource() {
    	List<Project> projects = projectService.listProjectByResponsibleResource(1l);
    	assertNotNull(projects);
    	assertFalse(projects.isEmpty());
    	
    	List<Project> projects2 = projectService.listProjectByResponsibleResource(13131l);
    	assertTrue(projects2.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#positionStatusList()}.
     */
    @Test
    public void testPositionStatusList() {
    	List<PositionStatus> status = projectService.positionStatusList();
    	assertNotNull(status);
    	assertFalse(status.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#posResourceStatusList()}.
     */
    @Test
    public void testPosResourceStatusList() {
    	List<PosResourceStatus> status = projectService.posResourceStatusList();
    	assertNotNull(status);
    	assertFalse(status.isEmpty());
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchCustomers(de.tarent.activity.domain.filter.BaseFilter)}.
     */
    @Test
    public void testSearchCustomers() {
    	FilterResult<Customer> res = projectService.searchCustomers(new BaseFilter(1l, 1l, null, null));
    	assertNotNull(res);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchJob(de.tarent.activity.domain.filter.JobFilter)}.
     */
    @Test
    public void testSearchJob() {
    	FilterResult<Job> job = projectService.searchJob(new JobFilter(null, null, null, null, null, null, null, null, null));
    	assertNotNull(job);
    	
    	boolean thrown = false;
        try {
        	projectService.searchJob(new JobFilter("teststtststs", 1321313l,1213l,21313l,4242l, 4242l,4242l, "sortColumnteststst", "sdasda"));        	
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);
    	
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchMyPosition(de.tarent.activity.domain.filter.PositionFilter)}.
     */
    @Test
    public void testSearchMyPosition() {
    	FilterResult<Position> position = projectService.searchMyPosition(new PositionFilter(null, null, null, null, null, null));
    	assertNotNull(position);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchPosition(de.tarent.activity.domain.filter.PositionFilter)}.
     */
    @Test
    public void testSearchPosition() {
    	FilterResult<Position> pos = projectService.searchPosition(new PositionFilter(null, null, null, null, null, null));
    	assertNotNull(pos);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchPosResourceMapping(de.tarent.activity.domain.filter.PosResourceMappingFilter)}.
     */
    @Test
    public void testSearchPosResourceMapping() {
    	FilterResult<PosResourceMapping> pos_res = projectService.searchPosResourceMapping(new PosResourceMappingFilter(null, null, null, null, null, null, null));
    	assertNotNull(pos_res);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#searchProject(de.tarent.activity.domain.filter.ProjectFilter)}.
     */
    @Test
    public void testSearchProject() {
         FilterResult<Project> projects = projectService.searchProject(new ProjectFilter(null, null, null, null, null, null, null, null, null));
         assertNotNull(projects);
    }
    
    @Test
    public void testgetStats() {
    	JobStats stats = projectService.getJobStats(1l);
    	assertNotNull(stats);
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#updateCustomer(de.tarent.activity.domain.Customer)}.
     */
    @Test
    public void testUpdateCustomer() {
    	// TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#updateJob(de.tarent.activity.domain.Job)}.
     */
    @Test
    public void testUpdateJob() {
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#updatePosition(de.tarent.activity.domain.Position)}.
     */
    @Test
    public void testUpdatePosition() {
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#updatePosResourceMapping(de.tarent.activity.domain.PosResourceMapping)}.
     */
    @Test
    public void testUpdatePosResourceMapping() {
        // TODO
    }

    /**
     * Test method for {@link de.tarent.activity.services.impl.ProjectServiceImpl#updateProject(de.tarent.activity.domain.Project)}.
     */
    @Test
    public void testUpdateProject() {
        // TODO
    }

}
