/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.ActivityDAOImpl;
import de.tarent.activity.dao.impl.LossDAOImpl;
import de.tarent.activity.dao.impl.LossStatusDAOImpl;
import de.tarent.activity.dao.impl.LossTypeDAOImpl;
import de.tarent.activity.dao.impl.OvertimeDAOImpl;
import de.tarent.activity.dao.impl.TimerDAOImpl;
import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.LossStatus;
import de.tarent.activity.domain.LossType;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.OvertimeStatus;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Timer;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.domain.filter.OvertimeFilter;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.util.TestUserUtilImpl;

public class ActivityServiceImplTest extends BaseServiceImplTest {

	private ActivityService activityService;

	@Before
    public void setUp() {
        JavaEEGloss gloss = new JavaEEGloss();

        LossStatusDAOImpl lossStatusDao = new LossStatusDAOImpl();
        lossStatusDao.setEntityManager(entityManager);
        lossStatusDao.setUserUtil(new TestUserUtilImpl());

        LossTypeDAOImpl lossTypeDao = new LossTypeDAOImpl();
        lossTypeDao.setEntityManager(entityManager);
        lossTypeDao.setUserUtil(new TestUserUtilImpl());

        LossDAOImpl lossDao = new LossDAOImpl();
        lossDao.setEntityManager(entityManager);
        lossDao.setUserUtil(new TestUserUtilImpl());

        TimerDAOImpl timerDao = new TimerDAOImpl();
        timerDao.setEntityManager(entityManager);
        timerDao.setUserUtil(new TestUserUtilImpl());

        ActivityDAOImpl activityDao = new ActivityDAOImpl();
        activityDao.setEntityManager(entityManager);
        activityDao.setUserUtil(new TestUserUtilImpl());

        OvertimeDAOImpl overtimeDao = new OvertimeDAOImpl();
        overtimeDao.setEntityManager(entityManager);
        overtimeDao.setUserUtil(new TestUserUtilImpl());

        gloss.addEJB(lossDao);
        gloss.addEJB(lossStatusDao);
        gloss.addEJB(lossTypeDao);
        gloss.addEJB(timerDao);
        gloss.addEJB(activityDao);
        gloss.addEJB(overtimeDao);

        activityService = gloss.make(ActivityServiceImpl.class);
    }

	@Test
    public void testSearchActivities() {
		FilterResult<Activity> filterResult = activityService.searchActivities(new ActivityFilter(1l, 1l, 1l, null, null, 1l, null, null, null, null));
		assertNotNull(filterResult);
	}

	@Test
    public void testGetActivitiesHoursSum() {
        BigDecimal sum = activityService.getActivitiesHoursSum(new ActivityFilter(1l, 1l, 1l, null, null, 1l, null, null, null, null));
        assertNotNull(sum);
    }

	@Test
    public void testGetActivity() {
		Activity activity = activityService.getActivity(1l);
		assertNotNull(activity);
    }

	@Test
    public void testAddActivity() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
        Activity activity = new Activity();
        activity.setDate(startDate);
        activity.setHours(new BigDecimal(2));
        activity.setName("TEST NAME");
        activity.setPosition(new Position(1l));
        activity.setResource(new Resource(1l));
        long pk = activityService.addActivity(activity);

        Activity activity2 = activityService.getActivity(pk);
        assertNotNull(activity2);
    }

	@Test
    public void testUpdateActivity() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Activity activity = new Activity();
        activity.setDate(startDate);
        activity.setHours(new BigDecimal(2));
        activity.setName("TEST NAME");
        activity.setPosition(new Position(1l));
        activity.setResource(new Resource(1l));
        long pk = activityService.addActivity(activity);

        Activity activity2 = activityService.getActivity(pk);
        activity2.setName("TEST NEUER NAME");
        activityService.updateActivity(activity2);

        assertTrue(activity.getName()== activity2.getName());
    }

	@Test
    public void testDeleteActivity() throws UniqueValidationException {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Activity activity = new Activity();
        activity.setDate(startDate);
        activity.setHours(new BigDecimal(2));
        activity.setName("TEST NAME");
        activity.setPosition(new Position(1l));
        activity.setResource(new Resource(1l));
        long pk = activityService.addActivity(activity);
        activityService.deleteActivity(pk);

        Activity activity2 = activityService.getActivity(pk);
        assertNull(activity2);

        boolean thrown = false;
        try {
        	activityService.deleteActivity(121321312312l);
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);

    }

	@Test
    public void testSearchOvertimes() {
		FilterResult<Overtime> filterResult = activityService.searchOvertimes(new OvertimeFilter(1l, 1l, 1l, null, null, null, null, null, null));
		assertNotNull(filterResult);
	}

	@Test
    public void testGetOvertimeHoursSumByFilter() {
        BigDecimal overtimeHours = activityService.getOvertimeHoursSumByFilter(new OvertimeFilter(1l, 1l, 1l, null, null, null, null, null, null));
        assertNotNull(overtimeHours);
    }

	@Test
    public void testGetOvertime() {
        Overtime overtime = activityService.getOvertime(1l);
        assertNotNull(overtime);

        Overtime overtime2 = activityService.getOvertime(13232323l);
        assertNull(overtime2);
    }

	@Test
    public void testAddOvertime() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Overtime overtime = new Overtime();
		overtime.setDate(startDate);
        overtime.setHours(new BigDecimal(3));
        overtime.setNote("TEST NOTE");
        overtime.setYear(2000l);
        overtime.setOvertimeStatus(new OvertimeStatus(1l));
        overtime.setProject(new Project(1l));
        overtime.setResource(new Resource(1l));
        long pk = activityService.addOvertime(overtime);

        Overtime overtime2 = activityService.getOvertime(pk);
        assertNotNull(overtime2);
    }

	@Test
    public void testUpdateOvertime() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Overtime overtime = new Overtime();
		overtime.setDate(startDate);
        overtime.setHours(new BigDecimal(3));
        overtime.setNote("TEST NOTE");
        overtime.setYear(2000l);
        overtime.setOvertimeStatus(new OvertimeStatus(1l));
        overtime.setProject(new Project(1l));
        overtime.setResource(new Resource(1l));
        long pk = activityService.addOvertime(overtime);

        Overtime overtime2 = activityService.getOvertime(pk);
        overtime.setNote("NEUE TEST NOTE");
        activityService.updateOvertime(overtime2);
        assertTrue(overtime.getNote() == overtime2.getNote());
    }

	@Test
    public void testDeleteOvertime() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Overtime overtime = new Overtime();
		overtime.setDate(startDate);
        overtime.setHours(new BigDecimal(3));
        overtime.setNote("TEST NOTE");
        overtime.setYear(2000l);
        overtime.setOvertimeStatus(new OvertimeStatus(1l));
        overtime.setProject(new Project(1l));
        overtime.setResource(new Resource(1l));
        long pk = activityService.addOvertime(overtime);
        activityService.deleteOvertime(pk);

        Overtime overtime2 = activityService.getOvertime(pk);
        assertNull(overtime2);
    }

	@Test
    public void testGetTimerByResource() {
        Timer timer = activityService.getTimerByResource(2l);
        assertNotNull(timer);
    }

	@Test
    public void testAddTimer() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Timer timer = new Timer();
		timer.setTime(startDate);
		timer.setTimeDiff(startDate);
		timer.setPosition(new Position(1l));
		timer.setResource(new Resource(1l));
		long pk = activityService.addTimer(timer);

		Timer timer2 = new Timer(pk, new Resource(1l));
		assertNotNull(timer2);

    }

	@Test
    public void testDeleteTimer() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Timer timer = new Timer();
		timer.setTime(startDate);
		timer.setTimeDiff(startDate);
		timer.setPosition(new Position(1l));
		timer.setResource(new Resource(1l));
		long pk = activityService.addTimer(timer);
		activityService.deleteTimer(pk);
		Timer timer2 = activityService.getTimerById(pk);
		assertNull(timer2);
	}

	@Test
    public void testUpdateTimer() {
    }

	@Test
    public void testAddLoss() {
        Loss loss = new Loss();
        activityService.addLoss(loss);
        assertNotNull(loss);
    }

	@Test
    public void testUpdateLoss() {
        //TODO
    }

	@Test
    public void testDeleteLoss() {
        Loss loss = new Loss();
        long pk = activityService.addLoss(loss);
        activityService.deleteLoss(pk);
        Loss loss2 = activityService.getLoss(pk);
        assertNull(loss2);

        boolean thrown = false;
        try {
        	activityService.deleteLoss(10402340230l);
        } catch (Exception e) {
        	thrown = true;
        }
        assertTrue(thrown);


    }

	@Test
    public void testSearchLoss() {
		FilterResult<Loss> filterRes = activityService.searchLoss(new LossFilter(null, null, null, null, null, null, null, null, null, null));
		assertNotNull(filterRes);
    }

	@Test
    public void testGetLoss() {
        Loss loss = activityService.getLoss(1l);
        assertNotNull(loss);
    }

	@Test
    public void testLossTypeList() {
		List<LossType> list = activityService.lossTypeList();
		assertFalse(list.isEmpty());
    }

	@Test
    public void testColleaguesLoss() {
		//TODO
	}

	@Test
    public void testProjectResourceLossNow() {
		//TODO
	}

	@Test
    public void testProjectResourceLossFuture() {
//        TODO
    }

	@Test
    public void testGetLossType() {
        LossType lossType = activityService.getLossType(1l);
        assertNotNull(lossType);
    }

	@Test
    public void testGetLossStatus() {
        LossStatus lossStatus = activityService.getLossStatus(1l);
        assertNotNull(lossStatus);
    }

	@Test
    public void testLossStatusList() {
		List<LossStatus> list = activityService.lossStatusList();
		assertNotNull(list);
		assertFalse(list.isEmpty());
    }

	@Test
    public void testGetCurrentLossByProject() {
		//TODO
	}

	@Test
    public void testGetFutureLossByProject() {
        //TODO
    }

	@Test
    public void testGetCurrentLossByLocation() {
        //TODO
    }

	@Test
    public void testGetAllOvertimeSum() {
        BigDecimal overtime = activityService.getAllOvertimeSum(1l);
        assertNotNull(overtime);
    }

	@Test
    public void testGetLossDaysFromYear() {
        BigDecimal lossDays = activityService.getLossDaysFromYear(1l, 2012);
        assertNotNull(lossDays);
    }
	@Test
    public void testListActivitiesByPosition() {
		List<Activity> list = activityService.listActivitiesByPosition(1l);
		assertNotNull(list);
		assertFalse(list.isEmpty());
    }
	@Test
    public void testGetHoursFromInterval() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());

		cal.set(2013, 1,2);
		Date endDate = new Date();
		endDate.setTime(cal.getTimeInMillis());
        BigDecimal hoursFromInterval = activityService.getHoursFromInterval(1l, startDate, endDate);
        assertNotNull(hoursFromInterval);
    }
	@Test
    public void testDeleteActivityAndRequest() {
		//TODO
    }
	@Test
    public void testDeleteOvertimeAndRequest() {
        //TODO
    }
	@Test
    public void testDeleteLossAndRequest() {
        //TODO
    }
}
