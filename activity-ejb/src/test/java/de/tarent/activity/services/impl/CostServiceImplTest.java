/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.CostDAOImpl;
import de.tarent.activity.dao.impl.CostTypeDAOImpl;
import de.tarent.activity.dao.impl.InvoiceDAOImpl;
import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.CostType;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.service.CostService;
import de.tarent.activity.util.TestUserUtilImpl;

public class CostServiceImplTest extends BaseServiceImplTest{
	
	private CostService costService;
	
	@Before
	public void setUp() {
		JavaEEGloss gloss = new JavaEEGloss();

		CostDAOImpl costDao = new CostDAOImpl();
	    costDao.setEntityManager(entityManager);
	    costDao.setUserUtil(new TestUserUtilImpl());
	        
	    InvoiceDAOImpl invoiceDao = new InvoiceDAOImpl();
	    invoiceDao.setEntityManager(entityManager);
	    invoiceDao.setUserUtil(new TestUserUtilImpl());
	        
	    CostTypeDAOImpl costTypeDao = new CostTypeDAOImpl();
	    costTypeDao.setEntityManager(entityManager);
	    costTypeDao.setUserUtil(new TestUserUtilImpl());
	        
	    gloss.addEJB(costDao);
	    gloss.addEJB(invoiceDao);
	    gloss.addEJB(costTypeDao);
	        
	    costService = gloss.make(CostServiceImpl.class);
	}
	 
	@Test
	public void testAddCost() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Cost cost = new Cost();
	    BigDecimal db = new BigDecimal(5);
	    cost.setCost(db);
	    cost.setDate(startDate);
	    cost.setName("Test Costs");
	    cost.setNote("Test");
	    cost.setCostType(new CostType(1l));
	    cost.setJob(new Job(1l));
	    cost.setResource(new Resource(1l));
	    long pk = costService.addCost(cost);
	    Cost newCost = costService.getCost(pk);
	    assertNotNull(newCost);
	}
	
	@Test
	public void testAddInvoice() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Invoice invoice = new Invoice();
	    BigDecimal db = new BigDecimal(5);
	    invoice.setAmount(db);
	    invoice.setInvoiced(startDate);
	    invoice.setName("Test");
	    invoice.setNote("TestNotiz");
	    invoice.setNumber("TestNumber");
	    long pk = costService.addInvoice(invoice);
	    Invoice invoice2 = costService.getInvoice(pk);
	    assertNotNull(invoice2);
	}
	
	@Test
	public void testGetCostTypeList() {
		List<CostType> costType = costService.getCostTypeList();
		assertNotNull(costType);
		assertFalse(costType.isEmpty());
	}
	
	@Test
	public void testGetCost() {
		Cost cost = costService.getCost(1l);
		assertNotNull(cost);
		 
		Cost cost2 = costService.getCost(194284348l);
		assertNull(cost2);
	}
	
	@Test
	public void testGetInvoice() {
		Invoice invoice = costService.getInvoice(1l);
		assertNotNull(invoice);
		 
		Invoice invoice2 = costService.getInvoice(13332321l);
		assertNull(invoice2);
	}
	
	@Test
	public void testGetInvoicesByJob() {
		List<Invoice> invoice = costService.getInvoicesByJob(1l);
		assertFalse(invoice.isEmpty());
		 
		List<Invoice> invoice2 = costService.getInvoicesByJob(1343213l);
		assertTrue(invoice2.isEmpty());
	}
	
	@Test
	public void testSearchCost() {
		FilterResult<Cost> res = costService.searchCost(new CostFilter(null, null, null, null, null, null));
		assertNotNull(res);
		 
		boolean thrown = false;
	    	try {
	    		costService.searchCost(new CostFilter(1212213l, 12121221l, 12121221l, 12121221l, "dsadasd", "dasdas"));        	
	        } catch (Exception e) {
	        	thrown = true;
	        }
	        assertTrue(thrown);
	    }
	
	@Test
	public void testSearchInvoice() {
		FilterResult<Invoice> invoice = costService.searchInvoice(new InvoiceFilter(null, null, null, null, null, null, null, null));
		assertNotNull(invoice);
		 
		boolean thrown = false;
	       try {
	       		costService.searchInvoice(new InvoiceFilter(129228123l, 1221213l, 121234l, 1221323, 1232132l, 1232323l, "asdass", "dassdas"));        	
	       } catch (Exception e) {
	        	thrown = true;
	       }
	       assertTrue(thrown);
	}
	
	@Test
	public void testUpdateCost() {
		//TODO
	}
	
	@Test
	public void testUpdateInvoice() {
		//TODO
	}
	
	@Test
	public void testGetProjectInvoices() {
		List<Invoice> invoice = costService.getProjectInvoices(1l);
		assertNotNull(invoice);
		assertFalse(invoice.isEmpty());
		 
		List<Invoice> invoice2 = costService.getProjectInvoices(1434343l);
		assertTrue(invoice2.isEmpty());
	}
	
	@Test
	public void testGetProjectInvoice() {
		Invoice invoice = costService.getProjectInvoice(1l);
		assertNotNull(invoice);
		 
		Invoice invoice2 = costService.getProjectInvoice(11213113l);
		assertNull(invoice2);
	}
	
	@Test
	public void testSearchProjectInvoice() {
		FilterResult<Invoice> invoice = costService.searchProjectInvoice(new InvoiceFilter(null, null, null, null, null, null, null, null));
		assertNotNull(invoice);
		 
		boolean thrown = false;
	    try {
	    	costService.searchProjectInvoice(new InvoiceFilter(122112l, 122112l, 122112l, 122112, 122112l, 122112l, "dasds", "dsadas"));        	
	    } catch (Exception e) {
	        thrown = true;
	    }
	    	assertTrue(thrown);
	    }
	
	@Test
	public void testDeleteInvoice() {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 0, 1);
		Date startDate = new Date();
		startDate.setTime(cal.getTimeInMillis());
		Invoice invoice = new Invoice();
	    BigDecimal db = new BigDecimal(5);
	    invoice.setAmount(db);
	    invoice.setInvoiced(startDate);
	    invoice.setName("Test");
	    invoice.setNote("TestNotiz");
	    invoice.setNumber("TestNumber");
	    long pk = costService.addInvoice(invoice);
	    costService.deleteInvoice(pk);
	    Invoice invoice2 = costService.getInvoice(pk);
	    assertNull(invoice2);
	        
	    boolean thrown = false;
	    try {
	    	costService.deleteInvoice(122121321l);        	
	    } catch (Exception e) {
	        thrown = true;
	    }
	    assertTrue(thrown);
	    }
	}
