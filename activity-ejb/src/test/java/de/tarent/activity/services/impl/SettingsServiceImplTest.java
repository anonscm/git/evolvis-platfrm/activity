/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.SettingsDAOImpl;
import de.tarent.activity.dao.impl.TaractInfoDAOImpl;
import de.tarent.activity.domain.Settings;
import de.tarent.activity.service.SettingsService;
import de.tarent.activity.util.TestUserUtilImpl;

public class SettingsServiceImplTest extends BaseServiceImplTest {
	
	private SettingsService settingsService;
	
	@Before
	public void setUp() {
		JavaEEGloss gloss = new JavaEEGloss();

	    SettingsDAOImpl settingsDao = new SettingsDAOImpl();
	    settingsDao.setEntityManager(entityManager);
	    settingsDao.setUserUtil(new TestUserUtilImpl());
	        
	    TaractInfoDAOImpl taractInfoDao = new TaractInfoDAOImpl();
	    taractInfoDao.setEntityManager(entityManager);
	    taractInfoDao.setUserUtil(new TestUserUtilImpl());
	        
	    gloss.addEJB(settingsDao);
	    gloss.addEJB(taractInfoDao);
	        
	    settingsService = gloss.make(SettingsServiceImpl.class);
	}
	 
	 @Test
	 public void testSaveSetting() {
		 //TODO
	 }
	 
	 @Test
	 public void testGetSettings() {
		 List<Settings> settings = settingsService.getSettings();
		 assertNotNull(settings);
		 assertFalse(settings.isEmpty());
	 }
	 
	 @Test
	 public void testGetDBVersion() {
		 String version = settingsService.getDBVersion();
	     assertNotNull(version);
	 }
	 
	 @Test
	 public void testGetSettingsByKey() {
		Settings setting = settingsService.getSettingsByKey("test_key");
		assertNotNull(setting);
		 	
		Settings setting2 = settingsService.getSettingsByKey("testtesttest");
		assertNull(setting2);
	}
}
