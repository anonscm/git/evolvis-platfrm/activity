/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.PermissionDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.service.MailService;
import de.tarent.activity.util.TestUserUtilImpl;

public class MailServiceImplTest extends BaseServiceImplTest{
	
	private MailService mailService;
	
	/**
     * Initialize EasyGloss container and begin transaction.
     */
    @Before
    public void setUp() {
        JavaEEGloss gloss = new JavaEEGloss();
        
        PermissionDAOImpl permissionDao = new PermissionDAOImpl();
        permissionDao.setEntityManager(entityManager);
        permissionDao.setUserUtil(new TestUserUtilImpl());

        ResourceDAOImpl resourceDao = new ResourceDAOImpl();
        resourceDao.setEntityManager(entityManager);
        resourceDao.setUserUtil(new TestUserUtilImpl());

        gloss.addEJB(permissionDao);
        gloss.addEJB(resourceDao);
        
        mailService = gloss.make(MailServiceImpl.class);
    }
    
    @Test
    public void testMergeTemplate() {
    	//TODO
    }
    
    @Test
    public void testPrepareMailMessage() {
    	//TODO
    }
    
    @Test
    public void testGetControllersAddress() {
        //TODO
    }
    
    @Test
    public void testSendCustomerMail() {
        //TODO
    }
    
    @Test
    public void testSendHolidayApprovedMail() {
        //TODO
    }
    
    @Test
    public void testSendHolidayMail() {
        //TODO
    }
    
    @Test
    public void testSendHolidayRejectedMail() {
        //TODO
    }
    
    @Test
    public void testSendJobMail() {
        //TODO
    }
    
    @Test
    public void testSendMail() {
    	//TODO
    }
    
    @Test
    public void testSendProjectMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddHolidayMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableApprovedByManagerLossMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableApprovedByControllingLossMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableDeniedByManagerLossMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableDeniedByControllingLossMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddAbsenceMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddJobInvoiceMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableEditJobInvoiceMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddProjectInvoiceMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableEditProjectInvoiceMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddCustomerMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddJobMail() {
    	//TODO
    }
    
    @Test
    public void testSendConfigurableAddProjectMail() {
    	//TODO
    }

    @Test
    public void testReadEmailAddressesFromXmlFile() {
    	//TODO
    }
    
    @Test
    public void testGetlist() {
    	//TODO
    }
    
    @Test
    public void testGetConfigurableInformationByKey() {
    	//TODO
    }
}