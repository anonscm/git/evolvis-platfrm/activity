/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.PermissionDAOImpl;
import de.tarent.activity.dao.impl.ResourceDAOImpl;
import de.tarent.activity.dao.impl.RoleDAOImpl;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.PermissionService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.util.TestHelperUtil;

/**
 * Tests for PermissionServiceImpl. The tests assumes sometimes that at least some roles and permissions are stored in
 * database, which is the case when you run dafaultdata.sql on database.
 */
public class PermissionServiceImplTest extends BaseServiceImplTest {

	@Autowired
	private EntityManager entityManager;

	// Class to test
	private PermissionService permissionService;

	private ResourceService resourceService;
	
	private TestHelperUtil helper;

	/**
	 * Initialize EasyGloss container and begin transaction.
	 */
	@Before
	public void setUp() {
		JavaEEGloss gloss = new JavaEEGloss();

		RoleDAOImpl roleDao = new RoleDAOImpl();
		roleDao.setEntityManager(entityManager);

		ResourceDAOImpl resourceDao = new ResourceDAOImpl();
		resourceDao.setEntityManager(entityManager);

		PermissionDAOImpl permissionDao = new PermissionDAOImpl();
		permissionDao.setEntityManager(entityManager);

		gloss.addEJB(roleDao);
		gloss.addEJB(resourceDao);
		gloss.addEJB(permissionDao);

		permissionService = gloss.make(PermissionServiceImpl.class);
		resourceService = gloss.make(ResourceServiceImpl.class);
		
		helper = new TestHelperUtil(entityManager);
	}

	@Test
	public void testAddPermissionsToResource() {
		Resource res = resourceService.getResource(1L, false, false);
		int size = res.getPermissions().size();

		// get out which permission are not yet assigned to resource, for reference
		List<Permission> allPermission = permissionService.searchPermissions(new PermissionFilter()).getRecords();
		@SuppressWarnings("unchecked")
		List<Permission> notAssignedPermission = (List<Permission>) CollectionUtils.subtract(allPermission,
				Arrays.asList(res.getPermissions().toArray()));

		// add four new permission to user
		Long[] pks = new Long[4];
		for(int i = 0; i < 4; i++){
			pks[i] = notAssignedPermission.get(i).getPk();
		}
		
		res = permissionService.addPermissionsToResource(res.getPk(), pks);
		assertNotNull(res);
		assertEquals(size + 4, res.getPermissions().size());
	}

	@Test
	public void testAddPermissionsToRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getPermissions().size();
		
		// get out which permission are not yet assigned to role, for reference
		List<Permission> allPermission = permissionService.searchPermissions(new PermissionFilter()).getRecords();
		@SuppressWarnings("unchecked")
		List<Permission> notAssignedPermission = (List<Permission>) CollectionUtils.subtract(allPermission,
				Arrays.asList(role.getPermissions().toArray()));

		// add four new permission to role
		Long[] pks = new Long[4];
		for(int i = 0; i < 4; i++){
			pks[i] = notAssignedPermission.get(i).getPk();
		}
		
		role = permissionService.addPermissionsToRole(role.getPk(), pks);
		assertNotNull(role);
		assertEquals(size + 4, role.getPermissions().size());
	}

	@Test
	public void testAddResourcesToRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getResources().size();

		// add four new resources to role
		role = permissionService.addResourcesToRole(role.getPk(), new Long[] { 3L, 4L, 5L, 6L });
		assertNotNull(role);
		assertEquals(size + 4, role.getResources().size());
	}

	@Test
	public void testAddResourceToRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getResources().size();

		// add four new resources to role
		role = permissionService.addResourceToRole(role.getPk(), 3L);
		assertNotNull(role);
		assertEquals(size + 1, role.getResources().size());
	}

	@Test(expected = PermissionException.class)
	public void testCheckAndThrowPermission() {
		long resId = 2;

		// should not throw an exception as test user has this permissions by role assignment
		permissionService.checkAndThrowPermission(resId, PermissionIds.ACTIVITY_ADD);
		permissionService.checkAndThrowPermission(resId, PermissionIds.ACTIVITY_VIEW);

		// should not throw an exception as test user has this permissions by role assignment
		permissionService.checkAndThrowPermission(resId, PermissionIds.ACTIVITY_VIEW_ALL);

		// should throw an exception as test user does not have this permission, either by role nor direct
		permissionService.checkAndThrowPermission(resId, PermissionIds.ADMIN_EDIT_SETTINGS);
	}

	@Test
	public void testCheckPermission() {
		long resId = 2;

		// should return true as test user has this permissions by role assignment
		assertTrue(permissionService.checkPermission(resId, PermissionIds.ACTIVITY_ADD));
		assertTrue(permissionService.checkPermission(resId, PermissionIds.ACTIVITY_VIEW));

		// should return true as test user has this permissions by role assignment
		assertTrue(permissionService.checkPermission(resId, PermissionIds.ACTIVITY_VIEW_ALL));

		// should return false as test user does not have this permission, either by role nor direct
		assertFalse(permissionService.checkPermission(resId, PermissionIds.ADMIN_EDIT_SETTINGS));
	}

	@Test
	public void testGetAllRoles() {
		List<Role> roles = permissionService.getAllRoles();
		// we have at least nine roles in database
		assertTrue(roles.size() >= 9);
	}

	@Test
	public void testGetResourcesByPermission() {
		// as no test user has an direct assignment to 'Activity.ADD' we assume an empty result list
		List<Resource> resources = permissionService.getResourcesByPermission(PermissionIds.ACTIVITY_ADD);
		assertNotNull(resources);
		assertTrue(resources.isEmpty());

		// in testdata.sql two user have custom permission 'Customer.EDIT_ALL'
		resources = permissionService.getResourcesByPermission(PermissionIds.CUSTOMER_EDIT_ALL);
		assertNotNull(resources);
		assertEquals(2, resources.size());

		resources = permissionService.getResourcesByPermission(PermissionIds.ACTIVITY_VIEW_ALL);
		assertNotNull(resources);
		assertEquals(1, resources.size());

		// try with an permission that does not exist
		resources = permissionService.getResourcesByPermission("NOT_EXIST");
		assertNull(resources);
	}

	@Test
	public void testGetRole() {
		Role role = permissionService.getRole(1L);
		assertNotNull(role);
		assertEquals(1, role.getPk().intValue());
	}

	/**
	 * Assumes that resource with primary key 1 has at least three custom permissions assigned via testdata.sql.
	 */
	@Test
	public void testRemovePermissionsFromResource() {
		Resource res = resourceService.getResource(1L, false, false);
		int size = res.getPermissions().size();
		assertTrue(size >= 3);

		Permission[] perms = res.getPermissions().toArray(new Permission[] {});

		res = permissionService.removePermissionsFromResource(res.getPk(),
				new Long[] { perms[0].getPk(), perms[1].getPk() });
		assertNotNull(res);
		assertEquals(size - 2, res.getPermissions().size());
	}

	/**
	 * Assumes that role with primary key 1 has at least three permissions assigned via defaultdata.sql.
	 */
	@Test
	public void testRemovePermissionsFromRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getPermissions().size();
		assertTrue(size >= 3);

		Permission[] perms = role.getPermissions().toArray(new Permission[] {});

		role = permissionService.removePermissionsFromRole(role.getPk(),
				new Long[] { perms[0].getPk(), perms[1].getPk(), perms[2].getPk() });
		assertNotNull(role);
		assertEquals(size - 3, role.getPermissions().size());
	}

	/**
	 * Assumes that role with primary key 1 has at least two resources assigned via testdata.sql.
	 */
	@Test
	public void testRemoveResourceFromRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getResources().size();

		role = permissionService.removeResourceFromRole(1l, 1L);
		assertNotNull(role);
		assertEquals(size - 1, role.getResources().size());
	}

	/**
	 * Assumes that at least resource with primary key 1L and 2L are assigned to role with primary key 1L in
	 * testdata.sql.
	 */
	@Test
	public void testRemoveResourcesFromRole() {
		Role role = permissionService.getRole(1L);
		int size = role.getResources().size();

		role = permissionService.removeResourcesFromRole(role.getPk(), new Long[] { 1L, 2L, });
		assertNotNull(role);
		assertEquals(size - 2, role.getResources().size());
	}

	/**
	 * Assumes that at least 100 permissions are in database.
	 */
	@Test
	public void testSearchPermissions() {
		// an empty filter should return all permissions
		PermissionFilter filter = new PermissionFilter();
		FilterResult<Permission> result = permissionService.searchPermissions(filter);
		assertNotNull(result);
		assertEquals(helper.countAllPermissions(null), result.getRecords().size());

		// filter by role with primary key 1
		List<Long> roleIds = Arrays.asList(new Long[] { 1L });
		filter = new PermissionFilter();
		filter.setRoleIds(roleIds);
		result = permissionService.searchPermissions(filter);
		assertNotNull(result);
		assertEquals(helper.countPermissionsByRoles(roleIds), result.getRecords().size());

		// filter by resource with primary key 1
		List<Long> resIds = Arrays.asList(new Long[] { 1L });
		filter = new PermissionFilter();
		filter.setResourceIds(resIds);
		result = permissionService.searchPermissions(filter);
		assertNotNull(result);
		assertEquals(helper.countPermissionsByResources(resIds), result.getRecords().size());

		// filter by permission primary keys, should find all given permission
		List<Long> permIds = Arrays.asList(new Long[] { 1L, 2L, 3L, 11L, 22L });
		filter = new PermissionFilter();
		filter.setPermissionIds(permIds);
		result = permissionService.searchPermissions(filter);
		assertNotNull(result);
		assertEquals(helper.countAllPermissions(permIds), result.getRecords().size());
	}

}
