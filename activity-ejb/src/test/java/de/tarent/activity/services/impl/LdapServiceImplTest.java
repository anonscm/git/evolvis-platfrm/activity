/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.javaee.JavaEEGloss;

import de.tarent.activity.dao.impl.BranchOfficeDAOImpl;
import de.tarent.activity.dao.impl.LdapDAOImpl;
import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.service.LdapService;
import de.tarent.activity.util.TestUserUtilImpl;

public class LdapServiceImplTest extends BaseServiceImplTest{

	private LdapService ldapService;
	
	 @Before
	    public void setUp() {
	        JavaEEGloss gloss = new JavaEEGloss();

	        BranchOfficeDAOImpl branchOfficeDao = new BranchOfficeDAOImpl();
	        branchOfficeDao.setEntityManager(entityManager);
	        branchOfficeDao.setUserUtil(new TestUserUtilImpl());
	        
	        LdapDAOImpl ldapDao = new LdapDAOImpl();
	        
	        gloss.addEJB(branchOfficeDao);
	        gloss.addEJB(ldapDao);
	        
	        ldapService = gloss.make(LdapServiceImpl.class);
	    }
	 @Test
	    public void testGetBranchOfficeList(){
	        List<BranchOffice> office = ldapService.getBranchOfficeList();
	        assertNotNull(office);
	        assertFalse(office.isEmpty());
	    }
	 @Test
	    public void testConvertListToCommaSeparatedString(){
	        //TODO
	    }
	 @Test
	    public void testGetAllUserAndGroups(){
	        //TODO
	    }
	 @Test
	    public void testLdapLogin(){
	        //TODO
	    }
	 
}
