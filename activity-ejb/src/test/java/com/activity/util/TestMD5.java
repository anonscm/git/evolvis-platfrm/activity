/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.activity.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import junit.framework.TestCase;


public class TestMD5 extends TestCase {

	@Test
	public void testMD5() throws NoSuchAlgorithmException {
		System.out.println(getMD5Hash("admin"));
		System.out.println(getMD5Hash("miron"));
		System.out.println(getMD5Hash("iurie"));
		System.out.println(getMD5Hash("florentina"));
		System.out.println(getMD5Hash("marius"));
	}
	
	/**
	 * Gibt das übergebene Passwort als MD5-Hashwert zurück.
	 * 
	 * @param password Passwort
	 * @return MD5 Wert
	 * @throws NoSuchAlgorithmException Wenn MD5 nicht zur Verfügung steht
	 */
	public static String getMD5Hash(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte digest[] = md.digest(password.getBytes());
		StringBuffer buffer = new StringBuffer(32);
		for (int i = 0; i < digest.length; i++) {
			int b = digest[i] & 255;
			if (b < 16) buffer.append('0');
			buffer.append(Integer.toHexString(b));
		}
		return buffer.toString();
	}
}
