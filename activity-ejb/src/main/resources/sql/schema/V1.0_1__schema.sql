alter table activity.tinvoice alter column amount type numeric(15,2);
alter table activity.tinvoice_aud alter column amount type numeric(15,2);
