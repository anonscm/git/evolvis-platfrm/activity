--many columns used floating point types, i.e. double precision or float8, but have to be numeric (10,2)

BEGIN;
alter table activity.tactivity alter column costperhour type numeric(10,2) using costperhour::numeric(10,2);
alter table activity.tactivity alter column hours type numeric(10,2) using hours::numeric(10,2);
alter table activity.tactivity_AUD alter column costperhour type numeric(10,2) using costperhour::numeric(10,2);
alter table activity.tactivity_AUD alter column hours type numeric(10,2) using hours::numeric(10,2);
alter table activity.tloss alter column days type numeric(10,2) using days::numeric(10,2);
alter table activity.tloss_AUD alter column days type numeric(10,2) using days::numeric(10,2);
alter table activity.tovertime alter column hours type numeric(10,2) using hours::numeric(10,2);
alter table activity.tovertime_AUD alter column hours type numeric(10,2) using hours::numeric(10,2);
COMMIT;
