-- Bug [#3503]:alter all timestamps to dates where appropriate
-- alter statements generated with:

/*
select 'alter table activity.' || table_name || ' alter column ' || column_name || ' type date;'
from  information_schema.columns where data_type like 'timestamp%' and table_schema = 'activity' and column_name
not in ('cr_date', 'upd_date') and table_name not in ('schema_version') order by table_name;
*/
BEGIN;
alter table activity.tactivity alter COLUMN "date" type date;
alter table activity.tactivity_aud alter COLUMN "date" type date;
alter table activity.tcost alter COLUMN "date" type date;
alter table activity.tcost_aud alter COLUMN "date" type date;
alter table activity.tcustomer alter column payment_target type date;
alter table activity.tcustomer_aud alter column payment_target type date;
alter table activity.tinvoice alter column payed type date;
alter table activity.tinvoice alter column invoiced type date;
alter table activity.tinvoice_aud alter column invoiced type date;
alter table activity.tinvoice_aud alter column payed type date;
alter table activity.tjob alter column jobstartdate type date;
alter table activity.tjob alter column payed type date;
alter table activity.tjob alter column realbilling type date;
alter table activity.tjob alter column expectedbilling type date;
alter table activity.tjob alter column jobenddate type date;
alter table activity.tjob_aud alter column expectedbilling type date;
alter table activity.tjob_aud alter column jobstartdate type date;
alter table activity.tjob_aud alter column jobenddate type date;
alter table activity.tjob_aud alter column payed type date;
alter table activity.tjob_aud alter column realbilling type date;
alter table activity.tloss alter column startdate type date;
alter table activity.tloss alter column enddate type date;
alter table activity.tloss_aud alter column startdate type date;
alter table activity.tloss_aud alter column enddate type date;
alter table activity.tovertime alter COLUMN "date" type date;
alter table activity.tovertime_aud alter COLUMN "date" type date;
alter table activity.tposition alter column positionenddate type date;
alter table activity.tposition alter column positionstartdate type date;
alter table activity.tposition_aud alter column positionenddate type date;
alter table activity.tposition_aud alter column positionstartdate type date;
alter table activity.tpos_res_mapping alter column enddate type date;
alter table activity.tpos_res_mapping alter column startdate type date;
alter table activity.tpos_res_mapping_aud alter column enddate type date;
alter table activity.tpos_res_mapping_aud alter column startdate type date;
alter table activity.tproject alter column payment_target type date;
alter table activity.tproject_aud alter column payment_target type date;
alter table activity.tresource alter column exit type date;
alter table activity.tresource alter column entered type date;
alter table activity.tresource alter column birth type date;
alter table activity.tresource_aud alter column exit type date;
alter table activity.tresource_aud alter column entered type date;
alter table activity.tresource_aud alter column birth type date;
alter table activity.ttimer alter column time_diff type date;
alter table activity.ttimer alter column time type date;
alter table activity.ttimer_aud alter column time_diff type date;
alter table activity.ttimer_aud alter column time type date;
COMMIT;
