--decimal numbers columns were numberic (10,0), but have to be numeric (10,2)
BEGIN;
alter table activity.tcost alter column cost type numeric(10,2);
alter table activity.tcustomer alter column dayrate type numeric(10,2);
alter table activity.tinvoice alter column amount type numeric(10,2);
alter table activity.tposition alter column communicatedwork type numeric(10,2);
alter table activity.tposition alter column expectedwork type numeric(10,2);
alter table activity.tposition alter column fixedprice type numeric(10,2);
alter table activity.tposition alter column hourlyrate type numeric(10,2);
alter table activity.tposition alter column income type numeric(10,2);
alter table activity.tposition alter column percentdone type numeric(10,2);
alter table activity.tposition alter column realwork type numeric(10,2);
alter table activity.tproject alter column dayrate type numeric(10,2);
alter table activity.tresource alter column costperhour type numeric(10,2);
alter table activity.tresource alter column overtime_hours type numeric(10,2);
alter table activity.tresource alter column salery type numeric(10,2);
--alter table activity.tovertime alter column decided_by drop NOT NULL;
COMMIT;
