SET search_path = activity, pg_catalog;
BEGIN;
--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE activity.hibernate_sequence OWNER TO activity;

--
-- Name: revinfo; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);

ALTER TABLE activity.revinfo OWNER TO activity;





DROP VIEW vactivities_for_project;
DROP VIEW vres_assignment;
DROP VIEW vres_project_mapping;
DROP VIEW vres_active_project_mapping;

ALTER SEQUENCE activity.tactivity_pk_seq owned by NONE;
ALTER TABLE activity.tactivity alter column pk drop default;
DROP SEQUENCE activity.tactivity_pk_seq;

ALTER SEQUENCE activity.taract_info_pk_seq owned by NONE;
ALTER TABLE activity.taract_info alter column pk drop default;
DROP SEQUENCE activity.taract_info_pk_seq;

ALTER SEQUENCE activity.tbranchoffice_pk_seq owned by NONE;
ALTER TABLE activity.tbranchoffice alter column pk drop default;
DROP SEQUENCE activity.tbranchoffice_pk_seq;

ALTER SEQUENCE activity.tcost_pk_seq owned by NONE;
ALTER TABLE activity.tcost alter column pk drop default;
ALTER SEQUENCE activity.tcost_pk_seq OWNER TO activity;

ALTER SEQUENCE activity.tcosttype_pk_seq owned by NONE;
ALTER TABLE activity.tcosttype alter column pk drop default;
DROP SEQUENCE activity.tcosttype_pk_seq;

ALTER SEQUENCE activity.tcustomer_pk_seq owned by NONE;
ALTER TABLE activity.tcustomer alter column pk drop default;
DROP SEQUENCE activity.tcustomer_pk_seq;


ALTER SEQUENCE activity.tdelete_request_pk_seq owned by NONE;
ALTER TABLE activity.tdelete_request alter column pk drop default;
DROP SEQUENCE activity.tdelete_request_pk_seq;

ALTER SEQUENCE activity.temployment_pk_seq owned by NONE;
ALTER TABLE activity.temployment alter column pk drop default;
DROP SEQUENCE activity.temployment_pk_seq;

ALTER SEQUENCE activity.tinvoice_pk_seq owned by NONE;
ALTER TABLE activity.tinvoice alter column pk drop default;
DROP SEQUENCE activity.tinvoice_pk_seq;

ALTER SEQUENCE activity.tjob_pk_seq owned by NONE;
ALTER TABLE activity.tjob alter column pk drop default;
DROP SEQUENCE activity.tjob_pk_seq;

ALTER SEQUENCE activity.tjobstatus_pk_seq owned by NONE;
ALTER TABLE activity.tjobstatus alter column pk drop default;
DROP SEQUENCE activity.tjobstatus_pk_seq;

ALTER SEQUENCE activity.tjobtype_pk_seq owned by NONE;
ALTER TABLE activity.tjobtype alter column pk drop default;
DROP SEQUENCE activity.tjobtype_pk_seq;

ALTER SEQUENCE activity.tloss_pk_seq owned by NONE;
ALTER TABLE activity.tloss alter column pk drop default;
DROP SEQUENCE activity.tloss_pk_seq;

ALTER SEQUENCE activity.tlossstatus_pk_seq owned by NONE;
ALTER TABLE activity.tlossstatus alter column pk drop default;
DROP SEQUENCE activity.tlossstatus_pk_seq;

ALTER SEQUENCE activity.tlosstype_pk_seq owned by NONE;
ALTER TABLE activity.tlosstype alter column pk drop default;
DROP SEQUENCE activity.tlosstype_pk_seq;

ALTER SEQUENCE activity.tovertime_pk_seq owned by NONE;
ALTER TABLE activity.tovertime alter column pk drop default;
DROP SEQUENCE activity.tovertime_pk_seq;

ALTER SEQUENCE activity.tovertimestatus_pk_seq owned by NONE;
ALTER TABLE activity.tovertimestatus alter column pk drop default;
DROP SEQUENCE activity.tovertimestatus_pk_seq;

ALTER SEQUENCE activity.tpos_res_mapping_pk_seq owned by NONE;
ALTER TABLE activity.tpos_res_mapping alter column pk drop default;
DROP SEQUENCE activity.tpos_res_mapping_pk_seq;

ALTER SEQUENCE activity.tpos_res_status_pk_seq owned by NONE;
ALTER TABLE activity.tpos_res_status alter column pk drop default;
DROP SEQUENCE activity.tpos_res_status_pk_seq;

ALTER SEQUENCE activity.tposition_pk_seq owned by NONE;
ALTER TABLE activity.tposition alter column pk drop default;
DROP SEQUENCE activity.tposition_pk_seq;

ALTER SEQUENCE activity.tpositionstatus_pk_seq owned by NONE;
ALTER TABLE activity.tpositionstatus alter column pk drop default;
DROP SEQUENCE activity.tpositionstatus_pk_seq;

ALTER SEQUENCE activity.tproject_pk_seq owned by NONE;
ALTER TABLE activity.tproject alter column pk drop default;
DROP SEQUENCE activity.tproject_pk_seq;

ALTER SEQUENCE activity.tproject_fields_mapping_pk_seq owned by NONE;
ALTER TABLE activity.tproject_fields_mapping alter column pk drop default;
DROP SEQUENCE activity.tproject_fields_mapping_pk_seq;

ALTER SEQUENCE activity.tprojectfields_pk_seq owned by NONE;
ALTER TABLE activity.tprojectfields alter column pk drop default;
DROP SEQUENCE activity.tprojectfields_pk_seq;

ALTER SEQUENCE activity.tprojectinvoice_pk_seq owned by NONE;
ALTER TABLE activity.tprojectinvoice alter column pk drop default;
DROP SEQUENCE activity.tprojectinvoice_pk_seq;
DROP TABLE activity.tprojectinvoice;

ALTER SEQUENCE activity.tprojectsites_pk_seq owned by NONE;
ALTER TABLE activity.tprojectsites alter column pk drop default;
DROP SEQUENCE activity.tprojectsites_pk_seq;

ALTER SEQUENCE activity.treports_pk_seq owned by NONE;
ALTER TABLE activity.treports alter column pk drop default;
DROP SEQUENCE activity.treports_pk_seq;




ALTER SEQUENCE activity.treports_parameter_pk_seq owned by NONE;
ALTER TABLE activity.treports_parameter alter column pk drop default;
DROP SEQUENCE activity.treports_parameter_pk_seq;
DROP TABLE activity.treports_parameter;

ALTER SEQUENCE activity.treports_ref_pk_seq owned by NONE;
ALTER TABLE activity.treports_ref alter column pk drop default;
DROP SEQUENCE activity.treports_ref_pk_seq;
DROP TABLE activity.treports_ref;

ALTER SEQUENCE activity.tres_ftype_mapping_pk_seq owned by NONE;
ALTER TABLE activity.tres_ftype_mapping alter column pk drop default;
DROP SEQUENCE activity.tres_ftype_mapping_pk_seq;

ALTER SEQUENCE activity.tresource_pk_seq owned by NONE;
ALTER TABLE activity.tresource alter column pk drop default;
DROP SEQUENCE activity.tresource_pk_seq;

ALTER SEQUENCE activity.tresourcetype_pk_seq owned by NONE;
ALTER TABLE activity.tresourcetype alter column pk drop default;
DROP SEQUENCE activity.tresourcetype_pk_seq;

ALTER SEQUENCE activity.tsettings_pk_seq owned by NONE;
ALTER TABLE activity.tsettings alter column pk drop default;
DROP SEQUENCE activity.tsettings_pk_seq;

ALTER SEQUENCE activity.tskills_pk_seq owned by NONE;
ALTER TABLE activity.tskills alter column pk drop default;
DROP SEQUENCE activity.tskills_pk_seq;

ALTER SEQUENCE activity.tskills_def_pk_seq owned by NONE;
ALTER TABLE activity.tskills_def alter column pk drop default;
DROP SEQUENCE activity.tskills_def_pk_seq;

ALTER SEQUENCE activity.ttimer_pk_seq owned by NONE;
ALTER TABLE activity.ttimer alter column pk drop default;
DROP SEQUENCE activity.ttimer_pk_seq;

ALTER SEQUENCE activity.ttooltip_pk_seq owned by NONE;
ALTER TABLE activity.ttooltip alter column pk drop default;
DROP SEQUENCE activity.ttooltip_pk_seq;
DROP TABLE activity.ttooltip;

--ALTER SEQUENCE activity.tbranchoffice_pk_seq owned by NONE;
ALTER TABLE activity.tbranchoffice alter column pk drop default;
--DROP SEQUENCE activity.tbranchoffice_pk_seq;

--ALTER SEQUENCE activity.taract_info_pk_seq owned by NONE;
ALTER TABLE activity.taract_info alter column pk drop default;
--DROP SEQUENCE activity.taract_info_pk_seq;







ALTER TABLE activity.tactivity alter column pk type bigint;
ALTER TABLE activity.tactivity alter column fk_resource type bigint;
ALTER TABLE activity.tactivity alter column fk_position type bigint;
ALTER TABLE activity.tactivity alter column note type character varying (4000);
ALTER TABLE activity.tactivity alter column hours type double precision;
ALTER TABLE activity.tactivity alter column costperhour type double precision;
ALTER TABLE activity.tactivity alter column evolvis_task_id type bigint;

--
-- Name: tactivity_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tactivity_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    costperhour double precision,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    date timestamp without time zone,
    evolvis_task_id bigint,
    hours double precision,
    name character varying(255),
    note character varying(4000),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_position bigint,
    fk_resource bigint
);

ALTER TABLE activity.tactivity_aud OWNER TO activity;

ALTER TABLE activity.taract_info alter column pk type bigint;

ALTER TABLE activity.tbranchoffice alter column pk type bigint;


CREATE TABLE tbranchoffice_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);

ALTER TABLE activity.tbranchoffice_aud OWNER TO activity;

ALTER TABLE activity.tcost alter column pk type bigint;
ALTER TABLE activity.tcost alter column fk_job type bigint;
ALTER TABLE activity.tcost alter column fk_costtype type bigint;
ALTER TABLE activity.tcost alter column fk_resource type bigint;
ALTER TABLE activity.tcost alter column note type character varying (4000);
ALTER TABLE activity.tcost alter column cost drop not null;

--
-- Name: tcost_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tcost_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cost numeric(10,2),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    date timestamp without time zone,
    name character varying(255),
    note character varying(4000),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_costtype bigint,
    fk_job bigint,
    fk_resource bigint
);

ALTER TABLE activity.tcost_aud OWNER TO activity;

ALTER TABLE activity.tcosttype alter column pk type bigint;

ALTER TABLE activity.tcustomer alter column pk type bigint;
ALTER TABLE activity.tcustomer alter column note type character varying (4000);
ALTER TABLE activity.tcustomer alter column dayrate type numeric(10,0) using dayrate::numeric(10,0);

--
-- Name: tcustomer_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tcustomer_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    dayrate numeric(10,2),
    name character varying(255),
    note character varying(4000),
    payment_target timestamp without time zone,
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tcustomer_aud OWNER TO activity;

ALTER TABLE activity.tdelete_request alter column pk type bigint;
ALTER TABLE activity.tdelete_request alter column fk_type type character varying (255);
ALTER TABLE activity.tdelete_request alter column fk_type drop not null;
ALTER TABLE activity.tdelete_request alter column fk_type set default null;
ALTER TABLE activity.tdelete_request alter column fk_controller type bigint;
ALTER TABLE activity.tdelete_request alter column fk_resource type bigint;
ALTER TABLE activity.tdelete_request alter column note type character varying (255);
ALTER TABLE activity.tdelete_request alter column status type character varying (255);
ALTER TABLE activity.tdelete_request alter column status drop not null;
ALTER TABLE activity.tdelete_request alter column status set default null;
ALTER TABLE activity.tdelete_request alter column fk_id type bigint;

ALTER TABLE activity.temployment alter column pk type bigint;
ALTER TABLE activity.temployment alter column note type character varying (255);

--
-- Name: temployment_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE temployment_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.temployment_aud OWNER TO activity;

ALTER TABLE activity.tfunctiontype alter column pk type bigint;

ALTER TABLE activity.tinvoice alter column pk type bigint;
ALTER TABLE activity.tinvoice alter column fk_job type bigint;
ALTER TABLE activity.tinvoice alter column fk_job drop not null;
ALTER TABLE activity.tinvoice alter column fk_job set default null;
ALTER TABLE activity.tinvoice alter column amount type numeric(10,0);
ALTER TABLE activity.tinvoice alter column note type character varying (4000);
ALTER TABLE activity.tinvoice add column fk_project bigint;

--
-- Name: tinvoice_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tinvoice_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    amount numeric(10,2),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    invoiced timestamp without time zone,
    name character varying(255),
    note character varying(4000),
    nr character varying(255),
    payed timestamp without time zone,
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_job bigint,
    fk_project bigint
);

ALTER TABLE activity.tinvoice_aud OWNER TO activity;

ALTER TABLE activity.tjob alter column pk type bigint;
ALTER TABLE activity.tjob alter column fk_project type bigint;
ALTER TABLE activity.tjob alter column fk_jobtype type bigint;
ALTER TABLE activity.tjob alter column fk_jobstatus type bigint;
ALTER TABLE activity.tjob alter column fk_manager type bigint;
ALTER TABLE activity.tjob alter column note type character varying (4000);
ALTER TABLE activity.tjob alter column accounts type bigint;

--
-- Name: tjob_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjob_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    accounts bigint,
    billable character(1),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    crm character varying(255),
    expectedbilling timestamp without time zone,
    ismaintenance character varying(1),
    jobenddate timestamp without time zone,
    jobstartdate timestamp without time zone,
    name character varying(255),
    note character varying(4000),
    nr character varying(255),
    offer character varying(255),
    payed timestamp without time zone,
    realbilling timestamp without time zone,
    request character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_jobstatus bigint,
    fk_jobtype bigint,
    fk_manager bigint,
    fk_project bigint
);


ALTER TABLE activity.tjob_aud OWNER TO activity;

ALTER TABLE activity.tjobstatus alter column pk type bigint;

--
-- Name: tjobstatus_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjobstatus_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tjobstatus_aud OWNER TO activity;

ALTER TABLE activity.tjobtype alter column pk type bigint;

--
-- Name: tjobtype_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjobtype_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tjobtype_aud OWNER TO activity;

ALTER TABLE activity.tloss alter column pk type bigint;
ALTER TABLE activity.tloss alter column fk_lossstatus type bigint;
ALTER TABLE activity.tloss alter column fk_losstype type bigint;
ALTER TABLE activity.tloss alter column fk_resource type bigint;
ALTER TABLE activity.tloss alter column year type bigint;
ALTER TABLE activity.tloss alter column note type character varying (4000);
ALTER TABLE activity.tloss alter column fk_pl1 type bigint;
ALTER TABLE activity.tloss alter column fk_pl1_status type bigint;
ALTER TABLE activity.tloss alter column fk_pl2 type bigint;
ALTER TABLE activity.tloss alter column fk_pl2_status type bigint;
ALTER TABLE activity.tloss alter column fk_pl3 type bigint;
ALTER TABLE activity.tloss alter column fk_pl3_status type bigint;
ALTER TABLE activity.tloss alter column fk_pl4 type bigint;
ALTER TABLE activity.tloss alter column fk_pl4_status type bigint;
ALTER TABLE activity.tloss alter column fk_pl5 type bigint;
ALTER TABLE activity.tloss alter column fk_pl5_status type bigint;

--
-- Name: tloss_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tloss_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    days real,
    enddate timestamp without time zone,
    note character varying(4000),
    startdate timestamp without time zone,
    upd_date timestamp without time zone,
    upd_user character varying(255),
    year bigint,
    fk_lossstatus bigint,
    fk_pl1_status bigint,
    fk_pl2_status bigint,
    fk_pl3_status bigint,
    fk_pl4_status bigint,
    fk_pl5_status bigint,
    fk_losstype bigint,
    fk_pl1 bigint,
    fk_pl2 bigint,
    fk_pl3 bigint,
    fk_pl4 bigint,
    fk_pl5 bigint,
    fk_resource bigint
);


ALTER TABLE activity.tloss_aud OWNER TO activity;

ALTER TABLE activity.tlossdays alter column event_id type bigint;
ALTER TABLE activity.tlossdays alter column days type bigint;

--
-- Name: tlossdays_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tlossdays_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity.tlossdays_pk_seq OWNER TO activity;

ALTER TABLE activity.tlossstatus alter column pk type bigint;

--
-- Name: tlossstatus_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tlossstatus_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tlossstatus_aud OWNER TO activity;

ALTER TABLE activity.tlosstype alter column pk type bigint;

--
-- Name: tlosstype_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tlosstype_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tlosstype_aud OWNER TO activity;

ALTER TABLE activity.tovertime alter column pk type bigint;
ALTER TABLE activity.tovertime alter column fk_resource type bigint;
ALTER TABLE activity.tovertime alter column fk_project type bigint;
ALTER TABLE activity.tovertime alter column fk_overtimestatus type bigint;
ALTER TABLE activity.tovertime alter column hours type double precision;
ALTER TABLE activity.tovertime alter column decided_by type bigint;
ALTER TABLE activity.tovertime alter column year type bigint;
ALTER TABLE activity.tovertime alter column note type character varying (4000);

--
-- Name: tovertime_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tovertime_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    date timestamp without time zone,
    hours double precision,
    note character varying(4000),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    year bigint,
    fk_overtimestatus bigint,
    fk_project bigint,
    fk_resource bigint,
    decided_by bigint
);


ALTER TABLE activity.tovertime_aud OWNER TO activity;

ALTER TABLE activity.tovertimestatus alter column pk type bigint;

--
-- Name: tovertimestatus_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tovertimestatus_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tovertimestatus_aud OWNER TO activity;

--
-- Name: tpermission; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpermission (
    pk bigint NOT NULL,
    actionid character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE activity.tpermission OWNER TO activity;

--
-- Name: tpermission_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tpermission_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity.tpermission_pk_seq OWNER TO activity;

ALTER TABLE activity.tpos_res_mapping alter column pk type bigint;
ALTER TABLE activity.tpos_res_mapping alter column fk_position type bigint;
ALTER TABLE activity.tpos_res_mapping alter column fk_resource type bigint;
ALTER TABLE activity.tpos_res_mapping alter column percent type bigint;
ALTER TABLE activity.tpos_res_mapping alter column fk_status type bigint;

--
-- Name: tpos_res_mapping_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpos_res_mapping_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    enddate timestamp without time zone,
    percent bigint,
    startdate timestamp without time zone,
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_status bigint,
    fk_position bigint,
    fk_resource bigint
);


ALTER TABLE activity.tpos_res_mapping_aud OWNER TO activity;

ALTER TABLE activity.tpos_res_status alter column pk type bigint;

ALTER TABLE activity.tposition alter column pk type bigint;
ALTER TABLE activity.tposition alter column fk_job type bigint;
ALTER TABLE activity.tposition alter column fk_positionstatus type bigint;
ALTER TABLE activity.tposition alter column fixedprice type numeric(10,0);
ALTER TABLE activity.tposition alter column hourlyrate type numeric(10,0);
ALTER TABLE activity.tposition alter column costs type numeric(10,0);
ALTER TABLE activity.tposition alter column income type numeric(10,0);
ALTER TABLE activity.tposition alter column note type character varying (4000);
ALTER TABLE activity.tposition alter column expectedwork type numeric(10,0);
ALTER TABLE activity.tposition alter column realwork type numeric(10,0);
ALTER TABLE activity.tposition alter column communicatedwork type numeric(10,0);
ALTER TABLE activity.tposition alter column percentdone type numeric(10,0);
ALTER TABLE activity.tposition alter column evolvisprojektid type bigint;

--
-- Name: tposition_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tposition_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    communicatedwork numeric(10,2),
    costs numeric(10,2),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    evolvisprojektid bigint,
    evolvisurl character varying(255),
    expectedwork numeric(10,2),
    fixedprice numeric(10,2),
    hourlyrate numeric(10,2),
    income numeric(10,2),
    name character varying(255),
    note character varying(4000),
    percentdone numeric(10,2),
    positionenddate timestamp without time zone,
    positionstartdate timestamp without time zone,
    realwork numeric(10,2),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_job bigint,
    fk_positionstatus bigint
);


ALTER TABLE activity.tposition_aud OWNER TO activity;

ALTER TABLE activity.tpositionstatus alter column pk type bigint;

--
-- Name: tpositionstatus_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpositionstatus_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    name character varying(255),
    note character varying(1000),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tpositionstatus_aud OWNER TO activity;

ALTER TABLE activity.tproject alter column pk type bigint;
ALTER TABLE activity.tproject alter column fk_customer type bigint;
ALTER TABLE activity.tproject alter column note type character varying (4000);
ALTER TABLE activity.tproject alter column dayrate type numeric(10,0);
ALTER TABLE activity.tproject alter column fk_resource type bigint;
ALTER TABLE activity.tproject alter column accounts type bigint;
ALTER TABLE activity.tproject add column crm character varying (255);
ALTER TABLE activity.tproject add column riskanalysis character varying (255);
ALTER TABLE activity.tproject add column sonar character varying (255);

--
-- Name: tproject_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tproject_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    accounts bigint,
    contact_person character varying(255),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    crm character varying(255),
    dayrate numeric(10,2),
    dms character varying(255),
    enddate date,
    fk_resource bigint,
    issue_tracking character varying(255),
    mailinglists character varying(255),
    name character varying(255),
    note character varying(4000),
    offer character varying(255),
    page_auftrag character varying(255),
    payment_target timestamp without time zone,
    process_wiki character varying(255),
    product_backlog character varying(255),
    project_blog character varying(255),
    project_plan character varying(255),
    project_site character varying(255),
    protocolls character varying(255),
    riskanalysis character varying(255),
    sonar character varying(255),
    source_control character varying(255),
    specification character varying(255),
    sprint_backlog character varying(255),
    startdate date,
    test_protokoll character varying(255),
    test_server character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    wiki character varying(255),
    fk_customer bigint
);


ALTER TABLE activity.tproject_aud OWNER TO activity;

ALTER TABLE activity.tproject_fields_mapping alter column pk type bigint;
ALTER TABLE activity.tproject_fields_mapping alter column fk_project type bigint;
ALTER TABLE activity.tproject_fields_mapping alter column fk_projectfield type bigint;
ALTER TABLE activity.tproject_fields_mapping alter column sort type bigint;

--
-- Name: tproject_fields_mapping_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tproject_fields_mapping_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    fieldcontent character varying(255),
    sort bigint,
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_project bigint,
    fk_projectfield bigint
);


ALTER TABLE activity.tproject_fields_mapping_aud OWNER TO activity;

ALTER TABLE activity.tprojectfields alter column pk type bigint;

--
-- Name: tprojectfields_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tprojectfields_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    basefield integer,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    fieldname character varying(255),
    icon character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255)
);


ALTER TABLE activity.tprojectfields_aud OWNER TO activity;

ALTER TABLE activity.tprojectsites alter column pk type bigint;
ALTER TABLE activity.tprojectsites alter column fk_project type bigint;
ALTER TABLE activity.tprojectsites alter column sitecontent type text;

--
-- Name: tprojectsites_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tprojectsites_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    sitecontent text,
    sitename character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_project bigint
);


ALTER TABLE activity.tprojectsites_aud OWNER TO activity;

ALTER TABLE activity.treports alter column pk type bigint;
ALTER TABLE activity.treports alter column name type character varying (255);
ALTER TABLE activity.treports alter column name drop not null;
ALTER TABLE activity.treports alter column name set default null;
ALTER TABLE activity.treports alter column permission type bigint;
ALTER TABLE activity.treports alter column permission drop not null;
ALTER TABLE activity.treports alter column permission set default null;
ALTER TABLE activity.treports alter column note type character varying (255);
ALTER TABLE activity.treports alter column fk_resource type bigint;
ALTER TABLE activity.treports alter column fk_resource drop not null;
ALTER TABLE activity.treports alter column fk_resource set default null;
ALTER TABLE activity.treports alter column data type character varying (255);
ALTER TABLE activity.treports alter column data drop not null;
ALTER TABLE activity.treports alter column data set default null;

--
-- Name: treports_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE treports_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity.treports_pk_seq OWNER TO activity;

ALTER TABLE activity.tres_ftype_mapping alter column pk type bigint;
ALTER TABLE activity.tres_ftype_mapping alter column fk_functiontype type bigint;
ALTER TABLE activity.tres_ftype_mapping alter column fk_resource type bigint;

--
-- Name: tres_ftype_mapping_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tres_ftype_mapping_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_functiontype bigint,
    fk_resource bigint
);


ALTER TABLE activity.tres_ftype_mapping_aud OWNER TO activity;

ALTER TABLE activity.tresource alter column pk type bigint;
ALTER TABLE activity.tresource alter column fk_resourcetype type bigint;
ALTER TABLE activity.tresource alter column fk_resourcetype set default 0;
ALTER TABLE activity.tresource alter column note type character varying (4000);
ALTER TABLE activity.tresource alter column rights type bigint;
ALTER TABLE activity.tresource alter column salery type numeric(10,0);
ALTER TABLE activity.tresource alter column costperhour type numeric(10,0);
ALTER TABLE activity.tresource alter column fk_tcid type bigint;
ALTER TABLE activity.tresource alter column available_hours type bigint;
ALTER TABLE activity.tresource alter column remain_holiday type numeric(5,2) using remain_holiday::numeric(5,2);
ALTER TABLE activity.tresource alter column remain_holiday set default 0.00;
ALTER TABLE activity.tresource alter column holiday type numeric(5,2) using holiday::numeric(5,2);
ALTER TABLE activity.tresource drop column fk_functiontype;
ALTER TABLE activity.tresource alter column fk_employment type bigint;
ALTER TABLE activity.tresource alter column overtime_hours type numeric(10,0);
ALTER TABLE activity.tresource alter column fk_branchoffice type bigint;
ALTER TABLE activity.tresource add column groups character varying (4000);

--
-- Name: tresource_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tresource_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    active character(1),
    available_hours bigint,
    birth timestamp without time zone,
    costperhour numeric(10,2),
    cr_date timestamp without time zone,
    cr_user character varying(255),
    entered timestamp without time zone,
    exit timestamp without time zone,
    firstname character varying(255),
    fk_tcid bigint,
    groups character varying(4000),
    holiday numeric(5,2),
    lastname character varying(255),
    mail character varying(255),
    note character varying(4000),
    overtime_hours numeric(10,2),
    passwd character varying(32),
    remain_holiday numeric(5,2) DEFAULT 0.00,
    rights bigint,
    salery numeric(10,2),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    username character varying(10),
    fk_branchoffice bigint,
    fk_employment bigint,
    fk_resourcetype bigint
);


ALTER TABLE activity.tresource_aud OWNER TO activity;

--
-- Name: tresources_tpermissions; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tresources_tpermissions (
    resources_pk bigint NOT NULL,
    permissions_pk bigint NOT NULL
);


ALTER TABLE activity.tresources_tpermissions OWNER TO activity;

--
-- Name: tresources_tpermissions_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tresources_tpermissions_aud (
    rev integer NOT NULL,
    resources_pk bigint NOT NULL,
    permissions_pk bigint NOT NULL,
    revtype smallint
);


ALTER TABLE activity.tresources_tpermissions_aud OWNER TO activity;

ALTER table activity.tresourcetype alter column pk type bigint;

--
-- Name: trole; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE trole (
    pk bigint NOT NULL,
    description character varying(1000),
    name character varying(255) NOT NULL
);


ALTER TABLE activity.trole OWNER TO activity;

--
-- Name: trole_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE trole_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity.trole_pk_seq OWNER TO activity;

--
-- Name: troles_tpermissions; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE troles_tpermissions (
    roles_pk bigint NOT NULL,
    permissions_pk bigint NOT NULL
);


ALTER TABLE activity.troles_tpermissions OWNER TO activity;

--
-- Name: troles_tresources; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE troles_tresources (
    roles_pk bigint NOT NULL,
    resources_pk bigint NOT NULL
);


ALTER TABLE activity.troles_tresources OWNER TO activity;

ALTER TABLE activity.tsettings alter column pk type bigint;

ALTER TABLE activity.tskills alter column pk type bigint;
ALTER TABLE activity.tskills alter column fk_resource type bigint;
ALTER TABLE activity.tskills alter column fk_skills_def type bigint;

--
-- Name: tskills_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tskills_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    upd_date timestamp without time zone,
    upd_user character varying(255),
    value integer,
    fk_resource bigint,
    fk_skills_def bigint
);


ALTER TABLE activity.tskills_aud OWNER TO activity;

ALTER TABLE activity.tskills_def alter column pk type bigint;
ALTER TABLE activity.tskills_def alter column name type character varying (255);

--
-- Name: ttbranchoffice_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE ttbranchoffice_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity.ttbranchoffice_pk_seq OWNER TO activity;

ALTER TABLE activity.ttimer alter column pk type bigint;
ALTER TABLE activity.ttimer alter column fk_resource type bigint;
ALTER TABLE activity.ttimer alter column fk_position type bigint;

--
-- Name: ttimer_aud; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE ttimer_aud (
    pk bigint NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    cr_date timestamp without time zone,
    cr_user character varying(255),
    "time" timestamp without time zone,
    time_diff timestamp without time zone,
    upd_date timestamp without time zone,
    upd_user character varying(255),
    fk_position bigint,
    fk_resource bigint
);


ALTER TABLE activity.ttimer_aud OWNER TO activity;

--
-- Name: vres_active_project_mapping; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE vres_active_project_mapping (
    fk_project bigint NOT NULL,
    fk_resource bigint NOT NULL,
    fk_status integer
);


ALTER TABLE activity.vres_active_project_mapping OWNER TO activity;




--
-- Name: revinfo_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: tactivity_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tactivity_aud
    ADD CONSTRAINT tactivity_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tactivity_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tactivity
    ADD CONSTRAINT tactivity_pkey PRIMARY KEY (pk);


--
-- Name: taract_info_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY taract_info
    ADD CONSTRAINT taract_info_pkey PRIMARY KEY (pk);


--
-- Name: tbranchoffice_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tbranchoffice_aud
    ADD CONSTRAINT tbranchoffice_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tbranchoffice_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tbranchoffice
    ADD CONSTRAINT tbranchoffice_pkey PRIMARY KEY (pk);


--
-- Name: tcost_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tcost_aud
    ADD CONSTRAINT tcost_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tcost_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tcost
    ADD CONSTRAINT tcost_pkey PRIMARY KEY (pk);


--
-- Name: tcosttype_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tcosttype
    ADD CONSTRAINT tcosttype_pkey PRIMARY KEY (pk);


--
-- Name: tcustomer_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tcustomer_aud
    ADD CONSTRAINT tcustomer_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tcustomer_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tcustomer
    ADD CONSTRAINT tcustomer_pkey PRIMARY KEY (pk);


--
-- Name: tdelete_request_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tdelete_request
    ADD CONSTRAINT tdelete_request_pkey PRIMARY KEY (pk);


--
-- Name: temployment_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY temployment_aud
    ADD CONSTRAINT temployment_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: temployment_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY temployment
    ADD CONSTRAINT temployment_pkey PRIMARY KEY (pk);


--
-- Name: tfunctiontype_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tfunctiontype
    ADD CONSTRAINT tfunctiontype_pkey PRIMARY KEY (pk);


--
-- Name: tinvoice_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tinvoice_aud
    ADD CONSTRAINT tinvoice_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tinvoice_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tinvoice
    ADD CONSTRAINT tinvoice_pkey PRIMARY KEY (pk);


--
-- Name: tjob_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjob_aud
    ADD CONSTRAINT tjob_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tjob_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT tjob_pkey PRIMARY KEY (pk);


--
-- Name: tjobstatus_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjobstatus_aud
    ADD CONSTRAINT tjobstatus_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tjobstatus_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjobstatus
    ADD CONSTRAINT tjobstatus_pkey PRIMARY KEY (pk);


--
-- Name: tjobtype_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjobtype_aud
    ADD CONSTRAINT tjobtype_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tjobtype_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tjobtype
    ADD CONSTRAINT tjobtype_pkey PRIMARY KEY (pk);


--
-- Name: tloss_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tloss_aud
    ADD CONSTRAINT tloss_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tloss_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT tloss_pkey PRIMARY KEY (pk);


--
-- Name: tlossdays_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tlossdays
    ADD CONSTRAINT tlossdays_pkey PRIMARY KEY (event_id);


--
-- Name: tlossstatus_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tlossstatus_aud
    ADD CONSTRAINT tlossstatus_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tlossstatus_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tlossstatus
    ADD CONSTRAINT tlossstatus_pkey PRIMARY KEY (pk);


--
-- Name: tlosstype_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tlosstype_aud
    ADD CONSTRAINT tlosstype_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tlosstype_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tlosstype
    ADD CONSTRAINT tlosstype_pkey PRIMARY KEY (pk);


--
-- Name: tovertime_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tovertime_aud
    ADD CONSTRAINT tovertime_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tovertime_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tovertime
    ADD CONSTRAINT tovertime_pkey PRIMARY KEY (pk);


--
-- Name: tovertimestatus_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tovertimestatus_aud
    ADD CONSTRAINT tovertimestatus_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tovertimestatus_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tovertimestatus
    ADD CONSTRAINT tovertimestatus_pkey PRIMARY KEY (pk);


--
-- Name: tpermission_actionid_key; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpermission
    ADD CONSTRAINT tpermission_actionid_key UNIQUE (actionid);


--
-- Name: tpermission_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpermission
    ADD CONSTRAINT tpermission_pkey PRIMARY KEY (pk);


--
-- Name: tpos_res_mapping_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpos_res_mapping_aud
    ADD CONSTRAINT tpos_res_mapping_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tpos_res_mapping_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpos_res_mapping
    ADD CONSTRAINT tpos_res_mapping_pkey PRIMARY KEY (pk);


--
-- Name: tpos_res_status_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpos_res_status
    ADD CONSTRAINT tpos_res_status_pkey PRIMARY KEY (pk);


--
-- Name: tposition_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tposition_aud
    ADD CONSTRAINT tposition_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tposition_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tposition
    ADD CONSTRAINT tposition_pkey PRIMARY KEY (pk);


--
-- Name: tpositionstatus_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpositionstatus_aud
    ADD CONSTRAINT tpositionstatus_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tpositionstatus_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tpositionstatus
    ADD CONSTRAINT tpositionstatus_pkey PRIMARY KEY (pk);


--
-- Name: tproject_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tproject_aud
    ADD CONSTRAINT tproject_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tproject_fields_mapping_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tproject_fields_mapping_aud
    ADD CONSTRAINT tproject_fields_mapping_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tproject_fields_mapping_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tproject_fields_mapping
    ADD CONSTRAINT tproject_fields_mapping_pkey PRIMARY KEY (pk);


--
-- Name: tproject_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tproject
    ADD CONSTRAINT tproject_pkey PRIMARY KEY (pk);


--
-- Name: tprojectfields_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tprojectfields_aud
    ADD CONSTRAINT tprojectfields_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tprojectfields_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tprojectfields
    ADD CONSTRAINT tprojectfields_pkey PRIMARY KEY (pk);


--
-- Name: tprojectsites_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tprojectsites_aud
    ADD CONSTRAINT tprojectsites_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tprojectsites_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tprojectsites
    ADD CONSTRAINT tprojectsites_pkey PRIMARY KEY (pk);


--
-- Name: treports_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY treports
    ADD CONSTRAINT treports_pkey PRIMARY KEY (pk);


--
-- Name: tres_ftype_mapping_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tres_ftype_mapping_aud
    ADD CONSTRAINT tres_ftype_mapping_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tres_ftype_mapping_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tres_ftype_mapping
    ADD CONSTRAINT tres_ftype_mapping_pkey PRIMARY KEY (pk);


--
-- Name: tresource_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresource_aud
    ADD CONSTRAINT tresource_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tresource_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresource
    ADD CONSTRAINT tresource_pkey PRIMARY KEY (pk);


--
-- Name: tresource_username_key; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresource
    ADD CONSTRAINT tresource_username_key UNIQUE (username);


--
-- Name: tresources_tpermissions_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresources_tpermissions_aud
    ADD CONSTRAINT tresources_tpermissions_aud_pkey PRIMARY KEY (rev, resources_pk, permissions_pk);


--
-- Name: tresources_tpermissions_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresources_tpermissions
    ADD CONSTRAINT tresources_tpermissions_pkey PRIMARY KEY (resources_pk, permissions_pk);


--
-- Name: tresourcetype_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tresourcetype
    ADD CONSTRAINT tresourcetype_pkey PRIMARY KEY (pk);


--
-- Name: trole_name_key; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY trole
    ADD CONSTRAINT trole_name_key UNIQUE (name);


--
-- Name: trole_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY trole
    ADD CONSTRAINT trole_pkey PRIMARY KEY (pk);


--
-- Name: troles_tpermissions_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY troles_tpermissions
    ADD CONSTRAINT troles_tpermissions_pkey PRIMARY KEY (roles_pk, permissions_pk);


--
-- Name: troles_tresources_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY troles_tresources
    ADD CONSTRAINT troles_tresources_pkey PRIMARY KEY (roles_pk, resources_pk);


--
-- Name: tsettings_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tsettings
    ADD CONSTRAINT tsettings_pkey PRIMARY KEY (pk);


--
-- Name: tskills_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tskills_aud
    ADD CONSTRAINT tskills_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: tskills_def_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tskills_def
    ADD CONSTRAINT tskills_def_pkey PRIMARY KEY (pk);


--
-- Name: tskills_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY tskills
    ADD CONSTRAINT tskills_pkey PRIMARY KEY (pk);


--
-- Name: ttimer_aud_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY ttimer_aud
    ADD CONSTRAINT ttimer_aud_pkey PRIMARY KEY (pk, rev);


--
-- Name: ttimer_fk_resource_key; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY ttimer
    ADD CONSTRAINT ttimer_fk_resource_key UNIQUE (fk_resource);


--
-- Name: ttimer_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY ttimer
    ADD CONSTRAINT ttimer_pkey PRIMARY KEY (pk);


--
-- Name: vres_active_project_mapping_pkey; Type: CONSTRAINT; Schema: activity; Owner: activity; Tablespace:
--

ALTER TABLE ONLY vres_active_project_mapping
    ADD CONSTRAINT vres_active_project_mapping_pkey PRIMARY KEY (fk_project, fk_resource);


--
-- Name: fk10ea76a2df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tlosstype_aud
    ADD CONSTRAINT fk10ea76a2df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk10fc15e45ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY vres_active_project_mapping
    ADD CONSTRAINT fk10fc15e45ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk10fc15e4fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY vres_active_project_mapping
    ADD CONSTRAINT fk10fc15e4fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk156afceedf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tposition_aud
    ADD CONSTRAINT fk156afceedf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk15b67b92df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcost_aud
    ADD CONSTRAINT fk15b67b92df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk1aae108213aed99c; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresource
    ADD CONSTRAINT fk1aae108213aed99c FOREIGN KEY (fk_branchoffice) REFERENCES tbranchoffice(pk);


--
-- Name: fk1aae108238b65470; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresource
    ADD CONSTRAINT fk1aae108238b65470 FOREIGN KEY (fk_resourcetype) REFERENCES tresourcetype(pk);


--
-- Name: fk1aae1082884fdf98; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresource
    ADD CONSTRAINT fk1aae1082884fdf98 FOREIGN KEY (fk_employment) REFERENCES temployment(pk);


--
-- Name: fk1ef96173df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject_fields_mapping_aud
    ADD CONSTRAINT fk1ef96173df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk21ff0b58df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectsites_aud
    ADD CONSTRAINT fk21ff0b58df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk2d9374dc4d5bca04; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tres_ftype_mapping
    ADD CONSTRAINT fk2d9374dc4d5bca04 FOREIGN KEY (fk_functiontype) REFERENCES tfunctiontype(pk);


--
-- Name: fk2d9374dcfe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tres_ftype_mapping
    ADD CONSTRAINT fk2d9374dcfe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk34e373afdf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresources_tpermissions_aud
    ADD CONSTRAINT fk34e373afdf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk3656c95ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT fk3656c95ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk3656c976a4303e; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT fk3656c976a4303e FOREIGN KEY (fk_jobstatus) REFERENCES tjobstatus(pk);


--
-- Name: fk3656c97f31fd4e; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT fk3656c97f31fd4e FOREIGN KEY (fk_jobtype) REFERENCES tjobtype(pk);


--
-- Name: fk3656c9abaec7c7; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob
    ADD CONSTRAINT fk3656c9abaec7c7 FOREIGN KEY (fk_manager) REFERENCES tresource(pk);


--
-- Name: fk36644a23df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tbranchoffice_aud
    ADD CONSTRAINT fk36644a23df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk447e3376df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject_aud
    ADD CONSTRAINT fk447e3376df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk490d8123df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcustomer_aud
    ADD CONSTRAINT fk490d8123df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk4922018adf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tinvoice_aud
    ADD CONSTRAINT fk4922018adf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk4a12b767889d5fe9; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY troles_tresources
    ADD CONSTRAINT fk4a12b767889d5fe9 FOREIGN KEY (resources_pk) REFERENCES tresource(pk);


--
-- Name: fk4a12b767985017b9; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY troles_tresources
    ADD CONSTRAINT fk4a12b767985017b9 FOREIGN KEY (roles_pk) REFERENCES trole(pk);


--
-- Name: fk4c74b9075ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectsites
    ADD CONSTRAINT fk4c74b9075ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk4e9a52151a0e070e; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime
    ADD CONSTRAINT fk4e9a52151a0e070e FOREIGN KEY (decided_by) REFERENCES tresource(pk);


--
-- Name: fk4e9a52155ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime
    ADD CONSTRAINT fk4e9a52155ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk4e9a5215b0c65586; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime
    ADD CONSTRAINT fk4e9a5215b0c65586 FOREIGN KEY (fk_overtimestatus) REFERENCES tovertimestatus(pk);


--
-- Name: fk4e9a5215fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime
    ADD CONSTRAINT fk4e9a5215fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk5b94e59d2065f096; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tposition
    ADD CONSTRAINT fk5b94e59d2065f096 FOREIGN KEY (fk_positionstatus) REFERENCES tpositionstatus(pk);


--
-- Name: fk5b94e59df359d7ba; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tposition
    ADD CONSTRAINT fk5b94e59df359d7ba FOREIGN KEY (fk_job) REFERENCES tjob(pk);


--
-- Name: fk6915641f359d7ba; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcost
    ADD CONSTRAINT fk6915641f359d7ba FOREIGN KEY (fk_job) REFERENCES tjob(pk);


--
-- Name: fk6915641f4721cee; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcost
    ADD CONSTRAINT fk6915641f4721cee FOREIGN KEY (fk_costtype) REFERENCES tcosttype(pk);


--
-- Name: fk6915641fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcost
    ADD CONSTRAINT fk6915641fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk6956d97298b5ded; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d97298b5ded FOREIGN KEY (fk_pl5_status) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d97422b159a; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d97422b159a FOREIGN KEY (fk_losstype) REFERENCES tlosstype(pk);


--
-- Name: fk6956d9753979ccf; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9753979ccf FOREIGN KEY (fk_pl1) REFERENCES tresource(pk);


--
-- Name: fk6956d9753979cd0; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9753979cd0 FOREIGN KEY (fk_pl2) REFERENCES tresource(pk);


--
-- Name: fk6956d9753979cd1; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9753979cd1 FOREIGN KEY (fk_pl3) REFERENCES tresource(pk);


--
-- Name: fk6956d9753979cd2; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9753979cd2 FOREIGN KEY (fk_pl4) REFERENCES tresource(pk);


--
-- Name: fk6956d9753979cd3; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9753979cd3 FOREIGN KEY (fk_pl5) REFERENCES tresource(pk);


--
-- Name: fk6956d9759c9042f; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d9759c9042f FOREIGN KEY (fk_pl3_status) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d975fb8658a; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d975fb8658a FOREIGN KEY (fk_lossstatus) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d978a06aa71; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d978a06aa71 FOREIGN KEY (fk_pl1_status) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d97c1aa310e; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d97c1aa310e FOREIGN KEY (fk_pl4_status) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d97f1e7d750; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d97f1e7d750 FOREIGN KEY (fk_pl2_status) REFERENCES tlossstatus(pk);


--
-- Name: fk6956d97fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss
    ADD CONSTRAINT fk6956d97fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk6c142131df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY temployment_aud
    ADD CONSTRAINT fk6c142131df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk6c2c54a25ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject_fields_mapping
    ADD CONSTRAINT fk6c2c54a25ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk6c2c54a2b5895027; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject_fields_mapping
    ADD CONSTRAINT fk6c2c54a2b5895027 FOREIGN KEY (fk_projectfield) REFERENCES tprojectfields(pk);


--
-- Name: fk712696f45ca08ea; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tdelete_request
    ADD CONSTRAINT fk712696f45ca08ea FOREIGN KEY (fk_controller) REFERENCES tresource(pk);


--
-- Name: fk712696ffe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tdelete_request
    ADD CONSTRAINT fk712696ffe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk7478f3d4df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tactivity_aud
    ADD CONSTRAINT fk7478f3d4df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk7d1493d08028ba12; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_mapping
    ADD CONSTRAINT fk7d1493d08028ba12 FOREIGN KEY (fk_position) REFERENCES tposition(pk);


--
-- Name: fk7d1493d0edde62fa; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_mapping
    ADD CONSTRAINT fk7d1493d0edde62fa FOREIGN KEY (fk_status) REFERENCES tpos_res_status(pk);


--
-- Name: fk7d1493d0fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_mapping
    ADD CONSTRAINT fk7d1493d0fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fk7f1fe44cdf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjobstatus_aud
    ADD CONSTRAINT fk7f1fe44cdf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fk86de98395ad1672; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tinvoice
    ADD CONSTRAINT fk86de98395ad1672 FOREIGN KEY (fk_project) REFERENCES tproject(pk);


--
-- Name: fk86de9839f359d7ba; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tinvoice
    ADD CONSTRAINT fk86de9839f359d7ba FOREIGN KEY (fk_job) REFERENCES tjob(pk);


--
-- Name: fk9b01d4addf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tres_ftype_mapping_aud
    ADD CONSTRAINT fk9b01d4addf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fka3fa5467f69516b; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY troles_tpermissions
    ADD CONSTRAINT fka3fa5467f69516b FOREIGN KEY (permissions_pk) REFERENCES tpermission(pk);


--
-- Name: fka3fa546985017b9; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY troles_tpermissions
    ADD CONSTRAINT fka3fa546985017b9 FOREIGN KEY (roles_pk) REFERENCES trole(pk);


--
-- Name: fka8f00dd4df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjobtype_aud
    ADD CONSTRAINT fka8f00dd4df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkb4784ca2df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY ttimer_aud
    ADD CONSTRAINT fkb4784ca2df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkbc57f81adf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob_aud
    ADD CONSTRAINT fkbc57f81adf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkbc92f7e8df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss_aud
    ADD CONSTRAINT fkbc92f7e8df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkc2a559b61dbeb5df; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tskills
    ADD CONSTRAINT fkc2a559b61dbeb5df FOREIGN KEY (fk_skills_def) REFERENCES tskills_def(pk);


--
-- Name: fkc2a559b6fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tskills
    ADD CONSTRAINT fkc2a559b6fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fkcc4e3f838028ba12; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tactivity
    ADD CONSTRAINT fkcc4e3f838028ba12 FOREIGN KEY (fk_position) REFERENCES tposition(pk);


--
-- Name: fkcc4e3f83fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tactivity
    ADD CONSTRAINT fkcc4e3f83fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fkcc862f518028ba12; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY ttimer
    ADD CONSTRAINT fkcc862f518028ba12 FOREIGN KEY (fk_position) REFERENCES tposition(pk);


--
-- Name: fkcc862f51fe5b0fdc; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY ttimer
    ADD CONSTRAINT fkcc862f51fe5b0fdc FOREIGN KEY (fk_resource) REFERENCES tresource(pk);


--
-- Name: fkd223519adf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tlossstatus_aud
    ADD CONSTRAINT fkd223519adf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkd881bc87df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tskills_aud
    ADD CONSTRAINT fkd881bc87df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkdceb3098df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertimestatus_aud
    ADD CONSTRAINT fkdceb3098df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fke390e4de7f69516b; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresources_tpermissions
    ADD CONSTRAINT fke390e4de7f69516b FOREIGN KEY (permissions_pk) REFERENCES tpermission(pk);


--
-- Name: fke390e4de889d5fe9; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresources_tpermissions
    ADD CONSTRAINT fke390e4de889d5fe9 FOREIGN KEY (resources_pk) REFERENCES tresource(pk);


--
-- Name: fke588fa20df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpositionstatus_aud
    ADD CONSTRAINT fke588fa20df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkf63259a1df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_mapping_aud
    ADD CONSTRAINT fkf63259a1df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkf6756d66df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime_aud
    ADD CONSTRAINT fkf6756d66df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkf9d01defdf74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectfields_aud
    ADD CONSTRAINT fkf9d01defdf74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkfc7a2d53df74e053; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresource_aud
    ADD CONSTRAINT fkfc7a2d53df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: fkff98a0256f46877c; Type: FK CONSTRAINT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject
    ADD CONSTRAINT fkff98a0256f46877c FOREIGN KEY (fk_customer) REFERENCES tcustomer(pk);

 	create sequence activity.tactivity_pk_seq;

    --create sequence activity.tcost_pk_seq;

    create sequence activity.tcosttype_pk_seq;

    create sequence activity.tcustomer_pk_seq;

    create sequence activity.tdelete_request_pk_seq;

    create sequence activity.temployment_pk_seq;

    --create sequence activity.tfunctiontype_pk_seq;

    create sequence activity.tinvoice_pk_seq;

    create sequence activity.tjob_pk_seq;

    create sequence activity.tjobstatus_pk_seq;

    create sequence activity.tjobtype_pk_seq;

    create sequence activity.tloss_pk_seq;

    --create sequence activity.tlossdays_pk_seq;

    create sequence activity.tlossstatus_pk_seq;

    create sequence activity.tlosstype_pk_seq;

    create sequence activity.tovertime_pk_seq;

    create sequence activity.tovertimestatus_pk_seq;

    --create sequence activity.tpermission_pk_seq;

    create sequence activity.tpos_res_mapping_pk_seq;

    create sequence activity.tpos_res_status_pk_seq;

    create sequence activity.tposition_pk_seq;

    create sequence activity.tpositionstatus_pk_seq;

    create sequence activity.tproject_fields_mapping_pk_seq;

    create sequence activity.tproject_pk_seq;

    create sequence activity.tprojectfields_pk_seq;

    create sequence activity.tprojectsites_pk_seq;

    --create sequence activity.treports_pk_seq;

    create sequence activity.tres_ftype_mapping_pk_seq;

    create sequence activity.tresource_pk_seq;

    create sequence activity.tresourcetype_pk_seq;

    --create sequence activity.trole_pk_seq;

    create sequence activity.tsettings_pk_seq;

    create sequence activity.tskills_def_pk_seq;

    create sequence activity.tskills_pk_seq;

    --create sequence activity.ttbranchoffice_pk_seq;

    create sequence activity.ttimer_pk_seq;
COMMIT;
