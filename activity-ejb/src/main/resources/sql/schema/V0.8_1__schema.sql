-- view for Jasper-Reports (cpreil)

CREATE OR REPLACE VIEW activity.vformatted_dates AS (
      SELECT DISTINCT
        date,
        date_part('year', date) AS year,
        date_part('quarter', date) AS quarter,
        date_part('month', date) AS month,
        date_part('week', date) AS week,
        date_part('day', date) AS day
      from activity.tactivity ORDER BY date ASC
);
