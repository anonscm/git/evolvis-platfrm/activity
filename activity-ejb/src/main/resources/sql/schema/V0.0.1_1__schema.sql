BEGIN;
--
-- activityQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: activity; Type: SCHEMA; Schema: -; Owner: activity
--



--ALTER SCHEMA activity OWNER TO activity;

SET search_path = activity, pg_catalog;

--
-- Name: concat(text); Type: AGGREGATE; Schema: activity; Owner: activity
--

CREATE AGGREGATE concat(text) (
    SFUNC = textcat,
    STYPE = text,
    INITCOND = ''
);

--@CHANGE
ALTER AGGREGATE concat(text) OWNER TO activity;

SET search_path = activity, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tactivity; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tactivity (
    pk integer NOT NULL,
    fk_resource integer NOT NULL,
    fk_position integer NOT NULL,
    name character varying(255),
    date timestamp without time zone,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    hours numeric(10,2),
    costperhour numeric(10,2),
    evolvis_task_id integer
);


ALTER TABLE activity.tactivity OWNER TO activity;

--
-- Name: tactivity_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tactivity_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tactivity_pk_seq OWNER TO activity;

--
-- Name: tactivity_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tactivity_pk_seq OWNED BY tactivity.pk;


--
-- Name: tactivity_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tactivity_pk_seq', 209078, true);


--
-- Name: taract_info; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE taract_info (
    dbversion_string character varying(255),
    pk integer NOT NULL
);


ALTER TABLE activity.taract_info OWNER TO activity;

--
-- Name: COLUMN taract_info.dbversion_string; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN taract_info.dbversion_string IS 'full version indentifier';


--
-- Name: taract_info_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE taract_info_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.taract_info_pk_seq OWNER TO activity;

--
-- Name: taract_info_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE taract_info_pk_seq OWNED BY taract_info.pk;


--
-- Name: taract_info_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('taract_info_pk_seq', 7, true);


--
-- Name: tbranchoffice; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tbranchoffice (
    pk integer NOT NULL,
    name character varying(255) NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tbranchoffice OWNER TO activity;

--
-- Name: tbranchoffice_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tbranchoffice_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tbranchoffice_pk_seq OWNER TO activity;

--
-- Name: tbranchoffice_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tbranchoffice_pk_seq OWNED BY tbranchoffice.pk;


--
-- Name: tbranchoffice_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tbranchoffice_pk_seq', 2, true);


--
-- Name: tcost; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tcost (
    pk integer NOT NULL,
    fk_job integer NOT NULL,
    fk_costtype integer NOT NULL,
    fk_resource integer,
    name character varying(255),
    date timestamp without time zone,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    cost numeric(10,2)
);


ALTER TABLE activity.tcost OWNER TO activity;

--
-- Name: tcost_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tcost_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tcost_pk_seq OWNER TO activity;

--
-- Name: tcost_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tcost_pk_seq OWNED BY tcost.pk;


--
-- Name: tcost_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tcost_pk_seq', 36, true);


--
-- Name: tcosttype; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tcosttype (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tcosttype OWNER TO activity;

--
-- Name: tcosttype_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tcosttype_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tcosttype_pk_seq OWNER TO activity;

--
-- Name: tcosttype_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tcosttype_pk_seq OWNED BY tcosttype.pk;


--
-- Name: tcosttype_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tcosttype_pk_seq', 1, false);


--
-- Name: tcustomer; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tcustomer (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    dayrate numeric(10,2),
    payment_target timestamp without time zone
);


ALTER TABLE activity.tcustomer OWNER TO activity;

--
-- Name: tcustomer_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tcustomer_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tcustomer_pk_seq OWNER TO activity;

--
-- Name: tcustomer_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tcustomer_pk_seq OWNED BY tcustomer.pk;


--
-- Name: tcustomer_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tcustomer_pk_seq', 72, true);


--
-- Name: tdelete_request; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tdelete_request (
    pk integer NOT NULL,
    fk_type text NOT NULL,
    fk_controller integer NOT NULL,
    fk_resource integer NOT NULL,
    note text,
    status text NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    fk_id integer
);


ALTER TABLE activity.tdelete_request OWNER TO activity;

--
-- Name: tdelete_request_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tdelete_request_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tdelete_request_pk_seq OWNER TO activity;

--
-- Name: tdelete_request_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tdelete_request_pk_seq OWNED BY tdelete_request.pk;


--
-- Name: tdelete_request_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tdelete_request_pk_seq', 346, true);


--
-- Name: temployment; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE temployment (
    pk integer NOT NULL,
    name character varying(255),
    note text,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.temployment OWNER TO activity;

--
-- Name: temployment_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE temployment_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.temployment_pk_seq OWNER TO activity;

--
-- Name: temployment_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE temployment_pk_seq OWNED BY temployment.pk;


--
-- Name: temployment_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('temployment_pk_seq', 6, true);


--
-- Name: tfunctiontype; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tfunctiontype (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tfunctiontype OWNER TO activity;

--
-- Name: tfunctiontype_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tfunctiontype_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tfunctiontype_pk_seq OWNER TO activity;

--
-- Name: tfunctiontype_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tfunctiontype_pk_seq OWNED BY tfunctiontype.pk;


--
-- Name: tfunctiontype_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tfunctiontype_pk_seq', 4, true);


--
-- Name: tinvoice; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tinvoice (
    pk integer NOT NULL,
    fk_job integer NOT NULL,
    name character varying(255),
    amount numeric(10,2) NOT NULL,
    invoiced timestamp without time zone,
    payed timestamp without time zone,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    nr character varying(255)
);


ALTER TABLE activity.tinvoice OWNER TO activity;

--
-- Name: tinvoice_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tinvoice_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tinvoice_pk_seq OWNER TO activity;

--
-- Name: tinvoice_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tinvoice_pk_seq OWNED BY tinvoice.pk;


--
-- Name: tinvoice_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tinvoice_pk_seq', 1969, true);


--
-- Name: tjob; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjob (
    pk integer NOT NULL,
    fk_project integer NOT NULL,
    fk_jobtype integer NOT NULL,
    fk_jobstatus integer NOT NULL,
    fk_manager integer NOT NULL,
    name character varying(255),
    billable character(1),
    expectedbilling timestamp without time zone,
    realbilling timestamp without time zone,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    payed timestamp without time zone,
    nr character varying(255),
    ismaintenance character varying(1),
    jobstartdate timestamp without time zone,
    accounts integer,
    jobenddate timestamp without time zone,
    offer character varying(255),
    crm character varying(255),
    request character varying(255)
);


ALTER TABLE activity.tjob OWNER TO activity;

--
-- Name: COLUMN tjob.jobstartdate; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tjob.jobstartdate IS 'the job start date';


--
-- Name: COLUMN tjob.accounts; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tjob.accounts IS 'Kostenstelle';


--
-- Name: tjob_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tjob_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tjob_pk_seq OWNER TO activity;

--
-- Name: tjob_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tjob_pk_seq OWNED BY tjob.pk;


--
-- Name: tjob_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tjob_pk_seq', 1278, true);


--
-- Name: tjobstatus; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjobstatus (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tjobstatus OWNER TO activity;

--
-- Name: tjobstatus_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tjobstatus_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tjobstatus_pk_seq OWNER TO activity;

--
-- Name: tjobstatus_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tjobstatus_pk_seq OWNED BY tjobstatus.pk;


--
-- Name: tjobstatus_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tjobstatus_pk_seq', 1, false);


--
-- Name: tjobtype; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tjobtype (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tjobtype OWNER TO activity;

--
-- Name: tjobtype_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tjobtype_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tjobtype_pk_seq OWNER TO activity;

--
-- Name: tjobtype_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tjobtype_pk_seq OWNED BY tjobtype.pk;


--
-- Name: tjobtype_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tjobtype_pk_seq', 1, false);


--
-- Name: tloss; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tloss (
    pk integer NOT NULL,
    fk_lossstatus integer NOT NULL,
    fk_losstype integer NOT NULL,
    fk_resource integer NOT NULL,
    startdate timestamp without time zone,
    enddate timestamp without time zone,
    year integer,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    days real,
    fk_pl5 integer,
    fk_pl1 integer,
    fk_pl2 integer,
    fk_pl3 integer,
    fk_pl3_status integer,
    fk_pl4 integer,
    fk_pl4_status integer,
    fk_pl5_status integer,
    fk_pl1_status integer,
    fk_pl2_status integer
);


ALTER TABLE activity.tloss OWNER TO activity;

--
-- Name: tloss_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tloss_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tloss_pk_seq OWNER TO activity;

--
-- Name: tloss_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tloss_pk_seq OWNED BY tloss.pk;


--
-- Name: tloss_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tloss_pk_seq', 7729, true);


--
-- Name: tlossdays; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tlossdays (
    event_id integer NOT NULL,
    days integer,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tlossdays OWNER TO activity;

--
-- Name: tlossstatus; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tlossstatus (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tlossstatus OWNER TO activity;

--
-- Name: tlossstatus_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tlossstatus_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tlossstatus_pk_seq OWNER TO activity;

--
-- Name: tlossstatus_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tlossstatus_pk_seq OWNED BY tlossstatus.pk;


--
-- Name: tlossstatus_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tlossstatus_pk_seq', 1, false);


--
-- Name: tlosstype; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tlosstype (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tlosstype OWNER TO activity;

--
-- Name: tlosstype_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tlosstype_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tlosstype_pk_seq OWNER TO activity;

--
-- Name: tlosstype_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tlosstype_pk_seq OWNED BY tlosstype.pk;


--
-- Name: tlosstype_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tlosstype_pk_seq', 1, false);


--
-- Name: tovertime; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tovertime (
    pk integer NOT NULL,
    fk_resource integer NOT NULL,
    fk_project integer,
    fk_overtimestatus integer NOT NULL,
    date timestamp without time zone,
    hours numeric(10,2),
    decided_by integer NOT NULL,
    year integer,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tovertime OWNER TO activity;

--
-- Name: tovertime_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tovertime_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tovertime_pk_seq OWNER TO activity;

--
-- Name: tovertime_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tovertime_pk_seq OWNED BY tovertime.pk;


--
-- Name: tovertime_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tovertime_pk_seq', 8394, true);


--
-- Name: tovertimestatus; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tovertimestatus (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tovertimestatus OWNER TO activity;

--
-- Name: tovertimestatus_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tovertimestatus_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tovertimestatus_pk_seq OWNER TO activity;

--
-- Name: tovertimestatus_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tovertimestatus_pk_seq OWNED BY tovertimestatus.pk;


--
-- Name: tovertimestatus_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tovertimestatus_pk_seq', 1, false);


--
-- Name: tpos_res_mapping; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpos_res_mapping (
    pk integer NOT NULL,
    fk_position integer NOT NULL,
    fk_resource integer NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    percent integer,
    fk_status integer DEFAULT 0 NOT NULL,
    startdate timestamp without time zone,
    enddate timestamp without time zone
);


ALTER TABLE activity.tpos_res_mapping OWNER TO activity;

--
-- Name: tpos_res_mapping_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tpos_res_mapping_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tpos_res_mapping_pk_seq OWNER TO activity;

--
-- Name: tpos_res_mapping_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tpos_res_mapping_pk_seq OWNED BY tpos_res_mapping.pk;


--
-- Name: tpos_res_mapping_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tpos_res_mapping_pk_seq', 12212, true);


--
-- Name: tpos_res_status; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpos_res_status (
    pk integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tpos_res_status OWNER TO activity;

--
-- Name: tpos_res_status_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tpos_res_status_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tpos_res_status_pk_seq OWNER TO activity;

--
-- Name: tpos_res_status_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tpos_res_status_pk_seq OWNED BY tpos_res_status.pk;


--
-- Name: tpos_res_status_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tpos_res_status_pk_seq', 1, false);


--
-- Name: tposition; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tposition (
    pk integer NOT NULL,
    fk_job integer NOT NULL,
    fk_positionstatus integer NOT NULL,
    name character varying(255),
    fixedprice numeric(10,2),
    hourlyrate numeric(10,2),
    costs numeric(10,2),
    income numeric(10,2),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    expectedwork numeric(10,2),
    realwork numeric(10,2),
    communicatedwork numeric(10,2),
    positionstartdate timestamp without time zone,
    percentdone numeric(10,2),
    positionenddate timestamp without time zone,
    evolvisurl character varying(255),
    evolvisprojektid integer
);


ALTER TABLE activity.tposition OWNER TO activity;

--
-- Name: COLUMN tposition.positionstartdate; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tposition.positionstartdate IS 'the position start date';


--
-- Name: COLUMN tposition.percentdone; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tposition.percentdone IS 'Shows how much percent of this position is done.';


--
-- Name: tposition_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tposition_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tposition_pk_seq OWNER TO activity;

--
-- Name: tposition_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tposition_pk_seq OWNED BY tposition.pk;


--
-- Name: tposition_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tposition_pk_seq', 4883, true);


--
-- Name: tpositionstatus; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tpositionstatus (
    pk integer NOT NULL,
    name character varying(255) NOT NULL,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tpositionstatus OWNER TO activity;

--
-- Name: tpositionstatus_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tpositionstatus_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tpositionstatus_pk_seq OWNER TO activity;

--
-- Name: tpositionstatus_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tpositionstatus_pk_seq OWNED BY tpositionstatus.pk;


--
-- Name: tpositionstatus_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tpositionstatus_pk_seq', 1, false);


--
-- Name: tproject; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tproject (
    pk integer NOT NULL,
    fk_customer integer NOT NULL,
    name character varying(255),
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    dayrate numeric(10,2),
    payment_target timestamp without time zone,
    wiki character varying(255),
    test_server character varying(255),
    sprint_backlog character varying(255),
    product_backlog character varying(255),
    specification character varying(255),
    source_control character varying(255),
    protocolls character varying(255),
    project_site character varying(255),
    project_blog character varying(255),
    project_plan character varying(255),
    mailinglists character varying(255),
    issue_tracking character varying(255),
    dms character varying(255),
    offer character varying(255),
    contact_person character varying(255),
    fk_resource integer,
    enddate date,
    startdate date,
    page_auftrag character varying(255) DEFAULT ''::character varying,
    test_protokoll character varying(255) DEFAULT ''::character varying,
    process_wiki character varying(255) DEFAULT ''::character varying,
    accounts integer
);


ALTER TABLE activity.tproject OWNER TO activity;

--
-- Name: tproject_fields_mapping; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tproject_fields_mapping (
    pk integer NOT NULL,
    fk_project integer NOT NULL,
    fk_projectfield integer NOT NULL,
    fieldcontent character varying(255),
    sort integer,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tproject_fields_mapping OWNER TO activity;

--
-- Name: tproject_fields_mapping_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tproject_fields_mapping_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tproject_fields_mapping_pk_seq OWNER TO activity;

--
-- Name: tproject_fields_mapping_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tproject_fields_mapping_pk_seq OWNED BY tproject_fields_mapping.pk;


--
-- Name: tproject_fields_mapping_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tproject_fields_mapping_pk_seq', 43, true);


--
-- Name: tproject_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tproject_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tproject_pk_seq OWNER TO activity;

--
-- Name: tproject_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tproject_pk_seq OWNED BY tproject.pk;


--
-- Name: tproject_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tproject_pk_seq', 285, true);


--
-- Name: tprojectfields; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tprojectfields (
    pk integer NOT NULL,
    fieldname character varying(255),
    basefield integer DEFAULT 0 NOT NULL,
    icon character varying(255),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tprojectfields OWNER TO activity;

--
-- Name: tprojectfields_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tprojectfields_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tprojectfields_pk_seq OWNER TO activity;

--
-- Name: tprojectfields_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tprojectfields_pk_seq OWNED BY tprojectfields.pk;


--
-- Name: tprojectfields_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tprojectfields_pk_seq', 22, true);


--
-- Name: tprojectinvoice; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tprojectinvoice (
    pk integer NOT NULL,
    fk_project integer NOT NULL,
    name character varying(255),
    nr character varying(255),
    amount numeric(10,2) NOT NULL,
    invoiced timestamp without time zone,
    payed timestamp without time zone,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tprojectinvoice OWNER TO activity;

--
-- Name: tprojectinvoice_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tprojectinvoice_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tprojectinvoice_pk_seq OWNER TO activity;

--
-- Name: tprojectinvoice_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tprojectinvoice_pk_seq OWNED BY tprojectinvoice.pk;


--
-- Name: tprojectinvoice_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tprojectinvoice_pk_seq', 35, true);


--
-- Name: tprojectsites; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tprojectsites (
    pk integer NOT NULL,
    sitename character varying(255),
    sitecontent text,
    fk_project integer NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tprojectsites OWNER TO activity;

--
-- Name: tprojectsites_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tprojectsites_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tprojectsites_pk_seq OWNER TO activity;

--
-- Name: tprojectsites_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tprojectsites_pk_seq OWNED BY tprojectsites.pk;


--
-- Name: tprojectsites_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tprojectsites_pk_seq', 4, true);


--
-- Name: treports; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE treports (
    pk integer NOT NULL,
    name text NOT NULL,
    permission integer NOT NULL,
    note text,
    fk_resource integer NOT NULL,
    data text NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.treports OWNER TO activity;

--
-- Name: treports_parameter; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE treports_parameter (
    pk integer NOT NULL,
    fk_reports integer NOT NULL,
    key text,
    value text,
    type text,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.treports_parameter OWNER TO activity;

--
-- Name: treports_parameter_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE treports_parameter_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.treports_parameter_pk_seq OWNER TO activity;

--
-- Name: treports_parameter_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE treports_parameter_pk_seq OWNED BY treports_parameter.pk;


--
-- Name: treports_parameter_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('treports_parameter_pk_seq', 1, false);


--
-- Name: treports_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE treports_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.treports_pk_seq OWNER TO activity;

--
-- Name: treports_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE treports_pk_seq OWNED BY treports.pk;


--
-- Name: treports_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('treports_pk_seq', 1, false);


--
-- Name: treports_ref; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE treports_ref (
    pk integer NOT NULL,
    name text NOT NULL,
    fk_report integer NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    used integer
);


ALTER TABLE activity.treports_ref OWNER TO activity;

--
-- Name: treports_ref_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE treports_ref_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.treports_ref_pk_seq OWNER TO activity;

--
-- Name: treports_ref_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE treports_ref_pk_seq OWNED BY treports_ref.pk;


--
-- Name: treports_ref_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('treports_ref_pk_seq', 1, false);


--
-- Name: tres_ftype_mapping; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tres_ftype_mapping (
    pk integer NOT NULL,
    fk_resource integer NOT NULL,
    fk_functiontype integer NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tres_ftype_mapping OWNER TO activity;

--
-- Name: tres_ftype_mapping_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tres_ftype_mapping_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tres_ftype_mapping_pk_seq OWNER TO activity;

--
-- Name: tres_ftype_mapping_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tres_ftype_mapping_pk_seq OWNED BY tres_ftype_mapping.pk;


--
-- Name: tres_ftype_mapping_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tres_ftype_mapping_pk_seq', 187, true);


--
-- Name: tresource; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tresource (
    pk integer NOT NULL,
    fk_resourcetype integer DEFAULT 1 NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    birth timestamp without time zone,
    entered timestamp without time zone,
    exit timestamp without time zone,
    note character varying(1000),
    username character varying(10),
    passwd character varying(32),
    rights integer,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone,
    mail character varying(255),
    salery numeric(10,2),
    costperhour numeric(10,2),
    fk_tcid integer,
    available_hours numeric(10,2),
    active character(1),
    remain_holiday real DEFAULT 0.0,
    holiday real DEFAULT 0.0,
    fk_functiontype integer,
    fk_employment integer,
    overtime_hours numeric(10,2) DEFAULT 0,
    fk_branchoffice integer
);


ALTER TABLE activity.tresource OWNER TO activity;

--
-- Name: COLUMN tresource.available_hours; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tresource.available_hours IS 'available hours per month';


--
-- Name: COLUMN tresource.fk_functiontype; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tresource.fk_functiontype IS 'fk to table tfunctiontype';


--
-- Name: COLUMN tresource.overtime_hours; Type: COMMENT; Schema: activity; Owner: activity
--

COMMENT ON COLUMN tresource.overtime_hours IS 'Ueberstundenkonto';


--
-- Name: tresource_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tresource_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tresource_pk_seq OWNER TO activity;

--
-- Name: tresource_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tresource_pk_seq OWNED BY tresource.pk;


--
-- Name: tresource_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tresource_pk_seq', 428, true);


--
-- Name: tresourcetype; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tresourcetype (
    pk integer NOT NULL,
    name character varying(255) NOT NULL,
    note character varying(1000),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tresourcetype OWNER TO activity;

--
-- Name: tresourcetype_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tresourcetype_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tresourcetype_pk_seq OWNER TO activity;

--
-- Name: tresourcetype_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tresourcetype_pk_seq OWNED BY tresourcetype.pk;


--
-- Name: tresourcetype_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tresourcetype_pk_seq', 1, false);


--
-- Name: tsettings; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tsettings (
    pk integer NOT NULL,
    key character varying(255),
    value character varying(255),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tsettings OWNER TO activity;

--
-- Name: tsettings_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tsettings_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tsettings_pk_seq OWNER TO activity;

--
-- Name: tsettings_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tsettings_pk_seq OWNED BY tsettings.pk;


--
-- Name: tsettings_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tsettings_pk_seq', 4, true);


--
-- Name: tskills; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tskills (
    pk integer NOT NULL,
    fk_resource integer NOT NULL,
    fk_skills_def integer NOT NULL,
    value integer NOT NULL,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tskills OWNER TO activity;

--
-- Name: tskills_def; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE tskills_def (
    pk integer NOT NULL,
    name text,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.tskills_def OWNER TO activity;

--
-- Name: tskills_def_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tskills_def_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tskills_def_pk_seq OWNER TO activity;

--
-- Name: tskills_def_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tskills_def_pk_seq OWNED BY tskills_def.pk;


--
-- Name: tskills_def_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tskills_def_pk_seq', 2, true);


--
-- Name: tskills_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE tskills_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.tskills_pk_seq OWNER TO activity;

--
-- Name: tskills_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE tskills_pk_seq OWNED BY tskills.pk;


--
-- Name: tskills_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('tskills_pk_seq', 4, true);


--
-- Name: ttimer; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE ttimer (
    pk integer NOT NULL,
    fk_resource integer NOT NULL,
    fk_position integer,
    "time" timestamp without time zone,
    time_diff timestamp without time zone,
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.ttimer OWNER TO activity;

--
-- Name: ttimer_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE ttimer_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.ttimer_pk_seq OWNER TO activity;

--
-- Name: ttimer_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE ttimer_pk_seq OWNED BY ttimer.pk;


--
-- Name: ttimer_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('ttimer_pk_seq', 319, true);


--
-- Name: ttooltip; Type: TABLE; Schema: activity; Owner: activity; Tablespace:
--

CREATE TABLE ttooltip (
    pk integer NOT NULL,
    page character varying(255),
    html_id character varying(255),
    help_text character varying(255),
    cr_user character varying(255),
    cr_date timestamp without time zone,
    upd_user character varying(255),
    upd_date timestamp without time zone
);


ALTER TABLE activity.ttooltip OWNER TO activity;

--
-- Name: ttooltip_pk_seq; Type: SEQUENCE; Schema: activity; Owner: activity
--

CREATE SEQUENCE ttooltip_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE activity.ttooltip_pk_seq OWNER TO activity;

--
-- Name: ttooltip_pk_seq; Type: SEQUENCE OWNED BY; Schema: activity; Owner: activity
--

ALTER SEQUENCE ttooltip_pk_seq OWNED BY ttooltip.pk;


--
-- Name: ttooltip_pk_seq; Type: SEQUENCE SET; Schema: activity; Owner: activity
--

SELECT pg_catalog.setval('ttooltip_pk_seq', 1, false);


--
-- Name: vactivities_for_project; Type: VIEW; Schema: activity; Owner: activity
--

CREATE VIEW vactivities_for_project AS
    SELECT tactivity.pk, tresource.firstname AS resource_firstname, tresource.lastname AS resource_lastname, tposition.pk AS position_pk, tposition.name AS position_name, tjob.pk AS job_pk, tjob.name AS job_name, tproject.pk AS project_pk, tproject.name AS project_name, tactivity.name AS "desc", tactivity.date, tactivity.hours FROM ((((tactivity JOIN tresource ON ((tresource.pk = tactivity.fk_resource))) JOIN tposition ON ((tposition.pk = tactivity.fk_position))) JOIN tjob ON ((tjob.pk = tposition.fk_job))) JOIN tproject ON ((tproject.pk = tjob.fk_project)));


ALTER TABLE activity.vactivities_for_project OWNER TO activity;

--
-- Name: vres_active_project_mapping; Type: VIEW; Schema: activity; Owner: activity
--

CREATE VIEW vres_active_project_mapping AS
    SELECT tresource.pk AS fk_resource, tproject.pk AS fk_project, tpos_res_mapping.fk_status FROM ((((tresource LEFT JOIN tpos_res_mapping ON ((tresource.pk = tpos_res_mapping.fk_resource))) JOIN tposition ON ((tpos_res_mapping.fk_position = tposition.pk))) JOIN tjob ON (((tposition.fk_job = tjob.pk) AND ((tjob.fk_jobstatus = 1) OR (tjob.fk_jobstatus = 2))))) JOIN tproject ON ((tjob.fk_project = tproject.pk))) GROUP BY tresource.pk, tproject.pk, tpos_res_mapping.fk_status;


ALTER TABLE activity.vres_active_project_mapping OWNER TO activity;

--
-- Name: vres_assignment; Type: VIEW; Schema: activity; Owner: activity
--

CREATE VIEW vres_assignment AS
    SELECT tresource.pk AS resource_pk, tresource.lastname AS name, tresource.firstname, btrim(concat(((tfunctiontype.name)::text || ', '::text)), ', '::text) AS function, temployment.name AS employment FROM (((tresource LEFT JOIN tres_ftype_mapping ON ((tresource.pk = tres_ftype_mapping.fk_resource))) LEFT JOIN tfunctiontype ON ((tres_ftype_mapping.fk_functiontype = tfunctiontype.pk))) LEFT JOIN temployment ON ((tresource.fk_employment = temployment.pk))) GROUP BY tresource.pk, tresource.lastname, tresource.firstname, temployment.name;

ALTER TABLE activity.vres_assignment OWNER TO activity;

--
-- Name: vres_project_mapping; Type: VIEW; Schema: activity; Owner: activity
--

CREATE VIEW vres_project_mapping AS
    SELECT tresource.pk AS fk_resource, tproject.pk AS fk_project, tpos_res_mapping.fk_status FROM ((((tresource LEFT JOIN tpos_res_mapping ON ((tresource.pk = tpos_res_mapping.fk_resource))) JOIN tposition ON ((tpos_res_mapping.fk_position = tposition.pk))) JOIN tjob ON ((tposition.fk_job = tjob.pk))) JOIN tproject ON ((tjob.fk_project = tproject.pk))) GROUP BY tresource.pk, tproject.pk, tpos_res_mapping.fk_status;


ALTER TABLE activity.vres_project_mapping OWNER TO activity;

--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tactivity ALTER COLUMN pk SET DEFAULT nextval('tactivity_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY taract_info ALTER COLUMN pk SET DEFAULT nextval('taract_info_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tbranchoffice ALTER COLUMN pk SET DEFAULT nextval('tbranchoffice_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcost ALTER COLUMN pk SET DEFAULT nextval('tcost_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcosttype ALTER COLUMN pk SET DEFAULT nextval('tcosttype_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tcustomer ALTER COLUMN pk SET DEFAULT nextval('tcustomer_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tdelete_request ALTER COLUMN pk SET DEFAULT nextval('tdelete_request_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY temployment ALTER COLUMN pk SET DEFAULT nextval('temployment_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tfunctiontype ALTER COLUMN pk SET DEFAULT nextval('tfunctiontype_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tinvoice ALTER COLUMN pk SET DEFAULT nextval('tinvoice_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjob ALTER COLUMN pk SET DEFAULT nextval('tjob_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjobstatus ALTER COLUMN pk SET DEFAULT nextval('tjobstatus_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tjobtype ALTER COLUMN pk SET DEFAULT nextval('tjobtype_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tloss ALTER COLUMN pk SET DEFAULT nextval('tloss_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tlossstatus ALTER COLUMN pk SET DEFAULT nextval('tlossstatus_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tlosstype ALTER COLUMN pk SET DEFAULT nextval('tlosstype_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertime ALTER COLUMN pk SET DEFAULT nextval('tovertime_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tovertimestatus ALTER COLUMN pk SET DEFAULT nextval('tovertimestatus_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_mapping ALTER COLUMN pk SET DEFAULT nextval('tpos_res_mapping_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpos_res_status ALTER COLUMN pk SET DEFAULT nextval('tpos_res_status_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tposition ALTER COLUMN pk SET DEFAULT nextval('tposition_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tpositionstatus ALTER COLUMN pk SET DEFAULT nextval('tpositionstatus_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject ALTER COLUMN pk SET DEFAULT nextval('tproject_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tproject_fields_mapping ALTER COLUMN pk SET DEFAULT nextval('tproject_fields_mapping_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectfields ALTER COLUMN pk SET DEFAULT nextval('tprojectfields_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectinvoice ALTER COLUMN pk SET DEFAULT nextval('tprojectinvoice_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tprojectsites ALTER COLUMN pk SET DEFAULT nextval('tprojectsites_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY treports ALTER COLUMN pk SET DEFAULT nextval('treports_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY treports_parameter ALTER COLUMN pk SET DEFAULT nextval('treports_parameter_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY treports_ref ALTER COLUMN pk SET DEFAULT nextval('treports_ref_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tres_ftype_mapping ALTER COLUMN pk SET DEFAULT nextval('tres_ftype_mapping_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresource ALTER COLUMN pk SET DEFAULT nextval('tresource_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tresourcetype ALTER COLUMN pk SET DEFAULT nextval('tresourcetype_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tsettings ALTER COLUMN pk SET DEFAULT nextval('tsettings_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tskills ALTER COLUMN pk SET DEFAULT nextval('tskills_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY tskills_def ALTER COLUMN pk SET DEFAULT nextval('tskills_def_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY ttimer ALTER COLUMN pk SET DEFAULT nextval('ttimer_pk_seq'::regclass);


--
-- Name: pk; Type: DEFAULT; Schema: activity; Owner: activity
--

ALTER TABLE ONLY ttooltip ALTER COLUMN pk SET DEFAULT nextval('ttooltip_pk_seq'::regclass);

COMMIT;
