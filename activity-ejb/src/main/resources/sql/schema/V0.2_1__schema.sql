-- CHANGES GO HERE
BEGIN;
create table activity.troles_tresources_AUD (
	REV int4 not null,
    roles_pk int8 not null,
    resources_pk int8 not null,
    REVTYPE int2,
    primary key (REV, roles_pk, resources_pk)
);

-- remove unused column
alter table tovertime drop column decided_by;
alter table tovertime_aud drop column decided_by;
COMMIT;
