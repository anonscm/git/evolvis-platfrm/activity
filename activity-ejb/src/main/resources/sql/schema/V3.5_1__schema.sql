UPDATE activity.tresource SET remain_holiday=0 WHERE remain_holiday IS NULL;
ALTER TABLE activity.tresource ALTER COLUMN remain_holiday SET NOT NULL;
