-- Added 3 permissions for the role "Projektleiter"
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Resource.APPEND_ALL';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Resource.DETACH_ALL';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Resource.VIEW_ALL';
