-- New Permissions for PL
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.VIEW_ALL';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.VIEW_ALL_DETAIL';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.ADD_ALL';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.EDIT_ALL';
