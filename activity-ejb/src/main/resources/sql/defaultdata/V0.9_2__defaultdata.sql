-- New Permissions for PL
insert into activity.tpermission select nextval('activity.tpermission_pk_seq'), 'Cost.VIEW_BY_PROJECT', 'Eigenes Projekt -> Kosten sehen';
insert into activity.tpermission select nextval('activity.tpermission_pk_seq'), 'Cost.EDIT_BY_PROJECT', 'Eigenes Projekt -> Kosten bearbeiten';

insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.VIEW_BY_PROJECT';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Cost.EDIT_BY_PROJECT';
