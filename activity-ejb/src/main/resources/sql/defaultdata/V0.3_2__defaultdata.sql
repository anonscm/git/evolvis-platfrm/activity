-- remove permisison to view all resources from projektleiter, architekt and bereichsleiter role
delete from activity.troles_tpermissions where roles_pk = 3 and permissions_pk in (select pk from tpermission p where p.actionId = 'Resource.VIEW_ALL');
delete from activity.troles_tpermissions where roles_pk = 4 and permissions_pk in (select pk from tpermission p where p.actionId = 'Resource.VIEW_ALL');
delete from activity.troles_tpermissions where roles_pk = 6 and permissions_pk in (select pk from tpermission p where p.actionId = 'Resource.VIEW_ALL');
