-- fix typos

UPDATE activity.tjobstatus
SET note='Der Auftrag befindet sich in der Angebotsphase.',
cr_user='admin', cr_date=now() WHERE pk=1;

UPDATE activity.tjobtype
SET note='Abrechenbarer Auftrag',
cr_user='admin', cr_date=now() WHERE pk=1;

UPDATE activity.tjobtype
SET note='nicht abrechenbare Leistungen',
cr_user='admin', cr_date=now() WHERE pk=3;

UPDATE activity.tcosttype
SET note='Kosten, die durch Reisetätigkeit entstehen',
cr_user='admin', cr_date=now() WHERE pk=1;

UPDATE activity.tcosttype
SET note='alle sonstigen dem Auftrag zurechenbaren Kosten',
cr_user='admin', cr_date=now() WHERE pk=2;
