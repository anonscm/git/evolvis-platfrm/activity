-- Added a new value 'Portfolioprojekt' and renamed 'BodyLeasing' to 'Personaldienstleistung'
-- Also added a stored procedure to determine the internal or external nature of project.
BEGIN;
CREATE OR REPLACE FUNCTION isProjectExternal(projectId bigint) RETURNS integer AS $$
DECLARE
        mviews RECORD;
        returnValue integer;
BEGIN
        returnValue := 0;
        FOR mviews IN SELECT * FROM activity.tjob where activity.tjob.fk_project = $1 LOOP
                IF mviews.fk_jobtype IN (1,2,4,5) THEN
                        returnValue := 1;
                END IF;
        END LOOP;

        RETURN returnValue;

END;
$$ LANGUAGE plpgsql;

update activity.tjobtype SET name='Personaldienstleistung' where name='BodyLeasing';
INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (nextval('activity.tjobtype_pk_seq'), 'Portfolioprojekt', 'Portfolioprojekt', 'admin', now());
COMMIT;
