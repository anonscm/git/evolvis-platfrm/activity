----------------------------------------------------------------------------------------------
-- Tabelle activity.tpos_res_status
UPDATE activity.tpos_res_status
SET name='inaktiv', cr_user='admin', cr_date=now()
WHERE pk=0;

----------------------------------------------------------------------------------------------
--- Anstellungsverhältnisse
UPDATE activity.temployment
SET cr_user='admin', cr_date=now();

----------------------------------------------------------------------------------------------
-- Geschäftsstellen
UPDATE activity.tbranchoffice
SET cr_user='admin', cr_date=now();

----------------------------------------------------------------------------------------------
-- Tabelle activity.tfunctiontype
INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (1, 'Admin', '2007-08-03 00:00:00');

INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (2, 'Studi', '2007-08-03 00:00:00');

INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (3, 'QS', '2007-08-03 00:00:00');

INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (4, 'Entwickler', '2007-08-03 00:00:00');

INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (5, 'Buchhaltung', '2007-08-03 00:00:00');

INSERT INTO activity.tfunctiontype (pk, "name", cr_date)
VALUES (6, 'Boss', '2007-08-03 00:00:00');

----------------------------------------------------------------------------------------------
-- Tabelle activity.tprojectfields
INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (0, 'Projektverantwortlicher', 1, 'proto-person.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (1, 'Ansprechpartner (Kunde)', 1, 'person.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (2, 'Projektplan', 1, 'label_meeting.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (3, 'Projektseite', 1, 'internet-news-reader.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (4, 'Wiki', 1, 'wiki.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (5, 'Issue Tracking', 1, 'tutorials16.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (6, 'Mailinglisten', 1, 'mailing_list.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (7, 'Projektblog', 1, 'x-office-document.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (8, 'Quellcodeverwaltung', 1, 'text-x-generic.png', 'admin', now());

INSERT INTO activity.tprojectfields (pk, fieldname, basefield, icon, cr_user, cr_date)
VALUES (9, 'Testserver', 1, 'network-server.png', 'admin', now());

---------------
-- Icons
UPDATE activity.tprojectfields SET icon='proto-person.png'
WHERE activity.tprojectfields.fieldname='Projektverantwortlicher';

UPDATE activity.tprojectfields SET icon='person.png'
WHERE activity.tprojectfields.fieldname='Ansprechpartner (Kunde)';

UPDATE activity.tprojectfields SET icon='label_meeting.png'
WHERE activity.tprojectfields.fieldname='Projektplan';

UPDATE activity.tprojectfields SET icon='internet-news-reader.png'
WHERE activity.tprojectfields.fieldname='Projektseite';

UPDATE activity.tprojectfields SET icon='wiki.png'
WHERE activity.tprojectfields.fieldname='Wiki';

UPDATE activity.tprojectfields SET icon='tutorials16.png'
WHERE activity.tprojectfields.fieldname='Issue Tracking';

UPDATE activity.tprojectfields SET icon='mailing_list.png'
WHERE activity.tprojectfields.fieldname='Mailinglisten';

UPDATE activity.tprojectfields SET icon='x-office-document.png'
WHERE activity.tprojectfields.fieldname='Projektblog';

UPDATE activity.tprojectfields SET icon='text-x-generic.png'
WHERE activity.tprojectfields.fieldname='Quellcodeverwaltung';

UPDATE activity.tprojectfields SET icon='network-server.png'
WHERE activity.tprojectfields.fieldname='Testserver';

----------------------------------------------------------------------------------------------
-- Tabelle activity.tjobtype
INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (4, 'Wartungsvertrag', 'Wartungsvertrag', 'admin', now());

INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (5, 'Gewährleistung', 'Gewährleistung', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tcosttype
INSERT INTO activity.tcosttype (pk, name, note, cr_user, cr_date)
VALUES (3, 'Kosten aus Buchhaltung', 'Export der angefallenen Kosten', 'admin', now());
