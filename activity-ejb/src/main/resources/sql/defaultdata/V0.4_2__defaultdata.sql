-- remove permisison to import LDAP from Controller role
delete from activity.troles_tpermissions where roles_pk = 2 and permissions_pk in (select pk from tpermission p where p.actionId = 'Admin.IMPORT_LDAP');

-- Bugfix in the description.
update activity.tpermission set description = 'Bekommt Ressourcen relevante Emails' where actionid = 'Resource.CONTROLLER';
