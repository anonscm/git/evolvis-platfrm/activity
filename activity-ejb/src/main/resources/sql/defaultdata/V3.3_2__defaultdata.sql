-- Umbenennung Ressource zu Mitarbeiter --
update activity.tpermission set description = 'Mitarbeiter sehen' where actionid = 'Resource.VIEW_ALL';
update activity.tpermission set description = 'Mitarbeiter anlegen' where actionid = 'Resource.ADD_ALL';
update activity.tpermission set description = 'Mitarbeiter bearbeiten' where actionid = 'Resource.EDIT_ALL';
update activity.tpermission set description = 'Mitarbeiter löschen' where actionid = 'Resource.DELETE_ALL';
update activity.tpermission set description = 'Mitarbeiter sperren' where actionid = 'Resource.LOCK_ALL';
update activity.tpermission set description = 'Mitarbeiter zu Positionen hinzufügen' where actionid = 'Resource.APPEND_ALL';
update activity.tpermission set description = 'Mitarbeiter von Positionen entfernen' where actionid = 'Resource.DETACH_ALL';
