----------------------------------------------------------------------------------------------
-- Tabelle activity.tovertimestatus
INSERT INTO activity.tovertimestatus(pk, name, note, cr_user, cr_date)
VALUES (0, 'offen', 'Die Überstunden sind noch offen und wurden nicht ausgeglichen', 'admin', now());

INSERT INTO activity.tovertimestatus(pk, name, note, cr_user, cr_date)
VALUES (1, 'ausgeglichen', 'Überstunden wurden ausgeglichen', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tjobstatus
INSERT INTO activity.tjobstatus (pk, name, note, cr_user, cr_date)
VALUES (1, 'Angebot', 'Der Auftrag befindet sich in der Angbotsphase', 'admin', now());

INSERT INTO activity.tjobstatus (pk, name, note, cr_user, cr_date)
VALUES (2, 'Beauftragt', 'Das Angebot wurde beauftragt.', 'admin', now());

INSERT INTO activity.tjobstatus (pk, name, note, cr_user, cr_date)
VALUES (3, 'Abgelehnt', 'Das Angebot wurde abgelehnt.', 'admin', now());

INSERT INTO activity.tjobstatus (pk, name, note, cr_user, cr_date)
VALUES (4, 'Fertiggestellt', 'Der Auftrag ist fertiggestellt und kann abgerechnet werden.', 'admin', now());

INSERT INTO activity.tjobstatus (pk, name, note, cr_user, cr_date)
VALUES (5, 'Abgerechnet', 'Der Auftrag ist abgerechnet.', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tjobtype
INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (1, 'Kundenauftrag', 'Abrechenbarer Auftrag.', 'admin', now());

INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (2, 'BodyLeasing', 'Dienstleistung auf Tagessatzbasis', 'admin', now());

INSERT INTO activity.tjobtype (pk, name, note, cr_user, cr_date)
VALUES (3, 'Intern', 'nicht Abrechenbare Leistungen', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tpositionstatus
INSERT INTO activity.tpositionstatus (pk, name, note, cr_user, cr_date)
VALUES (1, 'offen', 'Position noch nicht begonnen.', 'admin', now());

INSERT INTO activity.tpositionstatus (pk, name, note, cr_user, cr_date)
VALUES (2, 'in Arbeit', 'Postition in Arbeit', 'admin', now());

INSERT INTO activity.tpositionstatus (pk, name, note, cr_user, cr_date)
VALUES (3, 'fertiggestellt', 'Position fertiggestellt', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tresourcetype
INSERT INTO activity.tresourcetype (pk, name, note, cr_user, cr_date)
VALUES (1, 'int. Person', 'Festangestellter Mitarbeiter', 'admin', now());

INSERT INTO activity.tresourcetype (pk, name, note, cr_user, cr_date)
VALUES (2, 'ext. Person', 'Externer Mitarbeiter', 'admin', now());

INSERT INTO activity.tresourcetype (pk, name, note, cr_user, cr_date)
VALUES (3, 'Kostenstelle', 'sonstige zurechenbare Kosten', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tcosttype
INSERT INTO activity.tcosttype (pk, name, note, cr_user, cr_date)
VALUES (1, 'Reisekosten', 'Kosten die durch Reisetätigkeit eintstehen', 'admin', now());

INSERT INTO activity.tcosttype (pk, name, note, cr_user, cr_date)
VALUES (2, 'sonstige Kosten', 'alle Sonstigen dem Auftrag zurechenbaren Kosten', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tlosstype
INSERT INTO activity.tlosstype (pk, name, note, cr_user, cr_date)
VALUES (1, 'Urlaub', 'Urlaubsstag', 'admin', now());

INSERT INTO activity.tlosstype (pk, name, note, cr_user, cr_date)
VALUES (2, 'Krankheit', 'Krankheitstag', 'admin', now());

INSERT INTO activity.tlosstype (pk, name, note, cr_user, cr_date)
VALUES (3, 'Abwesendheit', 'Nicht anwesend', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tlossstatus
INSERT INTO activity.tlossstatus (pk, name, note, cr_user, cr_date)
VALUES (1, 'beantragt', '', 'admin', now());

INSERT INTO activity.tlossstatus (pk, name, note, cr_user, cr_date)
VALUES (2, 'genehmigt', '', 'admin', now());

INSERT INTO activity.tlossstatus (pk, name, note, cr_user, cr_date)
VALUES (3, 'abgelehnt', '', 'admin', now());

INSERT INTO activity.tlossstatus (pk, name, note, cr_user, cr_date)
VALUES (4, 'gebucht', '', 'admin', now());

----------------------------------------------------------------------------------------------
-- Tabelle activity.tpos_res_status
INSERT INTO activity.tpos_res_status(pk, name, note, cr_user, cr_date)
VALUES (0, 'zugeordnet', '', 'admin', now());

INSERT INTO activity.tpos_res_status(pk, name, note, cr_user, cr_date)
VALUES (1, 'aktiv', '', 'admin', now());

INSERT INTO activity.tpos_res_status(pk, name, note, cr_user, cr_date)
VALUES (2, 'geplant', '', 'admin', now());

----------------------------------------------------------------------------------------------
--- Anstellungsverhältnisse
INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (1, 'angestellt', NULL, 'admin', now());

INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (2, 'Mini-Job', NULL, 'admin', now());

INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (3, 'Azubi', NULL, 'admin', now());

INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (4, 'Studi', NULL, 'admin', now());

INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (5, 'Extern', NULL, 'admin', now());

INSERT INTO activity.temployment (pk, name, note, cr_user, cr_date)
VALUES (6, 'kein Rahmenvertrag', NULL, 'admin', now());

----------------------------------------------------------------------------------------------
-- Geschäftsstellen
INSERT INTO activity.tbranchoffice (pk, name, cr_user, cr_date)
VALUES (1, 'Bonn', 'admin', now());

INSERT INTO activity.tbranchoffice (pk, name, cr_user, cr_date)
VALUES (2, 'Berlin', 'admin', now());
