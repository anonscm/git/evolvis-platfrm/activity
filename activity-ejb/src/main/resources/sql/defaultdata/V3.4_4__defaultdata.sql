-- First change all jobs' status in tjob table from Abgerechnet, abgelehnt --> Feriggestellt
-- Then change all jobs' status in tjob table from Angebot --> Beauftragt
-- Finally remove the three empty statuses ('Abgerechnet', 'Abgelehnt', 'Angebot') from the tjobstatus table
BEGIN;

update activity.tjob SET fk_jobstatus=(select pk from activity.tjobstatus where name='Fertiggestellt')
where fk_jobstatus=(select pk from activity.tjobstatus where name='Abgerechnet');
update activity.tjob SET fk_jobstatus=(select pk from activity.tjobstatus where name='Fertiggestellt')
where fk_jobstatus=(select pk from activity.tjobstatus where name='Abgelehnt');
update activity.tjob SET fk_jobstatus=(select pk from activity.tjobstatus where name='Beauftragt')
where fk_jobstatus=(select pk from activity.tjobstatus where name='Angebot');

delete from activity.tjobstatus where name IN ('Abgerechnet', 'Abgelehnt', 'Angebot');

COMMIT;
