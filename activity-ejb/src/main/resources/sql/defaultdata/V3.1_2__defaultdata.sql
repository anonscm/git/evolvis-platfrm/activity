-- Delete for Bug [#4284] remove 2 permissions from role 'Projektleiter'
delete from activity.troles_tpermissions where roles_pk = 3 and permissions_pk in (select pk from tpermission p where p.actionId = 'Resource.APPEND_ALL');
delete from activity.troles_tpermissions where roles_pk = 3 and permissions_pk in (select pk from tpermission p where p.actionId = 'Resource.DETACH_ALL');
