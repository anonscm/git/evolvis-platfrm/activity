-- CHANGES GO HERE

-- Add new permissions for project-manager related stuff
insert into activity.tpermission select nextval('activity.tpermission_pk_seq'), 'Project.VIEW_MANAGER', 'Eigene Projekte sehen (als Manager)';
insert into activity.tpermission select nextval('activity.tpermission_pk_seq'), 'Job.VIEW_MANAGER', 'Eigene Aufträge sehen (als Manager)';
insert into activity.tpermission select nextval('activity.tpermission_pk_seq'), 'Position.VIEW_MANAGER', 'Eigene Positionen sehen (als Manager)';

-- Add some missing role <> permission assignments
-- Role: Controller
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Skill.DELETE';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Cost.EDIT_ALL';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Project.VIEW_MANAGER';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Job.VIEW_MANAGER';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Position.VIEW_MANAGER';
insert into activity.troles_tpermissions select 2, pk from activity.tpermission where actionid = 'Loss.EXPORT_ALL';

-- Role: Projektleiter
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Skill.DELETE';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Project.VIEW_MANAGER';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Job.VIEW_MANAGER';
insert into activity.troles_tpermissions select 3, pk from activity.tpermission where actionid = 'Position.VIEW_MANAGER';
-- Role: Architekt
insert into activity.troles_tpermissions select 4, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 4, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 4, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 4, pk from activity.tpermission where actionid = 'Skill.DELETE';
-- Role: Admin
insert into activity.troles_tpermissions select 5, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 5, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 5, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 5, pk from activity.tpermission where actionid = 'Skill.DELETE';
-- Role: Bereichsleiter
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Job.VIEW_ALL';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Skill.DELETE';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Project.VIEW_MANAGER';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Job.VIEW_MANAGER';
insert into activity.troles_tpermissions select 6, pk from activity.tpermission where actionid = 'Position.VIEW_MANAGER';
-- Role: Buchhaltung
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Skill.DELETE';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Project.VIEW_MANAGER';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Job.VIEW_MANAGER';
insert into activity.troles_tpermissions select 7, pk from activity.tpermission where actionid = 'Position.VIEW_MANAGER';
-- Role: Personal
insert into activity.troles_tpermissions select 8, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 8, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 8, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 8, pk from activity.tpermission where actionid = 'Skill.DELETE';
insert into activity.troles_tpermissions select 8, pk from activity.tpermission where actionid = 'Skill.VIEW_ALL';
-- Role: Vertrieb
insert into activity.troles_tpermissions select 9, pk from activity.tpermission where actionid = 'Skill.VIEW';
insert into activity.troles_tpermissions select 9, pk from activity.tpermission where actionid = 'Skill.ADD';
insert into activity.troles_tpermissions select 9, pk from activity.tpermission where actionid = 'Skill.EDIT';
insert into activity.troles_tpermissions select 9, pk from activity.tpermission where actionid = 'Skill.DELETE';

-- delete permissions that are inserted by mistake
delete from activity.tpermission where actionid = 'Client.VIEW_ALL';
delete from activity.tpermission where actionid = 'Client.ADD_ALL';
delete from activity.tpermission where actionid = 'Client.EDIT_ALL';
delete from activity.tpermission where actionid = 'Client.DELETE_ALL';
