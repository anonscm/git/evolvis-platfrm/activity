-- mittest activities
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (17, 11, 1, 'mittest leistung fuer position11', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (18, 11, 2, 'mittest leistung fuer position12', '2010-10-11 00:00:00', 4.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (19, 11, 3, 'mittest leistung fuer position21', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (20, 11, 4, 'mittest leistung fuer position22', '2010-10-11 00:00:00', 4.000);

-- contest activities
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (21, 12, 1, 'contest leistung fuer position11', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (22, 12, 2, 'contest leistung fuer position12', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (23, 12, 3, 'contest leistung fuer position21', '2010-10-11 00:00:00', 4.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (24, 12, 4, 'contest leistung fuer position22', '2010-10-11 00:00:00', 8.000);

-- protest activities
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (25, 13, 1, 'protest leistung fuer position11', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (26, 13, 2, 'protest leistung fuer position12', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (27, 13, 3, 'protest leistung fuer position21', '2010-10-11 00:00:00', 8.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (28, 13, 4, 'protest leistung fuer position22', '2010-10-11 00:00:00', 4.000);