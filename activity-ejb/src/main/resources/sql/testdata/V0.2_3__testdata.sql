-- CHANGES GO HERE
-- add some job-invoices for testing
insert into activity.tinvoice (pk,fk_job,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R1',100.3,null,null,'Not invoiced, not payed';
insert into activity.tinvoice (pk,fk_job,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R2',1200.3,now(),null,'invoiced, not payed';
insert into activity.tinvoice (pk,fk_job,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R3',122200.3,null,now(),'Not invoices, but payed';
insert into activity.tinvoice (pk,fk_job,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R3',122200.3,now(),now(),'invoiced, payed';

-- add some project-invoices for testing
insert into activity.tinvoice (pk,fk_project,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R1',100.3,null,null,'Not invoiced, not payed';
insert into activity.tinvoice (pk,fk_project,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R2',1200.3,now(),null,'invoiced, not payed';
insert into activity.tinvoice (pk,fk_project,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R3',122200.3,null,now(),'Not invoices, but payed';
insert into activity.tinvoice (pk,fk_project,name,nr,amount,invoiced,payed,note) select nextval('activity.tinvoice_pk_seq'),1,'Testrechnung','R3',122200.3,now(),now(),'invoiced, payed';

INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (11, 1, 'Mitarbeiter', 'Test', 'mittest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (12, 1, 'Controller', 'Test', 'contest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (13, 1, 'Projektleiter', 'Test', 'protest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (14, 1, 'Architekt', 'Test', 'arctest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (15, 1, 'Admin', 'Test', 'admtest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (16, 1, 'Bereichsleiter', 'Test', 'bertest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (17, 1, 'Buchhaltung', 'Test', 'buctest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (18, 1, 'Personal', 'Test', 'pertest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (19, 1, 'Vertrieb', 'Test', 'vertest', md5('test'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
SELECT setval('activity.tresource_pk_seq', 19);

insert into activity.troles_tresources select pk, 11 from activity.trole where name = 'Mitarbeiter';
insert into activity.troles_tresources select pk, 12 from activity.trole where name = 'Controller';
insert into activity.troles_tresources select pk, 13 from activity.trole where name = 'Projektleiter';
insert into activity.troles_tresources select pk, 14 from activity.trole where name = 'Architekt';
insert into activity.troles_tresources select pk, 15 from activity.trole where name = 'Admin';
insert into activity.troles_tresources select pk, 16 from activity.trole where name = 'Bereichsleiter';
insert into activity.troles_tresources select pk, 17 from activity.trole where name = 'Buchhaltung';
insert into activity.troles_tresources select pk, 18 from activity.trole where name = 'Personal';
insert into activity.troles_tresources select pk, 19 from activity.trole where name = 'Vertrieb';

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (5, 1, 'Projekt Mitarbeiter', 'Projekt Mitarbeiter', 400, current_timestamp - interval '30 days', 11, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (6, 1, 'Projekt Controller', 'Projekt Controller', 400, current_timestamp - interval '30 days', 12, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (7, 1, 'Projekt Projektleiter', 'Projekt Projektleiter', 400, current_timestamp - interval '30 days', 13, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (8, 1, 'Projekt Architekt', 'Projekt Architekt', 400, current_timestamp - interval '30 days', 14, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (9, 1, 'Projekt Admin', 'Projekt Admin', 400, current_timestamp - interval '30 days', 15, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (10, 1, 'Projekt Bereichsleiter', 'Projekt Bereichsleiter', 400, current_timestamp - interval '30 days', 16, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (11, 1, 'Projekt Buchhaltung', 'Projekt Buchhaltung', 400, current_timestamp - interval '30 days', 17, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (12, 1, 'Projekt Personal', 'Projekt Personal', 400, current_timestamp - interval '30 days', 18, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (13, 1, 'Projekt Vertrieb', 'Projekt Vertrieb', 400, current_timestamp - interval '30 days', 19, now());

SELECT setval('activity.tproject_pk_seq', 13);


INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (6, 5, 1, 2, 1, 'Job Mitarbeiter', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (7, 6, 1, 2, 1, 'Job Controller', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (8, 7, 1, 2, 1, 'Job Projektleiter', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (9, 8, 1, 2, 1, 'Job Architekt', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (10, 9, 1, 2, 1, 'Job Admin', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (11, 10, 1, 2, 1, 'Job Bereichsleiter', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (12, 11, 1, 2, 1, 'Job Buchhaltung', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (13, 12, 1, 2, 1, 'Job Personal', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (14, 13, 1, 2, 1, 'Job Vertrieb', 't', 'nr', 'f', now());
SELECT setval('activity.tjob_pk_seq', 14);

INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (10, 6, 2, 'Position Mitarbeiter', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (11, 7, 2, 'Position Controller', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (12, 8, 2, 'Position Projektleiter', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (13, 9, 2, 'Position Architekt', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (14, 10, 2, 'Position Admin', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (15, 11, 2, 'Position Bereichsleiter', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (16, 12, 2, 'Position Buchhaltung', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (17, 13, 2, 'Position Personal', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (18, 14, 2, 'Position Vertrieb', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
SELECT setval('activity.tposition_pk_seq', 18);
