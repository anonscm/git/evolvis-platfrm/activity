-- THIS FILE IS NOW LOCKED AND MUST NOT BE CHANGED
--
-- ANY MODIFICATIONS MUST GO IN A SEPARATE SQL FILE

-- insert some resources
truncate activity.tresource cascade;
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (1, 1, 'activity', 'admin', 'admin', md5('admin'), 't', 45, NULL, NULL);
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (2, 1, 'Timo', 'Pick', 'tpick', md5('tpick'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (3, 1, 'Nils', 'Kollat', 'nkolla', md5('nkolla'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (4, 1, 'Katja', 'Hapke', 'khapke', md5('khapke'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (5, 1, 'Patrick', 'Appel', 'pappel', md5('pappel'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (6, 1, 'Thorsten', 'Glaser', 'tglase', md5('tglase'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (7, 1, 'Thomas', 'Schmitz', 'tschmi', md5('tschmi'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Berlin'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (8, 1, 'Martin', 'Buhren', 'mbuhre', md5('mbuhre'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (9, 1, 'Daniel', 'Enge', 'denge', md5('denge'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
INSERT INTO activity.tresource (pk, fk_resourcetype, firstname, lastname, username, passwd, active, holiday, fk_employment, fk_branchoffice) VALUES (10, 1, 'Uli', 'Gross', 'ugross', md5('ugross'), 't', 45, 1, (select pk from activity.tbranchoffice where name = 'Bonn'));
SELECT setval('activity.tresource_pk_seq', 10);

-- insert some customer
truncate activity.tcustomer cascade;
INSERT INTO activity.tcustomer (pk, name, note, dayrate, payment_target, cr_user, cr_date) VALUES (1, 'ABC Co & KG', 'Testkunde', 200, now(), 'admin', now());
INSERT INTO activity.tcustomer (pk, name, note, dayrate, payment_target, cr_user, cr_date) VALUES (2, 'XYZ AG', 'Testkunde', 300, now(), 'admin', now());
INSERT INTO activity.tcustomer (pk, name, note, dayrate, payment_target, cr_user, cr_date) VALUES (3, 'Die GmbH', 'Testkunde', 100, now(), 'admin', now());
SELECT setval('activity.tcustomer_pk_seq', 3);

DELETE FROM activity.taract_info;
INSERT INTO activity.taract_info (dbversion_string, pk) VALUES ('2.3.0', 1);
INSERT INTO activity.taract_info (dbversion_string, pk) VALUES ('2.3.1', 2);
INSERT INTO activity.taract_info (dbversion_string, pk) VALUES ('2.5', 3);
INSERT INTO activity.taract_info (dbversion_string, pk) VALUES ('2.4', 4);
INSERT INTO activity.taract_info (dbversion_string, pk) VALUES ('3.0', 5);

-- insert roles <> resources assignment
delete from activity.troles_tresources;
-- resource admin gets all roles
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Mitarbeiter';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Controller';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Projektleiter';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Architekt';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Admin';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Bereichsleiter';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Buchhaltung';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Personal';
insert into activity.troles_tresources select pk, 1 from activity.trole where name = 'Vertrieb';
-- all other resources gets each one of roles
insert into activity.troles_tresources select pk, 2 from activity.trole where name = 'Mitarbeiter';
insert into activity.troles_tresources select pk, 3 from activity.trole where name = 'Controller';
insert into activity.troles_tresources select pk, 4 from activity.trole where name = 'Projektleiter';
insert into activity.troles_tresources select pk, 5 from activity.trole where name = 'Architekt';
insert into activity.troles_tresources select pk, 6 from activity.trole where name = 'Admin';
insert into activity.troles_tresources select pk, 7 from activity.trole where name = 'Bereichsleiter';
insert into activity.troles_tresources select pk, 8 from activity.trole where name = 'Buchhaltung';
insert into activity.troles_tresources select pk, 9 from activity.trole where name = 'Personal';
insert into activity.troles_tresources select pk, 10 from activity.trole where name = 'Vertrieb';

--  insert custom permissions
-- user admin
insert into activity.tresources_tpermissions select 1, pk from activity.tpermission where actionid = 'Activity.VIEW';
insert into activity.tresources_tpermissions select 1, pk from activity.tpermission where actionid = 'Customer.EDIT_ALL';
insert into activity.tresources_tpermissions select 1, pk from activity.tpermission where actionid = 'Skill.ADD';
-- user tpick
insert into activity.tresources_tpermissions select 2, pk from activity.tpermission where actionid = 'Activity.VIEW_ALL';
insert into activity.tresources_tpermissions select 2, pk from activity.tpermission where actionid = 'Customer.EDIT_ALL';
insert into activity.tresources_tpermissions select 2, pk from activity.tpermission where actionid = 'Skill.ADD';

--insert holidays
delete from activity.tloss;
INSERT INTO  activity.tloss (pk,fk_lossstatus,fk_losstype,fk_resource,startdate,enddate,year,days,note)
VALUES (1,1,1,2,'2011-02-01 00:00:00.0','2011-02-03 00:00:00.0',2011,0.0, 'holiday description');

SELECT setval('activity.tloss_pk_seq', 1);

--insert projects
delete from activity.tproject;
INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (1, 1, 'Projekt ABC', 'test1 project', 400, current_timestamp + interval '30 days', 1, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (2, 2, 'Projekt XYZ', 'test2 project', 400, current_timestamp + interval '30 days', 1, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (3, 3, 'Projekt 42', 'test3 project', 400, current_timestamp - interval '30 days', 1, now());

INSERT INTO activity.tproject (pk, fk_customer, name, note, dayrate, payment_target, fk_resource, startdate)
VALUES (4, 1, 'Test Projekt', 'test4 project', 400, current_timestamp - interval '30 days', 1, now());

SELECT setval('activity.tproject_pk_seq', 4);

--insert overtime
delete from activity.tovertime;
INSERT INTO activity.tovertime (pk,fk_resource,fk_project,fk_overtimestatus,date,hours,decided_by,year,note)
VALUES (1,2,2,0,'2011-02-01 00:00:00.0',5.00,1,2011,'record description');

INSERT INTO activity.tovertime (pk,fk_resource,fk_project,fk_overtimestatus,date,hours,decided_by,year,note)
VALUES (2,2,1,0,'2011-02-03 00:00:00.0',-4.00,1,2011,'reduct description');

SELECT setval('activity.tovertime_pk_seq', 2);

--insert jobs
delete from activity.tjob;
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (1, 1, 1, 2, 1, 'test1 job1', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (2, 1, 1, 2, 1, 'test1 job2', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (3, 2, 1, 2, 1, 'test2 job1', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (4, 2, 1, 2, 1, 'test2 job2', 't', 'nr', 'f', now());
INSERT INTO activity.tjob (pk, fk_project, fk_jobtype, fk_jobstatus, fk_manager, name, billable, nr, ismaintenance, jobstartdate) VALUES (5, 4, 1, 2, 1, 'Test Auftrag', 't', 'nr', 'f', now());
SELECT setval('activity.tjob_pk_seq', 5);

--insert positions
delete from activity.tposition;
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (1, 1, 2, 'test1 position11', 100.00, 300.00, 1000.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (2, 1, 2, 'test1 position12', 220.00, 105.00, 40.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (3, 2, 2, 'test1 position21', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (4, 2, 2, 'test1 position22', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (5, 3, 2, 'test2 position11', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (6, 3, 2, 'test2 position12', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (7, 4, 2, 'test2 position21', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (8, 4, 2, 'test2 position22', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
INSERT INTO activity.tposition (pk, fk_job, fk_positionstatus, name, costs, income, expectedwork, positionstartdate) VALUES (9, 5, 2, 'Test Position', 1.00, 1.00, 1.00, '2010-10-11 00:00:00');
SELECT setval('activity.tposition_pk_seq', 9);

--insert timers
delete from activity.ttimer;
INSERT INTO activity.ttimer (pk,fk_resource,fk_position,time,time_diff) VALUES (1,2,1,'2011-02-14 19:09:11.519','2011-02-14 19:09:14.943');
SELECT setval('activity.ttimer_pk_seq', 1);

--insert activities
delete from activity.tactivity;
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (1, 2, 1, 'activity1 position11', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (2, 2, 2, 'activity1 position12', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (3, 2, 3, 'activity1 position21', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (4, 2, 4, 'activity1 position22', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (5, 2, 5, 'activity2 position11', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (6, 2, 6, 'activity2 position12', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (7, 2, 7, 'activity2 position21', '2010-10-11 00:00:00', 1.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (8, 2, 8, 'activity2 position22', '2010-10-11 00:00:00', 1.000);

-- activities for ActivityDAOImplTest#testGetHoursFromInterval  (complete hours: 64,5)
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (9, 1, 1, 'test1', '2012-10-30 00:00:00', 1.750);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (10, 1, 2, 'test2', '2012-10-29 00:00:00', 12.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (11, 1, 3, 'test3', '2012-10-13 00:00:00', 1.600);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (12, 1, 4, 'test4', '2012-10-14 00:00:00', 3.250);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (13, 1, 5, 'test5', '2012-10-15 00:00:00', 1.800);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (14, 1, 6, 'test6', '2012-10-16 00:00:00', 10.000);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (15, 1, 7, 'test7', '2012-10-20 00:00:00', 1.100);
INSERT INTO activity.tactivity (pk, fk_resource, fk_position, name, date, hours) VALUES (16, 1, 8, 'test8', '2012-10-21 00:00:00', 33.000);


SELECT setval('activity.tactivity_pk_seq', 16);

--insert position mappings
delete from activity.tpos_res_mapping;
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (1, 1, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (2, 2, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (3, 3, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (4, 4, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (5, 5, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (6, 6, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (7, 7, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (8, 8, 2, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (9, 1, 4, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (10, 2, 4, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (11, 3, 4, 1);
INSERT INTO activity.tpos_res_mapping (pk, fk_position, fk_resource, fk_status) VALUES (12, 8, 1, 1);
SELECT setval('activity.tpos_res_mapping_pk_seq', 12);

--insert costs
delete from activity.tcost;
INSERT INTO activity.tcost (pk,fk_job,fk_costtype,fk_resource,name,cost,date,note) VALUES (1 ,1 ,1 ,1,'cost',1 ,now(),'note');
SELECT setval('activity.tcost_pk_seq', 1);

--insert delete_requets
delete from activity.tdelete_request;
INSERT INTO activity.tdelete_request (pk,fk_type,fk_controller,fk_resource,note,status,fk_id) VALUES (1,'activity' ,1 ,2 ,'note','open',0);
INSERT INTO activity.tdelete_request (pk,fk_type,fk_controller,fk_resource,note,status,fk_id) VALUES (2,'overtime' ,1 ,2 ,'note','open',0);
INSERT INTO activity.tdelete_request (pk,fk_type,fk_controller,fk_resource,note,status,fk_id) VALUES (3,'loss' ,1 ,2 ,'note','open',0);
SELECT setval('activity.tdelete_request_pk_seq', 3);

--insert invoices
delete from activity.tinvoice;
INSERT INTO activity.tinvoice (pk,fk_job,name,nr,amount,invoiced,payed,note) VALUES (1,1 ,'name','number',1,'2011-02-04 11:27:29.430','2011-02-04 11:27:29.430','note');
SELECT setval('activity.tinvoice_pk_seq', 1);

--insert lossdays
delete from activity.tlossdays;
INSERT INTO activity.tlossdays (event_id,days) VALUES (1,1);

--insert settings
delete from activity.tsettings;
INSERT INTO activity.tsettings (pk,key,value) VALUES (1,'test_key','test_value');
SELECT setval('activity.tsettings_pk_seq', 1);

--insert skill defs
truncate activity.tskills_def cascade;
INSERT INTO activity.tskills_def (pk,name) VALUES (1,'java');
INSERT INTO activity.tskills_def (pk,name) VALUES (2,'SQL');
INSERT INTO activity.tskills_def (pk,name) VALUES (3,'PHP');
INSERT INTO activity.tskills_def (pk,name) VALUES (4,'Design Pattern');
INSERT INTO activity.tskills_def (pk,name) VALUES (5,'Javascript');
INSERT INTO activity.tskills_def (pk,name) VALUES (6,'Kaffe kochen');
INSERT INTO activity.tskills_def (pk,name) VALUES (7,'CSS');
INSERT INTO activity.tskills_def (pk,name) VALUES (8,'HTML5');
INSERT INTO activity.tskills_def (pk,name) VALUES (9,'Ätsch sagen');
SELECT setval('activity.tskills_def_pk_seq', 9);

--insert skills
delete from activity.tskills;
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (1,1,1,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (2,1,2,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (3,1,3,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (4,1,4,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (5,2,5,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (6,3,6,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (7,4,7,3);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (8,5,8,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (9,6,9,3);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (10,7,1,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (11,8,2,1);
INSERT INTO activity.tskills (pk,fk_resource,fk_skills_def,value) VALUES (12,9,3,1);
SELECT setval('activity.tskills_pk_seq', 12);
