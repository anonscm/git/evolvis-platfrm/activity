/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.DeleteRequestDAO;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.filter.DeleteRequestFilter;

/**
 * Implementation for DeleteRequestDAO.
 *
 */
@Stateless
@Local(DeleteRequestDAO.class)
public class DeleteRequestDAOImpl extends BaseDAOWithJPA<DeleteRequest> implements DeleteRequestDAO {

	@Override
	public List<DeleteRequest> listDeleteRequests(DeleteRequestFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("SELECT dr FROM DeleteRequest as dr join fetch dr.resource as resource join fetch dr.controller as controller");

		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}
	
	private void qlFilterBuilder(DeleteRequestFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("status", filter.getStatus(), "dr.status=:status", params, hql, hasWhere);
		hasWhere = addCondition("resourceId", filter.getResourceId(), "dr.resource.pk=:resourceId", params, hql, hasWhere);
		hasWhere = addCondition("controllerId", filter.getControllerId(), "dr.controller.pk=:controllerId", params, hql, hasWhere);
	}

	@Override
	public Long countDeleteRequests(DeleteRequestFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		StringBuilder hql = new StringBuilder("SELECT count(dr) FROM DeleteRequest as dr join dr.resource as resource join dr.controller as controller");
		
		qlFilterBuilder(filter, params, hql, false);
		
		return getSingleResult(hql, params);
	}
	@Override
	public DeleteRequest getRequestByTypeAndId(String entityType, Long entityId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("entityId", entityId);
		params.put("entityType", entityType);
		List<DeleteRequest> res = list("DeleteRequest.GET_BY_TYPE_AND_ID", params);
		
		if (res.size() > 0) {
			return res.get(0);
		} else {
			return null;
		}
	}
}
