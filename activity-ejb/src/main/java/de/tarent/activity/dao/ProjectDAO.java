/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.filter.ProjectFilter;

/**
 * DAO interface for Project entity.
 *
 */
public interface ProjectDAO extends BaseDAO<Project> {

	/**
	 * List all Projects.
	 *
	 * @return List<Project>
	 */
	List<Project> findAll();

	/**
	 * List projects by Resource id.
	 *
	 * @param resId
	 *            Resource id
	 * @return List<Project>
	 */
	List<Project> findByResource(Long resId);

	/**
	 * List filtered projects.
	 *
	 * @param filter
	 *            ProjectFilter
	 * @return List<Project>
	 */
	List<Project> listProjects(ProjectFilter filter);

	/**
	 * Count filtered projects.
	 *
	 * @param filter
	 *            ProjectFilter
	 * @return number of filtered projects
	 */
	Long countProject(ProjectFilter filter);

	/**
	 * List projects by customer id.
	 *
	 * @param customerId
	 *            customerId
	 * @return project list
	 */
	List<Project> findByCustomer(Long customerId);

	/**
	 * List of project where user is responsible.
	 *
	 * @param resId
	 *            -resource id
	 * @return list of projects
	 */
	List<Project> findByResponsibleResource(Long resId);

	/**
	 * List of all active projects.
	 *
	 * @return list of projects
	 */
	List<Project> findAllActive();

	/**
	 * Returns {@code true} if the project is deletable.
	 *
	 * @param projectId
	 * @return
	 */
	boolean isDeletable(Long projectId);

	/**
	 * Returns all recorded changes of the {@link Project} with the given id.
	 *
	 * @param id
	 * @return
	 */
	List<ChangeInfo> getChanges(Long id);

	/**
	 * Deletes a {@link Project} with its jobs.
	 *
	 * @param entity
	 *            the {@link Project} to delete
	 */
	@Override
	void delete(Project entity);
}
