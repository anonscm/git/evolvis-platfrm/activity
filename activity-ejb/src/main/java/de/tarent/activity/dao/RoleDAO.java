/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;

/**
 * DAO interface for Role entity.
 */
public interface RoleDAO extends BaseDAO<Role> {

    /**
     * Returns a list of all roles.
     * 
     * @return all existence roles
     */
    List<Role> findAll();

    /**
     * Returns a list of all roles that have an assignment to given permission.
     * 
     * @param permission
     *            permission of role to search
     * @return list of roles, if no role exists with given permission assignment an empty list will be returned
     */
    List<Role> findByPermission(Permission permission);

    /**
     * Returns a list of all roles that are assigned to given resource.
     * 
     * @param resource
     *            resource of role to search
     * @return list of roles, if no role is assigned to given resource an empty list will be returned
     */
    List<Role> findByResource(Resource resource);

    /**
     * Returns a list of roles with coexisting assignment to given resource and given permission.
     * 
     * @param resId
     *            resource of role to search
     * @param actionId
     *            permission of role to search
     * @return list of roles, if no role has a coexisting assignment to given resource and permission an empty list will
     *         be returned
     */
    List<Role> findByResourceAndPermission(Long resId, String actionId);

}
