/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.filter.ActivityFilter;

/**
 *
 * DAO interface for accessing Activity access.
 */
public interface ActivityDAO extends BaseDAO<Activity> {

	/**List filtered activities.
	 * @param filter ActivityFilter
	 * @return List<Activity>
	 */
	List<Activity> listActivity(ActivityFilter filter);

	/**Count filtered activities.
	 * @param filter ActivityFilter
	 * @return number of activities
	 */
	Long countActivity(ActivityFilter filter);

	/**Sum filtered activities hours.
	 * @param filter ActivityFilter
	 * @return total activities hours.
	 */
	BigDecimal getActivitiesHoursSum(ActivityFilter filter);

	/**
	 * @param positionId positionId
	 * @return List<Activity>
	 */
	List<Activity> listActivitiesByPosition(Long positionId);


	/**
	 * Calculate sum of hours between start date and end date.
	 * @param resourceId resource id
	 * @param startDate start date
	 * @param endDate end date
	 * @return sum of hours
	 */
	BigDecimal getHoursFromInterval(Long resourceId, Date startDate, Date endDate);

}
