/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;


import de.tarent.activity.dao.SkillsDefDAO;
import de.tarent.activity.domain.SkillsDef;

/**
 * Implementation class for SkillsDefDAO interface.
 * 
 */
@Stateless
@Local(SkillsDefDAO.class)
public class SkillsDefDAOImpl extends BaseDAOWithJPA<SkillsDef> implements SkillsDefDAO {

	@Override
	public List<SkillsDef> findAll() {
		return list("SkillsDef.GET_ALL");
	}

	@Override
	public List<SkillsDef> getSkillsDefByName(String name) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		return list("SkillsDef.GET_BY_NAME", params);

	}

}
