/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.LossDAO;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.LossFilter;

/**
 * Implementation class for OvertimeDAO interface.
 *
 */
@Stateless
@Local(LossDAO.class)
public class LossDAOImpl extends BaseDAOWithJPA<Loss> implements LossDAO {

	@Override
	public Long countLoss(LossFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select count(loss) FROM Loss as loss " + "join loss.lossType as type "
				+ "join loss.lossStatusByFkLossStatus as status " + "join loss.resourceByFkResource as res "
				+ "left join loss.resourceByFkPl1 as res1 " + "left join loss.resourceByFkPl2 as res2 "
				+ "left join loss.resourceByFkPl3 as res3 " + "left join loss.resourceByFkPl4 as res4 "
				+ "left join loss.resourceByFkPl5 as res5 " + "left join loss.lossStatusByFkPl1Status as st1 "
				+ "left join loss.lossStatusByFkPl2Status as st2 " + "left join loss.lossStatusByFkPl3Status as st3 "
				+ "left join loss.lossStatusByFkPl4Status as st4 " + "left join loss.lossStatusByFkPl5Status as st5 ");

		qlFilterBuilder(filter, params, hql, false);

		return getSingleResult(hql, params);
	}

	@Override
	public List<Loss> listLoss(LossFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("SELECT loss FROM Loss as loss " + "join fetch loss.lossType as type "
				+ "join fetch loss.lossStatusByFkLossStatus as status "
				+ "join fetch loss.resourceByFkResource as res " + "left join fetch loss.resourceByFkPl1 as res1 "
				+ "left join fetch loss.resourceByFkPl2 as res2 " + "left join fetch loss.resourceByFkPl3 as res3 "
				+ "left join fetch loss.resourceByFkPl4 as res4 " + "left join fetch loss.resourceByFkPl5 as res5 "
				+ "left join fetch loss.lossStatusByFkPl1Status as st1 " + "left join fetch loss.lossStatusByFkPl2Status as st2 "
				+ "left join fetch loss.lossStatusByFkPl3Status as st3 " + "left join fetch loss.lossStatusByFkPl4Status as st4 "
				+ "left join fetch loss.lossStatusByFkPl5Status as st5 ");

		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	private void qlFilterBuilder(LossFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("resId", filter.getResourceId(), "res.pk=:resId", params, hql, hasWhere);
		hasWhere = addCondition("typeId", filter.getType(), "type.pk=:typeId", params, hql, hasWhere);
		hasWhere = addCondition("statusId", filter.getStatus(), "status.pk=:statusId", params, hql, hasWhere);
		hasWhere = addCondition("endDate", filter.getEndDate(), "loss.endDate<=:endDate", params, hql, hasWhere);
		hasWhere = addCondition("startDate", filter.getStartDate(), "loss.startDate>=:startDate", params, hql, hasWhere);

//		String condition = ":resPk in (res1.pk, res2.pk, res3.pk, res4.pk, res5.pk)";
		String condition = "((res1.pk=:resPk and st1 is null ) or "
				+ "(res2.pk=:resPk and st2 is null) or "
				+ "(res3.pk=:resPk and st3 is null) or "
				+ "(res4.pk=:resPk and st4 is null) or "
				+ "(res5.pk=:resPk and st5 is null))";

		hasWhere = addCondition("resPk", filter.getResourceByPk(), condition, params, hql, hasWhere);

	}

	@Override
	public List<Loss> getCurrentLossByProject(Date date, Long projectId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("projectId", projectId);
		params.put("date", date);
		return list("Loss.GET_CURRENT_BY_PROJRCT", params);
	}

	@Override
	public List<Loss> getFuturetLossByProject(Date date, Long projectId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("projectId", projectId);
		params.put("date", date);
		return list("Loss.GET_FUTURE_BY_PROJRCT", params);
	}

	@Override
	public List<Loss> getCurrentLossByLocation(Date date, String[] location) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("date", date);

		StringBuilder query = new StringBuilder();
		query.append("select loss FROM Loss loss " + "join loss.resourceByFkResource as res "
				+ "join res.branchOffice as branch " + "left outer join loss.lossType as type "
				+ "left outer join loss.lossStatusByFkLossStatus as status " + "where ( res.active ='t' "
				+ "and loss.startDate <= :date and loss.endDate >= :date "
				+ "and ((type.pk=1 AND status.pk=2) OR (type.pk=1 AND status.pk=4) OR (type.pk=2) OR (type.pk=3 AND status.pk=2)) "
				+ "and branch.pk in (");

		for (int i = 0; i < location.length; i++) {
			query.append(location[i].toString());
			if (i < location.length - 1) {
				query.append(",");
			}
		}
		query.append(")) ");
		return tableQuery(query, new BaseFilter(null, null, "loss.resourceByFkResource.branchOffice.pk",
				"asc"), params);
	}

	@Override
	public BigDecimal getLossDayFromYear(Long resourceId, Integer year) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resId", resourceId);
		params.put("year", year.longValue());
		BigDecimal days = (BigDecimal) list("Loss.GET_LOSS_DAYS", params).get(0);
		return days != null ? days : BigDecimal.ZERO;
	}

}
