/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.SortControl;
import javax.naming.ldap.SortResponseControl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.dao.LdapDAO;
import de.tarent.activity.domain.LdapConfig;
import de.tarent.activity.domain.LdapUser;
import de.tarent.activity.service.ReportService;

@Stateless
@Local(LdapDAO.class)
public class LdapDAOImpl implements LdapDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

	private LdapContext getAnonBindToLdap(LdapConfig ldapConfig) {
		Hashtable<String, Object> anonymousEnv = new Hashtable<String, Object>(11);
		anonymousEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		anonymousEnv.put(Context.PROVIDER_URL, ldapConfig.getLdapUrl());
		anonymousEnv.put(Context.SECURITY_AUTHENTICATION, "none"); // Anonymous bind

		LdapContext anonCtx = null;

		try {
			anonCtx = new InitialLdapContext(anonymousEnv, null);
		} catch (NamingException e) {
			LOGGER.error("", e);
		} 

		return anonCtx;
	}

	@Override
	public List<LdapUser> findAllUsers(LdapConfig ldapConfig, Map<String, String> sortAlias) {

		SearchControls searchControls = new SearchControls();
		searchControls
				.setReturningAttributes(new String[] { ldapConfig.getUserFilter(), "uid", "givenName", "sn", "mail", "univentionBirthday" });
		searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

		String searchFilter = "(" + ldapConfig.getUserFilter() + "=*)";
		List<LdapUser> ldapUserList = new ArrayList<LdapUser>();

		NamingEnumeration<SearchResult> results = null;

		try {
			this.getAnonBindToLdap(ldapConfig).setRequestControls(new Control[] { new SortControl(sortAlias.get("sortAttrib"), Control.CRITICAL) }); 
			// Not server so far
			results = this.getAnonBindToLdap(ldapConfig).search(ldapConfig.getLdapBaseDn(), searchFilter, searchControls);

			if (results != null) {
				while (results.hasMore()) {
					LdapUser ldapUser = new LdapUser();
					SearchResult searchResult = results.next();
					Attributes attributes = searchResult.getAttributes();

					Attribute username = attributes.get(ldapConfig.getUserFilter());
					if (username == null) {
						LOGGER.warn("LDAP entry has no username, will be skipped.");
						continue;
					}
					ldapUser.setUsername(username.get(0).toString());

					Attribute sn = attributes.get("sn");
					if (sn != null) {
						ldapUser.setLastname(sn.get(0).toString());						
					} 

					Attribute givenName = attributes.get("givenName");
					if (givenName != null) {
						ldapUser.setFirstname(givenName.get(0).toString());						
					} 

					Attribute mail = attributes.get("mail");
					if (mail != null) {
						ldapUser.setMail(mail.get(0).toString());						
					} 

					Attribute univentionBirthday = attributes.get("univentionBirthday");
					if (univentionBirthday != null) {
						ldapUser.setBirth(univentionBirthday.get(0).toString());						
					} 

					ldapUserList.add(ldapUser);
				}

				Control[] controls = this.getAnonBindToLdap(ldapConfig).getResponseControls();

				if (controls != null) {
					for (int i = 0; i < controls.length; i++) {
						if (controls[i] instanceof SortResponseControl) {
							SortResponseControl src = (SortResponseControl) controls[i];
							if (!src.isSorted()) {
								throw src.getException();
							}
						}
					}
				}
			}
		} catch (NamingException e) {
			LOGGER.error("", e);
		} catch (IOException e) {
			LOGGER.error("", e);
		}

		return ldapUserList;
	}

	@Override
	public List<String> getGroupsOfUser(LdapConfig ldapConfig, String userid) {

		SearchControls searchControls = new SearchControls();
		searchControls.setReturningAttributes(new String[] { ldapConfig.getRoleFilter() });
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		String searchFilter = "(&(objectClass=posixGroup)(" + ldapConfig.getLinkFilter() + "=" + ldapConfig.getUserFilter() + "=" + userid
				+ "," + ldapConfig.getLdapBaseDn() + "))";
		NamingEnumeration<SearchResult> results = null;
		List<String> list = new ArrayList<String>();

		try {
			results = this.getAnonBindToLdap(ldapConfig).search(ldapConfig.getGroupDn(), searchFilter, searchControls);

			if (results != null) {
				while (results.hasMore()) {
					SearchResult searchResult = results.next();
					Attributes attributes = searchResult.getAttributes();
					Attribute attribute = attributes.get(ldapConfig.getRoleFilter());
					String roleCn = attribute.get(0).toString();
					list.add(roleCn);
				}
			}
		} catch (NamingException e) {
            LOGGER.error("NamingException " + e.getMessage());
		}
		return list;
	}
}