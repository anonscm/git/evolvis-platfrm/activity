/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.util.List;

import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ResourceFilter;

/**
 * DAO interface for Resource entity.
 *
 */
public interface ResourceDAO extends BaseDAO<Resource> {

	/**List all Resources.
	 * @return List<Resource>
	 */
	List<Resource> findAll();
	
	/**
	 * @param username Resource username
	 * @return a Resource by the given username
	 */
	Resource loadResource(String username);
	
	/**List filtered resources.
	 * @param filter ResourceFilter
	 * @return List<Resource>
	 */
	List<Resource> listResource(ResourceFilter filter);

	/**Count filtered resources.
	 * @param filter ResourceFilter
	 * @return number of filtered resources
	 */
	Long countResource(ResourceFilter filter);
	
	/**
	 * Get resource list from project.
	 * 
	 * @param id - project id
	 * @return List<Resource>
	 */
	List<Resource> listResourceFromProject(Long id);
	
	/**
	 * Gets all active resources.
	 * @return List with active resources
	 */
	List<Resource> getActiveResources();

	/**
	 * List resources by position id.
	 * @param selectedPositionId Position id
	 * @return List of resources.
	 */
	List<Resource> getByPosition(Long selectedPositionId);
	
	/**
	 * @return List<Resource>
	 */
	List<Resource> getAllProjectManager();
	
	List<Resource> getByPermission(Permission permission);
}
