/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.ActivityDAO;
import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.filter.ActivityFilter;

/**
 * Implementation class for ActivityDAO interface.
 *
 */
@Stateless
@Local(ActivityDAO.class)
public class ActivityDAOImpl extends BaseDAOWithJPA<Activity> implements ActivityDAO {

	@Override
	public Long countActivity(ActivityFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select count(activity) from Activity as activity "
				+ "left join activity.position as pos " + "left join pos.job as job");

		qlFilterBuilder(filter, params, hql, false);

		return getSingleResult(hql, params);
	}

	@Override
	public BigDecimal getActivitiesHoursSum(ActivityFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select sum(activity.hours) from Activity as activity "
				+ "left join activity.position as pos " + "left join pos.job as job");

		qlFilterBuilder(filter, params, hql, false);

		BigDecimal ret = getSingleResult(hql, params);
		return ret != null ? ret : BigDecimal.ZERO;

	}

	@Override
	public List<Activity> listActivity(ActivityFilter filter) {

		Map<String, Object> params = new HashMap<String, Object>();

		// TODO see if a fetch join is really necessary
		StringBuilder hql = new StringBuilder("select activity from Activity as activity "
				+ "left join activity.position as pos " + "left join pos.job as job");

		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	/**
	 * This method build the query StringBuilder depending on the filter
	 * parameters.
	 *
	 * @param filter
	 *            ActivityFilter
	 * @param params
	 *            filter parameters
	 * @param hql
	 *            query StringBuilder
	 */
	private void qlFilterBuilder(ActivityFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("resId", filter.getResourceId(), "activity.resource.id=:resId", params, hql, hasWhere);
		hasWhere = addCondition("jobId", filter.getJobId(), "job.pk=:jobId", params, hql, hasWhere);
		hasWhere = addCondition("posId", filter.getPositionId(), "pos.pk=:posId", params, hql, hasWhere);
		hasWhere = addCondition("startDate", filter.getStartDate(), "activity.date>=:startDate", params, hql, hasWhere);
		hasWhere = addCondition("endDate", filter.getEndDate(), "activity.date<=:endDate", params, hql, hasWhere);
        hasWhere = addCondition("projectId", filter.getProjectId(), "job.project.pk=:projectId", params, hql, hasWhere);
	}

	@Override
	protected String getSecondSortColumn() {
		return "activity.pk";
	}

	@Override
	public List<Activity> listActivitiesByPosition(Long positionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("positionId", positionId);
		return list("Activity.GET_BY_POSITION", params);
	}

	@Override
	public BigDecimal getHoursFromInterval(Long resourceId, Date startDate, Date endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceId", resourceId);

		StringBuilder hql = new StringBuilder(
				"select sum(activity.hours) from Activity as activity where activity.resource.id=:resourceId");

		if (startDate != null) {
			hql.append(" and activity.date > :startDate");
			params.put("startDate", startDate);
		}

		if (endDate != null) {
			hql.append(" and activity.date <= :endDate");
			params.put("endDate", endDate);
		}

		return getSingleResult(hql, params);

	}

}
