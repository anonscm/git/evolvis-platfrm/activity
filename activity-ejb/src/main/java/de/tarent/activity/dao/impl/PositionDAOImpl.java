/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.dao.PositionDAO;
import de.tarent.activity.domain.AuditCompareField;
import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Timer;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.service.ProjectService;

/**
 * Implementation class for PositionDAO interface.
 * 
 */
@Stateless
@Local(PositionDAO.class)
public class PositionDAOImpl extends BaseDAOWithJPA<Position> implements PositionDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionDAOImpl.class);

	@Override
	public List<Position> listPosition(PositionFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select distinct(position) from Position as position left join position.posResourceMappings as posResourceMapping " +
				"join fetch position.positionStatus as positionStatus join fetch position.job as job join fetch job.project as project");
		qlFilterBuilder(filter, params, hql, false);
		return tableQuery(hql, filter, params);
	}
	
	private void qlFilterBuilder(PositionFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("jobId", filter.getJobId(), "position.job.id=:jobId", params, hql, hasWhere);
		
		// search positions dependent on user permissions
		if(filter.getViewBy() == PositionFilter.VIEW_BY_MANAGER){
		    // managers are only allowed to list position of project where he is project manager
		    hasWhere = addCondition("resourceId", filter.getResourceId(), " project.fkResource=:resourceId ", params, hql,
                    hasWhere);
		} else if (filter.getViewBy() == PositionFilter.VIEW_BY_DIVISION){
            throw new RuntimeException("Division Manager functionality is not implemented yet.");
		} else {
		    // if no special permission is given we list all positions to which the user is assigned
		    hasWhere = addCondition("resourceId", filter.getResourceId(), "posResourceMapping.resource.id=:resourceId", params, hql, hasWhere);
		}

	}

	@Override
	public Long countPosition(PositionFilter filter) {
        filter.setOffset(null);
        filter.setPageItems(null);
        return new Long(listPosition(filter).size());
	}
	
	
	@Override
	public List<Position> listMyPosition(PositionFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select distinct(position) from Position as position join fetch position.posResourceMappings as posResourceMapping join fetch position.job as job");
		qlFilterBuilder(filter, params, hql, false);
		return tableQuery(hql, filter, params);
	}
	
	

	@Override
	public Long countMyPosition(PositionFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select count(position) from Position as position join position.posResourceMappings as posResourceMapping");
		qlFilterBuilder(filter, params, hql, false);
		return getSingleResult(hql, params);
	}

	@Override
	public List<Position> findByJob(Long jobId, Long statusId, Long resourceId) {
		
		boolean isResourceId = (resourceId != null) ? true : false;
		
		StringBuilder sb = new StringBuilder("SELECT pos FROM Position pos " 
				+ "INNER JOIN pos.job job");
		
		if(isResourceId){
			sb.append(" INNER JOIN pos.posResourceMappings prm");
		}
		
		sb.append(" WHERE job.pk =:jobId");
		
		if(statusId != null){
			sb.append(" AND pos.positionStatus.pk =:statusId");
		}
		
		if(isResourceId){
			//position-resource mapping has active status, and was active in the last 24h
			sb.append(" AND pos.job.pk = :jobId AND prm.resource.pk = :resId"
				+ " AND prm.posResourceStatus.pk = :posResStatusId AND (prm.startDate is null OR prm.startDate <= :endDate)"
				+ " AND (prm.endDate is null OR prm.endDate >= :startDate)");
		}
		sb.append(" ORDER BY pos.name");
		
		Query query = entityManager.createQuery(sb.toString());
		
		query.setParameter("jobId", jobId);
		if(statusId != null){
			query.setParameter("statusId", statusId);
		}		
		if(isResourceId){
			Calendar cal = Calendar.getInstance();
			Date endDate = cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, -1);
			Date startDate = cal.getTime();
			
			query.setParameter("resId", resourceId);
			query.setParameter("posResStatusId", ProjectService.POS_RES_STATUS_ACTIVE);
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}

		return (List<Position>)query.getResultList();
	}

    @Override
    public boolean isDeletable(Long positionId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("positionId", positionId);
        Long count = getSingleResult("Activity.GET_COUNT_BY_POSITION", params);
        return count <= 0;
    }
    
    /**
     * Delete position entity with the given pk and also all relations to
     * resources which are stored in "pos_res_mapping" and all associated
     * timers.
     */
    @Override
    public void delete(Position entity) {
        if (entity == null) {
            throw new RuntimeException("Trying to remove a null entity "
                    + getEntityClass());
        }
        
        LOGGER.debug("Removing " + getEntityClass().getName());

        for (PosResourceMapping prm : entity.getPosResourceMappings()){
            entityManager.remove(prm);
        }

        for (Timer timer : entity.getTimers()){
            entityManager.remove(timer);
        }
        
        entityManager.remove(entity);
    }

    @Override
    public List<ChangeInfo> getChanges(Long positionId) {
        // get history
        AuditReader reader = AuditReaderFactory.get(entityManager);
        AuditQuery query = reader.createQuery().forRevisionsOfEntity(
                Position.class, false, true);
        query = query.add(AuditEntity.id().eq(positionId));
        @SuppressWarnings("unchecked")
        List<Object[]> revisions = query.getResultList();

        if (revisions != null && revisions.size() >= 2) { // diff(s) possible
            List<ChangeInfo> changes = new ArrayList<ChangeInfo>(
                    revisions.size() - 1);
            Position pos1 = (Position) revisions.get(0)[0];
            Field[] fields = Position.class.getDeclaredFields();
            for (int i = 1; i < revisions.size(); i++) {
                Position pos2 = (Position) revisions.get(i)[0];
                DefaultRevisionEntity dre = (DefaultRevisionEntity) revisions.get(i)[1];
                ChangeInfo info = new ChangeInfo(dre.getRevisionDate(), pos2.getUpdUser());
                try {
                    if (fields != null){
                        for (Field field : fields){
                            if (field
                                    .isAnnotationPresent(AuditCompareField.class)) {
                                field.setAccessible(true);
                                Object obj1 = field.get(pos1);
                                Object obj2 = field.get(pos2);
                                if ((obj1 == null && obj2 != null)
                                        || (obj1 != null && !obj1.equals(obj2))) {
                                    if (obj1 != null && obj1 instanceof BigDecimal
                                            && // special handling needed
                                               // because equals() of BigDecimal
                                               // is false if scale is not equal
                                            obj2 != null
                                            && ((BigDecimal) obj1)
                                                    .compareTo((BigDecimal) obj2) == 0){
                                        continue;
                                    }
                                    info.addChangeField(field.getName(), obj1, obj2);
                                }
                                field.setAccessible(false);
                            }
                        }
                    }
                } catch (IllegalArgumentException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                } catch (IllegalAccessException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                }
                changes.add(info);
                pos1 = pos2;
            }
            Collections.reverse(changes);
            return changes;
        }
        return Collections.emptyList();
    }
    
	@Override
	public BigDecimal getTotalDaysSpent(Long positionId) {
		
		BigDecimal totalDays = new BigDecimal(0);
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("positionId", positionId);
        BigDecimal hours = (BigDecimal) getSingleResult("Position.SUM_TOTAL_HOURS_SPENT", params);
        
        if(hours != null){
        	totalDays = new BigDecimal(Math.round(hours.doubleValue() / 2d) / 4d);
        }
        
        return totalDays;
	}
}
