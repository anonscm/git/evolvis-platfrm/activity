/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.RoleDAO;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;

/**
 * Implementation of Role DAO.
 */
@Stateless
@Local(RoleDAO.class)
public class RoleDAOImpl extends BaseDAOWithJPA<Role> implements RoleDAO {

	@Override
	public List<Role> findAll() {
		return list("Role.GET_ALL");
	}

	@Override
	public List<Role> findByPermission(Permission permission) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actionId", permission.getActionId());
		
		return list("Role.GET_BY_PERMISSION", params);
	}

	@Override
	public List<Role> findByResource(Resource resource) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceId", resource.getPk());
		
		return list("Role.GET_BY_RESOURCE", params);
	}
	
	@Override
	public List<Role> findByResourceAndPermission(Long resId, String actionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actionId", actionId);
		params.put("resourceId", resId);
		
		return list("Role.GET_BY_PERMISSION_AND_RESOURCE", params);
	}

}
