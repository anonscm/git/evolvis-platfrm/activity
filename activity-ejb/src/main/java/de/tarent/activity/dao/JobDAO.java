/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.math.BigDecimal;
import java.util.List;

import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.filter.JobFilter;

/**
 * DAO interface for JOB entity.
 */
public interface JobDAO extends BaseDAO<Job> {

	/**
	 * List all Jobs.
	 *
	 * @return List<Job>
	 */
	List<Job> findAll();

	/**
	 * List active Jobs by Resource id. Restrictions: position - resource
	 * mapping has status active, and was active in the last 24h
	 *
	 * @param resId
	 *            Resource id.
	 * @return List<Job>
	 */
	List<Job> findActiveJobsByResource(Long resId);

	/**
	 * List Jobs by Project id.
	 *
	 * @param projectId
	 *            Project id.
	 * @return List<Job>
	 */
	List<Job> findByProject(Long projectId);

	/**
	 * List filtered jobs.
	 *
	 * @param filter
	 *            filter
	 * @return List<Job>
	 */
	List<Job> listJob(JobFilter filter);

	/**
	 * Count filtered activities.
	 *
	 * @param filter
	 *            ActivityFilter
	 * @return number of activities
	 */
	Long countJob(JobFilter filter);

	/**
	 * Gets not maintenance jobs.
	 *
	 * @return list jobs
	 */
	List<Job> findNotMaintenanceJobs();

	/**
	 * List jobs with JobSatatus ordered.
	 *
	 * @return list with ordered jobs
	 */
	List<Job> getOrderedList();

	/**
	 * List Jobs by Resource id.
	 *
	 * @param resId
	 *            Resource id.
	 * @return List<Job>
	 */
	List<Job> findJobsByResource(Long resId);

	/**
	 * Returns {@code true} if the job is deletable.
	 *
	 * @param jobId
	 * @return
	 */
	boolean isDeletable(Long jobId);

	/**
	 * Returns all recorded changes of the {@link Job} with the given jobPk.
	 *
	 * @param jobPk
	 * @return
	 */
	List<ChangeInfo> getChanges(Long jobPk);

	/**
	 * Deletes a {@link Job} with its underlying positions.
	 *
	 * @param entity
	 *            the job to delete
	 */
	@Override
	public void delete(Job entity);
	
	/**
	 * Returns the total hours spent on this job
	 * 
	 * @param jobId
	 * @return
	 */
	BigDecimal getTotalDaysSpent(Long jobId);

	BigDecimal getTotalExpectedWork(Long jobId);

	BigDecimal getTotalCommunicatedWork(Long jobId);

	BigDecimal getTotalServiceCosts(Long jobId);

	BigDecimal getTotalFixedPrice(Long jobId);

	BigDecimal getTotalInvoice(Long jobId);

	BigDecimal getTotalInvoicePaid(Long jobId);

	BigDecimal getTotalOtherCosts(Long jobId);
	
}