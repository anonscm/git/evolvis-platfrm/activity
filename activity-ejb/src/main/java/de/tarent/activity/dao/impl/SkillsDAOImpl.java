/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;


import de.tarent.activity.dao.SkillsDAO;
import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.filter.AllSkillsFilter;
import de.tarent.activity.domain.filter.SkillsFilter;

/**
 * Implementation class for SkillsDAO interface.
 * 
 */
@Stateless
@Local(SkillsDAO.class)
public class SkillsDAOImpl extends BaseDAOWithJPA<Skills> implements SkillsDAO {

	@Override
	public List<Skills> listSkills(SkillsFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select skill from Skills as skill join fetch skill.skillsDef");

		qlFilterBuilder(filter, params, hql, false);
		return tableQuery(hql, filter, params);
	}

	@Override
	public Long countSkills(SkillsFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select count(skill) from Skills as skill join skill.skillsDef");

		qlFilterBuilder(filter, params, hql, false);
		return getSingleResult(hql, params);

	}

	private void qlFilterBuilder(SkillsFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("resourceId", filter.getResourceId(), "skill.resource.pk=:resourceId", params, hql,
				hasWhere);
		hasWhere = addCondition("skillsDefId", filter.getSkillsDefId(), "skill.skillsDef.pk=:skillsDefId", params, hql,
				hasWhere);

	}

	@Override
	public List<Skills> listAllSkills(AllSkillsFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select skill from Skills as skill");

		qlFilterBuilder(filter, params, hql, false);
		return tableQuery(hql, filter, params);
	}

	@Override
	public Long countAllSkills(AllSkillsFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select count(skill) from Skills as skill");

		qlFilterBuilder(filter, params, hql, false);
		return getSingleResult(hql, params);
	}

	private void qlFilterBuilder(AllSkillsFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition(
				"resourceName",
				filter.getResourceName(),
				"lower(skill.resource.firstname || ' ' || skill.resource.lastname) like lower('%'||:resourceName||'%')",
				params, hql, hasWhere);
		hasWhere = addCondition("skillsDefName", filter.getSkillsDefName(),
				"lower(skill.skillsDef.name) like lower('%'||:skillsDefName||'%')", params, hql, hasWhere);
	}

	@Override
	public Skills getUserSkillBySkillDef(Long resourceId, Long skillDefId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resId", resourceId);
		params.put("skillDefId", skillDefId);
		List<Skills> list = list("Skills.GET_BY_RESOURCE_AND_SKILL_DEF", params);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

}
