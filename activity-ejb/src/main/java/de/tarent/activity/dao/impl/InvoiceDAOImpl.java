/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.InvoiceDAO;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.service.ProjectService;

/**
 * Implementation class for InvoiceDAO interface.
 * 
 */
@Stateless
@Local(InvoiceDAO.class)
public class InvoiceDAOImpl extends BaseDAOWithJPA<Invoice> implements InvoiceDAO {
	
	@Override
	public List<Invoice> listInvoice(InvoiceFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
			
		StringBuilder hql = new StringBuilder("SELECT invoice FROM Invoice invoice LEFT JOIN invoice.job job LEFT JOIN invoice.job.project pro LEFT JOIN invoice.project project ");
		qlFilterBuilder(filter, params, hql);
		
		return tableQuery(hql, filter, params);
	}

	@Override
	public Long countInvoice(InvoiceFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder("SELECT count(invoice) FROM Invoice invoice LEFT JOIN invoice.job job LEFT JOIN invoice.job.project pro LEFT JOIN invoice.project project ");
		qlFilterBuilder(filter, params, hql);
		
		return getSingleResult(hql, params);
	}

	private void qlFilterBuilder(InvoiceFilter filter, Map<String, Object> params, StringBuilder hql) {
		boolean hasWhere = false;
		if (ProjectService.INVOICE_TYPE_NEW.equals(filter.getType())) {
			hql.append("where invoice.invoiced is null ");
			hasWhere = true;
		} else if (ProjectService.INVOICE_TYPE_INVOICED.equals(filter.getType())) {
			hql.append("where invoice.invoiced is not null and invoice.payed is null");
			hasWhere = true;
		} else if (ProjectService.INVOICE_TYPE_PAID.equals(filter.getType())) {
			hql.append("where invoice.invoiced is not null and invoice.payed is not null");
			hasWhere = true;
		} else if (ProjectService.INVOICE_TYPE_COMPLETED.equals(filter.getType())) {
			hql.append("where invoice.invoiced is not null and invoice.payed is not null and job.isMaintenance!='t' and job.jobStatus.id="
					+ ProjectService.JOBSTATUS_FINISHED);
			hasWhere = true;
		}
		
		hasWhere = compositeOrCondition("customerId", filter.getCustomerId(), "(pro.customer.pk = :customerId or project.customer.pk = :customerId)", params, hql,
				hasWhere);
		hasWhere = addCondition("projectId", filter.getProjectId(), "(project.pk = :projectId or pro.pk = :projectId)", params, hql, hasWhere);
		addCondition("jobId", filter.getJobId(), "job.pk=:jobId", params, hql, hasWhere);
	}

	@Override
	public List<Invoice> findByJob(Long jobId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("jobId", jobId);
		return list("Invoice.GET_BY_JOB", params);
	}

	@Override
	public List<Invoice> invoicesByProject(Long projectId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("projectId", projectId);
		return list("Invoice.GET_INVOICES_BY_PROJECT", params);
	}

}
