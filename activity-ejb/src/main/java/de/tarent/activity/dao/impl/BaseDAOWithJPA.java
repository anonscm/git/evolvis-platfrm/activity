/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.dao.BaseDAO;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.util.Auditable;
import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueConstraint;
import de.tarent.activity.domain.util.UniqueProperty;
import de.tarent.activity.domain.util.UniqueValidationAware;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.util.UserUtil;

/**
 * This is base DAO class using JPA.
 * 
 * @param <EntityClass>
 *            Entity
 */
public abstract class BaseDAOWithJPA<EntityClass extends DomainObject> implements BaseDAO<EntityClass> {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOWithJPA.class);

	private Class<EntityClass> entityClass;
	
	@EJB
	private UserUtil userUtil;

	@PersistenceContext(unitName = "activityPU")
	protected EntityManager entityManager;

	protected EntityManager getEntityManager() { 
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Constructor.
	 */
	@SuppressWarnings("unchecked")
	public BaseDAOWithJPA() {
		this.entityClass = (Class<EntityClass>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	protected final Class<EntityClass> getEntityClass() {
		return entityClass;
	}

	@Override
	public EntityClass find(Long pk) {
		if (pk == null) {
			throw new RuntimeException("Trying to find an entity "
					+ getEntityClass() + " with null id");
		}
		
		LOGGER.debug("Find " + getEntityClass().getName() + " for Id " + pk);

		return entityManager.find(getEntityClass(), pk);
	}

	@Override
	public Long create(EntityClass entity) {
		if (entity == null) {
			throw new RuntimeException("Trying to persist a null entity "
					+ getEntityClass());
		}
		
		LOGGER.debug("Creating " + getEntityClass().getName());
		
		if (entity instanceof Auditable) {
			((Auditable) entity).setCrDate(new Date());
			((Auditable) entity).setCrUser(userUtil.getCurrentUser());
		}
		
		entityManager.persist(entity);
		
		return entity.getPk();
	}

	@Override
	public EntityClass update(EntityClass entity) {
		if (entity == null) {
			throw new RuntimeException("Trying to merge a null entity "
					+ getEntityClass());
		}

		LOGGER.debug("Merging " + getEntityClass().getName());
		
		if (entity instanceof Auditable) {
			((Auditable) entity).setUpdDate(new Date());
			((Auditable) entity).setUpdUser(userUtil.getCurrentUser());
		}
		
		return entityManager.merge(entity);
	}
	
	@Override
	public void validateUniqueConstraint(EntityClass entity) throws UniqueValidationException   {
		if (!(entity instanceof UniqueValidationAware)) {
			LOGGER.warn("Trying to validate unique constraint on a non-UniqueValidationAware entity!");
			return;
		}
		
		List<UniqueConstraint> uniqueConstraints = ((UniqueValidationAware) entity).getUniqueConstraints();
		for (UniqueConstraint constraint : uniqueConstraints) {
			if (!validateConstraint(constraint)) {
				throw new UniqueValidationException(constraint);
			}
		}
	}

	private Boolean validateConstraint(UniqueConstraint constraint) {
		Boolean hasWhere = false;
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("FROM " + getEntityClass().getName());
		
		for (UniqueProperty property : constraint.getProperties()) {
			params.put(property.getQueryParamName(), property.getValue());
			hasWhere = addCondition(property.getName() + "=:" + property.getQueryParamName(), hql, hasWhere);
		}
		
		if (constraint.getCurrentEntityId() != null) {
			params.put("pk", constraint.getCurrentEntityId());
			hasWhere = addCondition("pk!=:pk", hql, hasWhere);
		}
		
		Query query = entityManager.createQuery(hql.toString());
		addQueryParams(query, params);
		
		return !(query.getResultList().size() > 0);
	}

	public void delete(EntityClass entity) {
		if (entity == null) {
			throw new RuntimeException("Trying to remove a null entity "
					+ getEntityClass());
		}
		
		LOGGER.debug("Removing " + getEntityClass().getName());

		entityManager.remove(entity);
	}

	@Override
	public void delete(Long pk) {
		if (pk == null) {
			throw new RuntimeException("Trying to remove an entity with type "
					+ getEntityClass() + " and id null");
		}

		EntityClass entity = find(pk);
		
		LOGGER.debug("Removing " + getEntityClass().getName() + " with Id " + pk);

		delete(entity);
	}

	protected <V> List<V> list(String query) {
		return list(query, (Map<String, ?>) null);
	}

	protected <V> List<V> list(String query, Map<String, ?> params) {
		return list(query, Integer.MAX_VALUE, Integer.MIN_VALUE, params);
	}

	protected <V> List<V> list(String query, int startPosition, int maxResult,
			Map<String, ?> params) {
		if (query == null) {
			throw new RuntimeException("Query is null");
		}

		Query ejbqlQuery = entityManager.createNamedQuery(query);

		if (startPosition != Integer.MAX_VALUE) {
			ejbqlQuery.setFirstResult(startPosition);
		}
		if (maxResult != Integer.MIN_VALUE) {
			ejbqlQuery.setMaxResults(maxResult);
		}

		if (params != null) {
			for (String key : params.keySet()) {
				Object value = params.get(key);

				ejbqlQuery.setParameter(key, value);
			}
		}

		return (List<V>) ejbqlQuery.getResultList();
	}

    protected <T> T getSingleResult(StringBuilder hql,
            Map<String, Object> params) {
        Query query = entityManager.createQuery(hql.toString());
        return (T) getSingleResult(query, params);
    }

    protected <T> T getSingleResult(String queryName,
            Map<String, Object> params) {
        Query query = entityManager.createNamedQuery(queryName);
        return (T) getSingleResult(query, params);
    }

    private <T> T getSingleResult(Query query, Map<String, Object> params) {
        addQueryParams(query, params);

        return (T) query.getSingleResult();
    }

	protected List<EntityClass> tableQuery(StringBuilder hql, BaseFilter filter,
			Map<String, Object> params) {

		if (filter.getSortColumn() != null && filter.getSortColumn().length() > 0) {
			hql.append(" order by " + filter.getSortColumn());

			if (filter.getSortOrder().equalsIgnoreCase("asc")) {
				hql.append(" asc");
			} else {
				hql.append(" desc");
			}

			if (getSecondSortColumn() != null && getSecondSortColumn().length() > 0 && !getSecondSortColumn().equals(filter.getSortColumn())) {
				hql.append(", " + getSecondSortColumn() + " " + filter.getSortOrder());
			}
		}
		
		Query query = entityManager.createQuery(hql.toString());
		
		addQueryParams(query, params);

		if (filter.getOffset() != null) {
			query.setFirstResult(filter.getOffset().intValue());
		}
		if (filter.getPageItems() != null) {
			query.setMaxResults(filter.getPageItems().intValue());
		}

		return query.getResultList();
	}

	private void addQueryParams(Query query, Map<String, Object> params) {
		for (String param : params.keySet()) {
			query.setParameter(param, params.get(param));
		}
	}

	protected Boolean addCondition(String filterKey, Object filterValue, String condition, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		if (filterValue != null	&& (!(filterValue instanceof String) || ((String) filterValue).length() != 0)) {
			if (hasWhere) {
				hql.append(" and ");
			} else {
				hql.append(" where ");
				hasWhere = true;
			}

			hql.append(condition);

			params.put(filterKey, filterValue);
		}
		return hasWhere;
	}

    protected Boolean compositeOrCondition(String filterKey, Object filterValue, String condition, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
        if (filterValue != null	&& (!(filterValue instanceof String) || ((String) filterValue).length() != 0)) {
            if (hasWhere) {
                hql.append(" and ");
            } else {
                hql.append(" where ");
                hasWhere = true;
            }

            hql.append(condition);

            params.put(filterKey, filterValue);
        }
        return hasWhere;
    }
	
	protected Boolean addCondition(String condition, StringBuilder hql, Boolean hasWhere) {
		if (hasWhere) {
			hql.append(" and ");
		} else {
			hql.append(" where ");
			hasWhere = true;
		}
		hql.append(condition);
		return hasWhere;
	}
	
	public UserUtil getUserUtil() {
		return userUtil;
	}

	public void setUserUtil(UserUtil userUtil) {
		this.userUtil = userUtil;
	}
	
	protected String getSecondSortColumn() {
		return null;
	}
}
