/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;


import de.tarent.activity.domain.util.DomainObject;
import de.tarent.activity.domain.util.UniqueValidationAware;
import de.tarent.activity.exception.UniqueValidationException;



/**
 * Common DAO interface.
 * @param <EntityClass> EntityClass
 */
public interface BaseDAO<EntityClass extends DomainObject> {
	
	/**
	 * Find an Entity.
	 * 
	 * @param pk Entity Id
	 * @return EntityClass
	 */
	EntityClass find(Long pk);
	
	/**
	 * Update an Entity.
	 * 
	 * @param entity Entity
	 * @return EntityClass
	 */
	EntityClass update(EntityClass entity);
	
	/**
	 * Create an Entity.
	 * 
	 * @param entity entity 
	 * @return Job Id
	 */
	Long create(EntityClass entity);
	
	/**Delete an Entity.
	 * @param pk Entity id
	 */
	void delete(Long pk);

	/**
	 * Delete an Entity.
	 * @param entity Entity
	 */
	void delete(EntityClass entity);

	/**
	 * Checks for pre-existing entities which violate unique constraints defined
	 * by the entity.
	 * @see UniqueValidationAware
	 * @param entity the entity that is validated
	 * @throws UniqueValidationException when an unique constraint is violated
	 */
	void validateUniqueConstraint(EntityClass entity)
			throws UniqueValidationException;
}
