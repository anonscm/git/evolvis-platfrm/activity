/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;


import de.tarent.activity.dao.PosResourceMappingDAO;
import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.filter.PosResourceMappingFilter;

/**
 * Implementation class for PosResourceMappingDAO interface.
 * 
 */
@Stateless
@Local(PosResourceMappingDAO.class)
public class PosResourceMappingDAOImpl extends BaseDAOWithJPA<PosResourceMapping> implements
		PosResourceMappingDAO {

	@Override
	public List<PosResourceMapping> listPosResourceMapping(PosResourceMappingFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder(
				"select posResMapping from PosResourceMapping as posResMapping join fetch posResMapping.resource as res "
						+ "join fetch posResMapping.position as position join fetch position.job as job join fetch job.project as project");

		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	@Override
	public Long countPosResourceMapping(PosResourceMappingFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder(
				"select count(posResMapping) from PosResourceMapping as posResMapping join posResMapping.resource as res "
						+ "join posResMapping.position as position join position.job as job join job.project as project");

		qlFilterBuilder(filter, params, hql, false);
		return getSingleResult(hql, params);

	}


	
	private void qlFilterBuilder(PosResourceMappingFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("projectId", filter.getProjectId(), "project.pk=:projectId", params, hql, hasWhere);
		hasWhere = addCondition("jobId", filter.getJobId(), "job.pk=:jobId", params, hql, hasWhere);
		hasWhere = addCondition("positionId", filter.getPositionId(), "position.pk=:positionId", params, hql, hasWhere);

	}

	@Override
	public List<PosResourceMapping> getPosResourceMappingsByPosition(
			Long positionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("posId", positionId);
		return list("PosResourceMapping.GET_BY_POSITION", params);
	}

	@Override
	public PosResourceMapping getPosResourceMappingByPositionAndResource(Long positionId, Long resourceId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("posId", positionId);
		params.put("resourceId", resourceId);
		List<PosResourceMapping> list = list("PosResourceMapping.GET_BY_POSITION_AND_RESOURCE", params);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

}
