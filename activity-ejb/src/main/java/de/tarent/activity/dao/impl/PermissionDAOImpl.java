/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.PermissionDAO;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.filter.PermissionFilter;

@Stateless
@Local(PermissionDAO.class)
public class PermissionDAOImpl extends BaseDAOWithJPA<Permission> implements PermissionDAO {

	@Override
	public Long countPermissions() {
		StringBuilder hql = new StringBuilder("select count(perm) from Permission perm");

		return getSingleResult(hql, new HashMap<String, Object>());
	}

	@Override
	public List<Permission> listPermission(PermissionFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select distinct perm from Permission as perm");

		boolean hasWhere = false;
		if (filter.getRoleIds() != null && !filter.getRoleIds().isEmpty()) {
			hql.append(" left join perm.roles as roles");
			addCondition("roleIds", filter.getRoleIds(), "roles.pk in (:roleIds)", params, hql, false);
			hasWhere = true;
		} else if (filter.getResourceIds() != null && !filter.getResourceIds().isEmpty()) {
			hql.append(" join perm.resources as res");
			addCondition("resIds", filter.getResourceIds(), "res.pk in (:resIds)", params, hql, false);
			hasWhere = true;
		} 
		if (filter.getPermissionIds() != null && !filter.getPermissionIds().isEmpty()) {
			addCondition("permIds", filter.getPermissionIds(), "perm.pk in (:permIds)", params, hql, hasWhere);
		}

		return tableQuery(hql, filter, params);
	}

	@Override
	public Permission findByActionId(String actionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actionId", actionId);

		List<Permission> list = list("Permission.GET_BY_ACTION_ID", params);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public Permission findByResourceAndPermission(Long resId, String permId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceId", resId);
		params.put("actionId", permId);

		List<Permission> list = list("Permission.GET_BY_RESOURCE_AND_ACTION_ID", params);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
}
