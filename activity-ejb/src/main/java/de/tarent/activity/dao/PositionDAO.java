/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao;

import java.math.BigDecimal;
import java.util.List;

import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.PositionFilter;

/**
 * DAO interface for Position entity.
 *
 */
public interface PositionDAO extends BaseDAO<Position> {

	/**
	 * List Positions by Job id, status id or/and Resource id (Restrictions: position -
	 * resource mapping has active status, and was active in the last 24h)
	 *
	 * @param jobId Job id
	 * @param statusId statusId 
	 * 				
	 * @return List<Position>
	 */
	List<Position> findByJob(Long jobId, Long statusId, Long resId);

	/**
	 * List filtered positions.
	 *
	 * @param filter
	 *            PositionFilter
	 * @return List
	 */
	List<Position> listPosition(PositionFilter filter);

	/**
	 * Count filtered positions.
	 *
	 * @param filter
	 *            PositionFilter
	 * @return number of filtered positions
	 */
	Long countPosition(PositionFilter filter);

	/**
	 * @param filter
	 *            filter
	 * @return List<Position>
	 */
	List<Position> listMyPosition(PositionFilter filter);

	/**
	 * @param filter
	 *            filter
	 * @return countPosition
	 */
	Long countMyPosition(PositionFilter filter);

	/**
	 * Returns {@code true} if the position is deletable.
	 *
	 * @param positionId
	 * @return
	 */
	boolean isDeletable(Long positionId);

	/**
	 * Returns all recorded changes of the {@link Position} with the given id.
	 *
	 * @param positionId
	 * @return
	 */
	List<ChangeInfo> getChanges(Long positionId);

	/**
	 * Deletes a {@link Position}
	 *
	 * @param entity
	 *            the position to delete
	 */
	@Override
	void delete(Position entity);

	/**
	 * Returns the total hours spent on this position
	 * 
	 * @param positionId
	 * @return
	 */
	BigDecimal getTotalDaysSpent(Long positionId);
}
