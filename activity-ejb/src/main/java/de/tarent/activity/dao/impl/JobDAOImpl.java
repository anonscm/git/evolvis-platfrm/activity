/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import de.tarent.activity.dao.JobDAO;
import de.tarent.activity.dao.PositionDAO;
import de.tarent.activity.domain.AuditCompareField;
import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.JobFilter;
import de.tarent.activity.service.ProjectService;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * Implementation class for JobDAO interface.
 * 
 */
@Stateless
@Local(JobDAO.class)
public class JobDAOImpl extends BaseDAOWithJPA<Job> implements JobDAO {
    
    @EJB
    private PositionDAO positionDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(JobDAOImpl.class);

	@Override
	public Long countJob(JobFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder(
				"select count(job) FROM Job as job ");
		
		qlJoinBuilder(filter, hql);
		qlFilterBuilder(filter, params, hql, false);

		return getSingleResult(hql, params);
	}

	@Override
	public List<Job> findActiveJobsByResource(Long resId) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		Calendar cal = Calendar.getInstance();
		Date endDate = cal.getTime();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date startDate = cal.getTime();
		
		params.put("resId", resId);
		params.put("posStatusId", ProjectService.POS_RES_STATUS_ACTIVE);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
        // Todo: we should cleanup the jobstatus "Angebot" later right now it is simply ignored
		params.put("jobStatusId1", ProjectService.JOBSTATUS_OFFER);
		params.put("jobStatusId2", ProjectService.JOBSTATUS_ORDERED);
		
		//TODO: zwei lazy Listen in Job, aber positions werden eager gebraucht 
		return list("Job.GET_ACTIVE_BY_RESOURCE", params);
	}
	
	@Override
	public List<Job> findJobsByResource(Long resId) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("resId", resId);
		
		return list("Job.GET_BY_RESOURCE", params);
	}

	@Override
	public List<Job> listJob(JobFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("SELECT job FROM Job as job ");
		
		qlJoinBuilder(filter, hql);
		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	/**
	 * Adds join-statements to given hql string, dependent on given filter.
	 */
	private void qlJoinBuilder(JobFilter filter, StringBuilder hql){
	    if(filter.getViewBy() == JobFilter.VIEW_OWN){
            // need to join to position-resource-mapping
            hql.append(" join job.positions p join p.posResourceMappings prm ");
        }
	}
	
	/**
	 * Adds where-statements to given hql string, dependent on given filter criteria.
	 */
	private void qlFilterBuilder(JobFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("maitenance", filter.getMaintenance(), "job.isMaintenance=:maitenance", params, hql, hasWhere);
		hasWhere = addCondition("status", filter.getStatus(), "job.jobStatus.pk=:status", params, hql, hasWhere);
		hasWhere = addCondition("type", filter.getType(), "job.jobType.pk=:type", params, hql, hasWhere);
		hasWhere = addCondition("projectId", filter.getProjectId(), "job.project.pk=:projectId", params, hql, hasWhere);
		
		// select jobs by permission of current user
		switch (filter.getViewBy()) {
            case JobFilter.VIEW_ALL:
                // nothing to do
                break;
            case JobFilter.VIEW_BY_DIVISION:
                // TODO not yet implemented
                break;
            case JobFilter.VIEW_BY_MANAGER:
                // get only jobs for which current user is manager or is manager of the overall project
                hasWhere = compositeOrCondition("managerId", filter.getResourceId(), "(job.manager.pk=:managerId or job.project.fkResource=:managerId)", params, hql, hasWhere);
                break;
            case JobFilter.VIEW_OWN:
                // get only jobs for which current user is assigned by job-positions
                hasWhere = addCondition("resourceId", filter.getResourceId(), "prm.resource.pk=:resourceId", params, hql, hasWhere);
                break;
            default:
                break;
        }
	}

	@Override
	public List<Job> findAll() {
		return list("Job.GET_ALL");
	}
	
	@Override
	public List<Job> findNotMaintenanceJobs() {
		return list("Job.GET_NOT_MAINTENANCE");
	}

	@Override
	public List<Job> findByProject(Long projectId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("projectId", projectId);
		return list("Job.GET_BY_PROJECT", params);
	}

	@Override
	public List<Job> getOrderedList() {
		return list("Job.GET_ORDERED");
	}

    @Override
    public boolean isDeletable(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
        Long count;
        count = getSingleResult("Activity.GET_COUNT_BY_JOB", params);
        if (count > 0){
            return false;
        }
        count = getSingleResult("Invoice.GET_COUNT_BY_JOB", params);
        if (count > 0){
            return false;
        }
        count = getSingleResult("Cost.GET_COUNT_BY_JOB", params);
        return count <= 0;
    }

    @Override
    public void delete(Job job) {
        if (job == null) {
            throw new RuntimeException("Trying to remove a null entity "
                    + getEntityClass());
        }

        for (Position position : job.getPositions()){
            positionDAO.delete(position);
        }

        entityManager.remove(job);
    }
    
    @Override
    public List<ChangeInfo> getChanges(Long jobPk) {
        // get history
        AuditReader reader = AuditReaderFactory.get(entityManager);
        AuditQuery query = reader.createQuery().forRevisionsOfEntity(
                Job.class, false, true);
        query = query.add(AuditEntity.id().eq(jobPk));
        @SuppressWarnings("unchecked")
        List<Object[]> revisions = query.getResultList();

        if (revisions != null && revisions.size() >= 2) { // diff(s) possible
            List<ChangeInfo> changes = new ArrayList<ChangeInfo>(
                    revisions.size() - 1);
            Job pos1 = (Job) revisions.get(0)[0];
            Field[] fields = Job.class.getDeclaredFields();
            for (int i = 1; i < revisions.size(); i++) {
                Job pos2 = (Job) revisions.get(i)[0];
                DefaultRevisionEntity dre = (DefaultRevisionEntity) revisions.get(i)[1];
                ChangeInfo info = new ChangeInfo(dre.getRevisionDate(), pos2.getUpdUser());
                try {
                    if (fields != null)
                        for (Field field : fields)
                            if (field
                                    .isAnnotationPresent(AuditCompareField.class)) {
                                field.setAccessible(true);
                                Object obj1 = field.get(pos1);
                                Object obj2 = field.get(pos2);
                                if ((obj1 == null && obj2 != null)
                                        || (obj1 != null && !obj1.equals(obj2))) {
                                    if (obj1 != null && obj1 instanceof BigDecimal
                                            && // special handling needed
                                               // because equals() of BigDecimal
                                               // is false if scale is not equal
                                            obj2 != null
                                            && ((BigDecimal) obj1)
                                                    .compareTo((BigDecimal) obj2) == 0){
                                        continue;
                                    }
                                    info.addChangeField(field.getName(), obj1, obj2);
                                }
                                field.setAccessible(false);
                            }
                } catch (IllegalArgumentException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                } catch (IllegalAccessException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                }
                changes.add(info);
                pos1 = pos2;
            }
            Collections.reverse(changes);
            return changes;
        }
        return Collections.emptyList();
    }

	@Override
	public BigDecimal getTotalDaysSpent(Long jobId) {
		
		BigDecimal totalDays = new BigDecimal(0);
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
        
        BigDecimal hours = getSingleResult("Job.SUM_TOTAL_HOURS_SPENT", params);
        
        if(hours != null){
        	totalDays = new BigDecimal(Math.round(hours.doubleValue() / 2d) / 4d);
        }
        
        return totalDays;
	}

	@Override
	public BigDecimal getTotalExpectedWork(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_EXPECTED_WORK", params);
	}
	
	@Override
	public BigDecimal getTotalCommunicatedWork(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_COMMUNICATED_WORK", params);
	}
	
	@Override
	public BigDecimal getTotalServiceCosts(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_SERVICE_COSTS", params);
	}
	
	@Override
	public BigDecimal getTotalFixedPrice(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_FIXED_PRICE", params);
	}
	
	@Override
	public BigDecimal getTotalInvoice(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_INVOICE", params);
	}
	
	@Override
	public BigDecimal getTotalInvoicePaid(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_INVOICE_PAID", params);
	}
	
	@Override
	public BigDecimal getTotalOtherCosts(Long jobId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jobId", jobId);
		return (BigDecimal) getSingleResult("Job.SUM_OTHER_COSTS", params);
	}
	
}