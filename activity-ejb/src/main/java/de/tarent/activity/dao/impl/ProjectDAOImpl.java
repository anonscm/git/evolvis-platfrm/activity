/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import de.tarent.activity.dao.JobDAO;
import de.tarent.activity.dao.ProjectDAO;
import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.ProjectFilter;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * Implementation class for ProjectDAO interface.
 * 
 */
@Stateless
@Local(ProjectDAO.class)
public class ProjectDAOImpl extends BaseDAOWithJPA<Project> implements ProjectDAO {

    private static final int PROJECT_TYPE_INTERN = 3;
    private static final int PROJECT_TYPE_EXTERN = 1;

    @EJB
    private JobDAO jobDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectDAOImpl.class);
    
	@Override
	public List<Project> findAll() {
		return list("Project.GET_ALL");
	}

	@Override
	public List<Project> findByResource(Long resId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resId", resId);
		return list("Project.GET_BY_RESOURCE", params);

	}

	@Override
	public List<Project> listProjects(ProjectFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("SELECT distinct project FROM Project project "
				+ "left join project.jobs job " + "left join job.jobStatus status "
				+ "left join job.jobType type " + "left join job.positions pos "
				+ "left join pos.posResourceMappings prm left join prm.posResourceStatus mappStatus ");
		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	@Override
	public Long countProject(ProjectFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("SELECT count(distinct project) FROM Project project "
				+ "left join project.jobs job " + "left join job.jobStatus status "
				+ "left join job.jobType type " + "left join job.positions pos "
				+ "left join pos.posResourceMappings prm left join prm.posResourceStatus mappStatus ");
		qlFilterBuilder(filter, params, hql, false);

		return getSingleResult(hql, params);
	}

	private void qlFilterBuilder(ProjectFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("posResStatusId", filter.getPosResStatus(),
				" mappStatus.pk=:posResStatusId ", params, hql, hasWhere);
		hasWhere = addCondition("statusId", filter.getStatus(), " status.pk=:statusId and mappStatus.pk = 1", params, hql, hasWhere);
		
		if (filter.getType() != null) {
            if( filter.getType() == PROJECT_TYPE_EXTERN ){
                hasWhere = addCondition(" (isProjectExternal(project.pk) = 1) ", hql, hasWhere);
            }else if( filter.getType() == PROJECT_TYPE_INTERN ){
                hasWhere = addCondition(" (isProjectExternal(project.pk) = 0) ", hql, hasWhere);
            }
		}
		
		if(filter.getViewBy() == ProjectFilter.VIEW_BY_MANAGER){
			hasWhere = addCondition("resourceId", filter.getResourceId(), " project.fkResource=:resourceId ", params, hql,
					hasWhere);
		} else if(filter.getViewBy() == ProjectFilter.VIEW_BY_DIVISION){
            throw new RuntimeException("Division Manager functionality is not implemented yet.");
		} else {
			hasWhere = addCondition("resourceId", filter.getResourceId(), " prm.resource.pk=:resourceId ", params, hql,
					hasWhere);
		}
		
		hasWhere = addCondition("customerId", filter.getCustomerId(), " project.customer.pk=:customerId", params, hql,
				hasWhere);
	}

	@Override
	public List<Project> findByCustomer(Long customerId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerId", customerId);
		return list("Project.GET_BY_CUSTOMER", params);
	}

	@Override
	public List<Project> findByResponsibleResource(Long resId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resId", resId);
		return list("Project.GET_BY_RESPONSIBLE_RESOURCE", params);
	}
	
	@Override
	public List<Project> findAllActive() {
		return list("Project.GET_ALL_ACTIVE");
	}
	
    @Override
    public boolean isDeletable(Long projectId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("projectId", projectId);
        Long count;
        count = getSingleResult("Activity.GET_COUNT_BY_PROJECT", params);
        if (count > 0){
            return false;
        }
        count = getSingleResult("Invoice.GET_COUNT_BY_PROJECT", params);
        if (count > 0){
        	return false;
        }
        count = getSingleResult("Overtime.GET_COUNT_BY_PROJECT", params);
        if (count > 0){
        	return false;
        }
        
        return true;
    }

    @Override
    public void delete(Project project) {
        if (project == null){
            throw new RuntimeException("Trying to remove a null entity "
                    + getEntityClass());
        }

        for (Job job : project.getJobs()){
            jobDAO.delete(job);
        }
        
        for (ProjectSites site : project.getProjectSites()){
            entityManager.remove(site);
        }
        
        for (ProjectFieldsMapping pfmap : project.getProjectFieldsMappings()){
            entityManager.remove(pfmap);
        }

        entityManager.remove(project);
    }

    @Override
    public List<ChangeInfo> getChanges(Long id) {
        // get history
        AuditReader reader = AuditReaderFactory.get(entityManager);
        AuditQuery query = reader.createQuery().forRevisionsOfEntity(
                Project.class, false, true);
        query = query.add(AuditEntity.id().eq(id));
        @SuppressWarnings("unchecked")
        List<Object[]> revisions = query.getResultList();

        if (revisions != null && revisions.size() >= 2) { // diff(s) possible
            List<ChangeInfo> changes = new ArrayList<ChangeInfo>(
                    revisions.size() - 1);
            Project pos1 = (Project) revisions.get(0)[0];
            Field[] fields = Project.class.getDeclaredFields();
            for (int i = 1; i < revisions.size(); i++) {
                Project pos2 = (Project) revisions.get(i)[0];
                DefaultRevisionEntity dre = (DefaultRevisionEntity) revisions.get(i)[1];
                ChangeInfo info = new ChangeInfo(dre.getRevisionDate(), pos2.getUpdUser());
                try {
                    if (fields != null)
                        for (Field field : fields)
                            if (field
                                    .isAnnotationPresent(AuditCompareField.class)) {
                                field.setAccessible(true);
                                Object obj1 = field.get(pos1);
                                Object obj2 = field.get(pos2);
                                if ((obj1 == null && obj2 != null)
                                        || (obj1 != null && !obj1.equals(obj2))) {
                                    if (obj1 != null && obj1 instanceof BigDecimal
                                            && // special handling needed
                                               // because equals() of BigDecimal
                                               // is false if scale is not equal
                                            obj2 != null
                                            && ((BigDecimal) obj1)
                                                    .compareTo((BigDecimal) obj2) == 0)
                                        continue;
                                    info.addChangeField(field.getName(), obj1, obj2);
                                }
                                field.setAccessible(false);
                            }
                } catch (IllegalArgumentException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                } catch (IllegalAccessException e) {
                    LOGGER.error("IllegalArgumentException " + e.getMessage());
                    continue;
                }
                changes.add(info);
                pos1 = pos2;
            }
            Collections.reverse(changes);
            return changes;
        }
        return Collections.emptyList();
    }
}
