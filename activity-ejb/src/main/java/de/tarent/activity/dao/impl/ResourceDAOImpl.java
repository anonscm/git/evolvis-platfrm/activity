/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.ResourceDAO;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ResourceFilter;

/**
 * Implementation class for ResourceDAO interface.
 * 
 */
@Stateless
@Local(ResourceDAO.class)
public class ResourceDAOImpl extends BaseDAOWithJPA<Resource> implements ResourceDAO {

	@Override
	public List<Resource> findAll() {
		return list("Resource.GET_ALL");
	}

	@Override
	public Resource loadResource(String username) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		List<Resource> list = list("Resource.GET_RESOURCE_BY_USERNAME", params);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<Resource> listResource(ResourceFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select distinct resource from Resource as resource");

		boolean hasWhere = false;
        if (filter.getRoleIds() != null && !filter.getRoleIds().isEmpty()) {
            hql.append(" join resource.roles as roles");
            addCondition("roleIds", filter.getRoleIds(), "roles.pk in (:roleIds)", params, hql, hasWhere);
            hasWhere = true;
        }
        if (filter.getPositionId() != null){
        	hql.append(" join fetch resource.posResourceMappings as mapp");
        	addCondition("positionId", filter.getPositionId(), "mapp.position.id=:positionId", params, hql, hasWhere);
        	hasWhere = true;
        }
        else if (filter.getJobId() != null){
        	hql.append(" join resource.posResourceMappings as mapp join mapp.posResourceStatus as mappStatus");
        	hql.append(" join mapp.position as pos join pos.job as job");
        	addCondition("jobId", filter.getJobId(), "job.id=:jobId", params, hql, hasWhere);
        	hasWhere = true;
        }

        qlFilterBuilder(filter, params, hql, hasWhere);
        
		return tableQuery(hql, filter, params);

	}

	private void qlFilterBuilder(ResourceFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("active", filter.getActive(), "resource.active=:active", params, hql, hasWhere);
		hasWhere = addCondition("resIds", filter.getResourceIds(), "resource.pk in (:resIds)", params, hql, hasWhere);
	}

	@Override
	public Long countResource(ResourceFilter filter) {
        filter.setPageItems(null);
        filter.setOffset(null);
        List<Resource> resources = listResource(filter);

		return new Long(resources.size());
	}

	@Override
	public List<Resource> getActiveResources() {
		return list("Resource.GET_ACTIVE_RESOURCES");
	}

	@Override
	public List<Resource> listResourceFromProject(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("projectId", id);
		return list("Resource.GET_RESOURCE_BY_PROJECT", params);
	}

	@Override
	public List<Resource> getByPosition(Long selectedPositionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("positionId", selectedPositionId);
		return list("Resource.GET_RESOURCE_BY_POSITION", params);
	}

	@Override
	public List<Resource> getAllProjectManager() {
		Map<String, Object> params = new HashMap<String, Object>();
		return list("Resource.GET_PROJECT_MANAGER", params);
	}

	@Override
	public List<Resource> getByPermission(Permission permission) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actionId", permission.getActionId());
		
		return list("Resource.GET_RESOURCE_BY_PERMISSION", params);
	}

}
