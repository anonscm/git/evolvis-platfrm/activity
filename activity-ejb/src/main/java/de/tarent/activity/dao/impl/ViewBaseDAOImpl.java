/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.tarent.activity.domain.filter.BaseFilter;

public class ViewBaseDAOImpl<EntityClass> {

	private Class<EntityClass> entityClass;

	@PersistenceContext(unitName = "activityPU")
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Constructor.
	 */
	@SuppressWarnings("unchecked")
	public ViewBaseDAOImpl() {
		this.entityClass = (Class<EntityClass>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	protected final Class<EntityClass> getEntityClass() {
		return entityClass;
	}

	protected <V> List<V> list(String query) {
		return list(query, (Map<String, ?>) null);
	}

	protected <V> List<V> list(String query, Map<String, ?> params) {
		return list(query, Integer.MAX_VALUE, Integer.MIN_VALUE, params);
	}

	protected <V> List<V> list(String query, int startPosition, int maxResult, Map<String, ?> params) {
		if (query == null) {
			throw new RuntimeException("Query is null");
		}

		Query ejbqlQuery = entityManager.createNamedQuery(query);

		if (startPosition != Integer.MAX_VALUE) {
			ejbqlQuery.setFirstResult(startPosition);
		}
		if (maxResult != Integer.MIN_VALUE) {
			ejbqlQuery.setMaxResults(maxResult);
		}

		if (params != null) {
			for (String key : params.keySet()) {
				Object value = params.get(key);

				ejbqlQuery.setParameter(key, value);
			}
		}

		return (List<V>) ejbqlQuery.getResultList();
	}

	protected <T> T getSingleResult(StringBuilder hql, Map<String, Object> params) {
		Query query = entityManager.createQuery(hql.toString());

		addQueryParams(query, params);

		return (T) query.getSingleResult();
	}

	public List<EntityClass> tableQuery(StringBuilder hql, BaseFilter filter, Map<String, Object> params) {

		if (filter.getSortColumn() != null && filter.getSortColumn().length() > 0) {
			hql.append(" order by " + filter.getSortColumn());

			if (filter.getSortOrder().equals("asc")) {
				hql.append(" asc");
			} else {
				hql.append(" desc");
			}

			// when ordering on a column which has identical values, the result
			// is unpredictable, causing pagination problems,
			// therefore some rows are never displayed
			// FIX: add another sort parameter to enforce an unique order
			if (getSecondSortColumn() != null && getSecondSortColumn().length() > 0
					&& !getSecondSortColumn().equals(filter.getSortColumn())) {
				hql.append(", " + getSecondSortColumn() + " " + filter.getSortOrder());
			}
		}

		Query query = entityManager.createQuery(hql.toString());

		addQueryParams(query, params);

		if (filter.getOffset() != null) {
			query.setFirstResult(filter.getOffset().intValue());
		}
		if (filter.getPageItems() != null) {
			query.setMaxResults(filter.getPageItems().intValue());
		}

		return query.getResultList();
	}

	private void addQueryParams(Query query, Map<String, Object> params) {
		for (String param : params.keySet()) {
			query.setParameter(param, params.get(param));
		}
	}

	protected Boolean addCondition(String filterKey, Object filterValue, String condition, Map<String, Object> params,
			StringBuilder hql, Boolean hasWhere) {
		if (filterValue != null && (!(filterValue instanceof String) || ((String) filterValue).length() != 0)) {
			if (hasWhere) {
				hql.append(" and ");
			} else {
				hql.append(" where ");
				hasWhere = true;
			}

			hql.append(condition);

			params.put(filterKey, filterValue);
		}
		return hasWhere;
	}

	protected Boolean addCondition(String condition, StringBuilder hql, Boolean hasWhere) {
		if (hasWhere) {
			hql.append(" and ");
		} else {
			hql.append(" where ");
			hasWhere = true;
		}
		hql.append(condition);
		return hasWhere;
	}

	protected String getSecondSortColumn() {
		return null;
	}

}
