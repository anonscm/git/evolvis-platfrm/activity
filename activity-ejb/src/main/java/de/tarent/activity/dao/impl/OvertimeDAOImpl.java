/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;

import de.tarent.activity.dao.OvertimeDAO;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.filter.OvertimeFilter;

/**
 * Implementation class for OvertimeDAO interface.
 *
 */
@Stateless
@Local(OvertimeDAO.class)
public class OvertimeDAOImpl extends BaseDAOWithJPA<Overtime> implements OvertimeDAO {

	@Override
	public Long countOvertime(OvertimeFilter filter) {

		Map<String, Object> params = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder("select count(overtime) from Overtime as overtime "
				+ "left outer join overtime.project as prj left outer join overtime.resource as resource");

		qlFilterBuilder(filter, params, hql, false);

		return getSingleResult(hql, params);
	}

	@Override
	public List<Overtime> listOvertime(OvertimeFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select overtime from Overtime as overtime "
				+ "left outer join fetch overtime.project as prj left outer join fetch overtime.resource as resource");

		qlFilterBuilder(filter, params, hql, false);

		return tableQuery(hql, filter, params);
	}

	@Override
	public BigDecimal getSumOvertimeHours(OvertimeFilter filter) {
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder hql = new StringBuilder("select sum(overtime.hours) from Overtime as overtime "
				+ "left outer join overtime.project as prj left outer join overtime.resource as resource");

		qlFilterBuilder(filter, params, hql, false);

		BigDecimal ret = getSingleResult(hql, params);
		return ret!=null ? ret : BigDecimal.ZERO;
	}

	private void qlFilterBuilder(OvertimeFilter filter, Map<String, Object> params, StringBuilder hql, Boolean hasWhere) {
		hasWhere = addCondition("resourceId", filter.getResourceId(), "resource.pk=:resourceId", params, hql, hasWhere);
		hasWhere = addCondition("prjId", filter.getProjectId(), "prj.pk=:prjId", params, hql, hasWhere);
		hasWhere = addCondition("startDate", filter.getStartDate(), "overtime.date>=:startDate", params, hql, hasWhere);
		hasWhere = addCondition("endDate", filter.getEndDate(), "overtime.date<=:endDate", params, hql, hasWhere);
		if (filter.getType() != null && filter.getType() != 0) {
			if (filter.getType() > 0) {
				hasWhere = addCondition("overtime.hours > 0", hql, hasWhere);
			} else {
				hasWhere = addCondition("overtime.hours < 0", hql, hasWhere);
			}
		}
	}

	@Override
	public BigDecimal getAllOvertimeHours(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resId", id);
		BigDecimal hours = (BigDecimal) list("Overtime.GET_SUM_HOURS", params).get(0);
		return hours != null ? hours : BigDecimal.ZERO;
	}

}
