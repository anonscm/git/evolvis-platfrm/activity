/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.interceptors;

import javax.interceptor.InvocationContext;

/**
 * A class providing helper methods for interceptors.
 * 
 * @author Sebastian Gerhards, tarent solutions GmbH
 * @author Timo Kanera, tarent solutions GmbH
 * @author Timo Pick, tarent solutions GmbH
 */
public class InterceptorHelper {

	/**
	 * Gets the name of the class and the name of the method
	 * 
	 * @param invocation
	 *            the invocation context
	 * 
	 * @return class and method name
	 */
	public static String getClassAndMethodName(InvocationContext invocation, boolean includeParameters) {
		if (invocation == null || invocation.getMethod() == null) {
			return "n/a";
		}
		String methodName = invocation.getMethod().getName();
		StringBuilder sb = new StringBuilder();
		Class<?> declaringClass = invocation.getMethod().getDeclaringClass();
		if (declaringClass != null) {
			sb.append(declaringClass.getName());
			sb.append(".");
		}
		sb.append(methodName);
		if (includeParameters) {
			sb.append("(");
			sb.append(join(invocation.getParameters(), ", "));
			sb.append(")");
		}
		return sb.toString();
	}

	public static String join(Object[] inputArray, String glueString) {
		String output = "";
		if (inputArray.length > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(inputArray[0]);
			for (int i = 1; i < inputArray.length; i++) {
				sb.append(glueString);
				sb.append(inputArray[i]);
			}
			output = sb.toString();
		}
		return output;
	}

}
