/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import de.tarent.activity.dao.*;
import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.*;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for ResourceService.
 *
 */
@Stateless(name = "ResourceService")
@Remote(value = ResourceService.class)
public class ResourceServiceImpl implements ResourceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceService.class);

	@EJB
	private ResourceDAO resourceDAO;
	
	@EJB
	private PermissionDAO permissionDAO;

	@EJB
	private DeleteRequestDAO deleteRequestDAO;

	@EJB
	private ResourceTypeDAO resourceTypeDAO;
	
	@EJB 
	private PosResourceStatusDAO posResourceStatusDAO;

	@EJB
	private BranchOfficeDAO branchOfficeDAO;

	@EJB
	private EmploymentDAO employmentDAO;

	@EJB
	private SkillsDAO skillsDAO;

	@EJB
	private SkillsDefDAO skillsDefDAO;
	
	@EJB
	private FunctionTypeDAO functionTypeDAO;
	
	@EJB
	private ResourceFTypeMappingDAO resourceFTypeMappingDAO;
	
	@EJB
	private VresActiveProjectMappingDAO vrMappingDAO;
	

	@Override
//	@PermitAll
	public List<Resource> getResourceList() {
		LOGGER.debug("Number of resources: " + resourceDAO.findAll().size());
		return resourceDAO.findAll();
	}

	@Override
//	@PermitAll
	public Resource loadResource(String username) {
		return resourceDAO.loadResource(username);
	}

	@Override
//	@RolesAllowed({"user","resourceAdmin","projectAdmin","resourceController"})
	public Resource getResource(Long resourceId, boolean fetchRoles, boolean fetchPermissions) {
		Resource resource = resourceDAO.find(resourceId);
	    if(resource != null){
	    	if(fetchRoles && resource.getRoles() != null){
	    		// lazy fetch roles
	    		resource.getRoles().size();
	    	}
	    	if(fetchPermissions && resource.getPermissions() != null){
	    		// lazy fetch permission
	    		resource.getPermissions().size();
	    	}
	    }
	    return resource;
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public List<Resource> managerList(Long resourceId) {
		return new ArrayList<Resource>();
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public FilterResult<Resource> searchResource(ResourceFilter filter) {
		List<Resource> resources = resourceDAO.listResource(filter);
		Long count = resourceDAO.countResource(filter);
		return new FilterResult<Resource>(resources, count);
	}

	@Override
//	@RolesAllowed({"user", "resourceAdmin","jobAdmin"})
	public List<PosResourceStatus> posResourceStatusList() {
		return posResourceStatusDAO.findAll();
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public Long addResource(Resource resource) throws UniqueValidationException {
		resourceDAO.validateUniqueConstraint(resource);
        convertResourcePasswordToMDFHashIfGiven(resource);
		return resourceDAO.create(resource);
	}

    private void convertResourcePasswordToMDFHashIfGiven(Resource resource) {
        if(resource.getPassword() != null){
            resource.setPassword(getMD5Hash(resource.getPassword()));
        }
    }

    /**
	 * Gibt das übergebene Passwort als MD5-Hashwert zurück.
	 * Copied from old activity.
	 * 
	 * @param password Passwort
	 * @return MD5 Wert
	 */
//	@RolesAllowed({"resourceAdmin"})
	public static String getMD5Hash(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte digest[] = md.digest(password.getBytes());
			StringBuffer buffer = new StringBuffer(32);
			for (int i = 0; i < digest.length; i++) {
				int b = digest[i] & 255;
				if (b < 16) {
					buffer.append('0');
				}
				buffer.append(Integer.toHexString(b));
			}
			return buffer.toString();
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("", e);
			return null;
		}
	}
	
	@Override
//	@RolesAllowed({"user","resourceAdmin","resourceController"})
	public void updateResource(Resource resource) throws UniqueValidationException {
		resourceDAO.validateUniqueConstraint(resource);
        if( resource.getPassword() != null ){
		    resource.setPassword(getMD5Hash(resource.getPassword()));
        }
		resourceDAO.update(resource);
	}

	@Override
	public Skills getSkills(Long id) {
		return skillsDAO.find(id);
	}

	@Override
	public FilterResult<Skills> searchSkills(SkillsFilter filter) {
		List<Skills> skills = skillsDAO.listSkills(filter);
		Long count = skillsDAO.countSkills(filter);
		return new FilterResult<Skills>(skills, count);
	}
	
	@Override
//	@RolesAllowed({"resourceAdmin"})
	public FilterResult<Skills> searchAllSkills(AllSkillsFilter filter) {
		List<Skills> skills = skillsDAO.listAllSkills(filter);
		Long count = skillsDAO.countAllSkills(filter);
		return new FilterResult<Skills>(skills, count);
	}

	@Override
	public List<SkillsDef> skillsDefList() {
		return skillsDefDAO.findAll();
	}

	@Override
	public Long addSkillDef(SkillsDef skilldef) {
		return skillsDefDAO.create(skilldef);

	}

	@Override
	public SkillsDef getSkillsDefByName(String name) {
		List<SkillsDef> skillsDef = skillsDefDAO.getSkillsDefByName(name);
		if (skillsDef.size() > 0) {
			return skillsDef.get(0);
		}
		return null;
	}

	@Override
	public Long addSkill(Skills skill) {
		return skillsDAO.create(skill);
	}

	@Override
	public void updateSkill(Skills skill) {
		skillsDAO.update(skill);
	}

	@Override
	public void deleteSkill(Long skillId) {
		skillsDAO.delete(skillId);
	}

	@Override
//	@PermitAll
	public FilterResult<DeleteRequest> searchRequest(DeleteRequestFilter filter) {
		List<DeleteRequest> list = deleteRequestDAO.listDeleteRequests(filter);
		Long count = deleteRequestDAO.countDeleteRequests(filter);
		return new FilterResult<DeleteRequest>(list, count);
	}

	@Override
//	@PermitAll
	public void updateRequest(DeleteRequest request) {
		deleteRequestDAO.update(request);
	}

	@Override
//	@PermitAll
	public DeleteRequest getRequest(Long requestId) {
		return deleteRequestDAO.find(requestId);
	}

	@Override
//	@RolesAllowed({"superAdmin"})
	public void deleteRequest(Long requestId) {
		deleteRequestDAO.delete(requestId);
	}

	@Override
//	@RolesAllowed({"costController","resourceController"})
	public Long addRequest(DeleteRequest request) {
		if (deleteRequestDAO.getRequestByTypeAndId(request.getType(), request.getId()) == null) {
			return deleteRequestDAO.create(request);
		} else {
			return null;
		}
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public List<ResourceType> getResourceTypeList() {
		return resourceTypeDAO.findAll();
	}

	@Override
//	@PermitAll
	public List<BranchOffice> getBranchOfficeList() {
		return branchOfficeDAO.findAll();
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public List<Employment> getEmploymentList() {
		return employmentDAO.findAll();
	}

	@Override
//	@RolesAllowed({"jobAdmin","resourceAdmin"})
	public List<Resource> listActiveResources() {
		return resourceDAO.getActiveResources();
		
	}

	@Override
//	@RolesAllowed({"user", "projectAdmin"})
	public List<Resource> getResourceFromProject(Long projectPk) {
		return resourceDAO.listResourceFromProject(projectPk);
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public ResourceType getResourceType(Long resourceTypeId) {
		return resourceTypeDAO.find(resourceTypeId);
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public BranchOffice getBranchOffice(Long branchOfficeId) {
		return branchOfficeDAO.find(branchOfficeId);
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public Employment getEmployment(Long employmentId) {
		return employmentDAO.find(employmentId);
	}

	@Override
//	@RolesAllowed({"costController","resourceController"})
	public List<Resource> getByPosition(Long selectedPositionId) {
		return resourceDAO.getByPosition(selectedPositionId);
	}

	@Override
	public Skills getUserSkillBySkillDef(Long pk, Long skillDefId) {
		return skillsDAO.getUserSkillBySkillDef(pk, skillDefId);
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public List<FunctionType> getFunctionTypeList() {
		return functionTypeDAO.findAll(); 
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public List<ResourceFTypeMapping> getResourceFTypeMapping(Long resourceId) {
		return resourceFTypeMappingDAO.getResourceFTypeMapping(resourceId);
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public void deleteResourceFTypeMappingByResource(Long resourceId) {
		resourceFTypeMappingDAO.deleteByResource(resourceId);		
	}

	@Override
//	@RolesAllowed({"resourceAdmin"})
	public Long addResourceFTypeMapping(ResourceFTypeMapping resourceFunction) {
		return resourceFTypeMappingDAO.create(resourceFunction);
		
	}

//	@Override
//	@PermitAll
//	public List<Resource> getByRights(Long rights) {
//		return resourceDAO.getByRights(rights);
//	}

	@Override
	public List<Resource> getAllProjectManager() {
		return resourceDAO.getAllProjectManager();
	}

	@Override
	public DeleteRequest getRequestByTypeAndId(String entityType, Long entityId) {
		return deleteRequestDAO.getRequestByTypeAndId(entityType, entityId);
	}

	@Override
	public List<VresActiveProjectMapping> getResourceProjectByStatus(Long resourceId, Long status) {
		return vrMappingDAO.getByResourceAndStatus(resourceId, status);
	}

	@Override
	public void deleteRessource(Long resourceId) {
		resourceDAO.delete(resourceId);
		
	}

}
