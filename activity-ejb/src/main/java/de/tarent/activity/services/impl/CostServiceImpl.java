/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import de.tarent.activity.dao.CostDAO;
import de.tarent.activity.dao.CostTypeDAO;
import de.tarent.activity.dao.InvoiceDAO;
import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.CostType;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.service.CostService;

/**
 * Implementation for CostService.
 * 
 */
@Stateless(name = "CostService")
@Remote(value = CostService.class)
//@RolesAllowed({ "user" })
public class CostServiceImpl implements CostService {

	@EJB
	private CostDAO costDao;

	@EJB
	private InvoiceDAO invoiceDAO;

	@EJB
	private CostTypeDAO costTypeDAO;

//	@EJB
//	private ProjectInvoiceDAO invoiceDAO;

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "costController" })
	public Long addCost(Cost cost) {
		return costDao.create(cost);
	}

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "costController", "jobAdmin" })
	public Long addInvoice(Invoice invoice) {
		return invoiceDAO.create(invoice);
	}

	@Override
//	@RolesAllowed({ "costController" })
	public List<CostType> getCostTypeList() {
		return costTypeDAO.findAll();
	}

	@Override
//	@RolesAllowed({ "costController" })
	public Cost getCost(Long costId) {
		return costDao.find(costId);
	}

	@Override
	public Invoice getInvoice(Long id) {
		return invoiceDAO.find(id);
	}

	@Override
//	@RolesAllowed({ "user", "jobAdmin" })
	public List<Invoice> getInvoicesByJob(Long jobId) {
		return invoiceDAO.findByJob(jobId);
	}

	@Override
//	@RolesAllowed({ "costController" })
	public FilterResult<Cost> searchCost(CostFilter filter) {
		List<Cost> list = costDao.listCost(filter);
		Long count = costDao.countCost(filter);
		return new FilterResult<Cost>(list, count);
	}

	@Override
//	@RolesAllowed({ "costController", "jobAdmin" })
	public FilterResult<Invoice> searchInvoice(InvoiceFilter invoiceFilter) {
		List<Invoice> invoices = new ArrayList<Invoice>();
		invoices = invoiceDAO.listInvoice(invoiceFilter);
		Long count = invoiceDAO.countInvoice(invoiceFilter);
		return new FilterResult<Invoice>(invoices, count);
	}

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "costController" })
	public void updateCost(Cost cost) {
		costDao.update(cost);
	}

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "costController", "jobAdmin" })
	public void updateInvoice(Invoice invoice) {
		invoiceDAO.update(invoice);
	}

	@Override
//	@RolesAllowed({ "projectAdmin" })
	public List<Invoice> getProjectInvoices(Long projectId) {
		return invoiceDAO.invoicesByProject(projectId);
	}

	@Override
	public Invoice getProjectInvoice(Long id) {
		return invoiceDAO.find(id);
	}

	@Override
//	@RolesAllowed({ "costController", "jobAdmin" })
	public FilterResult<Invoice> searchProjectInvoice(InvoiceFilter invoiceFilter) {
		List<Invoice> invoices = invoiceDAO.listInvoice(invoiceFilter);
		Long count = invoiceDAO.countInvoice(invoiceFilter);
		return new FilterResult<Invoice>(invoices, count);
	}

	@Override
	public void deleteInvoice(Long id) {
		invoiceDAO.delete(id);		
	}

}
