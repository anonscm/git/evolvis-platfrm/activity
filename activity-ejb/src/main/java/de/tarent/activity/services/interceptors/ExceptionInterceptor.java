/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.exception.ServiceException;

/**
 * ExceptionInterceptor.
 *
 */
public class ExceptionInterceptor {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionInterceptor.class);

	/**
	 * @param ctx ctx
	 * @return Object
	 * @throws Exception Exception
	 */
	@AroundInvoke
	public Object handleException(InvocationContext ctx) throws Exception {
		try {
			LOGGER.debug("Intercepting ejb invocation " + ctx.getTarget() + ", method " + ctx.getMethod());
			return ctx.proceed();
		} catch (Exception e) {
			LOGGER.error("Exception rethrowing in " + ctx.getTarget() + ", method " + ctx.getMethod() + ", exception "
					+ e.getClass().getName() + ", message: " + e.getMessage());
			if (e instanceof ServiceException || e instanceof PermissionException) {
				//don't need to wrap known Exceptions
				throw e;
			} else {
				// wrap original exception in known ServiceException
				ServiceException se = new ServiceException(e.getMessage());
				se.fillInStackTrace();
				throw se;
			}
		}
	}
}
