/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.dao.ReportDAO;
import de.tarent.activity.domain.Report;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ReportService;


/**
 * Implementation for ReportService.
 *
 */
@Stateless(name = "ReportService")
@Remote(value = ReportService.class)
//@RolesAllowed({"reportAdmin"})
public class ReportServiceImpl implements ReportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

	@EJB
	private ReportDAO reportDAO;

	@Override
	@TransactionAttribute
	public Long addReport(Report report) throws UniqueValidationException {
		reportDAO.validateUniqueConstraint(report);
		
		return reportDAO.create(report);
	}

	@Override
	public Report getReport(Long id) {
		Report report = reportDAO.find(id);
		LOGGER.debug("Report name with id " + id + " is:" + report.getName());
		return report;
	}

	@Override
	@TransactionAttribute
	public void updateReport(Report report) throws UniqueValidationException {
		reportDAO.validateUniqueConstraint(report);
		
		reportDAO.update(report);
		LOGGER.debug("Report " + report.getPk() + ": " + report.getName() + " was updated");
		
	}

	@Override
	@TransactionAttribute
	public void deleteReport(Long id) {
		reportDAO.delete(id);
		LOGGER.debug("Report with id " + id + " was removed");
	}

	@Override
//	@RolesAllowed({"reportAdmin","superAdmin","resourceController","costController"})
	public FilterResult<Report> searchReports(BaseFilter filter) {
		List<Report> reports = reportDAO.listReport(filter);
		LOGGER.debug("Number of reports : " + reports.size());
		Long count = reportDAO.countReport(filter);
		LOGGER.debug("Number of count reports: " + count);
		return new FilterResult<Report>(reports, count);
	}

}
