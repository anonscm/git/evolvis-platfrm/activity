/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.dao.PermissionDAO;
import de.tarent.activity.dao.ResourceDAO;
import de.tarent.activity.dao.RoleDAO;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.PermissionService;

@Stateless(name = "PermissionService")
@Remote(value = PermissionService.class)
public class PermissionServiceImpl implements PermissionService {

	@EJB
	private PermissionDAO permissionDao;
	
	@EJB
	private ResourceDAO resourceDao;
	
	@EJB
	private RoleDAO roleDao;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PermissionServiceImpl.class);
	
	@Override
    public Resource addPermissionsToResource(Long resId, Long[] permissionIds) {
        Resource res = resourceDao.find(resId);
        
        for(Long id : permissionIds){
            Permission perm = permissionDao.find(id);
            if(perm != null){
            	res.getPermissions().add(perm);
            }
        }
        return res;
    }
	
	@Override
    public Role addPermissionsToRole(Long roleId, Long[] permissionIds) {
        Role role = roleDao.find(roleId);
        for (Long permId : permissionIds) {
            Permission perm = permissionDao.find(permId);
            if(perm != null){
            	role.getPermissions().add(perm);
            }
        }
        
        return roleDao.update(role);
    }
	
	@Override
    public Role addResourcesToRole(Long roleId, Long[] userIds) {
        Role role = roleDao.find(roleId);
        
        for(Long id : userIds){
            Resource res = resourceDao.find(id);
            if(res != null){
            	role.getResources().add(res);
            }
        }
        return role;
    }

	@Override
    public Role addResourceToRole(Long roleId, Long userId) {
        Role role = roleDao.find(roleId);
        Resource res = resourceDao.find(userId);
        if(res != null){
        	role.getResources().add(res);
        }
        return role;
    }
	
	@Override
	public void checkAndThrowPermission(Long resourceId, String... actionId) {
	    if (!checkPermission(resourceId, actionId)) {
	    	LOGGER.warn("User '" + resourceId + "' does not have the permission '" + Arrays.toString(actionId) + "'");
	        throw new PermissionException("You don't have permission to access this resource.");
	    }
	}

	@Override
	public boolean checkPermission(Long resourceId, String... actionId) {
	    // for each given actionId check if user has permission until first permission is found
	    for(String action : actionId){
            Permission perm = permissionDao.findByResourceAndPermission(resourceId, action);
            if(perm != null){
                return true;
            }
            List<Role> roles = roleDao.findByResourceAndPermission(resourceId, action);
            if(roles != null && !roles.isEmpty()){
                return true;
            }
	    }
		
		return false;
	}
	
	@Override
	public List<Role> getAllRoles() {
		return roleDao.findAll();
	}

	@Override
	public List<Resource> getResourcesByPermission(String actionId){
		Permission permission = permissionDao.findByActionId(actionId);
		if(permission == null){
		    return null;
		}
		return resourceDao.getByPermission(permission);
	}
	
	@Override
	public Role getRole(Long pk) {
		return roleDao.find(pk);
	}

    @Override
    public Resource removePermissionsFromResource(Long resId, Long[] permissionIds) {
        Resource res = resourceDao.find(resId);
        
        for(Long id : permissionIds){
            Permission perm = permissionDao.find(id);
            res.getPermissions().remove(perm);
        }
        return res;
    }

    @Override
    public Role removePermissionsFromRole(Long roleId, Long[] permissionIds) {
        Role role = roleDao.find(roleId);
        for (Long permId : permissionIds) {
            Permission perm = permissionDao.find(permId);
            role.getPermissions().remove(perm);
        }
        
        return roleDao.update(role);
    }

    @Override
    public Role removeResourceFromRole(Long roleId, Long userId) {
        Role role = roleDao.find(roleId);
        Resource res = resourceDao.find(userId);
        role.getResources().remove(res);
        return role;
    }

    @Override
    public Role removeResourcesFromRole(Long roleId, Long[] userIds) {
	    Role role = roleDao.find(roleId);
        
	    for (Long userId : userIds) {
            Resource user = resourceDao.find(userId);
            role.getResources().remove(user);
        }
        
        return roleDao.update(role);
    }

    @Override
	public FilterResult<Permission> searchPermissions(PermissionFilter filter){
	    List<Permission> permissions = permissionDao.listPermission(filter);
	    long totalCount = permissionDao.countPermissions();
	    
	    return new FilterResult<Permission>(permissions, totalCount);
	}
    
}
