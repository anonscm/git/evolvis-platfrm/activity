/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.exception.ActivityImportException;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ImportService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.ImportResultModel;

/**
 * Implementation for ImportService.
 *
 */
@Stateless(name = "ImportService")
@Remote(value = ImportService.class)
public class ImportServiceImpl implements ImportService {

	@EJB
	private ResourceService resourceService;

	@EJB
	private ActivityService activityService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ImportServiceImpl.class);

	@Override
	// @RolesAllowed({ "costController", "resourceController" })
	public List<ImportResultModel> doImportExcel(byte[] data, Long resourceId,
			Long positionId) throws ActivityImportException {

		List<ImportResultModel> errors = new ArrayList<ImportResultModel>();

		if (data != null) {
			try {
				errors = importExcelSheet(new ByteArrayInputStream(data),
						resourceId, positionId);
			} catch (IndexOutOfBoundsException e) {
				throw new ActivityImportException(e.getMessage());
			} catch (IOException e) {
				throw new ActivityImportException(e.getMessage());
			}
			LOGGER.info("errors size: " + errors.size());

		}
		return errors;
	}

	/**
	 * Get the value of the column <code>col</code> as date.
	 *
	 * @param row
	 * @param col
	 * @return
	 */
	private Date getCellAsDate(HSSFRow row, int col) {
		HSSFCell cell = row.getCell(col);
		if (cell == null){
			return null;
        }
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			return cell.getDateCellValue();
        } else {
			return null;
        }
	}

	/**
	 * Get the value of the column <code>col</code> as double.
	 *
	 * @param row
	 * @param col
	 * @return
	 */
	private Double getCellAsDouble(HSSFRow row, int col) {
		HSSFCell cell = row.getCell(col);
		if (cell == null){
			return null;
        }
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			return cell.getNumericCellValue();
        } else {
			return null;
        }
	}

	/**
	 * Get the value of the column <code>col</code> as string.
	 *
	 * @param row
	 * @param col
	 * @return
	 */
	private String getCellAsString(HSSFRow row, int col) {
		HSSFCell cell = row.getCell(col);
		if (cell == null){
			return null;
        }
		if (cell.getCellType() == Cell.CELL_TYPE_STRING){
			return cell.getStringCellValue();
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			return Double.toString(cell.getNumericCellValue());
        } else {
			return null;
        }
	}

	private ArrayList<ImportResultModel> importExcelSheet(HSSFSheet sheet,
			Resource resource, Long positionId) throws IOException {

		ArrayList<ImportResultModel> excelErrors = new ArrayList<ImportResultModel>();
		Integer linesSuccess = 0;
		Integer linesFailure = 0;

        Integer excelTableStartRowOfTable = skipToTheStartOfTheExcelTable(sheet);
        LOGGER.info("First table row: " + excelTableStartRowOfTable);
		LOGGER.info("Last table row: " + sheet.getLastRowNum());
		for (int r = excelTableStartRowOfTable; r <= sheet.getLastRowNum(); r++) {
			LOGGER.info("Processing excel datatable row number: " + r);
			HSSFRow row = sheet.getRow(r);
			try {

				int rowNum = row.getRowNum() + 1;
				Activity activity = new Activity();
				activity.setResource(resource);
				activity.setPosition(new Position(positionId));
				if (resource.getCostPerHour() != null) {
					activity.setCostPerHour(new BigDecimal(resource
							.getCostPerHour().doubleValue()));
				}

                String firstCol = getCellAsString(row, 0);
                String secondCol = getCellAsString(row, 1);
                String thirdCol = getCellAsString(row, 2);
                if(firstCol == null && secondCol == null && thirdCol == null){
                    //Automatically finish the import when an empty row occurs in or at the end of data.
                    break;
                }else{
				    activity.setDate(getCellAsDate(row, 0));
				    activity.setName(getCellAsString(row, 1));
				    activity.setHours(new BigDecimal(getCellAsDouble(row, 2)));
                }

				if (((activity.getName() == null) || (activity.getName()
						.length() == 0))
						&& (activity.getDate() == null)
						&& (activity.getHours() == null)) {
					LOGGER.info("Empty row.");
				} else {

					ImportResultModel activityError = verify(activity, rowNum);

					if (activityError.getErrorMessages().isEmpty()) {
						try {
							activityService.addActivity(activity);
							LOGGER.info("Add success");
							linesSuccess++;
						} catch (UniqueValidationException e) {
							LOGGER.info("....UniqueValidationException + "
									+ e.getMessage());
							ImportResultModel errorExist = new ImportResultModel();
							errorExist.setRow(rowNum);
							errorExist.getErrorMessages().add(
									"validate_unique_constraint");
							excelErrors.add(errorExist);
							linesFailure++;
						}
					} else {
						linesFailure++;
						excelErrors.add(activityError);

					}
				}

			} catch (NumberFormatException e) {
				// number format exceptions will be ignored
                LOGGER.error("NumberFormatException " + e.getMessage());
				linesFailure++;
			} catch (NullPointerException e) {
				// null pointer exceptions will be ignored
                LOGGER.error("NullPointerException " + e.getMessage());
				linesFailure++;
			}
		}

        ImportResultModel.setLines(linesSuccess + linesFailure);
        ImportResultModel.setLinesSuccess(linesSuccess);
        ImportResultModel.setLinesFailure(linesFailure);

		LOGGER.info("excel errors size: " + excelErrors.size());

		return excelErrors;

	}

    private Integer skipToTheStartOfTheExcelTable(HSSFSheet sheet) {
        int r = 0;
        for (r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
            LOGGER.info("Skipping row: " + r);
            HSSFRow row = sheet.getRow(r);
            String firstCol = getCellAsString(row, 0) != null ? getCellAsString(row, 0) : "";
            String secondCol = getCellAsString(row, 1) != null ? getCellAsString(row, 1) : "";
            String thirdCol = getCellAsString(row, 2) != null ? getCellAsString(row, 2) : "";
            if( firstCol.equals("Datum") &&
                secondCol.equals("Beschreibung der Leistung") &&
                thirdCol.equals("Stundenanzahl") ){
                return r+1; //return the next row as the first datarow
            }

        }
        return r;
    }

    private List<ImportResultModel> importExcelSheet(InputStream inputStream,
			Long resourceId, Long positionId) throws IOException,
			IndexOutOfBoundsException {

		POIFSFileSystem fs = new POIFSFileSystem(inputStream);
		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		Resource resource = resourceService.getResource(resourceId, false,
				false);
		List<ImportResultModel> errors = importExcelSheet(
				workbook.getSheetAt(0), resource, positionId);
		LOGGER.info("...errors size: " + errors.size());
		return errors;

	}

	private ImportResultModel verify(Activity activity, Integer row) {
		ImportResultModel activityError = new ImportResultModel();
		List<String> messages = new ArrayList<String>();
		if (((activity.getName() == null) || (activity.getName().length() == 0))
				&& (activity.getDate() == null)
				&& (activity.getHours() == null)) {
			activityError.setRow(row);
			messages.add("row_not_saved");
		} else {

			if (activity.getDate() == null) {
				activityError.setRow(row);
				messages.add("validate_date_not_empty");
			} else if (activity.getDate().after(new Date())) {
				activityError.setRow(row);
				messages.add("validate_activity_future");
			}

			if (activity.getName() == null) {
				activityError.setRow(row);
				messages.add("validate_description_not_empty");
			} else if (activity.getName().length() > 255) {
				activityError.setRow(row);
				messages.add("validate_activity_description_length");
			}

			if (activity.getHours() == null) {
				activityError.setRow(row);
				messages.add("validate_hours_not_empty");

			} else if (activity.getHours().compareTo(new BigDecimal(24)) > 0
					|| activity.getHours().compareTo(BigDecimal.ZERO) <= 0) {
				activityError.setRow(row);
				messages.add("validate_hours_range");
			}
		}

		activityError.setErrorMessages(messages);
		return activityError;

	}

}
