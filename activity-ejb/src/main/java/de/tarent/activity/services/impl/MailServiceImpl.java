/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.PermissionService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.util.MailMessage;
import de.tarent.activity.util.UserUtil;

/**
 * Implementation for MailService.
 * 
 */
@Stateless(name = "MailService")
@Remote(value = MailService.class)
//@RolesAllowed({ "user" })
public class MailServiceImpl implements MailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceImpl.class);

	@EJB
	private UserUtil userUtil;

	@EJB
	private ResourceService resourceService;
	
	@EJB
	private PermissionService permissionService;
	
	private static final String SUBJECT_CONFIGURABLE_ADD_HOLIDAY_MAIL = "subject.configurable_add_holiday_mail";
	private static final String SUBJECT_CONFIGURABLE_APPROVED_BY_MANAGER_LOSS_MAIL = "subject.configurable_approved_by_manager_loss_mail";
	private static final String SUBJECT_CONFIGURABLE_ADD_PROJECT = "subject.configurable_add_project";
	private static final String SUBJECT_CONFIGURABLE_ADD_JOB = "subject.configurable_add_job";
	private static final String SUBJECT_CONFIGURABLE_ADD_CUSTOMER = "subject.configurable_add_customer";
	private static final String SUBJECT_CONFIGURABLE_EDIT_PROJECT_INVOICE = "subject.configurable_edit_project_invoice";
	private static final String SUBJECT_CONFIGURABLE_ADD_PROJECT_INVOICE = "subject.configurable_add_project_invoice";
	private static final String SUBJECT_CONFIGURABLE_EDIT_JOB_INVOICE = "subject.configurable_edit_job_invoice";
	private static final String SUBJECT_CONFIGURABLE_ADD_JOB_INVOICE = "subject.configurable_add_job_invoice";
	private static final String SUBJECT_CONFIGURABLE_ADD_ABSENCE = "subject.configurable_add_absence";
	private static final String SUBJECT_CONFIGURABLE_DENIED_BY_CONTROLLING_LOSS = "subject.configurable_denied_by_controlling_loss";
	private static final String SUBJECT_CONFIGURABLE_DENIED_BY_MANAGER_LOSS = "subject.configurable_denied_by_manager_loss";
	private static final String SUBJECT_CONFIGURABLE_APPROVED_BY_CONTROLLING_LOSS = "subject.configurable_approved_by_controlling_loss";

	private static final String JOB_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/newJob.vm";
	private static final String PROJECT_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/newProject.vm";
	private static final String CUSTOMER_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/newCustomer.vm";
	private static final String RESOURCE_HOLIDAY_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceHoliday.vm";
	private static final String RESOURCE_HOLIDAY_APPROVED_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceHolidayApproved.vm";
	private static final String RESOURCE_HOLIDAY_REJECTED_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceHolidayRejected.vm";

	private static final String RESOURCE_HOLIDAY_APPROVED_BY_MANAGER_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceHolidayApprovedByManager.vm";
	private static final String RESOURCE_HOLIDAY_REJECTED_BY_MANAGER_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceHolidayRejectedByManager.vm";
	private static final String RESOURCE_ABSENCE_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/resourceAbsence.vm";

	private static final String NEW_JOB_INVOICE_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/newJobInvoice.vm";
	private static final String EDIT_JOB_INVOICE_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/editJobInvoice.vm";
	private static final String NEW_PROJECT_INVOICE_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/newProjectInvoice.vm";
	private static final String EDIT_PROJECT_INVOICE_NOTIFICATION_TEMPLATE = "de/tarent/activity/services/impl/editProjectInvoice.vm";

    private static final String LOSS_CONSTANT = "loss";

	private static Properties props = new Properties();

	private static Map<String, List<InternetAddress>> emailMap = new HashMap<String, List<InternetAddress>>();

	/**
	 * Init class properties.
	 * 
	 * @throws Exception
	 */
	@PostConstruct
	public void init() {

		InputStream is = MailServiceImpl.class
				.getResourceAsStream("/de/tarent/activity/services/impl/mailMessage.properties");
		try {
			props.load(is);
		} catch (IOException e) {
			LOGGER.error("Error while loading mail property file "+e.getMessage());
		}

		readEmailAddressesFromXmlFile();
	}

	private String mergeTemplate(VelocityContext context, String templatePath) {
		LOGGER.debug("Merging template");

		Properties p = new Properties();
		p.put("resource.loader", "class");
		p.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

		Velocity.init(p);
		LOGGER.debug("Velocity initialized successfully");

		Template template = Velocity.getTemplate(templatePath);

		StringWriter sw = new StringWriter();
		template.merge(context, sw);
		LOGGER.debug("Merged template " + templatePath);

		return sw.getBuffer().toString();
	}

	/**
	 * Creates the object containing email details (subject, sender, recipients,
	 * date etc.). The list of recipients consists of all the resource
	 * controllers (rights = 512) and the sender is the current user.
	 * 
	 * @param context
	 * @param templateSubject
	 *            the email subject
	 * @param templatePath
	 *            the path to the template file
	 * @return a populated {@code MailMessage} object
	 */
	private MailMessage prepareMailMessage(VelocityContext context, String templateSubject, String templatePath,
			List<InternetAddress> toAddresses) {
		//FIXME: UserUtil always returns 'anonymous'
		Resource currentUser = resourceService.loadResource(userUtil.getCurrentUser());
        //Resource resource = RequestUtil.getLoggedUser(request);    //Another alternative
		context.put("from", currentUser);

		InternetAddress from;
		if (currentUser != null && currentUser.getMail() != null) {
			try {
				from = new InternetAddress(currentUser.getMail());
				// add current user to recipients' list
				toAddresses.add(from);
			} catch (AddressException e) {
				LOGGER.warn("Sender email is invalid, not sending any mail message: " + currentUser.getUsername());
				return null;
			}
		} else {
			LOGGER.warn("Sender address is null, not sending any mail message.");
			return null;
		}

		String text = mergeTemplate(context, templatePath);

		MailMessage message = new MailMessage();
		message.setSubject(getConfigurableInformationByKey(templateSubject));
		message.setFrom(from);
		message.setTo(toAddresses);
		message.setText(text);
		message.setDate(new Date());
		return message;
	}

    /*
    Todo: Overloaded method to get the mail sending work, must unify with the other method to remove code duplication.
     */
    private MailMessage prepareMailMessage(VelocityContext context, String templateSubject, String templatePath,
                                           List<InternetAddress> toAddresses, String loggedInUser) {
        Resource currentUser = resourceService.loadResource(loggedInUser);
        context.put("from", currentUser);

        InternetAddress from;
        if (currentUser.getMail() != null) {
            try {
                from = new InternetAddress(currentUser.getMail());
                // add current user to recipients' list
                toAddresses.add(from);
            } catch (AddressException e) {
                LOGGER.warn("Sender email is invalid, not sending any mail message: " + currentUser.getUsername());
                return null;
            }
        } else {
            LOGGER.warn("Sender address is null, not sending any mail message: " + currentUser.getUsername());
            return null;
        }

        String text = mergeTemplate(context, templatePath);

        MailMessage message = new MailMessage();
        message.setSubject(getConfigurableInformationByKey(templateSubject));
        message.setFrom(from);
        message.setTo(toAddresses);
        message.setText(text);
        message.setDate(new Date());
        return message;
    }

	private List<InternetAddress> getControllersAddress() {
		// get resource controllers
		List<Resource> resourceControllers = permissionService.getResourcesByPermission(PermissionIds.RESOURCE_CONTROLLER);
		if ((resourceControllers == null) || (resourceControllers.size() == 0)) {
			LOGGER.warn("No resource controllers found, not sending any mail message.");
			return null;
		}

		LOGGER.debug("Loaded " + resourceControllers.size() + " controllers");

		List<InternetAddress> toAddresses = new ArrayList<InternetAddress>();
		for (Resource resource : resourceControllers) {
			if (resource.getMail() != null) {
				try {
					InternetAddress address = new InternetAddress(resource.getMail());
					toAddresses.add(address);
				} catch (AddressException e) {
					LOGGER.warn("Cannot parse address, skipping recipient: " + resource.getUsername(), e);
					continue;
				}
			} else {
				LOGGER.warn("Null email address, skipping recipient: " + resource.getUsername());
			}
		}

		return toAddresses;
	}

	@Override
//	@RolesAllowed({ "customerAdmin" })
	public void sendCustomerMail(Customer customer) throws MessagingException {
		LOGGER.debug("Sending new customer mail.");
		VelocityContext context = new VelocityContext();
		context.put("customer", customer);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_CUSTOMER,
				CUSTOMER_NOTIFICATION_TEMPLATE, getControllersAddress());
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
//	@RolesAllowed({ "resourceController" })
	public void sendHolidayApprovedMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_APPROVED_BY_CONTROLLING_LOSS,
				RESOURCE_HOLIDAY_APPROVED_NOTIFICATION_TEMPLATE, getControllersAddress());
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
	public void sendHolidayMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_HOLIDAY_MAIL,
				RESOURCE_HOLIDAY_NOTIFICATION_TEMPLATE, getControllersAddress(), loss.getResourceByFkResource().getUsername());
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
//	@RolesAllowed({ "resourceController" })
	public void sendHolidayRejectedMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_DENIED_BY_CONTROLLING_LOSS,
				RESOURCE_HOLIDAY_REJECTED_NOTIFICATION_TEMPLATE, getControllersAddress());
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public void sendJobMail(Job job) throws MessagingException {
		LOGGER.debug("Sending new customer mail.");
		VelocityContext context = new VelocityContext();
		context.put("job", job);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_JOB, JOB_NOTIFICATION_TEMPLATE,
				getControllersAddress());
		if (message != null) {
			sendMail(message);
		}
	}

	private void sendMail(MailMessage message) throws MessagingException {
		LOGGER.info("Sending message...");
		String jndiMailName = "java:/jboss/mail/ActivityMailSession";
		Session mailSession = null;
		try {
			mailSession = InitialContext.doLookup(jndiMailName);
		} catch (NamingException e) {
			LOGGER.warn("Mail session not configured correctly. JNDI resource " + jndiMailName
					+ " not found. Skipping mail sending.");
			return;
		}

		MimeMessage mimeMessage = new MimeMessage(mailSession);
		mimeMessage.setSubject(message.getSubject());
		mimeMessage.setFrom(message.getFrom());
		mimeMessage.setRecipients(RecipientType.TO, message.getTo().toArray(new InternetAddress[0]));
		mimeMessage.setRecipients(RecipientType.CC, message.getToCC().toArray(new InternetAddress[0]));
		mimeMessage.setRecipients(RecipientType.BCC, message.getToBCC().toArray(new InternetAddress[0]));
		mimeMessage.setText(message.getText());
		mimeMessage.setSentDate(message.getDate());
		mimeMessage.saveChanges();

		Transport transport = mailSession.getTransport();
		transport.connect(null, null);

		try {
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			LOGGER.info("Message sent");
		} finally {
			transport.close();
		}
	}

	@Override
//	@RolesAllowed({ "projectAdmin" })
	public void sendProjectMail(Project project) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("project", project);
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_PROJECT,
				PROJECT_NOTIFICATION_TEMPLATE, getControllersAddress());
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
	public void sendConfigurableAddHolidayMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("holiday-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_HOLIDAY_MAIL,
				RESOURCE_HOLIDAY_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableApprovedByManagerLossMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("holiday-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_APPROVED_BY_MANAGER_LOSS_MAIL,
				RESOURCE_HOLIDAY_APPROVED_BY_MANAGER_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableApprovedByControllingLossMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("holiday-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_APPROVED_BY_CONTROLLING_LOSS,
				RESOURCE_HOLIDAY_APPROVED_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableDeniedByManagerLossMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("holiday-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_DENIED_BY_MANAGER_LOSS,
				RESOURCE_HOLIDAY_REJECTED_BY_MANAGER_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableDeniedByControllingLossMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("holiday-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_DENIED_BY_CONTROLLING_LOSS,
				RESOURCE_HOLIDAY_REJECTED_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableAddAbsenceMail(Loss loss) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put(LOSS_CONSTANT, loss);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("absence-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_ABSENCE,
				RESOURCE_ABSENCE_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
	public void sendConfigurableAddJobInvoiceMail(Invoice invoice) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("invoice", invoice);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("invoice-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_JOB_INVOICE,
				NEW_JOB_INVOICE_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableEditJobInvoiceMail(Invoice invoice) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("invoice", invoice);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("invoice-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_EDIT_JOB_INVOICE,
				EDIT_JOB_INVOICE_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableAddProjectInvoiceMail(Invoice invoice) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("invoice", invoice);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("invoice-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_PROJECT_INVOICE,
				NEW_PROJECT_INVOICE_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableEditProjectInvoiceMail(Invoice invoice) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("invoice", invoice);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("invoice-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_EDIT_PROJECT_INVOICE,
				EDIT_PROJECT_INVOICE_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
	public void sendConfigurableAddCustomerMail(Customer customer) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("customer", customer);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("project-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_CUSTOMER,
				CUSTOMER_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}

	}

	@Override
	public void sendConfigurableAddJobMail(Job job) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("job", job);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("project-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_JOB, JOB_NOTIFICATION_TEMPLATE,
				toAddresses);
		if (message != null) {
			sendMail(message);
		}
	}

	@Override
	public void sendConfigurableAddProjectMail(Project project) throws MessagingException {
		VelocityContext context = new VelocityContext();
		context.put("project", project);
		List<InternetAddress> toAddresses = readEmailAddressesFromXmlFile("project-mails");
		MailMessage message = prepareMailMessage(context, SUBJECT_CONFIGURABLE_ADD_PROJECT,
				PROJECT_NOTIFICATION_TEMPLATE, toAddresses);
		if (message != null) {
			sendMail(message);
		}
	}

	/**
	 * Get mail list for specific event.
	 * 
	 * @param tagName
	 *            - specific tag name
	 * @return
	 */
	private List<InternetAddress> readEmailAddressesFromXmlFile(String tagName) {
		return emailMap.get(tagName);
	}

	/**
	 * Create mail list.
	 */
	private void readEmailAddressesFromXmlFile() {

		try {
			InputStream fXmlFile = MailServiceImpl.class
					.getResourceAsStream("/de/tarent/activity/services/impl/email_address.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			emailMap.put("holiday-mails", getlist("holiday-mails", doc));
			emailMap.put("invoice-mails", getlist("invoice-mails", doc));
			emailMap.put("project-mails", getlist("project-mails", doc));
			emailMap.put("absence-mails", getlist("absence-mails", doc));

		} catch (Exception e) {

		}
	}

	/**
	 * Get email list from xml file.
	 * 
	 * @param tagName
	 *            - tag name
	 * @param doc
	 *            - Document
	 * @return
	 */
	private List<InternetAddress> getlist(String tagName, Document doc) {
		List<InternetAddress> list = new ArrayList<InternetAddress>();
		try {
			NodeList nList = doc.getElementsByTagName(tagName).item(0).getChildNodes();

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					list.add(new InternetAddress(nNode.getChildNodes().item(0).getNodeValue()));
				}
			}
		} catch (Exception e) {

		}
		return list;
	}

	/**
	 * Get mail subject from properties file.
	 * 
	 * @param key
	 * @return
	 */
	private String getConfigurableInformationByKey(String key) {
		return props.getProperty(key) != null ? props.getProperty(key) : key;
	}

}
