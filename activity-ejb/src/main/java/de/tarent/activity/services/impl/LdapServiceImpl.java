/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import de.tarent.activity.dao.BranchOfficeDAO;
import de.tarent.activity.dao.LdapDAO;
import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.domain.LdapConfig;
import de.tarent.activity.domain.LdapUser;
import de.tarent.activity.service.LdapService;

@Stateless(name = "LdapService")
@Remote(value = LdapService.class)
//@RolesAllowed({"user"})
public class LdapServiceImpl implements LdapService {

    @EJB
	private LdapDAO ldapDAO;
    
	@EJB
	private BranchOfficeDAO branchOfficeDAO;
    

	@Override
//	@PermitAll
	public List<BranchOffice> getBranchOfficeList() {
		return branchOfficeDAO.findAll();
	}
	
	private String convertListToCommaSeparatedString(List<String> list) {
		String commaSeparatedString = "";
		for(int i = 0; i< list.size(); i++) {
			commaSeparatedString += list.get(i);
			if(i != (list.size()-1)) {
				commaSeparatedString +=", ";
			}					
		}
		return commaSeparatedString;
	}
	
	@Override
//	@PermitAll
	public List<LdapUser> getAllUserAndGroups(LdapConfig ldapConfig, Map<String, String> sortAlias) {
		
		List<LdapUser> userList = this.ldapDAO.findAllUsers(ldapConfig, sortAlias);
		List<String> singleGroupList = null; ;
		
		for(int i = 0; i < userList.size(); i++) {			

			singleGroupList = this.ldapDAO.getGroupsOfUser(ldapConfig, userList.get(i).getUsername());
			userList.get(i).setGroups((this.convertListToCommaSeparatedString(singleGroupList)));
		}

		return userList;
	}
}
