/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.io.IOException;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.DocumentException;

import de.tarent.activity.domain.util.TableModel;
import de.tarent.activity.service.ExportService;
import de.tarent.activity.util.DefaultExcelWriter;
import de.tarent.activity.util.DefaultPdfWriter;
import de.tarent.activity.util.ExcelWriter;
import de.tarent.activity.util.PdfWriter;

/**
 * Implementation for ExportService.
 * 
 */
@Stateless(name = "ExportService")
@Remote(value = ExportService.class)
//@PermitAll
public class ExportServiceImpl implements ExportService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportServiceImpl.class);

	@Override
	public byte[] doExportExcel(TableModel model) throws IOException {
		LOGGER.info("Writing " + model.getItems().size() + " items to excel document...");
		ExcelWriter excelWriter = new DefaultExcelWriter();
		return excelWriter.write(model);
	}

	@Override
	public byte[] doExportPdf(TableModel model) throws DocumentException {
		LOGGER.info("Writing " + model.getItems().size() + " items to pdf document...");
		PdfWriter pdfWriter = new DefaultPdfWriter();

		return pdfWriter.write(model);
	}
}
