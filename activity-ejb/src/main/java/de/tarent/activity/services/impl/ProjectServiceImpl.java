/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import de.tarent.activity.dao.*;
import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.*;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for ProjectService.
 * 
 */
@Stateless(name = "ProjectService")
@Remote(value = ProjectService.class)
//@RolesAllowed({ "user" })
public class ProjectServiceImpl implements ProjectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);

	@EJB
	private JobDAO jobDAO;

	@EJB
	private JobStatusDAO jobStatusDAO;

	@EJB
	private JobTypeDAO jobTypeDAO;

	@EJB
	private PositionDAO positionDAO;

	@EJB
	private PosResourceMappingDAO posResourceMappingDAO;

	@EJB
	private PositionStatusDAO positionStatusDAO;

	@EJB
	private CostDAO costDAO;

	@EJB
	private ProjectDAO projectDAO;

	@EJB
	private CustomerDAO customerDAO;

	@EJB
	private PosResourceStatusDAO posResourceStatusDAO;

	@Override
//	@RolesAllowed({ "customerAdmin" })
	public Long addCustomer(Customer customer) {
		return customerDAO.create(customer);
	}

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "jobAdmin" })
	public Long addJob(Job job) throws UniqueValidationException {
		jobDAO.validateUniqueConstraint(job);

		return jobDAO.create(job);
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public Long addPosition(Position position) {
		return positionDAO.create(position);

	}

	@Override
//	@RolesAllowed({"user", "resourceAdmin", "jobAdmin" })
	public void addPosResourceMapping(PosResourceMapping posRes) {
		this.posResourceMappingDAO.create(posRes);
	}

	@Override
//	@RolesAllowed({ "projectAdmin" })
	public Long addProject(Project project) throws UniqueValidationException {
		projectDAO.validateUniqueConstraint(project);

		return projectDAO.create(project);
	}

	@Override
//	@RolesAllowed({ "customerAdmin" })
	public void deleteCustomer(Long id) {
		customerDAO.delete(id);
	}

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public void deleteJob(Long jobId) {
        jobDAO.delete(jobId);
    }

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public void deletePosition(Long positionId) {
        positionDAO.delete(positionId);
    }

	@Override
//	@RolesAllowed({ "resourceAdmin" })
	public void deletePosResourceMapping(Long id) {
		posResourceMappingDAO.delete(id);

	}

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public void deleteProject(Long projectId) {
        projectDAO.delete(projectId);
    }

	@Override
	@TransactionAttribute
//	@RolesAllowed({ "jobAdmin" })
	public Job duplicateJob(Job job) throws UniqueValidationException {
		Job newJob = new Job(job);
		addJob(newJob);

		List<Position> positions = getAllPositionsByJob(job.getPk());
		List<Position> newPositions = new ArrayList<Position>();
		
		for (Position position : positions) {
			Position newPosition = new Position(position);
			newPosition.setJob(newJob);
			addPosition(newPosition);
			newPositions.add(newPosition);

			List<PosResourceMapping> posResList = getPosResourceMappingsByPosition(position.getPk());
			for (PosResourceMapping posRes : posResList) {
				PosResourceMapping newPosRes = new PosResourceMapping(posRes);
				newPosRes.setPosition(newPosition);

				addPosResourceMapping(newPosRes);
			}
		}
		newJob.setPositions(newPositions);
		return newJob;
	}

	@Override
//	@RolesAllowed({ "user", "costController", "resourceController" })
	public List<Job> getActiveJobsByResource(Long resourceId) {
		List<Job> jobs = jobDAO.findActiveJobsByResource(resourceId);
		LOGGER.debug("Number of active jobs for resource id " + resourceId + " is:" + jobs.size());
		return jobs;
	}

	@Override
//	@PermitAll
	public List<Project> getAllActiveProjectList() {
		return projectDAO.findAllActive();
	}

    @Override
    public boolean isProjectActive(Long projectId){
        List<Job> jobs = getJobsByProject(projectId);
        for (Job job : jobs) {
            if( jobHasStatusOrderedAndAtLeastOneActiveResource(job, job.getJobStatus()) ){
                return true;
            }
        }

        return false;
    }

    private boolean jobHasStatusOrderedAndAtLeastOneActiveResource(Job job, JobStatus jobStatus) {
        boolean active = false;
        if (jobStatus != null && jobStatus.getPk() == ProjectService.JOBSTATUS_ORDERED) {
            for(Position pos : job.getPositions()){
                for(PosResourceMapping prm : pos.getPosResourceMappings()){
                    if(prm.getPosResourceStatus().getPk() == ProjectService.POS_RES_STATUS_ACTIVE){
                        active = true;
                        break;
                    }
                }
            }
        }
        return active;
    }


    @Override
//	@RolesAllowed({"user", "jobAdmin", "resourceAdmin", "costController", "resourceController" })
	public List<Position> getAllPositionsByJob(Long jobId) {
		return positionDAO.findByJob(jobId, null, null);
	}

	@Override
//	@RolesAllowed({"user", "jobAdmin" })
	public List<Cost> getCostsByJob(Long jobId) {
		return costDAO.findByJob(jobId);
	}

	@Override
//	@RolesAllowed({ "user", "customerAdmin", "projectAdmin" })
	public Customer getCustomer(Long id) {
		return customerDAO.find(id);
	}

	@Override
//	@RolesAllowed({ "jodAdmin", "costController" })
	public List<Customer> getCustomerList() {
		return customerDAO.findAll();
	}

	@Override
//	@RolesAllowed({ "user", "jobAdmin" })
	public Job getJob(Long id) {
		Job job = jobDAO.find(id);
		LOGGER.debug("Job name with id " + id + " is:" + job.getName());
		return job;
	}

	@Override
//    @RolesAllowed({"user", "jobAdmin"})
    public List<ChangeInfo> getJobChangeInfo(Long jobPk) {
        return jobDAO.getChanges(jobPk);
    }

	@Override
//	@RolesAllowed({ "jobAdmin", "resourceAdmin", "costController" })
	public List<Job> getJobList() {
		return jobDAO.findAll();
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public List<Job> getJobListToManage() {
		List<Job> jobs = jobDAO.findNotMaintenanceJobs();
		LOGGER.debug("Number of all jobs : " + jobs.size());
		return jobs;
	}

	@Override
//	@RolesAllowed({ "user", "jobAdmin", "projectAdmin", "resourceAdmin", "costController" })
	public List<Job> getJobsByProject(Long projectId) {
		return jobDAO.findByProject(projectId);

	}

	@Override
//	@RolesAllowed({ "user", "costController", "resourceController" })
	public List<Job> getJobsByResource(Long resourceId) {
		List<Job> jobs = jobDAO.findJobsByResource(resourceId);
		LOGGER.debug("Number of jobs for resource id " + resourceId + " is:" + jobs.size());
		return jobs;
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public JobStatus getJobStatus(Long id) {
		return jobStatusDAO.find(id);
	}

	@Override
//	@RolesAllowed({"user", "jobAdmin", "projectAdmin" })
	public List<JobStatus> getJobStatusList() {
		return jobStatusDAO.findAll();
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public JobType getJobType(Long id) {
		return jobTypeDAO.find(id);
	}
	
	@Override
//	@RolesAllowed({"user", "jobAdmin" })
	public List<JobType> getJobTypeList() {
		return jobTypeDAO.findAll();
	}

	@Override
//	@RolesAllowed({ "user", "jobAdmin" })
	public Position getPosition(Long id) {
		Position position = positionDAO.find(id);
		LOGGER.debug("Position name with id " + id + " is:" + position.getName());
		return position;
	}

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public List<ChangeInfo> getPositionChangeInfo(Long positionId) {
        return positionDAO.getChanges(positionId);
    }

	@Override
//	@RolesAllowed({ "jobAdmin", "resourceAdmin", "costController", "resourceController" })
	public List<Position> getPositionsByJob(Long jobId, Long statusId, Long resourceId) {
		return positionDAO.findByJob(jobId, statusId, resourceId);
	}
	
	@Override
	public JobStats getPositionStats(Long positionId) {
		Position p = positionDAO.find(positionId);
		JobStats stats = new JobStats();
		BigDecimal spent = positionDAO.getTotalDaysSpent(positionId);
		BigDecimal expected = p.getExpectedWork();
		stats.setRealWork(spent);
		stats.setExpectedWork(expected);
		stats.setCommunicatedWork(p.getCommunicatedWork());
		stats.setIncome(p.getIncome());
		stats.setInvoicePaidTotal(null);
		stats.setInvoiceTotal(null);
		stats.setOtherCosts(null);
		stats.setServiceCosts(p.getCost());
        stats.setPercentDone( calculatePercentDone( spent, expected) );
        return stats;
	}

    @Override
	public JobStats getJobStats(Long jobId) {
		JobStats stats = new JobStats();
		
		
		BigDecimal spent = jobDAO.getTotalDaysSpent(jobId);
		BigDecimal expected = jobDAO.getTotalExpectedWork(jobId);
		stats.setRealWork(spent);
		stats.setExpectedWork(expected);
		stats.setCommunicatedWork(jobDAO.getTotalCommunicatedWork(jobId));
		stats.setFixedPrice(jobDAO.getTotalFixedPrice(jobId));
		stats.setInvoicePaidTotal(jobDAO.getTotalInvoicePaid(jobId));
		stats.setInvoiceTotal(jobDAO.getTotalInvoice(jobId));
		stats.setServiceCosts(jobDAO.getTotalServiceCosts(jobId));
		stats.setOtherCosts(jobDAO.getTotalOtherCosts(jobId));
        stats.setPercentDone( calculatePercentDone( spent, expected) );
		return stats;
	}

    private BigDecimal calculatePercentDone(BigDecimal spent, BigDecimal expected) {
        return  (expected != null && !(expected.compareTo(BigDecimal.ZERO) == 0) && spent != null ?
                spent.divide(expected, 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)) : BigDecimal.ZERO);
    }

    @Override
//	@RolesAllowed({ "jobAdmin" })
	public PositionStatus getPositionStatus(Long id) {
		return positionStatusDAO.find(id);
	}
	
	@Override
//	@RolesAllowed({ "resourceAdmin" })
	public PosResourceMapping getPosResourceMapping(Long id) {
		PosResourceMapping prm = posResourceMappingDAO.find(id);
		prm.getResource().getResourceFTypeMappings().size();
		return prm;
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public List<PosResourceMapping> getPosResourceMappingsByPosition(Long positionId) {
		List<PosResourceMapping> posResList = posResourceMappingDAO.getPosResourceMappingsByPosition(positionId);

		return posResList;
	}

	@Override
//	@RolesAllowed({"user", "jobAdmin", "resourceAdmin" })
	public PosResourceMapping getPosResourceMappingsByPositionAndResource(Long positionId, Long resourceId) {
		return posResourceMappingDAO.getPosResourceMappingByPositionAndResource(positionId, resourceId);
	}

	@Override
	//@RolesAllowed({ "user", "projectAdmin" })
//	@PermitAll
	public Project getProject(Long id) {
		return projectDAO.find(id);
	}

	@Override
//    @RolesAllowed({"user", "jobAdmin"})
    public List<ChangeInfo> getProjectChangeInfo(Long id) {
        return projectDAO.getChanges(id);
    }

	@Override
//	@RolesAllowed({ "jobAdmin", "resourceAdmin", "costController", "resourceController" })
	public List<Project> getProjectList() {
		List<Project> projects = projectDAO.findAll();
		LOGGER.debug("Number of projects: " + projects.size());
		return projects;
	}

	@Override
//	@RolesAllowed({"user", "jodAdmin", "costController", "customerAdmin" })
	public List<Project> getProjectsByCustomer(Long customerId) {
		return projectDAO.findByCustomer(customerId);
	}

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public boolean isDeletableJob(Long jobId) {
        return jobDAO.isDeletable(jobId);
    }

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public boolean isDeletablePosition(Long positionId) {
        return positionDAO.isDeletable(positionId);
    }

	@Override
//    @RolesAllowed({"user", "jobAdmin" })
    public boolean isDeletableProject(Long projectId) {
        return projectDAO.isDeletable(projectId);
    }

	@Override
//	@RolesAllowed({ "costController", "resourceController" })
	public List<Job> listOrderedJobs() {
		return jobDAO.getOrderedList();
	}

	@Override
//	@RolesAllowed({ "user", "resourceController", "projectAdmin" })
	public List<Project> listProjectByResource(Long resId) {
		List<Project> projects = projectDAO.findByResource(resId);
		LOGGER.debug("Number of projects for resId " + resId + " is:" + projects.size());
		return projects;
	}

	@Override
//	@RolesAllowed({ "user" })
	public List<Project> listProjectByResponsibleResource(Long resId) {
		List<Project> projects = projectDAO.findByResponsibleResource(resId);
		LOGGER.debug("Number of projects where user is responsable for resId " + resId + " is:" + projects.size());
		return projects;
	}

	@Override
//	@RolesAllowed({ "jobAdmin" })
	public List<PositionStatus> positionStatusList() {
		return positionStatusDAO.findAll();
	}

	@Override
	public List<PosResourceStatus> posResourceStatusList() {
		return posResourceStatusDAO.findAll();
	}

	@Override
//	@RolesAllowed({"user", "customerAdmin", "projectAdmin" })
	public FilterResult<Customer> searchCustomers(BaseFilter filter) {
		List<Customer> customers = customerDAO.listCustomer(filter);
		Long count = customerDAO.countCustomer(filter);
		return new FilterResult<Customer>(customers, count);
	}

	@Override
//	@RolesAllowed({"user", "jobAdmin"})
	public FilterResult<Job> searchJob(JobFilter filter) {
		List<Job> jobs = jobDAO.listJob(filter);
		Long count = jobDAO.countJob(filter);
		LOGGER.info("count job: " + count);
		return new FilterResult<Job>(jobs, count);
	}

    @Override
	public FilterResult<Position> searchMyPosition(PositionFilter filter) {
		List<Position> positions = positionDAO.listMyPosition(filter);
		Long count = positionDAO.countMyPosition(filter);
		return new FilterResult<Position>(positions, count);
	}

    @Override
//	@RolesAllowed({"user", "jobAdmin" })
	public FilterResult<Position> searchPosition(PositionFilter filter) {
		List<Position> positions = positionDAO.listPosition(filter);
		Long count = positionDAO.countPosition(filter);
		return new FilterResult<Position>(positions, count);
	}

    @Override
//	@RolesAllowed({ "resourceAdmin" })
	public FilterResult<PosResourceMapping> searchPosResourceMapping(PosResourceMappingFilter filter) {
		List<PosResourceMapping> list = posResourceMappingDAO.listPosResourceMapping(filter);
		Long count = posResourceMappingDAO.countPosResourceMapping(filter);
		for (PosResourceMapping prm : list) {
			prm.getResource().getResourceFTypeMappings().size();
		}
		return new FilterResult<PosResourceMapping>(list, count);
	}

    @Override
	//@RolesAllowed({ "user", "projectAdmin" })
//	@PermitAll
	public FilterResult<Project> searchProject(ProjectFilter filter) {
		List<Project> projects = projectDAO.listProjects(filter);
		Long count = projectDAO.countProject(filter);
		return new FilterResult<Project>(projects, count);
	}

    @Override
//	@RolesAllowed({ "customerAdmin" })
	public void updateCustomer(Customer customer) {
		customerDAO.update(customer);
	}
    
    @Override
	@TransactionAttribute
//	@RolesAllowed({ "jobAdmin" })
	public void updateJob(Job job) throws UniqueValidationException {
		jobDAO.validateUniqueConstraint(job);

		jobDAO.update(job);
	}

    @Override
//	@RolesAllowed({ "jobAdmin" })
	public void updatePosition(Position position) {
		positionDAO.update(position);

	}

    @Override
//	@RolesAllowed({ "resourceAdmin" })
	public void updatePosResourceMapping(PosResourceMapping posResourceMapping) {
		posResourceMappingDAO.update(posResourceMapping);

	}

    @Override
//	@RolesAllowed({ "projectAdmin" })
	public void updateProject(Project project) throws UniqueValidationException {
		projectDAO.validateUniqueConstraint(project);

		projectDAO.update(project);
	}
}