/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import de.tarent.activity.dao.ActivityDAO;
import de.tarent.activity.dao.LossDAO;
import de.tarent.activity.dao.LossStatusDAO;
import de.tarent.activity.dao.LossTypeDAO;
import de.tarent.activity.dao.OvertimeDAO;
import de.tarent.activity.dao.TimerDAO;
import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.LossStatus;
import de.tarent.activity.domain.LossType;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.Timer;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.domain.filter.OvertimeFilter;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ResourceService;

/**
 * Implementation class for ActivityService interface.
 *
 */
@Stateless(name = "ActivityService")
@Remote(value = ActivityService.class)
public class ActivityServiceImpl implements ActivityService {

	@EJB
	private ActivityDAO activityDAO;

	@EJB
	private OvertimeDAO overtimeDAO;

	@EJB
	private TimerDAO timerDAO;

	@EJB
	private LossDAO lossDAO;

	@EJB
	private LossTypeDAO lossTypeDAO;

	@EJB
	private LossStatusDAO lossStatusDAO;

	@EJB
	private ResourceService resourceService;

	@Override
	public FilterResult<Activity> searchActivities(ActivityFilter filter) {
		List<Activity> activities = activityDAO.listActivity(filter);
		Long count = activityDAO.countActivity(filter);
		return new FilterResult<Activity>(activities, count);
	}

	@Override
	public BigDecimal getActivitiesHoursSum(ActivityFilter filter){
		return activityDAO.getActivitiesHoursSum(filter);
	}

	@Override
	public Activity getActivity(Long id) {
		return activityDAO.find(id);
	}

	@Override
	@TransactionAttribute
	public Long addActivity(Activity activity) throws UniqueValidationException {
		activityDAO.validateUniqueConstraint(activity);

		return activityDAO.create(activity);
	}

	@Override
	@TransactionAttribute
	public void updateActivity(Activity activity) throws UniqueValidationException {
		activityDAO.validateUniqueConstraint(activity);

		activityDAO.update(activity);
	}

	@Override
	@TransactionAttribute
	public void deleteActivity(Long id) {
		activityDAO.delete(id);
	}

	@Override
	public FilterResult<Overtime> searchOvertimes(OvertimeFilter filter) {
		List<Overtime> overtimes = overtimeDAO.listOvertime(filter);
		Long count = overtimeDAO.countOvertime(filter);
		return new FilterResult<Overtime>(overtimes, count);
	}

	@Override
	public BigDecimal getOvertimeHoursSumByFilter(OvertimeFilter filter) {
		return overtimeDAO.getSumOvertimeHours(filter);
	}

	@Override
	public Overtime getOvertime(Long id) {
		return overtimeDAO.find(id);
	}

	@Override
	@TransactionAttribute
	public Long addOvertime(Overtime overtime) {
		return overtimeDAO.create(overtime);
	}

	@Override
	@TransactionAttribute
	public void updateOvertime(Overtime overtime) {
		overtimeDAO.update(overtime);
	}

	@Override
	@TransactionAttribute
	public void deleteOvertime(Long id) {
		overtimeDAO.delete(id);
	}

	@Override
	public Timer getTimerByResource(Long resId) {
		return timerDAO.findByResource(resId);
	}

	@Override
	public Timer getTimerById(Long id){
		return timerDAO.find(id);
	}

	@Override
	@TransactionAttribute
	public Long addTimer(Timer timer) {
		return timerDAO.create(timer);
	}

	@Override
	@TransactionAttribute
	public void deleteTimer(Long id) {
		timerDAO.delete(id);
	}

	@Override
	@TransactionAttribute
	public void updateTimer(Timer timer) {
		timerDAO.update(timer);

	}

	@Override
	public Long addLoss(Loss loss) {
		return lossDAO.create(loss);
	}

	@Override
	public void updateLoss(Loss loss) {
		lossDAO.update(loss);
	}

	@Override
	public void deleteLoss(Long id) {
		lossDAO.delete(id);

	}

	@Override
	public FilterResult<Loss> searchLoss(LossFilter filter) {
		List<Loss> loss = lossDAO.listLoss(filter);
		Long count = lossDAO.countLoss(filter);
		return new FilterResult<Loss>(loss, count);
	}

	@Override
	public Loss getLoss(Long id) {
		return lossDAO.find(id);
	}

	@Override
	public List<LossType> lossTypeList() {
		return lossTypeDAO.findAll();
	}

	@Override
	public List<Loss> colleaguesLoss(Long resourceId) {
		// TODO Auto-generated method stub
		return new ArrayList<Loss>();
	}

	@Override
	public List<Loss> projectResourceLossNow(Long projectId) {
		// TODO Auto-generated method stub
		return new ArrayList<Loss>();
	}

	@Override
	public List<Loss> projectResourceLossFuture(Long projectId) {
		// TODO Auto-generated method stub
		return new ArrayList<Loss>();
	}

	@Override
	public LossType getLossType(Long pk) {
		return lossTypeDAO.find(pk);
	}

	@Override
	public LossStatus getLossStatus(Long id) {
		return lossStatusDAO.find(id);
	}

	@Override
	public List<LossStatus> lossStatusList() {
		return lossStatusDAO.findAll();
	}

	@Override
	public List<Loss> getCurrentLossByProject(Date date, Long projectPk) {
		return lossDAO.getCurrentLossByProject(date, projectPk);
	}

	@Override
	public List<Loss> getFutureLossByProject(Date date, Long projectPk) {
		return lossDAO.getFuturetLossByProject(date, projectPk);
	}

	@Override
	public List<Loss> getCurrentLossByLocation(Date date, String[] location) {
		return lossDAO.getCurrentLossByLocation(date, location);
	}

	@Override
	public BigDecimal getAllOvertimeSum(Long id) {
		return overtimeDAO.getAllOvertimeHours(id);
	}

	@Override
	public BigDecimal getLossDaysFromYear(Long resourceId, Integer year) {
		return lossDAO.getLossDayFromYear(resourceId, year);
	}

	@Override
	public List<Activity> listActivitiesByPosition(Long positionId) {
		return activityDAO.listActivitiesByPosition(positionId);

	}

	@Override
	public BigDecimal getHoursFromInterval(Long resourceId, Date startDate, Date endDate) {
		BigDecimal hours = activityDAO.getHoursFromInterval(resourceId, startDate, endDate);
		if(hours == null){
			hours = BigDecimal.ZERO;
		}
		return hours;
	}

	@Override
	@TransactionAttribute
	public void deleteActivityAndRequest(Long id) {
		DeleteRequest dr = resourceService.getRequestByTypeAndId(ResourceService.DELETE_REQUEST_TYPE_ACTIVITY, id);
		if (dr != null) {
			resourceService.deleteRequest(dr.getPk());
		}
		deleteActivity(id);
	}

	@Override
	@TransactionAttribute
	public void deleteOvertimeAndRequest(Long id) {
		DeleteRequest dr = resourceService.getRequestByTypeAndId(ResourceService.DELETE_REQUEST_TYPE_OVERTIME, id);
		if (dr != null) {
			resourceService.deleteRequest(dr.getPk());
		}
		deleteOvertime(id);
	}

	@Override
	@TransactionAttribute
	public void deleteLossAndRequest(Long id) {
		DeleteRequest dr = resourceService.getRequestByTypeAndId(ResourceService.DELETE_REQUEST_TYPE_LOSS, id);
		if (dr != null) {
			resourceService.deleteRequest(dr.getPk());
		}
		deleteLoss(id);
	}

}
