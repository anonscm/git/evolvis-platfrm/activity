/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.interceptors;

import java.lang.reflect.Method;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.hibernate.validator.method.MethodValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExceptionInterceptor.
 * 
 */
public class ValidationInterceptor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationInterceptor.class);

	@Resource
	private Validator validator;

	@Resource
	private SessionContext sessionContext;

	@SuppressWarnings("unchecked")
	@AroundInvoke
	public Object handleValidation(final InvocationContext ejbContext) throws Exception {
		long start = System.currentTimeMillis();
		Class<?> bvalClazzToValidate = ejbContext.getTarget().getClass();
		if (this.sessionContext != null && ejbContext.getTarget().getClass().getInterfaces().length > 0) {
			bvalClazzToValidate = this.sessionContext.getInvokedBusinessInterface();
		}
		Method method = ejbContext.getMethod();
		if (!bvalClazzToValidate.equals(ejbContext.getTarget().getClass())) {
			method = bvalClazzToValidate.getMethod(method.getName(), method.getParameterTypes());
		}

		MethodValidator methodValidator = this.validator.unwrap(MethodValidator.class);

		Set<?> violations = methodValidator.validateAllParameters(ejbContext.getTarget(), ejbContext.getMethod(), ejbContext.getParameters(),
				new Class[0]);
		if (violations.size() > 0) {
			logViolations((Set<ConstraintViolation<?>>) violations);
			throw new ConstraintViolationException((Set<ConstraintViolation<?>>) violations);
		}
		Object returnedValue = ejbContext.proceed();

		violations = methodValidator.validateReturnValue(ejbContext.getTarget(), ejbContext.getMethod(), returnedValue, new Class[0]);
		if (violations.size() > 0) {
			logViolations((Set<ConstraintViolation<?>>) violations);
			throw new ConstraintViolationException((Set<ConstraintViolation<?>>) violations);
		}
		
		LOGGER.debug("ValidationInterceptor.handleValidation took " + (System.currentTimeMillis() - start) + " ms");
		return returnedValue;
	}
	
	private static void logViolations(Set<ConstraintViolation<?>> cvs) {
		StringBuffer sb = new StringBuffer();
		for (ConstraintViolation<?> cv : cvs) {
			sb.append(cv.toString() + "\n");
		}
		LOGGER.error(sb.toString());
	}
}
