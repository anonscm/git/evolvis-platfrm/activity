/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.services.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import de.tarent.activity.dao.SettingsDAO;
import de.tarent.activity.dao.TaractInfoDAO;
import de.tarent.activity.domain.Settings;
import de.tarent.activity.domain.TaractInfo;
import de.tarent.activity.service.SettingsService;

/**
 * Implementation for SettingsService.
 * 
 */
@Stateless(name = "SettingsService")
@Remote(value = SettingsService.class)
//@RolesAllowed({ "user", "admin" })
public class SettingsServiceImpl implements SettingsService {

	@EJB
	private TaractInfoDAO taractInfoDao;

	@EJB
	private SettingsDAO settingsDAO;

	@Override
	public void saveSetting(Settings setting) {
		if (setting.getPk() != null) {
			settingsDAO.update(setting);
		} else {
			settingsDAO.create(setting);
		}
	}

	@Override
	public List<Settings> getSettings() {
		return settingsDAO.findAll();
	}

	@Override
//	@PermitAll
	public String getDBVersion() {
		List<TaractInfo> list = taractInfoDao.findAll();
		return (list != null && list.size() > 0) ? list.get(0).getDbversion() : null;
	}

	@Override
//	@PermitAll
	public String getDBCollation() {
		return taractInfoDao.getCollation();
	}

	@Override
	public Settings getSettingsByKey(String key) {
		return settingsDAO.getSettingsByKey(key);
	}

}
