/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.util;

import java.awt.Color;
import java.io.ByteArrayOutputStream;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;

import de.tarent.activity.domain.util.TableModel;

/**
 * Implementation for PdfWriter.
 * 
 */
public class DefaultPdfWriter implements PdfWriter {

	private Table table;
	private Font smallFont;

	/**
	 * Generates the header cells, which persist on every page of the PDF
	 * document.
	 * 
	 * @param model
	 *            model
	 * @throws BadElementException
	 *             IText exception
	 */
	protected void generateHeaders(TableModel model) throws BadElementException {
		for (String columnHeader : model.getHeaderCellList()) {
			Cell hdrCell = getCell(columnHeader);
			hdrCell.setGrayFill(0.9f);
			hdrCell.setHeader(true);
			table.addCell(hdrCell);
		}
	}

	private void generatePdfTable(TableModel model) throws BadElementException {
		generateHeaders(model);
		table.endHeaders();
		generateRows(model);
	}

	private void generateRows(TableModel model) throws BadElementException {
		for (Object[] item : model.getItems()) {
			for (Object itemAttribute : item) {
				Cell cell = getCell(itemAttribute.toString());
				table.addCell(cell);
			}
		}
	}

	/**
	 * Returns a formatted cell for the given value.
	 * 
	 * @param value
	 *            cell value
	 * @return Cell
	 * @throws BadElementException
	 *             errors while generating content
	 */
	private Cell getCell(String value) throws BadElementException {
		Cell cell = new Cell(new Chunk(value, smallFont));
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setLeading(8);

		return cell;
	}

	private void initTable(TableModel model) throws BadElementException {
		table = new Table(model.getNumberOfColumns());
		table.setCellsFitPage(true);
		table.setWidth(100);

		table.setPadding(2);
		table.setSpacing(0);

		smallFont = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL, new Color(0, 0, 0));
	}

	@Override
	public byte[] write(TableModel model) throws DocumentException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		// Initialize the table with the appropriate number of columns
		initTable(model);

		// Initialize the document
		Document document = new Document(PageSize.A4.rotate(), 60, 60, 40, 40);
		document.addCreationDate();
		HeaderFooter footer = new HeaderFooter(new Phrase(), true);
		HeaderFooter header = new HeaderFooter(new Phrase(model.getTableCaption()), false);
		footer.setBorder(Rectangle.NO_BORDER);
		footer.setAlignment(Element.ALIGN_CENTER);

		com.lowagie.text.pdf.PdfWriter.getInstance(document, out);

		// write data to document
		generatePdfTable(model);
		document.setHeader(header);
		document.open();
		document.setFooter(footer);
		document.add(this.table);
		document.close();

		return out.toByteArray();
	}
}
