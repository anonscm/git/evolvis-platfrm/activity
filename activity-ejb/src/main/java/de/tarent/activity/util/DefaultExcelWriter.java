/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tarent.activity.domain.util.TableModel;

/**
 * Implementation for ExcelWriter.
 * 
 */
public class DefaultExcelWriter implements ExcelWriter {
	/**
	 * Percent Excel format.
	 */
	private short pctFormat = HSSFDataFormat.getBuiltinFormat("0.00%");
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExcelWriter.class);
	private static final Integer CAPTION_CELL_STYLES = 0;
	private static final Integer HEADER_FOOTER_STYLE = 1;
	private static final Integer PERCENTAGE_CELL_STYLE = 2;
	private static final Integer DATE_CELL_STYLE = 3;

	private HSSFWorkbook wb;
	private HSSFSheet sheet;
	private HSSFRow currentRow;
	private HSSFCell currentCell;
	private int rowNum;
	private int colNum;
	private Map<Integer, HSSFCellStyle> cellStyles = new HashMap<Integer, HSSFCellStyle>();

	/**
	 * Constructor.
	 */
	public DefaultExcelWriter() {
		this.wb = new HSSFWorkbook();

		this.initCellStyles();
	}

	private void autosizeColumns() {
		LOGGER.debug("Autosizing columns");

		HSSFRow row = this.sheet.getRow(1);

		if (row != null) {
			for (short j = 0; j <= row.getLastCellNum(); j++) {
				sheet.autoSizeColumn(j);
			}
		}
	}

	private void initCaptionCellStyle() {
		HSSFCellStyle style = this.wb.createCellStyle();
		HSSFFont bold = this.wb.createFont();
		bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		bold.setFontHeightInPoints((short) 14);
		style.setFont(bold);
		style.setAlignment(CellStyle.ALIGN_CENTER);

		this.cellStyles.put(CAPTION_CELL_STYLES, style);
	}

	private void initCellStyles() {
		initCaptionCellStyle();
		initHeaderFooterCellStyle();
		initDataCellStyle();
	}

	private void initDataCellStyle() {
		HSSFCellStyle percentageStyle = this.wb.createCellStyle();
		percentageStyle.setDataFormat(this.pctFormat);
		this.cellStyles.put(PERCENTAGE_CELL_STYLE, percentageStyle);

		HSSFDataFormat df = this.wb.createDataFormat();
		HSSFCellStyle dateCellStyle = this.wb.createCellStyle();
		dateCellStyle.setDataFormat(df.getFormat("yyyy-MM-dd"));
		this.cellStyles.put(DATE_CELL_STYLE, dateCellStyle);
	}

	/**
	 * Initialize the style used to render a header or footer.
	 */
	protected void initHeaderFooterCellStyle() {
		// HSSFCellStyle style = this.wb.createCellStyle();
		// style.setFillPattern(CellStyle.FINE_DOTS);
		// style.setFillBackgroundColor(HSSFColor.BLUE_GREY.index);
		// HSSFFont bold = this.wb.createFont();
		// bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		// bold.setColor(HSSFColor.WHITE.index);
		// style.setFont(bold);

		HSSFCellStyle style = this.wb.createCellStyle();
		HSSFFont bold = this.wb.createFont();
		bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		bold.setFontHeightInPoints((short) 14);
		style.setFont(bold);
		style.setAlignment(CellStyle.ALIGN_CENTER);

		this.cellStyles.put(HEADER_FOOTER_STYLE, style);
	}

	private void rowSpanTable(TableModel model) {
		this.sheet.addMergedRegion(new CellRangeAddress(this.currentRow.getRowNum(), this.currentCell.getColumnIndex(),
				this.currentRow.getRowNum(), model.getNumberOfColumns()));
	}

	@Override
	public byte[] write(TableModel model) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		this.sheet = wb.createSheet(model.getTableCaption());
		writeExcel(model);
		wb.write(out);

		return out.toByteArray();
	}

	private void writeColumnOpener() {
		this.currentCell = this.currentRow.createCell(this.colNum++);
	}

	/**
	 * @param value
	 *            value
	 */
	protected void writeColumnValue(Object value) {
		if (value instanceof Number) {
			Number num = (Number) value;
			// Percentage
			if (value.toString().indexOf("%") > -1) {
				this.currentCell.setCellValue(num.doubleValue() / 100);
				this.currentCell.setCellStyle(this.cellStyles.get(PERCENTAGE_CELL_STYLE));
			} else {
				this.currentCell.setCellValue(num.doubleValue());
			}
		} else if (value instanceof Date) {
			this.currentCell.setCellValue((Date) value);
			this.currentCell.setCellStyle(this.cellStyles.get(DATE_CELL_STYLE));
		} else if (value instanceof Calendar) {
			this.currentCell.setCellValue((Calendar) value);
			this.currentCell.setCellStyle(this.cellStyles.get(DATE_CELL_STYLE));
		} else {
			// TODO maybe escape string
			this.currentCell.setCellValue(new HSSFRichTextString(value.toString()));
		}
	}

	private void writeExcel(TableModel model) {
		writeExcelCaption(model);
		if (model.getItemsDescription() != null) {
			writeExcelDescription(model);
		}
		writeExcelHeader(model);
		writeExcelBody(model);
		writeExcelFooter(model);
		autosizeColumns();
	}

	/**
	 * @param model
	 *            model
	 */
	protected void writeExcelBody(TableModel model) {
		LOGGER.debug("Writing " + model.getItems().size() + " rows.");

		for (Object[] item : model.getItems()) {
			writeRowOpener();
			for (Object itemAttribute : item) {
				writeColumnOpener();
				writeColumnValue(itemAttribute);
			}
		}
	}

	/**
	 * @param model
	 *            model
	 */
	protected void writeExcelDescription(TableModel model) {
		LOGGER.debug("Writing  description");
		writeRowOpener();
		for (Object[] item : model.getItemsDescription()) {
			writeRowOpener();
			for (Object itemAttribute : item) {
				writeColumnOpener();
				writeColumnValue(itemAttribute);
			}
		}
		writeRowOpener();
	}

	/**
	 * @param model
	 *            model
	 */
	protected void writeExcelCaption(TableModel model) {
		LOGGER.debug("Writing caption");
		this.colNum = 0;
		this.currentRow = this.sheet.createRow(this.rowNum++);
		this.currentCell = this.currentRow.createCell(this.colNum);
		this.currentCell.setCellStyle(cellStyles.get(CAPTION_CELL_STYLES));
		String caption = model.getTableCaption();
		this.currentCell.setCellValue(new HSSFRichTextString(caption));
		this.rowSpanTable(model);
	}

	/**
	 * @param model
	 *            model
	 */
	protected void writeExcelFooter(TableModel model) {
		LOGGER.debug("Writing footer");
		this.currentRow = this.sheet.createRow(this.rowNum++);
		this.colNum = 0;
		writeHeaderFooterCell(model.getTableFooter(), this.currentRow, cellStyles.get(HEADER_FOOTER_STYLE));
	}

	/**
	 * @param model
	 *            model
	 */
	protected void writeExcelHeader(TableModel model) {
		LOGGER.debug("Writing header: " + model.getNumberOfColumns() + " columns");
		writeRowOpener();
		for (String headerCell : model.getHeaderCellList()) {
			this.writeHeaderFooterCell(headerCell, this.currentRow, cellStyles.get(HEADER_FOOTER_STYLE));
		}
	}

	private void writeHeaderFooterCell(String value, HSSFRow row, HSSFCellStyle style) {
		this.currentCell = row.createCell(this.colNum++);
		this.currentCell.setCellValue(new HSSFRichTextString(value));
		this.currentCell.setCellStyle(style);
	}

	private void writeRowOpener() {
		this.currentRow = this.sheet.createRow(this.rowNum++);
		this.colNum = 0;
	}
}
