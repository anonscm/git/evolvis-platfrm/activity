<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="onchangeFunction" required="true" rtexprvalue="true"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/>:</label>
<select name="${name}" id="${form}-${name}" class="${styleClass}"
	<c:if test="${not empty onchangeFunction}">
		 onchange="${onchangeFunction}"
	</c:if>>
	<option value=""><spring:message code="all"/></option>
	<option value="1"><spring:message code="record"/></option>
	<option value="-1"><spring:message code="reduce"/></option>
</select>
