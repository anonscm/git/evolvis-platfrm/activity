<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="requiredMessageCode" required="true"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true"%>
<%@ attribute name="jobsList" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="disabled" required="false" rtexprvalue="true"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<select name="${name}" id="${form}-${name}" 
	<c:if test="${onChange!=null}"> onchange='${onChange}'</c:if>
	<c:if test="${focus}"> focus='true'</c:if> 
	<c:if test="${disabled}"> disabled="disabled"</c:if>
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if>
	<c:if test="${required}"> required="required"</c:if>
>
	<option value=""><spring:message code="select_please"/></option>
	<c:forEach var="job" items="${jobsList}" varStatus="myStatus">
		<c:choose>
		<c:when test="${myStatus.begin}" >
			<optgroup label="<c:out value="${job.project.name}"></c:out>">
				<option value="${job.pk}" 
					<c:choose>
						<c:when test="${selectedValue eq job.pk }">
							selected="selected" 
						</c:when>
					</c:choose>
				><c:out value="${job.name}"></c:out></option>
		</c:when>
		<c:otherwise> 
			<c:if test="${!myStatus.last}">
				<c:choose>
					<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
						<option value="${job.pk}"
							<c:choose>
								<c:when test="${selectedValue eq job.pk }">
									selected="selected" 
								</c:when>
							</c:choose>
						><c:out value="${job.name}"></c:out></option>	
					</c:when>
					<c:otherwise>
						</optgroup>
						<optgroup label="<c:out value="${job.project.name}"></c:out>">
							<option value="${job.pk}" 
								<c:choose>
									<c:when test="${selectedValue eq job.pk }">
										selected="selected" 
									</c:when>
								</c:choose>
							><c:out value="${job.name}"></c:out></option>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${myStatus.last}" >
				<c:choose>
						<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
								<option value="${job.pk}"
									<c:choose>
										<c:when test="${selectedValue eq job.pk }">
											selected="selected" 
										</c:when>
									</c:choose>
								><c:out value="${job.name}"></c:out></option>
							</optgroup>	
						</c:when>
						<c:otherwise>
							</optgroup>
							<optgroup label="<c:out value="${job.project.name}"></c:out>">
								<option value="${job.pk}"
									<c:choose>
										<c:when test="${selectedValue eq job.pk }">
											selected="selected" 
										</c:when>
									</c:choose>
								><c:out value="${job.name}"></c:out></option>
							</optgroup>
						</c:otherwise>
				</c:choose>
			</c:if>
		</c:otherwise>
		</c:choose>
	</c:forEach>
</select>
<c:if test="${disabled}"> 
	<input type="hidden" name="${name}" value="${selectedValue}">
</c:if>
<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
	<spring:message code="${requiredMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" ></form:errors>
<c:if test="${focus}">
<script type="text/javascript">
if(!("autofocus" in document.createElement("input"))) {
	document.getElementById("${form}-${name}").focus();
}
</script>
</c:if>