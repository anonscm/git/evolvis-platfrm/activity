<%@ tag body-content="empty"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="requiredMessageCode" required="false"%>
<%@ attribute name="linkMessageCode" required="false"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="maxlength" required="true" type="Long"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="allowWebdav" required="false" type="Boolean"%>

<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<input type="text" name="${name}" id="${form}-${name}" maxlength="${maxlength}"  
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if>
	<c:if test="${not empty value}"> value="${fn:escapeXml(value)}"</c:if> 
	<c:if test="${required}"> required="required"</c:if>
/>
<c:if test="${required}">
	<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
		<spring:message code="${requiredMessageCode}"/>
	</div>
</c:if>
<c:choose>
	<c:when test="${allowWebdav ne null and allowWebdav eq true}">
		<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-linkWebdav">
			<spring:message code="error.projectView.link_webdav"/>
		</div>	
	</c:when>
	<c:otherwise>
		<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-link">
			<spring:message code="${linkMessageCode}"/>
		</div>	
	</c:otherwise>
</c:choose>

<form:errors path="${name}" cssClass="portlet-msg-error" />
<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${form}-${name}").focus();
	}
	</script>
</c:if>