<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="jobsList" required="false" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true" %>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/>:</label>
<select name="${name}" id="${form}-${name}" class="default" onchange="${onchangeFunction}">
	<option value="" selected="selected"><spring:message code="all_test"/></option>
	<c:choose>
		<c:when test="${fn:length(jobsList)>0}">
			<c:forEach var="job" items="${jobsList}">
				<option value="${job.pk}" 
					<c:choose>
						<c:when test="${selectedValue eq job.pk }">
							selected="selected" 
						</c:when>
					</c:choose>
					><c:out value="${job.name}"></c:out></option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">jQuery('#${form}-${name}').attr('disabled','disabled');</script>
		</c:otherwise>
	</c:choose>
</select>

<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${form}-${name}").focus();
	}
	</script>
</c:if>