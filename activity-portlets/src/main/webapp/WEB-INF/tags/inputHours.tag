<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="value" required="false" rtexprvalue="true"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="validationMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="style" required="false"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="disabled" required="false" rtexprvalue="true"%>

<c:set var="id" value="${form}-${name}"/>

<label for="${id}"><spring:message code="${labelCode}" /><c:if test="${required}">*</c:if>:</label>
<input type="number" name="${name}" id="${id}" min="0.0"
	max="12" step="0.25" pattern="^(:?1?[0-9]|2[0-4])(:?[.,](:?00?|25|50?|75))?$"
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if>
	<c:if test="${not empty style}"> style="${style}"</c:if>
	<c:if test="${required}"> required="required"</c:if>
	<c:if test="${not empty value}"> value="${value}"</c:if>
	<c:if test="${disabled}"> readonly="readonly"</c:if> />

<div id="${id}-slider"></div>

<script>
	jQuery(function() {
		jQuery("#${id}-slider").slider({
			value : ${value},
			min : 0.0,
			max : 12,
			step : 0.25,
			slide : function(event, ui) {
				jQuery("#${id}").val(ui.value);
			}
		});
		jQuery("#${id}").val(jQuery("#${id}-slider").slider("value"));
	});
</script>

<div class="portlet-msg-error yui-pe-content"
	id="${id}-activityHours">
	<spring:message code="${validationMessageCode}" />
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" />

<c:if test="${focus}">
	<script type="text/javascript">
		if (!("autofocus" in document.createElement("input"))) {
			document.getElementById("${id}").focus();
		}
	</script>
</c:if>