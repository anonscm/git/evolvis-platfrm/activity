<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- required attributes of this tag --%>
<%@ attribute name="ns" required="true"%>
<%@ attribute name="projectStateList" required="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true" %>

<%-- optional attributes of this tag --%>
<%@ attribute name="labelCode" description="Default value: select_resource"%>
<%@ attribute name="focus" type="Boolean"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-stateId"/>

<%-- set default label code (select_state) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_state"/>
</c:if>

<label for="${id}"><spring:message code="${labelCode}"/>:</label>

<select id="${id}" name="${name}" onchange="${onchangeFunction}">
	<option value=""><spring:message code="all"/></option>
	<c:forEach var="projectState" items="${projectStateList}" >
		<option value="${projectState.pk}">
			<spring:message code="${projectState.name}" />
		</option>
	</c:forEach>
</select>

<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${id}").focus();
	}
	</script>
</c:if>
