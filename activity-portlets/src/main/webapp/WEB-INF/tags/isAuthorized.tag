<%@tag language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@tag import="java.util.Arrays"%>
<%@tag import="java.lang.String"%>
<%@tag import="de.tarent.activity.domain.Resource"%>
<%@tag import="de.tarent.activity.service.PermissionService"%>
<%@tag import="de.tarent.activity.web.util.RequestUtil"%>
<%@tag import="org.springframework.web.context.WebApplicationContext"%>
<%@tag import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@attribute name="var" required="true" rtexprvalue="false" description="Set a variable with the result of the permission check (true = authorized, false = not authorized)"%>
<%@attribute name="permissions" description="A comma separated list of permissions, one of which the user must posses for the body to be output."%>
<%@variable name-from-attribute="var" alias="v" variable-class="java.lang.Boolean" scope="AT_END"%>  

<portlet:defineObjects/>

<%
	Resource res = null;
	if (renderRequest != null) {
		res = RequestUtil.getLoggedUser(renderRequest);
	} else if (resourceRequest != null) {
		res = RequestUtil.getLoggedUser(resourceRequest);
	}
	
	WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(application);
    PermissionService permChecker = (PermissionService)context.getBean(PermissionService.class);
%>
<c:choose>
	<c:when test="${permissions != null && not empty permissions}">
		<%
			String[] perm = permissions.split(",");
			for (int i = 0; i < perm.length; i++){
				perm[i] = perm[i].trim();
			}
		%>
		<c:set var="v" value="<%= permChecker.checkPermission(res.getPk(), perm) %>" />
	</c:when>
	<c:otherwise>
		<c:set var="v" value="${true}" />
	</c:otherwise>
</c:choose>