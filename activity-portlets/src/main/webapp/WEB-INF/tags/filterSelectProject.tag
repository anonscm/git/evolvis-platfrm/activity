<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%-- required attributes of this tag --%>
<%@ attribute name="ns" required="true"%>
<%@ attribute name="projectList" required="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true" %>

<%-- optional attributes of this tag --%>
<%@ attribute name="labelCode" description="Default value: select_project"%>
<%@ attribute name="focus" type="Boolean"%>
<%@ attribute name="name" type="String"%>
<%@ attribute name="disabled"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-projectId"/>

<%-- set default label code (select_project) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_project"/>
</c:if>

<label for="${id}"><spring:message code="${labelCode}"/>:</label>

<select id="${id}" name="${name}" onchange="${onchangeFunction}" <c:if test="${disabled}"> disabled="disabled"</c:if>>
	<option value=""><spring:message code="all"/></option>
	<c:forEach var="project" items="${projectList}" >
		<option value="${project.pk}">
			<c:out value="${project.name}"></c:out>
		</option>
	</c:forEach>
</select>

<%-- set focus --%>
<c:if test="${focus}">
	<script type="text/javascript">
		if(!("autofocus" in document.createElement("input"))) {
			document.getElementById("${id}").focus();
		}
	</script>
</c:if>
