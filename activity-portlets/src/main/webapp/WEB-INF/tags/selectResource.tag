<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="requiredMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="resourceList" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>

<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<select name="${name}" id="${form}-${name}" class="${styleClass}">
	<c:choose>
		<c:when test="${fn:length(resourceList)>0}">
			<option value="" selected="selected"><spring:message code="select_please"/></option>
			<c:forEach var="resource" items="${resourceList}">
				<option value="${resource.pk}" <c:if test="${selectedValue eq resource.pk }">selected="selected"</c:if>><c:out value="${resource.lastname}, ${resource.firstname}"></c:out></option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<option value="" selected="selected"><spring:message code="select_please"/></option>
		</c:otherwise>
	</c:choose>
</select>
<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
	<spring:message code="${requiredMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" ></form:errors>
<c:if test="${focus}">
<script type="text/javascript">
if(!("autofocus" in document.createElement("input"))) {
	document.getElementById("${form}-${name}").focus();
}
</script>
</c:if>