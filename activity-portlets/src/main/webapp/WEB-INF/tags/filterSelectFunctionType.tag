<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="functionTypeList" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/>:</label>
<select name="${name}" id="${form}-${name}" class="${styleClass}" onchange="${onchangeFunction}">
	<option value=""><spring:message code="option.all"/></option>
	<c:forEach var="functionType" items="${functionTypeList}" >
		<option value="${functionType.pk}">
			<c:out value="${functionType.name}"></c:out>
		</option>
	</c:forEach>
</select>

<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${form}-${name}").focus();
	}
	</script>
</c:if>