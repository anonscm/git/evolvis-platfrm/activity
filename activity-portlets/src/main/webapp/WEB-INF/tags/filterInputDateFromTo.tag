<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="ns" required="true" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="formId" required="true" %>
<%@ attribute name="dataTable" required="true" %>

<%-- set base id for select boxes --%>
<c:set var="id" value="${ns}-select-${name}"/>

<fmt:formatDate var="dateToDisplay" value="${value}" pattern="dd.MM.yyyy" />
<fmt:formatDate var="dateToSend" value="${value}" pattern="yy-mm-dd" />

<div class="layout-cell">
<label for="${id}_from"><spring:message code="start_date"/>:</label>

<input type="text" id="${id}_from" name="${name}_from" value="${dateToDisplay}" />
<div class="portlet-msg-error yui-pe-content" id="${id}_from_date">
	<spring:message code="typeMismatch.java.util.Date"/>
</div>

<%-- Use input type='hidden' to post date in format that is expected by back-end. This field is set by date-picker. --%>
<input type="hidden" name="${name}_from_alt" id="${id}_from_alt" value="${dateToSend}" />

<script type="text/javascript">
 	//use js datepicker								
	jQuery('#${id}_from').datepicker({
							closeText: '<spring:message code="close"/>',
							prevText: '<spring:message code="back"/>',
							nextText: '<spring:message code="forward"/>',
							currentText: '<spring:message code="today"/>',
							monthNames: ['Januar','Februar','März','April','Mai','Juni',
							 			'Juli','August','September','Oktober','November','Dezember'],
							monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
							dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
							dayNamesShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
							dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
							dateFormat: 'dd.mm.yy',
							altFormat: 'yy-mm-dd',
							altField: '#${id}_from_alt',
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							yearRange: '-10:+10',
							firstDay: 1,
							onSelect: function(selected){
								jQuery('#${id}_to').datepicker('option', 'minDate', selected);
								jQuery('#${id}_from').blur();
								${ns}filterByStartDate();
							}
	}).keyup(function(e) {
   		if(e.keyCode == 8 || e.keyCode == 46) {
        	jQuery.datepicker._clearDate(this);
    	}
	});
	
	//start date
	var ${ns}filterByStartDate = function(){
		var valid = YAHOO.de.tarent.validate('${formId}', [{id:'${id}_from_alt', validator:'date'}]);
		if (valid == true && YAHOO.de.tarent.validateStartBeforEnd('${formId}', '${id}_from_alt', '${id}_to_alt') == true) {
			var newDate = document.getElementById('${formId}').elements['${id}_from_alt'].value;
			YAHOO.de.tarent.${ns}filter.startdate = newDate;
			${dataTable}.reloadData();
			return true;
		} 
		return false;
	};

</script>
</div>
<div class="layout-cell">
	
	<label for="${id}_to"><spring:message code="end_date"/>:</label>

	<input type="text" id="${id}_to" name="${name}_to" value="${dateToDisplay}" />
	<div class="portlet-msg-error yui-pe-content" id="${id}_to_date">
		<spring:message code="typeMismatch.java.util.Date"/>
	</div>

	<%-- Use input type='hidden' to post date in format that is expected by back-end. This field is set by date-picker. --%>
	<input type="hidden" name="${name}_to_alt" id="${id}_to_alt" value="${dateToSend}" />
	
	<script type="text/javascript">
 	//use js datepicker		
	jQuery('#${id}_to').datepicker({
							closeText: '<spring:message code="close"/>',
							prevText: '<spring:message code="back"/>',
							nextText: '<spring:message code="forward"/>',
							currentText: '<spring:message code="today"/>',
							monthNames: ['Januar','Februar','März','April','Mai','Juni',
							 			'Juli','August','September','Oktober','November','Dezember'],
							monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
							dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
							dayNamesShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
							dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
							dateFormat: 'dd.mm.yy',
							altFormat: 'yy-mm-dd',
							altField: '#${id}_to_alt',
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							yearRange: '-10:+10',
							firstDay: 1,
							onSelect: function(selected) {
					            jQuery('#${id}_from').datepicker('option', 'maxDate', selected);
								jQuery('#${id}_to').blur();
								${ns}filterByEndDate();
							}
	}).keyup(function(e) {
   		if(e.keyCode == 8 || e.keyCode == 46) {
        	jQuery.datepicker._clearDate(this);
    	}
	});
	
	//end date
	var ${ns}filterByEndDate = function(){
		var valid = YAHOO.de.tarent.validate('${formId}', [{id:'${id}_to_alt', validator:'date'}]);
		if (valid == true && YAHOO.de.tarent.validateStartBeforEnd('${formId}', '${id}_from_alt', '${id}_to_alt') == true) {
			var newDate = document.getElementById('${formId}').elements['${id}_to_alt'].value;
			YAHOO.de.tarent.${ns}filter.enddate = newDate;
			${dataTable}.reloadData();
			return true;
		}
		return false;
	};
</script>
</div>