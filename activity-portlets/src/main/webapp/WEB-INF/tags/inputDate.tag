<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true" type="java.util.Date"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="requiredMessageCode" required="false"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="disabled" required="false" rtexprvalue="true"%>
<%@ attribute name="onchangeFunction" required="false"%>

<c:set value="${form}-${name}" var="id"/>

<fmt:formatDate var="dateToDisplay" value="${value}" pattern="dd.MM.yyyy" />
<fmt:formatDate var="dateToSend" value="${value}" pattern="yyyy-MM-dd" />

<label for="${id}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>

<%--
	Note: Use input type='text' otherwise the date-picker does not work in chrome browser.
	Also Note: This input field is only for entering and displaying date in locale date format. The date that will be
	posted to back-end must be in always have a yyyy-MM-dd format. See next input type='hidden' field!
--%>
<input type="text" id="${id}" value="${dateToDisplay}"
	<c:if test="${required}"> required="required" </c:if>
	<c:if test="${disabled}"> readonly="readonly"</c:if>
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if>
/>
<%-- Use input type='hidden' to post date in format that is expected by back-end. This field is set by date-picker. --%>
<input type="hidden" name="${name}" id="${id}-alt" value="${dateToSend}" />

<c:if test="${required}">
	<div class="portlet-msg-error yui-pe-content" id="${id}-required">
		<spring:message code="${requiredMessageCode}"/>
	</div>
</c:if>
<div class="portlet-msg-error yui-pe-content" id="${id}-date">
	<spring:message code="typeMismatch.java.util.Date"/>
</div>
<div class="portlet-msg-error yui-pe-content" id="${id}-dateInThePast">
	<spring:message code="validate_date_future"/>
</div>

<form:errors path="${name}" cssClass="portlet-msg-error" />

<script type="text/javascript">
jQuery('#${id}').datepicker({
							closeText: '<spring:message code="close"/>',
							prevText: '<spring:message code="back"/>',
							nextText: '<spring:message code="forward"/>',
							currentText: '<spring:message code="today"/>',
							monthNames: ['Januar','Februar','März','April','Mai','Juni',
							 			'Juli','August','September','Oktober','November','Dezember'],
							monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
							dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
							dayNamesShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
							dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
							dateFormat: 'dd.mm.yy',
							altFormat:'yy-mm-dd',
							altField:'#${id}-alt',
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							yearRange: '-10:+10',
							firstDay: 1,
							onSelect: function(){
								jQuery('#${id}').blur();
								${onchangeFunction}
							}
	}).keyup(function(e) {
   		if(e.keyCode == 8 || e.keyCode == 46) {
        	jQuery.datepicker._clearDate(this);
    	}
	});

jQuery('input[id=${id}]').change(function() {
	if(!jQuery('input[id=${id}]').val()) {
		jQuery('input[name=${name}]').val("");
	}
});
</script>

<c:if test="${focus}">
<script type="text/javascript">
if(!("autofocus" in document.createElement("input"))) {
	document.getElementById("${id}").focus();
}
</script>
</c:if>