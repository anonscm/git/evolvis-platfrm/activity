<%@tag language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@tag import="java.util.Arrays"%>
<%@tag import="java.lang.String"%>
<%@tag import="de.tarent.activity.domain.Resource"%>
<%@tag import="de.tarent.activity.service.PermissionService"%>
<%@tag import="de.tarent.activity.web.util.RequestUtil"%>
<%@tag import="org.springframework.web.context.WebApplicationContext"%>
<%@tag import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@attribute name="ifAnyGranted" description="A comma separated list of permissions, one of which the user must posses for the body to be output."%>

<portlet:defineObjects/>

<%
	Resource res = null;
	if (renderRequest != null) {
		res = RequestUtil.getLoggedUser(renderRequest);
	} else if (resourceRequest != null) {
		res = RequestUtil.getLoggedUser(resourceRequest);
	}
	
	WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(application);
    PermissionService permChecker = (PermissionService)context.getBean(PermissionService.class);
%>
<c:choose>
	<c:when test="${ifAnyGranted != null && not empty ifAnyGranted}">
		<%
			String[] permissions = ifAnyGranted.split(",");
			for (int i = 0; i < permissions.length; i++){
				permissions[i] = permissions[i].trim();
			}
		%>
		<c:if test="<%= permChecker.checkPermission(res.getPk(), permissions) %>">
			<jsp:doBody/>
		</c:if>
	</c:when>
	<c:otherwise>
		<% /* When ifAnyGrant is empty or not set we does not check any permission and show the body! */ %>
		<jsp:doBody/>
	</c:otherwise>
</c:choose>