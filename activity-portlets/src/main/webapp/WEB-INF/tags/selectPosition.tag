<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="requiredMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="onChange" required="false"%>
<%@ attribute name="positionsList" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="disabled" required="false" rtexprvalue="true"%>
<%@ attribute name="posForActivities" required="false" rtexprvalue="true"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<select name="${name}" id="${form}-${name}" <c:if test="${onChange!=null}"> onchange='${onChange}'</c:if> class="${styleClass}" <c:if test="${disabled}"> disabled="disabled"</c:if>>
	<option value="" selected="selected"><spring:message code="select_please"/></option>
	<c:choose>
		<c:when test="${fn:length(positionsList)>0}">
			<c:forEach var="position" items="${positionsList}">
				<option value="${position.pk}" 
					<c:if test="${selectedValue eq position.pk }"> selected="selected"</c:if>
				><c:out value="${position.name}"></c:out></option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">jQuery('#${form}-${name}').attr('disabled','disabled');</script>
		</c:otherwise>
	</c:choose>
</select>
<c:if test="${disabled}"> 
	<input type="hidden" name="${name}" value="${selectedValue}">
</c:if>

<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
	<spring:message code="${requiredMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" ></form:errors>
<c:if test="${focus}">
<script type="text/javascript">
if(!("autofocus" in document.createElement("input"))) {
	document.getElementById("${form}-${name}").focus();
}
</script>
</c:if>