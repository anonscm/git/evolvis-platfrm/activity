<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="requiredMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="required" required="false" type="Boolean"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="projectList" required="true" rtexprvalue="true" type="java.util.List"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="onChange" required="false" rtexprvalue="true"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<select name="${name}" id="${form}-${name}" class="${styleClass}" 
	<c:if test="${onChange != ''}">onchange="${onChange}"</c:if>>
	<option value=""><spring:message code="select_please"/></option>
	<c:forEach var="project" items="${projectList}" >
		<option value="${project.pk}" 
			<c:if test="${selectedValue eq project.pk }">
				selected="selected" 
			</c:if>>
			<c:out value="${project.name}"></c:out>
		</option>
	</c:forEach>
</select>
<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
	<spring:message code="${requiredMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" ></form:errors>
<script type="text/javascript">
if(!("autofocus" in document.createElement("input"))) {
	document.getElementById("${form}-${name}").focus();
}
</script> 