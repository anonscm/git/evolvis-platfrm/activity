<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="formId" required="true" rtexprvalue="true"%>
<%@ attribute name="divId" required="true" rtexprvalue="true"%>
<%@ attribute name="saved" required="false" type="java.lang.Boolean"%>
<%@ attribute name="provideFeedback" required="false" rtexprvalue="true"%>
<%@ attribute name="feedbackMessage" required="false" rtexprvalue="true"%>

<c:if test="${provideFeedback eq true and empty feedbackMessage }">
	<c:set var="feedbackMessage">
		<fmt:message key="success" />
	</c:set>
</c:if>

<script type="text/javascript">
var status = '${saved}';
function feedback(provideFeedback, message){
	
 	if(status=='true' && provideFeedback == 'true'){
 		jQuery('#ajax-messages').jnotifyAddMessage({
           text: message,
           permanent: false,
           showIcon: true
       });
 	}	     
}

var form${formId} = jQuery('#${formId}');
form${formId}.bind("submit",function(event) {
	if(form${formId}.attr('isFormValid') == "false") {
		return;
	}
	event.preventDefault();
	var inputs = jQuery("input, textarea, select", form${formId});
	
	jQuery('#${divId}').html('<div class="loading-animation"></div>').load(form${formId}.attr('action') + "&" + form${formId}.serialize(), 
			function() {
		        feedback('${provideFeedback}', '${feedbackMessage}');
		    });
});
</script>
