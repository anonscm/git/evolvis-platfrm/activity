<%@tag description="Synchronizer Token" import="de.tarent.activity.web.util.TokenUtil" %>

<input 
	type="hidden" 
	name="<%=TokenUtil.REQUEST_TOKEN_KEY%>" 
	value='<%=request.getAttribute(TokenUtil.REQUEST_TOKEN_KEY)%>' 
/>