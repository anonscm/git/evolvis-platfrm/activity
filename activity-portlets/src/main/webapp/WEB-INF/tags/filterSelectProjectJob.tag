<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="ns" required="true" description="Unique namespace." %>
<%@ attribute name="onchangeFunction" required="true" %>
<%@ attribute name="jobsList" required="true" type="java.util.List"%>
<%@ attribute name="selectedValue" %>
<%@ attribute name="labelCode" description="Default value: select_job"%>
<%@ attribute name="focus" type="Boolean"%>
<%@ attribute name="name" type="String"%>
<%@ attribute name="disabled"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-jobId"/>
<c:if test="${empty name}">
    <c:set var="name" value="jobId"/>
</c:if>

<%-- set default label code (select_job) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_job"/>
</c:if>

<label for="${id}"><spring:message code="${labelCode}"/>:</label>

<select id="${id}" name="${name}" onchange='${onchangeFunction}' <c:if test="${disabled}"> disabled="disabled"</c:if>>
	<option id="${id}-first" value=""><spring:message code="all"/></option>
	<c:choose>
		<c:when test="${fn:length(jobsList) > 0}">
			<c:forEach var="job" items="${jobsList}" varStatus="myStatus">
				<c:choose>
					<c:when test="${myStatus.begin}" >
						<optgroup label="${job.project.name}">
							<option value="${job.pk}" 
								<c:if test="${activityView.jobId eq job.pk }"> selected="selected" </c:if>
									><c:out value="${job.name}"></c:out>
							</option>
					</c:when>
					<c:when test="${!myStatus.last}">
						<c:choose>
							<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
								<option value="${job.pk}"
									<c:if test="${selectedValue eq job.pk }"> selected="selected" </c:if>
										><c:out value="${job.name}"></c:out>
								</option>	
							</c:when>
							<c:otherwise>
								</optgroup>
								<optgroup label="${job.project.name}">
									<option value="${job.pk}" 
										<c:if test="${selectedValue eq job.pk }"> selected="selected" </c:if>
											><c:out value="${job.name}"></c:out>
									</option>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise> 
						<c:choose>
							<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
								<option value="${job.pk}"
									<c:if test="${selectedValue eq job.pk }"> selected="selected" </c:if>
									><c:out value="${job.name}"></c:out>
								</option>
								</optgroup>	
							</c:when>
							<c:otherwise>
								</optgroup>
								<optgroup label="${job.project.name}">
									<option value="${job.pk}"
										<c:if test="${selectedValue eq job.pk }"> selected="selected" </c:if>
									><c:out value="${job.name}"/></option>
								</optgroup>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">jQuery('#${id}').attr('disabled','disabled');</script>
		</c:otherwise>
	</c:choose>
</select>

<%-- set focus --%>
<c:if test="${focus}">
	<script type="text/javascript">
		if(!("autofocus" in document.createElement("input"))) {
			document.getElementById("${id}").focus();
		}
	</script>
</c:if>
