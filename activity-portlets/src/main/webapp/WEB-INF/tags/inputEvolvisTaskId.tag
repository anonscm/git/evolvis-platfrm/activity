<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="validationMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="style" required="false"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<label for="${form}-${name}"><spring:message code="${labelCode}"/>:</label>
<input type="text" name="${name}" id="${form}-${name}" maxlength="255" value="${value}"
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if> 
	<c:if test="${not empty style}"> style="${style}"</c:if>  
/>

<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-evolvisId">
	<spring:message code="${validationMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" />

<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${form}-${name}").focus();
	}
	</script>
</c:if>