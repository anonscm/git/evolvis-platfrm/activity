<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="ns" required="true"%>
<%@ attribute name="positionsList" required="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true" %>
<%@ attribute name="selectedValue"%>
<%@ attribute name="labelCode" description="Default value: select_position"%>
<%@ attribute name="focus" type="Boolean"%>
<%@ attribute name="name" type="String"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-positionId"/>
<c:if test="${empty name}">
    <c:set var="name" value="positionId"/>
</c:if>

<%-- set default label code (select_position) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_position"/>
</c:if>

<label for="${id}"><spring:message code="${labelCode}"/>:</label>

<select id="${id}" name="${name}" onchange="${onchangeFunction}">
	<option value=""><spring:message code="all"/></option>
	<c:choose>
		<c:when test="${fn:length(positionsList)>0}">
			<c:forEach var="position" items="${positionsList}" >
				<option value="${position.id}">
					<c:out value="${position.positionName}"></c:out>
				</option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">jQuery('#${id}').attr('disabled','disabled');</script>
		</c:otherwise>
	</c:choose>
</select>

<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${id}").focus();
	}
	</script>
</c:if>
