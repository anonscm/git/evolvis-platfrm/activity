<%@ tag body-content="empty"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="onchangeFunction" required="false" %>
<%@ attribute name="name" required="true"%>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="required" required="true" type="Boolean"%>
<%@ attribute name="requiredMessageCode" required="false"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="maxlength" required="true" type="Long"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="type" required="false" %>

<c:if test="${empty type}">
	<c:set var="type" value="text"/>
</c:if>
<label for="${form}-${name}"><spring:message code="${labelCode}"/><c:if test="${required}">*</c:if>:</label>
<input type="${type}" name="${name}" id="${form}-${name}" maxlength="${maxlength}" onchange="${onchangeFunction}"
	<c:if test="${not empty styleClass}"> class="${styleClass}"</c:if> 
	<c:if test="${not empty value}"> value="${fn:escapeXml(value)}"</c:if> 
	<c:if test="${required}"> required="required" </c:if> 
/>
<c:if test="${required}">
	<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
		<spring:message code="${requiredMessageCode}"/>
	</div>
</c:if>
<form:errors path="${name}" cssClass="portlet-msg-error" />
<c:if test="${focus}">
	<script type="text/javascript">
	if(!("autofocus" in document.createElement("input"))) {
		document.getElementById("${form}-${name}").focus();
	}
	</script>
</c:if>


