<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="ns" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="onchangeFunction" required="true" %>
<%@ attribute name="labelCode" required="false"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-${name}"/>

<fmt:formatDate var="dateToDisplay" value="${value}" pattern="dd.MM.yyyy" />
<fmt:formatDate var="dateToSend" value="${value}" pattern="yy-mm-dd" />

<%-- set default label code (select_date) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_date"/>
</c:if>
<label for="${id}"><spring:message code="${labelCode}"/>:</label>

<%--
	Note: Use input type='text' otherwise the date-picker does not work in chrome browser.
	Also Note: This input field is only for entering and displaying date in locale date format. The date that will be
	posted to back-end must be in always have a yyyy-MM-dd format. See next input type='hidden' field!
--%>
<input type="text" id="${id}" name="${name}" value="${dateToDisplay}" />
<div class="portlet-msg-error yui-pe-content" id="${id}-date">
	<spring:message code="typeMismatch.java.util.Date"/>
</div>

<%-- Use input type='hidden' to post date in format that is expected by back-end. This field is set by date-picker. --%>
<input type="hidden" name="${name}-alt" id="${id}-alt" value="${dateToSend}" />

<script type="text/javascript">
 	//use js datepicker								
	jQuery('#${id}').datepicker({
							closeText: '<spring:message code="close"/>',
							prevText: '<spring:message code="back"/>',
							nextText: '<spring:message code="forward"/>',
							currentText: '<spring:message code="today"/>',
							monthNames: ['Januar','Februar','März','April','Mai','Juni',
							 			'Juli','August','September','Oktober','November','Dezember'],
							monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
							dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
							dayNamesShort: ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'],
							dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
							dateFormat: 'dd.mm.yy',
							altFormat: 'yy-mm-dd',
							altField: '#${id}-alt',
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							yearRange: '-10:+10',
							firstDay: 1,
							onSelect: function(){
								jQuery('#${id}').blur();
								${onchangeFunction}
							}
	}).keyup(function(e) {
   		if(e.keyCode == 8 || e.keyCode == 46) {
        	jQuery.datepicker._clearDate(this);
    	}
	});
</script>
