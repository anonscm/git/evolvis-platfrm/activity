<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="ns" required="true"%>
<%@ attribute name="resourceList" required="true" type="java.util.List"%>
<%@ attribute name="onchangeFunction" required="true"%>
<%@ attribute name="labelCode" description="Default value: select_resource"%>
<%@ attribute name="focus" type="Boolean"%>
<%@ attribute name="selectedValue" type="Double"%>

<%-- set unique id for select box --%>
<c:set var="id" value="${ns}-select-resourceId" />

<%-- set default label code (select_resource) --%>
<c:if test="${empty labelCode}">
	<c:set var="labelCode" value="select_resource" />
</c:if>

<label for="${id}"><spring:message code="${labelCode}" />:</label>

<select id="${id}" name="resourceId" onchange="${onchangeFunction}">
	<option value="">
		<spring:message code="all" />
	</option>
	<c:forEach var="resource" items="${resourceList}">
		<option value="${resource.pk}" <c:if test="${selectedValue eq resource.pk }">selected="selected"</c:if>>
			<c:out value="${resource.lastname}, ${resource.firstname}"></c:out>
		</option>
	</c:forEach>
</select>

<c:if test="${focus}">
	<script type="text/javascript">
		if (!("autofocus" in document.createElement("input"))) {
			document.getElementById("${id}").focus();
		}
	</script>
</c:if>

