<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="labelCode" required="true"%>
<%@ attribute name="requiredMessageCode" required="true"%>
<%@ attribute name="styleClass" required="false"%>
<%@ attribute name="form" required="true" rtexprvalue="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="selectedValue" required="true" rtexprvalue="true"%>
<%@ attribute name="focus" required="false" type="Boolean"%>
<%@ attribute name="inputSelectId" required="true" %>
<label for="${form}-${name}"><spring:message code="${labelCode}"/>:</label>
<div class="radioblock">
	<div class="radio-element">	
		<input type="radio" name="${name}" id="record" value="1" checked="checked" onchange="enableSelect()">
	    <label for="record"><spring:message code="record"/></label>
	</div>
	<div class="radio-element">
		<input type="radio" name="${name}" id="reduce" value="-1" onchange="disabledSelect()" 
				<c:if test="${selectedValue eq -1 }">
					checked="checked"
				</c:if>
		>
		<label for="reduce"><spring:message code="reduce"/></label>   
	</div>	
</div>	 
<div class="portlet-msg-error yui-pe-content" id="${form}-${name}-required">
	<spring:message code="${requiredMessageCode}"/>
</div>
<form:errors path="${name}" cssClass="portlet-msg-error" ></form:errors>
<script type="text/javascript">

function disabledSelect(){
	jQuery('#${inputSelectId}').attr('disabled', 'disabled');
}

function enableSelect(){
	jQuery('#${inputSelectId}').removeAttr('disabled');
}
</script>

<c:if test="${selectedValue eq -1 }">
	<script type="text/javascript">
	disabledSelect();
	</script>
</c:if>