<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
<h2><spring:message code="resource_skills"/></h2>
    <form:form method="POST" id="${ns}form" action="${add}" novalidate="novalidate">

	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-fifty">
				<label for="skillDefName"><spring:message code="skill"/></label>
				<input type="text" id="skillsDefName" name="skillDefName" onchange="${ns}filterBySkill(this.value);"/>
			</div>
			<div class="layout-cell cell-fifty">
				<label for="username"><spring:message code="user"/></label>
				<input type="text" id="resourceName" name="username" onchange="${ns}filterByResource(this.value);"/>	    			
			</div>			
		</div>
	</div>
	
	<div id="${ns}form-skills" class="clearDiv"></div>
	    		
	</form:form>
</div>  


<script type="text/javascript">

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		resourceName :'',
		skillsDefName :''
	};
	

	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"resourceName",label :"<spring:message code='user'/>",sortable :true}, 
	                	{key :"skillsDefName",label :"<spring:message code='skill'/>",sortable :true}, 
	 					{key :"value",className: "smallColumn",label :"<spring:message code='classification'/>",sortable :true}];
		

	//datasource column definitions
	var ${ns}dsFields = [{key :"resourceName"},
					{key :"skillsDefName"},
					{key :"value", parser :"number"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-skills","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

	//resource filter
	var ${ns}filterByResource = function(resourceName){
		YAHOO.de.tarent.${ns}filter.resourceName = resourceName;
		${ns}myDataTable.reloadData();
	}
	//skill filter
	var ${ns}filterBySkill = function(skillsDefName){
		YAHOO.de.tarent.${ns}filter.skillsDefName = skillsDefName;
		${ns}myDataTable.reloadData();
	}

		
</script>	

