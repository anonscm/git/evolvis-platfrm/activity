<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getClientTable" id="getClientTable"/>
<portlet:resourceURL var="getClientAdd" id="getClientAdd"/>
<portlet:renderURL var="cancelCustomerView">
    <portlet:param name="ctx" value="cancelCustomerView"/>
</portlet:renderURL>
<portlet:renderURL var="getClientEdit">
    <portlet:param name="ctx" value="getClientEdit"/>
</portlet:renderURL>
<portlet:renderURL var="getClientDetails">
    <portlet:param name="ctx" value="getClientDetails"/>
</portlet:renderURL>

<portlet:renderURL var="getClientAdd" >
    <portlet:param name="ctx" value="getClientAdd"/>
</portlet:renderURL>
<!-- Meine aenderungen Anfang -->
<portlet:renderURL var="getClientList" >
    <portlet:param name="ctx" value="getClientList"/>
    <portlet:param name="clientPk" value="${customerView.id}"/>
</portlet:renderURL>
<!-- Meine aenderungen Ende -->

<portlet:actionURL var="doClientDelete" name="doClientDelete"/>

<script type="text/javascript">
	var breadcrumbValue = "<h1><a href=${cancelCustomerView}><spring:message code='customer'/></a></h1>";
	var breadcrumbFromProjectValue = "<h1><a href=${getClientList}><spring:message code='customer'/></a></h1>";
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	<c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
		YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbFromProjectValue+"</h1>");
	</c:if>
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
    <form:form method="GET" id="${ns}form" action="${getClientAdd}" onsubmit="${ns}handleDoAdd()">
		<input type="hidden" name="backUrl" value="${backUrl}" >
   		<h2><spring:message code="customers"/></h2>

	    <a:authorize ifAnyGranted="Customer.ADD_ALL">
		    <div class="button-div-top">
			   	<input type="button" onclick="${ns}loadAddNewClientUrl(); return false;" value="<spring:message code="new_customer"/>" />
			</div>
		</a:authorize>

     	<div id="${ns}form-clients" class=""></div>

    </form:form>
</div>


<script type="text/javascript">

	YAHOO.widget.DataTable.Formatter.${ns}formatDescription = function(elLiner, oRecord, oColumn, oData) {
		var value = YAHOO.lang.escapeHTML(oData);
		elLiner.innerHTML = value.replace(/\r\n|\n|\r/g,"<br />");
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		var state = "${getClientEdit}&backUrl=${cancelCustomerView}&clientPk="+oData;
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}handleDoEdit('"+state+"');YAHOO.de.tarent.goToUrl('"+state+"');return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
		var state = "${getClientDetails}&backUrl=${backUrl}&clientPk="+oData;
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}handleDoDetails('"+state+"');YAHOO.de.tarent.goToUrl('"+state+"');return false;\">&nbsp;</a>";
	};

    YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
        var state = "${doClientDelete}&backUrl=${backUrl}&clientPk="+oData;
        var clientName = oRecord.getData("name");
        var msg = "<spring:message code='confirm_delete_customer' arguments='"+clientName+"'/>";
        elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}handleDoDetails('"+state+"'); if (confirm('"+msg+"'))${ns}loadDeleteClientUrl('"+state+"');return false;\">&nbsp;</a>";
    };

	var ${ns}myColumnDefs = [{key :"idTable",className :"smallColumn",label :'<spring:message code="id"/>',sortable :true},
	                    {key :"name",label :'<spring:message code="customer"/>',sortable :true}, 
	 					<a:authorize ifAnyGranted="Customer.EDIT_ALL">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
                        <a:authorize ifAnyGranted="Customer.DELETE_ALL">
                            {key :"id",className :"deleteColumn", label: "&nbsp;", formatter: "${ns}formatDelete", sortable :false},
                        </a:authorize>
                        <a:authorize ifAnyGranted="Customer.VIEW_ALL">
                            {key :"id",className :"detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable :false}];
                        </a:authorize>

	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"idTable",parser :"number"},
	                {key :"name"}];

	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-clients","${getClientTable}", ${ns}dsFields, ${ns}myColumnDefs,
            YAHOO.de.tarent.${ns}filter, "", "name", "asc", ${(not empty clientsRowsPerPage) ? clientsRowsPerPage : 10});

	${ns}History = YAHOO.util.History;
	var ${ns}portletHistoryFlag;
	var ${ns}init = "${backUrl}";

	var ${ns}OnStateChanged = function (state) {
		if(${ns}portletHistoryFlag == true){
			jQuery('#${ns}mainDiv').load(state);
		}
		${ns}portletHistoryFlag = true;
	};

	${ns}History.register("${ns}mainDiv", ${ns}init, ${ns}OnStateChanged);

	var ${ns}handleDoAdd = function () {
		${ns}portletHistoryFlag = false;
		${ns}History.navigate("${ns}mainDiv", "${getClientAdd}");
	};

	var ${ns}handleDoEdit = function (state) {
		${ns}portletHistoryFlag = false;
		${ns}History.navigate("${ns}mainDiv", state);
	};

	var ${ns}handleDoDetails = function (state) {
		${ns}portletHistoryFlag = false;
		${ns}History.navigate("${ns}mainDiv", state);
	};

    function ${ns}loadAddNewClientUrl() {
        this.document.location.href = '${getClientAdd}';
    };
    function ${ns}loadDeleteClientUrl( deleteUrl) {
        this.document.location.href = deleteUrl;
    };

</script>

