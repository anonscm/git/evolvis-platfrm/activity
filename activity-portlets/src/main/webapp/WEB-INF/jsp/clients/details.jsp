<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%-- ajax urls from client controllers --%>
<portlet:renderURL var="getClientEdit">
    <portlet:param name="ctx" value="getClientEdit"/>
    <portlet:param name="backUrl" value="${backToClientDetailsRenderUrl}"/>
</portlet:renderURL>
<portlet:renderURL var="cancelCustomerView">
    <portlet:param name="ctx" value="cancelCustomerView"/>
</portlet:renderURL>
<portlet:renderURL var="getClientDetails">
    <portlet:param name="ctx" value="getClientDetails"/>
    <portlet:param name="clientPk" value="${customerView.id}"/>
</portlet:renderURL>
<%-- ajax urls from project controllers --%>
<portlet:resourceURL var="getProjectTable" id="getProjectTable" />
<portlet:renderURL var="getProjectAdd">
    <portlet:param name="ctx" value="getProjectAdd"/>
	<portlet:param name="clientId" value="${customerView.id}"/>
    <portlet:param name="backUrl" value="${backToClientDetailsRenderUrl}"/>
</portlet:renderURL>
<portlet:resourceURL var="getProjectEdit" id="getProjectEdit">
	<portlet:param name="backUrl" value="${backToClientDetailsRenderUrl}"/>
</portlet:resourceURL>
<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="backUrl" value="${backToClientDetailsRenderUrl}"/>
</portlet:renderURL>
<portlet:renderURL var="getClientList" >
    <portlet:param name="ctx" value="getClientList"/>
    <portlet:param name="clientPk" value="${customerView.id}"/>
</portlet:renderURL>	

<%-- if no backUrl is present we go back to client list view. --%>
<c:if test="${empty backUrl}">
	<portlet:renderURL var="backUrl"/>
</c:if>

<script type="text/javascript">
	var customerListLink = "";
</script>
<c:if test="${not empty linkToCustomerList}">
	<script type="text/javascript">customerListLink ="<a href=\"${linkToCustomerList}\"><spring:message code='customers'/></a>&nbsp;&raquo;&nbsp;";</script>
</c:if>
<script type="text/javascript">
    var breadcrumbClients = "<h1><a href=${cancelCustomerView}><spring:message code='customer'/></a>";
    var breadcrumbAllClients = "<h1><a href=${getClientList}><spring:message code='customer'/></a>";
    var breadcrumbCurrentlySelectedClient = "<a href=${getClientDetails}>${customerView.name}</a></h1>";
	YAHOO.de.tarent.createBreadcrumb(breadcrumbClients+"&nbsp;&raquo;&nbsp;" +breadcrumbCurrentlySelectedClient);
	var breadcrumbValueAllProjects = "<a href=${getProjectTable}><spring:message code='projects'/></a>";
    var breadcrumbValueSelectedProjects = "<a href=${getProjectDetails}>${project.name}</a>";
    
	<c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbClients+"&nbsp;&raquo;&nbsp;" +breadcrumbCurrentlySelectedClient+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedProjects+"</h1>");
    </c:if>
    
    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
    	YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbAllClients+"&nbsp;&raquo;&nbsp;" +breadcrumbCurrentlySelectedClient+"</h1>");
	</c:if>
	
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<form:form method="POST" id="${ns}form" action="${backUrl}" onsubmit="${ns}handleBrowserHistory('${backUrl}');" novalidate="novalidate" >
	
   		<h2><spring:message code="customer"/>: <c:out value="${customerView.name}"/></h2>

    	<a:authorize ifAnyGranted="">
    			<div class="button-div-top">
    				<input type="button" value="<spring:message code="new_project"/>" onClick="${ns}loadProjectAddUrl(); return false;" />
		  			<input type="button" value="<spring:message code="edit_customer"/>" onclick="YAHOO.de.tarent.goToUrl('${getClientEdit}&clientPk=${customerView.id}');return false;">
		    	</div>
    	</a:authorize>
   			
		 <div class="layout-table">
	    	<div class="layout-cell cell-fifty" >
	    		<h3><spring:message code="description"/>:</h3>
				<c:out value="${customerView.description}" escapeXml="false"/>
			</div>
			<div class="layout-cell cell-fifty" >
				<a:authorize ifAnyGranted="">
					<h3><spring:message code="economic_data"/>:</h3>

							<div class="details">
								<span class="info"><spring:message code="dayrate"/>:</span>
								<c:out value="${customerView.dayrate}"/>
							</div>
							<div class="details">
								<span class="info"><spring:message code="payment_date"/>:</span>
								<fmt:formatDate value="${customerView.paymentDate}" pattern="${dateformat}"/>
							</div>		
				</a:authorize>
	    	</div>
	    </div>
		<div class="top-margin">
			<h3><spring:message code="projects"/>:</h3>
			<div id="${ns}form-projects"></div>
		</div>
	    <div class="button-div">
		   	<input type="submit" id="cancelButton" value="<spring:message code="back_to_main_mask"/>" />
		</div>  	
	</form:form>
</div>
<script type="text/javascript">

//custom formatter for table edit column
YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
	var action = "${getProjectEdit}&projectId="+oData;
	elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('"+action+"');return false;\">&nbsp;</a>";
};

YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
	var action = "${getProjectDetails}&projectId="+oData;
    elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('"+action+"');return false;\">&nbsp;</a>";
};

//javascript filter object
YAHOO.de.tarent.${ns}filter = {
	status : '',
	type : '',
	clientId: '${customerView.id}'
};

//datatable column definitions
var ${ns}myColumnDefs = [{key:"idTable", className:"smallColumn", label:"<spring:message code='id'/>", sortable:true},
                		{key:"projectName", label:"<spring:message code='project'/>", sortable:true}, 
 						{key:"description", label:"<spring:message code='description'/>", sortable:true},
 						<a:authorize ifAnyGranted="Project.EDIT, Project.EDIT_ALL">
 							{key:"id", className:"editColumn", label:"&nbsp;", formatter:"${ns}formatEdit", sortable:false},
 						</a:authorize>
 						<a:authorize ifAnyGranted="Project.VIEW, Project.VIEW_ALL_DETAIL">
 							{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}
 						</a:authorize>];
	
//datasource column definitions
var ${ns}dsFields = [{key:"id", parser:"number"},
                	{key:"idTable", parser:"number"},
					{key:"projectName"},
					{key:"description"}];

//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-projects","${getProjectTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
	
var ${ns}handleBrowserHistory = function (state) {
	 ${ns}portletHistoryFlag = false;
	 ${ns}History.navigate("${ns}mainDiv", state);
};

function ${ns}loadProjectAddUrl() {
    this.document.location.href = '${getProjectAdd}';
};

function ${ns}loadUrl(urlToLoad) {
    this.document.location.href = urlToLoad;
};

</script>

