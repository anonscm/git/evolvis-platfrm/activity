<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="getClientSave" name="getClientSave">
    <portlet:param name="backUrl" value="${backUrl}"/>
</portlet:actionURL>

<%-- if no backUrl is present we go back to client list view. --%>
<c:if test="${empty backUrl}">
    <portlet:renderURL var="backUrl">
        <portlet:param name="ctx" value="cancelCustomerView"/>
    </portlet:renderURL>
</c:if>

<portlet:renderURL var="cancelCustomerView">
    <portlet:param name="ctx" value="cancelCustomerView"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbClients = "<h1><a href=${cancelCustomerView}><spring:message code='customer'/></a>";
    var breadcrumbEditOrNewClient = "<spring:message code='add_customer'/></h1>";
    YAHOO.de.tarent.createBreadcrumb(breadcrumbClients+"&nbsp;&raquo;&nbsp;" +breadcrumbEditOrNewClient);
</script>

<div id="${ns}mainDiv" class="mainDiv">

	<h2><spring:message code="add_customer"/></h2>
	
	<form:form method="POST" id="${ns}form" action="${getClientSave}" commandName="customerView" novalidate="novalidate">

			<form:hidden path="id"/>

			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-seventy-five">
						<a:inputText name="name" form="${ns}form" maxlength="255" labelCode="name" styleClass="large" required="true" value="${customerView.name}" requiredMessageCode="validate_name_not_empty"/>
					</div>
				
					<div class="layout-cell cell-twenty-five">
						<label for="dayrate"><spring:message code="dayrate"/>:</label>
						<input name="dayrate" id="dayrate"  value="${customerView.dayrate}" pattern="^((:?1?[0-9]|2[0-4])(:?[.,](:?00?|25|50?|75))?)?$"/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-dayrate-number"><spring:message code="validate_daily_rate"/></div>
		    			<form:errors path="dayrate" cssClass="portlet-msg-error" ></form:errors>
					</div>
				</div>
				
				<div class="layout-row">
					<div class="layout-cell cell-seventy-five">	
						<label for="${ns}form-description"><spring:message code="description"/>:</label>
						<textarea id="${ns}form-description" name="description" rows="8" maxlength="4000"><c:out value="${customerView.description}" /></textarea>
				
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
								<spring:message code="validate_description_length"/>
						</div>
						
						<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
					</div>
					
					<div class="layout-cell cell-twenty-five">
						<a:inputDate styleClass="date" name="paymentDate" labelCode="payment_date" form="${ns}form" value="${customerView.paymentDate}" 
						required="false" requiredMessageCode="validate_date"/>
					</div>
				</div>
			</div>

	    <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	    	<input type="button" onclick="YAHOO.de.tarent.goToUrl('${backUrl}'); return false;" class="cancel" value="<spring:message code="cancel"/>" />
	   </div>
	</form:form>
</div>