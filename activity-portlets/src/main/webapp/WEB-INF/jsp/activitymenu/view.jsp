<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div id="${ns}mainDiv" class="mainMenu">
	<c:if test="${not empty myActivitiesPageLink or not empty myOvertimesPageLink or not empty myHolidayPageLink}">
				<%--or not empty mySkillsPageLink}"--%>
		<div class="mainMenuCat">
			<h2><spring:message code="my_actions"/></h2>
			<ul>
				<c:if test="${not empty myActivitiesPageLink}">
					<li><a href="${myActivitiesPageLink}"><spring:message code="my_activities"/></a></li>
				</c:if>
				<c:if test="${not empty myOvertimesPageLink}">
					<li><a href="${myOvertimesPageLink}"><spring:message code="my_overtimee"/></a></li>
				</c:if>
				<c:if test="${not empty myHolidayPageLink}">
					<li><a href="${myHolidayPageLink}"><spring:message code="my_absence"/></a></li>
				</c:if>
				<%--<c:if test="${not empty mySkillsPageLink}">
					<li><a href="${mySkillsPageLink}"><spring:message code="my_skills"/></a></li>
				</c:if--%>
                <c:if test="${not empty myRequestsPageLink}">
                    <li><a href="${myRequestsPageLink}"><spring:message code="my_delete_request"/></a></li>
                </c:if>
			</ul>
		</div>
	</c:if>
    <c:if test="${not empty myActiveProjectsPageLink}">
        <div class="mainMenuCat">
            <h2><spring:message code="my_projects"/></h2>
            <ul>
                <li><a href="${myActiveProjectsPageLink}"><spring:message code="my_active_projects"/></a></li>
            </ul>
        </div>
    </c:if>
	<c:if test="${not empty customersPageLink or not empty projectsPageLink or not empty jobsPageLink
				or not empty positionsPageLink or not empty resourcesPageLink or not empty resourceAllocationPageLink
				or not empty skillsPageLink}">
		<div class="mainMenuCat">
			<h2><spring:message code="management"/></h2>
			<ul>
				<c:if test="${not empty customersPageLink}">
					<li><a href="${customersPageLink}"><spring:message code="customers"/></a></li>
				</c:if>
				<c:if test="${not empty projectsPageLink}">
					<li><a href="${projectsPageLink}"><spring:message code="projects"/></a></li>
				</c:if>
				<c:if test="${not empty jobsPageLink}">
					<li><a href="${jobsPageLink}"><spring:message code="jobs"/></a></li>
				</c:if>
				<c:if test="${not empty positionsPageLink}">
					<li><a href="${positionsPageLink}"><spring:message code="positions"/></a></li>
				</c:if>
				<c:if test="${not empty resourcesPageLink}">
					<li><a href="${resourcesPageLink}"><spring:message code="resource_administration"/></a></li>
				</c:if>				
				<c:if test="${not empty resourceAllocationPageLink}">
					<li><a href="${resourceAllocationPageLink}"><spring:message code="resource_allocation"/></a></li>
				</c:if>
				<c:if test="${not empty skillsPageLink}">
					<li><a href="${skillsPageLink}"><spring:message code="resource_skills"/></a></li>
				</c:if>
			</ul> 
		</div>
	</c:if>
	<c:if test="${not empty activitiesPageLink or not empty holidayPageLink or not empty overtimesPageLink 
				or not empty invoicesPageLink or not empty deleteRequestsPageLink}">
		<div class="mainMenuCat">
			<h2><spring:message code="controlling"/></h2>
			<ul>
				<c:if test="${not empty activitiesPageLink}">
					<li><a href="${activitiesPageLink}"><spring:message code="activities"/></a></li>
				</c:if>
				<c:if test="${not empty holidayPageLink}">
					<li><a href="${holidayPageLink}"><spring:message code="absence"/></a></li>
				</c:if>
				<c:if test="${not empty overtimesPageLink}">
					<li><a href="${overtimesPageLink}"><spring:message code="overtime"/></a></li>
				</c:if>
				<c:if test="${not empty invoicesPageLink}">
					<li><a href="${invoicesPageLink}"><spring:message code="invoices"/></a></li>
				</c:if>
				<c:if test="${not empty deleteRequestsPageLink}">
					<li><a href="${deleteRequestsPageLink}"><spring:message code="delete_requests"/></a></li>
				</c:if>
			</ul> 
		</div>
	</c:if>
	<c:if test="${not empty permissionsPageLink or not empty ldapImportPageLink}">
		<div class="mainMenuCat">
			<h2><spring:message code="administration"/></h2>
			<ul>
				<c:if test="${not empty permissionsPageLink}">
					<li><a href="${permissionsPageLink}"><spring:message code="permissions"/></a></li>
				</c:if>
				<c:if test="${not empty ldapImportPageLink}">
					<li><a href="${ldapImportPageLink}"><spring:message code="ldap_import"/></a></li>
				</c:if>
			</ul> 
		</div>
	</c:if>
</div>
