<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getPositionTable" id="getPositionTable" />
<portlet:renderURL var="getPositionEdit">
    <portlet:param name="ctx" value="positionEdit"/>
</portlet:renderURL>

<portlet:renderURL var="getPositionDetail" >
    <portlet:param name="ctx" value="positionDetails"/>
</portlet:renderURL>
<portlet:renderURL var="getPositionAdd">
    <portlet:param name="ctx" value="getPositionAdd"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/positions/breadcrumbs/breadcrumb-links.jsp"%>

<script type="text/javascript">

    var breadcrumbValue = "<h1><a href=${getPositionList}><spring:message code='positions'/></a></h1>"

    <c:if test="${ ns == '_positionsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
    </c:if>

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<form:form method="POST" id="${ns}form" action="${getPositionAdd}">
		<h2>
			<spring:message code="positions" />
		</h2>

        <a:authorize ifAnyGranted="Position.ADD">
            <div class="button-div-top">
                <input type="submit" value="<spring:message code="new_position"/>">
            </div>
        </a:authorize>

		<a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
			<div class="bottom-margin input_width">
				<a:filterSelectProjectJob ns="${ns}" jobsList="${jobsListToSelect}"
					selectedValue="${paramJob.pk}"
					onchangeFunction="${ns}filterByJob(this.value);" />
			</div>
		</a:authorize>

        <a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
		    <div id="${ns}form-positions" class="clearDiv"></div>
        </a:authorize>

	</form:form>
</div>

<script type="text/javascript">

	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}loadPositionEditUrl("+oData+"); return false;\">&nbsp;</a>";
	};

	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}loadPositionDetailUrl("+oData+"); return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatManDays = function(elLiner, oRecord, oColumn, oData) {
		if(null !== oData){
			elLiner.innerHTML = oData.toFixed(2);
		}
	};

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {

		// if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
		jobId : ${(not empty paramJob) ? paramJob.pk : "''"}

	};


	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"positionId",label :"<spring:message code='id'/>",sortable :true},
	                    {key :"positionName",label :"<spring:message code='position'/>",sortable :true},
	            		{key :"projectName",label :"<spring:message code='project'/>", sortable :true},
	            		{key :"jobName",label :"<spring:message code='job'/>", sortable :true},
	 					{key :"positionStatusName",label :"<spring:message code='status'/>",sortable :true},
	 					{key :"expectedWork",className: "smallColumn",label :"<spring:message code='estimated_man_day'/>",formatter:"${ns}formatManDays",sortable :true},
	 					{key :"stats.realWork",className: "smallColumn",label :"<spring:message code='done_man_day'/>",formatter:"${ns}formatManDays",sortable :true},
	 					{key :"communicatedWork",className: "smallColumn",label :"<spring:message code='communicated_man_day'/>",formatter:"${ns}formatManDays", sortable :true},
	 					<a:authorize ifAnyGranted="Position.EDIT_ALL, Position.EDIT">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
							{key :"id",className :"detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
                    {key :"positionId",parser :"number"},     
	                {key :"positionName"},
					{key :"positionStatusName"},
					{key :"expectedWork",parser :"number"},
					{key :"stats.realWork",parser :"number"},
					{key :"communicatedWork",parser :"number"},
					{key :"projectName"},
					{key :"jobName"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-positions","${getPositionTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter,
                        null, "positionName", "asc", ${(not empty positionsRowsPerPage) ? positionsRowsPerPage : 10});


	//job selection
	var ${ns}filterByJob = function(job){
		YAHOO.de.tarent.${ns}filter.jobId = job;
		var breadcrumbValue = "<h1><spring:message code='positions'/></h1>"
		YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		${ns}myDataTable.reloadData();
	}

    function ${ns}loadPositionEditUrl(posId) {
        this.document.location.href = '${getPositionEdit}&positionId='+posId;
    };

    function ${ns}loadPositionDetailUrl(posId) {
        this.document.location.href = '${getPositionDetail}&positionId='+posId;
    };

</script>

