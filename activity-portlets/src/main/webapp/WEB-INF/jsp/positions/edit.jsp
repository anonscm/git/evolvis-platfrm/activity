<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="getPositionSave" name="doPositionSave">
    <portlet:param name="ctx" value="positionEdit"/>
    <portlet:param name="backUrl" value="${backUrl}"/>
</portlet:actionURL>
<portlet:renderURL var="getPositionCancel">
    <portlet:param name="ctx" value="getPositionCancel"/>
</portlet:renderURL>
<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/positions/breadcrumbs/breadcrumb-links.jsp"%>

<script type="text/javascript">

    var breadcrumbValueSelectedJob = "<a href=${getJobDetails}&jobId=${positionView.jobId}>${positionView.jobName}</a>";
    var breadcrumbPositions = "<a href=${getPositionList}><spring:message code='positions'/></a>";
    var breadcrumbSelectedPosition = "<a href=${getPositionDetail}&positionId=${positionView.id}>${positionView.positionName}</a>";

    <c:choose>
        <c:when test="${not empty positionView.id}">
            <%-- Position Edit view --%>
            <c:choose>
                <c:when test="${ ns == '_positionsportlet_WAR_activityportlets_'}">
                    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbPositions+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+"</h1>");
                </c:when>
                <c:otherwise>
                    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedJob+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+"</h1>");
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <%-- New Position view --%>
            <c:choose>
                <c:when test="${ ns == '_positionsportlet_WAR_activityportlets_'}">
                    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbPositions+"&nbsp;&raquo;&nbsp;" +"<spring:message code='new_position'/></h1>");
                </c:when>
                <c:otherwise>
                    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedJob+"&nbsp;&raquo;&nbsp;<spring:message code='new_position'/></h1>");
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>

    function ${ns}loadCancelPositionUrl() {
        if('${backUrl}' == null || '${backUrl}' == ""){
            this.document.location.href = '${getPositionCancel}';
        }else{
            this.document.location.href = '${backUrl}';
        }
    }

</script>


<div id="${ns}mainDiv" class="mainDiv">

    <h2><spring:message code="add_position"/></h2>
	<form:form method="POST" id="${ns}form" action="${getPositionSave}" commandName="positionView" novalidate="novalidate">

			<fieldset>
			<legend><spring:message code="positions_data"/></legend>
				<c:if test="${lessEndDate eq 'less'}">
		    		<div class="portlet-msg-error"><spring:message code="validate_start_before_end"/></div>
		    	</c:if>
		    	<input type="hidden" name="positionId" value="${positionView.id}"/>
				<form:hidden path="id" />
				<div class="layout-table">
					<div class="layout-row">
						<div class="layout-cell cell-thirty-three">
							<a:inputText name="positionName" form="${ns}form" maxlength="255" labelCode="position" styleClass="large" required="true" value="${positionView.positionName}" requiredMessageCode="validate_position_name_not_empty"/>
						</div>
						<div class="layout-cell cell-thirty-three">
							<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job"
							styleClass="large" selectedValue="${not empty positionView.jobId ? positionView.jobId : paramJob.pk }" requiredMessageCode="validate_job_not_empty" required="true"/>
						</div>
						<div class="layout-cell cell-thirty-three">
							<label for="positionStatusId"><spring:message code="status"/>*:</label>
							<select name="positionStatusId" id="positionStatusId" required="required">
							   	<option value=""><spring:message code="select_please"/></option>
							    	<c:forEach var="status" items="${statusList}" >
										<option value="${status.pk}"
											<c:if test="${positionView.positionStatusId eq status.pk}">
												selected="selected"
											</c:if>
										><c:out value="${status.name}"/></option>
									</c:forEach>
							</select>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-positionStatusId-required"><spring:message code="validate_status_not_empty"/></div>
							<form:errors path="positionStatusId" cssClass="portlet-msg-error" />
						</div>
					</div>
				</div>
				<div class="layout-table">
					<div class="layout-row">
						<div class="layout-cell">
							<label for="${ns}form-description"><spring:message code="description"/>:</label>
							<textarea id="${ns}form-description" name="description" rows="3" cols="100" maxlength="4000"><c:out value="${positionView.description}" /></textarea>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
								<spring:message code="validate_description_length"/>
							</div>
							<form:errors path="description" cssClass="portlet-msg-error" />
						</div>
					</div>
				</div>
				<div class="layout-table width-auto">
					<div class="layout-row width-auto">
						<div class="layout-cell cell-twenty-five">
							<a:inputDate styleClass="date" name="beginDate" labelCode="start_date" form="${ns}form" value="${positionView.beginDate}" />
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-beginDate-dateInOrder">
								<spring:message code="validate_start_before_end"/>
							</div>
						</div>
						<div class="layout-cell">
							<a:inputDate styleClass="date" name="endDate" labelCode="end_date" form="${ns}form" value="${positionView.endDate}" />
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend><spring:message code="evolvis_data"/></legend>
				<div class="layout-table">
					<div class="layout-row">
						<div class="layout-cell cell-fifty">
							<a:inputLink name="evolvisURL" maxlength="255" form="${ns}form" labelCode="evolvis_url" styleClass="large" value="${positionView.evolvisURL}" linkMessageCode="validate_http_url"/>
						</div>
						<div class="layout-cell cell-twenty-five">
							<a:inputEvolvisTaskId name="evolvisProjectId" labelCode="evolvis_project_id" validationMessageCode="validate_digit" form="${ns}form" value="${positionView.evolvisProjectId}" styleClass="large"/>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
			<legend><spring:message code="financial_data"/></legend>
				<div class="layout-table">
					<div class="layout-row">
						<a:authorize ifAnyGranted="">
							<div class="layout-cell cell-thirty-three">
								<a:inputText name="fixedPrice" form="${ns}form" maxlength="255" labelCode="price_net" required="true" value="${positionView.fixedPrice}" requiredMessageCode="validate_price_not_empty"/>
								<div class="portlet-msg-error yui-pe-content" id="${ns}form-fixedPrice-positiveNumber">
									<spring:message code="validate_digit_positive"/>
								</div>
							</div>
						</a:authorize>
						<div class="layout-cell cell-thirty-three">
						<label for="expectedWork"><spring:message code="expected_work"/>*:</label>
						<spring:bind path="positionView.expectedWork">
							<input type="number" name="expectedWork" id="expectedWork"
								value="${status.displayValue}" />
							<div class="portlet-msg-error yui-pe-content"
								id="${ns}form-expectedWork-positiveNumber">
								<spring:message code="validate_digit_positive" />
							</div>
							<form:errors path="expectedWork" cssClass="portlet-msg-error" />
						</spring:bind>
					</div>
						<div class="layout-cell cell-thirty-three">
						<label for="communicatedWork"><spring:message code="communicated_work"/>:</label>
						<spring:bind path="positionView.communicatedWork">
							<input type="number"  name="communicatedWork" id="communicatedWork" value="${status.displayValue}" />
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-communicatedWork-positiveNumber">
								<spring:message code="validate_digit_positive"/>
							</div>
							<form:errors path="communicatedWork" cssClass="portlet-msg-error" />
							</spring:bind>
						</div>
					</div>
				</div>
			</fieldset>
	    <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	    	<input type="button" onclick="${ns}loadCancelPositionUrl();return false;" class="cancel" value="<spring:message code="cancel"/>" />
	   </div>
	</form:form>
</div>

