<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ include file="/WEB-INF/jsp/positions/breadcrumbs/breadcrumb-links.jsp"%>

<portlet:resourceURL var="doPositionSave" id="doPositionSave"/>
<portlet:resourceURL var="doCancel" id="doCancel"/>
<portlet:renderURL var="getPositionDetail">
    <portlet:param name="ctx" value="positionDetails"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbPositions = "<a href=${getPositionList}><spring:message code='positions'/></a>";
    var breadcrumbSelectedPosition = "<a href=${getPositionDetail}&positionId=${positionView.id}>${positionView.positionName}</a>";

    <c:choose>
        <c:when test="${ ns == '_positionsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbPositions+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+
                    "&nbsp;&raquo;&nbsp;<spring:message code='changes'/></h1>");
        </c:when>
        <c:otherwise>
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbSelectedPosition+
                    "&nbsp;&raquo;&nbsp;<spring:message code='changes'/></h1>");
        </c:otherwise>
    </c:choose>

    function ${ns}loadPositionDetailUrl() {
        this.document.location.href = '${getPositionDetail}&positionId=${positionView.id}';
    }

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
<h2><spring:message code="changes"/></h2>
	<form:form method="POST" id="${ns}form" action="${doPositionSave}"
		commandName="positionView">

	<div class="yui-dt">
		<div class="yui-dt-mask" style="display: none;"></div>
		<table>
			<colgroup>
				<col>
				<col>
				<col>
				<col>
				<col>
			</colgroup>
			<thead>
				<tr>
					<th><div class="yui-dt-liner"><spring:message code="changed_date"/></div></th>
					<th><div class="yui-dt-liner"><spring:message code="changed_by"/></div></th>
					<th><div class="yui-dt-liner"><spring:message code="field_name"/></div></th>
					<th><div class="yui-dt-liner"><spring:message code="old_value"/></div></th>
					<th><div class="yui-dt-liner"><spring:message code="new_value"/></div></th>
				</tr>
			</thead>
			<tbody tabindex="0" class="yui-dt-data" style="">

				<c:forEach var="changeInfo" items="${positionChanges}">
					<tr class="yui-dt-rec yui-dt-first yui-dt-even">
						<td headers="yui-dt113-th-positionName "><div
								class="yui-dt-liner">
								<c:out value="${changeInfo.changeDate}" />
							</div></td>
						<td headers="yui-dt113-th-description "><div
								class="yui-dt-liner">
								<c:out value="${changeInfo.changeUser}" />
							</div></td>
						<td headers="yui-dt113-th-positionStatusName " colspan="3"><div
								class="yui-dt-liner">&nbsp;</div></td>
					</tr>
					<c:forEach var="changeField" items="${changeInfo.changeFields}">

						<tr class="yui-dt-rec yui-dt-first yui-dt-even">
							<td headers="yui-dt113-th-positionStatusName " colspan="2"><div
									class="yui-dt-liner">&nbsp;</div></td>
							<td headers="yui-dt113-th-positionName "><div>
									<c:out value="${changeField.fieldName}" />
								</div></td>
							<td headers="yui-dt113-th-description "><div
									class="yui-dt-liner">
									<c:out value="${changeField.oldValue}" />
								</div></td>
							<td headers="yui-dt113-th-description "><div
									class="yui-dt-liner">
									<c:out value="${changeField.newValue}" />
								</div></td>
						</tr>
					</c:forEach>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<div class="button-div">
		<input type="submit"
            onclick="${ns}loadPositionDetailUrl(); return false;"
			class="cancel"
			value="<spring:message code="back"/>" />
	</div>
				
	</form:form>
</div>

