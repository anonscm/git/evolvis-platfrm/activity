<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="doAssign">
    <portlet:param name="ctx" value="positionEdit"/>
    <portlet:param name="action" value="doAssign"/>
    <portlet:param name="positionId" value="${positionView.id}"/>
</portlet:renderURL>
<portlet:renderURL var="getPositionCancel">
    <portlet:param name="ctx" value="getPositionCancel"/>
</portlet:renderURL>
<portlet:actionURL var="doPositionDelete" name="doDelete"/>
<portlet:renderURL var="getPositionChanges">
    <portlet:param name="ctx" value="positionDetails"/>
    <portlet:param name="action" value="showChanges"/>
</portlet:renderURL>
<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/positions/breadcrumbs/breadcrumb-links.jsp"%>

<portlet:renderURL var="getPositionEdit">
    <portlet:param name="ctx" value="positionEdit"/>
    <portlet:param name="backUrl" value="${getPositionDetail}&positionId=${positionView.id}"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbPositions = "<a href=${getPositionList}><spring:message code='positions'/></a>";
    var breadcrumbValueSelectedJob = "<a href=${getJobDetails}&jobId=${positionView.jobId}>${positionView.jobName}</a>";
    var breadcrumbSelectedPosition = "<a href=${getPositionDetail}&positionId=${positionView.id}>${positionView.positionName}</a>";

    <c:choose>
        <c:when test="${ ns == '_positionsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbPositions+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+"</h1>");
        </c:when>
        <c:otherwise>
            <%-- Same for Jobs, Projects and Clients--%>
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedJob+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+"</h1>");
        </c:otherwise>
    </c:choose>

    function ${ns}loadGetPositionEditUrl() {
        this.document.location.href = '${getPositionEdit}&positionId=${positionView.id}';
    };
    function ${ns}loadDeletePositionUrl() {
        this.document.location.href = '${doPositionDelete}&id=${positionView.id}';
    };

    function ${ns}loadCancelPositionUrl() {
        if('${backUrl}' == null || '${backUrl}' == ""){
            YAHOO.de.tarent.goToUrl('${getPositionCancel}');
        }else{
            YAHOO.de.tarent.goToUrl('${backUrl}');
        }
    }

</script>

<div id="${ns}mainDiv" class="mainDiv">
    <h2><spring:message code="position"/>: <c:out value="${positionView.positionName}"/></h2>
	<form:form method="POST" id="${ns}form" action="${doAssign}" commandName="positionView" novalidate="novalidate">

		<form:hidden path="id"/>

		<div class="button-div-top">
            <a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
                <input type="submit" style="width: auto;" value="<spring:message code="changes"/>" onclick="YAHOO.de.tarent.goToUrl('${getPositionChanges}&positionId=${positionView.id}');return false;" />
            </a:authorize>
            <a:authorize ifAnyGranted="Resource.EDIT_ALL, Resource.APPEND_ALL, Resource.DETACH_ALL">
                <input type="submit"  style="width: auto;" value="<spring:message code="assign_resource"/>" />
            </a:authorize>
            <a:authorize ifAnyGranted="Position.EDIT, Position.EDIT_ALL">
            	<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${getPositionEdit}&positionId=${positionView.id}'); return false;" value="<spring:message code="edit_position"/>" />
            </a:authorize>
            <a:authorize ifAnyGranted="Position.DELETE, Position.DELETE_ALL">
                <input type="submit" value="<spring:message code="delete"/>" onclick="if (confirm('<spring:message code="confirm_delete_position" arguments="${positionView.positionName}" htmlEscape="true"/>'))YAHOO.de.tarent.goToUrl('${doPositionDelete}&id=${positionView.id}'); return false;" <c:if test="${! isDeletablePosition}">disabled="disabled"</c:if> />
            </a:authorize>
            
		</div>

  		 <div class="layout-table">
   		 	<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
	    			<span class="info"><spring:message code="id" />: </span>
	    			<span><c:out value="${positionView.id}"></c:out></span>
				</div>
				<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="start_date"/>: </span>
						<span><fmt:formatDate value="${positionView.beginDate}" pattern="${dateformat}"/></span>
				</div>
			</div>
   		 	<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="position" />: </span>
	    				<span><c:out value="${positionView.positionName}"></c:out></span>
				</div>
				<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="end_date"/>: </span>
						<span><fmt:formatDate value="${positionView.endDate}" pattern="${dateformat}"/></span>
				</div>
			</div>
			<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="status"/>: </span>
						<span><c:out value="${positionView.positionStatusName}"></c:out></span>
				</div>
				<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="completed"/>: </span>
						<span><c:out value="${positionView.stats.percentDone}%"></c:out></span>
				</div>
			</div>
		</div>
		<div class="total_width">
			<span class="info"><spring:message code="description" />: </span>
	    	<span><c:out value="${positionView.description}" escapeXml="false"></c:out></span>
		</div>

		<div class="layout-table top-margin">
			<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="evolvis_url"/>: </span>
						<span><c:out value="${positionView.evolvisURL}"></c:out></span>
				</div>
				<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="evolvis_project_id"/>: </span>
						<span><c:out value="${positionView.evolvisProjectId}"></c:out></span>
				</div>
			</div>
		</div>

		<h3 class="top-margin"><spring:message code="economic_data"/></h3>
		<div class="layout-table">
			<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
	    				<a:authorize ifAnyGranted="">
	    					<span class="info"><spring:message code="price_net"/>: </span>
	    					<span><c:out value="${positionView.fixedPrice}"></c:out></span>
	    				</a:authorize>
	    		</div>
	    		<div class="layout-cell cell-fifty">
				</div>
			</div>
			<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="expected_work"/>: </span>
					<spring:bind path="positionView.expectedWork">
						<span><c:out value="${status.displayValue}"></c:out> <spring:message
								code="man_days" /></span>
					</spring:bind>
				</div>
	    		<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="communicated_work"/>: </span>
					<spring:bind path="positionView.communicatedWork">
						<span><c:out value="${status.displayValue}"></c:out> <spring:message
								code="man_days" /></span>
					</spring:bind>
				</div>
			</div>
		</div>
		<a:authorize ifAnyGranted="">
			<h4 class="top-margin"><spring:message code="calculated_cost_yield"/></h4>
			<div class="layout-table">
				<div class="layout-row">
		    		<div class="layout-cell cell-fifty details">
						<span class="info"><spring:message code="cost"/>: </span>
						<spring:bind path="positionView.stats.serviceCosts">
							<span><c:out value="${status.displayValue}"></c:out></span>
						</spring:bind>
					</div>
		    		<div class="layout-cell cell-fifty details">
								<span class="info"><spring:message code="yield"/>: </span>
								<spring:bind path="positionView.stats.income">
									<span><c:out value="${status.displayValue}"></c:out></span>
								</spring:bind>
					</div>
				</div>
			</div>
		</a:authorize>
		<h4 class="top-margin"><spring:message code="calculated_performance"/></h4>
		<div class="layout-table">
			<div class="layout-row">
	    		<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="performance"/>: </span>
					<spring:bind path="positionView.stats.realWork">
						<span><c:out value="${status.displayValue}"></c:out> <spring:message
								code="man_days" /></span>
					</spring:bind>
				</div>
				<div class="layout-cell cell-fifty"></div>
			</div>
		</div>
		<a:authorize ifAnyGranted="">
			<div class="button-div">   				
				<input type="submit" onclick="${ns}loadCancelPositionUrl(); return false;" class="cancel" value="<spring:message code="back_to_main_mask"/>" />
			</div>
		</a:authorize>
	</form:form>
</div>


