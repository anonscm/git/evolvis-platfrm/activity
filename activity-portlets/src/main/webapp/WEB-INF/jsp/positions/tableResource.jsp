<script type="text/javascript">

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		selectedPositionId : ${(not empty posResourceMappingView.positionId) ? posResourceMappingView.positionId : "''"}
	};
	
	//lst das Loeschen einer Ressource aus
	YAHOO.widget.DataTable.Formatter.${ns}formatRemove = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${removeResource}&id="+oData+"&positionId="+oRecord.getData("positionId")+"');return false;\">&nbsp;</a>";
	};
	
	//baut die Select-Box zum aendern des Status' der Resourcen zusammen
	YAHOO.widget.DataTable.Formatter.${ns}formatStatus = function(el, oRecord, oColumn, oData, oDataTable) {
		
		var statusViewList = ${statusViews};

		var statusSelect = "<select id='statusSelect' name='statusSelect' onChange=\"${ns}setStatus(this.value, " + oRecord.getData('positionId') + ", " + oRecord.getData('resourceId') + ")\">";
		var selected = "selected='selected'";
		
		for(var i = 0; i < statusViewList.length; i++) {
			statusSelect += "<option value=" + statusViewList[i].pk;
			if(statusViewList[i].pk==oData) {
				statusSelect += " "+selected;
			}
			statusSelect += ">"+statusViewList[i].name+"</option>";
		}
		
		statusSelect += "</select>";	
	 	
		el.innerHTML = statusSelect;
    };
    
    var ${ns}setStatus = function(statusId, positionId, resourceId) {
		
		var changeStatus = '${changeStatus}&statusId='+statusId+'&positionId='+positionId+'&resourceId='+resourceId+'';
		
		jQuery('#${ns}mainDiv').load(changeStatus);
		return false;
	}; 
	
	//datatable column definitions
	var ${ns}myColumnDefs = [
	            		{key :"resourceName",label :"<spring:message code='resource'/>",sortable :true},
	 					{key :"statusId",label :"<spring:message code='status'/>", formatter : "${ns}formatStatus", sortable : false},
	 					{key :"startDate",label :"<spring:message code='start_date'/>", formatter:"mdyDate", sortable :true},
	 					{key :"endDate",label :"<spring:message code='end_date'/>", formatter:"mdyDate", sortable :true},
	 					{key :"id", label :"&nbsp;", formatter :"${ns}formatRemove", className: "deleteColumn", sortable : false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id"},
	                {key :"positionId", parser: "number"},
					{key :"resourceName"},
					{key :"resourceId"},
					{key :"statusId", parser: "number"},
					{key :"startDate", parser:"date"},
					{key :"endDate", parser:"date"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}tableResources-position","${tableResources}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

</script>