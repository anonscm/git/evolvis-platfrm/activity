
<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<portlet:resourceURL var="getPositionTable" id="getPositionTable" />
<portlet:renderURL var="getPositionEdit" >
    <portlet:param name="ctx" value="positionEdit"/>
    <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
</portlet:renderURL>
<portlet:renderURL var="getPositionDetail" >
    <portlet:param name="ctx" value="positionDetails"/>
    <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
</portlet:renderURL>

<script type="text/javascript">

	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getPositionEdit}&positionId="+oData+"');return false;\">&nbsp;</a>";
	};

	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetail = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getPositionDetail}&positionId="+oData+"');return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatManDays = function(elLiner, oRecord, oColumn, oData) {
		if(null !== oData){
			elLiner.innerHTML = oData.toFixed(2);
		}
	};



	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {

		// if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
		jobId : ${(not empty paramJob) ? paramJob.pk : "''"}

	};


	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"positionName",label :"<spring:message code='position'/>",sortable :true},
	            		{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"positionStatusName",label :"<spring:message code='status'/>",sortable :true},
	 					{key :"expectedWork",label :"<spring:message code='estimated_man_day'/>", formatter:"${ns}formatManDays",sortable :true},
	 					{key :"stats.realWork",label :"<spring:message code='done_man_day'/>", formatter:"${ns}formatManDays",sortable :true},
	 					{key :"communicatedWork",label :"<spring:message code='communicated_man_day'/>", formatter:"${ns}formatManDays", sortable :true},
	 					{key :"evolvis",label :"<spring:message code='evolvis_url'/>", sortable :false},
	 					<a:authorize ifAnyGranted="Position.EDIT_ALL">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
	 					{key :"id",className: "detailsColumn",label :"&nbsp;", formatter:"${ns}formatDetail", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"positionName"},
					{key :"description"},
					{key :"positionStatusName"},
					{key :"expectedWork",parser :"number"},
					{key :"stats.realWork",parser :"number"},
					{key :"communicatedWork",parser :"number"},
					{key :"evolvis"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-positions","${getPositionTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);


	//job selection
	var ${ns}filterByJob = function(job){
		YAHOO.de.tarent.${ns}filter.jobId = job;
		var breadcrumbValue = "<h1><spring:message code='positions'/></h1>"
		YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		${ns}myDataTable.reloadData();
	}

</script>