<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ include file="/WEB-INF/jsp/positions/breadcrumbs/breadcrumb-links.jsp"%>

<portlet:resourceURL var="tableResources" id="tableResources"/>
<portlet:actionURL var="doSave" name="doSave">
    <portlet:param name="ctx" value="positionEdit"/>
</portlet:actionURL>
<portlet:resourceURL var="positions" id="positions"/>
<portlet:actionURL var="removeResource" name="removeResource">
    <portlet:param name="ctx" value="positionEdit"/>
</portlet:actionURL>
<portlet:resourceURL var="changeStatus" id="changeStatus"/>

<script type="text/javascript">

    var breadcrumbPositions = "<a href=${getPositionList}><spring:message code='positions'/></a>";
    var breadcrumbSelectedPosition = "<a href=${getPositionDetail}&positionId=${posResourceMappingView.positionId}>${posResourceMappingView.positionName}</a>";

    <c:choose>
        <c:when test="${ ns == '_positionsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbPositions+"&nbsp;&raquo;&nbsp;" +breadcrumbSelectedPosition+"&nbsp;&raquo;&nbsp;"+"<spring:message code='assign_resources'/></h1>");
        </c:when>
        <c:otherwise>
            <%-- Same for Jobs, Projects and Clients--%>
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbSelectedPosition+"&nbsp;&raquo;&nbsp;"+"<spring:message code='assign_resources'/></h1>");
        </c:otherwise>
    </c:choose>

    function ${ns}loadCancelBasedOnContext() {
        <%-- Same for Positions, Jobs, Projects and Clients--%>
        this.document.location.href = '${getPositionDetail}&positionId=${posResourceMappingView.positionId}';
    }
</script>


<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
<h2><spring:message code="assign_resource"/></h2>
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="posResourceMappingView" novalidate="novalidate">
		<form:hidden path="id"/>

			<c:if test="${exist eq 'exist'}">
				<div class="portlet-msg-error"> <spring:message code="validate_duplicate_position_assignment"/></div>
	 		</c:if>
			<div class="layout-table">
				<div class="layout-row">
					<c:if test="${action eq 'edit'}">
						<form:hidden path="resourceId"/>
					</c:if>
					<c:choose>
						<c:when test="${posResourceMappingView.jobId != null && action != 'add'}">
							<form:hidden path="jobId"/>
							<form:hidden path="positionId"/>
							<div class="layout-cell cell-fifty">
								<label for="jobName"><spring:message code="job"/>:</label>
								<c:out value="${posResourceMappingView.jobName}"></c:out>
							</div>
							<div class="layout-cell cell-fifty">
								<label for="positionName"><spring:message code="position"/>:</label>
								<c:out value="${posResourceMappingView.positionName}"></c:out>
							</div>
						</c:when>
						<c:otherwise>
							<div class="layout-cell cell-fifty">
				           		<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job" styleClass="large" 
				           		selectedValue="" requiredMessageCode="validate_job_not_empty" onChange="${ns}ajax_get_positions(this.value);"
				           		focus="true" required="true"/>
							</div>
							<div class="layout-cell cell-fifty">
								<a:selectPosition styleClass="large" name="positionId" labelCode="select_position" selectedValue="${posResourceMappingView.positionId}" 
						   			requiredMessageCode="validate_position_not_empty" form="${ns}form" positionsList="${positionsList}" required="true"/>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>

			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<c:choose>
							<c:when test="${action eq 'edit'}">
								<label for="resourceName"><spring:message code="resource"/>:</label>
								<input type="text" readonly="readonly" name="resourceName" id="resourceName" value="<c:out value="${posResourceMappingView.resourceName}"></c:out>">
							</c:when>
							<c:otherwise>
								<a:selectResource  name="resourceId" labelCode="resource" required="true" selectedValue="${resource.pk}" 
									requiredMessageCode="validate_resource_not_empty" form="${ns}form" resourceList="${resourceList}"/>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="layout-cell cell-fifty">
							<label for="statusId"><spring:message code="status"/>*:</label>
						    <select name="statusId" id="statusId">
							  	<option value=""><spring:message code="select_please"/></option>
							   	<c:forEach var="status" items="${statusList}" >
									<option value="${status.pk}" 
										<c:if test="${posResourceMappingView.statusId eq status.pk}">
												selected="selected" 
										</c:if>
									><c:out value="${status.name}"></c:out></option>
								</c:forEach>
							</select>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-statusId-required"><spring:message code="validate_status_not_empty"/></div>
							<form:errors path="statusId" cssClass="portlet-msg-error" ></form:errors>
						</div>
				</div>
			</div>
			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-twenty-five">
						<a:inputDate styleClass="date" name="startDate" labelCode="start_date" form="${ns}form" value="${posResourceMappingView.startDate}" required=""/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-startDate-dateInOrder">
							<spring:message code="validate_start_before_end"/>
						</div>
					</div>
					<div class="layout-cell cell-twenty-five">
						<a:inputDate styleClass="date" name="endDate" labelCode="end_date" form="${ns}form" value="${posResourceMappingView.endDate}" required=""/>
					</div>
					<div class="layout-cell">
						<label for="percent"><spring:message code="resource_capacity_utilisation"/>:</label>
						<input type="number" name="percent" class="small" id="percent" value="${posResourceMappingView.percent}"/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-percent-positiveInteger"><spring:message code="validate_perzent_positive"/></div>
						<form:errors path="percent" cssClass="portlet-msg-error" ></form:errors>
					</div>
				</div>
			</div>
			<!-- siehe tableResource.jsp fuer div-Inhalt  -->
			<div id="${ns}tableResources-position" class="clearDiv yui-skin-sam"></div>
	    <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	    	<input type="submit"  onclick="${ns}loadCancelBasedOnContext(); return false;" class="cancel" value="<spring:message code="cancel"/>" />
	   </div>
	</form:form>
</div>

<script type="text/javascript">

	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions = function(jobID){
			var positionsSelectLink = '${positions}&jobId=' + jobID;
			jQuery('#${ns}form-positionId').load(positionsSelectLink);
			return false;		
	}
	/**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"positionId", validator:"required"},
    	                     {id:"jobId", validator:"required"},
    	                     {id:"statusId", validator:"required"},
    	                     {id:"resourceId", validator:"required"},
    	                     {id:"percent", validator:"positiveInteger"},
    	                     {id:"startDate", validator:"date"},
    	                     {id:"startDate", validator:"dateInOrder", compareDate:"endDate"},
    	                     {id:"endDate", validator:"date"}
    	                     ];
   }

      
    var ${ns}zaForm = jQuery("#${ns}form"); 
     ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor));
	});*/
     
</script>
<%@ include file="/WEB-INF/jsp/positions/tableResource.jsp" %>

