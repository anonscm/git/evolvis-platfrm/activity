<%@page import="de.tarent.activity.web.util.TokenUtil"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doProjectTable" id="doProjectTable"/>
<portlet:renderURL var="getProjectEdit">
	<portlet:param name="action" value="getProjectEdit"/>
</portlet:renderURL>
<portlet:renderURL var="reportPdf">
	<portlet:param name="action" value="resultPdf"/>
</portlet:renderURL>
<portlet:renderURL var="reportHtml">
	<portlet:param name="action" value="resultHtml"/>
</portlet:renderURL>
<portlet:actionURL var="doProjectDelete" name="doProjectDelete">
	<portlet:param name="<%=TokenUtil.REQUEST_TOKEN_KEY %>" value="<%=(String)request.getAttribute(TokenUtil.REQUEST_TOKEN_KEY) %>"/>
</portlet:actionURL>

<script>
function confirmDelete(){

	if(confirm('<spring:message code="confirm_delete"/>')){
		return true;
	}else{
		return false;
	}
	
}

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
    <div class="layout-table">
    	<div class="layout-cell cell-seventy" >
    		<fieldset>
				<legend><spring:message code="all_reports"/></legend>
		     	<div id="${ns}form-reports" class="clearDiv"></div>
	    	</fieldset>
    	</div>
    </div>    	
    <a:authorize ifAnyGranted="">
	<div>
    	<div class="layout-table">
	    	<div class="layout-cell" style="vertical-align: top;">
		    	<fieldset>
		    		<legend><spring:message code="add_report"/></legend>
		    		<a href="${getProjectEdit}"><spring:message code="add"/></a>
		    	</fieldset>	
	    	</div>
		</div>
	</div>
	</a:authorize>
<script type="text/javascript">

	//custom formatter for table - report as pdf
	YAHOO.widget.DataTable.Formatter.${ns}formatReportPdf = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href='${reportPdf}&reportPk="+oData+"'><spring:message code='pdf'/></a>";
	};
	
	//custom formatter for table - report as html
	YAHOO.widget.DataTable.Formatter.${ns}formatReportHtml = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href='${reportHtml}&reportPk="+oData+"'><spring:message code='html'/></a>";
	};

	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href='${getProjectEdit}&reportPk="+oData+"'><spring:message code='edit'/></a>";
	};

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href='${doProjectDelete}&reportId="+oData+"' onclick='return confirmDelete();'><spring:message code='delete'/></a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatPermission = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("permission") == 0){
			elLiner.innerHTML = "<spring:message code='public'/>";
		}else if(oRecord.getData("permission") == 1){
			elLiner.innerHTML = "<spring:message code='limited'/>";
		} else {
			elLiner.innerHTML = "-";
		}
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"name",label :"<spring:message code='name'/>",sortable :true},
	                	{key :"permission",label :"<spring:message code='permission'/>", formatter:"${ns}formatPermission", sortable :false},
	                	{key :"idTable",label :"<spring:message code='id'/>",sortable :true}, 
		                {key :"note",label :"<spring:message code='note'/>", sortable :false},
	 					<a:authorize ifAnyGranted="">
	 					{key :"id",label :"<spring:message code='edit'/>", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
	 					{key :"id",label :"<spring:message code='pdf'/>", formatter:"${ns}formatReportPdf", sortable :false},
	 					{key :"id",label :"<spring:message code='html'/>", formatter:"${ns}formatReportHtml", sortable :false}
	 					<a:authorize ifAnyGranted="">
	 					,
	 					{key :"id",label :"<spring:message code='delete'/>", formatter:"${ns}formatDelete", sortable :false}
	 					</a:authorize>
	 					];

	//datasource column definitions
	var ${ns}dsFields = [{key :"name"},
	                {key :"permission"},
	                {key :"idTable",parser :"number"},
					{key :"note"},
					{key :"id",parser :"number"}
					];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-reports","${doProjectTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

	
</script>	
</div>

