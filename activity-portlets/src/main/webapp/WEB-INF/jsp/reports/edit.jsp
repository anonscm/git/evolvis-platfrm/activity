<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSaveReport"/>
<portlet:resourceURL var="doCancelReport" id="doCancelReport"/>

<script>
var ${ns}validatorDescriptor=[{id:"name", validator:"required"}
			<c:if test="${reportView.id == null or reportView.id == 0}">
			,{id:"file", validator:"required"}
			</c:if>
        ];

</script>

	
<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doSaveReport}" enctype="multipart/form-data" commandName="reportView">
		<a:token/>
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<fieldset>
						<legend><spring:message code="report"/></legend>
						<input type="hidden" name="id" value="${reportView.id}"/>
						<div class="layout-table">
							<div class="layout-row">
								<div class="layout-cell" >
									<a:inputText name="note" labelCode="note" value="${reportView.note}" form="${ns}form" required="false" styleClass="large" maxlength="255"/>
									<br>
									<a:inputText name="name" labelCode="name" value="${reportView.name}" form="${ns}form" required="true" styleClass="large" maxlength="255" requiredMessageCode="validate_name_not_empty"/>
									<br>
									<label><spring:message code="permission"/>: </label>
									<select name="permission">
										<option value="0"<c:if test="${reportView.permission eq 0}"> selected="selected"</c:if>><spring:message code="public"/></option>
	  									<option value="1"<c:if test="${reportView.permission eq 1}"> selected="selected"</c:if>><spring:message code="limited"/></option>
									</select>
									<br>
								</div>
							</div>
						</div>
						<div class="layout-table">
							<div class="layout-row">
								<div class="layout-cell">
									<input type="file" name="file"  style="width: 100%;" title="label.button_choose_file">
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-file-required">
										<spring:message code="choose_file"/>
									</div>
									<form:errors path="file" cssClass="portlet-msg-error" ></form:errors>
								</div>
							</div>
						</div>
				    </fieldset>
				    <div class="button-div">
				    	<input name="doSaveReport" onclick='return YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor);' type="submit" class="save" value="<spring:message code="save"/>" />
				    	<input name="doCancelReport" type="submit" onclick="jQuery('#${ns}mainDiv').load('${doCancelReport}');return false;" class="save" value="<spring:message code="cancel"/>" />
				   </div>
				</div>
			</div>
		</div>
	</form:form>
</div>


