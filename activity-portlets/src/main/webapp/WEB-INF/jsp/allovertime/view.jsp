<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doOvertimeTable" id="doOvertimeTable"/>
<portlet:resourceURL var="doRequestDelete" id="doRequestDelete"/>
<portlet:renderURL var="doAddOvertime">
    <portlet:param name="ctx" value="doAddOvertime"/>
</portlet:renderURL>
<portlet:resourceURL var="doListProjects" id="doListProjects"/>


<script type="text/javascript">
    var breadcrumbAllOverTime = "<a href='/web/guest/activity/overtime'><spring:message code='overtime'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbAllOverTime+"</h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
<h2><spring:message code="all_overtime"/></h2>
    <form:form method="POST" id="${ns}form" >
		<div class="layout-table">
	    	<div class="layout-row">
	    		<div class="layout-cell cell-thirty-three">
		    		<a:filterSelectResource  ns="${ns}"  
		    			onchangeFunction="${ns}ajax_get_projects(this.value); ${ns}filterByResource(this.value); " resourceList="${resourceList}"/>
		    	</div>	
   				<div class="layout-cell cell-thirty-three">
		    		<a:filterSelectProject  ns="${ns}"
		    			projectList="${projectList}" onchangeFunction="${ns}filterByProject(this.value)"/>
		    	</div>
   				<div class="layout-cell cell-thirty-three">				    		
		    		<a:filterSelectOvertimeType  name="type" labelCode="select_type" 
						form="${ns}form" onchangeFunction="${ns}filterByType(this.value);"/>
				</div>
			</div>
   			<div class="layout-row">
			    <a:filterInputDateFromTo ns="${ns}" formId="${ns}form" name="startDate" dataTable="${ns}myDataTable"/>
   				<div class="layout-cell cell-thirty-three">	</div>						
    		</div>
    	</div>
		    	
    	<div id="${ns}form-overtime" class="clearDiv top-margin"></div>

		<a:authorize ifAnyGranted="Overtime.ADD_ALL">
            <div class="button-div">
                <input type="submit" class="cancel" onclick="YAHOO.de.tarent.goToUrl('${doAddOvertime}');return false;" value="<spring:message code="reduce_overtime"/>" />
            </div>
        </a:authorize>

    </form:form>
   
</div>
 
<script type="text/javascript">

	var ${ns}ajax_get_projects = function(id) {
		var projectsSelectLink = '${doListProjects}&selectedResourceId=' + id;
		jQuery('#${ns}form-projectId').load(projectsSelectLink);
		return false;		
	}
	
	YAHOO.widget.DataTable.Formatter.${ns}formatDeleteCheck = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = " <input type='checkbox' name='selectedForDelete' value="+oData+">";

		var element = elLiner.parentNode.parentNode;
		var children = YAHOO.util.Dom.getChildren(element);
		
		if(oRecord.getData("type") == '1') { 
			YAHOO.util.Dom.addClass(element, "redRow"); 
        } 
        else { 
        	YAHOO.util.Dom.addClass(element, "greenRow"); 
       } 
	};

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${doRequestDelete}&overtimePk="+oData+"');return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}hoursFormatter = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("hours")<0){
			elLiner.innerHTML = "<font color='red'>"+oData+"</font>";	
		}else{
			elLiner.innerHTML = oData;
		}
	};

	
	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		selectedResource :'',
		projectId :'',
		typeId :'',
		startdate :'',
		enddate :''
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"resourceName",label :"<spring:message code='resource'/>", sortable :true},
	                	{key :"projectName",label :"<spring:message code='project'/>",sortable :true}, 
	 					{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"hours",className: "smallColumn",label :"<spring:message code='hours'/>", formatter:"${ns}hoursFormatter", sortable :true},
	 					{key :"date",className: "smallColumn",label :"<spring:message code='date'/>", formatter:"mdyDate", sortable :true}
            <a:authorize ifAnyGranted="Overtime.DELETE_ALL">
	 					,{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
            </a:authorize>
    ];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"resourceId",parser :"number"},
	                {key :"resourceName"},
	                {key :"projectId",parser :"number"},
					{key :"projectName"},
					{key :"description"},
					{key :"hours",parser :"number"},
					{key :"date",parser :"date"},
					{key :"type", parser: "number"}];

	var ${ns}columnStat = { status: true, displayMsg: '<spring:message code="total_overtime"/>'};
	
	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-overtime","${doOvertimeTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat, "date", "desc");

	var ${ns}filterByResource = function(resource){
		jQuery('#${ns}form-type').removeAttr('disabled');
		jQuery('#${ns}form-projectId').removeAttr('disabled');
		YAHOO.de.tarent.${ns}filter.selectedResource = resource;
		${ns}myDataTable.reloadData();
	};
	
	var ${ns}filterByProject = function(project){
		var value = jQuery('#${ns}form-projectId').val();
		if(value != "" ){
			jQuery('#${ns}form-type').attr('disabled','disabled');
			jQuery('#${ns}form-type').val("");
		}else{
			jQuery('#${ns}form-type').removeAttr('disabled');
		}

		YAHOO.de.tarent.${ns}filter.projectId = project;
		${ns}myDataTable.reloadData();
	};
	
	var ${ns}filterByType = function(type){

		var value = jQuery('#${ns}form-type').val();
		if(value == -1){
			jQuery('#${ns}form-projectId').attr('disabled','disabled');
		}else{
			jQuery('#${ns}form-projectId').removeAttr('disabled');
		}
		
		YAHOO.de.tarent.${ns}filter.typeId = type;
		${ns}myDataTable.reloadData();
	};
	
</script>	
		

