<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSaveAddOvertime" name="doSaveAddOvertime">
</portlet:actionURL>
<portlet:renderURL var="doCancel">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbAllOverTime = "<a href='/web/guest/activity/overtime'><spring:message code='overtime'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbAllOverTime+"&nbsp;&raquo;&nbsp;"+"<spring:message code='reduce_overtime'/></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form id="${ns}form" method="POST" action="${doSaveAddOvertime}"  commandName="overtimeView" novalidate="novalidate">
    	<h2><spring:message code="reduce_overtime"/></h2>
			<div class="layout-table ">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
				    	<label><spring:message code="resource"/>*:</label>
				    	<select name="resourceId" id="${ns}-select-resourceId" >
							<option value=""><spring:message code="select_please"/></option>
							<c:forEach var="resource" items="${resourceList}" >
								<option value="${resource.pk}"
									<c:if test="${overtimeView.resourceId eq resource.pk }">
										selected="selected"
									</c:if>>
									<c:out value="${resource.lastname},"></c:out> <c:out value="${resource.firstname}"></c:out>
								</option>
							</c:forEach>
						</select>
						<div class="portlet-msg-error yui-pe-content" id="${ns}-select-resourceId-required">
							<spring:message code="validate_resource_not_empty"/>
						</div>
						<form:errors path="resourceId" cssClass="portlet-msg-error" ></form:errors>
					</div>
					<div class="layout-cell cell-fifty">
						<label><spring:message code="select_type"/></label>
						<spring:message code="reduce"/>
						<form:hidden path="type"/>
					</div>
				</div>
			</div>
			<div class="layout-table">
				<div class="layout-row">
                    <div class="layout-cell cell-fifteen">
                        <a:inputDate styleClass="small date" name="date" labelCode="date" form="${ns}form"
                                     value="${overtimeView.date}" required="true" requiredMessageCode="validate_date_not_empty"/>
                    </div>
					<div class="layout-cell cell-fifteen">
				    	 <a:inputHours name="hours" labelCode="hours" form="${ns}form"
				    	 	value="${(not empty overtimeView.hours) ? overtimeView.hours : 0 }" validationMessageCode="validate_hours_not_empty" required="true"></a:inputHours>
				 	</div>
					<div class="layout-cell cell-seventy">
						<a:inputText name="description" form="${ns}form" maxlength="4000" labelCode="description"
							styleClass="large" required="false" value="${overtimeView.description}"
							requiredMessageCode="validate_description_not_empty"/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
							<spring:message code="validate_description_length"/>
						</div>
					</div>
				</div>
			</div>

		 <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	   		<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${doCancel}');return false;" class="cancel" value="<spring:message code="cancel"/>" />
	    </div>
	</form:form>
</div>
