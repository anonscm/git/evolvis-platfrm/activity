<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doInvoiceDelete" name="doProjectInvoiceDelete">
	<portlet:param name="projectId" value="${project.pk}"/>
</portlet:actionURL>
<portlet:renderURL var="getInvoiceEdit">
    <portlet:param name="ctx" value="getProjectInvoiceEdit"/>
    <portlet:param name="projectId" value="${project.pk}"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceDetail">
    <portlet:param name="ctx" value="getProjectInvoiceDetails"/>
    <portlet:param name="projectId" value="${project.pk}"/>
</portlet:renderURL>
<portlet:resourceURL var="getInvoicesTable" id="getInvoiceTable"/>
<portlet:resourceURL var="getProjectTeamDetails" id="getProjectTeamDetails"/>
<portlet:resourceURL var="getProjectFinancialDetails" id="getProjectFinancialDetails"/>

<%@ include file="/WEB-INF/jsp/projects/breadcrumbs/breadcrumbs.jsp"%>

<div id="${ns}invoiceTab" class="mainDiv yui-skin-sam">
	<h2>
    	<spring:message code="project"/>: <c:out value="${project.name}"/>
    </h2>
    
	<%-- Tabs --%>
	<ul class="tabs ui-tabs">	
   		<li>
   			<a href="#" onclick="YAHOO.de.tarent.goToUrl('${getProjectDetails}');return false;">
   				<spring:message code="details"/>
   			</a>
   		</li>
   		<li>
			<a href="#" onclick="jQuery('#${ns}invoiceTab').load('${getProjectTeamDetails}');return false;">
				<spring:message code="team" />
			</a>
		</li>
        <a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
            <li>
                <a href="#" onclick="jQuery('#${ns}invoiceTab').load('${getProjectFinancialDetails}');return false;">
                    <spring:message code="finance" />
                </a>
            </li>
        </a:authorize>
        <a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
            <li class="current">
                <a href="#">
                    <spring:message code="invoices" />
                </a>
            </li>
        </a:authorize>
   	</ul>

	<div id="${ns}form-bills" class="clearDiv"></div>
	
	<div class="button-div">
        <input type="button" onclick="${ns}loadBackFromProjectDetailsUrl();return false;" class="cancel" value="<spring:message code="back_to_main_mask"/>" />
	</div>

</div>

<script type="text/javascript">
	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceEdit}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
	};
	
	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"if (confirm('<spring:message code="confirm_delete"/>'))${ns}loadInvoiceDeleteUrl('${doInvoiceDelete}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
	}
	
	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceDetail}&invoicePk="+oData+"&backUrl=${getProjectDetails}');return false;\">&nbsp;</a>";
	}

	/** custom formatter for amount column */
	YAHOO.widget.DataTable.Formatter.${ns}formatAmount = function(elLiner, oRecord, oColumn, oData) {
    	numeral.language('de');
    	elLiner.innerHTML = numeral(oData).format('0,0.00 $');
	};

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",label :'<spring:message code="id"/>',sortable :true},
	            		{key :"jobName",label :'<spring:message code="job"/>',sortable :true}, 
	 					{key :"name",label :'<spring:message code="name"/>',sortable :true}, 
	 					{key :"number",label :'<spring:message code="invoice_number"/>',sortable :true},
	 					{key :"amount",label :'<spring:message code="amount"/>', formatter: "${ns}formatAmount", sortable :true},
	 					{key :"invoiceDate",label :'<spring:message code="invoice_date"/>', formatter:"mdyDate", sortable :true},
	 					{key :"payDate",label :'<spring:message code="invoice_pay_date"/>', formatter:"mdyDate", sortable :true},
	 					{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false},
						{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id", parser : "number"},
	                {key : "idTable", parser : "number"},
	                {key : "jobName"},
					{key : "name"},
					{key : "number"},
					{key : "amount", parser : "number"},
					{key : "invoiceDate",parser : "date"},
					{key : "payDate", parser : "date"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-bills","${getInvoicesTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

    function ${ns}loadInvoiceDeleteUrl(invoiceDeleteUrl){
        this.document.location.href = invoiceDeleteUrl;
    };
    function ${ns}loadBackFromProjectDetailsUrl() {
        this.document.location.href = '${getProjectDetailsCancel}&';
    };
</script>
