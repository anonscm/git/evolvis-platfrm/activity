<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getProjectTable" id="getProjectTable" />
<portlet:resourceURL var="getProjectEdit" id="getProjectEdit" />

<portlet:renderURL var="getProjectAdd">
    <portlet:param name="ctx" value="getProjectAdd"/>
</portlet:renderURL>
<portlet:renderURL var="getAllProjectsTable">
    <portlet:param name="ctx" value="getAllProjectsTable"/>
</portlet:renderURL>
<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
</portlet:renderURL>
<portlet:resourceURL var="doProjectsView" id="ajaxView"/>

<script type="text/javascript">
    ${ns}onStateChanged = function (state) {
        if(YAHOO.de.tarent.portletHistoryFlag == true){
            jQuery("#${ns}mainDiv").load(state);
        }
        YAHOO.de.tarent.portletHistoryFlag = true;
    };

    //${ns}init = YAHOO.util.History.getBookmarkedState('${ns}') || '${doProjectsView}';
    YAHOO.de.tarent.history.register("${ns}", "${doProjectsView}", ${ns}onStateChanged);

    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getAllProjectsTable}><spring:message code='projects'/></a></h1>");

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<form:form method="POST" id="${ns}form" action="${getProjectAdd}">
		<h2>
			<spring:message code="projects" />
		</h2>

		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-thirty-three">
					<label for="${ns}form-clientId"><spring:message code="customer" />:</label> 
					<select name="clientId" id="${ns}form-clientId" class="default" onchange="${ns}filterByCustomer(this.value)">
						<option value="">
							<spring:message code="all" />
						</option>
						<c:forEach var="client" items="${customerList}">
							<option value="${client.pk}">
								<c:out value="${client.name}" />
							</option>
						</c:forEach>
					</select>
				</div>
                <!-- TODO: Change the filtering from job status to project status (active, inactive) -->
				<div class="layout-cell cell-thirty-three">
					<label for="${ns}form-status"><spring:message code="status" />:</label> 
					<select name="status" id="${ns}form-status" class="default" onchange="${ns}filterByStatus(this.value)">
						<option value="" selected="selected">
							<spring:message code="all" />
						</option>
						<c:forEach var="status" items="${statusList}">
							<option value="${status.pk}">
								<c:out value="${status.name}" />
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="layout-cell cell-thirty-three">
					<label for="${ns}form-type"><spring:message code="type" />:</label> 
					<select name="type" id="${ns}form-type" class="default" onchange="${ns}filterByType(this.value)">
						<option value="" selected="selected">
							<spring:message code="all" />
						</option>
						<option value="1">
							<spring:message code="external" />
						</option>
						<option value="3">
							<spring:message code="internal" />
						</option>
					</select>
				</div>
			</div>
		</div>
		<div class="button-div top-margin">
			<a:authorize ifAnyGranted="Project.ADD">
				<input type="submit" class="save" value="<spring:message code="new_project"/>" />
			</a:authorize>
		</div>
		<div id="${ns}form-projects"></div>
	</form:form>
</div>

<script type="text/javascript">
	
	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${getProjectEdit}&projectId="+oData+"');return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getProjectDetails}&backUrl=${getAllProjectsTable}&projectId="+oData+"');return false;\">&nbsp;</a>";
	};
	
	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		status : '',
		type : '',
		clientId:''
	};

	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",className:"smallColumn",label :"<spring:message code='id'/>", sortable :true},
	                	{key :"projectName",label :"<spring:message code='project'/>",sortable :true}, 
	 					{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"customerName",label :"<spring:message code='customer'/>",sortable :true},
	 					<a:authorize ifAnyGranted="Project.EDIT_ALL,Project.EDIT_BY_DIVISION">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
 						{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}
	 					];

	
		
	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"idTable",parser :"number"},
					{key :"projectName"},
					{key :"description"},
					{key :"customerName"}];

	//the datatable
	var ${ns}limit = ${config.pageItems};
	var ${ns}offset = ${config.offset};
	var ${ns}sort = "${config.sortColumn}";
	var ${ns}dir = "${config.sortOrder}";
	
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-projects","${getProjectTable}", ${ns}dsFields, ${ns}myColumnDefs,
            YAHOO.de.tarent.${ns}filter, null, "projectName", "asc", ${ns}limit, ${ns}offset, ${ns}sort, ${ns}dir, true);

	var ${ns}filterByStatus = function(status){
		YAHOO.de.tarent.${ns}filter.status = status;
		${ns}myDataTable.reloadData();
	};

	var ${ns}filterByType = function(type){
		YAHOO.de.tarent.${ns}filter.type = type;
		${ns}myDataTable.reloadData();
	};

	var ${ns}filterByCustomer = function(customer){
		YAHOO.de.tarent.${ns}filter.clientId = customer;
		${ns}myDataTable.reloadData();
	};

</script>


