<%@page import="de.tarent.activity.web.util.RequestUtil"%>
<%@page import="de.tarent.activity.domain.Resource"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ include file="/WEB-INF/jsp/projects/breadcrumbs/breadcrumbs.jsp"%>

<portlet:defineObjects/>

<portlet:resourceURL var="getProjectTeamDetails" id="getProjectTeamDetails"/>
<portlet:resourceURL var="getProjectFinancialDetails" id="getProjectFinancialDetails"/>
<portlet:resourceURL var="getProjectInvoiceDetails" id="getProjectInvoiceDetails"/>
<portlet:renderURL var="getProjectInvoices">
    <portlet:param name="ctx" value="projectInvoices"/>
</portlet:renderURL>
<portlet:renderURL var="getProjectEdit">
    <portlet:param name="ctx" value="getProjectEdit"/>
    <portlet:param name="projectId" value="${project.pk}"/>
    <portlet:param name="backUrl" value="${getProjectDetails}"/>
</portlet:renderURL>
<portlet:renderURL var="getJobAdd">
    <portlet:param name="ctx" value="jobUpdate"/>
    <portlet:param name="projectId" value="${project.pk}"/>
    <portlet:param name="projectBeginDate" value="${project.startDate}"/>
    <portlet:param name="projectOwner" value="${project.fkResource}"/>
    <portlet:param name="projectCostCenter" value="${project.accounts}"/>

    <portlet:param name="backUrl" value="${getProjectDetails}"/>
</portlet:renderURL>
<portlet:resourceURL var="doProjectDelete" id="doProjectDelete"/>
<portlet:resourceURL var="getJobTable" id="getJobTable"/>
<portlet:resourceURL var="getProjectExport" id="getProjectExport" />
<portlet:resourceURL var="getProjectChanges" id="showChanges"/>

<div id="${ns}detailsTab" class="mainDiv yui-skin-sam">
	<h2>
		<spring:message code="project" />: <c:out value="${project.name}"></c:out>
    </h2>
	<form:form method="POST" id="${ns}form" action="${getProjectExport}">
		<input type="hidden" name="projectId" value="${project.pk}">
        <input type="hidden" name="backUrl" value="${backUrl}" >

		<%-- Tabs --%>
		<ul class="tabs ui-tabs">
			<li class="current first">
				<a href="#"><spring:message code="details" /></a>
			</li>
			<li>
				<a href="#" onclick="jQuery('#${ns}detailsTab').load('${getProjectTeamDetails}');return false;">
					<spring:message code="team" />
				</a>
			</li>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
			<li>
				<a href="#" onclick="jQuery('#${ns}detailsTab').load('${getProjectFinancialDetails}');return false;">
					<spring:message code="finance" />
				</a>
			</li>
			</a:authorize>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
                <li>
                    <a href="#" onclick="jQuery('#${ns}detailsTab').load('${getProjectInvoiceDetails}');return false;">
                        <spring:message code="invoices" />
                    </a>
                </li>
			</a:authorize>
		</ul>

		<div class="button-div-top">
		<c:choose>
			<c:when test="${isOwner}">
				<input type="button" value="<spring:message code="new_job"/>" onclick="YAHOO.de.tarent.goToUrl('${getJobAdd}'); return false" />
				<input type="submit" value="<spring:message code="edit_project"/>" onclick="YAHOO.de.tarent.goToUrl('${getProjectEdit}');return false;" />
				<input type="submit" value="<spring:message code="changes"/>" onclick="jQuery('#${ns}detailsTab').load('${getProjectChanges}');return false;" />
				<input type="submit" value="<spring:message code="delete"/>" onclick="if (confirm('<spring:message code="confirm_delete_project" arguments="${project.name}"/>'))jQuery('#${ns}detailsTab').load('${doProjectDelete}');return false;"
					<c:if test="${! isDeletableProject}">disabled="disabled"</c:if> />	
			</c:when>
			<c:otherwise>
				<a:isAuthorized var="hasJobAdd" permissions="Job.ADD"/>
				<c:if test="${hasJobAdd}">
					<input type="button" value="<spring:message code="new_job"/>" onclick="YAHOO.de.tarent.goToUrl('${getJobAdd}'); return false" />				
				</c:if>
				<a:isAuthorized var="hasEditAll" permissions="Project.EDIT_ALL,Project.EDIT_BY_DIVISION"/>
				<c:if test="${hasEditAll}">
					<input type="submit" value="<spring:message code="edit_project"/>" onclick="YAHOO.de.tarent.goToUrl('${getProjectEdit}');return false;" />
					<input type="submit" value="<spring:message code="changes"/>" onclick="jQuery('#${ns}detailsTab').load('${getProjectChanges}');return false;" />
				</c:if>
				<a:isAuthorized var="hasDeleteAll" permissions="Project.DELETE_ALL,Project.DELETE_BY_DIVISION"/>
				<c:if test="${hasDeleteAll}">
					<input type="submit" value="<spring:message code="delete"/>" onclick="if (confirm('<spring:message code="confirm_delete_project" arguments="${project.name}"/>'))jQuery('#${ns}detailsTab').load('${doProjectDelete}');return false;"
					<c:if test="${! isDeletableProject}">disabled="disabled"</c:if> />
				</c:if>
			</c:otherwise>
		</c:choose>		
		</div>
		
		<div class="layout-table details">
			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="status" />: </span>
					<c:choose>
						<c:when test="${projectStatus eq 1}">
							<spring:message code="active" />
						</c:when>
						<c:otherwise>
							<spring:message code="inactive" />
						</c:otherwise>
					</c:choose>
				</div>
				<div class="layout-cell">
					<span class="info"><spring:message code="project_start" />: </span>
					<fmt:formatDate value="${project.startDate}" pattern="${dateformat}" />
				</div>
			</div>

			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="project_responsible" />: </span>
					<c:out value="${projectManager.firstname}" />
					<c:out value="${projectManager.lastname}" />
				</div>
				<div class="layout-cell">
					<span class="info"><spring:message code="project_end" />: </span>
					<fmt:formatDate value="${project.endDate}" pattern="${dateformat}" />
				</div>
			</div>

			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="customer" />: </span>
                    <a href=${getClientDetails}>${project.customer.name}</a>
				</div>
				<div class="layout-cell">
					<span class="info"><spring:message code="cost_center" />: </span>
					<c:out value="${project.accounts}" />
				</div>
			</div>

			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="contact_person" />: </span>
					<c:out value="${project.contactPerson}" />
				</div>
				<div class="layout-cell"></div>
			</div>
		</div>

		<div class="total_width top-margin">
			<span class="info"><spring:message
					code="description" />: </span>
			<c:out value="${project.note}" escapeXml="false" />
		</div>

		<div class="layout-table top-margin">
			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="project_links" /></span>
				</div>
			</div>
		</div>

		<div class="total_width top-margin">
			<c:if test="${not empty project.offer}">
					<a href="${project.offer}"><spring:message code="offer" /></a>
			</c:if>
			<c:if test="${not empty project.pageAuftrag}">
					<a href="${project.pageAuftrag}"><spring:message code="order" /></a>
			</c:if>
			<c:if test="${not empty project.crm}">
					<a href="${project.crm}"><spring:message code="crm" /></a>
			</c:if>
			<c:if test="${not empty project.dms}">
					<a href="${project.dms}"><spring:message code="dms" /></a>
			</c:if>
			<c:if test="${not empty project.projectSite}">
					<a href="${project.projectSite}"><spring:message code="evolvis_project_page" /></a>
			</c:if>
			<c:if test="${not empty project.productBacklog}">
					<a href="${project.productBacklog}"><spring:message code="product_backlog" /></a>
			</c:if>
			<c:if test="${not empty project.projectPlan}">
					<a href="${project.projectPlan}"><spring:message code="project_plan" /></a>
			</c:if>
			<c:if test="${not empty project.protocols}">
					<a href="${project.protocols}"><spring:message code="protocols" /></a>
			</c:if>
			<c:if test="${not empty project.riskAnalysis}">
					<a href="${project.riskAnalysis}"><spring:message code="risk_analyse" /></a>&nbsp;
			</c:if>
			<c:if test="${not empty project.sonar}">
					<a href="${project.sonar}"><spring:message code="sonar_results" /></a>
			</c:if>
			<c:if test="${not empty project.specification}">
					<a href="${project.specification}"><spring:message code="specification" /></a>
			</c:if>
		</div>

		<h3 class="top-padding"><spring:message code="jobs" /></h3>
		<div id="${ns}form-jobs" class="clearDiv"></div>

		<a:authorize ifAnyGranted="Activity.EXPORT_ALL">
			<h3 class="top-margin">
				<spring:message code="export_activities" />
			</h3>
				
			<div class="layout-table">
                <div class="layout-row">
                    <div class="layout-cell cell-twenty-five">
                        <a:inputDate name="fromDate" form="${ns}form" labelCode="project_start" styleClass="" required="true" value="${fromDate}" />
                    </div>
                    <div class="layout-cell cell-twenty-five">
                        <a:inputDate name="toDate" form="${ns}form" labelCode="project_end" styleClass="" required="true" value="${toDate}" />
                    </div>
                    <div class="layout-cell cell-fifty input_width cell_bottom">
                        <input type="submit" value="<spring:message code="export"/>" />		                
                    </div>
                </div>
            </div>
			
		</a:authorize>
		
		<div class="button-div">
            <input type="button" onclick="YAHOO.de.tarent.goToUrl('${backUrl}');return false;" class="cancel" value="<spring:message code="back_to_main_mask"/>" />
		</div>
	</form:form>
</div>

<script type="text/javascript">
	
		//custom formatter for table details column
		YAHOO.widget.DataTable.Formatter.${ns}formatDetails= function(elLiner, oRecord, oColumn, oData) {
			<c:if test="${not empty publicJobIdLink}">
				elLiner.innerHTML = "<a href=\"${publicJobIdLink}&jobId="+oData+"\">&nbsp;</a>";
			</c:if>
		};


	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		projectId :'${project.pk}'	
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",className:"smallColumn", label :'<spring:message code="id"/>',sortable :true},
	                    {key :"name",label :'<spring:message code="name"/>',sortable :true},
	                    {key :"description", label : '<spring:message code="description"/>', sortable :true},
	                    {key :"jobStatusName",className:"smallColumn", label :'<spring:message code="status"/>',sortable :true}, 
	 					{key :"jobTypeName",className:"smallColumn", label :'<spring:message code="type"/>',sortable :true}, 
	 					{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                     {key :"idTable",parser :"number"},
	                {key :"jobStatusName"},
					{key :"jobTypeName"},
					{key :"name"},
					{key :"description"}];


	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-jobs","${getJobTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

</script>
