<%-- View for editing or creating projects. --%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%-- if no backUrl is present we go back to client list view. --%>
<c:if test="${empty backUrl}">
	<portlet:renderURL var="backUrl">
        <portlet:param name="ctx" value="getProjectEditCancel"/>
    </portlet:renderURL>
</c:if>
<portlet:actionURL var="doProjectSave" name="doProjectSave">
    <portlet:param name="backUrl" value="${backUrl}"/>
</portlet:actionURL>
<portlet:renderURL var="getAllProjectsTable">
    <portlet:param name="ctx" value="getAllProjectsTable"/>
</portlet:renderURL>
<portlet:renderURL var="getClientDetails">
    <portlet:param name="ctx" value="getClientDetails"/>
    <portlet:param name="clientPk" value="${projectView.clientId}"/>
</portlet:renderURL>
<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="projectId" value="${projectView.id}"/>
</portlet:renderURL>

<script type="text/javascript">
    var clientName = "<c:forEach var='client' items='${customerList}'><c:if test='${projectView.clientId eq client.pk }'> <c:out value='${client.name}'/> </c:if></c:forEach>";
    var breadcrumbCurrentlySelectedClient = "<a href=${getClientDetails}>"+clientName+"</a>";

    <c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbCurrentlySelectedClient+"&nbsp;&raquo;&nbsp;" +"<spring:message code='add_project'/></h1>");
    </c:if>

    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getAllProjectsTable}><spring:message code='projects'/></a>"+"&nbsp;&raquo;&nbsp;" +"<spring:message code='add_project'/></h1>");
    </c:if>


</script>

<div id="${ns}editDiv" class="mainDiv yui-skin-sam">

	<h2>
		<spring:message code="add_project" />: <c:out value="${projectView.projectName}"></c:out>
	</h2>
	<form:form method="POST" id="${ns}form" action="${doProjectSave}" commandName="projectView" novalidate="novalidate">
		<form:errors path="" cssClass="portlet-msg-error" />
		<form:hidden path="id" />
		<fieldset>
			<legend>
				<spring:message code="project_data" />
			</legend>

			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputText name="projectName" form="${ns}form" labelCode="name"  
							required="true" value="${projectView.projectName}" requiredMessageCode="validate_project_name_not_empty" maxlength="255" />
					</div>
					<div class="layout-cell cell-twenty-five">
						<a:inputDate name="startDate" form="${ns}form" labelCode="project_start" styleClass=""
							required="true" value="${projectView.startDate}" requiredMessageCode="validate_date_not_empty" />
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-startDate-dateInOrder">
							<spring:message code="validate_start_before_end" />
						</div>
					</div>
					<div class="layout-cell cell-twenty-five">
						<a:inputDate name="endDate" form="${ns}form" labelCode="project_end" styleClass=""
							value="${projectView.endDate}" required="false" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<label for="${ns}form-clientId"><spring:message code="customer" />*:</label> 
						<select name="clientId" id="${ns}form-clientId">
							<option value="">
								<spring:message code="select_customer" />
							</option>
							<c:forEach var="client" items="${customerList}">
								<option value="${client.pk}"
									<c:if test="${projectView.clientId eq client.pk }">selected="selected"</c:if>>
									<c:out value="${client.name}"/>
								</option>
							</c:forEach>
						</select>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-clientId-required">
							<spring:message code="validate_customer_not_empty" />
						</div>
						<form:errors path="clientId" cssClass="portlet-msg-error"></form:errors>
					</div>

					<div class="layout-cell cell-twenty-five">
						<label for="${ns}form-responsibleId"><spring:message code="project_responsible" />*:</label>
						<select name="responsibleId" id="${ns}form-responsibleId">
							<option value="">
								<spring:message code="select_project_responsible" />
							</option>
							<c:forEach var="resource" items="${resourceList}">
								<option value="${resource.pk}"
									<c:if test="${projectView.responsibleId eq resource.pk }">selected="selected"</c:if>>
									<c:out value="${resource.lastname},"/>
									<c:out value="${resource.firstname}"/>
								</option>
							</c:forEach>
						</select>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-responsibleId-required">
							<spring:message code="validate_project_responsible_not_empty" />
						</div>
						<form:errors path="responsibleId" cssClass="portlet-msg-error"></form:errors>
					</div>

					<div class="layout-cell cell-twenty-five">
						<a:inputText name="costCenter" form="${ns}form" maxlength="8" labelCode="cost_center" styleClass=""
							required="false" value="${projectView.costCenter}" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputText name="contactPerson" form="${ns}form" maxlength="255" labelCode="contact_person"
							required="false" value="${projectView.contactPerson}" />
					</div>
					<div class="layout-cell cell-twenty-five"></div>

					<div class="layout-cell cell-twenty-five"></div>
				</div>
			</div>

			<div class="total_width">
				<label for="${ns}-description"><spring:message code="description" />:</label>
				<textarea id="${ns}-description" name="description" rows="5" cols="60" maxlength="4000"><c:out value="${projectView.description}" /></textarea>
				<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
					<spring:message code="validate_description_length" />
				</div>
				<form:errors path="description" cssClass="portlet-msg-error"></form:errors>
			</div>
		</fieldset>

		<fieldset>
			<legend>
				<spring:message code="economic_conditions" />
			</legend>

			<div class="layout-table auto-width">
				<div class="layout-row auto-width">
					<div class="layout-cell cell-twenty-five">
						<label for="dailyRate"><spring:message code="dayrate"/>:</label>
						<input name="dailyRate" form="${ns}form" maxlength="255" value="${projectView.dailyRate}" />
						&euro;
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-dailyRate-number">
							<spring:message code="validate_daily_rate" />
						</div>
						<form:errors path="dailyRate" cssClass="portlet-msg-error" ></form:errors>
					</div>
					<div class="layout-cell">
						<a:inputDate styleClass="small" name="paymentDate" labelCode="payment_date" form="${ns}form"
							value="${projectView.paymentDate}" required="false" />
					</div>
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>
				<spring:message code="project_additional_information" />
			</legend>

			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="offer" form="${ns}form" maxlength="255" labelCode="offer" required="false"
							value="${projectView.offer}" linkMessageCode="validate_html_link" />
					</div>

					<div class="layout-cell cell-fifty">
						<a:inputLink name="order" form="${ns}form" maxlength="255" labelCode="order" required="false"
							value="${projectView.order}" linkMessageCode="validate_html_link" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="crm" form="${ns}form" maxlength="255" labelCode="crm" required="false"
							value="${projectView.crm}" linkMessageCode="validate_html_link" />
					</div>
					<div class="layout-cell cell-fifty">
						<a:inputLink name="dms" form="${ns}form" maxlength="255" labelCode="dms" required="false"
							value="${projectView.dms}" allowWebdav="true" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="evolvisProjectPage" form="${ns}form" maxlength="255" labelCode="evolvis_project_page"
							required="false" value="${projectView.evolvisProjectPage}" linkMessageCode="validate_html_link" />
					</div>

					<div class="layout-cell cell-fifty">
						<a:inputLink name="productBacklog" form="${ns}form" maxlength="255" labelCode="product_backlog"
							required="false" value="${projectView.productBacklog}" linkMessageCode="validate_html_link" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="projectPlan" form="${ns}form" maxlength="255" labelCode="project_plan"
							required="false" value="${projectView.projectPlan}" linkMessageCode="validate_html_link" />
					</div>

					<div class="layout-cell cell-fifty">
						<a:inputLink name="protocols" form="${ns}form" maxlength="255" labelCode="protocols"
							required="false" value="${projectView.protocols}" linkMessageCode="validate_html_link" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="riskAnalysis" form="${ns}form" maxlength="255" labelCode="risk_analyse"
							required="false" value="${projectView.riskAnalysis}" linkMessageCode="validate_html_link" />
					</div>

					<div class="layout-cell cell-fifty">
						<a:inputLink name="sonar" form="${ns}form" maxlength="255" labelCode="sonar_results"required="false"
							value="${projectView.sonar}" linkMessageCode="validate_html_link" />
					</div>
				</div>

				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<a:inputLink name="specification" form="${ns}form" maxlength="255" labelCode="specification"
							required="false" value="${projectView.specification}" linkMessageCode="validate_html_link" />
					</div>
					<div class="layout-cell cell-fifty"></div>
				</div>
			</div>
		</fieldset>
		
		<div class="button-div">
			<input type="submit" class="save" value="<spring:message code="save"/>" />
			<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${backUrl}'); return false;" class="save" value="<spring:message code="cancel"/>" />
		</div>

	</form:form>
</div>

