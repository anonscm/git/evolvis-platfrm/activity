<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getProjectTeamDetails" id="getProjectTeamDetails"/>
<portlet:resourceURL var="getProjectInvoiceDetails" id="getProjectInvoiceDetails"/>

<%@ include file="/WEB-INF/jsp/projects/breadcrumbs/breadcrumbs.jsp"%>

<script type="text/javascript">
    function ${ns}loadBackFromProjectDetailsUrl() {
        this.document.location.href = '${getProjectDetailsCancel}&';
    };
</script>

<div id="${ns}financialTab" class="mainDiv">
    <h2>
    	<spring:message code="project"/>: <c:out value="${project.name}"/>
    </h2>
	
	<%-- Tabs --%>
   	<ul class="tabs ui-tabs">
   		<li>
   			<a href="#" onclick="YAHOO.de.tarent.goToUrl('${getProjectDetails}');return false;">
   				<spring:message code="details"/>
   			</a>
   		</li>
   		<li>
   			<a href="#" onclick="jQuery('#${ns}financialTab').load('${getProjectTeamDetails}');return false;">
   				<spring:message code="team"/>
   			</a>
   		</li>
        <a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
            <li class="current">
                <a href="#" >
                    <spring:message code="finance"/>
                </a>
            </li>
        </a:authorize>
   		<a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
			<li>
				<a href="#" onclick="jQuery('#${ns}financialTab').load('${getProjectInvoiceDetails}');return false;">
					<spring:message code="invoices" />
				</a>
			</li>
		</a:authorize>
   	</ul>	
    	
	<div class="bottom-margin">
		<div class="layout-table auto-width">
			<div class="layout-row">
				<div class="layout-cell">
		    		<span class="info"><spring:message code="sales"/>:</span> 
		    	</div>
		    	<div class="layout-cell">
	    			<c:if test="${invoicesum!=0}">
	    				<c:out value="${invoicesum}"/> <spring:message code="euro"/>
	    			</c:if>
	    			<c:if test="${invoicesumpayed!=0}">
	    				(<spring:message code="sales_note"/>: <c:out value="${invoicesumpayed}"/> <spring:message code="euro"/>)
	    			</c:if>
				</div>
			</div>
			<div class="layout-row">
				<div class="layout-cell">
					<span class="info"><spring:message code="daily_rate"/>:</span>
				</div>
				<div class="layout-cell">
					<c:choose>
						<c:when test="${project.dayRate eq ''}">
							<c:out value="${project.dayRate}"/>
						</c:when>
						<c:otherwise>
							<c:out value="${project.customer.dayRate}"/>
						</c:otherwise>
					</c:choose>
				</div>		
			</div>
			<div class="layout-row">	
				<div class="layout-cell">
					<span class="info"><spring:message code="payment_date"/>:</span>
				</div>
				<div class="layout-cell">
					<c:choose>
						<c:when test="${project.paymentTarget eq ''}">
							<fmt:formatDate value="${project.paymentTarget}" pattern="${dateformat}"/>
						</c:when>
						<c:otherwise>
							<fmt:formatDate value="${project.customer.paymentTarget}" pattern="${dateformat}"/>
						</c:otherwise>
					</c:choose>
				</div>					
			</div>
		</div>
				
 		<div class="button-div">
             <input type="button" onclick="${ns}loadBackFromProjectDetailsUrl();return false;" class="cancel" value="<spring:message code="back_to_main_mask"/>" />
		</div>
	</div>

</div>
	
<script type="text/javascript">

	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails= function(elLiner, oRecord, oColumn, oData) {
		<c:if test="${not empty publicJobIdLink}">
			elLiner.innerHTML = "<a href=\"${publicJobIdLink}"+oData+"\"><spring:message code='details'/></a>";
		</c:if>
	};


//javascript filter object
YAHOO.de.tarent.${ns}filter = {
	projectId :'${project.pk}'	
};

//datatable column definitions
var ${ns}myColumnDefs = [{key :"idTable",label :'<spring:message code="id"/>',sortable :true},
                    {key :"name",label :'<spring:message code="name"/>',sortable :true},
                    {key :"description", label : '<spring:message code="description"/>', sortable :true},
                    {key :"jobStatusName",label :'<spring:message code="status"/>',sortable :true}, 
 					{key :"jobTypeName",label :'<spring:message code="type"/>',sortable :true}, 
 					{key: "id", label: "<spring:message code='details'/>", formatter: "${ns}formatDetails", sortable: false}];

//datasource column definitions
var ${ns}dsFields = [{key :"id",parser :"number"},
                     {key :"idTable",parser :"number"},
                {key :"jobStatusName"},
				{key :"jobTypeName"},
				{key :"name"},
				{key :"description"}];


//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-jobs","${tableJob}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

</script>

	