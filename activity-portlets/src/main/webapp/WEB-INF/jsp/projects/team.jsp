<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getProjectFinancialDetails" id="getProjectFinancialDetails"/>
<portlet:resourceURL var="getProjectInvoiceDetails" id="getProjectInvoiceDetails"/>

<portlet:resourceURL var="getTeamMembersTable" id="getTeamMembersTable"/>

<portlet:renderURL var="getPositionDetail" >
    <portlet:param name="ctx" value="positionDetails"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/projects/breadcrumbs/breadcrumbs.jsp"%>

<script type="text/javascript">
    function ${ns}loadBackFromProjectDetailsUrl() {
        this.document.location.href = '${getProjectDetailsCancel}&';
    };
</script>

<div id="${ns}teamTab" class="mainDiv">
	<h2>
		<spring:message code="project" />: <c:out value="${project.name}"/>
	</h2>

	<%-- Tabs --%>
	<ul class="tabs ui-tabs">
		<li>
			<a href="#" onclick="YAHOO.de.tarent.goToUrl('${getProjectDetails}');return false;">
				<spring:message code="details" />
			</a>
		</li>
		<li class="current">
			<a href="#">
				<spring:message code="team" />
			</a>
		</li>
		<a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
		<li>
			<a href="#" onclick="jQuery('#${ns}teamTab').load('${getProjectFinancialDetails}');return false;">
				<spring:message code="finance" />
			</a>
		</li>
		</a:authorize>
		<a:authorize ifAnyGranted="Invoice.VIEW_ALL_DETAIL,Invoice.VIEW_BY_PROJECT">
		<li>
			<a href="#" onclick="jQuery('#${ns}teamTab').load('${getProjectInvoiceDetails}');return false;">
				<spring:message code="invoices" />
			</a>
		</li>
		</a:authorize>
	</ul>

		 <div class="layout-table">
		 	<div class="layout-row">
	    		<div class="layout-cell cell-fifty" >
					<h3><spring:message code="project_involved" />:</h3>
					<ul class="NoTopMargin">
						<c:forEach var="resource" items="${resourceList}">
							<li><c:out value="${resource.lastname}, ${resource.firstname}"/></li>
						</c:forEach>
					</ul>
				</div>
				
				<div class="layout-cell cell-fifty" >

					<h3><spring:message code="absent" /></h3>
					<h4><spring:message code="currently" />:</h4>

					<ul class="NoTopMargin">
						<c:forEach var="cLoss" items="${currentLossList}">
							<li>
								<c:out value="${cLoss.resourceByFkResource.lastname}"/>,
                                <c:out value="${cLoss.resourceByFkResource.firstname}"/> (<c:out value="${cLoss.lossType.name}"/>)
								<spring:message code="to" /> <c:out value="${cLoss.endDate}"/>
							</li>
						</c:forEach>
					</ul>
				
					<h4 class="top-margin"><spring:message code="absent_in_the_future" />:</h4>
					<ul class="NoTopMargin">
						<c:forEach var="fLoss" items="${futureLossList}">
							<li>
								<c:out value="${fLoss.resourceByFkResource.lastname}"/>,
                                <c:out value="${fLoss.resourceByFkResource.firstname}"/> (<c:out value="${fLoss.lossType.name}"/>)
								<spring:message code="from" /> <c:out value="${fLoss.startDate}"/>
								<spring:message code="to" /> <c:out value="${fLoss.endDate}"/>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
    <div id="${ns}form-allocations" class="clearDiv"></div>

    <div class="button-div">
        <input type="button" onclick="${ns}loadBackFromProjectDetailsUrl();return false;" class="cancel" value="<spring:message code="back_to_main_mask"/>" />
    </div>
</div>


<script type="text/javascript">
    //ajax call for selecting positions options for the given jobId
    var ${ns}ajax_get_positions = function(jobID){
        var positionsSelectLink = '${getPositionsByJobAndResource}';

        if(jobID.toString() == ""){
            positionsSelectLink += "&isFilter=false";
            jQuery('#${ns}-select-positionId').load(positionsSelectLink);
        }else{
            positionsSelectLink += '&isFilter=true&jobId=' + jobID;
            jQuery('#${ns}-select-positionId').load(positionsSelectLink);
        }

        return false;
    };

    YAHOO.widget.DataTable.Formatter.${ns}formatStatus = function(el, oRecord, oColumn, oData, oDataTable) {
        if( oData == 1){
            el.innerHTML = "<spring:message code='active'/>";
        }else{
            el.innerHTML = "<spring:message code='inactive'/>"
        }
    };

    //custom formatter for table details column
    YAHOO.widget.DataTable.Formatter.${ns}formatPositionDetails = function(elLiner, oRecord, oColumn, oData) {
        var positionDetailUrl = "${getPositionDetail}&positionId="+oRecord.getData('positionId').toString();
        elLiner.innerHTML = "<a href="+positionDetailUrl+">"+oRecord.getData('positionName')+"</a>";
    };

    //javascript filter object
    YAHOO.de.tarent.${ns}filter = {
        projectId :'',
        jobId :'',
        selectedPosition :'',
        selectedResource :''
    };

    var ${ns}myColumnDefs = [{key :"id",className: "smallColumn",label :"<spring:message code='id'/>",sortable :true},
        {key :"resourceName",label :"<spring:message code='resource'/>",sortable :true},
        {key :"statusId",label :"<spring:message code='status'/>", formatter : "${ns}formatStatus", sortable : false},
        {key :"positionName",label :"<spring:message code='position'/>", formatter:"${ns}formatPositionDetails", sortable :false},
        {key :"jobName",label :"<spring:message code='job'/>",formatter:"simpleFormat", sortable :false}
    ];
    var ${ns}dsFields = [{key :"id",parser :"number"},
        {key :"resourceName"},
        {key :"statusId"},
        {key :"positionName"},
        {key :"positionId"},
        {key :"jobName"}];

    //the datatable
    ${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-allocations","${getTeamMembersTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter,
            null, "resourceName", "asc", ${(not empty resourceAllocationRowsPerPage) ? resourceAllocationRowsPerPage : 10});

    //position selection
    var ${ns}filterByPosition = function(position){
        YAHOO.de.tarent.${ns}filter.selectedPosition = position;
        ${ns}myDataTable.reloadData();
    };
    //job selection
    var ${ns}filterByJob = function(job){
        YAHOO.de.tarent.${ns}filter.jobId = job;
        YAHOO.de.tarent.${ns}filter.selectedPosition = '';
        ${ns}myDataTable.reloadData();
    };
    //project selection
    var ${ns}filterByProject = function(project){
        YAHOO.de.tarent.${ns}filter.projectId = project;
        YAHOO.de.tarent.${ns}filter.jobId = '';
        YAHOO.de.tarent.${ns}filter.selectedPosition = '';
        ${ns}myDataTable.reloadData();
    };
    var ${ns}filterByResource = function(resource){
        YAHOO.de.tarent.${ns}filter.selectedResource = resource;
        YAHOO.de.tarent.${ns}filter.jobId = '';
        YAHOO.de.tarent.${ns}filter.selectedPosition = '';
        ${ns}myDataTable.reloadData();
    };

    //history
    ${ns}History = YAHOO.util.History;
    var ${ns}init = "${listPage}";

    var ${ns}portletHistoryFlag;

    var ${ns}OnStateChanged = function (state) {
        if(${ns}portletHistoryFlag == true){
            jQuery('#${ns}mainDiv').load(state);
        }
        ${ns}portletHistoryFlag = true;
    };

    ${ns}History.register("${ns}mainDiv", ${ns}init, ${ns}OnStateChanged);

    var ${ns}handleDoAssign = function () {
        ${ns}portletHistoryFlag = false;
        ${ns}History.navigate("${ns}mainDiv", "${doAssign}");
    };

</script>


