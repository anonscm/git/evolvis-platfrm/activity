<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="projectId" value="${projectView.id}"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbValueSelectedProjects = "<a href=${getProjectDetails}>${projectView.projectName}</a>";

    <c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedProjects+" - <spring:message code="changes"/></h1>");
    </c:if>

    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedProjects+" - <spring:message code="changes"/></h1>");
    </c:if>
</script>

<div id="${ns}changes">

	<form:form method="POST" id="${ns}form" action="${doProjectSave}" commandName="positionView">
	
		<h2>
			<spring:message code="changes"/>
		</h2>




					<div class="clearDiv yui-dt">
						<div class="yui-dt-mask" style="display: none;"></div>
						<table>
							<colgroup>
								<col>
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<thead>
								<tr>
									<th><div class="yui-dt-liner"><spring:message code="changed_date"/></div></th>
									<th><div class="yui-dt-liner"><spring:message code="changed_by"/></div></th>
									<th><div class="yui-dt-liner"><spring:message code="field_name"/></div></th>
									<th><div class="yui-dt-liner"><spring:message code="old_value"/></div></th>
									<th><div class="yui-dt-liner"><spring:message code="new_value"/></div></th>
								</tr>
							</thead>
							<tbody tabindex="0" class="yui-dt-data" style="">

								<c:forEach var="changeInfo" items="${projectChanges}">
									<tr class="yui-dt-rec yui-dt-first yui-dt-even">
										<td headers="yui-dt113-th-positionName "><div
												class="yui-dt-liner">
                                            <fmt:timeZone value="<%=java.util.TimeZone.getDefault().getID()%>">
                                                <fmt:formatDate value="${changeInfo.changeDate}" type="both" dateStyle="full" timeStyle="short" timeZone="${zn}" /> <br />
                                            </fmt:timeZone>
											</div></td>
										<td headers="yui-dt113-th-description "><div
												class="yui-dt-liner">
												<c:out value="${changeInfo.changeUser}" />
											</div></td>
										<td headers="yui-dt113-th-positionStatusName " colspan="3"><div
												class="yui-dt-liner">&nbsp;</div></td>
									</tr>
									<c:forEach var="changeField" items="${changeInfo.changeFields}">

										<tr class="yui-dt-rec yui-dt-first yui-dt-even">
											<td headers="yui-dt113-th-positionStatusName " colspan="2"><div
													class="yui-dt-liner">&nbsp;</div></td>
											<td headers="yui-dt113-th-positionName "><div>
													<c:out value="${changeField.fieldName}" />
												</div></td>
											<td headers="yui-dt113-th-description "><div
													class="yui-dt-liner">
													<c:out value="${changeField.oldValue}" />
												</div></td>
											<td headers="yui-dt113-th-description "><div class="yui-dt-liner">
													<c:out value="${changeField.newValue}" />
												</div></td>
										</tr>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</div>


					<div class="button-div">
						<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${getProjectDetails}');return false;" class="cancel" value="<spring:message code="back"/>" />
					</div>

	</form:form>
</div>
