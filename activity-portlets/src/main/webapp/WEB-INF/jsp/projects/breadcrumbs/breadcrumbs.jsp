<portlet:renderURL var="getProjectDetailsCancel">
    <portlet:param name="ctx" value="getProjectDetailsCancel"/>
</portlet:renderURL>

<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="projectId" value="${project.pk}"/>
</portlet:renderURL>

<portlet:renderURL var="cancelCustomerView">
    <portlet:param name="ctx" value="cancelCustomerView"/>
</portlet:renderURL>
<portlet:renderURL var="getClientDetails">
    <portlet:param name="ctx" value="getClientDetails"/>
    <portlet:param name="clientPk" value="${project.customer.pk}"/>
</portlet:renderURL>

<portlet:renderURL var="getAllProjectsTable">
    <portlet:param name="ctx" value="getAllProjectsTable"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbClients = "<a href=${cancelCustomerView}><spring:message code='customer'/></a>";
    var breadcrumbCurrentlySelectedClient = "<a href=${getClientDetails}>${project.customer.name}</a>";
    var breadcrumbValueAllProjects = "<a href=${getAllProjectsTable}><spring:message code='projects'/></a>";
    var breadcrumbValueSelectedProjects = "<a href=${getProjectDetails}>${project.name}</a>";


    <c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbClients+"&nbsp;&raquo;&nbsp;" +breadcrumbCurrentlySelectedClient+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedProjects+"</h1>");
    </c:if>

    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
            YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueAllProjects+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedProjects+"</h1>");
    </c:if>
</script>