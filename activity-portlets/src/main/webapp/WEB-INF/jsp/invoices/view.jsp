<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="getAllInvoices">
    <portlet:param name="ctx" value="getAllInvoices"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceAdd">
	<portlet:param name="ctx" value="invoiceUpdate"/>
</portlet:renderURL>
<portlet:resourceURL var="getProjects" id="getProjects" />
<portlet:resourceURL var="getJobs" id="getJobs" />
<portlet:resourceURL var="details" id="getProjectInvoiceDetails" />

<script type="text/javascript">
    var breadcrumbValue = "<h1><a href=${getAllInvoices}><spring:message code='invoices'/></a></h1>"
    YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<form:form method="GET" id="${ns}form" action="${getInvoiceAdd}">
		<h2>
			<spring:message code="invoices" />
		</h2>
		<div class="button-div-top">
			<input type="button" onclick="YAHOO.de.tarent.goToUrl('${getInvoiceAdd}'); return false;" value="<spring:message code="new_invoice"/>" >
		</div>

		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-twenty-five">
					<script type="text/JavaScript">
						var e = document.getElementById("${ns}form-customerId");
						var customerId = e.options[e.selectedIndex].value;
					</script>
					<c:set var="disableEditSelects" value="true"/>
						<c:if test="${not empty customerId}">
							<c:set var="disableEditSelects" value="false"/>
						</c:if>
					<label for="${ns}form-customerId"><spring:message code="customer" />:</label> 
					<select id="${ns}form-customerId" name="customerId" onchange=" ${ns}ajax_get_jobs();${ns}ajax_get_projects(this.value);${ns}filterByCustomer(this.value);">
						<option value="">
							<spring:message code="all" />
						</option>
						<c:forEach var="customer" items="${customerList}">
							<option value="${customer.pk}">
								<c:out value="${customer.name}"></c:out>
							</option>
						</c:forEach>
					</select>
				</div>

				<div class="layout-cell cell-twenty-five">
					<a:filterSelectProject ns="${ns}" projectList="${projectList}" disabled="${disableEditSelects}"
						onchangeFunction="${ns}ajax_get_jobs(this.value);${ns}filterByProject(this.value);" />
				</div>

				<div class="layout-cell cell-twenty-five">
					<a:filterSelectProjectJob ns="${ns}" jobsList="${jobList}" selectedValue="${paramJob.pk}"
						disabled="${disableEditSelects}" onchangeFunction="${ns}filterByJob(this.value);" />
				</div>

				<div class="layout-cell cell-twenty-five">
					<label for="${ns}form-type"><spring:message code="status" />:</label> <select id="${ns}form-type" name="type"
						onchange="${ns}filterByType(this.value);">
						<option value="">
							<spring:message code="all" />
						</option>
						<option value="0">
							<spring:message code="new" />
						</option>
						<option value="1">
							<spring:message code="invoiced" />
						</option>
						<option value="2">
							<spring:message code="invoice_pay_date" />
						</option>
						<option value="3">
							<spring:message code="completed" />
						</option>
					</select>
				</div>
			</div>
		</div>


		<div id="${ns}form-bills" class="clearDiv"></div>
	</form:form>
</div>

<script type="text/javascript">

/** Ajax call to retrieve projects by customer id. */
var ${ns}ajax_get_projects = function(customerId){
	if(customerId.toString() != ""){
		document.getElementById('${ns}-select-projectId').removeAttribute("disabled");
        var projectsSelectLink = '${getProjects}&isFilter=true&customerId=' + customerId;
		jQuery('#${ns}-select-projectId').load(projectsSelectLink);
		return false;		
	} else {
		jQuery('#${ns}-select-projectId option:first-child').attr("selected", true);
		document.getElementById('${ns}-select-projectId').setAttribute("disabled", "disabled");		
	}
};


/** Ajax call to retrieve job by project id. */
var ${ns}ajax_get_jobs = function(projectId){
	var e = document.getElementById("${ns}form-customerId");
	var customerId = e.options[e.selectedIndex].value;
	if (projectId == null) {
		projectId = -1;
	}
       
	if(customerId.toString() != ""){
		document.getElementById('${ns}-select-jobId').removeAttribute("disabled");
		var jobsSelectLink = '${getJobs}&isFilter=true&projectId=' + projectId + "&customerId=" + customerId;
		jQuery('#${ns}-select-jobId').load(jobsSelectLink);
	    return false;
	} else {
		jQuery('#${ns}-select-jobId option:first-child').attr("selected", true);
		document.getElementById('${ns}-select-jobId').setAttribute("disabled", "disabled");
	}
};


/** Filter invoices by customer. */
var ${ns}filterByCustomer = function(customer){
	YAHOO.de.tarent.${ns}filter.clientId = customer;
	YAHOO.de.tarent.${ns}filter.projectId = '';
	YAHOO.de.tarent.${ns}filter.jobId = '';
	
	var breadcrumbValue = "<h1><spring:message code='invoices'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	${ns}myDataTable.reloadData();
};

/** Filter invoices by project. */
var ${ns}filterByProject = function(project){
	YAHOO.de.tarent.${ns}filter.projectId = project;
	YAHOO.de.tarent.${ns}filter.jobId = '';
	var breadcrumbValue = "<h1><spring:message code='invoices'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	${ns}myDataTable.reloadData();
};

/** Filter invoices by job. */
var ${ns}filterByJob = function(job){
	YAHOO.de.tarent.${ns}filter.jobId = job;
	var breadcrumbValue = "<h1><spring:message code='invoices'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	${ns}myDataTable.reloadData();
};

/** Filter invoices by type. */
var ${ns}filterByType = function(type){
	YAHOO.de.tarent.${ns}filter.typeId = type;
	var breadcrumbValue = "<h1><spring:message code='invoices'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	${ns}myDataTable.reloadData();
};
</script>
<%@ include file="table.jsp"%>
