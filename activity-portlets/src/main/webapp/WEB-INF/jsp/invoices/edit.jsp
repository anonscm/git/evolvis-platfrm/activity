<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doInvoiceSave" name="doInvoiceSave">
	<portlet:param name="ctx" value="invoiceUpdate"/>
	<portlet:param name="jobId" value="${jobView.id}"/>
</portlet:actionURL>
<portlet:resourceURL var="getJobListForProject" id="getJobsByProject" />
<portlet:renderURL var="getRenderInvoiceCancel"/>

<portlet:renderURL var="getAllInvoices">
    <portlet:param name="ctx" value="getAllInvoices"/>
</portlet:renderURL>

<%-- if no backUrl is present we go back to invoice list view. --%>
<c:if test="${empty backUrl}">
	<portlet:renderURL var="backUrl"/>
</c:if>

<c:choose>
	<c:when test="${backToProjectInvoices}">
		<portlet:resourceURL var="getInvoiceCancel" id="getProjectInvoiceDetails">
			<portlet:param name="publicProjectId" value="${invoiceView.projectId}" />
		</portlet:resourceURL>
	</c:when>
	<c:otherwise>
		<portlet:resourceURL var="getInvoiceCancel" id="getInvoiceCancel" />
	</c:otherwise>
</c:choose>


<script type="text/javascript">
    var breadcrumbValue = "<a href=${getAllInvoices}><spring:message code='invoices'/></a>"+"&nbsp;&raquo;&nbsp;<spring:message code='add_invoice'/>"
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValue+"</h1>");
</script>


<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doInvoiceSave}" commandName="invoiceView" novalidate="novalidate">
		<form:hidden path="id" />
        <input type="hidden" name="backUrl" value="${backUrl}" >
		<h2>
			<spring:message code="add_invoice" />
		</h2>
		<div class="layout-table bottom-margin">
			<div class="layout-row">
				<div class="layout-cell cell-fifty">
					<a:selectProject name="projectId" form="${ns}form" labelCode="project" projectList="${projectList}"
						selectedValue="${invoiceView.projectId}" requiredMessageCode="validate_project_not_empty" required="true"
						onChange="${ns}ajax_get_jobs(this.value);" />
				</div>
				<div class="layout-cell cell-fifty">
					<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobList}" labelCode="job"
						selectedValue="${invoiceView.jobId}" requiredMessageCode="validate_job_not_empty" required="false" />
				</div>
			</div>
		</div>

		<h3>
			<spring:message code="invoice_data" />
		</h3>
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-fifty">
					<a:inputText name="name" form="${ns}form" maxlength="255" labelCode="name"
						requiredMessageCode="validate_name_not_empty" required="true" value="${invoiceView.name}" />
				</div>
				<div class="layout-cell cell-fifty">
					<a:inputText name="number" form="${ns}form" maxlength="255" labelCode="invoice_number" required="false"
						value="${invoiceView.number}" />
				</div>
			</div>
		</div>
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<label for="${ns}form-description"><spring:message code="description" />:</label>
					<textarea id="${ns}form-description" name="description" rows="9" cols="130" maxlength="4000"><c:out value="${invoiceView.description}" /></textarea>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
						<spring:message code="validate_description_length" />
					</div>
					<form:errors path="description" cssClass="portlet-msg-error"></form:errors>
				</div>
			</div>
		</div>
		<div class="layout-table auto-width">
			<div class="layout-row">
				<div class="layout-cell">
					<a:inputDate styleClass="small date" name="invoiceDate" labelCode="invoice_date" form="${ns}form"
						value="${invoiceView.invoiceDate}" required="false" />
				</div>
				<div class="layout-cell">
					<a:inputDate styleClass="small date" name="payDate" labelCode="invoice_pay_date" form="${ns}form"
						value="${invoiceView.payDate}" required="false" />
				</div>
				<div class="layout-cell">
					<a:inputText name="amount" form="${ns}form" maxlength="255" labelCode="amount" required="true"
						requiredMessageCode="validate_amount_not_empty" value="${invoiceView.amount}" />
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-amount-number">
						<spring:message code="validate_digit" />
					</div>
				</div>
			</div>
		</div>

		<div class="button-div">
			<input type="submit" class="save" value="<spring:message code="save"/>" /> 
			<input type="button" onclick="YAHOO.de.tarent.goToUrl('${backUrl}');return false;" class="save" value="<spring:message code="cancel"/>" />
		</div>

	</form:form>
</div>

<script type="text/javascript">
	/** Ajax call to retrieve jobs by project id. */
	var ${ns}ajax_get_jobs = function(projectId) {
		var jobsSelectLink = '${getJobListForProject}&isFilter=false&projectIdId='
				+ projectId;
		jQuery('#${ns}form-jobId').load(jobsSelectLink);
		return false;
	};
</script>

