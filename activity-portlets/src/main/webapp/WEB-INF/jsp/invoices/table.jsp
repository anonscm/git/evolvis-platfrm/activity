<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<portlet:renderURL var="getAllInvoices">
    <portlet:param name="ctx" value="getAllInvoices"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceEdit">
	<portlet:param name="ctx" value="invoiceUpdate"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceDetails">
	<portlet:param name="ctx" value="getProjectInvoiceDetails"/>
    <portlet:param name="backUrl" value="${getAllInvoices}"/>
</portlet:renderURL>
<portlet:resourceURL var="getInvoiceTable" id="getInvoiceTable"/>

<script type="text/javascript">
/** custom formatter for table edit column */
YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
	elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceEdit}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
};

/** custom formatter for table details column */
YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
	elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceDetails}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
};

/** custom formatter for amount column */
YAHOO.widget.DataTable.Formatter.${ns}formatAmount = function(elLiner, oRecord, oColumn, oData) {
    numeral.language('de');
    elLiner.innerHTML = numeral(oData).format('0,0.00 $');
};

/** javascript filter object */
YAHOO.de.tarent.${ns}filter = {
	clientId :'',
	projectId :'',
	jobId :'',
	typeId :''
};

/** datatable column definitions */
var ${ns}myColumnDefs = [{key :"idTable",className: "smallColumn",label :'<spring:message code="id"/>',sortable :true},
                    {key :"name",label :'<spring:message code="invoice"/>',sortable :true},
                    {key :"customerName",label :'<spring:message code="customer"/>',sortable :true},
                    {key :"projectName",label :'<spring:message code="project"/>',sortable :true},
            		{key :"jobName",label :'<spring:message code="job"/>',sortable :true}, 
 					{key :"number",label :'<spring:message code="invoice_number"/>',sortable :true},
 					{key :"amount",label :'<spring:message code="amount"/>', formatter:"${ns}formatAmount" ,sortable :true},
 					{key :"invoiceDate",label :'<spring:message code="invoice_date"/>', formatter:"mdyDate", sortable :true},
 					{key :"payDate",label :'<spring:message code="invoice_pay_date"/>', formatter:"mdyDate", sortable :true},
 					{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
 					{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}];

/** datasource column definitions */
var ${ns}dsFields = [{key :"id",parser :"number"},
                {key :"idTable",parser :"number"},
                {key :"customerName"},
                {key :"projectName"},
                {key :"jobName"},
				{key :"name"},
				{key :"number"},
				{key :"amount", parser :"number"},
				{key :"invoiceDate",parser :"date"},
				{key :"payDate", parser :"date"}];

/** the datatable */
var ${ns}limit = ${config.pageItems};
var ${ns}offset = ${config.offset};
var ${ns}sort = "${config.sortColumn}";
var ${ns}dir = "${config.sortOrder}";
	
${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-bills","${getInvoiceTable}", ${ns}dsFields, 
		${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, null, "idTable", "desc", ${ns}limit, ${ns}offset, ${ns}sort, ${ns}dir);

</script>