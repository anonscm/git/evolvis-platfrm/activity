<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="doInvoiceEdit">
    <portlet:param name="ctx" value="invoiceUpdate"/>
</portlet:renderURL>
<portlet:renderURL var="getRenderInvoiceCancel"/>

<portlet:renderURL var="getProjectInvoices">
    <portlet:param name="ctx" value="projectInvoices"/>
    <portlet:param name="projectId" value="${project.pk}"/>
</portlet:renderURL>

<portlet:renderURL var="getAllInvoices">
    <portlet:param name="ctx" value="getAllInvoices"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceDetails">
    <portlet:param name="ctx" value="getProjectInvoiceDetails"/>
    <portlet:param name="invoicePk" value="${invoiceView.id}"/>
</portlet:renderURL>

<%-- if no backUrl is present we go back to invoice list view. --%>
<c:if test="${empty backUrl}">
    <portlet:renderURL var="backUrl">
    	<portlet:param name="ctx" value="projectInvoices"/>
    	<portlet:param name="projectId" value="${project.pk}"/>
    </portlet:renderURL>
</c:if>

<script type="text/javascript">
    <c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
    	var breadcrumbValue = "<a href=${getProjectInvoices}><spring:message code='invoices'/></a>"+"&nbsp;&raquo;&nbsp;"+
            "<a href=${getInvoiceDetails}>${invoiceView.name}</a>"
    	YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValue+"</h1>");
    </c:if>
    
    <c:if test="${ ns == '_invoiceportlet_WAR_activityportlets_' }">
    	var breadcrumbValue = "<a href=${getAllInvoices}><spring:message code='invoices'/></a>"+"&nbsp;&raquo;&nbsp;"+
            "<a href=${getInvoiceDetails}>${invoiceView.name}</a>"
    	YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValue+"</h1>");
    </c:if>
    
    <c:if test="${ ns == '_invoiceportlet_WAR_activityportlets_' }">
		var breadcrumbValue = "<a href=${getAllInvoices}><spring:message code='invoices'/></a>"+"&nbsp;&raquo;&nbsp;"+
        	"<a href=${getInvoiceDetails}>${invoiceView.name}</a>"
		YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValue+"</h1>");
	</c:if>
</script>


<div id="${ns}mainDiv" class="mainDiv">
<h2><spring:message code="invoice_details"/></h2>
	<a:authorize ifAnyGranted="Invoice.EDIT_ALL">
		<div class="button-div-top">
			<input type="button" onclick="YAHOO.de.tarent.goToUrl('${doInvoiceEdit}&invoicePk=${invoiceView.id}');return false;" value="<spring:message code="edit"/>" />
		</div>
	</a:authorize>
	
	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="name"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">
				<c:out value="${invoiceView.name}"></c:out>
			</div>
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="amount"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">
                <fmt:formatNumber value="${invoiceView.amount}" type="currency"/>
			</div>			
		</div>
		<div class="layout-row">
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="invoice_number"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">	
				<c:out value="${invoiceView.number}"></c:out>
			</div>
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="invoice_date_added"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">				
				<fmt:formatDate value="${invoiceView.invoiceDate}" pattern="${dateformat}"/>
			</div>			
		</div>
		<div class="layout-row">
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="id"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">				
				<c:out value="${invoiceView.id}"></c:out>
			</div>
			<div class="layout-cell cell-twenty-five">
				<span class="info"><spring:message code="pay_date"/>:</span>
			</div>
			<div class="layout-cell cell-twenty-five">
				<fmt:formatDate value="${invoiceView.payDate}" pattern="${dateformat}"/>
			</div>			
		</div>
	</div>
	<div class="total_width top-margin">
  		<span class="info"><spring:message code="description"/>:</span>
		<c:out value="${invoiceView.description}" escapeXml="false"></c:out>
	</div>

	<div class="button-div">
		<input type="button" onclick="YAHOO.de.tarent.goToUrl('${backUrl}');return false;" value="<spring:message code="cancel"/>" />
	</div>
</div>