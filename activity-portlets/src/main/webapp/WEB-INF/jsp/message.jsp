<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="${message}" arguments="${args}" />
