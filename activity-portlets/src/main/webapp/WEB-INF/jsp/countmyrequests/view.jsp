<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div id="${ns}mainDiv" class="mainDiv">

	<h2>To dos</h2>
	
	<div class="portlet_content">
		<ul>
			<c:choose>
				<c:when test="${not empty linkToDeleteRequestsPage }">
					<li><a href="${linkToDeleteRequestsPage}"><spring:message code="count_my_requests" arguments="${numberOfRequests}"/></a></li>
				</c:when>
				<c:otherwise>
					<li><spring:message code="count_my_requests" arguments="${numberOfRequests}"/></li>
				</c:otherwise>
			</c:choose>
			<a:authorize ifAnyGranted="Loss.VIEW,Loss.VIEW_ALL">
                <c:if test="${not empty linkToLossListPage }">
                    <li><a href="${linkToLossListPage}"><spring:message code="count_holiday_requests" arguments="${numberOfLoss}"/></a></li>
                </c:if>
			</a:authorize>
		</ul>
	</div>
	
</div>