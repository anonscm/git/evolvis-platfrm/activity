<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doSaveNews" id="doSaveNews" />
<portlet:resourceURL var="doDetailsGeneralAddresses" id="doDetailsGeneralAddresses"/>
<portlet:resourceURL var="doNews" id="doNews"/>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doSaveNews}" commandName="settingsView">
		<ul class="tabs ui-tabs">	
    		<li ><a href="#" onclick="jQuery('#${ns}mainDiv').load('${doDetailsGeneralAddresses}');return false;"><spring:message code="general_addresses"/></a></li>
    		<li class="current first"><a href="#"><spring:message code="news"/></a></li>
    	</ul>
		
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<fieldset>
						<legend><spring:message code="news"/></legend>
						<input type="text" name="welcomeMessage" value="${settingsView.welcomeMessage}">
					</fieldset>
				</div>
			</div>
		</div>	
		<div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	   </div>
	</form:form>
</div>	
			