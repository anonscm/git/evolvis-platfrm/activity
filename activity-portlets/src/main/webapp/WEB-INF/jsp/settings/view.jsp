<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doSave" id="doSave" />
<portlet:resourceURL var="doDetailsGeneralAddresses" id="doDetailsGeneralAddresses"/>
<portlet:resourceURL var="doNews" id="doNews"/>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="settingsView">
		<ul class="tabs ui-tabs">	
    		<li class="current first"><a href="#"><spring:message code="general_addresses"/></a></li>
    		<li><a href="#" onclick="jQuery('#${ns}mainDiv').load('${doNews}');return false;"><spring:message code="news"/></a></li>
    	</ul>
		
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<fieldset>
						<legend><spring:message code="resource_management"/></legend>
						<input type="text" name="resourceManagementMail" value="${settingsView.resourceManagementMail}">
					</fieldset>
				</div>
			</div>
			<div class="layout-row">
				<div class="layout-cell">
					<fieldset>
						<legend><spring:message code="personnel_department"/></legend>
						<input type="text" name="personnelOfficeMail" value="${settingsView.personnelOfficeMail}">
					</fieldset>
				</div>
			</div>
		</div>
		<div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	   </div>
	</form:form>
</div>
						