<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="a" tagdir="/WEB-INF/tags"%>

<c:set var="ns"><portlet:namespace/></c:set>

<!-- This variable is used in alot of portlets to set the display settings for date format from the language setting  -->
<spring:message var="dateformat" code="date_format"/>

<c:if test="${not empty successMessage}">
	<div id="successMsg" class="portlet-msg-success"><spring:message code="${successMessage}" arguments="${messageArgs}" text="Die Aktion war erfolgreich"/></div>		
</c:if>
<c:if test="${not empty warningMessage}">
	<div id="warningMsg" class="portlet-msg-alert"><spring:message code="${warningMessage}" arguments="${messageArgs}" text="Es liegt eine Warnung vor"/></div>
</c:if>
<c:if test="${not empty errorMessage}">
	<div id="errorMsg" class="portlet-msg-error"><spring:message code="${errorMessage}" arguments="${messageArgs}" text="Es ist ein Fehler aufgetreten"/></div>
</c:if>

<!-- <script type="text/javascript">
jQuery(document).ready(function() {
	var showNotifications = '${successMessage}' != '' || '${warningMessage}' != '' || '${errorMessage}' != '';
	if (showNotifications) {
		jQuery('#notifications').show();
		jQuery('#notifications').fadeOut(3000);
	}
});
</script> -->
