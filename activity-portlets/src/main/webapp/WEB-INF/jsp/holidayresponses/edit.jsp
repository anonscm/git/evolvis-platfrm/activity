<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSaveAnswer" name="doSaveAnswer" />
<portlet:renderURL var="getAllHolidays">
    <portlet:param name="ctx" value="showHolidayResponses"/>
</portlet:renderURL>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="post" id="${ns}form" action="${doSaveAnswer}" novalidate="novalidate">
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<fieldset>
						<legend><spring:message code="holiday"/></legend>
						<input type="hidden" name="lossId" value="${lossView.id}">
						<div class="layout-table">
							<div class="layout-row">
								<div class="layout-cell" >
									<spring:message code="name"/>: <c:out value="${lossView.resourceName}"></c:out>
									<br>
									<spring:message code="period"/>: <fmt:formatDate value="${lossView.startDate}" pattern="${dateformat}"/> - <fmt:formatDate value="${lossView.endDate}" pattern="${dateformat}"/>
									<br>
									<spring:message code="description"/>: <c:out value="${lossView.description}"></c:out>
								</div>
							</div>
							<div class="layout-row">
								<div class="layout-cell" >
					            	<label for="${ns}form-typeId"><spring:message code="status"/>:</label>
									<select name="answerStatusId" id="${ns}form}-statusId" class="">
										<option value="" selected="selected"><spring:message code="select_please"/></option>
										<option value="2"><spring:message code="agree"/></option>
										<option value="3"><spring:message code="reject"/></option>
									</select>
								</div>
							</div>
						</div>
				    </fieldset>
				</div>
			</div>
		</div>
				    <div class="button-div">
				    	<input type="submit" class="save" value="<spring:message code="save"/>" />
				    	<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${getAllHolidays}');return false;" class="save" value="<spring:message code="cancel"/>" />
				   </div>
	</form:form>
</div>
