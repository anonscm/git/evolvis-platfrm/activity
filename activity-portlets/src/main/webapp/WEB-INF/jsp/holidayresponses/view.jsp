<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:renderURL var="doEdit">
    <portlet:param name="ctx" value="doEdit"/>
</portlet:renderURL>

<c:if test="${currentLoggedInUserRole eq 'Projektleiter' || currentLoggedInUserRole eq 'Controller' }">
    <div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
        <form:form method="POST" id="${ns}form">
                    <h2><spring:message code="holiday_requests"/></h2>
                    <div id="${ns}form-loss" class="clearDiv"></div>
        </form:form>
    </div>
</c:if>
<script type="text/javascript">

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${doEdit}&lossPk="+oData+"');return false;\">&nbsp;</a>";
	};


	YAHOO.widget.DataTable.Formatter.${ns}formatStatus = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("typeId") == 2 ){
			elLiner.innerHTML = "-";
		}else{
			elLiner.innerHTML = oData;
		}
	};

	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"resourceName",label :"<spring:message code='resource'/>",sortable :true},
	                	{key :"startDate",label :"<spring:message code='from'/>", formatter:"mdyDate", sortable :true},
	                    {key :"endDate",label :"<spring:message code='to'/>", formatter:"mdyDate", sortable :true},
	                    {key :"typeLabel",label :"<spring:message code='type'/>",sortable :true},
	 					//{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"statusLabel",label :"<spring:message code='status'/>",formatter:"${ns}formatStatus", sortable :true},
	 					{key :"days",label :"<spring:message code='days'/>", sortable :true},
	 					{key :"answers",label :"<spring:message code='answers'/>", formatter:"simpleFormat"},
	 					{key :"id",label :"&nbsp;", className: "editColumn", formatter:"${ns}formatEdit", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"resourceName"},
	            	{key :"id",parser :"number"},
	                {key :"startDate",parser :"date"},
	                {key :"endDate",parser :"date"},
					{key :"typeLabel"},
					{key :"typeId",parser :"number"},
					//{key :"description"},
					{key :"statusLabel"},
					{key :"days",parser :"number"},
					{key :"answers"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-loss","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);


</script>
