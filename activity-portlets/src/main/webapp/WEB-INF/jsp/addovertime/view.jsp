<%@ include file="/WEB-INF/jsp/include.jsp"%>


<portlet:renderURL var="view"/>
<portlet:actionURL var="doAdd" name="doAdd">
	<portlet:param name="redirect" value="${view}"/>
</portlet:actionURL>

<portlet:renderURL var="getMyOvertimeTable">
    <portlet:param name="ctx" value="getMyOvertimeTable"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyOvertimeTable}><spring:message code='my_overtime'/></a></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form id="${ns}form" method="POST" action="${doAdd}" commandName="overtimeView" novalidate="novalidate" onsubmit="return ${ns}validateForm();">
        <input type="hidden" name="overtimeViewId" value="${overtimeView.id}">
        <input type="hidden" name="resourceId" id="${ns}form-resourceId" value="${overtimeView.resourceId}">
        <input type="hidden" name="resourceName" id="${ns}form-resourceName" value="${overtimeView.resourceName}">

        <h2><spring:message code="add_overtime"/></h2>
		<div class="layout-table ">
			<div class="layout-cell cell-fifty">
		    	<a:selectProject  name="projectId" labelCode="select_project" selectedValue="${overtimeView.projectId}" requiredMessageCode="validate_project_not_empty" 
		    		projectList="${projectList}" form="${ns}form" required="true"/>
	    		<div class="portlet-msg-error yui-pe-content" id="${ns}form-projectId-projectOvertime">
					<spring:message code="validate_project_not_empty"/>
				</div>
			</div>
			<div class="layout-cell cell-fifty inline input_width">
				<a:inputOvertimeType  name="type" labelCode="select_type" selectedValue="${overtimeView.type}" inputSelectId="${ns}form-projectId" 
		    		requiredMessageCode="validate_type_not_empty" form="${ns}form" />
			</div>
		</div>
		<div class="layout-table">
			<div class="layout-cell cell-fifteen">
				<a:inputDate styleClass="small date" name="date" labelCode="date" form="${ns}form" 
					value="${overtimeView.date}" required="true" requiredMessageCode="validate_date_not_empty"/>
			</div>
			<div class="layout-cell cell-fifteen">
		    	 <a:inputHours  name="hours" labelCode="hours" form="${ns}form" 
		    	 	value="${(not empty overtimeView.hours) ? overtimeView.hours : 0 }" validationMessageCode="validate_hours_range" required="true"/>
		 	</div>   
			<div class="layout-cell cell-seventy">
				<a:inputText name="description" form="${ns}form" maxlength="4000" labelCode="description" 
					styleClass="large" required="false" value="${overtimeView.description}" requiredMessageCode="validate_description_not_empty"/>
				<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
					<spring:message code="validate_description_length"/>
				</div>				
			</div>
		</div>

		 <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" /> 
	    </div>
	    
	</form:form>
</div>

<script type="text/javascript">
    function ${ns}validateForm(){
        var hoursValue = document.getElementById('${ns}form-hours').value;

        if( YAHOO.de.tarent.hoursIsNotEmptyAndContainsAValidNumber(hoursValue) ){
           return true;
        }else {
            alert('<spring:message code="hours"/>'+': '+'<spring:message code="validate_digit"/>');
            return false;
        }
    };
</script>


