<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:renderURL var="doEdit">
    <portlet:param name="ctx" value="doEdit"/>
</portlet:renderURL>
<portlet:renderURL var="doAddLoss" >
    <portlet:param name="ctx" value="doAddLoss"/>
</portlet:renderURL>

<portlet:renderURL var="getMyHolidayTable">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyHolidayTable}><spring:message code='my_absence'/></a></h1>");
</script>


<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >

    <h2><spring:message code="my_absence"/></h2>
    <form:form method="POST" id="${ns}form" action="${doAddLoss}">

	<div class="button-div-top">
		<input type="button" onclick="${ns}loadApplyHolidayUrl(); return false;" value="<spring:message code="enter_holiday"/>" />
	</div>

	<div id="${ns}form-loss" class="clearDiv"></div>

		    		<div class="layout-table top-margin holidayOverview">
		    			<div class="layout-row">
		    				<div class="layout-cell cell-fifty">
		    					<span class="info"><spring:message code="holiday_this_year"/>: </span>
		    						<span><fmt:formatNumber type="number" pattern="##0.00" value="${lossDaysThisYear}"/></span>
		    				</div>
		    				<div class="layout-cell cell-fifty">
		    					<span class="info"><spring:message code="remaining_holidays_this_year"/>: </span>
		    						<span><fmt:formatNumber type="number" pattern="##0.00" value="${remainingLossDaysFromThisYear}"/></span>
		    				</div>
						</div>

						<div class="layout-row">
		    				<div class="layout-cell cell-fifty">
		    					<span class="info"><spring:message code="contract_holidays"/>: </span>
		    					<span><fmt:formatNumber type="number" pattern="##0.00" value="${contractLossDays}"/></span>
		    				</div>
		    				<div class="layout-cell cell-fifty">
		    					<span class="info"><spring:message code="remaining_holidays_last_year"/>: </span>
		    					<span><fmt:formatNumber type="number" pattern="##0.00" value="${remainingLossDaysFromLastYear}"/></span>
		    				</div>
		    			</div>
		    			<div class="layout-row">
		    				<div class="layout-cell cell-fifty">
		    					<span class="info"><spring:message code="remaining_holidays_sum"/>: </span>
		    					<span><fmt:formatNumber type="number" pattern="##0.00" value="${remainingLossDaysSum}"/></span>
		    				</div>
		    			</div>
		    		</div>

    </form:form>
</div>

<script type="text/javascript">

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("statusId") != 2 &&  oRecord.getData("statusId") != 4){
			elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}loadEditHolidayUrl("+oRecord.getData("id")+"); return false;\">&nbsp;</a>";
		}

	var element = elLiner.parentNode.parentNode;
		if (oRecord.getData("statusId") == 2 ){
			YAHOO.util.Dom.addClass(element, "greenRow");
        }
		if (oRecord.getData("statusId") == 3 ){
        	YAHOO.util.Dom.addClass(element, "redRow");
       }
	};

	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"startDate",className: "smallColumn",label :"<spring:message code='from'/>", formatter:"mdyDate", sortable :true},
	                    {key :"endDate",className: "smallColumn",label :"<spring:message code='to'/>", formatter:"mdyDate", sortable :true},
	                    {key :"typeLabel",className: "smallColumn",label :"<spring:message code='type'/>", sortable :true},
	 					{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"statusLabel",className: "smallColumn",label :"<spring:message code='status'/>",sortable :true},
	 					{key :"days",className: "smallColumn",label :"<spring:message code='days'/>", sortable :true},
	 					{key :"answers",label :"<spring:message code='answers'/>", formatter:"simpleFormat"},
	 					{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"startDate",parser :"date"},
	                {key :"endDate",parser :"date"},
					{key :"typeLabel"},
					{key :"description"},
					{key :"statusLabel"},
					{key :"days",parser :"number"},
					{key :"answers"},
					{key :"statusId" ,parser :"number"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-loss","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, null, "startDate", "desc",
			${config.pageItems}, ${config.offset}, "${config.sortColumn}", "${config.sortOrder}");

    function ${ns}loadApplyHolidayUrl() {
        this.document.location.href = '${doAddLoss}';
    };

    function ${ns}loadEditHolidayUrl(lossPk) {
        this.document.location.href = '${doEdit}&lossPk='+lossPk;
    };
</script>


