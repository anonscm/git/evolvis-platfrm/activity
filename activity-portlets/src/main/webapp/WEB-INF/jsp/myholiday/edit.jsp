<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSave" name="doSave"/>
<portlet:renderURL var="doCancel" >
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>
<portlet:renderURL var="getMyHolidayTable">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<script type="text/javascript">
    function verifyManagerListPermision(type){
            if(document.getElementById('${ns}form-resourceList')!= undefined){
                if(type == 1){
                    document.getElementById('${ns}form-resourceList').disabled="";
            } else {
                document.getElementById('${ns}form-resourceList').disabled="disabled";
            }
        }
    }
    <c:choose>
        <c:when test="${not empty lossView.typeId}">
            YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyHolidayTable}><spring:message code='my_absence'/></a>"+
                    "&nbsp;&raquo;&nbsp;"+"<spring:message code='edit_holiday'/></h1>");
        </c:when>
        <c:otherwise>
        YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyHolidayTable}><spring:message code='my_absence'/></a>"+
                "&nbsp;&raquo;&nbsp;"+"<spring:message code='enter_holiday'/></h1>");
        </c:otherwise>
    </c:choose>
</script>



<div id="${ns}mainDiv" class="mainDiv">

    <h2><spring:message code="enter_holiday"/></h2>
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="lossView" novalidate="novalidate">

		<form:hidden path="id"/>
		<div class="layout-table">
			<div class="layout-cell cell-fifty" >
            	<label for="${ns}form-typeId"><spring:message code="type"/>*:</label>
				<select name="typeId" id="${ns}form-typeId-select" class="" onchange="verifyManagerListPermision(this.value);">
					<c:choose>
						<c:when test="${fn:length(lossTypeList)>0}">
							<option value="" selected="selected"><spring:message code="select_please"/></option>
							<c:forEach var="lossType" items="${lossTypeList}">
								<option value="${lossType.pk}" <c:if test="${lossView.typeId eq lossType.pk }">selected="selected"</c:if>><c:out value="${lossType.name}"></c:out></option>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<option value="" selected="selected"><spring:message code="select_job"/></option>
						</c:otherwise>
					</c:choose>
				</select>
				<div class="portlet-msg-error yui-pe-content" id="${ns}form-typeId-required">
					<spring:message code="validate_type_not_empty"/>
				</div>
				<form:errors path="typeId" cssClass="portlet-msg-error" ></form:errors>
			</div>
			<div class="layout-cell ">
			   	<a:inputDate styleClass="" name="startDate" labelCode="from" form="${ns}form" 
			   		value="${lossView.startDate}" required="true" requiredMessageCode="validate_date_not_empty"/>
		   		<div class="portlet-msg-error yui-pe-content" id="${ns}form-startDate-dateInOrder">
					<spring:message code="validate_start_before_end"/>
				</div>
		 	</div>
		 	<div class="layout-cell ">
				<a:inputDate styleClass="" name="endDate" labelCode="to" form="${ns}form" 
			   		value="${lossView.endDate}" required="true" requiredMessageCode="validate_date_not_empty"/>
			</div> 
		</div>
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell">
					<label for="${ns}form-typeId"><spring:message code="description"/>:</label>
		    		<input type="text" name="description" maxlength="4000" value="<c:out value="${lossView.description}"></c:out>">
		    		<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
		    		<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
						<spring:message code="validate_description_length"/>
					</div>
				</div>
				
			</div>
		</div>
		<div class="layout-table auto-width">
			<div class="layout-row auto-width">
				<div class="layout-cell">
					<c:if test="${lossView.typeId ne 2}">
						<label for="${ns}form-typeId"><spring:message code="project_manager"/>:</label>
						<c:choose>
							<c:when test="${lossView.id>0}">

								<c:forEach var="answer" items="${answersList}">
					    			<div id="${ns}form-answer-div-${answer.pk}">
						    			<c:out value="${answer.firstname}"></c:out> <c:out value="${answer.lastname}"></c:out>
					    			</div>
					    		</c:forEach>

				    		</c:when>
							<c:otherwise>											
								<select id="${ns}form-resourceList" onchange="${ns}addManager();">
					    			<option value=""><spring:message code="select_project_manager"/></option>
					    			<c:forEach var="resource" items="${responsibleManagerList}">
					    				<option value="${resource.pk}"><c:out value="${resource.lastname}"></c:out>, <c:out value="${resource.firstname}"></c:out></option>
					    			</c:forEach>
					    		</select>

					    		<div id="${ns}form-answer"></div>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
			</div>
		</div>

    <div class="button-div">
    	<input type="submit" class="save" value="<spring:message code="save"/>" />
    	<input type="submit" onclick="${ns}loadCancelHolidayUrl(); return false;" class="save" value="<spring:message code="cancel"/>" />
   </div>

	</form:form>
</div>

<script type="text/javascript">

    ${ns}disableProjectManagerSelectIfVacationTypeIsNotUrlaub();
    
    function ${ns}addAManagerToTheView( managerId, managerText){

        div = document.getElementById("${ns}form-answer");
        items = div.getElementsByTagName("div");

        if((items == null || items.length<5) && managerId!=""){
            if (document.getElementById("${ns}form-answer-div-"+managerId) == null) {
                var oldHTML = document.getElementById('${ns}form-answer').innerHTML;
                var newHTML = YAHOO.lang.escapeHTML(managerText)+"<input type='hidden' name='selectedAnswersList' value='"+managerId+"'>";
                var ahtml =  "&nbsp;<a href=\"#\" onclick=\"${ns}removeManager('"+managerId+"')\">x</a>"
                var divhtml = "<div id='${ns}form-answer-div-"+managerId+"'>"+newHTML+ahtml+"</div>"

                document.getElementById('${ns}form-answer').innerHTML = oldHTML + divhtml;
            }
            var select = document.getElementById("${ns}form-resourceList");
            var opt = select.options[select.selectedIndex].style="display: none;";
        }
        if(items.length>= 5){
        	document.getElementById('${ns}form-resourceList').disabled="disabled";
        }
    };
    
    var hiddenManageroptions = new Array();

	function ${ns}addManager(){
		var sel = document.getElementById('${ns}form-resourceList');
		var opt = sel.options[sel.selectedIndex];

		hiddenManageroptions[opt.value] = sel.selectedIndex;
        ${ns}addAManagerToTheView( opt.value, opt.text);
		sel.selectedIndex = 0;
	};

	function ${ns}removeManager(pk){
		var element = document.getElementById('${ns}form-answer-div-'+pk);
		var parent = document.getElementById('${ns}form-answer');
		parent.removeChild(element);
        items = parent.getElementsByTagName("div");
        if(items.length < 5){
        	document.getElementById('${ns}form-resourceList').disabled="";
        }
		
		var select = document.getElementById("${ns}form-resourceList");
		var option=select.options[hiddenManageroptions[pk]].style="display: ;";
	};		

    function ${ns}loadCancelHolidayUrl() {
        this.document.location.href = '${doCancel}';
    };

    function ${ns}disableProjectManagerSelectIfVacationTypeIsNotUrlaub(){
        var urlaubTypeSelect = document.getElementById('${ns}form-typeId-select');
        if(urlaubTypeSelect != undefined){
            if(urlaubTypeSelect.options[urlaubTypeSelect.selectedIndex].value == 1){
                document.getElementById('${ns}form-resourceList').disabled="";
                var selectedCommaList =  '${selectedAnswersList}';
                var selectedList = selectedCommaList.split(",");

                for(var i=0;i<selectedList.length;i++){
                    ${ns}addAManagerToTheView( selectedList[i], ${ns}getManagerTextFromId( selectedList[i] ));
                }

            } else {
                document.getElementById('${ns}form-resourceList').disabled="disabled";
            }
        }

    };

    function ${ns}getManagerTextFromId(managerId){
        var managerSel = document.getElementById('${ns}form-resourceList');
        for(i=0;i<managerSel.length;i++){
            if( managerSel.options[ i ].value == managerId){
                return managerSel.options[ i ].text;
            }
        }
        return "NULL";

    };

</script>

