<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:actionURL var="doDelete" name="doDelete"/>

<portlet:renderURL var="getAllRequests">
    <portlet:param name="ctx" value="getAllRequests"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbValue = "<h1><a href=${getAllRequests}><spring:message code='all_delete_requests'/></a></h1>"
    YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
<h2><spring:message code="all_delete_requests"/></h2>
    <form:form id="${ns}form">

	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-thirty-three">
	    			<a:filterSelectResource ns="${ns}"  
	    				onchangeFunction="${ns}filterByResource(this.value);" resourceList="${resourceList}"/>
			</div>
			<div class="layout-cell cell-thirty-three">
	    			<a:filterSelectResource ns="${ns}" labelCode="controller" 
	    				onchangeFunction="${ns}filterByController(this.value);" resourceList="${resourceList}"/>
			</div>
			<div class="layout-cell cell-thirty-three">
	    		
	    			<label for="${ns}form-status"><spring:message code="status"/></label>
	    			<select id="${ns}form-status" name="status" onchange="${ns}filterByStatus(this.value);">
	    				<option value=""><spring:message code="all"/></option>
	    				<option value="open"><spring:message code="select_open"/></option>
	    				<option value="deleted"><spring:message code="select_deleted"/></option>
	    				<option value="cancelled"><spring:message code="select_countermanded"/></option>
	    			</select>
			</div>
		</div>
	</div>		    	
			    		
    <div id="${ns}form-requests" class="clearDiv"></div>

    </form:form>
</div>  

<script type="text/javascript">

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}deleteConfirmation("+oData+");\">&nbsp;</a>";
	};

	function ${ns}deleteConfirmation(oData) {
		var answer = confirm("<spring:message code='confirm_delete'/>");
		if (answer){
			this.document.location.href = '${doDelete}&requestPk='+oData;	
		}
	}
	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		selectedResource :'',
		selectedController:'',
		selectedStatus:''
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",className: "smallColumn",label :"<spring:message code='id'/>",sortable :true},
	                    {key :"resourceName",label :"<spring:message code='user'/>",sortable :true},
	                    {key :"dataType", label : "<spring:message code='data_type'/>", sortable :true},
	                    {key :"dataTypeId",label :"<spring:message code='data_type_id'/>", sortable :true}, 
	 					{key :"description",label :"<spring:message code='description'/>", sortable :true}, 
	 					{key :"status",label :"<spring:message code='status'/>", sortable :true},
	 					{key: "controllerName", label: "<spring:message code='controller'/>", sortable: true},
	 					{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                     {key :"idTable",parser :"number"},
	                {key :"resourceName"},
	                {key :"dataType"},
	                {key :"dataTypeId", parser:"number"},
					{key :"description"},
					{key :"status"},
					{key :"controllerName"}];


	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-requests","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
		
	//request resource filter
	var ${ns}filterByResource = function(resource){
		YAHOO.de.tarent.${ns}filter.selectedResource = resource;
		${ns}myDataTable.reloadData();
	}
	
	//request controller filter
	var ${ns}filterByController = function(controller){
		YAHOO.de.tarent.${ns}filter.selectedController = controller;
		${ns}myDataTable.reloadData();
	}
	
	//request status filter
	var ${ns}filterByStatus = function(status){
		YAHOO.de.tarent.${ns}filter.selectedStatus = status;
		${ns}myDataTable.reloadData();
	}
	
</script>	

