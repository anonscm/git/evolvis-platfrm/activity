<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSaveLoss" name="doSaveLoss"/>
<portlet:resourceURL var="doGetDaysValue" id="doGetDaysValue"/>

<portlet:renderURL var="getAllHolidays">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<portlet:renderURL var="showEdit">
    <portlet:param name="ctx" value="showEdit"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbAllHolidays = "<a href=${getAllHolidays}><spring:message code='all_absence'/></a>";
    var breadcrumbEditHoliday = "<a href=${showEdit}&lossPk=${lossView.id}><spring:message code='edit'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbAllHolidays+"&nbsp;&raquo;&nbsp;"+breadcrumbEditHoliday+"</h1>");

</script>

<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doSaveLoss}" commandName="lossView" novalidate="novalidate">
		<input type="hidden" name="allHoliday" value="${allHoliday}">
		<h2><spring:message code="holiday"/>: <c:out value="${lossView.resourceName}"/></h2>

		<form:hidden path="id"/>
		<form:hidden path="typeId"/>

		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-thirty-three">
				   	<a:inputDate name="startDate" labelCode="from" form="${ns}form"
				 		value="${lossView.startDate}" required="true" requiredMessageCode="validate_date_not_empty"
				  		onchangeFunction="getDaysValue()"/>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-startDate-dateInOrder">
						<spring:message code="validate_start_before_end"/>
					</div>
				</div>
				<div class="layout-cell cell-thirty-three">
					<a:inputDate name="endDate" labelCode="to" form="${ns}form"
				  		value="${lossView.endDate}" required="true" requiredMessageCode="validate_date_not_empty"
				   		onchangeFunction="getDaysValue()"/>
				</div>
				<div class="layout-cell">
					<label for="${ns}form-days"><spring:message code="days"/>:</label>
					<spring:bind path="lossView.days">
						<input id="${ns}form-days" type="text" name="days" value="<c:out value="${status.displayValue}"></c:out>">
					</spring:bind>
					<form:errors path="days" cssClass="portlet-msg-error" ></form:errors>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-days-number"><spring:message code="validate_digit"/></div>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-days-roundingNumber"><spring:message code="validate_loss_days"/></div>
				</div>
			</div>
		</div>

		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-thirty-three" >
			       	<label for="${ns}form-typeId"><spring:message code="status"/>:</label>
					<select name="statusId" id="${ns}form}-statusId" class="">
						<option value="" selected="selected"><spring:message code="select_please"/></option>
						<c:forEach var="lossStatus" items="${lossStatusList}">
							<option value="${lossStatus.pk}" <c:if test="${lossView.statusId eq lossStatus.pk }">selected="selected"</c:if>><c:out value="${lossStatus.name}"></c:out></option>
						</c:forEach>
					</select>
				</div>

				<div class="layout-cell cell-sixty-six">
					<label for="${ns}form-description"><spring:message code="description"/>:</label>
		    		<input id="${ns}form-description" type="text" name="description" maxlength="4000" value="<c:out value="${lossView.description}" ></c:out>">
		    		<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
			    		<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
							<spring:message code="validate_description_length"/>
						</div>
				</div>
			</div>
		</div>

		<div class="button-div">
		   	<input type="submit" class="save" value="<spring:message code="save"/>" />
		   	<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${getAllHolidays}');return false;" class="cancel" value="<spring:message code="cancel"/>" />
		</div>

	</form:form>
</div>

<script type="text/javascript">

	function getDaysValue(){
		var firstday = document.getElementById('${ns}form-startDate-alt').value;
		var lastday = document.getElementById('${ns}form-endDate-alt').value;
		jQuery.ajax({
			url: '${doGetDaysValue}&startdate='+firstday+'&enddate='+lastday,
			success: function(data){
				document.getElementById('${ns}form-days').value = data;
			}
		});


	}

	var ${ns}confirmation = function(){

		form = document.getElementById("${ns}form");
		var days = form.elements['allHoliday'];

		if(days.value != ""){
			var answer = confirm('<spring:message code="confirm_allow_holiday" arguments="'+days.value+'"/>');
			if (answer){
				var form${ns}form = jQuery('#${ns}form');
				jQuery('#${ns}mainDiv').load('${doSaveLoss}&confirmation=yes&' + form${ns}form.serialize());
			}
		}
	}

	${ns}confirmation();

    /**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"startDate", validator:"required"},
    	                          {id:"startDate", validator:"date"},
    	                          {id:"startDate", validator:"dateInOrder", compareDate:"endDate"},
    	                          {id:"endDate", validator:"date"},
	               				  {id:"endDate", validator:"required"},
	               				  {id:"days", validator:"number"},
	               				  {id:"days", validator:"roundingNumber"},
    	                          {id:"description", validator:"maxLength", maxLength:"4000"}];
   }

    var ${ns}zaForm = jQuery("#${ns}form");
    ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor));
	});*/
</script>

