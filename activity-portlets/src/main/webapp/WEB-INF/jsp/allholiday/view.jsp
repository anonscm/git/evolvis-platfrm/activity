<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:resourceURL var="doEdit" id="doEdit"/>
<portlet:resourceURL var="doRequestDelete" id="doRequestDelete"/>
<portlet:resourceURL var="getHolidaysExport" id="getHolidaysExport"/>
<portlet:actionURL var="doMassUpdate" name="doMassUpdate"/>

<portlet:renderURL var="getAllHolidays">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getAllHolidays}><spring:message code='all_absence'/></a></h1>");
</script>

<c:if test="${currentLoggedInUserRole eq 'Controller'}">
    <div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
        <form:form method="POST" id="${ns}form" name="${ns}form" action="${getHolidaysExport}">

                <h2><spring:message code="all_absence"/></h2>
                <div class="layout-table">
                    <div class="layout-row">
                        <div class="layout-cell cell-thirty-three">
                            <a:filterSelectResource ns="${ns}"
                            onchangeFunction="${ns}filterByResource(this.value)" resourceList="${resourceList}"/>
                        </div>

			   			<a:filterInputDateFromTo ns="${ns}" formId="${ns}form" name="startDate" dataTable="${ns}myDataTable"/>

                        <div class="portlet-msg-error yui-pe-content" id="${ns}form-endDate-validateStartBeforEnd">
                            <spring:message code="validate_start_before_end"/>
                        </div>
                    </div>

                    <div class="layout-row">
                        <div class="layout-cell">
                            <label for="${ns}form-typeId"><spring:message code="type"/>:</label>
                            <select name="typeId" id="${ns}form-typeId" class="default" onchange="${ns}filterByType(this.value)">
                                <option value="" selected="selected"><spring:message code="all"/></option>
                                <c:forEach var="lossType" items="${lossTypeList}">
                                    <option value="${lossType.pk}"><c:out value="${lossType.name}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="layout-cell">
                            <label for="${ns}form-statusId"><spring:message code="status"/>:</label>
                            <select name="statusId" id="${ns}form-statusId" class="default" onchange="${ns}filterByStatus(this.value)">
                                <option value="" selected="selected"><spring:message code="all"/></option>
                                <c:forEach var="lossStatus" items="${lossStatusList}">
                                    <option value="${lossStatus.pk}"><c:out value="${lossStatus.name}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="${ns}form-loss" class=""></div>
                <a:authorize ifAnyGranted="Loss.EDIT_ALL">
                    <h3><spring:message code="bulk_edit_absents" /></h3>
                    <div class="layout-table">
                        <div class="layout-row">
                            <div class="layout-cell cell-twenty-five">
                                <label for="${ns}form-statusId"><spring:message code="status"/>:</label>
                                <select name="statusId2" id="${ns}form-statusId2" class="default" ">
                                    <c:forEach var="lossStatus" items="${lossStatusList}">
                                        <option value="${lossStatus.pk}"><c:out value="${lossStatus.name}"></c:out></option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="layout-cell cell-twenty-five">
                                <label for="${ns}form-submit">&nbsp;</label>
                                <input type="submit" onclick="return ${ns}performMassRebook();" value="<spring:message code='save'/>"/>
                            </div>
                            <div class="layout-cell cell-twenty-five">
                                <label for="${ns}form-cancel">&nbsp;</label>
                                <input type="button" onclick="YAHOO.de.tarent.setOrUnsetAllSelectBoxValues(document.${ns}form.massUpdateSelect, false);" value="<spring:message code='cancel'/>"/>
                            </div>
                        </div>
                    </div>
                </a:authorize>
                <a:authorize ifAnyGranted="Loss.EXPORT_ALL">
                    <div class="button-div">
                        <input type="submit" value="<spring:message code='export_as_excel'/>"/>
                    </div>
                </a:authorize>
           </form:form>
    </div>
</c:if>
 
<script type="text/javascript">

     //custom formatter for massUpdate
    YAHOO.widget.DataTable.Formatter.${ns}formatSelectForMassUpdate = function(elLiner, oRecord, oColumn, oData) {
        if (oRecord.getData("typeId") != 2 ){
            var disableIfStatusIsNotBeantragt = (oRecord.getData("statusId") != 1) ? "DISABLED" : "";
            elLiner.innerHTML = "<input type=\"checkbox\" "+disableIfStatusIsNotBeantragt+" name=\"massUpdateSelect\"  id=\"massUpdateSelect\"  value=\""+oRecord.getData("id")+"\"> ";
        }
    };

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("typeId") != 2 ){
			elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${doEdit}&lossPk="+oData+"');return false;\">&nbsp;</a>";
		}
	};

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${doRequestDelete}&lossPk="+oData+"');return false;\">&nbsp;</a>";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatStatus = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("typeId") == 2 ){
			elLiner.innerHTML = "-";
		}else{
			elLiner.innerHTML = oData;
		}

		var element = elLiner.parentNode.parentNode;
		if (oRecord.getData("statusId") == 2 ){
			YAHOO.util.Dom.addClass(element, "greenRow"); 
        } 
		if (oRecord.getData("statusId") == 3 ){
        	YAHOO.util.Dom.addClass(element, "redRow"); 
       } 
	};

     var ${ns}performMassRebook = function (){
         var performAction = confirm("<spring:message code='confirm_rebook_absences'/>");
         if (performAction){
             document.${ns}form.action='${doMassUpdate}';
             document.${ns}form.submit();
             return true;
         }
         return false;
     };


     //javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		selectedResource :'',
		startdate :'',
		enddate :'',
		typeId : '',
		statusId : ''
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [
                        <a:authorize ifAnyGranted="Loss.EDIT_ALL">
                            {key :"selectForMassUpdate",label :"&nbsp;", formatter:"${ns}formatSelectForMassUpdate", sortable :false},
                        </a:authorize>
                        {key :"resourceName",label :"<spring:message code='resource'/>", sortable :true},
	                	{key :"startDate",label :"<spring:message code='from'/>", formatter:"mdyDate", sortable :true},
	                    {key :"endDate",label :"<spring:message code='to'/>", formatter:"mdyDate", sortable :true},
	                    {key :"typeLabel",label :"<spring:message code='type'/>",sortable :true},
	 					{key :"description",label :"<spring:message code='description'/>",sortable :true},
	 					{key :"statusLabel",label :"<spring:message code='status'/>",formatter:"${ns}formatStatus", sortable :true},
	 					{key :"days",className: "smallColumn",label :"<spring:message code='days'/>", sortable :true},
	 					{key :"answers",label :"<spring:message code='answers'/>", formatter:"simpleFormat"}
	 					<a:authorize ifAnyGranted="Loss.EDIT_ALL">
	 						,{key :"id",label :"&nbsp;", className: "editColumn", formatter:"${ns}formatEdit", sortable :false}
	 					</a:authorize>
	 					<a:authorize ifAnyGranted="Loss.DELETE_ALL">	
	 						,{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
						</a:authorize>
						];

	//datasource column definitions
	var ${ns}dsFields = [{key :"resourceName"},
	            	{key :"id",parser :"number"},
	                {key :"startDate",parser :"date"},
	                {key :"endDate",parser :"date"},
					{key :"typeLabel"},
					{key :"typeId",parser :"number"},
					{key :"description"},
					{key :"statusLabel"},		
					{key :"statusId",parser :"number"},			
					{key :"days",parser :"number"},
					{key :"answers"}];

	var ${ns}columnStat = { status: false, displayMsg: ''};
	
	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-loss","${table}", ${ns}dsFields, ${ns}myColumnDefs, 
			YAHOO.de.tarent.${ns}filter, ${ns}columnStat, "resourceName", "asc", ${config.pageItems}, ${config.offset}, "${config.sortColumn}", "${config.sortOrder}");

	var ${ns}filterByResource = function(resource){
		YAHOO.de.tarent.${ns}filter.selectedResource = resource;
		${ns}myDataTable.reloadData();
	};

	var ${ns}filterByType = function(typeId){
		YAHOO.de.tarent.${ns}filter.typeId = typeId;
		status = document.getElementById("${ns}form-statusId");
		if(typeId==2){
			status.disabled=true;
		}else{
			status.disabled=false;
		}
		${ns}myDataTable.reloadData();
	};

	var ${ns}filterByStatus = function(statsuId){
		YAHOO.de.tarent.${ns}filter.statusId = statsuId;
		${ns}myDataTable.reloadData();
	};
</script>
		

