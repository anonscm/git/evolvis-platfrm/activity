<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div id="${ns}mainDiv" class="mainDiv layout-table">

	<h2><spring:message code="my_hours_overview"/></h2>
	
	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-fifty">
				<span class="info left"><spring:message code="my_hours_this_week"/>:</span> 
				<span class="value right"><c:out value="${hoursThisWeek}"></c:out> </span>
			</div>
	
			<div class="layout-cell cell-fifty">
				<span class="info left"><spring:message code="hours_total" />:</span>
				<span class="value right"><c:out value="${hourscomplete}"></c:out></span> 
			</div>	
		</div>
		
		<div class="layout-row">	
			<div class="layout-cell cell-fifty">
				<span class="info left"><spring:message code="my_hours_last_week"/>:</span>
				<span class="value right"><c:out value="${hoursLastWeek}"></c:out></span>
			</div>
				
			<div class="layout-cell cell-fifty">
				<span class="info left"><spring:message code="overtime_hours"/>:</span>
				<span class="value right"> <c:out value="${overtimeHours}"></c:out></span> 
			</div>	
		</div>
	</div>	
</div>
