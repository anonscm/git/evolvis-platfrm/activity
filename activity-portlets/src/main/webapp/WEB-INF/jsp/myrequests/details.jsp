<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="agree" id="agree"/>
<portlet:resourceURL var="disagree" id="disagree" />
<portlet:renderURL var="getMyRequests">
    <portlet:param name="ctx" value="getMyRequests"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyRequests}><spring:message code='my_delete_request'/></a>"+
            "&nbsp;&raquo;&nbsp;"+"<spring:message code='edit_delete_request'/></h1>");
</script>



<div id="${ns}mainDiv" class="mainDiv">
<div class="layout-table">
	<div class="layout-cell cell-seventy">
		<fieldset>
			<legend><spring:message code="send_delete_request"/></legend>
			<div class="layout-table label_details">
				<div class="layout-row">
					<label><spring:message code="data_type"/>:</label>
					<c:out value="${drView.dataType}"></c:out>
				</div>
				<div class="layout-row">
					<label><spring:message code="data_type_id"/>:</label>
					<c:out value="${drView.dataTypeId}"></c:out>
				</div>
				<div class="layout-row">
					<label><spring:message code="description"/>:</label>
					<c:out value="${drView.description}"></c:out>
				</div>
				<div class="layout-row">
					<label><spring:message code="date"/>:</label>
					<fmt:formatDate value="${drView.date}" pattern="${dateformat}"/>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="layout-row">
		<div class="button-div">
			<input type="submit" onclick="jQuery('#${ns}mainDiv').load('${agree}&requestId=${drView.id}');return false;" value="<spring:message code="agree"/>" />
			<input type="submit" onclick="jQuery('#${ns}mainDiv').load('${disagree}&requestId=${drView.id}');return false;" value="<spring:message code="reject"/>" />
			<input type="button" onclick="${ns}localRequestDetailCancelUrl(); return false;" value="<spring:message code="cancel"/>" >
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
    function ${ns}localRequestDetailCancelUrl() {
        this.document.location.href = '${getMyRequests}';
    };
</script>
