<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:resourceURL var="details" id="details"/>


<portlet:renderURL var="getMyRequests">
    <portlet:param name="ctx" value="getMyRequests"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyRequests}><spring:message code='my_delete_request'/></a></h1>");
</script>


<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
<h2><spring:message code="my_delete_request"/></h2>
    <form:form method="POST" id="${ns}form" action="">

 	<div id="${ns}form-requests" class="clearDiv"></div>

    </form:form>
</div>

<script type="text/javascript">

	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails= function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${details}&requestPk="+oData+"');return false;\"><spring:message code='detail'/></a>";
	};

	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",label :"<spring:message code='id'/>",sortable :true},
	                    {key :"dataType", label : "<spring:message code='data_type'/>", sortable :true},
	 					{key :"description",label :"<spring:message code='description'/>", sortable :true}, 
	 					{key :"status",label :"<spring:message code='status'/>", sortable :true},
	 					{key: "controllerName", label: "<spring:message code='controller'/>", sortable: true},
	 					{key :"id",label :"<spring:message code='details'/>", formatter:"${ns}formatDetails", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                     {key :"idTable",parser :"number"},
	                {key :"dataType"},
					{key :"description"},
					{key :"status"},
					{key :"controllerName"}];


	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-requests","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>	

