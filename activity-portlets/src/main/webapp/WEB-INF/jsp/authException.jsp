<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="portlet-msg-error">
	<spring:message code="unauthorized_exception"/>
	<br />
	<spring:message code="${exception.message}" /> 
</div>
