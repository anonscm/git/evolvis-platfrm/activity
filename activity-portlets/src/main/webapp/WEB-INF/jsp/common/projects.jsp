<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<option value="" selected="selected">
	<c:choose>
		<c:when test="${isFilter}">
			<spring:message code="all"/>
		</c:when>
		<c:otherwise>
			<spring:message code="select_please"/>
		</c:otherwise>
	</c:choose>
</option>
<c:forEach var="project" items="${projectList}">
	<option value="${project.pk}"><c:out value="${project.name}"></c:out></option>
</c:forEach>
