<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<option id="${ns}-jobs-firstOption" value="" selected="selected">
	<c:choose>
		<c:when test="${isFilter}">
			<spring:message code="all" />
		</c:when>
		<c:otherwise>
			<spring:message code="select_please" />
		</c:otherwise>
	</c:choose>
</option>
<c:forEach var="job" items="${jobsList}">
	<option value="${job.pk}">
		<c:out value="${job.name}"></c:out>
	</option>
</c:forEach>
<c:choose>
	<c:when test="${fn:length(jobsList)>0}">
		<script type="text/javascript">
			jQuery('#${ns}-jobs-firstOption').parent().removeAttr('disabled');
		</script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript">
			jQuery('#${ns}-jobs-firstOption').parent().attr('disabled',
					'disabled');
		</script>
	</c:otherwise>
</c:choose>