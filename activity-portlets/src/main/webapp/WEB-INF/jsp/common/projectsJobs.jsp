<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="firstId" value="${ns}-select-jobId-first"/>

<option id="${firstId}" value=""><spring:message code="all"/></option>
<c:forEach var="job" items="${jobsList}" varStatus="myStatus">
	<c:choose>
		<c:when test="${myStatus.begin}" >
			<optgroup label="${job.project.name}">
				<option value="${job.pk}" 
						<c:if test="${activityView.jobId eq job.pk }">
							selected="selected" 
						</c:if>
					><c:out value="${job.name}"/>
				</option>
		</c:when>
		<c:otherwise> 
			<c:if test="${!myStatus.last}"> 
				<c:choose>
					<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
						<option value="${job.pk}"
							<c:choose>
								<c:when test="${selectedValue eq job.pk }">
									selected="selected" 
								</c:when>
							</c:choose>
						><c:out value="${job.name}"/></option>	
					</c:when>
					<c:otherwise>
						</optgroup>
						<optgroup label="<c:out value="${job.project.name}"/>">
							<option value="${job.pk}" 
								<c:choose>
									<c:when test="${selectedValue eq job.pk }">
										selected="selected" 
									</c:when>
								</c:choose>
							><c:out value="${job.name}"/></option>
							
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${myStatus.last}" >
					<c:choose>
						<c:when test="${(jobsList[myStatus.index]).project.name == (jobsList[myStatus.index -1]).project.name}" >
								<option value="${job.pk}"
										<c:if test="${selectedValue eq job.pk }">
											selected="selected" 
										</c:if>
								><c:out value="${job.name}"/></option>
							</optgroup>	
						</c:when>
						<c:otherwise>
							</optgroup>
							<optgroup label="${job.project.name}">
								<option value="${job.pk}"
									<c:choose>
										<c:when test="${selectedValue eq job.pk }">
											selected="selected" 
										</c:when>
									</c:choose>
								><c:out value="${job.name}"/></option>
							</optgroup>
						</c:otherwise>
				</c:choose>
			</c:if>
		</c:otherwise>
	</c:choose>
</c:forEach>

<script type="text/javascript">
<c:choose>
	<c:when test="${fn:length(jobsList)>0}">
			jQuery('#${firstId}').parent().removeAttr('disabled');
	</c:when>
	<c:otherwise>
			jQuery('#${firstId}').parent().attr('disabled', 'disabled');
	</c:otherwise>
</c:choose>
</script>