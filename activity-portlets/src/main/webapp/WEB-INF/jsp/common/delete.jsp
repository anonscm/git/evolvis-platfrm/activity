<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="getSavaDeleteRequest" name="doSaveDeleteRequest">
    <portlet:param name="ctx" value="deleteRequest"/>
</portlet:actionURL>
<portlet:renderURL var="getCancelDeleteRequest" />


<script type="text/javascript">
	var breadcrumbValue = "<h1><spring:message code='delete_request'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);

    function ${ns}loadCancelDeleteUrl() {
        this.document.location.href = '${getCancelDeleteRequest}';
    };

</script>

<div id="${ns}mainDiv" class="mainDiv" >
<h2><spring:message code="send_delete_request"/></h2>
    <form:form method="POST" id="${ns}form" action="${getSavaDeleteRequest}">

	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-thirty-three">
		     	<span class="info"><spring:message code="user"/>:</span> 
		     	<c:out value="${deleteRequest.resource.firstname}"></c:out> <c:out value="${deleteRequest.resource.lastname}"></c:out>
			</div>
			<div class="layout-cell cell-thirty-three">
		     	<span class="info"><spring:message code="datatype"/>:</span> 
		     	<c:out value="${deleteRequest.type}"></c:out>
			</div>
			<div class="layout-cell cell-thirty-three">
		     	<span class="info"><spring:message code="id"/>: </span> 
		     	<c:out value="${deleteRequest.id}"></c:out>
		     	<input type="hidden" name="objectId" value="<c:out value="${deleteRequest.id}"></c:out>">
			</div>
		</div>
	</div>
	<div class="total_width">
	   	<label for="description"><spring:message code="description"/>:</label>
	   	<textarea id="description" name="description" value="<c:out value="${deleteRequest.description}"></c:out>"></textarea>
	</div>
	<div class="button-div">
	 		<input type="submit" value="<spring:message code="send"/>" />
	  		<input type="button" value="<spring:message code="cancel"/>" onclick="${ns}loadCancelDeleteUrl(); return false;" />
	</div>
   
    </form:form>    
</div>
