<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<option value="" selected="selected">
	<c:choose>
		<c:when test="${isFilter}">
			<spring:message code="all" />
		</c:when>
		<c:otherwise>
			<spring:message code="select_please" />
		</c:otherwise>
	</c:choose>
</option>
<c:forEach var="resource" items="${resourcesList}">
	<option value="${resource.pk}">
		<c:out value="${resource.lastname}," />
		<c:out value="${resource.firstname}" />
	</option>
</c:forEach>
