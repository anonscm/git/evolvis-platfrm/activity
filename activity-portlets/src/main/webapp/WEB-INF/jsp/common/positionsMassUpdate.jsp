<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<option id="${ns}-positions-mass-update-firstOption" value="" selected="selected">
	<c:choose>
		<c:when test="${isFilter}">
			<spring:message code="all"/>
		</c:when>
		<c:otherwise>
			<spring:message code="select_please"/>
		</c:otherwise>
	</c:choose>
</option>
<c:forEach var="position" items="${positionsListMassUpdate}">
	<option value="${position.pk}" 
		<c:if test="${activityView.positionId eq position.pk }"> selected="selected" </c:if>
	><c:out value="${position.name}"></c:out></option>
</c:forEach>
<c:choose>
	<c:when test="${fn:length(positionsListMassUpdate)>0}">
		<script type="text/javascript">jQuery('#${ns}-positions-mass-update-firstOption').parent().removeAttr('disabled');</script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript">jQuery('#${ns}-positions-mass-update-firstOption').parent().attr('disabled','disabled');</script>
	</c:otherwise>
</c:choose>


