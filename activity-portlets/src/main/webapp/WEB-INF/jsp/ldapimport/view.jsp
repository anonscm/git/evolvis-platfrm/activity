<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:resourceURL var="doLdapimport" id="doLdapimport"/>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<form:form action="${doLdapimport}" method="post" id="${ns}form">
		<div class="layout-table">
			<div class="layout-cell cell-seventy" >
	    		<fieldset>
					<legend><spring:message code="ldap_import"/></legend>
			     	<div id="${ns}form-ldap" class="clearDiv"></div>
			     	
			     	<div class="button-div">
    					<input type="submit" class="save" value="<spring:message code='to_import'/>" /> 
    				</div>	
		    	</fieldset>
	    	</div>
		</div>
	</form:form>
</div>

<script type="text/javascript">
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportCheck = function(elLiner, oRecord, oColumn, oData) {
		
		var resource = JSON.stringify(oData);
		var res = YAHOO.lang.JSON.parse(resource);
		var id = res.id;
		
		elLiner.innerHTML = "<input type='checkbox' name='selectedForImportId' value="+id+" />";
	};

	YAHOO.widget.DataTable.Formatter.${ns}formatImportState = function(elLiner, oRecord, oColumn, oData) {

		var element = elLiner.parentNode.parentNode;
		var children = YAHOO.util.Dom.getChildren(element);
		
		if(oRecord.getData("id") == null) { 
			YAHOO.util.Dom.addClass(element, "greenRow"); 
			elLiner.innerHTML = "<spring:message code='to_import'/>";
        } 
        else 
        { 
        	YAHOO.util.Dom.addClass(element, "redRow"); 
        	elLiner.innerHTML = "<spring:message code='to_update'/>";
       }
	};
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportBranchOffice = function(elLiner, oRecord, oColumn, oData) {
		
		var branchOfficeViewList = JSON.stringify(${branchOffice});		
		var bovl = YAHOO.lang.JSON.parse(branchOfficeViewList);		
		
		var bOffice = "<select id='branchOffice' name='branchOffice'>";
		var selected = "selected='selected'";
		
		for(var i = 0; i <  bovl.length; i++) {
			
			bOffice += "<option value="+bovl[i].branchOfficeId;
			if(bovl[i].branchOfficeId==oData) {
				bOffice += " "+selected;
			}
			bOffice += ">"+bovl[i].branchOfficeName+"</option>";
		}
		
		bOffice += "</select>";	
		elLiner.innerHTML = bOffice;
	}
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportEmployeeType = function(elLiner, oRecord, oColumn, oData) {
		
		var employmentViewList= JSON.stringify(${employment});
		var empl = YAHOO.lang.JSON.parse(employmentViewList);
		
		var emplList = "<select id='employmentType' name='employmentType'>";
		var selected = "selected='selected'";
		
		for(var i = 0; i < empl.length; i++) {
			
			emplList += "<option value="+empl[i].id;
			if(empl[i].id==oData) {
				emplList += " "+selected; 
			}
			emplList += ">"+empl[i].name+"</option>";
		}
		
		emplList += "</select>";
		elLiner.innerHTML = emplList;
	}
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportResourceType = function(elLiner, oRecord, oColumn, oData) {

		var resourceTypeViewList= JSON.stringify(${resourceType});
		var rtv = YAHOO.lang.JSON.parse(resourceTypeViewList);
		
		var rtvl = "<select id='resourceType' name='resourceType'>";
		var selected = "selected='selected'";
		
		for(var i = 0; i < rtv.length; i++) {
			
			rtvl += "<option value="+rtv[i].pk;
			if(rtv[i].pk==oData) {
				rtvl += " "+selected;
			}
			rtvl +=">"+rtv[i].name+"</option>";
		}
		
		rtvl += "</select>";	
		elLiner.innerHTML = rtvl;
	}
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportVacation = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<input type='text' id='holidays' name='holidays' value='"+oData+"' />";
	}
	
	YAHOO.widget.DataTable.Formatter.${ns}formatImportUnusedVacation = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<input type='text' id='remainingDays' name='remainingDays' value="+oData+" />";
	}
 	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"ldapUser",label :" ", formatter:"${ns}formatImportCheck"},
	                    {key :"username",label :"<spring:message code='username'/>",sortable :false},
	                	{key :"firstName",label :"<spring:message code='firstname'/>",sortable :false}, 
	 					{key :"lastName",label :"<spring:message code='lastname'/>",sortable :false},
	 					{key :"branchOfficeId",label :"<spring:message code='branch_office' />", formatter: "${ns}formatImportBranchOffice", sortable :false},
	 					{key :"employmentId", label :"<spring:message code='employee_type'/>", formatter: "${ns}formatImportEmployeeType", sortable: false},				
	 					{key :"resourceTypeId", label : "<spring:message code='resource_type' />", formatter: "${ns}formatImportResourceType", sortable: false},
	 					
	 					{key :"holidays", label : "<spring:message code='holidays' />", formatter: "${ns}formatImportVacation", sortable: false},			
	 					{key :"remainingDays", label : "<spring:message code='unused_holidays' />", formatter: "${ns}formatImportUnusedVacation", sortable: false},
	 					
	 					{key :"id",label :"<spring:message code='status'/>", formatter: "${ns}formatImportState", sortable :false},
	 					{key :"groups",label :"<spring:message code='group'/>", formatter:"{$ns}formatImportGroup", sortable :false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"ldapUser"},
	                {key :"username"},
	                {key :"firstName"},
	                {key :"lastName"},
	                {key :"branchOfficeId", parser: "number"},
	                {key :"employmentId", parser: "number"},
	                {key :"resourceTypeId", parser: "number"},              
	                
	                {key :"holidays"},
	                {key :"remainingDays", parser: "number"},          
	                
	                {key :"id", parser :"number"},
					{key :"groups"}];	

	var ${ns}columnStat = { status: false, displayMsg: ''};	
	
	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-ldap","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat);

	
</script>	

