<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="positions" id="positions" />
<portlet:resourceURL var="doStart" id="doStart"/>


<div id="${ns}mainDiv" class="mainDiv" >
	<form:form method="POST"  action="${doStart}" id="${ns}form">
	<div class="timerHelper">
		<a href="${timerLink}"><spring:message code="help"/></a>
	</div>
	
   	<fieldset>
			<legend><spring:message code="stopwatch"/></legend>
	
			<div class="layout-table">
				<div class="layout-cell cell-fifty">
	           		<input type="hidden" name="jobName" id="theJobName" >
	            	<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job"  
            		selectedValue="${activityView.jobId}" requiredMessageCode="validate_job_not_empty" onChange="${ns}ajax_get_positions(this.value);hideStartButton();"
            		focus="true" required="true"/>
					<input type="hidden" name="jobName" value="<c:out value="${job.name}"></c:out>" />
				</div>
				
				<div class="layout-cell cell-fifty">
			   		<a:selectPosition styleClass="default" name="positionId" labelCode="select_position" selectedValue="${activityView.positionId}" 
			   		requiredMessageCode="validate_position_not_empty" form="${ns}form" positionsList="${positionsList}" required="true" onChange="showStartButton(this.value);"/>
		 		</div>
			 
			 	<div class="button-div" id="${ns}-divStartButton" style="display:none;">
		    		<input type="submit" name="startTimer" class="submit" value="<spring:message code="start"/>" /> 
		    	</div>
		</div>
    </fieldset>
	</form:form> 
</div>


<script type="text/javascript">
	
	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions = function(jobID) {
			var positionsSelectLink = '${positions}&jobId=' + jobID;
			jQuery('#${ns}form-positionId').load(positionsSelectLink);
			return false;		
	};
   
    /**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    		// initialize JS validator if browser does not support 'required'
    		${ns}validatorDescriptor=[{id:"jobId", validator:"required"},
    	             			{id:"positionId", validator: "required"}];

    }

    var ${ns}zaForm = jQuery("#${ns}form"); 
    ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor));
	});*/
    
    function showStartButton(value){
    	if(parseInt(value)>0){
    		document.getElementById('${ns}-divStartButton').style.display='block';
    	} else {
    		document.getElementById('${ns}-divStartButton').style.display='none';
    	}
    }
    function hideStartButton(){
   		document.getElementById('${ns}-divStartButton').style.display='none';
    }
  
</script>



