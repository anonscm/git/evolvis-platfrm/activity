<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doRecord" id="doRecord" />
<portlet:resourceURL var="doContinue" id="doContinue"/>
<portlet:resourceURL var="doPause" id="doPause" />
<portlet:resourceURL var="doReset" id="doReset" />
<div id="${ns}mainDiv" class="mainDiv" >

	<form:form method="POST"  action="" id="${ns}form">
		<input type="hidden" id="statusChrono" name="statusChrono" value="${status}">
		<input type="hidden" id="startDiffChrono" name="startDiffChrono" value="${startDiff}">
		
		<div class="timerHelper">
			<a href="${timerLink}"><spring:message code="help"/></a>
		</div>
		
		<fieldset>
		   <legend>
				<c:choose>
					<c:when test="${status=='running'}">
						<spring:message code="stopwatch_running"/>
					</c:when>
					<c:when test="${status=='paused'}">
						<spring:message code="stopwatch_paused"/>			
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
		    	<span id="${ns}form-chronotime">0:00:00</span>
		    </legend>
		    <c:choose>
				<c:when test="${status=='running'}">
					<input type="submit" onclick="${ns}resetTimeout();jQuery('#${ns}mainDiv').load('${doPause}');return false;" name="pauseTimer" value="<spring:message code="pause"/>"/>   
				</c:when>
				<c:when test="${status=='paused'}">
					<input type="submit" onclick="jQuery('#${ns}mainDiv').load('${doContinue}');return false;" name="continueTimer" value="<spring:message code="continue"/>" />
				</c:when>
				<c:otherwise></c:otherwise>
			</c:choose>
		 	<input type="submit" onclick="${ns}resetTimeout();jQuery('#${ns}mainDiv').load('${doReset}');return false;" name="resetTimer" value="<spring:message code="reset"/>"/>
		    <input type="submit" onclick="jQuery('#${ns}mainDiv').load('${doRecord}');return false;" name="recordActivity" value="<spring:message code="stopwatch_save"/>" />	
		</fieldset>
   
	</form:form> 
	
</div>

<script type="text/javascript">	

	var ${ns}timerID = 0;
	var ${ns}hours = 0;
	var ${ns}minutes = 0;
	var ${ns}seconds = 0;
	
	var ${ns}createChrono = function(){
		var status  = document.getElementById("statusChrono").value;
		var startSec = document.getElementById("startDiffChrono").value;
		
		${ns}hours = (startSec - (startSec % (1000 * 60 * 60))) / (1000 * 60 * 60);
		startSec= startSec % (1000 * 60 * 60);
		${ns}minutes = (startSec - (startSec % (1000 * 60 ))) / (1000 * 60 );
		startSec = startSec % (1000 * 60 );
		${ns}seconds = (startSec - (startSec % 1000)) / 1000;
		
		if(status=="running"){
			${ns}chronoStart();
		}else if(status=="paused"){
			${ns}chronoStop();
		}
	}	

	var ${ns}setChronoValue = function(){
		var min = ${ns}minutes;
		var sec = ${ns}seconds;
		var hr = ${ns}hours;
		if (${ns}minutes < 10){
			min = "0" + ${ns}minutes;
		}
		if (${ns}seconds < 10){
			sec = "0" + ${ns}seconds;
		}
		document.getElementById("${ns}form-chronotime").innerHTML = hr + ":" + min + ":" + sec;
		

	}

	var ${ns}chronoIncrement = function(){
		${ns}seconds +=1;
		if(${ns}seconds == 60){
			${ns}seconds = 0;
			${ns}minutes+=1;
			if(${ns}minutes == 60){
				${ns}minutes=0;
				${ns}hours+=1;
				}
			}
	}
	
	var  ${ns}chrono = function(){
		${ns}chronoIncrement();
		${ns}setChronoValue();
		${ns}timerID = setTimeout("${ns}chrono()", 1000);
	};
	
	var ${ns}chronoStart = function(){
		${ns}chrono();
	};
	
	var ${ns}chronoStop = function(){
		${ns}setChronoValue();
		clearTimeout(${ns}timerID);
	};

	${ns}createChrono();

	
	function ${ns}resetTimeout() {
		clearTimeout(${ns}timerID);
	}

</script>


