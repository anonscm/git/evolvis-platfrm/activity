<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:actionURL var="doDelete" name="doDelete"/>
<portlet:renderURL var="showEdit">
    <portlet:param name="ctx" value="showEdit"/>
</portlet:renderURL>

<portlet:renderURL var="getMyOvertimeTable">
    <portlet:param name="ctx" value="getMyOvertimeTable"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getMyOvertimeTable}><spring:message code='my_overtime'/></a></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >

    <form:form method="POST" id="${ns}form">

        <h2><spring:message code="my_overtime"/></h2>
        <div class="layout-table">
            <div class="layout-row">
                <div class="layout-cell cell-fifty">
                    <a:filterSelectProject  ns="${ns}"
                        projectList="${projectList}" onchangeFunction="${ns}filterByProject(this.value)"/>
                </div>
                <div class="layout-cell cell-fifty">
                    <a:filterSelectOvertimeType  name="type" labelCode="select_type"
                        form="${ns}form" onchangeFunction="${ns}filterByType(this.value)"/>
                </div>
            </div>
        </div>
        <div class="layout-table bottom-margin auto-width">
            <div class="layout-row auto-width">
			    <a:filterInputDateFromTo ns="${ns}" formId="${ns}form" name="startDate" dataTable="${ns}myDataTable"/>
                <div class="layout-cell cell-fifty">
                </div>
            </div>
        </div>

        <div id="${ns}form-overtime" class="clearDiv"></div>

    </form:form>
</div>




<script type="text/javascript">

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		//commented to solve evolvis bug #128
		//if(oRecord.getData("type") == '1'){
			elLiner.innerHTML = "<a onclick=\"return confirm('<spring:message code="confirm_delete" htmlEscape="false"/>');\" href=\"${doDelete}&overtimePk="+oData+"\">&nbsp;</a>";
		//}
	};

    YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${showEdit}&overtimePk="+oData+"');return false;\">&nbsp;</a>";
    };

	YAHOO.widget.DataTable.Formatter.${ns}hoursFormatter = function(elLiner, oRecord, oColumn, oData) {
		if (oRecord.getData("hours")<0){
			elLiner.innerHTML = "<font color='red'>"+oData.toFixed(2)+"</font>";
		}else{
			elLiner.innerHTML = oData.toFixed(2);
		}

		var element = elLiner.parentNode.parentNode;
		var children = YAHOO.util.Dom.getChildren(element);

		if(oRecord.getData("type") == '1') {
			YAHOO.util.Dom.addClass(element, "redRow");
        }
        else {
        	YAHOO.util.Dom.addClass(element, "greenRow");
       }
	};

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		projectId :'',
		typeId :'',
		startdate :'',
		enddate :''
	};

	//datatable column definitions
	var ${ns}myColumnDefs = [
                        {key :"projectName",label :"<spring:message code='project'/>", sortable : true},
	 					{key :"description",label :"<spring:message code='description'/>",sortable : true},
	 					{key :"hours",className: "smallColumn",label :"<spring:message code='hours'/>", formatter:"${ns}hoursFormatter", sortable : true},
	 					{key :"date",className: "smallColumn",label :"<spring:message code='date'/>", formatter:"mdyDate", sortable : true}
                        <a:authorize ifAnyGranted="Overtime.EDIT">
                            ,{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false}
                        </a:authorize>
                        <a:authorize ifAnyGranted="Overtime.DELETE">
                            ,{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
                        </a:authorize>

    ];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"projectId",parser :"number"},
					{key :"projectName"},
					{key :"description"},
					{key :"hours",parser :"number"},
					{key :"date",parser :"date"},
					{key :"type", parser: "number"}];
                    //{key :"resourceName"}];


	var ${ns}columnStat = { status: true, displayMsg: '<spring:message code="total_overtime"/>'};

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-overtime","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat, 
			"date", "desc", ${config.pageItems}, ${config.offset}, "${config.sortColumn}", "${config.sortOrder}");
	
	var ${ns}filterByProject = function(project){
        if( project.toString() != "" ){
			jQuery('#${ns}form-type').attr('disabled','disabled');
			jQuery('#${ns}form-type').val("");
		}else{
			jQuery('#${ns}form-type').removeAttr('disabled');
		}

		YAHOO.de.tarent.${ns}filter.projectId = project;
		${ns}myDataTable.reloadData();
	};

	var ${ns}filterByType = function(type){
		var value = jQuery('#${ns}form-type').val();
		if(value == -1){
			jQuery('#${ns}form-projectId').attr('disabled','disabled');
		}else{
			jQuery('#${ns}form-projectId').removeAttr('disabled');
		}

		YAHOO.de.tarent.${ns}filter.typeId = type;
		${ns}myDataTable.reloadData();
	};

</script>


