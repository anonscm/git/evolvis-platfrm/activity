<%-- Main view for resource allocation. Contains a list of resources and a button --%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getPositionsByJobAndResource" id="getPositionsByJobAndResource"/>
<portlet:resourceURL var="getAllPositions" id="getAllPositions"/>
<portlet:resourceURL var="jobs" id="jobs"/>
<portlet:resourceURL var="table" id="table"/>
<portlet:resourceURL var="doDelete" id="doDelete"/>
<portlet:renderURL var="doAssign">
    <portlet:param name="ctx" value="doAssign"/>
</portlet:renderURL>
<portlet:resourceURL var="listPage" id="doCancel"/>
<portlet:renderURL var="showResourceAllocation">
    <portlet:param name="ctx" value="showResourceAllocation"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${showResourceAllocation}><spring:message code='resource_allocation'/></a></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
<h2><spring:message code="resource_allocation"/></h2>
	<form:form method="POST" id="${ns}form" onsubmit="${ns}handleDoAssign()" action="${doAssign}">
 	<div class="layout-table">
		<div class="layout-row"> 
			<div class="layout-cell cell-thirty-three">
				<a:filterSelectResource ns="${ns}" onchangeFunction="${ns}filterByResource(this.value)" resourceList="${resourceList}" focus="true"/>
			</div>
			<div class="layout-cell cell-thirty-three">
	   		<a:filterSelectProjectJob ns="${ns}" jobsList="${jobsList}" selectedValue="${paramJob.pk}" onchangeFunction="${ns}ajax_get_positions(this.value); ${ns}filterByJob(this.value);"/>
			</div> 				
			<div class="layout-cell cell-thirty-three">
	   		<a:filterSelectPosition ns="${ns}" selectedValue="" onchangeFunction="${ns}filterByPosition(this.value);" positionsList="${positionsList}"/>	
			</div> 				
		</div> 				  		
	</div>
	<div class="button-div">
		<input type="submit" style="width: auto;" value="<spring:message code="assign_resource"/>">
	</div>

	<div id="${ns}form-allocations" class="clearDiv"></div>

	</form:form>
</div>

<script type="text/javascript">
	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions = function(jobID){
        var positionsSelectLink = '${getPositionsByJobAndResource}';

        if(jobID.toString() == ""){
            positionsSelectLink += "&isFilter=false";
            jQuery('#${ns}-select-positionId').load(positionsSelectLink);
        }else{
            positionsSelectLink += '&isFilter=true&jobId=' + jobID;
            jQuery('#${ns}-select-positionId').load(positionsSelectLink);
        }

        return false;
	};

	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
		projectId :'',
		jobId :'',
		selectedPosition :'',
		selectedResource :''
	};

	var ${ns}myColumnDefs = [{key :"id",className: "smallColumn",label :"<spring:message code='id'/>",sortable :true}, 
	                     	{key :"firstName",label :"<spring:message code='firstname'/>",sortable :true}, 
	 	 					{key :"lastName",label :"<spring:message code='lastname'/>",sortable :true}, 
	 	 					//{key :"functions",label :"<spring:message code='role'/>",sortable :false},
	 	 					{key :"activeProjects",label :"<spring:message code='active_projects'/>",formatter:"simpleFormat", sortable :false},
	 	 					{key :"inactiveProjects",label :"<spring:message code='inactive_projects'/>", formatter:"simpleFormat", sortable :false},
	 	 					{key :"plannedProjects",label :"<spring:message code='planned_projects'/>",formatter:"simpleFormat", sortable :false}
	 	 					];
	var ${ns}dsFields = [{key :"id",parser :"number"},
	 					{key :"firstName"},
	 					{key :"lastName"},
	 					//{key :"functions"},
	 					{key :"activeProjects"},
	 					{key :"inactiveProjects"},
	 					{key :"plannedProjects"}];	
					
	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-allocations","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter,
            null, "firstName", "asc", ${(not empty resourceAllocationRowsPerPage) ? resourceAllocationRowsPerPage : 10});

	//position selection
	var ${ns}filterByPosition = function(position){
		YAHOO.de.tarent.${ns}filter.selectedPosition = position;
		${ns}myDataTable.reloadData();
	};
	//job selection
	var ${ns}filterByJob = function(job){
		YAHOO.de.tarent.${ns}filter.jobId = job;
		YAHOO.de.tarent.${ns}filter.selectedPosition = '';
		${ns}myDataTable.reloadData();
	};
	//project selection
	var ${ns}filterByProject = function(project){
		YAHOO.de.tarent.${ns}filter.projectId = project;
		YAHOO.de.tarent.${ns}filter.jobId = '';
		YAHOO.de.tarent.${ns}filter.selectedPosition = '';
		${ns}myDataTable.reloadData();
	};
	var ${ns}filterByResource = function(resource){
		YAHOO.de.tarent.${ns}filter.selectedResource = resource;
		YAHOO.de.tarent.${ns}filter.jobId = '';
		YAHOO.de.tarent.${ns}filter.selectedPosition = '';
		${ns}myDataTable.reloadData();
	};

	//history 
	${ns}History = YAHOO.util.History;
	var ${ns}init = "${listPage}";
	
	var ${ns}portletHistoryFlag;
	
	var ${ns}OnStateChanged = function (state) {
		if(${ns}portletHistoryFlag == true){
			jQuery('#${ns}mainDiv').load(state);
		}
		${ns}portletHistoryFlag = true;
	};
	
	${ns}History.register("${ns}mainDiv", ${ns}init, ${ns}OnStateChanged);
	
	var ${ns}handleDoAssign = function () {
		 ${ns}portletHistoryFlag = false;
		 ${ns}History.navigate("${ns}mainDiv", "${doAssign}");
	};
	
</script>

