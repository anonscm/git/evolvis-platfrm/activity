<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSave" name="doSave"/>
<portlet:resourceURL var="positions" id="getPositionsByJobAndResource"/>
<portlet:resourceURL var="doCancel" id="doCancel"/>

<portlet:renderURL var="showResourceAllocation">
    <portlet:param name="ctx" value="showResourceAllocation"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrubmResourceAllocation="<a href=${showResourceAllocation}><spring:message code='resource_allocation'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrubmResourceAllocation+"&nbsp;&raquo;&nbsp;<spring:message code="assign_resource"/></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv">
<h2><spring:message code="assign_resource"/></h2>
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="posResourceMappingView" onsubmit="return ${ns}validateForm();" >
		<form:hidden path="id"/>

			<c:if test="${exist eq 'exist'}">
				<p class="portlet-msg-error"> <spring:message code="validate_duplicate_position_assignment"/></p>
	 		</c:if>
			<div class="layout-table">
				<div class="layout-row">
					<c:if test="${action eq 'edit'}">
						<form:hidden path="resourceId"/>
					</c:if>
					<c:choose>
						<c:when test="${posResourceMappingView.jobId != null && action != 'add'}">
							<form:hidden path="jobId"/>
							<form:hidden path="positionId"/>
							<div class="layout-cell cell-fifty">
								<label for="jobName"><spring:message code="job"/>:</label>
								<input type="text" readonly="readonly" name="jobName" id="jobName" value="<c:out value="${posResourceMappingView.jobName}"></c:out>">
							</div>
							<div class="layout-cell cell-fifty">
								<label for="positionName"><spring:message code="position"/>:</label>
								<input type="text" readonly="readonly" name="positionName" id="positionName" value="<c:out value="${posResourceMappingView.positionName}"></c:out>">
							</div>
						</c:when>
						<c:otherwise>
							<div class="layout-cell cell-fifty">
				           		<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job" styleClass="large" 
				           		selectedValue="${posResourceMappingView.jobId}" requiredMessageCode="validate_job_not_empty" onChange="${ns}ajax_get_positions(this.value);"
				           		focus="true" required="true"/>
							</div>
							<div class="layout-cell cell-fifty">
						   		<a:selectPosition styleClass="large" name="positionId" labelCode="select_position" selectedValue="${posResourceMappingView.positionId}" 
						   			requiredMessageCode="validate_position_not_empty" form="${ns}form" positionsList="${positionsList}" required="true"/>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>


			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
						<c:choose>
							<c:when test="${action eq 'edit'}">
								<label for="resourceName"><spring:message code="resource"/>:</label>
								<input type="text" readonly="readonly" name="resourceName" id="resourceName" value="<c:out value="${posResourceMappingView.resourceName}"></c:out>">
							</c:when>
							<c:otherwise>
								<a:selectResource  name="resourceId" labelCode="resource" required="true" selectedValue="${resource.pk}" 
									requiredMessageCode="validate_resource_not_empty" form="${ns}form" resourceList="${resourceList}"/>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="layout-cell cell-fifty">
							<label for="statusId"><spring:message code="status"/>*:</label>
						    <select name="statusId" id="statusId">
							  	<option value=""><spring:message code="select_please"/></option>
							   	<c:forEach var="status" items="${statusList}" >
									<option value="${status.pk}" 
										<c:choose>
											<c:when test="${posResourceMappingView.statusId eq status.pk}">
												selected="selected" 
											</c:when>
										</c:choose>
									><c:out value="${status.name}"></c:out></option>
								</c:forEach>
							</select>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-statusId-required"><spring:message code="validate_status_not_empty"/></div>
							<form:errors path="statusId" cssClass="portlet-msg-error" ></form:errors>
						</div>
				</div>
			</div>
			<div class="layout-table auto-width">
				<div class="layout-row auto-width">
					<div class="layout-cell cell-twenty-five">
						<a:inputDate styleClass="date" name="startDate" labelCode="start_date" form="${ns}form" value="${posResourceMappingView.startDate}" required=""/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-startDate-dateInOrder">
							<spring:message code="validate_start_before_end"/>
						</div>
					</div>
					<div class="layout-cell cell-twenty-five">
						<a:inputDate styleClass="date" name="endDate" labelCode="end_date" form="${ns}form" value="${posResourceMappingView.endDate}" required=""/>
					</div>
					<div class="layout-cell">
						<label for="percent"><spring:message code="resource_capacity_utilisation"/>:</label>
						<input type="number" class="small" name="percent" id="percent" value="${posResourceMappingView.percent}"/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-percent-positiveInteger"><spring:message code="validate_perzent_positive"/></div>
						<form:errors path="percent" cssClass="portlet-msg-error" ></form:errors>
					</div>					
				</div>
			</div>

	    <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	    	<input type="submit"  onclick="jQuery('#${ns}mainDiv').load('${doCancel}');return false;" class="cancel" value="<spring:message code="cancel"/>" />
	   </div>
	</form:form>
</div>

<script type="text/javascript">



    function ${ns}validateForm(){
        if(document.getElementById('${ns}form-positionId').selectedIndex == 0 ||
                document.getElementById('${ns}form-resourceId').selectedIndex == 0 ||
                document.getElementById('statusId').selectedIndex == 0 ||
                document.getElementById('${ns}form-jobId').selectedIndex == 0){
            alert('Please Fill the form completely.');
            return false;
        }else{
            return true;
        }
    }

	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions = function(jobID){
			//jQuery('#${ns}form-positionId').load(positionsSelectLink);
			
			var positionsSelectLink = '${positions}';

	        if(jobID.toString() == ""){
	            positionsSelectLink += "&isFilter=false";
	            jQuery('#${ns}form-positionId').load(positionsSelectLink);
	        }else{
	            positionsSelectLink += '&isFilter=true&jobId=' + jobID;
	            jQuery('#${ns}form-positionId').load(positionsSelectLink);
	        }

	        return false;
	}
	/**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"positionId", validator:"required"},
    	                     {id:"jobId", validator:"required"},
    	                     {id:"statusId", validator:"required"},
    	                     {id:"resourceId", validator:"required"},
    	                     {id:"percent", validator:"positiveInteger"},
    	                     {id:"startDate", validator:"date"},
    	                     {id:"startDate", validator:"dateInOrder", compareDate:"endDate"},
    	                     {id:"endDate", validator:"date"}
    	                     ];
   }

      
    var ${ns}zaForm = jQuery("#${ns}form"); 
    ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		var isValid = YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor);
		if(isValid ==true){
			${ns}handleDoCancel();
		}
		${ns}zaForm.attr('isFormValid',isValid);
	});*/

    var ${ns}handleDoCancel = function () {
    	${ns}portletHistoryFlag = false;
    	${ns}History.navigate("${ns}mainDiv", "${doCancel}");
	};
	
</script>

