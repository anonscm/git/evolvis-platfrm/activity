<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="edit" id="edit">
	<portlet:param name="costPk" value="${costView.id}"/>
</portlet:resourceURL>
<portlet:renderURL var="doCostCancel">
    <portlet:param name="ctx" value="jobCosts"/>
    <portlet:param name="jobId" value="${costView.jobId}"/>
</portlet:renderURL>

<script type="text/javascript">
	var breadcrumbValue = "<h1><spring:message code='costs'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
    function ${ns}loadUrl(urlToload){
        this.document.location.href = urlToload;
    };
</script>

<div id="${ns}mainDiv" class="mainDiv">
	<h2><spring:message code="cost"/>: <c:out value="${costView.name}"/></h2>
	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell details cell-twenty">
				<span class="info"><spring:message code="date"/>:</span>
			</div>
			<div class="layout-cell details cell-fifty">			
				<c:out value="${costView.date}"></c:out>
			</div>
		</div>
		<div class="layout-row">
			<div class="layout-cell details cell-twenty">
				<span class="info"><spring:message code="created_by"/>:</span>
			</div>
			<div class="layout-cell details cell-fifty">			
				<c:out value="${costView.resourceName}"></c:out>
			</div>		
		</div>
		<div class="layout-row">
			<div class="layout-cell details cell-twenty">
				<span class="info"><spring:message code="id"/>:</span>
			</div>
			<div class="layout-cell details cell-fifty">			
				<c:out value="${costView.id}"></c:out>
			</div>
		</div>
	</div>
	<div class="total_width top-margin bottom-margin">
  		<span class="info"><spring:message code="description"/></span>
		<c:out value="${costView.description}" escapeXml="false"></c:out>
	</div>
	
	<h3><spring:message code="costs_and_expenses"/></h3>
	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell details cell-twenty">
				<span class="info"><spring:message code="type"/>:</span>
			</div>
			<div class="layout-cell details cell-fifty">			
				<c:out value="${costView.costTypeName}"></c:out>
			</div>
		</div>
		<div class="layout-row">
			<div class="layout-cell details cell-twenty">
				<span class="info"><spring:message code="cost"/>:</span>
			</div>
			<div class="layout-cell details cell-fifty">
				<c:out value="${costView.cost}"></c:out>
			</div>
		</div>
	</div>

	<div class="button-div">
		<a:authorize ifAnyGranted="Cost.EDIT_ALL">
			<!--input type="button" onclick="jQuery('#${ns}mainDiv').load('${edit}');return false;" value="<spring:message code="edit"/>" /-->
		</a:authorize>
		<input type="button" onclick="${ns}loadUrl('${doCostCancel}');return false;" value="<spring:message code="cancel"/>" />
	</div>
</div>