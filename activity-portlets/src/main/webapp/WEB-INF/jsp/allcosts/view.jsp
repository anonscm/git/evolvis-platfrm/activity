<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="costTable" id="table" />
<portlet:resourceURL var="edit" id="edit" />
<portlet:resourceURL var="add" id="add" />
<portlet:resourceURL var="jobs" id="jobs" />
<portlet:resourceURL var="details" id="details" />

<script type="text/javascript">
	<c:choose>
		<c:when test="${not empty paramJob}">
			var jobListLink = "";
			<c:if test="${not empty linkToJobPage}">
				jobListLink ="<a href='${linkToJobPage}'><spring:message code='jobs'/></a>&nbsp;&raquo;&nbsp";
			</c:if>
	
			var jobLink = "";
			<c:if test="${not empty linkToJobPage}">
				jobLink ="<a href='${linkToCurrentJobPage}'><c:out value='${paramJob.name}'></c:out></a>&nbsp;&raquo;&nbsp;";
			</c:if>
		
			var breadcrumbValue = "<h1>"+jobListLink+jobLink+"<spring:message code='costs'/></h1>"
			YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		</c:when>
		<c:otherwise>
			var breadcrumbValue = "<h1><spring:message code='costs'/></h1>"
			YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		</c:otherwise>
	</c:choose>
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2>
		<spring:message code="costs" />
	</h2>
	<form:form method="POST" id="${ns}form" action="${add}">

		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-fifty">
					<a:filterSelectProject ns="${ns}" projectList="${projectList}"
						onchangeFunction="${ns}ajax_get_jobs(this.value);${ns}filterByProject(this.value)" />
				</div>
				<div class="layout-cell cell-fifty">
					<a:filterSelectProjectJob ns="${ns}" jobsList="${jobList}" selectedValue="${paramJob.pk}"
						onchangeFunction="${ns}filterByJob(this.value);" />
				</div>
			</div>
		</div>

		<div id="${ns}form-costs" class="clearDiv"></div>

	</form:form>
</div>

<script type="text/javascript">
	//ajax call for selecting jobs options for the given project
	var ${ns}ajax_get_jobs = function(projectId){
			var jobsSelectLink = '${jobs}&isFilter=true&projectIdId=' + projectId;
			jQuery('#${ns}form-jobId').load(jobsSelectLink);
			return false;		
	}
	
	//project selection
	var ${ns}filterByProject = function(project){
		YAHOO.de.tarent.${ns}filter.projectId = project;
		YAHOO.de.tarent.${ns}filter.jobId = '';
		var breadcrumbValue = "<h1><spring:message code='costs'/></h1>"
		YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		${ns}myDataTable.reloadData();
	}
	//job selection
	var ${ns}filterByJob = function(job){
		YAHOO.de.tarent.${ns}filter.jobId = job;
		var breadcrumbValue = "<h1><spring:message code='costs'/></h1>"
		YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		${ns}myDataTable.reloadData();
	}
</script>

<%@ include file="/WEB-INF/jsp/allcosts/table.jsp"%>
