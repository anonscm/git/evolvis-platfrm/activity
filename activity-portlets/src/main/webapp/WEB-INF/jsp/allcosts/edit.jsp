<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doCostSave" name="doCostSave"/>
<portlet:renderURL var="doCostCancel">
    <portlet:param name="ctx" value="jobCosts"/>
	<portlet:param name="jobId" value="${costView.jobId}"/>
</portlet:renderURL>
<portlet:resourceURL var="jobs" id="jobs"/>

<script type="text/javascript">
	var breadcrumbValue = "<h1><spring:message code='costs'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>


<div id="${ns}mainDiv" class="mainDiv">
	<form:form method="POST" id="${ns}form" action="${doCostSave}" commandName="costView" novalidate="novalidate">
		<form:hidden path="id"/>
		<div class="layout-table">
			<div class="layout-cell">
				<fieldset>
					<div class="layout-table">
						<div class="layout-cell cell-fifty">
							<a:selectProject name="projectId" form="${ns}form" labelCode="project" projectList="${projectList}" 
								selectedValue="${costView.projectId}" requiredMessageCode="validate_project_not_empty" required="true" onChange="${ns}ajax_get_jobs(this.value);"/>
						</div>
						<div class="layout-cell cell-fifty">
							<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobList}" labelCode="job" selectedValue="${costView.jobId}" requiredMessageCode="validate_job_not_empty" required="true"/>
						</div>
					</div>
					<div class="layout-table">
			    		<div class="layout-cell cell-thirty-three">
			    			<a:inputText name="name" form="${ns}form" maxlength="255" labelCode="name" requiredMessageCode="validate_name_not_empty" required="true" value="${costView.name}"/>
			    		</div>
			    		<div class="layout-cell cell-thirty-three">
			    			<label for="${ns}-select-resourceId"><spring:message code="created_by"/>*:</label>
			    			<form:select id="${ns}-select-resourceId" path="resourceId"  htmlEscape="true">
			    				<form:option value=""><spring:message code="select_please"/></form:option>
                                <c:forEach var="resource" items="${resourceList}" >
                                    <option value="${resource.pk}">
                                        <c:out value="${resource.lastname}, ${resource.firstname}"></c:out>
                                    </option>
                                </c:forEach>
			    			</form:select>
			    			<div class="portlet-msg-error yui-pe-content" id="${ns}-select-resourceId-required">
			    				<spring:message code="validate_resource_not_empty"/>
			    			</div>
			    			<form:errors path="resourceId" cssClass="portlet-msg-error"></form:errors>
			    			<script type="text/javascript">
								if(!("autofocus" in document.createElement("input"))) {
									document.getElementById("${ns}-select-resourceId").focus();
								}
							</script>
			    		</div>
			    		<div class="layout-cell cell-thirty-three">
			    			<a:inputDate styleClass="date" name="date" labelCode="date" form="${ns}form" value="${costView.date}" required="true" requiredMessageCode="validate_date_not_empty"/>
			    		</div>
		    		</div>
					<div class="layout-table">
						<div class="layout-cell">
							<label for="${ns}form-description"><spring:message code="description"/>:</label>
							<textarea id="${ns}form-description" name="description" rows="3" cols="130" maxlength="4000"><c:out value="${costView.description}" /></textarea>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
								<spring:message code="validate_description_length"/>
							</div>
							<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
						</div>
					</div>
			    </fieldset>
			    <fieldset>
					<div class="layout-table">
						<div class="layout-cell cell-thirty-three">
							<label for="${ns}form-costTypeId"><spring:message code="type"/>*:</label>
			    			<form:select id="${ns}form-costTypeId" path="costTypeId">
			    				<form:option value=""><spring:message code="select_please"/></form:option>
			    				<form:options itemLabel="name" itemValue="pk" items="${costTypeList}"/>
			    			</form:select>
			    			<div class="portlet-msg-error yui-pe-content" id="${ns}form-costTypeId-required">
			    				<spring:message code="validate_type_not_empty"/>
			    			</div>
			    			<form:errors path="costTypeId" cssClass="portlet-msg-error"></form:errors>
			    			<script type="text/javascript">
								if(!("autofocus" in document.createElement("input"))) {
									document.getElementById("${ns}form-costTypeId").focus();
								}
							</script>
						</div>
						<div class="layout-cell cell-thirty-three">
							<a:inputText name="cost" form="${ns}form" maxlength="255" labelCode="cost"  required="true" requiredMessageCode="validate_amount_not_empty" value="${costView.cost}"/>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-cost-number">
								<spring:message code="validate_digit"/>
							</div>
						</div>
					</div>
			    </fieldset>
			</div>
		</div>
		<div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
	    	<input type="button" onclick="${ns}loadUrl('${doCostCancel}');return false;" class="save" value="<spring:message code="cancel"/>" />
  	 	</div>
	</form:form>
</div>

<script type="text/javascript">
	//ajax call for selecting jobs options for the given project
	var ${ns}ajax_get_jobs = function(projectId){
			var jobsSelectLink = '${jobs}&isFilter=false&projectIdId=' + projectId;
			jQuery('#${ns}form-jobId').load(jobsSelectLink);
			return false;		
	}

    /**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"projectId", validator:"required"},
		               			{id:"jobId", validator:"required"},
		               			{id:"name", validator:"required"},
		               			{id:"resourceId", validator:"required"},
		               			{id:"date", validator:"required"},
		               			{id:"date", validator:"date"},
		               			{id:"costTypeId", validator:"required"},
		               			{id:"cost", validator:"required"},
		               			{id:"cost", validator:"number"},
		               			{id:"description", validator:"maxLength", maxLength:"4000"}];
   }
    
    var ${ns}zaForm = jQuery("#${ns}form"); 
    ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor));
	});*/
    function ${ns}loadUrl(urlToload){
        this.document.location.href = urlToload;
    }
</script>

