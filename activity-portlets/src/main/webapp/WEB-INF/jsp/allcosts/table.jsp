<script type="text/javascript">
//custom formatter for table edit column
YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
	elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${edit}&costPk="+oData+"');return false;\">&nbsp;</a>";
};

//custom formatter for table details column
YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
	elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${details}&costPk="+oData+"');return false;\">&nbsp;</a>";
}

//javascript filter object
YAHOO.de.tarent.${ns}filter = {
	projectId :'',
	// if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
	jobId : ${(not empty paramJob) ? paramJob.pk : "''"}
};

//datatable column definitions
var ${ns}myColumnDefs = [{key :"idTable",className: "smallColumn",label :'<spring:message code="id"/>',sortable :true},
            		{key :"jobName",label :'<spring:message code="job"/>',sortable :true}, 
 					{key :"name",label :'<spring:message code="name"/>',sortable :true}, 
 					{key :"description",label :'<spring:message code="description"/>',sortable :true},
 					<a:authorize ifAnyGranted="Cost.EDIT_ALL">
 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
 					</a:authorize>
 					{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}];

//datasource column definitions
var ${ns}dsFields = [{key :"id",parser :"number"},
                {key :"idTable",parser :"number"},
                {key :"jobName"},
				{key :"name"},
				{key :"description"}];

//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-costs","${costTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>