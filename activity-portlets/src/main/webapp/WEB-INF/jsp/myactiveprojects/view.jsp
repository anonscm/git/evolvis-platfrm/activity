<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doSaveDetails" id="doSaveDetails"/>
<portlet:renderURL var="myActiveProjects"/>
<portlet:renderURL var="getMyPositions">
    <portlet:param name="ctx" value="getMyPositions"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbValueMyActiveProjects = "<a href=${myActiveProjects}><spring:message code='my_active_projects'/></a>";

    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueMyActiveProjects+"</h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">

    <h2><spring:message code="my_projects"/></h2>
    
	<c:if test="${fn:length(projects)>0}">
		<c:choose>
			<c:when test="${myProject eq 'true'}">
				<c:set var="newMyProject">false</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="newMyProject">true</c:set>
			</c:otherwise>
		</c:choose>
		<a href="#" onclick="jQuery('#${ns}mainDiv').load('${doSaveDetails}&myProject=${newMyProject}');return false;"><span style="font-weight: bolder;"><spring:message code="my_projects"/></span></a>
		<ul>
			<c:if test="${myProject eq 'true'}">
				<c:forEach var="pro" items="${projects}">
					<li>
                        <a href="#" onclick="YAHOO.de.tarent.goToUrl('${linkToProjectDetailsPage}&projectId=${pro.pk}');return false;">
                             <c:out value="${pro.name}"/>
                        </a>
					</li>
				</c:forEach>
			</c:if>
		</ul>
	</c:if>
	<c:if test="${fn:length(allActiveProjects)>0}">
		<c:choose>
			<c:when test="${allProject eq 'true'}">
				<c:set var="newAllProject">false</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="newAllProject">true</c:set>
			</c:otherwise>
		</c:choose>
		<a href="#" onclick="jQuery('#${ns}mainDiv').load('${doSaveDetails}&allProject=${newAllProject}');return false;"><span style="font-weight: bolder;"><spring:message code="active_projects"/></span></a>
		<ul>
			<c:if test="${allProject eq 'true'}">
				<c:forEach var="project" items="${allActiveProjects}">
					<li>
                        <a href="#" onclick="YAHOO.de.tarent.goToUrl('${linkToProjectDetailsPage}&projectId=${project.pk}');return false;">
                                <c:out value="${project.name}"/>
                        </a>
					</li>
				</c:forEach>
			</c:if>
		</ul>
	</c:if>	
	<c:if test="${fn:length(responsibleProjects)>0}">
		<c:choose>
			<c:when test="${responsibleProject eq 'true'}">
				<c:set var="newResponsibleProject">false</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="newResponsibleProject">true</c:set>
			</c:otherwise>
		</c:choose>
		<a href="#" onclick="jQuery('#${ns}mainDiv').load('${doSaveDetails}&responsibleProject=${newResponsibleProject}');return false;"><span style="font-weight: bolder;"><spring:message code="project_management"/></span></a>
		<ul>
			<c:if test="${responsibleProject eq 'true'}">
				<c:forEach var="respro" items="${responsibleProjects}">
					<li>
                        <a href="#" onclick="YAHOO.de.tarent.goToUrl('${linkToProjectDetailsPage}&projectId=${respro.pk}');return false;">
                            <c:out value="${respro.name}"/>
                        </a>
                    </li>
				</c:forEach>
			</c:if>
		</ul>	
	</c:if>

    <h2></h2>
    <h2><spring:message code="my_positions"/></h2>
    <ul>
        <li><a href="${getMyPositions}"><spring:message code="my_positions"/></a></li>
    </ul>

</div>


