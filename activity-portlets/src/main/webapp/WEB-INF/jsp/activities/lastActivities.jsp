<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
	<form:form id="${ns}form">
		<div class="yui-dt">
			<h2><spring:message code="my_last_activities"/></h2>

				<table>
					<tr>
						<th><div class="yui-dt-liner"><spring:message code="job"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="position"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="evolvis_task_id"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="description"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="date"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="hours"/></div></th>
					</tr>
					<c:forEach var="item" items="${myList}" varStatus="loop">
						<c:choose>
							<c:when test="${loop.index%2 eq 0}">
								<tr class="yui-dt-even">
							</c:when>
							<c:otherwise>
								<tr class="yui-dt-odd">
							</c:otherwise>
						</c:choose>
							<td><div class="yui-dt-liner"><c:out value="${item.jobName}"></c:out></div></td>
							<td><div class="yui-dt-liner"><c:out value="${item.positionName}"></c:out></div></td>
							<td><div class="yui-dt-liner"><c:out value="${item.evolvisTaskId}"></c:out></div></td>
							<td><div class="yui-dt-liner"><c:out value="${item.description}"></c:out></div></td>
							<td><div class="yui-dt-liner"><fmt:formatDate value="${item.date}" pattern="${dateformat}"/></div></td>
							<td><div class="yui-dt-liner"><c:out value="${item.hours}"></c:out></div></td>
						</tr>
					</c:forEach>
				</table>
		</div>
	</form:form>
</div>