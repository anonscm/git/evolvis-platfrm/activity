<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getPositionsByJob" id="positions" />
<portlet:resourceURL var="getActivityMyExport" id="getActivityMyExport" />
<portlet:actionURL var="doMassRebook" name="doMassRebook">
    <portlet:param name="ctx" value="activityMassUpdate"/>
</portlet:actionURL>
<portlet:actionURL var="doMassDelete" name="doMassDelete">
    <portlet:param name="ctx" value="activityMassUpdate"/>
</portlet:actionURL>
<portlet:actionURL var="doActivityDelete" name="doActivityDelete" />

<portlet:resourceURL var="getActivityTable" id="getActivityTable"/>

<portlet:renderURL var="getAllActivities">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getAllActivities}><spring:message code='my_activities'/></a></h1>");
</script>

<portlet:renderURL var="redirect" />
<portlet:actionURL var="doActivitySave" name="doActivitySave">
	<portlet:param name="redirect" value="${redirect}"/>
</portlet:actionURL>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2>
		<spring:message code="my_activities" />
	</h2>
	<form:form method="POST" id="${ns}form_add" action="${doActivitySave}" commandName="activityView" novalidate="novalidate">
		<c:choose>
			<c:when test="${not empty activityView.id}">
				<h3><spring:message code="edit_activity"/></h3>
			</c:when>
			<c:otherwise>
				<h3><spring:message code="new_activity"/></h3>
			</c:otherwise>
		</c:choose>
			<c:set var="disableEditActivity" value="false"/>
			<c:if test="${activityView.jobStatusId == 5}">
				<c:set var="disableEditActivity" value="true"/>
			</c:if>


			<form:hidden path="id"/>
			<div class="layout-table">
				<div class="layout-cell cell-fifty">
	            	<a:selectProjectJobs name="jobId" form="${ns}form_add" jobsList="${editActivityJobsList}" labelCode="select_job"
	            		selectedValue="${activityView.jobId}" requiredMessageCode="validate_job_not_empty" onChange="${ns}ajax_get_positions(this.value);"
	            		focus="true" disabled="${disableEditActivity}" required="true"/>
				</div>
				<div class="layout-cell cell-fifty">
				   	<a:selectPosition styleClass="default" name="positionId" labelCode="select_position"
				   		selectedValue="${activityView.positionId}" requiredMessageCode="validate_position_not_empty" form="${ns}form_add" 
				   		positionsList="${editActivityPositionsList}" disabled="${disableEditActivity}" required="true"/>
			 	</div>
			</div>
			<div class="layout-table auto-width">
				<div class="layout-row">
				 	<div class="layout-cell">
						<a:inputEvolvisTaskId styleClass="small" name="evolvisTaskId" labelCode="evolvis_task_id" validationMessageCode="validate_evolvis_task_id"
								form="${ns}form_add" value="${activityView.evolvisTaskId}"/>
					</div>
					<div class="layout-cell">
						<a:inputDate styleClass="small date" name="date" labelCode="date" form="${ns}form_add" value="${activityView.date}" required="true" requiredMessageCode="validate_date_not_empty" disabled="${disabled}"/>
					</div>
					<div class="layout-cell">
			    		<a:inputHours name="hours" labelCode="hours" form="${ns}form_add" value="${(not empty activityView.hours) ? activityView.hours : 0 }"
			    		validationMessageCode="validate_hours_range" required="true"></a:inputHours>
					</div>
				</div>
			</div>
			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell">
						<a:inputText name="description" labelCode="description" form="${ns}form_add" value="${activityView.description}" required="true"
						styleClass="large" maxlength="4000" requiredMessageCode="validate_description_not_empty"/>
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
							<spring:message code="validate_description_length"/>
						</div>
					</div>
				</div>
			</div>

		    <div class="button-div">
		    	<c:if test="${not empty activityView.id}">
		    		<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${getAllActivities}'); return false;" value="<spring:message code='cancel'/>"/>
		    	</c:if>
		    	<input type="submit" class="save" value="<spring:message code="save"/>" />
		   </div>
	</form:form>

	<br />
	<h3><spring:message code="activity_overview"/></h3>

	<form:form method="POST" id="${ns}form_filter" name="${ns}form_filter" action="${getActivityMyExport}">
		<div class="layout-table">
			<div class="layout-row">
				<div class="layout-cell cell-fifty">
					<a:filterSelectProjectJob name="jobId" jobsList="${jobsList}" focus="true" ns="${ns}-overview-activity"
						onchangeFunction="${ns}ajax_get_positions_filtered(this.value); ${ns}filterByJob(this.value);" />
				</div>
				<div class="layout-cell cell-fifty">
					<a:filterSelectPosition name="positionId" positionsList="${positionsList}"
						ns="${ns}-overview-activity" onchangeFunction="${ns}filterByPosition(this.value);" />
				</div>
			</div>
		</div>
		<div class="layout-table bottom-margin">
			<div class="layout-row">
			    <a:filterInputDateFromTo ns="${ns}" formId="${ns}form_filter" name="startDate" dataTable="${ns}myDataTable" />
				<div class="layout-cell"></div>
			</div>
		</div>

		<div id="${ns}form_activities" class=""></div>

        <a:authorize ifAnyGranted="Activity.REBOOK, Activity.DELETE">
            <div class="layout-table auto-width top-margin">
                <div class="layout-row">
                
                	<div class="layout-cell input_width cell_bottom">
                        <a:authorize ifAnyGranted="Activity.DELETE">
		                        <input type="submit" onclick="return ${ns}performMassDelete();" value="<spring:message code='delete_selected'/>"/>
		                </a:authorize>
		                <a:authorize ifAnyGranted="Activity.REBOOK, Activity.DELETE">
		                        <input type="button" onclick="YAHOO.de.tarent.setOrUnsetAllSelectBoxValues(document.${ns}form_filter.massUpdateSelect, false);" value="<spring:message code='remove_selection'/>"/>
		                </a:authorize>
                	</div>
                </div>
			</div>
			<br />
			<h3><spring:message code="rebook"/></h3>
			<div class="layout-table auto-width">            
                <div class="layout-row">
                	 
                    <div class="layout-cell cell_bottom cell-thirty-three">
                        <a:filterSelectProjectJob ns="${ns}-mass-update" name="mass-update" jobsList="${jobsList}" focus="true"
                                                  onchangeFunction="${ns}ajax_get_positions_mass_update(this.value);" />
					</div>
					
					<div class="layout-cell cell_bottom cell-thirty-three">
                        <a:filterSelectPosition ns="${ns}-mass-update" positionsList="${positionsList}" onchangeFunction="" />
 					</div>
 					
					<div class="layout-cell cell_bottom input_width">
		                <a:authorize ifAnyGranted="Activity.REBOOK">
		                        <input type="submit" onclick="return ${ns}performMassRebook()" value="<spring:message code='rebook_selected'/>"/>
		                </a:authorize>
                    </div>
                </div>
            </div>
        </a:authorize>

		<div class="button-div">
			<input type="submit" value="<spring:message code="export_as_excel"/>">
		</div>
	</form:form>
</div>

<script type="text/javascript">
	
	//Leistung eintragen bzw. editieren
	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions = function(jobID) {
		jQuery('#${ns}form_add-positionId').removeAttr('disabled');
			var positionsSelectLink = '${getPositionsByJob}&isFilter=false&jobId=' + jobID;
			jQuery('#${ns}form_add-positionId').load(positionsSelectLink);
			return false;
	};

	//Uebersicht
   	//ajax call for selecting positions options for the given jobId
	var ${ns}ajax_get_positions_filtered = function(jobID){
		jQuery('#${ns}-overview-activity-select-positionId').removeAttr('disabled');
		var positionsSelectLink = '${getPositionsByJob}&isFilter=true&jobId=' + jobID;
		jQuery('#${ns}-overview-activity-select-positionId').load(positionsSelectLink);
		return false;
	};

    var ${ns}ajax_get_positions_mass_update = function(jobID){
        jQuery('#${ns}-mass-update-select-positionId').removeAttr('disabled');
        var positionsSelectLink = '${getPositionsByJob}&isFilter=true&jobId=' + jobID;
        jQuery('#${ns}-mass-update-select-positionId').load(positionsSelectLink);
        return false;
    };

	//position selection
	var ${ns}filterByPosition = function(position){
		YAHOO.de.tarent.${ns}filter.selectedPosition = position;
		${ns}myDataTable.reloadData();
	};
	
	//job selection
	var ${ns}filterByJob = function(job){
		YAHOO.de.tarent.${ns}filter.jobId = job;
		YAHOO.de.tarent.${ns}filter.selectedPosition = '';
		${ns}myDataTable.reloadData();
	};

	// delete confirmation dialog
	var ${ns}deleteConfirmation = function (oData) {
		var answer = confirm("<spring:message code='confirm_delete' htmlEscape='false' />");
		if (answer){
			jQuery.ajax({
				url: '${doDelete}&activityPk='+oData,
				success: function(){
				     ${ns}myDataTable.reloadData();
				}
			});
		}
	};

    var ${ns}performMassDelete = function (){
        var answer = ${ns}warningBeforeDeletion("<spring:message code='confirm_delete_activities' htmlEscape='false'/>");
        if (answer){
            document.${ns}form_filter.action='${doMassDelete}';
            document.${ns}form_filter.submit();
            return true;
        }
        return false;
    };

   var ${ns}performMassRebook = function (){
       var jobSelectBox = document.getElementById("${ns}-mass-update-select-jobId");
       var positionSelectBox = document.getElementById("${ns}-mass-update-select-positionId");
       var jobSelectedValue = jobSelectBox.options[jobSelectBox.selectedIndex].value;
       var positionSelectedValue = positionSelectBox.options[positionSelectBox.selectedIndex].value;
       if( jobSelectedValue == "" || positionSelectedValue == "" || !isAnyCheckBoxSelected(document.${ns}form_filter.massUpdateSelect)){
           alert("<spring:message code='warning.rebook.missing.input' htmlEscape='false'/>");
           return false;
       }

       var performAction = confirm("<spring:message code='confirm_rebook_activities' htmlEscape='false'/>");
       if (performAction){
           document.${ns}form_filter.action='${doMassRebook}&jobForMassUpdate='+jobSelectedValue+'&positionForMassUpate='+positionSelectedValue;
           document.${ns}form_filter.submit();
           return true;
       }
       return false;
   };

    var isAnyCheckBoxSelected = function(selectBoxToCheck){
        atLeastOneCheckBoxIsSelected = false;
        if( selectBoxToCheck != null) {
            for (var i = 0; i < selectBoxToCheck.length; i++)
                if(selectBoxToCheck[i].checked == true ){
                    atLeastOneCheckBoxIsSelected = true;
                    break;
                }
        }
        return atLeastOneCheckBoxIsSelected;
    };

</script>


<script type="text/javascript">
    //custom formatter for massUpdate
    YAHOO.widget.DataTable.Formatter.${ns}formatSelectForMassUpdate = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<input type=\"checkbox\" name=\"massUpdateSelect\"  id=\"massUpdateSelect\"  value=\""+oRecord.getData("id")+"\"> ";
    };
    //custom formatter for table delete column
    YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<a onclick=\"return confirm('<spring:message code="confirm_delete_activity" htmlEscape="false"/>');\" href=\"${doActivityDelete}&activityPk="+oData+"\">&nbsp;</a>";
    };
    YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
        if(oRecord.getData("jobStatusId") != 5 && oRecord.getData("positionStatusId") != 1 && oRecord.getData("positionStatusId") != 3){
            elLiner.innerHTML = "<a href=\"${redirect}&activityPk="+oData+"\">&nbsp;</a>";
        }
    };

    YAHOO.widget.DataTable.Formatter.${ns}hoursFormatter = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = oData.toFixed(2);
	};

    function ${ns}warningBeforeDeletion (warnMessage){
        return confirm(warnMessage);
    };

    //javascript filter object
    YAHOO.de.tarent.${ns}filter = {
        selectedResource : '',
        // if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
        jobId : ${(not empty paramJob) ? paramJob.pk : "''"},
        selectedPosition : '',
        startdate : '',
        enddate : ''
    };

    var ${ns}columnStat = {
        status: true,
        displayMsg: '<spring:message code="activity_hours_total"/>',
    };

    /** Define datatable columns meta data. */
    var ${ns}myColumnDefs = [
        <a:authorize ifAnyGranted="Activity.REBOOK, Activity.DELETE">
        {key :"selectForMassUpdate",label :"&nbsp;", formatter:"${ns}formatSelectForMassUpdate", sortable :false},
        </a:authorize>
        {key :"resourceName",label :'<spring:message code="resource"/>',sortable :true},
        {key :"jobName",label :'<spring:message code="job"/>',sortable :true},
        {key :"positionName",label :'<spring:message code="position"/>',sortable :true},
        {key :"description",label :'<spring:message code="description"/>',sortable :true},
        {key :"hours",className: "smallColumn",label :'<spring:message code="hours"/>',formatter:"${ns}hoursFormatter",sortable :true},
        {key :"date",label :'<spring:message code="date"/>', formatter:"mdyDate", sortable :true},
    /** if we come from the Controlling -> ActivitesView, we can not change activity entries, therefore hide column, until feature is implemented **/
        <c:if test="${not hideEditActivity}">
        <a:authorize ifAnyGranted="Activity.REBOOK, Activity.REBOOK_ALL">
        {key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
        </a:authorize>
        </c:if>
        <a:authorize ifAnyGranted="Activity.DELETE, Activity.DELETE_ALL">
        {key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
        </a:authorize>
    ];

    /** Define datatable columns datasource. */
    var ${ns}dsFields = [{key :"id",parser :"number"},
        {key :"resourceId",parser :"number"},
        {key :"resourceName"},
        {key :"jobId",parser :"number"},
        {key :"jobName"},
        {key :"positionId",parser :"number"},
        {key :"positionName"},
        {key :"description"},
        {key :"hours",parser :"number"},
        {key :"jobStatusId",parser :"number"},
        {key :"date",parser :"date"},
        {key :"positionStatusId",parser :"number"}];

    //the datatable
    ${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form_activities","${getActivityTable}",
                            ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat, 
                            "date", "desc", ${config.pageItems}, ${config.offset}, "${config.sortColumn}", 
                            "${config.sortOrder}");
</script>
