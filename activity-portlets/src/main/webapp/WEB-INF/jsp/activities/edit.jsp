<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getPositionsByJob" id="getPositionsByJob"/>
<portlet:renderURL var="getActivityCancel" />
<portlet:renderURL var="redirect" />
<portlet:actionURL var="doActivitySave" name="doActivitySave">
	<portlet:param name="ctx" value="activityEdit"/>
	<portlet:param name="activityPk" value="${activityView.id}"/>
	<portlet:param name="redirect" value="${redirect}"/>
</portlet:actionURL>

<portlet:renderURL var="getAllActivities">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>
<portlet:renderURL var="activityEdit">
    <portlet:param name="ctx" value="activityEdit" />
    <portlet:param name="activityPk" value="${activityView.id}"/> 
</portlet:renderURL>

<script type="text/javascript">
	var breadcrumbAllActivities = "<a href=${getAllActivities}><spring:message code='all_activities'/></a>";
    var breadcrumbEditActivity = "<a href=${activityEdit}><spring:message code='edit_activity'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbAllActivities+"&nbsp;&raquo;&nbsp;"+breadcrumbEditActivity+"</h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv">
<h2><spring:message code="edit_activity"/></h2>
	<form:form method="POST" id="${ns}form" action="${doActivitySave}" commandName="activityView" novalidate="novalidate">
		<form:errors path="" cssClass="portlet-msg-error"/>

			<c:set var="disabled" value="false"/>
			<c:if test="${activityView.jobStatusId == 5}">
				<c:set var="disabled" value="true"/>
			</c:if>

			<form:hidden path="id"/>
			<div class="layout-table auto-width">
				<div class="layout-row auto-width">
					<div class="layout-cell cell-twenty-five">
	            		<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job"
	            		selectedValue="${activityView.jobId}" requiredMessageCode="validate_job_not_empty" onChange="${ns}ajax_get_positions(this.value);"
	            		focus="true" disabled="${disabled}" required="true"/>
					</div>
					<div class="layout-cell cell-twenty-five">
				   	<a:selectPosition styleClass="default" name="positionId" labelCode="select_position" selectedValue="${activityView.positionId}"
				   		requiredMessageCode="validate_position_not_empty" form="${ns}form" positionsList="${positionsList}"
				   		disabled="${disabled}" required="true"/>
			 		</div>
				</div>
			</div>
			<div class="layout-table top-margin">
				<div class="layout-row">
					<div class="layout-cell cell-thirty-three details">
						<span class="info"><spring:message code="hours"/>:</span>
					<spring:bind path="activityView.hours">
					${status.displayValue}
						<input type="hidden" name="hours" value="${status.displayValue}">
					</spring:bind>
				</div>
					<div class="layout-cell cell-thirty-three details">
						<span class="info"><spring:message code="date"/>:</span>
						<fmt:formatDate value="${activityView.date}" pattern="${dateformat}"/>
						<input type="hidden" name="date" value="${activityView.date}">
					</div>
				 	<div class="layout-cell cell-thirty-three details">
				 		<span class="info" style="width:50%;"><spring:message code="evolvis_task_id"/>:</span>
				 		${activityView.evolvisTaskId}
						<input type="hidden" name="evolvisTaskId" value="${activityView.evolvisTaskId}">
					</div>
				</div>
			</div>
		<div class="total_width">
			<label><spring:message code="description"/>:</label>
			<c:out value="${activityView.description}"></c:out>
			<input type="hidden" name="description" value="${activityView.description}">
		</div>

	    <div class="button-div">
	    	<input type="submit" class="save" value="<spring:message code="save"/>" />
            <input type="button" onclick="YAHOO.de.tarent.goToUrl('${getActivityCancel}'); return false;" class="cancel" value="<spring:message code="cancel"/>"/>
	    	<!--a href="${getActivityCancel}"><spring:message code="cancel"/></a-->
	    </div>
	</form:form>
</div>

<script type="text/javascript">
	//ajax call for selecting positions options for the given jobId
	//TODO: Aktion noch nicht fachlich geklaert bzw. korrekt implementiert
	var ${ns}ajax_get_positions = function(jobID) {
			var positionsSelectLink = '${getPositionsByJob}&jobId=' + jobID;
			jQuery('#${ns}form-positionId').load(positionsSelectLink);
			return false;
	};
</script>
