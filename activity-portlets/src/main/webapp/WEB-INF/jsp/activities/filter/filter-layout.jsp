<%-- resource urls for drop-down lists (CommonsController) --%>
<portlet:resourceURL var="getResourcesByPosition" id="getResourcesByPosition" />
<portlet:resourceURL var="getJobsByProject" id="getJobsByProject" />
<portlet:resourceURL var="getPositionsByJob" id="getPositionsByJob" />

<script type="text/javascript">

    var ${ns}ajax_get_jobs_by_project= function(projectID){
        var jobsSelectLink = '${getJobsByProject}&isFilter=true&projectIdId=' + projectID;
        jQuery('#${ns}-select-jobId').removeAttr('disabled');
        jQuery('#${ns}-select-jobId').load(jobsSelectLink);
        ${ns}ajax_get_positions('');
        return false;
    };

    /** Ajax call to retrieve positions by job id. */
    var ${ns}ajax_get_positions = function(jobID){
        var positionsSelectLink = '${getPositionsByJob}&isFilter=true&jobId=' + jobID;
        jQuery('#${ns}-select-positionId').load(positionsSelectLink);
        return false;
    };

    var ${ns}ajax_get_resources_by_position= function(positionID){
        return false;
    };


    /** Filter activities by project and reload data table. */
    var ${ns}filterByProject = function(project){
        YAHOO.de.tarent.${ns}filter.projectId = project;
        YAHOO.de.tarent.${ns}filter.jobId = '';
        YAHOO.de.tarent.${ns}filter.selectedPosition = '';
        ${ns}myDataTable.reloadData();
    };

    /** Filter activities by job and reload data table. */
    var ${ns}filterByJob = function(job){
        YAHOO.de.tarent.${ns}filter.jobId = job;
        YAHOO.de.tarent.${ns}filter.selectedPosition = '';
        ${ns}myDataTable.reloadData();
    };

    /** Filter activities by position and reload data table. */
    var ${ns}filterByPosition = function(position){
        YAHOO.de.tarent.${ns}filter.selectedPosition = position;
        ${ns}myDataTable.reloadData();
    };

    /** Filter activities by resource and reload data table. */
    var ${ns}filterByResource = function(resource){
        YAHOO.de.tarent.${ns}filter.selectedResource = resource;
        ${ns}myDataTable.reloadData();
    };
    
</script>
<div class="layout-table">
    <div class="layout-row">
        <div class="layout-cell cell-twenty-five">
            <a:filterSelectProject ns="${ns}" name="projectId" projectList="${projectList}"
                                   onchangeFunction="${ns}ajax_get_jobs_by_project(this.value);${ns}filterByProject(this.value);" />
        </div>
        <div class="layout-cell cell-twenty-five">
            <a:filterSelectProjectJob ns="${ns}" jobsList="${jobsList}" selectedValue="${paramJob.pk}"
                                      onchangeFunction="${ns}ajax_get_positions(this.value); ${ns}filterByJob(this.value);" />
        </div>
        <div class="layout-cell cell-twenty-five">
            <a:filterSelectPosition ns="${ns}" name="positionId" positionsList="${positionsList}"
                                    onchangeFunction="${ns}ajax_get_resources_by_position(this.value); ${ns}filterByPosition(this.value);" />
        </div>
        <div class="layout-cell cell-twenty-five">
            <a:filterSelectResource ns="${ns}" resourceList="${resourcesList}"
                                    onchangeFunction="${ns}filterByResource(this.value);" focus="true" />
        </div>
    </div>
    <div class="layout-row">
        <a:filterInputDateFromTo ns="${ns}" formId="${ns}form" name="startDate" dataTable="${ns}myDataTable" />
        <div class="layout-cell"></div>
    </div>
</div>