<%-- Delete me? (wird diese jsp ueberhaupt verwendet??? --%>

<%-- load resources / table data --%>
<portlet:resourceURL var="getActivityTable" id="getActivityTable"/>

<%-- actions --%>

<%-- open views --%>
<portlet:renderURL var="deleteRequest">
	<portlet:param name="ctx" value="deleteRequest" />
</portlet:renderURL>
<portlet:renderURL var="activityEdit">
	<portlet:param name="ctx" value="activityEdit" />
</portlet:renderURL>

<script type="text/javascript">
    //custom formatter for massUpdate
    YAHOO.widget.DataTable.Formatter.${ns}formatSelectForMassUpdate = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<input type=\"checkbox\" name=\"massUpdateSelect\"  id=\"massUpdateSelect\"  value=\""+oRecord.getData("id")+"\"> ";
    };
    //custom formatter for table delete column
    YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<a href=\"${deleteRequest}&activityPk="+oData+"\">&nbsp;</a>";
    };
    YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
        if(oRecord.getData("jobStatusId") != 5 && oRecord.getData("positionStatusId") != 1 && oRecord.getData("positionStatusId") != 3){
            elLiner.innerHTML = "<a href=\"${activityEdit}&activityPk="+oData+"\">&nbsp;</a>";
        }
    };

YAHOO.widget.DataTable.Formatter.${ns}hoursFormatter = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = oData.toFixed(2);
};

//javascript filter object
YAHOO.de.tarent.${ns}filter = {
	selectedResource :'',
	// if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
	jobId : ${(not empty paramJob) ? paramJob.pk : "''"},
	selectedPosition :'',
	startdate :'',
	enddate :''
};

var ${ns}columnStat = {
		status: true,
		displayMsg: '<spring:message code="activity_hours_total"/>',
};

/** Define datatable columns meta data. */
var ${ns}myColumnDefs = [
                    <a:authorize ifAnyGranted="Activity.EDIT">
                        {key :"selectForMassUpdate",label :"&nbsp;", formatter:"${ns}formatSelectForMassUpdate", sortable :false},
                    </a:authorize>
            		{key :"jobName",label :'<spring:message code="job"/>',sortable :true},
 					{key :"positionName",label :'<spring:message code="position"/>',sortable :true},
 					{key :"resourceName",label :'<spring:message code="resource"/>',sortable :true},
 					{key :"hours",className: "smallColumn",label :'<spring:message code="hours"/>',formatter:"${ns}hoursFormatter",sortable :true},
 					{key :"date",label :'<spring:message code="date"/>', formatter:"mdyDate", sortable :true},
 					/** if we come from the Controlling -> ActivitesView, we can not change activity entries, therefore hide column, until feature is implemented **/
 					<c:if test="${not hideEditActivity}">
 						<a:authorize ifAnyGranted="Activity.REBOOK, Activity.REBOOK_ALL">
 							{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
 						</a:authorize>
 					</c:if>
 					<a:authorize ifAnyGranted="Activity.DELETE, Activity.DELETE_ALL">
 						{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
 					</a:authorize>
 					];

/** Define datatable columns datasource. */
var ${ns}dsFields = [{key :"id",parser :"number"},
                {key :"resourceId",parser :"number"},
				{key :"jobId",parser :"number"},
				{key :"jobName"},
				{key :"positionId",parser :"number"},
				{key :"positionName"},
				{key :"resourceName"},
				{key :"hours",parser :"number"},
				{key :"jobStatusId",parser :"number"},
				{key :"date",parser :"date"},
				{key :"positionStatusId",parser :"number"}];

//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-activities","${getActivityTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat, "id", "desc");
</script>
