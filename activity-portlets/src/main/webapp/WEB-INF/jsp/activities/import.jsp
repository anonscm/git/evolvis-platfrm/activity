<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getPositions" id="getPositions" />
<portlet:resourceURL var="getResources" id="getResources" />
<portlet:actionURL var="doActivityImport" name="doActivityImport" />

<div id="${ns}mainDiv" class="mainDiv">
    <a:authorize ifAnyGranted="Activity.IMPORT_ALL">

	<form:form method="POST" action="${doActivityImport}" id="${ns}form" commandName="activityImportView"
		enctype="multipart/form-data" novalidate="novalidate">
       <h3><spring:message code="import_heading" /></h3>
		<fieldset>
            <c:if test="${lines > 0}">
                <c:choose>
                    <c:when test="${noErrors eq 'noErrors'}">
                        <label style="color: green;"><spring:message code="import_success" /></label>
                    </c:when>
                    <c:otherwise>
                        <label style="color: red;"><spring:message code="import_failed" /></label>
                    </c:otherwise>
                </c:choose>
                <br/><label><spring:message code="total_lines" />: ${lines}</label>
                <br/><label style="color: green;"><spring:message code="count_import_success" />: ${linesSuccess}</label>
                <br/><label style="color: red;"><spring:message code="count_import_errors" />: ${linesFailure}</label><br/>
            </c:if>
			<c:forEach var="error" items="${errors}">
				<label style="color: red;"><spring:message code="row" /> ${error.row}:</label>
				<c:forEach var="message" items="${error.errorMessages}">
					<li style="color: red;"><spring:message code="${message}" /></li>
				</c:forEach>
			</c:forEach>
			<div class="layout-table">
				<div class="layout-cell cell-thirty-three">
					<a:selectProjectJobs name="jobId" form="${ns}form" jobsList="${jobsList}" labelCode="select_job"
						selectedValue="${activityImportView.jobId}" requiredMessageCode="validate_job_not_empty"
						onChange="${ns}ajax_get_positions(this.value);" focus="true" required="true" />
				</div>
				<div class="layout-cell cell-thirty-three">
					<a:selectPosition styleClass="default" name="positionId" labelCode="select_position"
						selectedValue="${activityImportView.positionId}" onChange="${ns}ajax_get_resources(this.value);"
						requiredMessageCode="validate_position_not_empty" form="${ns}form" positionsList="${positionsList}"
						required="true" />
				</div>
				<div class="layout-cell cell-thirty-three">
					<a:selectResource name="resourceId" labelCode="select_resource" selectedValue="${activityImportView.resourceId}"
						requiredMessageCode="validate_resource_not_empty" form="${ns}form" resourceList="${resourceList}" />
				</div>
			</div>
			<br>
			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell">
						<input type="file" name="file" style="width: 100%;" title="label.button_choose_file">
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-file-required">
							<spring:message code="validate_import_file_not_empty" />
						</div>
						<form:errors path="file" cssClass="portlet-msg-error"></form:errors>
					</div>
				</div>
			</div>
		</fieldset>
        <div class="button-div">
            <input type="submit" onclick="return YAHOO.de.tarent.validate('${ns}form', ${ns}validatorDescriptor)" class="save"
                value="<spring:message code="to_import"/>" />
        </div>

    </form:form>
    </a:authorize>

</div>

<script type="text/javascript">
//ajax call for selecting positions options for the given jobId
var ${ns}ajax_get_positions = function(jobID) {
		var positionsSelectLink = '${getPositions}&jobId=' + jobID;
		${ns}ajax_get_resources('');
		jQuery('#${ns}form-positionId').load(positionsSelectLink);
		return false;
};

var ${ns}ajax_get_resources = function(positionID) {
	var resourcesSelectLink = '${getResources}&selectedPositionId=' + positionID;
	jQuery('#${ns}form-resourceId').load(resourcesSelectLink);
	return false;
};

</script>
