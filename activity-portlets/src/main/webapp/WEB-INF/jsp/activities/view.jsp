<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%-- resource urls for ajax calls (ActivityListController) --%>
<portlet:resourceURL var="getActivityAllExport" id="getActivityAllExport" />

<portlet:resourceURL var="getActivityTable" id="getActivityTable"/>

<portlet:renderURL var="deleteRequest">
    <portlet:param name="ctx" value="deleteRequest" />
</portlet:renderURL>

<portlet:actionURL var="doMassRebook" name="doMassRebook">
    <portlet:param name="ctx" value="activityMassUpdate"/>
</portlet:actionURL>

<portlet:renderURL var="getAllActivities">
    <portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>

<portlet:resourceURL var="getPositionsByJobForMassUpdate" id="getPositionsByJobForMassUpdate" />

<script type="text/javascript">
    YAHOO.de.tarent.createBreadcrumb("<h1><a href=${getAllActivities}><spring:message code='all_activities'/></a></h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2>
		<spring:message code="all_activities" />
	</h2>
	<form:form method="POST" id="${ns}form" name="${ns}form" action="${getActivityAllExport}">

        <%@ include file="/WEB-INF/jsp/activities/filter/filter-layout.jsp"%>

		<div id="${ns}form-activities" class="clearDiv top-margin"></div>

        <a:authorize ifAnyGranted="Activity.REBOOK_ALL">
        <h3><spring:message code="bulk_edit_activities" /></h3>
            <div class="layout-table">
                <div class="layout-row">
                    <div class="layout-cell cell-twenty-five">
                        <a:filterSelectProject ns="${ns}-mass-update" projectList="${projectList}" focus="true"
                                                  onchangeFunction="${ns}ajax_get_jobs_for_massupdate(this.value);" />
                    </div>
                    <div class="layout-cell cell-twenty-five">
                        <a:filterSelectProjectJob ns="${ns}-mass-update" jobsList="${jobsList}" focus="true"
                                                  onchangeFunction="${ns}ajax_get_positions_for_massupdate(this.value);" />
                    </div>
                    <div class="layout-cell cell-twenty-five">
                        <a:filterSelectPosition name="positionId-mass-update" ns="${ns}-mass-update" positionsList="${positionsList}" onchangeFunction="" />
                    </div>
                    <div class="layout-cell cell-twenty-five cell_bottom">
                    	<input type="submit" onclick="return ${ns}performMassRebook()" value="<spring:message code='rebook'/>"/>
                    </div>
                </div>
            </div>
            <div class="button-div">             
                <input type="button" onclick="YAHOO.de.tarent.setOrUnsetAllSelectBoxValues(document.${ns}form.massUpdateSelect, false);"  value="<spring:message code='remove_selection'/>"/>
            </div>
        </a:authorize>

        <a:authorize ifAnyGranted="Activity.EXPORT_ALL">
            <div class="button-div">
                <%-- better disable button until filter is selected because without filter there are
                     to many activities (more then 66000)
                --%>
                    <input type="submit" id="${ns}-export" value="<spring:message code="export_as_excel"/>">
            </div>
        </a:authorize>

	</form:form>
</div>

<script type="text/javascript">

    var ${ns}ajax_get_jobs_for_massupdate = function(projectId){
        jQuery('#${ns}-mass-update-select-jobId').removeAttr('disabled');
        var jobsSelectLink = '${getJobsByProject}&isFilter=true&projectIdId=' + projectId;
        jQuery('#${ns}-mass-update-select-jobId').load(jobsSelectLink);
        return false;
    };

    var ${ns}ajax_get_positions_for_massupdate = function(jobID){
        jQuery('#${ns}-mass-update-select-positionId').removeAttr('disabled');
        var positionsSelectLink = '${getPositionsByJobForMassUpdate}&isFilter=true&jobId=' + jobID;
        jQuery('#${ns}-mass-update-select-positionId').load(positionsSelectLink);
        return false;
    };

    var ${ns}performMassRebook = function (){
        var projectSelectBox = document.getElementById("${ns}-mass-update-select-projectId");
        var jobSelectBox = document.getElementById("${ns}-mass-update-select-jobId");
        var positionSelectBox = document.getElementById("${ns}-mass-update-select-positionId");
        var projectSelectedValue = projectSelectBox.options[projectSelectBox.selectedIndex].value;
        var jobSelectedValue = jobSelectBox.options[jobSelectBox.selectedIndex].value;
        var positionSelectedValue = positionSelectBox.options[positionSelectBox.selectedIndex].value;

        if( projectSelectedValue == "" || jobSelectedValue == "" || positionSelectedValue == "" ){
            return false;
        }

        var performAction = confirm("<spring:message code='confirm_rebook_activities'/>");
        if (performAction){
            document.${ns}form.action='${doMassRebook}&projectForMassUpdate='+projectSelectedValue+'&jobForMassUpdate='+jobSelectedValue+'&positionForMassUpate='+positionSelectedValue;
            document.${ns}form.submit();
            return true;
        }

        return false;
    };

</script>


<script type="text/javascript">

    //custom formatter for massUpdate
    YAHOO.widget.DataTable.Formatter.${ns}formatSelectForMassUpdate = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<input type=\"checkbox\" name=\"massUpdateSelect\"  id=\"massUpdateSelect\"  value=\""+oRecord.getData("id")+"\"> ";
    };
    //custom formatter for table delete column
    YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<a href=\"${deleteRequest}&activityPk="+oData+"\">&nbsp;</a>";
    };

    //javascript filter object
    YAHOO.de.tarent.${ns}filter = {
        selectedResource :'',
        // if a job-id is tranferred by public render parameter, select the appropriate job to filter positions
        jobId : ${(not empty paramJob) ? paramJob.pk : "''"},
        projectId : ${(not empty paramProject) ? paramProject.pk : "''"},
        selectedPosition :'',
        startdate :'',
        enddate :''
    };

    var ${ns}columnStat = {
        status: true,
        displayMsg: '<spring:message code="activity_hours_total"/>',
    };

    /** Define datatable columns meta data. */
    var ${ns}myColumnDefs = [
        <a:authorize ifAnyGranted="Activity.EDIT">
        {key :"selectForMassUpdate",label :"&nbsp;", formatter:"${ns}formatSelectForMassUpdate", sortable :false},
        </a:authorize>
        {key :"resourceName",label :'<spring:message code="resource"/>',sortable :true},
        {key :"jobName",label :'<spring:message code="job"/>',sortable :true},
        {key :"positionName",label :'<spring:message code="position"/>',sortable :true},
        {key :"description",label :'<spring:message code="description"/>',sortable :true},
        {key :"hours",className: "smallColumn",label :'<spring:message code="hours"/>',sortable :true},
        {key :"date",label :'<spring:message code="date"/>', formatter:"mdyDate", sortable :true},
        <a:authorize ifAnyGranted="Activity.DELETE_ALL">
            {key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
        </a:authorize>
    ];

    /** Define datatable columns datasource. */
    var ${ns}dsFields = [{key :"id",parser :"number"},
        {key :"resourceId",parser :"number"},
        {key :"resourceName"},
        {key :"jobId",parser :"number"},
        {key :"jobName"},
        {key :"positionId",parser :"number"},
        {key :"positionName"},
        {key :"description"},
        {key :"hours",parser :"number"},
        {key :"jobStatusId",parser :"number"},
        {key :"date",parser :"date"},
        {key :"positionStatusId",parser :"number"}];

    //the datatable
    ${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-activities","${getActivityTable}", ${ns}dsFields,
                        ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, ${ns}columnStat, "date", "desc", ${config.pageItems}, ${config.offset}, 
                        "${config.sortColumn}", "${config.sortOrder}");
</script>

