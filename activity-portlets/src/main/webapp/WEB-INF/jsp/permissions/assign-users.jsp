<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="linkToRoles" id="view"/>
<portlet:resourceURL var="usersTable" id="usersTable">
	<portlet:param name="roleId" value="${roleId}"/>
</portlet:resourceURL>
<portlet:actionURL var="saveUsers" name="saveRoleUsers">
	<portlet:param name="roleId" value="${roleId}"/>
</portlet:actionURL>

<portlet:renderURL var="showPermissions">
    <portlet:param name="ctx" value="showPermissions"/>
</portlet:renderURL>

<script type="text/javascript">
    breadcrumblinkToRolesAndPermissions = "<a href='${showPermissions}'><spring:message code='roles_and_permissions'/></a>&nbsp;&raquo;&nbsp;";
    var breadcrumbValue = "<h1>" + breadcrumblinkToRolesAndPermissions + "<c:out value='${roleName}'></c:out></h1>"
    YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="assign_resources"/></h2>
	<div id="${ns}permDiv">
    <form:form method="POST" id="${ns}form-users" action="${saveUsers}">
		<div id="${ns}users-table"></div>	    
		<div class="button-div" >
	    	<input type="submit" value="<spring:message code="save"/>" />
	    	<input type="submit" onclick="jQuery('#${ns}mainDiv').load('${linkToRoles}');return false;" value="<spring:message code="cancel"/>" />
	    </div> 
    </form:form>
    </div>
</div>

<script type="text/javascript">
	var ${ns}preselected = new Array();
	
	function ${ns}updatePermission(id){
		var name = '';
		if(jQuery('#user'+id).is(':checked')){
			name = 'addUserIds';
			${ns}preselected.push({name: 'preSelectedUserIds', value: id});
		} else {
			name = 'removeUserIds';
			for (var count = 0; count < ${ns}preselected.length; count++) {
			    if(${ns}preselected[count].value == id){
			    	${ns}preselected.splice(count, 1);
			    }
			}
			${ns}preselected.push({name: 'preSelectedUserIds', value: id * -1});
		}
		jQuery('<input>').attr({type: 'hidden',id: 'user'+id, name: name, value: id}).appendTo('#${ns}form-users');
		${ns}myDataTable.getDataSource().liveData = '${usersTable}&'+jQuery.param(${ns}preselected);
	}

	YAHOO.widget.DataTable.Formatter.${ns}formatCheckbox = function(elLiner, oRecord, oColumn, oData) {
		var isChecked = JSON.stringify(oRecord.getData("checked"));
		var checked = '';
		if(isChecked == 'true'){
			checked = 'checked';
		}
		var id = oRecord.getData("id");
		elLiner.innerHTML = "<input type=\"checkbox\" onchange=\"${ns}updatePermission("+id+")\" name=\"userIds\" id=\"user" + id + "\" value=\"" + id + "\" " + checked + " />";
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [
		{key : "id", label :'<spring:message code="id"/>', className: "smallColumn", sortable: true},
		{key : "username", label :'<spring:message code="username"/>',sortable: true}, 
	 	{key : "firstName", label :'<spring:message code="firstname"/>',sortable: true}, 
		{key : "lastName", label :'<spring:message code="lastname"/>',sortable: true}, 
	 	{key : "checked", label :'', className: "smallColumn", formatter: "${ns}formatCheckbox", sortable : false}
 	];

	//datasource column definitions
	var ${ns}dsFields = [
		{key : "id", parser: "number"},
	    {key : "username"},
	    {key : "firstName"},
	    {key : "lastName"},
		{key : "checked"}
	];
	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
	};

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}users-table", "${usersTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>


