<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="redirect"/>
<portlet:resourceURL var="linkToRoles" id="view"/>
<portlet:resourceURL var="permissionsTable" id="permissionsTable">
	<portlet:param name="roleId" value="${roleId}"/>
</portlet:resourceURL>
<portlet:actionURL var="savePermissions" name="savePermissions">
	<portlet:param name="roleId" value="${roleId}"/>
	<portlet:param name="redirect" value="${redirect}"/>
</portlet:actionURL>

<portlet:renderURL var="showPermissions">
    <portlet:param name="ctx" value="showPermissions"/>
</portlet:renderURL>

<script type="text/javascript">
    breadcrumblinkToRolesAndPermissions = "<a href='${showPermissions}'><spring:message code='roles_and_permissions'/></a>&nbsp;&raquo;&nbsp;";
    var breadcrumbValue = "<h1>" + breadcrumblinkToRolesAndPermissions + "<c:out value='${roleName}'></c:out></h1>"
    YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="assign_permissions"/></h2>
	<div id="${ns}permDiv">
    <form:form method="POST" action="${savePermissions}" id="${ns}form-perms">
		<div id="${ns}form-permissions"></div>	    
		<div class="button-div" >
	    	<input type="submit" value="<spring:message code="save"/>" />
	    	<input type="submit" onclick="jQuery('#${ns}mainDiv').load('${linkToRoles}');return false;" value="<spring:message code="cancel"/>" />
	    </div> 
    </form:form>
    </div>
</div>

<script type="text/javascript">
var ${ns}preselected = new Array();

function ${ns}updatePermission(id){
	var name = '';
	if(jQuery('#permCheck'+id).is(':checked')){
		name = 'addPermIds';
		${ns}preselected.push({name: 'preSelectedPermIds', value: id});
	} else {
		name = 'removePermIds';
		for (var count = 0; count < ${ns}preselected.length; count++) {
		    if(${ns}preselected[count].value == id){
		    	${ns}preselected.splice(count, 1);
		    }
		}
		${ns}preselected.push({name: 'preSelectedPermIds', value: id * -1});
	}
	jQuery('<input>').attr({type: 'hidden',id: 'permission'+id, name: name, value: id}).appendTo('#${ns}form-perms');
	${ns}myDataTable.getDataSource().liveData = '${permissionsTable}&'+jQuery.param(${ns}preselected);
}

//custom formatters for table delete column
YAHOO.widget.DataTable.Formatter.${ns}actions = function(elLiner, oRecord, oColumn, oData) {
};

YAHOO.widget.DataTable.Formatter.${ns}formatCheckbox = function(elLiner, oRecord, oColumn, oData) {
	var isChecked = JSON.stringify(oRecord.getData("checked"));
	var checked = '';
	if(isChecked == 'true'){
		checked = 'checked';
	}
	
	var isDisabled = JSON.stringify(oRecord.getData("disabled"));
	var disabled = '';
	if(isDisabled == 'true'){
		disabled = 'disabled';
	}
	
	var id = oRecord.getData("id");
	
	elLiner.innerHTML = "<input type=\"checkbox\" onchange=\"${ns}updatePermission("+id+")\" name=\"permIds\" id=\"permCheck"+id+"\" value=\""+id+"\" "+checked+" "+disabled+"\"/>";
};
	
//datatable column definitions
var ${ns}myColumnDefs = [{key : "actionId", label : '<spring:message code="action_id"/>', sortable : true},
                         {key : "description", label : '<spring:message code="description"/>', sortable : true},
 						 {key : "checked", label : '', formatter: "${ns}formatCheckbox", sortable : false}];

//datasource column definitions
var ${ns}dsFields = [{key : "actionId"},
                     {key : "description"},
					 {key : "checked"},
					 {key : "disabled"},
					 {key : "id", parser : "number"}];

//javascript filter object
YAHOO.de.tarent.${ns}filter = {
};

//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-permissions","${permissionsTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>


