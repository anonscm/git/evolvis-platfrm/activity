<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="assignUsers" id="assignUsers"/>
<portlet:resourceURL var="assignPermissions" id="assignPermissions"/>
<portlet:resourceURL var="rolesTable" id="rolesTable"/>

<portlet:renderURL var="showPermissions">
    <portlet:param name="ctx" value="showPermissions"/>
</portlet:renderURL>

<script type="text/javascript">
    breadcrumblinkToRolesAndPermissions = "<a href='${showPermissions}'><spring:message code='roles_and_permissions'/></a>";
    YAHOO.de.tarent.createBreadcrumb("<h1>" + breadcrumblinkToRolesAndPermissions + "</h1>");
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
    <h2><spring:message code="roles_and_permissions"/></h2>
	<div id="${ns}rolesTable"></div>
</div>  

<script type="text/javascript">

//custom formatter for table delete column
YAHOO.widget.DataTable.Formatter.${ns}assignUser = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${assignUsers}&roleId="+oData+"');return false;\">&nbsp;</a>";
};

YAHOO.widget.DataTable.Formatter.${ns}assignPermission = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"jQuery('#${ns}mainDiv').load('${assignPermissions}&roleId="+oData+"');return false;\">&nbsp;</a>";
};
	
//datatable column definitions
var ${ns}myColumnDefs = [
	{key : "name", label : '<spring:message code="name"/>', sortable : false},
    {key : "description", label : '<spring:message code="description"/>', sortable : false},
    {key : "id", className: "userAssign", label :"&nbsp;", formatter:"${ns}assignUser", sortable :false},
    {key : "id", className: "permissionsAssign", label :"&nbsp;", formatter:"${ns}assignPermission", sortable :false}
];

//datasource column definitions
var ${ns}dsFields = [
	{key : "name"},
    {key : "description"},
    {key : "id"}
];

//javascript filter object
YAHOO.de.tarent.${ns}filter = {
};

${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}rolesTable","${rolesTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>


