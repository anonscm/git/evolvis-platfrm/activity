<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div id="${ns}mainDiv" class="mainDiv">
	<fieldset>
		<legend><spring:message code="activity_details"/></legend>
		<div class="label_details">
			<label><spring:message code="database_version"/>: </label><c:out value="${dbVersion}"></c:out>
			<br>
			<label><spring:message code="web_application_version"/>: </label><c:out value="${webAppVersion}"></c:out>
		</div>
	</fieldset>



	<fieldset>
		<legend><spring:message code="details"/> <c:out value="${resource.firstname}"></c:out> <c:out value="${resource.lastname}"></c:out></legend>
		<div class="label_details">
			<label><spring:message code="username"/>: </label> <c:out value="${resource.username}"></c:out> (<spring:message code="id"/>: <c:out value="${resource.pk}"></c:out>)<br>
			<label><spring:message code="email"/>: </label><c:out value="${resource.mail}"></c:out><br>
			<label><spring:message code="birthday"/>: </label><fmt:formatDate value="${resource.birth}" pattern="${dateformat}"/> <br>
			<label><spring:message code="enter_date"/>:</label><c:out value="${resource.entered}"></c:out> <br>
			<label><spring:message code="available_hours_per_month"/>: </label> <c:out value="${resource.availableHours}"></c:out> <spring:message code="hours"/><br>
			<label><spring:message code="contract_holidays"/>:</label> <c:out value="${resource.holiday}"></c:out> <spring:message code="days"/><br>
		
		</div>	
	</fieldset>
	
	<fieldset>
		<legend><spring:message code="group"/></legend>
		<c:forEach var="group" items="${groupList}">
			<c:out value="${group}"></c:out> <br>
		</c:forEach>
	</fieldset>
	
	<fieldset>
		<legend><spring:message code="your_functions"/>:</legend>
		<c:if test="${roleUser eq 'true' }">
			<spring:message code="enter_services"/><br>
			<spring:message code="enter_holidays"/><br>
		</c:if>
		<c:if test="${roleManager eq 'true' }">
			<spring:message code="enter_admin_tasks"/>
		</c:if>
			
			
	</fieldset>




</div>
