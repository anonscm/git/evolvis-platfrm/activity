<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="doSave" name="doSave"/>
<portlet:renderURL var="doCancel">
	<portlet:param name="ctx" value="doCancel"/>
</portlet:renderURL>
<portlet:renderURL var="editRoles">
    <portlet:param name="ctx" value="editRoles"/>
	<portlet:param name="resId" value="${userView.id}"/>
</portlet:renderURL>

<portlet:renderURL var="showAllUsersList">
    <portlet:param name="ctx" value="showAllUsersList"/>
</portlet:renderURL>
<portlet:renderURL var="edit">
    <portlet:param name="ctx" value="edit"/>
    <portlet:param name="id" value="${userView.id}"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbAllUsers = "<a href=${showAllUsersList}><spring:message code='resources'/></a>";
    var breadcrumbEditUser = "<a href=${edit}><c:out value='${userView.firstName}'></c:out>&nbsp;<c:out value='${userView.lastName}'></c:out></a>";

    var doesFormNeedsSaving = false;

    function checkIfFormNeedsToBeSaved(){
        if( doesFormNeedsSaving == true){
            var confirmation = confirm( "<spring:message code='warning.not.saved'/>" );
            return !confirmation;
        }else{
            return false;
        }
    }

    function formNeedsSaving()
    {
        doesFormNeedsSaving = true;
    }

    function callEditRolesDialogue(editRolesUrl){
        if( !checkIfFormNeedsToBeSaved()){
            YAHOO.de.tarent.goToUrl(editRolesUrl);
        }
    }


    <c:choose>
		<c:when test="${not empty userView.id}">
			var breadcrumbValue = "<h1>"+breadcrumbAllUsers+"&nbsp;&raquo;&nbsp;"+breadcrumbEditUser+"</h1>";
			YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		</c:when>
		<c:otherwise>
			var breadcrumbValue = "<h1>"+breadcrumbAllUsers+"&nbsp;&raquo;&nbsp;<spring:message code='new_resource'/></h1>"
			YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
		</c:otherwise>
	</c:choose>
</script>

<div id="${ns}mainDiv" class="mainDiv">
    <h2><spring:message code="add_resource"/></h2>
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="userView" novalidate="novalidate">
		<div class="button-div-top">
			<input type="submit"  <c:if test="${userView.id == null }"> disabled="disabled" </c:if> onclick="callEditRolesDialogue('${editRoles}&id=${userView.id}');return false;" class="edit" value="<spring:message code="edit_permissions"/>" />
		</div>
		
		<fieldset>
			<legend><spring:message code="master_data"/></legend>
			<form:hidden path="id"/>

			<c:if test="${not empty errors}">
				<span class="portlet-msg-error">
					<spring:message code="${errors}"/>
				</span>
			</c:if>

			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
				         	<a:inputText name="firstName" labelCode="Vorname" form="${ns}form" value="${userView.firstName}" required="true" styleClass="large" maxlength="255" requiredMessageCode="validate_firstname_not_empty" onchangeFunction="formNeedsSaving()"/>
					</div>
					<div class="layout-cell cell-fifty">
				       	<a:inputText name="lastName" labelCode="Nachname" form="${ns}form" value="${userView.lastName}" required="true" styleClass="large" maxlength="255" requiredMessageCode="validate_lastname_not_empty" onchangeFunction="formNeedsSaving()"/>
					</div>
				</div>
				<div class="layout-row">				
					<div class="layout-cell cell-fifty">
					   	<a:inputText name="username" labelCode="username" form="${ns}form" value="${userView.username}" required="true" styleClass="large" maxlength="10" requiredMessageCode="validate_username_not_empty" onchangeFunction="formNeedsSaving()"/>
					</div>
					<div class="layout-cell cell-fifty">
						<a:inputText name="mail" labelCode="email" form="${ns}form" value="${userView.mail}" required="false" styleClass="large" maxlength="255" requiredMessageCode="validate_email" onchangeFunction="formNeedsSaving()"/>
					</div> 
				</div>
                <c:if test="${ activityAuthMechanism == 'DATABASE' }">
                    <div class="layout-row">
                        <div class="layout-cell cell-fifty">
                            <a:inputText name="password" type="password" labelCode="password" form="${ns}form" value="${userView.password}" required="false" styleClass="large" maxlength="255" requiredMessageCode="NotEmpty.userView.password" onchangeFunction="formNeedsSaving()"/>
                        </div>
                        <div class="layout-cell cell-fifty">
                            <a:inputText name="passwordRepeat" type="password" labelCode="password_repeat" form="${ns}form" value="${userView.passwordRepeat}" required="false" styleClass="large" maxlength="255" requiredMessageCode="NotEmpty.userView.repeat-password" onchangeFunction="formNeedsSaving()"/>
                        </div>
                    </div>
                </c:if>
			</div>
			
			<div class="layout-table">
				<div class="layout-row">
					<div class="layout-cell cell-fifty">
			    		<label for="resourceTypeId"><spring:message code="resource_type"/>*</label>
			    		<select name="resourceTypeId" id="resourceTypeId" required="required" onchange="formNeedsSaving()">
					    	<option value=""><spring:message code="select_please"/></option>
						    	<c:forEach var="resourceType" items="${resourceTypeList}" >
									<option value="${resourceType.pk}" 
										<c:choose>
											<c:when test="${userView.resourceTypeId eq resourceType.pk}">
												selected="selected" 
											</c:when>
										</c:choose>
									><c:out value="${resourceType.name}"/></option>
								</c:forEach>
					    </select>								    
						<div class="portlet-msg-error yui-pe-content" id="${ns}form-resourceTypeId-required">
							<spring:message code="select_resource_type"/>
						</div>									
						<form:errors path="resourceTypeId" cssClass="portlet-msg-error" />
					</div>
					<div class="layout-cell cell-fifty">
						<label for="branchOfficeId"><spring:message code="branch_office"/>*</label>
					  		<select name="branchOfficeId" id="branchOfficeId" required="required" onchange="formNeedsSaving()">
					    	<option value=""><spring:message code="select_please"/></option>
					    	<c:forEach var="branchOffice" items="${branchOfficeList}" >
								<option value="${branchOffice.pk}" 
									<c:choose>
										<c:when test="${userView.branchOfficeId eq branchOffice.pk}">
											selected="selected" 
										</c:when>
									</c:choose>
								><c:out value="${branchOffice.name}"></c:out></option>
							</c:forEach>
						    </select>
						   <div class="portlet-msg-error yui-pe-content" id="${ns}form-branchOfficeId-required">
								<spring:message code="select_branchoffice"/>
							</div>									
						<form:errors path="branchOfficeId" cssClass="portlet-msg-error" ></form:errors>
					</div>
					<!-- <div class="layout-cell cell-twenty">
						<a:inputDate styleClass="small date" name="birthDate" labelCode="birthday" form="${ns}form" value="${userView.birthDate}" required="false" requiredMessageCode="validate_date" onchangeFunction="formNeedsSaving()"/>
					</div>-->
		    	</div>
		    </div>

			<div class="total_width">
				<label for="${ns}form-description"><spring:message code="comment"/>:</label>
				<textarea id="${ns}form-description" name="description" rows="10" cols="60" maxlength="4000" onchange="formNeedsSaving()"><c:out value="${userView.description}" /></textarea>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
						<spring:message code="validate_description_length"/>
					</div>
					<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
			</div>
			<div class="top-margin">
				<label for="active" ><spring:message code="active_resource"/></label>
		    	<input type="checkbox" name="active"  id="active"  value="true" onchange="formNeedsSaving()"
		    		<c:choose>
		    			<c:when test="${userView.active eq 't' or userView.active eq true}">
		    				checked="checked"
		    			</c:when>
		    	</c:choose> />
		    </div> 
		</fieldset>				    
		<fieldset>
			<legend><spring:message code="contract_type"/></legend>
				 <div class="layout-table">
				    <div class="layout-row">
						<div class="layout-cell cell-thirty-three">
					    	<label for="employmentId"><spring:message code="contract_type"/>*</label>
							<select name="employmentId" id="employmentId" required="required" onchange="formNeedsSaving()">
							   	<option value=""><spring:message code="select_please"/></option>
							   	<c:forEach var="employment" items="${employmentList}" >
									<option value="${employment.pk}" 
										<c:choose>
											<c:when test="${userView.employmentId eq employment.pk}">
												selected="selected" 
											</c:when>
										</c:choose>
									><c:out value="${employment.name}"></c:out></option>
								</c:forEach>
							</select>
							<div class="portlet-msg-error yui-pe-content" id="${ns}form-employmentId-required">
								<spring:message code="validate_job_type_not_empty"/>
							</div>									
							<form:errors path="employmentId" cssClass="portlet-msg-error" ></form:errors>
						</div>
						<div class="layout-cell cell-thirty-three">
					    	<a:inputDate styleClass="date" name="joinDate" labelCode="join_date" form="${ns}form" value="${userView.joinDate}" required="false" requiredMessageCode="validate_join_before_leave" onchangeFunction="formNeedsSaving()"/>
					        <div class="portlet-msg-error yui-pe-content" id="${ns}form-joinDate-dateInOrder">
								<spring:message code="validate_join_before_leave"/>
							</div>
						</div>
						<div class="layout-cell cell-thirty-three">
						   <a:inputDate styleClass="date" name="leavingDate" labelCode="leaving_date" form="${ns}form" value="${userView.leavingDate}" required="false" requiredMessageCode="validate_date" onchangeFunction="formNeedsSaving()"/>
						</div>
					</div>
							 	
					<div class="layout-row">
						<div class="layout-cell cell-thirty-three">
									<label for="salary"><spring:message code="salery"/></label>
									<input type="number" name="salary" id="salary" style="width:73%;" value="${userView.salary}" onchange="formNeedsSaving()"/> <span class="right"><spring:message code="salery_per_montch"/></span>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-salary-positiveNumber"><spring:message code="validate_digit_positive"/></div>
									<form:errors path="salary" cssClass="portlet-msg-error" ></form:errors>
						</div>
						<div class="layout-cell cell-thirty-three">
									<label for="cost"><spring:message code="cost"/></label>
									<input type="number" name="cost" id="cost" style="width:73%;" value="${userView.cost}" onchange="formNeedsSaving()"/> <span class="right"><spring:message code="cost_per_hour"/></span>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-cost-positiveNumber"><spring:message code="validate_digit_positive"/></div>
									<form:errors path="cost" cssClass="portlet-msg-error" ></form:errors>
						</div>
						<div class="layout-cell cell-thirty-three">
									<label for="availableHours"><spring:message code="available_per_month"/></label>
									<input type="number" name="availableHours" id="availableHours"  value="${userView.availableHours}" onchange="formNeedsSaving()"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-availableHours-positiveNumber"><spring:message code="validate_digit_positive"/></div>
									<form:errors path="availableHours" cssClass="portlet-msg-error" ></form:errors>
						</div>
					</div>
		    	</div>
				    </fieldset>
				    
				     <fieldset>
				     	<legend><spring:message code="rest_holiday_and_overtime"/></legend>
				    	<div class="layout-table">
				    		<div class="layout-row">
				    		<div class="layout-cell cell-twenty-five">
							 		<a:inputText name="holidays" form="${ns}form" maxlength="10" labelCode="holidays"  required="true" value="${userView.holidays}" requiredMessageCode="validate_holidays_not_empty" onchangeFunction="formNeedsSaving()"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-holidays-number"><spring:message code="validate_digit"/></div>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-holidays-roundingNumber"><spring:message code="validate_holidays"/></div>
								</div> 
								<div class="layout-cell cell-twenty-five">
									<a:inputText name="remainingDays" form="${ns}form" maxlength="10" labelCode="remaining_holidays"  required="false" value="${userView.remainingDays}" requiredMessageCode="validate_remaining_days_not_empty" onchangeFunction="formNeedsSaving()"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-remainingDays-number"><spring:message code="validate_remaining_days"/></div>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-remainingDays-roundingNumber"><spring:message code="validate_remaining_days"/></div>
								</div>
								<div class="layout-cell cell-twenty-five">
									<label for="overtimeHours"><spring:message code="overtime"/></label>
									<input type="number" name="overtimeHours" id="overtimeHours"  value="${userView.overtimeHours}" onchange="formNeedsSaving()"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-overtimeHours-number"><spring:message code="validate_overtime_hours"/></div>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-overtimeHours-roundingNumber"><spring:message code="validate_overtime"/></div>
									<form:errors path="overtimeHours" cssClass="portlet-msg-error" ></form:errors>
					       		</div>
							</div>
		    			</div>
				    </fieldset>
		   
				    <div class="button-div">
				    	<input type="submit"  class="save" value="<spring:message code="save"/>" />
				    	<input type="submit"  onclick="YAHOO.de.tarent.goToUrl('${doCancel}');return false;" class="cancel" value="<spring:message code="cancel"/>" />
				   </div>
	</form:form>
</div>



<script type="text/javascript">
	/**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"firstName", validator:"required"},
    	                     {id:"lastName", validator:"required"},
    	                     {id:"mail", validator:"email"},
    	                     {id:"birthDate", validator:"date"},
    	                     {id:"birthDate", validator:"dateInThePast"},
    	                     {id:"description", validator:"maxLength", maxLength:"4000"},
    	                     {id:"joinDate", validator:"date"},
    	                     {id:"joinDate", validator:"dateInOrder", compareDate:"leavingDate"},
    	                     {id:"leavingDate", validator:"date"},
    	                     {id:"username", validator:"required"},
    	                     {id:"resourceTypeId", validator:"required"},
    	                     {id:"branchOfficeId", validator:"required"},
    	                     {id:"employmentId", validator:"required"},
    	                     {id:"holidays", validator:"required"},
    	                     {id:"holidays", validator:"number"},
    	                     {id:"holidays", validator:"roundingNumber"},
    	                     {id:"salary", validator:"positiveNumber"},
    	                     {id:"cost", validator:"positiveNumber"},
    	                     {id:"availableHours", validator:"positiveNumber"},
    	                     {id:"remainingDays", validator:"required"},
    	                     {id:"remainingDays", validator:"number"},
    	                     {id:"remainingDays", validator:"roundingNumber"},
    	                     {id:"overtimeHours", validator:"roundingNumber"},
    	                     {id:"overtimeHours", validator:"number"}];
    	
	    var ${ns}zaForm = jQuery("#${ns}form"); 
	    ${ns}zaForm.bind("submit",function(event) {
			event.preventDefault();
			${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form", ${ns}validatorDescriptor));
		});
   };*/

</script>

