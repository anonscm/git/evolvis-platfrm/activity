<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="userPermissionsTable" id="userPermissionsTable">
	<!--portlet:param name="resId" value="${resId}"/-->
</portlet:resourceURL>
<portlet:actionURL var="doSavePermission" name="doSavePermission">
	<portlet:param name="resId" value="${resId}"/>
</portlet:actionURL>

<portlet:renderURL var="showAllUsersList">
    <portlet:param name="ctx" value="showAllUsersList"/>
</portlet:renderURL>
<portlet:renderURL var="edit">
    <portlet:param name="ctx" value="edit"/>
    <portlet:param name="id" value="${resId}"/>
</portlet:renderURL>

<portlet:renderURL var="editRoles">
    <portlet:param name="ctx" value="editRoles"/>
    <portlet:param name="resId" value="${resId}"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbAllUsers = "<a href=${showAllUsersList}><spring:message code='resources'/></a>";
    var breadcrumbEditUser = "<a href=${edit}>${resName}</a>";
    var breadcrumbEditUserRoles = "<a href=${editRoles}&id=${resId}><spring:message code="edit_permissions"/></a>";


    <c:if test="${not empty resId}">
    var breadcrumbValue = "<h1>"+breadcrumbAllUsers+"&nbsp;&raquo;&nbsp;"+breadcrumbEditUser+"&nbsp;&raquo;&nbsp;"+
                                breadcrumbEditUserRoles+"&nbsp;&raquo;&nbsp;<spring:message code="custom_permissions"/></h1>";
    YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
    </c:if>
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="custom_permissions"/></h2>
	<div id="${ns}permDiv" class="permDiv" >
    <form:form method="POST" id="${ns}form-perms" action="${doSavePermission}" novalidate="novalidate">
    	<div class="button-div-top" >
	    	<input type="submit"  value="<spring:message code="save"/>" />
	    </div> 

			<div id="${ns}form-permissions" class=""></div>

	    <div class="button-div" >
	    	<input type="submit"  onclick="YAHOO.de.tarent.goToUrl('${editRoles}&id=${resId}&');return false;" value="<spring:message code="cancel"/>" />
	    </div> 
    </form:form>
    </div>
</div>  

<script type="text/javascript">
	
var ${ns}preselected = new Array();

function ${ns}updatePermission(id){
	var name = '';
	if(jQuery('#permCheck'+id).is(':checked')){
		name = 'addPermIds';
		${ns}preselected.push({name: 'preSelectedPermIds', value: id});
	} else {
		name = 'removePermIds';
		for (var count = 0; count < ${ns}preselected.length; count++) {
		    if(${ns}preselected[count].value == id){
		    	${ns}preselected.splice(count, 1);
		    }
		}
		${ns}preselected.push({name: 'preSelectedPermIds', value: id * -1});
	}
	jQuery('<input>').attr({type: 'hidden',id: 'permission'+id, name: name, value: id}).appendTo('#${ns}form-perms');
	${ns}myDataTable.getDataSource().liveData = '${userPermissionsTable}&'+jQuery.param(${ns}preselected);
}

//custom formatter for checkbox column
YAHOO.widget.DataTable.Formatter.${ns}formatCheckbox = function(elLiner, oRecord, oColumn, oData) {
	var isChecked = JSON.stringify(oRecord.getData("checked"));
	var checked = '';
	if(isChecked == 'true'){
		checked = 'checked';
	}
	
	var isDisabled = JSON.stringify(oRecord.getData("disabled"));
	var disabled = '';
	if(isDisabled == 'true'){
		disabled = 'disabled';
	}
	
	var id = oRecord.getData("id");
	
	elLiner.innerHTML = "<input type=\"checkbox\" onchange=\"${ns}updatePermission("+id+")\"  name=\"permIds\" id=\"permCheck"+id+"\" value=\""+id+"\" "+checked+" "+disabled+"/>";
};

//datatable column definitions
var ${ns}myColumnDefs = [{key :"id",className: "smallColumn",label :'<spring:message code="id"/>', sortable :true},
                         {key :"description",label :'<spring:message code="description"/>', sortable :true},
 						 {key :"checked",className: "smallColumn",label :'', formatter:"${ns}formatCheckbox", sortable :false}];

//datasource column definitions
var ${ns}dsFields = [{key :"id",parser :"number"},
                     {key :"description"},
					 {key :"checked"},
					 {key :"disabled"}];
				
//javascript filter object
YAHOO.de.tarent.${ns}filter = {
};

//the datatable
${ns}myDataTable = YAHOO.de.tarent.filteredDataTable('${ns}form-permissions','${userPermissionsTable}', ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>


