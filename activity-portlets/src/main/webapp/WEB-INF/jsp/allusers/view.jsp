<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>
<portlet:renderURL var="add">
    <portlet:param name="ctx" value="add"/>
</portlet:renderURL>
<portlet:renderURL var="edit">
    <portlet:param name="ctx" value="edit"/>
</portlet:renderURL>
<portlet:actionURL var="delete" name="doDeleteResource"/>

<portlet:renderURL var="showAllUsersList">
    <portlet:param name="ctx" value="showAllUsersList"/>
</portlet:renderURL>

<script type="text/javascript">
	var breadcrumbValue = "<h1><a href=${showAllUsersList}><spring:message code='resources'/></a></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
    <h2><spring:message code="resources"/></h2>
	<form:form method="POST" id="${ns}form" action="${add}">
		<a:authorize ifAnyGranted="Resource.ADD_ALL, Resource.APPEND_ALL">
		<div class="button-div">
			<input type="submit" id="addUserButton" onclick="this.form.action.value='${add}'" 
			value="<spring:message code="new_resource"/>" />
		</div>
		</a:authorize>
		<div>
			<label for="checkActive"><spring:message code="active_resources" /></label>
			<input type="checkbox" name="checkActive" id="checkActive" 
				<c:if test="${activeUser != null}"> 
					checked="checked" 
				</c:if>
				onchange="${ns}filterByActive(this.checked);">
		</div>

		<c:if test="${canEdit eq 'f'}">
				<p class="portlet-msg-error"><spring:message code="validate_edit_yourself" /></p>
		</c:if>
		
		<div id="${ns}form-users" class="clearDiv"></div>

	</form:form>
</div>

<script type="text/javascript">
	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${edit}&id="+oData+"');return false;\">&nbsp;</a>";
		var element = elLiner.parentNode.parentNode;
		var children = YAHOO.util.Dom.getChildren(element);
		if(oRecord.getData("active") != 't') { 
			YAHOO.util.Dom.addClass(element, 'redRow'); 
        }
	};

    //custom formatter for table delete column
    YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
        elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${delete}&id="+oData+"');return false;\">&nbsp;</a>";
        var element = elLiner.parentNode.parentNode;
        var children = YAHOO.util.Dom.getChildren(element);
        if(oRecord.getData("active") != 't') {
            YAHOO.util.Dom.addClass(element, 'redRow');
        }
    };


    //custom formatter for description column
	YAHOO.widget.DataTable.Formatter.${ns}formatDescription = function(elLiner, oRecord, oColumn, oData) {
		var value = YAHOO.lang.escapeHTML(oData);
		elLiner.innerHTML = value.replace(/\r\n|\n|\r/g,'<br />');
	};

	/*javascript filter object*/
	YAHOO.de.tarent.${ns}filter = {
		active : "${activeUser}"
	};
	
	/*datatable column definitions*/
	var ${ns}myColumnDefs = [
		{key :"idTable", label :'<spring:message code="id"/>', className: "smallColumn", sortable :true},
		{key :"username", label :'<spring:message code="username"/>', sortable :true},
		{key :"firstName", label :'<spring:message code="firstname"/>', sortable :true}, 
		{key :"lastName", label :'<spring:message code="lastname"/>', sortable :true}
        <a:authorize ifAnyGranted="Resource.EDIT_ALL, Resource.APPEND_ALL">
            ,{key :"id", className: "editColumn", label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false}
        </a:authorize>
        <a:authorize ifAnyGranted="Resource.DELETE_ALL, Resource.DETACH_ALL">
            ,{key :"id", className: "deleteColumn", label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}
        </a:authorize>
	 ];

	/*datasource column definitions*/
	var ${ns}dsFields = [
		{key :"id", parser :"number"},
	    {key :"idTable", parser :"number"},
	    {key :"lastName"},
		{key :"firstName"},
		{key :"username"},
		{key :"active"}
	];
	
	/*the datatable*/
	var ${ns}limit = ${config.pageItems};
	var ${ns}offset = ${config.offset};
	var ${ns}sort = "${config.sortColumn}";
	var ${ns}dir = "${config.sortOrder}";
	
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-users","${table}", ${ns}dsFields, ${ns}myColumnDefs, 
						YAHOO.de.tarent.${ns}filter, null, "username", "asc", ${ns}limit, ${ns}offset, ${ns}sort, ${ns}dir, true);

	/*check active*/ 
	var ${ns}filterByActive = function(active){
		if (active == true) {
			YAHOO.de.tarent.${ns}filter.active = 't';
		} else {
			YAHOO.de.tarent.${ns}filter.active = '';
		}
		${ns}myDataTable.reloadData();
	}
</script>

