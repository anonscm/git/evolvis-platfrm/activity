<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="edit" id="edit">
	<portlet:param name="id" value="${rolesView.resourceId}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doRemoveRole" id="doRemoveRole"/>
<portlet:resourceURL var="doAddRole" id="doAddRole"/>
<portlet:resourceURL var="rolePermissionsTable" id="rolePermissionsTable"/>
<portlet:renderURL var="editPermissions">
    <portlet:param name="ctx" value="editPermissions"/>
	<portlet:param name="resId" value="${rolesView.resourceId}"/>
</portlet:renderURL>

<portlet:renderURL var="showAllUsersList">
    <portlet:param name="ctx" value="showAllUsersList"/>
</portlet:renderURL>
<portlet:renderURL var="edit">
    <portlet:param name="ctx" value="edit"/>
    <portlet:param name="id" value="${resourceId}"/>
</portlet:renderURL>

<portlet:renderURL var="editRoles">
    <portlet:param name="ctx" value="editRoles"/>
    <portlet:param name="resId" value="${resourceId}"/>
</portlet:renderURL>

<script type="text/javascript">
    var breadcrumbAllUsers = "<a href=${showAllUsersList}><spring:message code='resources'/></a>";
    var breadcrumbEditUser = "<a href=${edit}>${resourceName}</a>";
    var breadcrumbEditUserRoles = "<a href=${editRoles}&id=${resourceId}><spring:message code="edit_permissions"/></a>";

    <c:if test="${not empty resourceId}">
        var breadcrumbValue = "<h1>"+breadcrumbAllUsers+"&nbsp;&raquo;&nbsp;"+breadcrumbEditUser+"&nbsp;&raquo;&nbsp;"+breadcrumbEditUserRoles+"</h1>";
        YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
    </c:if>
</script>

<%--script type="text/javascript">
	var linkToUserList = "";
	<c:if test="${not empty linkToUserList}">
		linkToUserList ="<a href='${linkToUserList}'><spring:message code='resources'/></a>&nbsp;&raquo;&nbsp;";
	</c:if>
	var breadcrumbValue = "<h1>"+linkToUserList+"<c:out value='${resourceName}'></c:out></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
</script --%>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
    <h2><spring:message code="edit_permissions"/>: <c:out value='${resourceName}'/></h2>
    <form:form method="POST" id="${ns}form-roles" commandName="rolesView" novalidate="novalidate">
   		<form:hidden path="resourceId"/>
   		<div class="button-div-top" >
	    	<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${editPermissions}&');return false;" value="<spring:message code="custom_permissions"/>" />
	    </div> 

   		<fieldset>
			<legend><spring:message code="roles"/></legend>
			<div class="layout-table auto-width">
			<c:forEach var="role" items="${rolesView.userRoles}">
   				<div class="layout-row auto-width">
   					<div class="layout-cell">
						<label for="${ns}form-user-roles"><c:out value="${role.name}"/></label>
					</div>
					<div class="layout-cell">
						<input type="hidden" name="${ns}roleIds" value="${role.id}">
						<input type="submit"  onclick="${ns}removeRole(${rolesView.resourceId}, ${role.id}); return false;" value="<spring:message code="remove"/>" />
					</div>
				</div>
			</c:forEach>
			<div class="layout-row auto-width">
				<div class="layout-cell">
					<select id="${ns}form-select-role">
						<c:forEach var="role" items="${rolesView.allRolesExceptUserRoles}">
							<option value="${role.id}"><c:out value="${role.name}"/></option>
						</c:forEach>
					</select>
				</div>
				<div class="layout-cell">
					<input type="submit"  onclick="${ns}addRole(${rolesView.resourceId}); return false;" value="<spring:message code="to_add"/>" />
				</div>
			</div>
			</div>
		</fieldset>
		<fieldset>
			<legend><spring:message code="permissions"/></legend>
			<div id="${ns}form-permissions" class=""></div>
		</fieldset>
	    <div class="button-div" >
	    	<input type="submit" onclick="YAHOO.de.tarent.goToUrl('${edit}');return false;" value="<spring:message code="back"/>" />
	    </div> 
    </form:form>
</div>  

<script type="text/javascript">

//custom formatter for checkbox column
YAHOO.widget.DataTable.Formatter.${ns}formatCheckbox = function(elLiner, oRecord, oColumn, oData) {
	var isChecked = JSON.stringify(oData);
	var checked = '';
	if(isChecked == 'true'){
		checked = 'checked';
	}
	
	var id = oRecord.getData("id");
	
	elLiner.innerHTML = "<input type=\"checkbox\" name=\"permission\" value=\""+id+"\" "+checked+" disabled />";
};

function ${ns}addRole(resId) {
	roleId = jQuery('#${ns}form-select-role').val();
	jQuery('#${ns}mainDiv').load('${doAddRole}&id='+roleId+'&resId='+resId);
}

function ${ns}removeRole(resId, roleId) {
	jQuery('#${ns}mainDiv').load('${doRemoveRole}&id='+roleId+'&resId='+resId);
}

//datatable column definitions
var ${ns}myColumnDefs = [{key :"id",className: "smallColumn",label :'<spring:message code="id"/>', sortable :true},
                         {key :"description",label :'<spring:message code="description"/>', sortable :true},
 						 {key :"checked",className: "smallColumn",label :'', formatter:"${ns}formatCheckbox", sortable :false}];

//datasource column definitions
var ${ns}dsFields = [{key :"id",parser :"number"},
                     {key :"description"},
					 {key :"checked"}];
				
//javascript filter object
YAHOO.de.tarent.${ns}filter = {
};

//the datatable
var roleIds = '';
jQuery("input:hidden[name='${ns}roleIds']").each(function(){
    roleIds += '&roleIds='+jQuery(this).val();
}).get();

${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-permissions","${rolePermissionsTable}" + roleIds, ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
</script>


