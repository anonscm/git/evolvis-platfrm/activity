<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="table"/>

<portlet:renderURL var="myActiveProjects"/>

<script type="text/javascript">
    var breadcrumbValueMyActiveProjects = "<a href=${myActiveProjects}><spring:message code='my_active_projects'/></a>";
    var breadcrumbValueMyPositions = "<spring:message code='my_positions'/>";

    <c:if test="${ ns == '_mypositionsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueMyPositions+"</h1>");
    </c:if>

    <c:if test="${ ns == '_myactiveprojectsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueMyActiveProjects+"&nbsp;&raquo;&nbsp;"+breadcrumbValueMyPositions+"</h1>");
    </c:if>
</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
	<h2><spring:message code="positions"/></h2>
    <form:form method="POST" id="${ns}form" action="">
		     <div id="${ns}form-position" class=""></div>   	
    </form:form>
</div>  
 
<script type="text/javascript">
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"jobName",label :"<spring:message code='job'/>",sortable :true}, 
	 					{key :"positionName",label :"<spring:message code='position'/>",sortable :true},
	 					{key :"positionStatus",label :"<spring:message code='position_status'/>",sortable :true}
	 					];

	//datasource column definitions
	var ${ns}dsFields = [{key :"jobName"},
	                {key :"positionName"},
	                {key :"positionStatus"}
	                ];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-position","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

</script>	
		

