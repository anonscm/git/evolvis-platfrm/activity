<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="getActivityTable"/>
<portlet:renderURL var="doCancel"/>
<portlet:resourceURL var="doDetails" id="details">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doPositions" id="doPositions">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doBills" id="doBills">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doCosts" id="doCosts">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="getJobExport" id="getJobExport">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>

<%@ include file="/WEB-INF/jsp/jobs/breadcrumbs/breadcrumbs.jsp"%>

<script type="text/javascript">

    function ${ns}loadBackUrl() {
        this.document.location.href = '${doCancel}';
    };

</script>


<form:form method="POST" id="${ns}form" name="${ns}form" action="${getJobExport}">

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="job"/>: <c:out value="${jobView.name}"/></h2>

	<div>
		<ul class="tabs marginBug">
			<portlet:renderURL var="getJobDetails">
				<portlet:param name="ctx" value="jobDetails"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:renderURL>
			<li><a href="${getJobDetails}"><spring:message code="details"/></a></li>
			<a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
				<portlet:renderURL var="getJobPositions">
					<portlet:param name="ctx" value="jobPositions"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobPositions}"><spring:message code="positions"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Activity.VIEW_BY_PROJECT, Activity.VIEW_ALL">
				<portlet:renderURL var="getJobActivities">
					<portlet:param name="ctx" value="jobActivities"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li class="current"><a href="${getJobActivities}"><spring:message code="activities"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL, Invoice.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobInvoices">
					<portlet:param name="ctx" value="jobInvoices"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobInvoices}"><spring:message code="invoices"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Cost.VIEW_ALL, Cost.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobCosts">
					<portlet:param name="ctx" value="jobCosts"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobCosts}"><spring:message code="costs"/></a></li>
			</a:authorize>
		</ul>
	</div>

    <%@ include file="/WEB-INF/jsp/jobs/activities-filter/filter-layout.jsp"%>

	<div id="${ns}form-activities" class="clearDiv top-margin"></div>
		
	<a:authorize ifAnyGranted="Activity.EXPORT_ALL">
		<h3 class="top-margin">
			<spring:message code="export_activities" />
		</h3>
		
		<div class="layout-table">
	        <div class="layout-row">
	            <div class="layout-cell cell-twenty-five">
	                <a:inputDate name="fromDate" form="${ns}form" labelCode="job_start" styleClass="" required="true" value="${fromDate}" />
	            </div>
	            <div class="layout-cell cell-twenty-five">
	                <a:inputDate name="toDate" form="${ns}form" labelCode="job_end" styleClass="" required="true" value="${toDate}" />
	            </div>
	            <div class="layout-cell cell-fifty input_width cell_bottom">
	                <input type="submit" value="<spring:message code="export"/>" />		                
	            </div>
	        </div>
	    </div>
	</a:authorize>
	
	<div class="button-div">
        <input type="button" onclick="${ns}loadBackUrl(); return false;" value="<spring:message code="back_to_main_mask"/>" />
	</div>
</div>

</form:form>
<%@ include file="/WEB-INF/jsp/activities/table.jsp" %>
