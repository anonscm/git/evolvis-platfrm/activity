<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ include file="/WEB-INF/jsp/jobs/breadcrumbs/breadcrumbs.jsp"%>

<portlet:renderURL var="doCancel"/>
<portlet:resourceURL var="doDetails" id="details">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doPositions" id="doPositions">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doActivities" id="doActivities">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doBills" id="doBills">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="doCosts" id="doCosts">
	<portlet:param name="jobId" value="${paramJob.pk}"/>
</portlet:resourceURL>
<portlet:resourceURL var="edit" id="editCost"/>
<portlet:renderURL var="doCostAdd">
    <portlet:param name="ctx" value="doCostAdd"/>
    <portlet:param name="jobId" value="${jobView.id}"/>
    <portlet:param name="projectId" value="${jobView.projectId}"/>
</portlet:renderURL>


<script type="text/javascript">
    function ${ns}loadBackUrl() {
        this.document.location.href = '${doCancel}';
    };
</script>


<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="job"/>: <c:out value="${jobView.name}"/></h2>

	<div>
		<ul class="tabs marginBug">
			<portlet:renderURL var="getJobDetails">
				<portlet:param name="ctx" value="jobDetails"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:renderURL>
			<li><a href="${getJobDetails}"><spring:message code="details"/></a></li>
			<a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
				<portlet:renderURL var="getJobPositions">
					<portlet:param name="ctx" value="jobPositions"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobPositions}"><spring:message code="positions"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Activity.VIEW_BY_PROJECT, Activity.VIEW_ALL">
				<portlet:renderURL var="getJobActivities">
					<portlet:param name="ctx" value="jobActivities"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobActivities}"><spring:message code="activities"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL, Invoice.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobInvoices">
					<portlet:param name="ctx" value="jobInvoices"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobInvoices}"><spring:message code="invoices"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Cost.VIEW_ALL, Cost.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobCosts">
					<portlet:param name="ctx" value="jobCosts"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li class="current"><a href="${getJobCosts}"><spring:message code="costs"/></a></li>
			</a:authorize>
		</ul>
	</div>

    <div class="button-div">
        <input type="button" onclick="YAHOO.de.tarent.goToUrl('${doCostAdd}'); return false;" value="<spring:message code="new_cost"/>" />
	</div>

	<div id="${ns}form-costs" class="clearDiv"></div>
	
	<div class="button-div">
        <input type="button" onclick="${ns}loadBackUrl(); return false;" value="<spring:message code="back_to_main_mask"/>" />
	</div>
</div>

<%@ include file="/WEB-INF/jsp/common/tables/costs.jsp" %>
