<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
    <portlet:param name="jobId" value="${jobView.id}"/>
</portlet:renderURL>

<script type="text/javascript">
	var customersPageLink = "";
	<c:if test="${not empty linkToCustomerPage}">
	customersPageLink = "<a href='${linkToCustomerPage}'><c:out value='${positionView.customerName}'></c:out></a>&nbsp;&raquo;&nbsp;";
	</c:if>
	var projectLink = "";
	<c:if test="${not empty linkToProjectPage}">
	projectLink = "<a href='${linkToProjectPage}'><c:out value='${positionView.projectName}'></c:out></a>&nbsp;&raquo;&nbsp;";
	</c:if>
	var jobLink = "";
	<c:if test="${not empty linkToJobPage}">
	jobLink = "<a href='${linkToJobPage}'><c:out value='${positionView.jobName}'></c:out></a>&nbsp;&raquo;&nbsp;";
	</c:if>
	var positionLink = "";
	<c:if test="${not empty linkToPositionPage}">
	positionLink = "<a href='${linkToPositionPage}'><spring:message code='positions'/></a>&nbsp;&raquo;&nbsp;";
	</c:if>

	<c:choose>
	<c:when test="${not empty positionView.id}">

	var breadcrumbValue = "<h1>" + customersPageLink + projectLink + jobLink
			+ positionLink
			+ "<c:out value='${positionView.positionName}'></c:out></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	</c:when>
	<c:when test="${not empty paramJob}">
	var breadcrumbValue = "<h1>"
			+ customersPageLink
			+ projectLink
			+ jobLink
			+ positionLink
			+ "<spring:message code='new_position'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	</c:when>
	<c:otherwise>
	var breadcrumbValue = "<h1>"
			+ positionLink
			+ "<spring:message code='new_position'/></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);
	</c:otherwise>
	</c:choose>

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
<h2><spring:message code="changes"/></h2>

	<form:form method="POST" id="${ns}form" action="${doPositionSave}"
		commandName="positionView">

	<div class="yui-dt">
		<div class="yui-dt-mask" style="display: none;"></div>
			<table>
				<colgroup>
					<col>
					<col>
					<col>
					<col>
					<col>
				</colgroup>
				<thead>
					<tr>
						<th><div class="yui-dt-liner"><spring:message code="changed_date"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="changed_by"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="field_name"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="old_value"/></div></th>
						<th><div class="yui-dt-liner"><spring:message code="new_value"/></div></th>
					</tr>
				</thead>
				<tbody tabindex="0" class="yui-dt-data" style="">

					<c:forEach var="changeInfo" items="${jobChanges}">
						<tr class="yui-dt-rec yui-dt-first yui-dt-even">
							<td headers="yui-dt113-th-positionName "><div
									class="yui-dt-liner">
									<c:out value="${changeInfo.changeDate}" />
								</div></td>
							<td headers="yui-dt113-th-description "><div
									class="yui-dt-liner">
									<c:out value="${changeInfo.changeUser}" />
								</div></td>
							<td headers="yui-dt113-th-positionStatusName " colspan="3"><div
									class="yui-dt-liner">&nbsp;</div></td>
						</tr>
						<c:forEach var="changeField" items="${changeInfo.changeFields}">

							<tr class="yui-dt-rec yui-dt-first yui-dt-even">
								<td headers="yui-dt113-th-positionStatusName " colspan="2"><div
										class="yui-dt-liner">&nbsp;</div></td>
								<td headers="yui-dt113-th-positionName "><div>
										<c:out value="${changeField.fieldName}" />
									</div></td>
								<td headers="yui-dt113-th-description "><div
										class="yui-dt-liner">
										<c:out value="${changeField.oldValue}" />
									</div></td>
								<td headers="yui-dt113-th-description "><div
										class="yui-dt-liner">
										<c:out value="${changeField.newValue}" />
									</div></td>
							</tr>
						</c:forEach>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div class="button-div">
            <input type="button" onclick="YAHOO.de.tarent.goToUrl('${getJobDetails}'); return false;" value="<spring:message code="back"/>" />
		</div>

	</form:form>
</div>
