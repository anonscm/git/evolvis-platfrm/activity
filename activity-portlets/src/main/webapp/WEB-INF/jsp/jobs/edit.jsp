<%@ include file="/WEB-INF/jsp/include.jsp"%>

<c:if test="${empty doCancel}">
	<%-- goes to JobListController default rendering --%>
	<portlet:renderURL var="doCancel"/>
</c:if>

<portlet:actionURL var="doSave" name="doJobSave">
    <portlet:param name="jobId" value="${jobView.id}"/>
	<portlet:param name="ctx" value="jobUpdate"/>
	<portlet:param name="redirect" value="${doCancel}"/>
    <portlet:param name="backUrl" value="${backUrl}"/>
</portlet:actionURL>

<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/jobs/breadcrumbs/edit-breadcrumbs.jsp"%>

<div id="${ns}mainDiv" class="mainDiv">

	<h2><spring:message code="add_job"/></h2>
	
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="jobView" novalidate="novalidate">
		<form:errors path="" cssClass="portlet-msg-error"/>
		<form:hidden path="id"/>
		
		<fieldset>
			<legend><spring:message code="master_Data"/></legend>
				<div class="left" style="width: 49%; margin-right: 1%;">
						<div class="layout-table">
							<div class="layout-row">
								<div class="layout-cell">
									<a:inputText name="name" form="${ns}form" maxlength="255" labelCode="name" styleClass="large" required="true" value="${jobView.name}" requiredMessageCode="validate_name_not_empty"/>
								</div>
							</div>
						</div>
						<div class="layout-table">
							<div class="layout-row">							
								<div class="layout-cell cell-twenty-five">
									<a:inputText name="nr" form="${ns}form" maxlength="255" labelCode="job_number" styleClass="large" required="true" value="${jobView.nr}" requiredMessageCode="validate_job_number_not_empty"/>
								</div>
								<div class="layout-cell cell-twenty-five">
									<a:inputText name="accounts" form="${ns}form" maxlength="8" labelCode="accounts" styleClass="large" required="true" value="${jobView.accounts}" requiredMessageCode="validate_accounts_not_empty"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-accounts-number">
										<spring:message code="validate_digit"/>
									</div>
								</div>
							</div>
							<div class="layout-row">		
								<div class="layout-cell cell-twenty-five">
									<a:inputDate styleClass="large date" name="jobStartDate" labelCode="start_date" form="${ns}form" value="${jobView.jobStartDate}" required="false"/>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-jobStartDate-dateInOrder">
										<spring:message code="validate_start_before_end"/>
									</div>
								</div>
								<div class="layout-cell cell-twenty-five">
									<a:inputDate styleClass="date" name="jobEndDate" labelCode="end_date" form="${ns}form" value="${jobView.jobEndDate}" required="false"/>
								</div>
							</div>							
							
						</div>
					</div>
					<div class="left" style="width:50%;">	
						<div class="layout-table">
							<div class="layout-row">
								<div class="layout-cell">
									<label for="${ns}form-description"><spring:message code="description"/>:</label>
									<textarea id="${ns}form-description" name="description" style="height:119px;" maxlength="4000"><c:out value="${jobView.description}" /></textarea>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-description-maxLength">
										<spring:message code="validate_description_length"/>
									</div>
									<form:errors path="description" cssClass="portlet-msg-error" ></form:errors>
								</div>
							</div>
						</div>
					</div>
				
		</fieldset>
		<fieldset>
			<legend><spring:message code="link_ups"/></legend>
				<div class="layout-table bottom-margin">
					<div class="layout-row">
						<div class="layout-cell cell-thirty-three">
					    	<a:selectProject styleClass="default" name="projectId" labelCode="project" selectedValue="${jobView.projectId}" required="true" requiredMessageCode="validate_position_not_empty" projectList="${projectList}" form="${ns}form"/>
					    </div>
					    
					    <div class="layout-cell cell-thirty-three">
					    	<label for="${ns}form-managerId"><spring:message code="project_manager"/>*:</label>
							<select name="managerId" id="${ns}form-managerId">
										<option value=""><spring:message code="select_please"/></option>
										<c:forEach var="manager" items="${managerList}" >
											<option value="${manager.pk}" 
												<c:if test="${jobView.managerId eq manager.pk }">
													selected="selected" 
												</c:if>>
												<c:out value="${manager.lastname},"></c:out> <c:out value="${manager.firstname}"></c:out>
											</option>
										</c:forEach>
									</select>
									<div class="portlet-msg-error yui-pe-content" id="${ns}form-managerId-required">
										<spring:message code="validate_project_responsible_not_empty"/>
									</div>
									<form:errors path="managerId" cssClass="portlet-msg-error" ></form:errors>
									<script type="text/javascript">
									if(!("autofocus" in document.createElement("input"))) {
										document.getElementById("${ns}form-managerId").focus();
									}
									</script>
					    </div>
				    	<div class="layout-cell cell-thirty-three"></div>				    
					</div>
				</div>
				<div class="layout-table">				
				    <div class="layout-row">
				    	<div class="layout-cell cell-thirty-three">
			    			<a:inputLink name="crm" form="${ns}form" maxlength="255" labelCode="crm" 
			    				styleClass="default" required="false" value="${jobView.crm}" linkMessageCode="validate_http_url"/>
			    		</div>
			    		<div class="layout-cell cell-thirty-three">
			    			<a:inputLink name="offer" form="${ns}form" maxlength="255" labelCode="offer_dms" 
			    				styleClass="default" required="false" value="${jobView.offer}" linkMessageCode="validate_http_url"/>
			    		</div>
			    		<div class="layout-cell cell-thirty-three">
			    			<a:inputLink name="request" form="${ns}form" maxlength="255" labelCode="request_dms" 
			    				styleClass="default" required="false" value="${jobView.request}" linkMessageCode="validate_http_url" />
			    		</div>
			    	</div>
				</div>
		</fieldset>
		
		<fieldset>
			<legend><spring:message code="status"/></legend>
				<div class="layout-table">
					<div class="layout-row">
				    		<div class="layout-cell cell-thirty-three">
				    			<label for="${ns}form-jobTypeId"><spring:message code="type"/>*:</label>
								<select name="jobTypeId" id="${ns}form-jobTypeId">
									<option value=""><spring:message code="select_please"/></option>
									<c:forEach var="jobType" items="${jobTypes}" >
										<option value="${jobType.pk}" 
											<c:if test="${jobView.jobTypeId eq jobType.pk }">
												selected="selected" 
											</c:if>>
											<c:out value="${jobType.name}"></c:out>
										</option>
									</c:forEach>
								</select>
								<div class="portlet-msg-error yui-pe-content" id="${ns}form-jobTypeId-required">
									<spring:message code="validate_job_type_not_empty"/>
								</div>
								<form:errors path="jobTypeId" cssClass="portlet-msg-error" ></form:errors>
								<script type="text/javascript">
								if(!("autofocus" in document.createElement("input"))) {
									document.getElementById("${ns}form-jobTypeId").focus();
								}
								</script>
							</div>
				    		<div class="layout-cell cell-thirty-three">
				    			<label for="${ns}form-jobStatusId"><spring:message code="status"/>*:</label>
								<select name="jobStatusId" id="${ns}form-jobStatusId">
									<option value=""><spring:message code="select_please"/></option>
									<c:forEach var="jobStatus" items="${jobStatuses}" >
										<option value="${jobStatus.pk}" 
											<c:if test="${jobView.jobStatusId eq jobStatus.pk }">
												selected="selected" 
											</c:if>>
											<c:out value="${jobStatus.name}"></c:out>
										</option>
									</c:forEach>
								</select>
								<div class="portlet-msg-error yui-pe-content" id="${ns}form-jobStatusId-required">
									<spring:message code="validate_job_status_not_empty"/>
								</div>
								<form:errors path="jobStatusId" cssClass="portlet-msg-error" ></form:errors>
								<script type="text/javascript">
								if(!("autofocus" in document.createElement("input"))) {
									document.getElementById("${ns}form-jobStatusId").focus();
								}
								</script>
							</div>
				    		<div class="layout-cell cell-thirty-three input_width">
								<c:if test="${jobView.id != null}">

									<span class="info"><spring:message code="update_position_status"/>:</span>
									<div style="line-height: 1.4em;">

											<input type="radio" name="updatePosition" id="updatePosition_yes" value="True" checked="checked" >
										    <label for="updatePosition_yes" style="display: inline; margin-right: 5px;"><spring:message code="yes"/></label>
											<input type="radio" name="updatePosition" id="updatePosition_no" value="False"  
											    <c:choose>
													<c:when test="${!jobView.updatePosition}">
														checked="checked"
													</c:when>
													<c:otherwise></c:otherwise>
												</c:choose> >
											<label for="updatePosition_no" style="display: inline;"><spring:message code="no"/></label>   
									</div>
								</c:if>
							</div>
						</div>
					</div>
		</fieldset>
		<fieldset>
			<legend><spring:message code="invoice_practice"/></legend>		
		    	<div class="layout-table">
		    		<div class="layout-row">
				    		<div class="layout-cell cell-thirty-three">
				    			<a:inputDate styleClass="date" name="expectedBilling" labelCode="expected_invoice_Date" form="${ns}form" value="${jobView.expectedBilling}" required="false"/>
				    		</div>
				    		<div class="layout-cell cell-thirty-three"></div>
				    		<div class="layout-cell cell-thirty-three"></div>
				   	</div>
				</div>
		</fieldset>
		<div class="button-div">
		   	<input type="submit" class="save" value="<spring:message code="save"/>" />
            <input type="button" onclick="YAHOO.de.tarent.goToUrl('${backUrl}');" value="<spring:message code="cancel"/>" />
		</div>
	</form:form>
</div>
