<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<portlet:renderURL var="getAllJobsTable">
    <portlet:param name="ctx" value="getAllJobsTable"/>
</portlet:renderURL>


<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="projectId" value="${projectId}"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbValueSelectedProjects = "<a href=${getProjectDetails}>${projectName}</a>";
    var breadcrumbValueSelectedJob = "<a href=${getJobDetails}&jobId=${jobView.id}>${jobView.name}</a>";
    var breadcrumbValueAllJobs = "<a href=${getAllJobsTable}><spring:message code='jobs'/></a>"

    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' || ns == '_clientsportlet_WAR_activityportlets_' }">
        <c:choose>
            <c:when test="${not empty jobView.id}">
                YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedJob+" - <spring:message code='edit_job'/></h1>");
            </c:when>
            <c:otherwise>
                YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedJob+" - <spring:message code='new_job'/></h1>");
            </c:otherwise>
        </c:choose>
    </c:if>

    <c:if test="${ ns == '_jobsportlet_WAR_activityportlets_' }">
        <c:choose>
            <c:when test="${not empty jobView.id}">
                YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueAllJobs+" - <spring:message code='edit_job'/></h1>");
            </c:when>
            <c:otherwise>
                YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueAllJobs+" - <spring:message code='new_job'/></h1>");
            </c:otherwise>
        </c:choose>
    </c:if>

</script>
