<portlet:renderURL var="getJobDetails">
    <portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>

<portlet:renderURL var="getAllJobsTable">
    <portlet:param name="ctx" value="getAllJobsTable"/>
</portlet:renderURL>


<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
    <portlet:param name="projectId" value="${jobView.projectId}"/>
</portlet:renderURL>

<script type="text/javascript">

    var breadcrumbValueSelectedProjects = "<a href=${getProjectDetails}>${jobView.projectName}</a>";
    var breadcrumbValueAllJobs = "<a href=${getAllJobsTable}><spring:message code='jobs'/></a>"
    var breadcrumbValueSelectedJob = "<a href=${getJobDetails}&jobId=${jobView.id}>${jobView.name}</a>";

    <c:if test="${ ns == '_clientsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedProjects+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedJob+"</h1>");
    </c:if>

    <c:if test="${ ns == '_projectsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueSelectedProjects+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedJob+"</h1>");
    </c:if>

    <c:if test="${ ns == '_jobsportlet_WAR_activityportlets_' }">
        YAHOO.de.tarent.createBreadcrumb("<h1>"+breadcrumbValueAllJobs+"&nbsp;&raquo;&nbsp;" +breadcrumbValueSelectedJob+"</h1>");
    </c:if>

</script>
