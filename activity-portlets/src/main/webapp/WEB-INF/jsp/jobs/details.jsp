<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:renderURL var="doCancel"/>
<portlet:renderURL var="getJobChanges">
    <portlet:param name="ctx" value="jobChanges"/>
</portlet:renderURL>
<portlet:renderURL var="getProjectDetails">
    <portlet:param name="ctx" value="showProjectDetails"/>
</portlet:renderURL>

<%@ include file="/WEB-INF/jsp/jobs/breadcrumbs/breadcrumbs.jsp"%>

<div id="${ns}mainDiv" class="mainDiv">
	<h2><spring:message code="job"/>: <c:out value="${jobView.name}"/></h2>

	<div>
		<ul class="tabs marginBug">
			<portlet:renderURL var="getJobDetails">
				<portlet:param name="ctx" value="jobDetails"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:renderURL>
			<li class="current"><a href="${getJobDetails}"><spring:message code="details"/></a></li>
			<a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
				<portlet:renderURL var="getJobPositions">
					<portlet:param name="ctx" value="jobPositions"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobPositions}"><spring:message code="positions"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Activity.VIEW_BY_PROJECT, Activity.VIEW_ALL">
				<portlet:renderURL var="getJobActivities">
					<portlet:param name="ctx" value="jobActivities"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobActivities}"><spring:message code="activities"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL, Invoice.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobInvoices">
					<portlet:param name="ctx" value="jobInvoices"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobInvoices}"><spring:message code="invoices"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Cost.VIEW_ALL, Cost.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobCosts">
					<portlet:param name="ctx" value="jobCosts"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobCosts}"><spring:message code="costs"/></a></li>
			</a:authorize>
		</ul>
	</div>

	<a:authorize ifAnyGranted="">
		<div class="button-div-top">
		<a:authorize ifAnyGranted="Position.ADD">
			<portlet:renderURL var="getPositionAdd">
				<portlet:param name="ctx" value="getPositionAdd"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
                <portlet:param name="jobName" value="${jobView.name}"/>
                <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
			</portlet:renderURL>
			<input type="button" value="<spring:message code="new_position"/>" onClick="${ns}loadUrl('${getPositionAdd}'); return false;" />
		</a:authorize>
		<a:authorize ifAnyGranted="Job.EDIT, Job.EDIT_ALL">
			<portlet:renderURL var="getJobEdit">
				<portlet:param name="ctx" value="jobUpdate"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
				<portlet:param name="redirect" value="${redirect}"/>
                <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
			</portlet:renderURL>
			<input type="button" value="<spring:message code="edit_job"/>" onClick="${ns}loadUrl('${getJobEdit}'); return false;" />
            <input type="submit" style="width: auto;" value="<spring:message code="changes"/>" onclick="${ns}loadJobChangesUrl(); return false;" />
		</a:authorize>
		<a:authorize ifAnyGranted="Job.ADD, Job.ADD_ALL">
			<portlet:actionURL var="doJobCopy" name="doJobCopy">
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:actionURL>
			<input type="button" value="<spring:message code="copy_job"/>" onClick="${ns}loadUrl('${doJobCopy}'); return false;" />
		</a:authorize>
		<a:authorize ifAnyGranted="Job.DELETE, Job.DELETE_ALL">
			<portlet:actionURL var="doJobDelete" name="doJobDelete">
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:actionURL>
	   		<input type="submit" value="<spring:message code="delete"/>" onclick="${ns}deleteConfirmation('${doJobDelete}'); return false;" <c:if test="${! isDeletableJob}">disabled="disabled"</c:if> />
	   	</a:authorize>
		</div>
    </a:authorize>

	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="id"/>:</span>
					<c:out value="${jobView.id}"/>
			</div>
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="project_manager"/>:</span>
					<c:out value="${jobView.managerName}"/>
			</div>
		</div>
		<div class="layout-row">
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="type"/>:</span>
					<c:out value="${jobView.jobTypeName}"/>
			</div>
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="project"/>:</span>
					<c:choose>
						<c:when test="${not empty publicProjectIdLink}">
							<a href="${publicProjectIdLink}"><c:out value="${jobView.projectName}"/></a>
						</c:when>
						<c:otherwise>
							<a href="${getProjectDetails}"><c:out value="${jobView.projectName}"/></a>
						</c:otherwise>
					</c:choose>
			</div>
		</div>

		<div class="layout-row">
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="status"/>:</span>
					<c:out value="${jobView.jobStatusName}"/>
			</div>
			<div class="layout-cell cell-fifty details">
			</div>
		</div>
		<div class="layout-row">
			<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="job_number"/>:</span>
					<c:out value="${jobView.nr}"></c:out>
			</div>
			<div class="layout-cell cell-fifty details">
			</div>
		</div>
		</div>

		<div class="layout-table top-margin">
			<div class="layout-row">
				<div class="layout-cell details">
					<span class="info" style="display:block;"><spring:message code="description"/>:</span>
					<c:out value="${jobView.description}" escapeXml="false"/>
				</div>
			</div>
		</div>

		<div class="layout-table auto-width">
			<div class="layout-row auto-width">
				<div class="layout-cell">
					<c:if test="${jobView.offer != ''}">
						<a href="${jobView.offer}"><spring:message code="offer_dms"/></a>
					</c:if>
				</div>
				<div class="layout-cell">
					<c:if test="${jobView.request != ''}">
						<a href="${jobView.request}"><spring:message code="order_dms"/></a>
					</c:if>
				</div>
                <div class="layout-cell">
                    <c:if test="${jobView.crm != ''}">
                        <a href="${jobView.crm}"><spring:message code="crm"/></a>
                    </c:if>
                </div>
			</div>
		</div>
	<a:authorize ifAnyGranted="Cost.VIEW_ALL">
	<h3 class="top-margin"><spring:message code="cost_category"/></h3>
		<div>
			<spring:message code="cost_category_info"/>
		</div>

		<div class="layout-table top-margin">
			<div class="layout-row">
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="estimated_time"/></span>
					<spring:bind path="jobStats.expectedWork">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="estimated_time_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="job_total_sum"/></span>
					<spring:bind path="jobStats.fixedPrice">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="job_total_sum_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
			</div>

			<div class="layout-row">
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="actual_cost"/></span>
					<spring:bind path="jobStats.realWork">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="actual_cost_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="service_cost"/></span>
					<spring:bind path="jobStats.serviceCosts">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="service_cost_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
			</div>

			<div class="layout-row">
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="communicated_effort"/></span>
					<spring:bind path="jobStats.communicatedWork">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="communicated_effort_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
				<div class="layout-cell cell-fifty details">
					<span class="info"><spring:message code="other_cost"/></span>
					<spring:bind path="jobStats.otherCosts">
					<!-- argument seperator must be something else than ',', because else numbers with a ',' will be treaded as two arguments -->
						<spring:message code="other_cost_value" arguments="${status.displayValue}" argumentSeparator="|"/>
					</spring:bind>
				</div>
			</div>
		</div>
	</a:authorize>
	<a:authorize ifAnyGranted="Invoice.VIEW_ALL, Invoice.VIEW_BY_PROJECT">
	<h3 class="top-margin"><spring:message code="invoice_summary"/></h3>
	<div class="layout-table">
		<div class="layout-row">
			<div class="layout-cell cell-thirty-three">
				<span class="info"><spring:message code="expected_invoice"/></span><br />
				<fmt:formatDate value="${jobView.expectedBilling}" pattern="${dateformat}"/>
			</div>

			<div class="layout-cell cell-thirty-three">
				<span class="info"><spring:message code="full_invoice"/></span><br />
				<fmt:formatDate value="${jobView.payedDate}" pattern="${dateformat}"/>
			</div>

			<div class="layout-cell cell-thirty-three">
				<span class="info"><spring:message code="total_sum_of_invoices"/></span><br />
				<!-- we use two argumen in the following spring:message tag, but can only bind to one value, therefore
				  we set a variable to get the binded value before getting the second binded value -->
				<spring:bind path="jobStats.invoicePaidTotal" >
					<c:set var="invoicePaidTotalValue" value="${status.displayValue}" />
				</spring:bind>
				<spring:bind path="jobStats.invoiceTotal" >
					<spring:message code="total_sum_of_invoices_value" arguments="${status.displayValue}|${invoicePaidTotalValue}" argumentSeparator="|"/>
				</spring:bind>
			</div>
		</div>
 	</div>
	 </a:authorize>
		<div class="button-div">
            <input type="button" onclick="${ns}loadBackUrl(); return false;" value="<spring:message code="back_to_main_mask"/>" />
		</div>
</div>
<script type="text/javascript">
    function ${ns}deleteConfirmation(url) {
        var answer = confirm("<spring:message code='confirm_delete'/>");
        if (answer){
//             jQuery.ajax({
//                 url: url
//             });
        	this.document.location.href = url;
        }
    }
    function ${ns}loadUrl(url) {
        this.document.location.href = url;
    }
    function ${ns}loadBackUrl() {
        this.document.location.href = '${doCancel}';
    };
    function ${ns}loadJobChangesUrl() {
        this.document.location.href = '${getJobChanges}&jobId=${jobView.id}';
    };
</script>
