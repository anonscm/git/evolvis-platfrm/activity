<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="getJobTable" id="getJobTable"/>
<portlet:renderURL var="getJobDetails">
	<portlet:param name="ctx" value="jobDetails"/>
</portlet:renderURL>
<portlet:renderURL var="getAllJobsTable">
    <portlet:param name="ctx" value="getAllJobsTable"/>
</portlet:renderURL>
<portlet:renderURL var="getJobAdd">
    <portlet:param name="ctx" value="jobAdd"/>
    <portlet:param name="backUrl" value="${getAllJobsTable}"/>
</portlet:renderURL>
<portlet:renderURL var="getJobEdit">
    <portlet:param name="ctx" value="jobUpdate"/>
    <portlet:param name="backUrl" value="${getAllJobsTable}"/>
</portlet:renderURL>
<script type="text/javascript">
	var breadcrumbValue = "<h1><a href=${getAllJobsTable}><spring:message code='jobs'/></a></h1>"
	YAHOO.de.tarent.createBreadcrumb(breadcrumbValue);

</script>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
    <h2><spring:message code="jobs"/></h2>
    <div class="layout-table" style="width:auto;">
   		<div class="layout-row" style="width:auto;">				    		
			<div class="layout-cell cell-twenty-five">	
				<label for="${ns}form-project" ><spring:message code="project"/></label>
			    <select id="${ns}form-project" name="project" onchange="${ns}filterByProject(this.value);">
			    	<option value=""><spring:message code="all"/></option>
			    	<c:forEach items="${availableProjects}" var="project">
			    		<option value="${project.pk}"><c:out value="${project.name}"></c:out></option>
			    	</c:forEach>
			    </select>
	    	</div>
	    		
			<div class="layout-cell cell-twenty-five">
				<label for="${ns}form-status" ><spring:message code="status"/></label>
	    		<select id="${ns}form-status" name="status" onchange="${ns}filterByJobStatus(this.value);">
	    			<option value="" selected="selected"><spring:message code="all"/></option>
   					<c:forEach items="${jobStatuses}" var="jobStatus">
	    				<option value="${jobStatus.pk}"><c:out value="${jobStatus.name}"></c:out></option>
	    			</c:forEach>
	    		</select>	
			</div>			
			<div class="layout-cell cell-twenty-five">
	    		<label for="${ns}form-type" ><spring:message code="type"/></label>
			    <select id="${ns}form-type" name="type" onchange="${ns}filterByJobType(this.value);">
			    	<option value=""><spring:message code="all"/></option>
			    	<c:forEach items="${jobTypes}" var="jobType">
			    		<option value="${jobType.pk}"><c:out value="${jobType.name}"></c:out></option>
			    	</c:forEach>
			    </select>
			</div>
		</div>
   </div>
	   
   <div class="button-div">
   <a:authorize ifAnyGranted="Job.ADD">
       <input type="button" id="addUserButton" onclick="${ns}callNewJobLink();" value="<spring:message code="new_job"/>" />
    </a:authorize>
	</div> 
   
	<div id="${ns}form-jobs" class="clearDiv"></div>
</div>  

<script type="text/javascript">

    function ${ns}callNewJobLink() {
        this.document.location.href = '${getJobAdd}';
    };

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"${getJobEdit}&jobId="+oData+"\">&nbsp;</a>";
	};
	
	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails= function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"${getJobDetails}&jobId="+oData+"\">&nbsp;</a>";
	};


	//javascript filter object
	YAHOO.de.tarent.${ns}filter = {
			jobIdStatus :'',
			jobIdType:'',
			projectId: ${(not empty projectParam) ? projectParam : "''"}
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",className: "smallColumn",label :'<spring:message code="id"/>',sortable :true},
	                    {key :"name",label :'<spring:message code="job"/>',sortable :true},
	                    {key :"projectName", label : '<spring:message code="project"/>', sortable :true},
	                    {key :"jobStatusName",className: "smallColumn",label :'<spring:message code="status"/>',sortable :true}, 
	 					{key :"jobTypeName",className: "smallColumn",label :'<spring:message code="type"/>',sortable :true}, 
	 					<a:authorize ifAnyGranted="Job.EDIT, Job.EDIT_ALL">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
	 						{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
	                {key :"idTable",parser :"number"},
	                {key :"projectName"},
	                {key :"jobStatusName"},
					{key :"jobTypeName"},
					{key :"name"}];


    //the datatable
    /*the datatable*/
	var ${ns}limit = ${config.pageItems};
	var ${ns}offset = ${config.offset};
	var ${ns}sort = "${config.sortColumn}";
	var ${ns}dir = "${config.sortOrder}";
    
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTableWithPagingConfig("${ns}form-jobs","${getJobTable}",  ${ns}dsFields,
            ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter, null, "date", "desc",  ${ns}limit, ${ns}offset, ${ns}sort, ${ns}dir, true);	
	
	
	//job status filter
	var ${ns}filterByJobStatus = function(jobStatus){
		YAHOO.de.tarent.${ns}filter.jobIdStatus = jobStatus;
		${ns}myDataTable.reloadData();
	}
	
	//job type filter
	var ${ns}filterByJobType = function(jobType){
		YAHOO.de.tarent.${ns}filter.jobIdType = jobType;
		${ns}myDataTable.reloadData();
	}
	
	//job project filter
	var ${ns}filterByProject = function(projectId){
		YAHOO.de.tarent.${ns}filter.projectId = projectId;
		${ns}myDataTable.reloadData();
	}
	
</script>	
