<portlet:resourceURL var="getPositionsByJob" id="getPositionsByJob"/>

<%-- resource urls for drop-down lists (CommonsController) --%>
<portlet:resourceURL var="getResourcesByPosition" id="getResourcesByPosition" />
<portlet:resourceURL var="getJobsByProject" id="getJobsByProject" />
<portlet:resourceURL var="getPositionsByJob" id="getPositionsByJob" />

<script type="text/javascript">
    /** Load resources drop-down list. */
    jQuery('#${ns}-select-resourceId').attr('disabled','disabled');

    var ${ns}ajax_get_resources_by_position= function(positionID){
        jQuery('#${ns}-select-resourceId').removeAttr('disabled');
        var resourcesSelectLink = '${getResourcesByPosition}&selectedPositionId=' + positionID;
        jQuery('#${ns}-select-resourceId').load(resourcesSelectLink);
        return false;
    };

    /** Filter activities by position and reload data table. */
    var ${ns}filterByPosition = function(position){
        YAHOO.de.tarent.${ns}filter.selectedPosition = position;
        YAHOO.de.tarent.${ns}filter.selectedResource = '';
        ${ns}myDataTable.reloadData();
    };

    /** Filter activities by resource and reload data table. */
    var ${ns}filterByResource = function(resource){
        YAHOO.de.tarent.${ns}filter.selectedResource = resource;
        ${ns}myDataTable.reloadData();
        jQuery('#${ns}-export').removeAttr("disabled");
    };

</script>

<div class="layout-table">
    <div class="layout-row">
        <div class="layout-cell cell-fifty">
            <a:filterSelectPosition ns="${ns}" positionsList="${positionsList}"
                                    onchangeFunction="${ns}ajax_get_resources_by_position(this.value); ${ns}filterByPosition(this.value);" />
        </div>
        <div class="layout-cell cell-fifty">
            <a:filterSelectResource ns="${ns}" resourceList="${resourcesList}"
                                    onchangeFunction="${ns}filterByResource(this.value)" focus="true" />
        </div>
    </div>
    <div class="layout-row">
		<a:filterInputDateFromTo ns="${ns}" formId="${ns}form" name="startDate" dataTable="${ns}myDataTable"/>
        <div class="layout-cell"></div>
    </div>
</div>