<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/jobs/breadcrumbs/breadcrumbs.jsp"%>

<portlet:renderURL var="getInvoiceEdit">
    <portlet:param name="ctx" value="invoiceUpdate"/>
    <portlet:param name="projectId" value="${jobView.projectId}"/>
    <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceDetails">
    <portlet:param name="ctx" value="getProjectInvoiceDetails"/>
    <portlet:param name="projectId" value="${jobView.projectId}"/>
    <portlet:param name="backUrl" value="${getJobDetails}&jobId=${jobView.id}"/>
</portlet:renderURL>
<portlet:resourceURL var="getInvoicesTable" id="getInvoiceTable"/>
<portlet:renderURL var="doCancel"/>
<portlet:renderURL var="getJobInvoices">
    <portlet:param name="ctx" value="jobInvoices"/>
    <portlet:param name="jobId" value="${jobView.id}"/>
</portlet:renderURL>
<portlet:renderURL var="getInvoiceAdd">
    <portlet:param name="ctx" value="invoiceUpdate"/>
    <portlet:param name="mode" value="addInvoice"/>
    <portlet:param name="jobId" value="${jobView.id}"/>
    <portlet:param name="projectId" value="${jobView.projectId}"/>
    <portlet:param name="backUrl" value="${getJobInvoices}"/>
</portlet:renderURL>

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam">
	<h2><spring:message code="job"/>: <c:out value="${jobView.name}"/></h2>

	<div>
		<ul class="tabs marginBug">
			<portlet:renderURL var="getJobDetails">
				<portlet:param name="ctx" value="jobDetails"/>
				<portlet:param name="jobId" value="${jobView.id}"/>
			</portlet:renderURL>
			<li><a href="${getJobDetails}"><spring:message code="details"/></a></li>
			<a:authorize ifAnyGranted="Position.VIEW, Position.VIEW_ALL">
				<portlet:renderURL var="getJobPositions">
					<portlet:param name="ctx" value="jobPositions"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobPositions}"><spring:message code="positions"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Activity.VIEW_BY_PROJECT, Activity.VIEW_ALL">
				<portlet:renderURL var="getJobActivities">
					<portlet:param name="ctx" value="jobActivities"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobActivities}"><spring:message code="activities"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Invoice.VIEW_ALL, Invoice.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobInvoices">
					<portlet:param name="ctx" value="jobInvoices"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li class="current"><a href="${getJobInvoices}"><spring:message code="invoices"/></a></li>
			</a:authorize>
			<a:authorize ifAnyGranted="Cost.VIEW_ALL, Cost.VIEW_BY_PROJECT">
				<portlet:renderURL var="getJobCosts">
					<portlet:param name="ctx" value="jobCosts"/>
					<portlet:param name="jobId" value="${jobView.id}"/>
				</portlet:renderURL>
				<li><a href="${getJobCosts}"><spring:message code="costs"/></a></li>
			</a:authorize>
		</ul>
	</div>

	<a:authorize ifAnyGranted="Invoice.EDIT_ALL">
	    <div class="button-div-top">
	        <input type="button" onclick="YAHOO.de.tarent.goToUrl('${getInvoiceAdd}'); return false;" value="<spring:message code="new_invoice"/>" >
	    </div>
    </a:authorize>
	
	<div id="${ns}form-bills" class="clearDiv"></div>
	
	<div class="button-div">
        <input type="button" onclick="${ns}loadBackUrl(); return false;" value="<spring:message code="back_to_main_mask"/>" />
	</div>
</div>

<script type="text/javascript">
	//custom formatter for table edit column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceEdit}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
	};
	
	//custom formatter for table details column
	YAHOO.widget.DataTable.Formatter.${ns}formatDetails = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"YAHOO.de.tarent.goToUrl('${getInvoiceDetails}&invoicePk="+oData+"');return false;\">&nbsp;</a>";
	}
	
	/** custom formatter for amount column */
	YAHOO.widget.DataTable.Formatter.${ns}formatAmount = function(elLiner, oRecord, oColumn, oData) {
    	numeral.language('de');
    	elLiner.innerHTML = numeral(oData).format('0,0.00 $');
	};
	
	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"idTable",className: "smallColumn",label :'<spring:message code="id"/>',sortable :true},
	                    {key :"customerName",label :'<spring:message code="customer"/>',sortable :true},
	                    {key :"projectName",label :'<spring:message code="project"/>',sortable :true},
	            		{key :"jobName",label :'<spring:message code="job"/>',sortable :true}, 
	 					{key :"name",label :'<spring:message code="name"/>',sortable :true}, 
	 					{key :"number",label :'<spring:message code="invoice_number"/>',sortable :true},
	 					{key :"amount",label :'<spring:message code="amount"/>', formatter:"${ns}formatAmount", sortable :true},
	 					{key :"invoiceDate",label :'<spring:message code="invoice_date"/>', formatter:"mdyDate", sortable :true},
	 					{key :"payDate",label :'<spring:message code="invoice_pay_date"/>', formatter:"mdyDate", sortable :true},
	 					<a:authorize ifAnyGranted="Invoice.EDIT_ALL">
	 						{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					</a:authorize>
	 					{key: "id",className: "detailsColumn", label: "&nbsp;", formatter: "${ns}formatDetails", sortable: false}];

	//datasource column definitions
	var ${ns}dsFields = [{key :"id", parser : "number"},
	                {key :"idTable", parser : "number"},
	                {key : "customerName"},
	                {key : "projectName"},
	                {key : "jobName"},
					{key : "name"},
					{key : "number"},
					{key : "amount", parser : "number"},
					{key : "invoiceDate", parser : "date"},
					{key : "payDate", parser : "date"}];
	
	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-bills","${getInvoicesTable}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);

    function ${ns}loadBackUrl() {
        this.document.location.href = '${doCancel}';
    };
</script>
