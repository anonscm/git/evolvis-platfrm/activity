<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doSave" id="doSave"/>

<div id="${ns}mainDiv" class="mainDiv" >
    <form:form method="POST" id="${ns}form" action="${doSave}">
	    <div class="layout-table">
	    	<div class="layout-cell">
			   	<fieldset>
					<legend><spring:message code="place_of_resource"/></legend>
					<div class="label_details">
						<c:forEach var="loc" items="${locationList}">
							<input type="checkbox" name="location" value="${loc.pk}"
								<c:if test="${fn:contains(locationSelect,loc.pk)}">
									checked="checked" 
								</c:if> 
							><label>${loc.name}</label><br>
						</c:forEach>
					</div>
					<input type="submit" value="<spring:message code="save"/>">
	    		</fieldset>
			</div>
		</div> 
	</form:form>
</div>  


