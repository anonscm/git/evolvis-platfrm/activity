<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="doShowAll" id="doShowAll" >
	<portlet:param name="redirect" value="${view}"/>
</portlet:resourceURL>

<div id="${ns}mainDiv" class="mainDiv">
    <form:form method="POST" id="${ns}form" action="${doShowAll}">
	<h2>
		<spring:message code="absent_resources" />
	</h2>

	<c:choose>
		<c:when test="${fn:length(currentLossList)>0}">
			<c:forEach var="loss" items="${currentLossList}" varStatus="myStatus">
				<c:choose>
					<c:when test="${myStatus.first}">
						<div class="div_office_label">${loss.resourceByFkResource.branchOffice.name}</div>
					</c:when>
					<c:otherwise>
						<c:if
							test="${(currentLossList[myStatus.index]).resourceByFkResource.branchOffice.pk != (currentLossList[myStatus.index -1]).resourceByFkResource.branchOffice.pk}"
						>
							<div class="div_office_label">${loss.resourceByFkResource.branchOffice.name}</div>
						</c:if>
					</c:otherwise>
				</c:choose>
				<div class="strong">
				<c:out value="${loss.resourceByFkResource.firstname}"></c:out>
				<c:out value="${loss.resourceByFkResource.lastname}"></c:out>
				</div>
				
				<div class="alignRight bottom-margin5">
				<spring:message code="to" />
				<fmt:formatDate value="${loss.endDate}" pattern="${dateformat}" />
				</div>
				
			</c:forEach>
		</c:when>
		<c:otherwise>
			<p>
				<spring:message code="no_absent_resources" />
			</p>
		</c:otherwise>
	</c:choose>
	<br />
    <input type="submit" <c:if test="${showAll eq 'all' }">disabled="disabled"</c:if> onclick="jQuery('#${ns}mainDiv').load('${doShowAll}');return false;" class="cancel" value="<spring:message code="all_branch_office"/>" />
	</form:form>
</div>

