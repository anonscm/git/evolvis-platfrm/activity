<%@ include file="/WEB-INF/jsp/include.jsp"%>


<portlet:actionURL var="doSave" name="doSaveMySkill"/>
<portlet:actionURL var="doCancel" name="doCancelSkillAction"/>

<div id="${ns}mainDiv" class="mainDiv">
<h2><spring:message code="add_skill"/></h2>
	<form:form method="POST" id="${ns}form" action="${doSave}" commandName="skillsView">

	 <c:if test="${exist eq 'exist'}">
   		<div class="portlet-msg-error"><spring:message code="validate_skill_exist"/></div>
  		 </c:if>
  			<form:hidden path="id"/>
		<div class="layout-table">
			<div class="layout-row">
	   			<div class="layout-cell cell-fifty" >
	   				<label for="skillsDefName"><spring:message code="skill"/>*:</label>
	   				<c:choose>
	   					<c:when test="${skillsView.id != null}">
	   						<input type="hidden" id="skillsDefId" name="skillsDefId" value="${skillsView.skillsDefId}"/>
	   						<input type="text" id="skillsDefName" name="skillsDefName" readonly="readonly" value="<c:out value="${skillsView.skillsDefName}"></c:out>"/>
	   					</c:when>
	   					<c:otherwise>
	   						<div id="${ns}form-inputSkill-div">
		   						<select name="skillsDefName" id="${ns}form-skillsDefId" onchange="${ns}getLabel(this);">
				   					<option value=""><spring:message code="select_please"/></option>
				   					<c:forEach var="skill" items="${skillsDef}">
				   						<option value="<c:out value="${skill.name}"></c:out>" 
				   							<c:choose>
												<c:when test="${skillsView.skillsDefId eq skill.pk }">
													selected="selected" 
												</c:when>
											</c:choose>
				   						><c:out value="${skill.name}"></c:out></option>
				   					</c:forEach>
				   					<option value="0" onclick="${ns}addSkillField();"><spring:message code="new_skill"/></option>
				   				</select>
			   				</div>
			   			</c:otherwise>
	   				</c:choose>
	   				<div class="portlet-msg-error yui-pe-content" id="${ns}form-skillsDefName-required">
	   				<spring:message code="validate_name_not_empty"/></div>
				    <form:errors path="skillsDefName" cssClass="portlet-msg-error" ></form:errors>
	   			</div>

		   		<div class="layout-cell cell-fifty">
			   		<label for="value"><spring:message code="classification"/>*:</label>
			   		 <select name="value" id="value" >
			   		 		<option value=""><spring:message code="select_please"/></option>
							<c:forEach var="option" items="${options}">
								<option value="${option.integerIndex}"
									<c:if test="${skillsView.value eq option.integerIndex}"> selected="selected"</c:if>
								><c:out value="${option.value}"></c:out></option>
							</c:forEach>
					</select>
					<div class="portlet-msg-error yui-pe-content" id="${ns}form-value-required"><spring:message code="select_skill_value"/></div>
		    		<form:errors path="value" cssClass="portlet-msg-error"></form:errors>
				</div>
			</div>
	</div>
	<div class="button-div">
	    <input type="submit" class="save" value="<spring:message code="save"/>" />
	    <input type="button" onclick="${ns}loadCancelUrl(); return false;" class="cancel" value="<spring:message code="cancel"/>" />
	</div>
	</form:form>
</div>

<script type="text/javascript">

	var ${ns}getLabel = function(elm)
	{
		 var num = elm.selectedIndex;
		 var skillsDefName = document.getElementById("skillsDefName").value = elm.options[num].text;
		
	}
	
	/**var ${ns}validatorDescriptor;

    if (!Modernizr.input.required){
    	${ns}validatorDescriptor=[{id:"skillsDefName", validator:"required"},
		            			{id:"value", validator:"required"}];
   }


    var ${ns}zaForm = jQuery("#${ns}form"); 
    ${ns}zaForm.bind("submit",function(event) {
		event.preventDefault();
		${ns}zaForm.attr('isFormValid',YAHOO.de.tarent.validate("${ns}form",${ns}validatorDescriptor));
	
		
	});*/

	function ${ns}addSkillField(){
		document.getElementById('${ns}form-inputSkill-div').innerHTML = '<input type="text" id="skillsDefName" name="skillsDefName"/>';
		document.getElementById('skillsDefName').focus();
	};

    function ${ns}loadCancelUrl() {
        this.document.location.href = '${doCancel}';
    };

</script>

<!-- Removed because it performs jquery tasks which causes problems with action and render urls
a:form divId="${ns}mainDiv" formId="${ns}form"/-->