<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:resourceURL var="table" id="showMySkillsTable"/>
<portlet:renderURL var="add">
    <portlet:param name="ctx" value="showMySkillsAddView"/>
</portlet:renderURL>
<portlet:renderURL var="edit">
    <portlet:param name="ctx" value="showMySkillsEditView"/>
</portlet:renderURL>
<portlet:actionURL var="doDelete" name="doDeleteMySkill" />

<div id="${ns}mainDiv" class="mainDiv yui-skin-sam" >
<h2><spring:message code="my_skills"/></h2>
   	<div id="${ns}form-skills" class="clearDiv"></div>

	<div class="button-div">
        <input type="button" onclick="${ns}loadAddUrl(); return false;" value="<spring:message code="add"/>" />
    </div>
</div>


<script type="text/javascript">

	//custom formatter for table delete column
	YAHOO.widget.DataTable.Formatter.${ns}formatEdit = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}loadUrl("+oData+");\" );\">&nbsp;</a>";
	};
	YAHOO.widget.DataTable.Formatter.${ns}formatDelete = function(elLiner, oRecord, oColumn, oData) {
		elLiner.innerHTML = "<a href=\"#\" onclick=\"${ns}deleteConfirmation("+oData+");\">&nbsp;</a>";
	};

	function ${ns}deleteConfirmation(oData) {
		var answer = confirm("<spring:message code='confirm_delete'/>");
		if (answer){
			this.document.location.href = '${doDelete}&skillPk='+oData;	
		}
	}
    function ${ns}loadUrl(oData) {
        this.document.location.href = '${edit}&skillPk='+oData;
    }
    function ${ns}loadAddUrl() {
        this.document.location.href = '${add}';
    };


	//datatable column definitions
	var ${ns}myColumnDefs = [{key :"skillsDefName",label :"<spring:message code='skill'/>",sortable :true}, 
	 					{key :"value",className: "smallColumn",label :"<spring:message code='classification'/>",sortable :true},
	 					{key :"id",className: "editColumn",label :"&nbsp;", formatter:"${ns}formatEdit", sortable :false},
	 					{key :"id",className: "deleteColumn",label :"&nbsp;", formatter:"${ns}formatDelete", sortable :false}];
		

	//datasource column definitions
	var ${ns}dsFields = [{key :"id",parser :"number"},
					{key :"skillsDefName"},
					{key :"value", parser :"number"}];

	//the datatable
	${ns}myDataTable = YAHOO.de.tarent.filteredDataTable("${ns}form-skills","${table}", ${ns}dsFields, ${ns}myColumnDefs, YAHOO.de.tarent.${ns}filter);
	
</script>	

