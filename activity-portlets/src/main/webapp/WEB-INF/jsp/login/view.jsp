<%@ include file="/WEB-INF/jsp/include.jsp"%>

<c:choose>
	<c:when test="${empty loggeduser}">
		<portlet:actionURL var="doAction" name="doLogin" />
	</c:when>
	<c:otherwise>
		<portlet:actionURL var="doAction" name="doLogout" />
	</c:otherwise>
</c:choose>

<div id="${ns}mainDiv" class="mainDiv">

	<h2>Login</h2>
		<form action="${doAction}" method="post" id="${ns}loginForm" novalidate="novalidate">
		<c:if test="${loggeduser == null }">
	
	     	<p><spring:message code="not_logged_in"/></p>

			<label for="field-username"><spring:message code="username"/>:</label>
			<input type="text" name="username"  class="required-field user" pattern="[A-Za-z][A-Za-z0-9]{1,32}" required="required" autofocus="autofocus"  id="field-username"/>
				    
			<label for="field-password"><spring:message code="password"/>:</label>
			<input type="password" name="password" class="required-field password" required="required" id="field-password"/>

			<span class="expected-format">
            	<spring:message code="expected_login_format" />
			</span>

			<div class="button-div">
				<input type="submit" class="login"  name="Login" id="login_btn" value="<spring:message code="login"/>"/>
			</div>
	
		</c:if>
		<c:if test="${loggeduser != null}">
			<input type="hidden" id="${ns}-form-redirectUrl" name="redirectUrl" value="<c:out value="${redirectUrl}"></c:out>">

			<h3><spring:message code="logged_in_as"/>: ${loggeduser.username}</h3>
			<div class="button-div">
				<input type="submit" class="logout" name="Logout" id="logout_btn" value="<spring:message code="logout"/>"/>
	    	</div>
		</c:if>
		</form>

</div>
