/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import de.tarent.activity.domain.JobStats;
import de.tarent.activity.domain.Position;
import de.tarent.activity.validator.constraints.Url;

public class PositionView implements Serializable {

    private static final long serialVersionUID = -3595068947031012461L;

    private Long id;
    
    private Long positionId;

	@NotEmpty(message = "{validate_position_name_not_empty}")
    private String positionName;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    @NotNull(message = "{validate_job_not_empty}")
    private Long jobId;

    private String jobName;

    @NotNull(message = "{validate_status_not_empty}")
    private Long positionStatusId;

    private String positionStatusName;
    
    private String positionStatus;

	@Min(value = 0, message = "{validate_digit_positive}")
    private BigDecimal expectedWork;

    @Min(value = 0, message = "{validate_digit_positive}")
    private BigDecimal communicatedWork;

    @NotNull(message = "{validate_price_not_empty}")
    @Min(value = 0, message = "{validate_digit_positive}")
    private BigDecimal fixedPrice;

    @Url(message = "{validate_http_url}")
    private String evolvisURL;

    @Min(value = 0, message = "{validate_evolvis_task_id}")
    private Long evolvisProjectId;

    @DateTimeFormat
    private Date beginDate;

    @DateTimeFormat
    private Date endDate;

    private String evolvis;

    private String projectName;

    private String customerName;
    
    private JobStats stats;

	public PositionView() {
    }

    public PositionView(Position p) {
    	this.id = p.getPk();
    	this.positionName = p.getName();
    	this.description = p.getNote();
    	this.jobId = p.getJob().getPk();
    	this.jobName = p.getJob().getName();
    	this.positionStatusId = p.getPositionStatus().getPk();
    	this.positionStatusName = p.getPositionStatus().getName();
    	this.expectedWork = p.getExpectedWork();
    	this.communicatedWork = p.getCommunicatedWork();
    	this.fixedPrice = p.getFixedPrice();
    	this.evolvisURL = p.getEvolvisurl();
    	this.evolvisProjectId = p.getEvolvisprojektid();
    	this.beginDate = p.getPositionStartDate();
    	this.endDate = p.getPositionEndDate();
    	this.evolvis = (p.getEvolvisprojektid() != null) ? p.getEvolvisurl()
                + "/" + p.getEvolvisprojektid() : p.getEvolvisurl();
        this.projectName = p.getJob().getProject().getName();
        this.jobName = p.getJob().getName();
        this.customerName = p.getJob().getProject().getCustomer().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Long getPositionStatusId() {
        return positionStatusId;
    }

    public void setPositionStatusId(Long positionStatusId) {
        this.positionStatusId = positionStatusId;
    }

    public String getPositionStatusName() {
        return positionStatusName;
    }

    public void setPositionStatusName(String positionStatusName) {
        this.positionStatusName = positionStatusName;
    }

    public BigDecimal getExpectedWork() {
        return expectedWork;
    }

    public void setExpectedWork(BigDecimal expectedWork) {
        this.expectedWork = expectedWork;
    }

    public BigDecimal getCommunicatedWork() {
        return communicatedWork;
    }

    public void setCommunicatedWork(BigDecimal communicatedWork) {
        this.communicatedWork = communicatedWork;
    }

    public String getEvolvisURL() {
        return evolvisURL;
    }

    public void setEvolvisURL(String evolvisURL) {
        this.evolvisURL = evolvisURL;
    }

    public BigDecimal getFixedPrice() {
        return fixedPrice;
    }

    public void setFixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public Long getEvolvisProjectId() {
        return evolvisProjectId;
    }

    public void setEvolvisProjectId(Long evolvisProjectId) {
        this.evolvisProjectId = evolvisProjectId;
    }

    public String getEvolvis() {
        return evolvis;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setEvolvis(String evolvis) {
        this.evolvis = evolvis;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    public String getPositionStatus() {
		return positionStatus;
	}

	public void setPositionStatus(String positionStatus) {
		this.positionStatus = positionStatus;
	}

    public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}
	
    public JobStats getStats() {
		return stats;
	}

	public void setStats(JobStats stats) {
		this.stats = stats;
	}

}
