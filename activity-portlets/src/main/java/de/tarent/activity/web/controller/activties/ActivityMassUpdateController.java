/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Position;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.web.controller.BaseActivityController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;

@Controller("ActivityMassUpdateController")
@RequestMapping(value = "view", params="ctx=activityMassUpdate")
public class ActivityMassUpdateController extends BaseActivityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityMassUpdateController.class);

    private static final String UNIQUE_MESSAGE_KEY = "validate_unique_constraint";

    @Autowired
    private ActivityService activityService;

    @RenderMapping
    public String view(Model model, PortletRequest request) {
        if(allActivities){
            return "activities/edit";
        }
        return "activities/viewMyActivities";
    }

    @ActionMapping("doMassRebook")
    public void doMassRebook(@RequestParam(value = "massUpdateSelect", required = true) String[] massUpdateSelect,
                             @RequestParam(value = "jobForMassUpdate", required = true) Long jobForMassUpdate,
                             @RequestParam(value = "positionForMassUpate", required = true) Long positionForMassUpate,
                             PortletRequest request, Model model) throws UniqueValidationException {
        for ( String activityId : massUpdateSelect){

            Activity activity = activityService.getActivity( new Long(activityId) );
            activity.setPosition(new Position(positionForMassUpate));

            try {
                activityService.updateActivity(activity);
            } catch (UniqueValidationException e) {
                LOGGER.error("Cannot save activity, unique constraint violation.");
                throw new UniqueValidationException( e.getConstraint() );
            }
        }
        request.setAttribute("successMessage", "saved");
    }

    @ActionMapping("doMassDelete")
    public void doMassDelete(@RequestParam(value = "massUpdateSelect", required = false) String[] massUpdateSelect,
                             Model model, ActionRequest request, ActionResponse response){
        if( massUpdateSelect != null ){
            for ( String activityPk : massUpdateSelect){
                activityService.deleteActivity( new Long(activityPk) );
            }
            model.addAttribute("successMessage", "deleted");
        }
    }
}
