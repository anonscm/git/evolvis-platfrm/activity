/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.util.Iterator;
import java.util.List;

import org.springframework.web.util.HtmlUtils;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceFTypeMapping;
import de.tarent.activity.domain.VresActiveProjectMapping;
import de.tarent.activity.web.util.RenderParametersUtil;

public class ResourceMappingView {

    private Long id;

    private String firstName;

    private String lastName;

    private String functions;

    private String activeProjects;

    private String inactiveProjects;

    private String plannedProjects;

    public ResourceMappingView() {
        super();
    }

    public ResourceMappingView(Resource resource) {
        this.id = resource.getPk();
        this.firstName = resource.getFirstname();
        this.lastName = resource.getLastname();

        StringBuffer sb = new StringBuffer();
        for (Iterator<ResourceFTypeMapping> iterator = resource.getResourceFTypeMappings().iterator(); iterator
                .hasNext();) {
            ResourceFTypeMapping item = iterator.next();
            sb.append(item.getFunctionType().getName());
            sb.append(iterator.hasNext() ? ", " : "");
        }
        this.functions = sb.toString();
    }

    public String getProjects(List<VresActiveProjectMapping> vres, String projectLink) {
        StringBuffer sb = new StringBuffer();
        for (Iterator<VresActiveProjectMapping> iterator = vres.iterator(); iterator.hasNext();) {
            VresActiveProjectMapping p = iterator.next();
            String link = RenderParametersUtil.getPublicPortletUrl(projectLink, "publicProjectId", p.getProject()
                    .getPk().toString());
            link = appendRenderParametersToLink(link, p.getProject().getPk().toString());
            if (link != null) {
                sb.append("<a href='");
                sb.append(link);
                sb.append("'>");
                sb.append(HtmlUtils.htmlEscape(p.getProject().getName()));
                sb.append("</a>");
            } else {
                sb.append(HtmlUtils.htmlEscape(p.getProject().getName()));
            }
            sb.append(iterator.hasNext() ? ", " : "");
        }
        return sb.toString();
    }

    private String appendRenderParametersToLink(String link, String projectId) {
        link = link+"&ctx=showProjectDetails&projectId="+projectId;
        return link;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFunctions() {
        return functions;
    }

    public void setFunctions(String functions) {
        this.functions = functions;
    }

    public String getActiveProjects() {
        return activeProjects;
    }

    public void setActiveProjects(String activeProjects) {
        this.activeProjects = activeProjects;
    }

    public String getInactiveProjects() {
        return inactiveProjects;
    }

    public void setInactiveProjects(String inactiveProjects) {
        this.inactiveProjects = inactiveProjects;
    }

    public String getPlannedProjects() {
        return plannedProjects;
    }

    public void setPlannedProjects(String plannedProjects) {
        this.plannedProjects = plannedProjects;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
