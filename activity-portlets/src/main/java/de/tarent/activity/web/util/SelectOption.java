/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

/**
 * Reusable helper class for YUI selectbox.
 * 
 */
public class SelectOption {
    private Long index;
    private Integer integerIndex;
    private String text;
    private String value;

    /**
     * Default constructor using all fields.
     * 
     * @param value
     *            String, the values of the select-box option
     * @param text
     *            String, the name of the option
     */
    public SelectOption(String value, String text) {
        this.text = text;
        this.value = value;
    }

    public SelectOption(Long index, String value) {
        this.index = index;
        this.value = value;
    }

    public SelectOption(Integer integerIndex, String value) {
        this.integerIndex = integerIndex;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public Integer getIntegerIndex() {
        return integerIndex;
    }

    public void setIntegerIndex(Integer integerIndex) {
        this.integerIndex = integerIndex;
    }

}
