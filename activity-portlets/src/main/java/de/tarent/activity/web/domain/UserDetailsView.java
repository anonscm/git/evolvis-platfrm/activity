/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Arrays;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Resource;

public class UserDetailsView extends UserView {

    private static final long serialVersionUID = -299684728741962800L;

    private Long idTable;

    //@NotNull(message = "{validate_password_length}")
    //@Length(min = 6, message = "{validate_password_length}")
    private String password;

    //@NotNull(message = "{validate_password_length}")
    //@Length(min = 6, message = "{validate_password_length}")
    private String passwordRepeat;

    @NotNull(message = "{validate_type_not_empty}")
    private Long resourceTypeId;

    private String resourceTypeName;

    @NotNull(message = "{validate_branch_office_not_empty}")
    private Long branchOfficeId;

    private String branchOfficeName;

    @NotNull(message = "{validate_contract_type_not_empty}")
    private Long employmentId;

    private String employmentName;

    @DateTimeFormat
    private Date joinDate;

    @DateTimeFormat
    private Date leavingDate;

    @NotNull(message = "{validate_holidays_not_empty}")
    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private BigDecimal holidays;

    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private BigDecimal salary;

    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private BigDecimal cost;

    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private Long availableHours;

    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private BigDecimal remainingDays;

    @NumberFormat
    @Range(min = 0, message = "{validate_digit_positive}")
    private BigDecimal overtimeHours;

    private Long[] functiontype;

    private String groups;

    public UserDetailsView() {

    }

    public UserDetailsView(Long id, String firstName, String lastName, String description, String username,
            String mail, Long resourceTypeId, String resourceTypeName, Long branchOfficeId, String branchOfficeName,
            Date birthDate, String active, Long employmentId, String employmentName, Date joinDate, Date leavingDate,
            BigDecimal holidays, BigDecimal salary, BigDecimal cost, Long availableHours, BigDecimal remainingDays) {
        super(id, firstName, lastName, username, mail, birthDate, null, description, active);
        this.resourceTypeId = resourceTypeId;
        this.resourceTypeName = resourceTypeName;
        this.branchOfficeId = branchOfficeId;
        this.branchOfficeName = branchOfficeName;
        this.employmentId = employmentId;
        this.employmentName = employmentName;
        this.joinDate = joinDate;
        this.leavingDate = leavingDate;
        this.holidays = holidays;
        this.salary = salary;
        this.cost = cost;
        this.availableHours = availableHours;
        this.remainingDays = remainingDays;
    }

    public UserDetailsView(Resource r) {
        this(r.getPk(), r.getFirstname(), r.getLastname(), r.getNote(), r.getUsername(), r.getMail(), r
                .getResourceType() == null ? null : r.getResourceType().getPk(), r.getResourceType() == null ? null : r
                .getResourceType().getName(), r.getBranchOffice() == null ? null : r.getBranchOffice().getPk(), r
                .getBranchOffice() == null ? null : r.getBranchOffice().getName(), r.getBirth(), String.valueOf(r
                .getActive()), r.getEmployment() == null ? null : r.getEmployment().getPk(),
                r.getEmployment() == null ? null : r.getEmployment().getName(), r.getEntered(), r.getExit(), r
                        .getHoliday(), r.getSalery(), r.getCostPerHour(), r.getAvailableHours(), r.getRemainHoliday());

    }

    public Long getResourceTypeId() {
        return resourceTypeId;
    }

    public void setResourceTypeId(Long resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }

    public String getResourceTypeName() {
        return resourceTypeName;
    }

    public void setResourceTypeName(String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }

    public Long getBranchOfficeId() {
        return branchOfficeId;
    }

    public void setBranchOfficeId(Long branchOfficeId) {
        this.branchOfficeId = branchOfficeId;
    }

    public String getBranchOfficeName() {
        return branchOfficeName;
    }

    public void setBranchOfficeName(String branchOfficeName) {
        this.branchOfficeName = branchOfficeName;
    }

    public Long getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(Long employmentId) {
        this.employmentId = employmentId;
    }

    public String getEmploymentName() {
        return employmentName;
    }

    public void setEmploymentName(String employmentName) {
        this.employmentName = employmentName;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Date getLeavingDate() {
        return leavingDate;
    }

    public void setLeavingDate(Date leavingDate) {
        this.leavingDate = leavingDate;
    }

    public BigDecimal getHolidays() {
        return holidays;
    }

    public void setHolidays(BigDecimal holidays) {
        this.holidays = holidays;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Long getAvailableHours() {
        return availableHours;
    }

    public void setAvailableHours(Long availableHours) {
        this.availableHours = availableHours;
    }

    public BigDecimal getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(BigDecimal remainingDays) {
        this.remainingDays = remainingDays;
    }

    public BigDecimal getOvertimeHours() {
        return overtimeHours;
    }

    public void setOvertimeHours(BigDecimal overtimeHours) {
        this.overtimeHours = overtimeHours;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public Long[] getFunctiontype() {
        return functiontype;
    }

    public void setFunctiontype(Long[] functiontype) {
        this.functiontype = functiontype != null ? Arrays.copyOf(functiontype, functiontype.length) : null;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

}
