/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import de.tarent.activity.domain.LdapUser;
import de.tarent.activity.domain.Resource;

public class UserExtendedView extends UserDetailsView implements Serializable {

    private static final long serialVersionUID = 5735632903097539939L;

    private LdapUser ldapUser;

    public LdapUser getLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(LdapUser ldapUser) {
        this.ldapUser = ldapUser;
    }

    public UserExtendedView(Resource r, LdapUser l) {
        this(r.getPk(), r.getUsername(), r.getFirstname(), r.getLastname(), r.getGroups(), String
                .valueOf(r.getActive()), r.getBranchOffice() != null ? r.getBranchOffice().getPk() : new Long(0), r
                .getEmployment() != null ? r.getEmployment().getPk() : new Long(0), r.getResourceType() != null ? r
                .getResourceType().getPk() : new Long(0), r.getHoliday() != null ? r.getHoliday() : new BigDecimal(0),
                r.getRemainHoliday() != null ? r.getRemainHoliday() : new BigDecimal(0), l);
    }

    public UserExtendedView(Long id, String userName, String firstName, String lastName, String groups, String active,
            Long branchOfficeId, Long employmentId, Long resourceTypeId, BigDecimal holidays, BigDecimal remainingDays,
            LdapUser ldapUser) {
        this.setId(id);
        this.setUsername(userName);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGroups(groups);
        this.setActive(active);
        this.setBranchOfficeId(branchOfficeId);
        this.setEmploymentId(employmentId);
        this.setResourceTypeId(resourceTypeId);
        this.setHolidays(holidays);
        this.setRemainingDays(remainingDays);
        this.setLdapUser(ldapUser);
    }
}
