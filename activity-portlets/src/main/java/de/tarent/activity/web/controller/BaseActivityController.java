/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Position;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.util.RequestUtil;


public abstract class BaseActivityController extends BaseController {

    protected Boolean allActivities = true;
    
    public static final Long STATUS_ID_IN_ARBEIT = 2L;

    @Autowired
    private ProjectService projectService;

    /**
     * May be set in spring context file.
     */
    public void setAllActivities(Boolean allActivities) {
        this.allActivities = allActivities;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    @ResourceMapping("positions")
    public String positions(Long jobId, Boolean isFilter, ResourceRequest request, Model model) {

        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, retrievePositionsList(jobId, request));
        model.addAttribute("isFilter", isFilter);

        return "common/positions";
    }

    /**
     * This method gives a position list by a jobId and current logged user.
     *
     * @param jobId
     *            Job ID
     * @param request
     *            PortletRequest
     * @return List<Position>
     */
    public List<Position> retrievePositionsList(Long jobId, PortletRequest request) {
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        return getProjectService().getPositionsByJob(jobId, STATUS_ID_IN_ARBEIT, resPk);
    }
}
