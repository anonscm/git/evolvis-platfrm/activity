/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;

import de.tarent.activity.domain.Skills;

/**
 * AllSkillsView.
 * 
 */
public class AllSkillsView implements Serializable {

    private static final long serialVersionUID = 7818159854074400595L;

    private String resourceName;

    private String skillsDefName;

    private Integer value;

    /**
     * Constructor.
     */
    public AllSkillsView() {

    }

    /**
     * Constructor.
     * 
     * @param resourceName
     *            resourceName
     * @param skillsDefName
     *            skillsDefName
     * @param value
     *            value
     */
    public AllSkillsView(String resourceName, String skillsDefName, Integer value) {
        this.resourceName = resourceName;
        this.skillsDefName = skillsDefName;
        this.value = value;
    }

    /**
     * Constructor.
     * 
     * @param skill
     *            skill
     */
    public AllSkillsView(Skills skill) {
        this(skill.getResource().getFirstname() + " " + skill.getResource().getLastname(), skill.getSkillsDef()
                .getName(), skill.getValue());
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getSkillsDefName() {
        return skillsDefName;
    }

    public void setSkillsDefName(String skillsDefName) {
        this.skillsDefName = skillsDefName;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
