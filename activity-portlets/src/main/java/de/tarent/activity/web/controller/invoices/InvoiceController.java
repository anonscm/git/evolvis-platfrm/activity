/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.invoices;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Project;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * InvoiceController.
 *
 */
@Controller(value = "InvoiceController")
@RequestMapping(value = "view")
public class InvoiceController extends BaseController {

	public static final String PAGING_CONFIG = "pagingConfigInvoices";
	
    @Autowired
    private ProjectService projectService;

    @ModelAttribute("customerList")
    public List<Customer> populateCustomerList(){
        return projectService.getCustomerList();
    }

    @ResourceMapping("getProjects")
    public String getProjectsList(@RequestParam(value = "customerId", required = false) Long  customerId, Model model){

        List<Project> projectsList = null;
        if( customerId != null){
            projectsList = projectService.getProjectsByCustomer(customerId);
        }else{
            projectsList = projectService.getProjectList();
        }

        model.addAttribute("projectList", projectsList);
        return "common/projects";
    }
    
    @ResourceMapping("getJobs")
    public String getJobList(@RequestParam(value = "customerId") Long  customerId, 
    		@RequestParam(value="projectId", required = false) Long projectId, Model model){

    	List<Project> projectList = null;
        List<Job> jobList = new ArrayList<Job>();
        
        if(projectId != null && projectId != -1){
        	jobList = projectService.getJobsByProject(projectId);
        	model.addAttribute("jobsList", jobList);
            return "common/jobs";
        } else {
        	projectList = projectService.getProjectsByCustomer(customerId);
        	for(Project pro : projectList){
        		jobList.addAll(projectService.getJobsByProject(pro.getPk()));
        	}
        	model.addAttribute("jobsList", jobList);
            return "common/projectsJobs";
        }
    }
    
    

    /**
     * Default view.
     */
    @RenderMapping
    public String view(Model model, PortletRequest request) {
        return ajaxView(model, request);
    }

    @RenderMapping(params = "ctx=getAllInvoices")
    public String getAllInvoices(Model model, PortletRequest request) {
    	return view(model, request);
    }

    /**
     * Default ajax view.
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        return "invoices/view";
    }

    @ResourceMapping("getInvoiceCancel")
    public String cancel(Model model, PortletRequest request) {
        return ajaxView(model, request);
    }

}
