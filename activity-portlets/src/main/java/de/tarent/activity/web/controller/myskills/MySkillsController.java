/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myskills;

import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.SkillsDef;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.SkillsFilter;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.SkillsView;
import de.tarent.activity.web.util.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller handling the 'My Skills' action.
 *
 */
@Controller("MySkillsController")
@RequestMapping(value = "view")
public class MySkillsController extends BaseController{

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private MenuPropertiesService menuPropertiesService;


    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("skillsDefName", "skill.skillsDef.name");
        SORT_ALIAS.put("value", "skill.value");

    }

    /**
     * Default renderer, handles render actions such as "cancel"
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(
            @RequestParam(value = ActivityConstants.CONTEXT_CONSTANT, required = false) final String context,
            @RequestParam(value = "skillPk", required = false) final String skillId,
            Model model, RenderRequest request)  throws IOException  {

        throwExceptionIfNotActivated();
        return "myskills/view";

    }

    /**
     * @param skillPk
     *            skillPk
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @RenderMapping(params = "ctx=showMySkillsEditView")
    public String showMySkillsEditView(Long skillPk, Model model, PortletRequest request) throws IOException {

        throwExceptionIfNotActivated();
        SkillsView skillsView = new SkillsView( resourceService.getSkills(skillPk) );
        if(((BindingAwareModelMap) model).get("skillsView") == null) {
            model.addAttribute("skillsView", skillsView);
        }
        model.addAttribute("options", getClassificationList());
        return "myskills/edit";

    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @RenderMapping(params = "ctx=showMySkillsAddView")
    public String showMySkillsAddView(Model model, PortletRequest request) throws IOException {

        throwExceptionIfNotActivated();
        SkillsView skillsView = null;

        if(((BindingAwareModelMap) model).get("skillsView") == null) {
            skillsView = new SkillsView();
            model.addAttribute("skillsView", skillsView);
        }

        List<SkillsDef> skillsDefList = resourceService.skillsDefList();
        model.addAttribute("skillsDef", skillsDefList);
        model.addAttribute("options", getClassificationList());
        return "myskills/edit";

    }

    /**
     * Supplies the MySkills table as a json string.
     *
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param active
     *            active
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("showMySkillsTable")
    public String showMySkillsTable(String sort, String dir, Long startIndex, Long results, String active, Model model,
            ResourceRequest request) throws IOException {

        throwExceptionIfNotActivated();
        SkillsFilter filter = new SkillsFilter(RequestUtil.getLoggedUser(request).getPk(), null, startIndex, results,
                SORT_ALIAS.get(sort), dir);

        FilterResult<Skills> skills = resourceService.searchSkills(filter);

        List<SkillsView> list = new ArrayList<SkillsView>();
        for (Skills s : skills.getRecords()) {
            SkillsView sv = new SkillsView(s);
            list.add(sv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<SkillsView>("skills list", null,
                list, skills.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * @param skillsView
     *            skillsView
     * @param result
     *            result
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ActionMapping(value = "doSaveMySkill")
    public void doSave(@ModelAttribute("skillsView") @Valid SkillsView skillsView, BindingResult result, Model model,
            PortletRequest request, ActionResponse response) {

        if (!result.hasErrors()) {
            if (skillsView.getId() == null) {
                // add new skill
                SkillsDef skillsDef = resourceService.getSkillsDefByName(skillsView.getSkillsDefName().trim());
                if (skillsDef == null) {
                    skillsDef = new SkillsDef();
                    skillsDef.setName(skillsView.getSkillsDefName().trim());
                    skillsDef.setPk(resourceService.addSkillDef(skillsDef));
                }
                Skills skill = resourceService.getUserSkillBySkillDef(RequestUtil.getLoggedUser(request).getPk(),
                        skillsDef.getPk());
                if (skill == null) {
                    skill = new Skills();
                    skill.setResource(RequestUtil.getLoggedUser(request));
                    skill.setSkillsDef(new SkillsDef(skillsDef.getPk()));
                    skill.setValue(skillsView.getValue());
                    resourceService.addSkill(skill);
                    request.setAttribute("successMessage", "saved");
                } else {
                    // error
                    model.addAttribute("exist", "exist");
                    response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showMySkillsEditView");
                }
            } else {
                // edit skill
                Skills skill = resourceService.getSkills(skillsView.getId());
                skill.setValue(skillsView.getValue());
                resourceService.updateSkill(skill);
                request.setAttribute("successMessage", "saved");
            }
            model.addAttribute("skillsView", new SkillsView());
            
        } else {
            if(skillsView.getId() != null ){
                response.setRenderParameter("skillPk", skillsView.getId().toString());
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showMySkillsEditView");

            }else{
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showMySkillsAddView");
            }

        }

    }


    /**
     * @param model
     *            model
     * @return String
     */
    @ActionMapping(value = "doCancelSkillAction")
    public void doCancel(Model model) {
        model.addAttribute("skillsView", new SkillsView());
    }

    private List<SelectOption> getClassificationList() {
        List<SelectOption> options = new ArrayList<SelectOption>();
        options.add(new SelectOption(1, "1-Professional"));
        options.add(new SelectOption(2, "2-Medium"));
        options.add(new SelectOption(3, "3-Basic"));
        return options;
    }

    /**
     * @param skillPk
     *            skillPk
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @ActionMapping(value = "doDeleteMySkill")
    public void doDelete(Long skillPk, ActionRequest request) throws IOException {
        resourceService.deleteSkill(skillPk);
        request.setAttribute("successMessage", "deleted");
    }

    private void throwExceptionIfNotActivated(){
        String myMenuPageLink = menuPropertiesService.getMyPositionsPageLink();
        if( myMenuPageLink == null || myMenuPageLink.equals("") ){
            throw new RuntimeException("This portlet is disabled.");
        }
    }

}
