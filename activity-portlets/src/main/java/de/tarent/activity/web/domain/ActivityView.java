/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.validator.constraints.ActivityHours;

/**
 * This class stores data from YUI dialog form.
 *
 */
public class ActivityView implements Serializable {

    private static final long serialVersionUID = -7306903759111598614L;

    private Long id;

    private Long resourceId;

    private String resourceName;

    @NotNull(message = "{validate_job_not_empty}")
    private Long jobId;

    private String jobName;

    private Long jobStatusId;

    private Long positionStatusId;

    @NotNull(message = "{validate_position_not_empty}")
    private Long positionId;

    private String positionName;

    @NotEmpty(message = "{validate_description_not_empty}")
    @Size(min = 0, max = 255, message = "{validate_description_length}")
    private String description;

    @NotNull(message = "{validate_hours_not_empty}")
    @DecimalMin(value = "0.25", message = "{validate_hours_range}")
    @DecimalMax(value = "24.00", message = "{validate_hours_range}")
    @ActivityHours
    private BigDecimal hours;

    @DateTimeFormat
    @NotNull(message = "{validate_date_not_empty}")
    private Date date;

    @Pattern(regexp = "^(:?[0-9]{1,})*", message = "{validate_evolvis_task_id}")
    @Size(max = 20, message = "{validate_evolvis_task_id}")
    @NumberFormat
    private String evolvisTaskId;

    /**
     * Default and null constructor.
     */
    public ActivityView() {
    }

    /**
     * Constructor using an Activity.
     *
     * @param a
     *            Activity
     */
    public ActivityView(Activity a) {

        this(a.getPk(), a.getPosition().getJob().getPk(), "(" + a.getPosition().getJob().getProject().getName() + ")"
                + " " +a.getPosition().getJob().getName(), a.getPosition().getPk(), a.getPosition().getName(), a.getName(),
                a.getHours(), a.getDate(), (a.getEvolvisTaskId() == null) ? "" : a.getEvolvisTaskId().toString());
        this.setResourceId(a.getResource().getPk());
        this.setResourceName(a.getResource().getUsername());
        this.jobStatusId = a.getPosition().getJob().getJobStatus().getPk();
        this.positionStatusId = a.getPosition().getPositionStatus().getPk();
    }

    /**
     * Constructor using all fields.
     *
     * @param id
     *            Activity id
     * @param jobId
     *            Job id
     * @param jobName
     *            Job name
     * @param positionId
     *            position id
     * @param positionName
     *            position name
     * @param description
     *            Activity description
     * @param hours
     *            Activity hours
     * @param date
     *            Activity date
     * @param evolvisTaskId
     *            Evolvis Task ID
     */
    public ActivityView(Long id, Long jobId, String jobName, Long positionId, String positionName, String description,
            BigDecimal hours, Date date, String evolvisTaskId) {
        this.id = id;
        this.jobId = jobId;
        this.jobName = jobName;
        this.positionId = positionId;
        this.positionName = positionName;
        this.description = description;
        this.hours = hours;
        this.date = date;
        this.evolvisTaskId = evolvisTaskId;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getEvolvisTaskId() {
        return evolvisTaskId;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public Long getId() {
        return id;
    }

    public Long getJobId() {
        return jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public Long getJobStatusId() {
        return jobStatusId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEvolvisTaskId(String evolvisTaskId) {
        this.evolvisTaskId = evolvisTaskId;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setJobStatusId(Long jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Long getPositionStatusId() {
        return positionStatusId;
    }

    public void setPositionStatusId(Long positionStatusId) {
        this.positionStatusId = positionStatusId;
    }

}
