/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myactiveprojects;

import de.tarent.activity.domain.Project;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.*;
import java.util.List;

/**
 * Controller handling the 'My Active Projects' action.
 * 
 */
@Controller("MyActiveProjectsController")
@RequestMapping(value = "view")
public class MyActiveProjectsController extends BaseController{

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PermissionChecker permissionChecker;

    /**
     * Shows user active projects.
     * 
     * @param model
     *            - Model
     * @param request
     *            - RenderRequest
     * @return the view's name
     */
    @RenderMapping
    public String view(Model model, RenderRequest request, RenderResponse response) {
        return ajaxView(model, request, response.createRenderURL());
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping(params="ctx=getMyPositions")
    public String getMyPositions(Model model, RenderRequest request) {
        return "myposition/view";
    }

    /**
     * Set model for view.
     * 
     * @param model
     *            - Model
     * @param request
     *            -PortletRequest
     * @return the view's name
     */
    private String ajaxView(Model model, PortletRequest request, PortletURL portletURL) {
        Long pk = RequestUtil.getLoggedUser(request).getPk();

        List<Project> projects = projectService.listProjectByResource(pk);
        model.addAttribute("projects", projects);

        List<Project> responsibleProjects = projectService.listProjectByResponsibleResource(pk);
        if( userHasTheRightToSeeResponsibleProjectList(model, request)){
            model.addAttribute("responsibleProjects", responsibleProjects);
        }

        // manual check if current user has permission to view all projects
        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_ALL)) {
            List<Project> allActiveProjects = projectService.getAllActiveProjectList();
            model.addAttribute("allActiveProjects", allActiveProjects);
        }

        portletURL.setParameter(ActivityConstants.CONTEXT_CONSTANT, "showProjectDetails");

        model.addAttribute("linkToProjectDetailsPage", portletURL);

        String myProject = (String) request.getPortletSession().getAttribute("MY_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("myProject", myProject);

        String responsibleProject = (String) request.getPortletSession().getAttribute("RESPONSIBLE_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("responsibleProject", responsibleProject);

        String allProject = (String) request.getPortletSession().getAttribute("ALL_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("allProject", allProject);

        return "myactiveprojects/view";
    }

    private boolean userHasTheRightToSeeResponsibleProjectList(Model model, PortletRequest request) {
        boolean isController = currentUserhasTheRequiredRole(model, request, "Controller");
        boolean isProjectLeader = currentUserhasTheRequiredRole(model, request, "Projektleiter");
        return isController || isProjectLeader;
    }

    /**
     * Save menu (collapsed or extended).
     * 
     * @param myProject
     *            - settings for my project list
     * @param responsibleProject
     *            - settings for responsible project
     * @param allProject
     *            - setting for all active projects
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("doSaveDetails")
    public String doSaveDetails(String myProject, String responsibleProject, String allProject, Model model,
            PortletRequest request, ResourceResponse response) {
        if (myProject != null) {
            request.getPortletSession().setAttribute("MY_PROJECT_LIST", myProject, PortletSession.APPLICATION_SCOPE);
        }

        if (responsibleProject != null) {
            request.getPortletSession().setAttribute("RESPONSIBLE_PROJECT_LIST", responsibleProject,
                    PortletSession.APPLICATION_SCOPE);
        }

        if (allProject != null) {
            request.getPortletSession().setAttribute("ALL_PROJECT_LIST", allProject, PortletSession.APPLICATION_SCOPE);
        }

        return ajaxView(model, request, response.createRenderURL());
    }

}
