/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Job;
import de.tarent.activity.validator.constraints.Url;
import de.tarent.activity.web.util.EscapeDescriptionUtil;

public class JobView {

    private Long id;

    private Long idTable;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    private String jobStatusName;

    @NotNull(message = "{validate_job_status_not_empty}")
    private Long jobStatusId;

    private String jobTypeName;

    @NotNull(message = "{validate_job_type_not_empty}")
    private Long jobTypeId;

    @NotEmpty(message = "{validate_name_not_empty}")
    @Size(max = 255)
    private String name;

    @NotEmpty(message = "{validate_job_number_not_empty}")
    private String nr;

    @NotNull(message = "{validate_accounts_not_empty}")
    @NumberFormat
    @Max(value = 99999999)
    private Long accounts;

    private Date jobStartDate;

    private Date jobEndDate;

    @NotNull(message = "{validate_project_not_empty}")
    private Long projectId;

    private String projectName;

    @NotNull(message = "{validate_project_responsible_not_empty}")
    private Long managerId;

    private String managerName;

    @Url
    private String crm;

    @Url
    private String offer;

    @Url
    private String request;

    private Boolean updatePosition;

    private Date expectedBilling;

    private Date payedDate;

    private Character billable;

    private String customerProjectName;

    public JobView() {
    }

    public JobView(Job job) {
        this(job.getPk(), job.getNote(), (job.getJobStatus() == null) ? "" : job.getJobStatus().getName(), (job
                .getJobStatus() == null) ? 0l : job.getJobStatus().getPk(), (job.getJobType() == null) ? "" : job
                .getJobType().getName(), (job.getJobType() == null) ? 0l : job.getJobType().getPk(), job.getName(), job
                .getNr(), job.getAccounts(), job.getJobStartDate(), job.getJobEndDate(),
                (job.getProject() == null) ? 0l : job.getProject().getPk(), (job.getProject() == null) ? "" : job
                        .getProject().getName(), (job.getManager() == null) ? 0L : job.getManager().getPk(), (job
                        .getManager() == null) ? "" : job.getManager().getLastname() + ", "
                        + job.getManager().getFirstname(), job.getCrm(), job.getOffer(), job.getRequest(),
                Boolean.FALSE, job.getExpectedBilling(), job.getPayed(), job.getBillable());
        this.customerProjectName = (job.getProject() == null) ? "" : job.getProject().getCustomer().getName();
    }

    public JobView(Long id, String description, String jobStatusName, Long jobStatusId, String jobTypeName,
            Long jobTypeId, String name, String nr, Long accounts, Date jobStartDate, Date jobEndDate, Long projectId,
            String projectName, Long managerId, String managerName, String crm, String offer, String request,
            Boolean updatePosition, Date expectedBilling, Date payedDate, Character billable) {
        super();
        this.id = id;
        this.idTable = id;
        this.description = description;
        this.jobStatusName = jobStatusName;
        this.jobStatusId = jobStatusId;
        this.jobTypeName = jobTypeName;
        this.jobTypeId = jobTypeId;
        this.name = name;
        this.nr = nr;
        this.accounts = accounts;
        this.jobStartDate = jobStartDate;
        this.jobEndDate = jobEndDate;
        this.projectId = projectId;
        this.projectName = projectName;
        this.managerId = managerId;
        this.managerName = managerName;
        this.crm = crm;
        this.offer = offer;
        this.request = request;
        this.updatePosition = updatePosition;
        this.expectedBilling = expectedBilling;
        this.payedDate = payedDate;
        this.billable = billable;
        
        if(this.description != null){
            this.description = EscapeDescriptionUtil.escapeDescriptionWithoutBr(this.description);
        }
    }

    public JobView(Long id, String description, String jobStatusName, String jobTypeName, String name) {
        super();
        this.id = id;
        this.description = description;
        this.jobStatusName = jobStatusName;
        this.jobTypeName = jobTypeName;
        this.name = name;
    }

    public Long getAccounts() {
        return accounts;
    }

    public String getCrm() {
        return crm;
    }

    public String getDescription() {
        return description;
    }

    public Date getExpectedBilling() {
        return expectedBilling;
    }

    public Long getId() {
        return id;
    }

    public Date getJobEndDate() {
        return jobEndDate;
    }

    public Date getJobStartDate() {
        return jobStartDate;
    }

    public Long getJobStatusId() {
        return jobStatusId;
    }

    public String getJobStatusName() {
        return jobStatusName;
    }

    public Long getJobTypeId() {
        return jobTypeId;
    }

    public String getJobTypeName() {
        return jobTypeName;
    }

    public Long getManagerId() {
        return managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getName() {
        return name;
    }

    public String getNr() {
        return nr;
    }

    public String getOffer() {
        return offer;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getRequest() {
        return request;
    }

    public Boolean getUpdatePosition() {
        return updatePosition;
    }

    public void setAccounts(Long accounts) {
        this.accounts = accounts;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setExpectedBilling(Date expectedBilling) {
        this.expectedBilling = expectedBilling;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setJobEndDate(Date jobEndDate) {
        this.jobEndDate = jobEndDate;
    }

    public void setJobStartDate(Date jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public void setJobStatusId(Long jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public void setJobStatusName(String jobStatusName) {
        this.jobStatusName = jobStatusName;
    }

    public void setJobTypeId(Long jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    public void setJobTypeName(String jobTypeName) {
        this.jobTypeName = jobTypeName;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public void setUpdatePosition(Boolean updatePosition) {
        this.updatePosition = updatePosition;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public Date getPayedDate() {
        return payedDate;
    }

    public void setPayedDate(Date payedDate) {
        this.payedDate = payedDate;
    }

    public Character getBillable() {
        return billable;
    }

    public void setBillable(Character billable) {
        this.billable = billable;
    }

    public String getCustomerProjectName() {
        return customerProjectName;
    }

    public void setCustomerProjectName(String customerProjectName) {
        this.customerProjectName = customerProjectName;
    }

}
