/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allrequests;

import de.tarent.activity.web.controller.BaseRequestController;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import java.io.IOException;

/**
 * AllRequestsController.
 * 
 * TODO: refactor ResourceMappings
 */
@Controller("AllRequestsController")
@RequestMapping(value = "view")
public class AllRequestsController extends BaseRequestController {
    /**
     * 
     * @param model
     *            Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {

        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, getResourceService().getResourceList());

        return "allrequests/view";
    }

    /**
     * Create request list by filter.
     * 
     * @param selectedResource
     *            Selected resource
     * @param selectedController
     *            Selected controller
     * @param selectedStatus
     *            - selected status
     * @param startIndex
     *            Offset
     * @param results
     *            Number of items per page
     * @param sort
     *            Sorted column
     * @param dir
     *            Sort direction
     * @param request
     *            -ResourceRequest
     * @param model
     *            - Model
     * @return Result location
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(Long selectedResource, Long selectedController, String selectedStatus, Long startIndex,
            Long results, String sort, String dir, ResourceRequest request, Model model) throws IOException {

        createDataTableResult(selectedResource, selectedController, selectedStatus, startIndex, results, sort, dir,
                model);

        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return ajaxView(model, request);
    }

    @RenderMapping(params = "ctx=getAllRequests")
    public String getAllRequests(Model model, RenderRequest request) {
        return view(model, request);
    }
    
    /**
     * Delete a delete-request by ID.
     * 
     * @param requestPk
     *            DeleteRequest id
     * @param request
     *            request
     * 
     * @throws IOException
     */
    @ActionMapping("doDelete")
    public void doDelete(Long requestPk, PortletRequest request) throws IOException {
        getResourceService().deleteRequest(requestPk);
        request.setAttribute("successMessage", "deleted");
    }
}
