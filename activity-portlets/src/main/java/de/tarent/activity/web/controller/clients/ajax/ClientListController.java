/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.clients.ajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.domain.CustomerView;
import de.tarent.activity.web.util.DataTableResult;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

@Controller("ClientController")
@RequestMapping(value = "view")
public class ClientListController {

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("idTable", "customer.pk");
        SORT_ALIAS.put("name", "customer.name");
        SORT_ALIAS.put("description", "customer.note");

    }

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProjectService projectService;

    @ResourceMapping("getClientTable")
    public String getClientTable(@RequestParam(required = false) String sort,
            @RequestParam(required = false) String dir, @RequestParam(required = false) Long startIndex,
            @RequestParam(required = false) Long results, @RequestParam(required = false) String active, PortletRequest request, Model model)
            throws IOException {

        PortletSession ps = request.getPortletSession();
        ps.setAttribute("LIFERAY_SHARED_clientsRowsPerPage", results, PortletSession.APPLICATION_SCOPE);

        BaseFilter filter = new BaseFilter(startIndex, results, SORT_ALIAS.get(sort), dir);
        FilterResult<Customer> customers = projectService.searchCustomers(filter);

        List<CustomerView> list = new ArrayList<CustomerView>();
        for (Customer c : customers.getRecords()) {
            CustomerView cv = new CustomerView(c, false);
            list.add(cv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<CustomerView>("customer list",
                null, list, customers.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;
    }
    @RenderMapping(params = "ctx=getClientList")
    public String getClientList(@RequestParam Long clientPk,Model model, PortletRequest request) {
    		return "clients/view";
    }
}
