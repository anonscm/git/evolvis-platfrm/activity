/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.positions;

import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PosResourceMappingFilter;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.PosResourceMappingView;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RequestUtil;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main controller for All Positions portlet.
 *
 */
@Controller("PositionController")
@RequestMapping(value = "view")
public class PositionController extends BaseController{

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PermissionChecker permissionChecker;
    
    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS_RESOURCE = new HashMap<String, String>();
    static {
        SORT_ALIAS_RESOURCE.put("resourceName", "posResMapping.resource.firstname");
        SORT_ALIAS_RESOURCE.put("startDate", "posResMapping.startDate");
        SORT_ALIAS_RESOURCE.put("endDate", "posResMapping.endDate");
    }

//    @ModelAttribute("jobsList")
//    public List<Job> getJobList() {
//        return projectService.getJobListToManage();
//    }

    @RenderMapping(params = "ctx=getPositionList")
    public String showPositionsList(PortletRequest request, Model model) {
        return defaultAjaxView(request, model);
    }

    @RenderMapping
    public String defaultView(String sort, String dir, Long startIndex, Long results, Long jobId, Model model,
            PortletRequest request) throws IOException {
    	
    	//Used for the filtered Job selectbox
    	//Shows just the jobs which belong to mine positions
    	 PositionFilter filter = new PositionFilter(null, jobId, startIndex, results, SORT_ALIAS_RESOURCE.get(sort), dir);

         if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW_ALL)) {
             filter.setViewBy(PositionFilter.VIEW_ALL);
         } else if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW_MANAGER)) {
             filter.setViewBy(PositionFilter.VIEW_BY_MANAGER);
             filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
         } else if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW)) {
             filter.setViewBy(PositionFilter.VIEW_OWN);
             filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
         } else {
             throw new PermissionException("You do not have the permission to view positions.");
         }
         
         FilterResult<Position> positions = projectService.searchPosition(filter);

         List<Job> jobListToSelect = new ArrayList<Job>();
         for (Position p : positions.getRecords()){
        	if(!jobListToSelect.contains(p.getJob())){ 
        		jobListToSelect.add(p.getJob());
        	}
         }        
         model.addAttribute("jobsList", jobListToSelect );
         
         return defaultAjaxView(request, model);
    }

    @ResourceMapping("getPositionList")
    public String defaultAjaxView(PortletRequest request, Model model) {
        PortletSession ps = request.getPortletSession();
        setUserPreferencesInSession(model, ps, "LIFERAY_SHARED_positionsRowsPerPage", "positionsRowsPerPage");
        return "positions/view";
    }


    @ResourceMapping("tableResources")
    public String tableResources(String sort, String dir, Long startIndex, Long results, Long selectedPositionId,
            Model model, ResourceRequest request) throws IOException {

        PosResourceMappingFilter filter = new PosResourceMappingFilter(null, null, selectedPositionId, startIndex,
                null, SORT_ALIAS_RESOURCE.get(sort), dir);

        FilterResult<PosResourceMapping> posResourceMapping = projectService.searchPosResourceMapping(filter);
        List<PosResourceMappingView> list = new ArrayList<PosResourceMappingView>();

        for (PosResourceMapping r : posResourceMapping.getRecords()) {
            PosResourceMappingView pv = new PosResourceMappingView(r);
            list.add(pv);
        }

        DataTableResult<PosResourceMappingView> dataTableResult = new DataTableResult<PosResourceMappingView>(
                "resource list", null, list, (long) list.size());

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(dataTableResult));
        return ActivityConstants.JSON_CONSTANT;
    }

    @ResourceMapping("getPositionCancel")
    public String getPositionCancel(Model model, PortletRequest request, Long positionId) throws IOException {
        return "positions/view";
    }

    @ActionMapping("doDelete")
    public void doDelete(Long id, Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, new PositionView());
        projectService.deletePosition(id);

        request.setAttribute("successMessage", "deleted");
    }

}
