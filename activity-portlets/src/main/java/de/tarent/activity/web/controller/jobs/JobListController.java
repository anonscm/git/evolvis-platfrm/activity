/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.jobs;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.domain.JobStatus;
import de.tarent.activity.domain.JobType;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.filter.JobFilter;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.RequestUtil;
import de.tarent.activity.web.util.PermissionChecker;

@Controller("JobListController")
@RequestMapping(value = "view")
public class JobListController extends BaseController{

	@Autowired
    private PermissionChecker permissionChecker;
	
    @Autowired
    private ProjectService projectService;

    @ModelAttribute("jobStatuses")
    public List<JobStatus> populateJobStatusList() {
        return projectService.getJobStatusList();
    }

    @ModelAttribute("jobTypes")
    public List<JobType> populateJobTypes() {
        return projectService.getJobTypeList();
    }
    
    //Drop-Down Menue unterscheidet nun die Rollen und zeigt nur die Projekte an, welche für die Ressource bestimmt sind.
    @ModelAttribute("availableProjects")
    public List<Project> populateAvailableProjects(Model model, PortletRequest request){
    	Long resourceId = RequestUtil.getLoggedUser(request).getPk();
    	List<Project> projectListToReturn = new ArrayList<Project>();
    	//Fuer Rolle Controller
    	if (permissionChecker.checkPermission(request, PermissionIds.JOB_VIEW_ALL)) {
    		projectListToReturn = projectService.getProjectList();
        }
    	//Fuer Rolle Projektleiter
    	else if (permissionChecker.checkPermission(request, PermissionIds.JOB_VIEW_MANAGER)) {
    		projectListToReturn = projectService.listProjectByResponsibleResource(resourceId);
        }
    	return projectListToReturn;
    }
    
    @RenderMapping
    public String defaultView(Model model, PortletRequest request ) {
        //Get rowsOnOnePage from userSpecified session
        setSessionPagingConfig(model, request, JobTableController.PAGING_CONFIG);

        return "jobs/view";
    }

    @RenderMapping(params = "ctx=getAllJobsTable")
    public String getAllJobssTable(Model model, RenderRequest request) {
        return defaultView(model, request);
    }
}
