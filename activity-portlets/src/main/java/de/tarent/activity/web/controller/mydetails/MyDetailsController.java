/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.mydetails;

import javax.portlet.RenderRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.service.PropertiesService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.SettingsService;
import de.tarent.activity.web.controller.myholiday.MyHolidayController;
import de.tarent.activity.web.util.RequestUtil;

/**
 * MyDetailsController portlet controller.
 * 
 */
@Controller("MyDetailsController")
@RequestMapping(value = "view")
public class MyDetailsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyHolidayController.class);

    @Autowired
    private PropertiesService propertiesService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private SettingsService settingsService;

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        LOGGER.info("MyDetailsController view ");

        Resource resource = resourceService.getResource(RequestUtil.getLoggedUser(request).getPk(), false, false);
        model.addAttribute("resource", resource);

        // TODO
        // int rights = getRights(resource);
        // model.addAttribute("groupList", getGroupsForUser(rights));
        // model.addAttribute("roleUser", isUser(rights));
        // model.addAttribute("roleManager", isManage(rights));

        model.addAttribute("dbVersion", settingsService.getDBVersion());
        model.addAttribute("webAppVersion", propertiesService.getWebApplicationVersion());

        return "mydetails/view";
    }

}
