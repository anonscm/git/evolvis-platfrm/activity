/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.util.TableModel;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ExportService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.RequestUtil;

@Controller("ActivityExportController")
@RequestMapping(value = "view")
public class ActivityExportController extends BaseController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ExportService exportService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ProjectService projectService;

    @ResourceMapping("getActivityAllExport")
    public void getActivityAllExport(   @RequestParam(required = false) Long resourceId,
                                        @RequestParam(required = false) Long projectId,
                                        @RequestParam(required = false) Long jobId,
                                        @RequestParam(required = false) Long positionId,
                                        @RequestParam(required = false, value = "startDate_from_alt") Date startDate,
                                        @RequestParam(required = false, value = "startDate_to_alt") Date endDate,
                                        ResourceRequest request, ResourceResponse response, Model model)
            throws IOException {

        ActivityFilter filter = new ActivityFilter(resourceId, jobId, positionId, startDate, endDate, projectId, null, null,
                null, null);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        Locale locale = request.getLocale();
        String[] columnHeaders = {
                messageSource.getMessage("id", null, locale),
                messageSource.getMessage("resource", null, locale),
                messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale),
                messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                messageSource.getMessage("description", null, locale),
                messageSource.getMessage("hours", null, locale),
                messageSource.getMessage("date", null, locale) };

        List<Object[]> items = new ArrayList<Object[]>();

        for (Activity activity : activities.getRecords()) {
            Object[] item = new Object[7];
            item[0] = activity.getPk();
            item[1] = activity.getResource() == null ? null : activity.getResource().getName();
            item[2] = activity.getPosition() == null || activity.getPosition().getJob() == null ? null : activity.getPosition().getJob().getName();
            item[3] = activity.getPosition() == null ? null : activity.getPosition().getName();
            item[4] = activity.getName();
            item[5] = activity.getHours();
            item[6] = activity.getDate();

            items.add(item);
        }

        // tell browser program going to return an application file
        // instead of html page
        response.setContentType("application/octet-stream");
        response.setProperty("Content-Disposition", "attachment;filename=Activity.xls");

        byte[] export = exportService.doExportExcel(new TableModel(columnHeaders, items, "Activity", null));
        OutputStream out = response.getPortletOutputStream();

        try {
            out.write(export);
            out.flush();
        } finally {
            out.close();
        }
    }

    @ResourceMapping("getActivityMyExport")
public void getActivityMyExport(    @RequestParam(required = false) Long jobId,
                                    @RequestParam(required = false) Long positionId,
                                    @RequestParam(required = false, value = "startDate-alt") Date startDate,
                                    @RequestParam(required = false, value = "endDate-alt") Date endDate, ResourceRequest request, ResourceResponse response,
                                    Model model) throws IOException {
        Resource res = RequestUtil.getLoggedUser(request);
        ActivityFilter filter = new ActivityFilter(res.getPk(), jobId, positionId, startDate, endDate, null, null, null,
                null, null);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        Locale locale = request.getLocale();
        String[] columnHeaders = { messageSource.getMessage("id", null, locale),
                messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale), messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                messageSource.getMessage("description", null, locale), messageSource.getMessage("hours", null, locale),
                messageSource.getMessage("date", null, locale) };

        List<Object[]> items = new ArrayList<Object[]>();

        for (Activity activity : activities.getRecords()) {
            Object[] item = new Object[6];
            item[0] = activity.getPk();
            item[1] = activity.getPosition().getJob() == null ? null : activity.getPosition().getJob().getName();
            item[2] = activity.getPosition() == null ? null : activity.getPosition().getName();
            item[3] = activity.getName();
            item[4] = activity.getHours();
            item[5] = activity.getDate();

            items.add(item);
        }

        List<Object[]> itemsDesc = new ArrayList<Object[]>();
        Object[] itemDescResource = {
                messageSource.getMessage("resource", null, locale),
                res.getFirstname() + " "
                        + res.getLastname() };
        itemsDesc.add(itemDescResource);

        if (jobId != null) {
            Job job = projectService.getJob(jobId);
            Object[] itemDescClient = { messageSource.getMessage("customer", null, locale),
                    job.getProject().getCustomer().getName() };
            itemsDesc.add(itemDescClient);
            Object[] itemDescProject = { messageSource.getMessage("project", null, locale), job.getProject().getName() };
            itemsDesc.add(itemDescProject);
            Object[] itemDescJob = { messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale), job.getName() };
            itemsDesc.add(itemDescJob);
            if (positionId != null) {
                Position pos = projectService.getPosition(positionId);
                Object[] itemDescPosition = { messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale), pos.getName() };
                itemsDesc.add(itemDescPosition);

            } else {
                Object[] itemDescPosition = { messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                        messageSource.getMessage(ActivityConstants.ALL_CONSTANT, null, locale) };
                itemsDesc.add(itemDescPosition);
            }
        } else {
            Object[] itemDescClient = { messageSource.getMessage("customer", null, locale),
                    messageSource.getMessage(ActivityConstants.ALL_CONSTANT, null, locale) };
            itemsDesc.add(itemDescClient);
            Object[] itemDescProject = { messageSource.getMessage("project", null, locale),
                    messageSource.getMessage(ActivityConstants.ALL_CONSTANT, null, locale) };
            itemsDesc.add(itemDescProject);
            Object[] itemDescJob = { messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale),
                    messageSource.getMessage(ActivityConstants.ALL_CONSTANT, null, locale) };
            itemsDesc.add(itemDescJob);
            Object[] itemDescPosition = { messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                    messageSource.getMessage(ActivityConstants.ALL_CONSTANT, null, locale) };
            itemsDesc.add(itemDescPosition);
        }

        // tell browser program going to return an application file
        // instead of html page
        response.setContentType("application/octet-stream");
        response.setProperty("Content-Disposition", "attachment;filename=Activity.xls");

        TableModel tableModel = new TableModel(columnHeaders, items, "Activity", null);
        tableModel.setItemsDescription(itemsDesc);
        byte[] export = exportService.doExportExcel(tableModel);
        OutputStream out = response.getPortletOutputStream();

        try {
            out.write(export);
            out.flush();
        } finally {
            out.close();
        }
    }
}
