/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;

/**
 * DropDownListController provides ajax-able request mappings to retrieve html snippets for several drop-down lists.
 * 
 * You may set a request parameter <code>isFilter</code> on each request to define for what you want to use the
 * drop-down list. The parameter will be passed to model so that it can be used to determine the initial text of the
 * drop-down list.
 */
@Controller("DropDownListController")
@RequestMapping(value = "view")
public class DropDownListController {
	
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    /**
     * Pass request parameter <code>isFilter</code> to model.
     */
    @ModelAttribute("isFilter")
    public Boolean populateIsFilter(@RequestParam(required = false, defaultValue = "false") Boolean isFilter) {
        return isFilter;
    }

    /**
     * Ajax-able request mapping to retrieve a drop-down list of jobs, selected by given project id.
     * 
     * @return html snippet with drop-down list of jobs
     */
    @ResourceMapping("getJobsByProject")
    public String getJobsByProject(@RequestParam Long projectIdId, Model model) {
        model.addAttribute("jobsList", projectService.getJobsByProject(projectIdId));

        return "common/jobs";
    }

    /**
     * Ajax-able request mapping to retrieve a drop-down list of jobs, selected by given resource id.
     * 
     * @return html snippet with drop-down list of jobs
     */
    @ResourceMapping("getJobsByResource")
    public String getJobsByResource(@RequestParam Long selectedResourceId, Model model) {
        model.addAttribute("jobsList", projectService.getActiveJobsByResource(selectedResourceId));

        return "common/projectsJobs";
    }
//TODO: Refactor me: drei folgenden Methoden machen das gleiche
    /**
     * Ajax-able request mapping to retrieve a drop-down list of positions, selected by given job id.
     * 
     * @return html snippet with drop-down list of positions
     */
    @ResourceMapping("getPositionsByJob")
    public String getPositionsByJob(@RequestParam Long jobId, Model model) {
        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, projectService.getPositionsByJob(jobId, BaseActivityController.STATUS_ID_IN_ARBEIT, null));

        return "common/positions";
    }
    
    @ResourceMapping("getPositionsByJobForEditActivity")
    public String getPositionsByJobForEditActivity(@RequestParam Long jobId, Model model) {
        model.addAttribute("editActivityPositionsList", projectService.getPositionsByJob(jobId, BaseActivityController.STATUS_ID_IN_ARBEIT, null));

        return "common/positions";
    }

    /**
     * Ajax-able request mapping to retrieve a drop-down list of positions mass update, selected by given job id.
     *
     * @return html snippet with drop-down list of positions
     */
    @ResourceMapping("getPositionsByJobForMassUpdate")
    public String getPositionsByJobForMassUpdate(@RequestParam Long jobId, Model model) {
        model.addAttribute("positionsListMassUpdate", projectService.getPositionsByJob(jobId, BaseActivityController.STATUS_ID_IN_ARBEIT, null));

        return "common/positionsMassUpdate";
    }

    /**
     * Ajax-able request mapping to retrieve a drop-down list of all resources.
     * 
     * @return html snippet with drop-down list of resources
     */
    @ResourceMapping("getResources")
    public String getResources(Model model) {
        model.addAttribute("resourcesList", resourceService.listActiveResources());

        return "common/resources";
    }
    
    /**
     * Ajax-able request mapping to retrieve a drop-down list of resources, selected by given position id.
     * 
     * @return html snippet with drop-down list of resources
     */
    @ResourceMapping("getResourcesByPosition")
    public String getResourcesByPosition(@RequestParam Long selectedPositionId, Model model) {
        model.addAttribute("resourcesList", resourceService.getByPosition(selectedPositionId));

        return "common/resources";
    }
}
