/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.PosResourceStatus;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.PositionStatus;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.PosResourceMappingFilter;
import de.tarent.activity.exception.ActivityImportException;
import de.tarent.activity.service.ImportService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.ImportResultModel;
import de.tarent.activity.web.controller.BaseActivityController;
import de.tarent.activity.web.domain.ActivityImportView;

/**
 * ActivityImportController.
 *
 */
@Controller("ActivityImportController")
@RequestMapping(value = "view")
public class ActivityImportController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ActivityImportController.class);
	
	@Autowired
	private ResourceService resourceService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ImportService importService;

	@ModelAttribute("jobsList")
	public List<Job> populateJobList(PortletRequest request) {
		return projectService.getJobList();
	}

	@RenderMapping
	public String view(Model model, RenderRequest request) {
		return "activities/import";
	}

	/**
	 * Returns subset of {@link Position} for a given {@link Job}, where the
	 * {@link PositionStatus} is {@link ProjectService#POSITIONSTATUS_ATWORK}.
	 *
	 * @param jobId
	 *            the job id to filter by
	 * @param model
	 *            the {@link Model}
	 * @param resourceRequest
	 *            the {@link ResourceRequest}
	 * @return view name
	 */
	@ResourceMapping("getPositions")
	public String getAtWorkPositionsByJobId(final Long jobId,
			final Model model, final ResourceRequest resourceRequest) {
		final List<Position> positions = projectService
				.getPositionsByJob(jobId, BaseActivityController.STATUS_ID_IN_ARBEIT, null);

		final Iterator<Position> it = positions.iterator();
		while (it.hasNext()) {
			final Position position = it.next();
			if (!position.getPositionStatus().getPk()
					.equals(ProjectService.POSITIONSTATUS_ATWORK)) {
				it.remove();
			}
		}

		model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, positions);

		return "common/positions";
	}

	/**
	 * Returns subset of {@link Resource} for a given {@link Position}, where
	 * the {@link PosResourceStatus} is
	 * {@link ProjectService#POS_RES_STATUS_ACTIVE}
	 *
	 * @param selectedPositionId
	 *            the position id
	 * @param model
	 *            the {@link Model}
	 * @param resourceRequest
	 *            the {@link ResourceRequest}
	 *
	 *            return view name
	 */
	@ResourceMapping("getResources")
	public String getResourceByPositionId(
			@RequestParam final Long selectedPositionId, final Model model,
			final ResourceRequest resourceRequest) {

		final List<PosResourceMapping> posRessourceMapping = projectService
				.getPosResourceMappingsByPosition(selectedPositionId);

		final List<Resource> resources = new ArrayList<Resource>();
		for(PosResourceMapping item : posRessourceMapping) {
			if (item.getPosResourceStatus().getPk()
					.equals(ProjectService.POS_RES_STATUS_ACTIVE)) {
				resources.add(item.getResource());
			}
		}

		model.addAttribute("resourcesList", resources);

		return "common/resources";
	}

	@ActionMapping("doActivityImport")
	public void doActivityImport(
			@ModelAttribute @Valid ActivityImportView activityImportView,
			BindingResult result, Model model, ActionRequest request) {
		List<ImportResultModel> errors = new ArrayList<ImportResultModel>();
		if (!result.hasErrors()) {
			CommonsMultipartFile f = activityImportView.getFile();
			byte[] data = getByteArray(f);
			try {
				errors = importService.doImportExcel(data,
						activityImportView.getResourceId(),
						activityImportView.getPositionId());

				if (errors.size() == 0) {
					model.addAttribute("noErrors", "noErrors");

				}

                model.addAttribute("lines", ImportResultModel.getLines());
                model.addAttribute("linesSuccess", ImportResultModel.getLinesSuccess());
                model.addAttribute("linesFailure", ImportResultModel.getLinesFailure());
                model.addAttribute("errors", errors);

                model.addAttribute("activityImportView",
						new ActivityImportView());

			} catch (ActivityImportException e) {
				result.rejectValue("file", "validate_xls");
				for (ObjectError error : result.getAllErrors()) {
					LOGGER.error(error.toString());
				}
				List<Position> positionsList = projectService
						.getPositionsByJob(activityImportView.getJobId(), BaseActivityController.STATUS_ID_IN_ARBEIT, null);
				model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, positionsList);
				List<Resource> resourceList = resourceService
						.getByPosition(activityImportView.getPositionId());
				model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);

			}

		} else {
			for (ObjectError error : result.getAllErrors()) {
				LOGGER.error(error.toString());
			}
		}
	}

	private byte[] getByteArray(CommonsMultipartFile file) {

		InputStream bs;
		byte[] ret;
		try {
			bs = file.getInputStream();
			ret = new byte[bs.available()];
			bs.read(ret, 0, bs.available());
			return ret;
		} catch (IOException e) {
            LOGGER.error("IOException " + e.getMessage());
			return null;
		}
	}

}
