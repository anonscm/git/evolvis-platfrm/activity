/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import de.tarent.activity.domain.Cost;

/**
 * CostView.
 *
 */
public class CostView {
    private Long id;

    private Long idTable;

    @NotNull(message = "{validate_project_not_empty}")
    private Long projectId;

    @NotNull(message = "{validate_job_not_empty}")
    private Long jobId;
    private String jobName;

    @NotEmpty(message = "{validate_name_not_empty}")
    private String name;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    @NotNull(message = "{validate_resource_not_empty}")
    private Long resourceId;
    private String resourceName;

    @DateTimeFormat
    private Date date;

    @NotNull(message = "{validate_type_not_empty}")
    private Long costTypeId;
    private String costTypeName;

    @NotNull(message = "{validate_amount_not_empty}")
    @NumberFormat(style = Style.CURRENCY)
    private BigDecimal cost;

    /**
     * Constructor.
     *
     * @param cost
     *            cost
     */
    public CostView(Cost cost) {
        this(cost.getPk(), cost.getJob() == null ? null : cost.getJob().getProject() == null ? null : cost.getJob()
                .getProject().getPk(), cost.getJob() == null ? null : cost.getJob().getPk(),
                cost.getJob() == null ? null : cost.getJob().getName(), cost.getName(), cost.getNote(), cost
                        .getResource() == null ? null : cost.getResource().getPk(), cost.getDate(), cost
                        .getCostType() == null ? null : cost.getCostType().getPk(), cost.getCostType() == null ? null
                        : cost.getCostType().getName(), cost.getCost());
    }

    /**
     * Constructor.
     *
     * @param id
     *            id
     * @param projectId
     *            projectId
     * @param jobId
     *            jobId
     * @param jobName
     *            jobName
     * @param name
     *            name
     * @param description
     *            description
     * @param resourceId
     *            resourceId
     * @param date
     *            date
     * @param costTypeId
     *            costTypeId
     * @param costTypeName
     *            costTypeName
     * @param cost
     *            cost
     */
    public CostView(Long id, Long projectId, Long jobId, String jobName, String name, String description,
            Long resourceId, Date date, Long costTypeId, String costTypeName, BigDecimal cost) {
        super();
        this.id = id;
        this.idTable = id;
        this.projectId = projectId;
        this.jobId = jobId;
        this.jobName = jobName;
        this.name = name;
        this.description = description;
        this.resourceId = resourceId;
        this.date = date;
        this.costTypeId = costTypeId;
        this.costTypeName = costTypeName;
        this.cost = cost;
    }

    /**
     * Constructor.
     */
    public CostView() {
    }

    public BigDecimal getCost() {
        return cost;
    }

    public Long getCostTypeId() {
        return costTypeId;
    }

    public String getCostTypeName() {
        return costTypeName;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public Long getJobId() {
        return jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public String getName() {
        return name;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public void setCostTypeId(Long costTypeId) {
        this.costTypeId = costTypeId;
    }

    public void setCostTypeName(String costTypeName) {
        this.costTypeName = costTypeName;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }
}
