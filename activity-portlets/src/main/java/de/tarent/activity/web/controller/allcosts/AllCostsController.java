/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allcosts;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.CostType;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseCostController;
import de.tarent.activity.web.domain.CostView;
import de.tarent.activity.web.util.EscapeDescriptionUtil;
import de.tarent.activity.web.util.RenderParametersUtil;

/**
 * AllCostsController.
 * 
 * TODO: refactor ResourceMappings
 */
@Controller("AllCostsController")
@RequestMapping(value = "view")
public class AllCostsController extends BaseCostController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllCostsController.class);

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    /**
     * Show Cost add render method.
     *
     * @param model
     *            model
     * @param request
     *            request
     * @return Result page location
     * @throws IOException
     *             IOException
     */
    @RenderMapping(params = "ctx=doCostAdd")
    public String add(Model model, PortletRequest request,
                      @RequestParam(value = "jobId", required = false) Long jobId,
                      @RequestParam(value = "projectId", required = false) Long projectId ) throws IOException {

        populateLists(model);

        CostView costView = new CostView();
        if(jobId != null){
            //costView.setJobId( Long.parseLong( new StringTokenizer(jobId, ",").nextToken()));
            costView.setJobId(jobId);
        }
        if(projectId != null){
            costView.setProjectId(projectId);
        }

        model.addAttribute("costView", costView);

        return "allcosts/edit";
    }

    /**
     *
     * @param model
     *            Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("doCostView")
    public String ajaxView(Model model, PortletRequest request) {
        populateCommonLists(model);

        return "allcosts/view";
    }

    /**
     * Cancel edit cost form.
     *
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return ajaxView
     */
    @ResourceMapping("doCostCancel")
    public String cancel(Model model, PortletRequest request) {
        model.addAttribute("costView", new CostView());
        return ajaxView(model, request);
    }

    /**
     * Details method.
     *
     * @param costPk
     *            Id of the cost to load
     * @param model
     *            model
     * @param request
     *            request
     * @return Result page location
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("doCostDetails")
    public String details(Long costPk, Model model, PortletRequest request) throws IOException {
        Cost cost = costService.getCost(costPk);

        CostView costView = new CostView(cost);
        if (costView != null && costView.getDescription() != null) {
            costView.setDescription(EscapeDescriptionUtil.escapeDescriptionWithoutBr(costView.getDescription()));
        }

        if( costView.getResourceId() != null){
            Resource resource =  resourceService.getResource(costView.getResourceId(), false, false);
            costView.setResourceName( resource.getFirstname()+" "+resource.getLastname());
        }

        model.addAttribute("costView", costView);

        return "allcosts/details";
    }

    /**
     * Edit method.
     *
     * @param costPk
     *            Id of the cost to load
     * @param model
     *            model
     * @param request
     *            request
     * @return Result page location
     * @throws IOException
     *             IOException
     *
     */
    @ResourceMapping("doCostEdit")
    public String edit(Long costPk, Model model, PortletRequest request) throws IOException {
        populateLists(model);

        Cost cost = costService.getCost(costPk);

        CostView costView = new CostView(cost);
        if(((BindingAwareModelMap) model).get("costView") == null) {
            model.addAttribute("costView", costView);
        }

        return "allcosts/edit";
    }

    @RenderMapping(params = "ctx=doCostEdit")
    public String showEdit(Long costPk, Model model, PortletRequest request) throws IOException {
        return "allcosts/edit";
    }

    /**
     * This method adds to the model a jobs list.
     *
     * @param projectIdId
     *            project ID
     * @param isFilter
     *            isFilter
     * @param request
     *            ResourceRequest
     * @param model
     *            Model
     * @return string path for jsp file
     */
    @ResourceMapping("jobs")
    public String jobs(Long projectIdId, Boolean isFilter, ResourceRequest request, Model model) {

        model.addAttribute("jobList", projectService.getJobsByProject(projectIdId));
        model.addAttribute("isFilter", isFilter);

        return "common/jobs";
    }

    private void populateCommonLists(Model model) {
        model.addAttribute("projectList", projectService.getProjectList());
        model.addAttribute("jobList", projectService.getJobList());
    }

    private void populateLists(Model model) {
        populateCommonLists(model);
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.listActiveResources());
        model.addAttribute("costTypeList", costService.getCostTypeList());
    }

    /**
     * Save a cost.
     *
     * @param costView
     *            - cost to save
     * @param result
     *            - BindingResult
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ActionMapping("doCostSave")
    public void save(@ModelAttribute("costView") @Valid CostView costView, BindingResult result,
            PortletRequest request, Model model, ActionResponse response) {

        LOGGER.debug(result.toString());

        if (!result.hasErrors()) {
            Cost cost = new Cost();
            cost.setPk(costView.getId());
            cost.setCost(costView.getCost());
            cost.setCostType(new CostType(costView.getCostTypeId()));
            cost.setDate(costView.getDate());
            cost.setJob(new Job(costView.getJobId()));
            cost.setName(costView.getName());
            cost.setNote(costView.getDescription());
            cost.setResource(new Resource(costView.getResourceId()));

            if (cost.getPk() != null) {
                LOGGER.debug("Updating cost " + cost);
                costService.updateCost(cost);
            } else {
                LOGGER.debug("Adding cost " + cost);
                costService.addCost(cost);
            }
            //Go back to jobs->costs tab after successfull save of a cost
            request.setAttribute("successMessage", "saved");
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobCosts");
            response.setRenderParameter("jobId", costView.getJobId().toString());
        } else {
            populateLists(model);
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doCostEdit");
        }
    }

    /**
     *
     * @param projectId
     *            Selected project Id
     * @param jobId
     *            Selected job Id
     * @param startIndex
     *            Offset
     * @param results
     *            Number of items per page
     * @param sort
     *            Sorted column
     * @param dir
     *            Sort direction
     * @param request
     *            request
     * @param model
     *            model
     * @return Result location
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("doCostTable")
    public String table(Long projectId, Long jobId, Long startIndex, Long results, String sort, String dir,
            ResourceRequest request, Model model) throws IOException {

        LOGGER.debug("project:" + projectId + "\njob" + jobId + "\nsort:" + sort + "\ndir:" + dir
                + "\nstartIndex:" + startIndex + "\nresults:" + results);

        createDataTableResult(projectId, jobId, startIndex, results, sort, dir, model);

        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    //@RenderMapping
    public String view(@RequestParam(required = false) Long publicJobId, Model model, PortletRequest request) {

        if (publicJobId != null) {
            Job job = projectService.getJob(publicJobId);
            model.addAttribute("paramJob", job);

            String linkToJobList = menuPropertiesService.getJobsPageLink();
            model.addAttribute("linkToJobPage", linkToJobList);

            String link = RenderParametersUtil
                    .getPublicPortletUrl(linkToJobList, "publicJobId", job.getPk().toString());
            if (link != null) {
                model.addAttribute("linkToCurrentJobPage", link);
            }
        }

        return ajaxView(model, request);
    }
}
