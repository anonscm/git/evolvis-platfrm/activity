/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allovertime;

import java.io.IOException;
import java.util.*;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import de.tarent.activity.domain.filter.ProjectFilter;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.PermissionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.web.controller.BaseOvertimeController;
import de.tarent.activity.web.domain.OvertimeView;
import de.tarent.activity.web.util.RequestUtil;

/**
 * AllOvertimeController.
 * 
 * TODO: refactor ResourceMappings
 */
@Controller("AllOvertimeController")
@RequestMapping(value = "view")
public class AllOvertimeController extends BaseOvertimeController {

    @Autowired
    @Qualifier("overtimeViewValidator")
    private Validator overtimeViewValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    /**
     * Shows overtime list.
     * 
     * @param model
     *            - Model
     * @param request
     *            - RenderRequest
     * @return the view's name
     */
    @RenderMapping
    public String view(Model model, RenderRequest request, @RequestParam(value = ActivityConstants.CONTEXT_CONSTANT, required = false) String context) {
        if(context != null && context.equals("showCommonDeleteView")){
            return "common/delete";
        }

        return ajaxView(model, request);
    }

    /**
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {

        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_ALL)) {
            populateAllProjectsAndResourcesLists(model);
        } else if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_MANAGER)) {
            populateOnlyResponsibleProjectsAndResourcesLists(model, request);
        }
        return "allovertime/view";
    }

    private void populateAllProjectsAndResourcesLists(Model model) {
        List<Project> projects = getProjectService().getProjectList();
        List<Resource> resources = getResourceService().listActiveResources();

        addProjectsAndResourcesInModel(model, projects, resources);
    }

    private void addProjectsAndResourcesInModel(Model model, List<Project> projects, List<Resource> resources) {
        model.addAttribute("projectList", projects);
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resources);
    }

    private void populateOnlyResponsibleProjectsAndResourcesLists(Model model, PortletRequest request) {
        List<Project> projectsResponsible = getProjectService().listProjectByResponsibleResource(RequestUtil.getLoggedUser(request).getPk());
        List<Resource> resourcesResponsible = sortResourceListByLastName(getResourcesFromSelectedProjects(projectsResponsible));

        addProjectsAndResourcesInModel(model, projectsResponsible, resourcesResponsible);
    }

    private List<Resource> getResourcesFromSelectedProjects(List<Project> projectsResponsible) {
        Set<Resource> set = new HashSet<Resource>();
        List<Resource> resourcesResponsible = new ArrayList<Resource>();

        for(Project project : projectsResponsible){
            List<Resource> projectResourceList = getResourceService().getResourceFromProject(project.getPk());
            for(Resource uniqueResource : projectResourceList){
                if( set.add(uniqueResource)){
                    resourcesResponsible.add(uniqueResource);
                }
            }
        }
        return resourcesResponsible;
    }

    /**
     * Get project list by selected resource.
     * 
     * @param selectedResourceId
     *            - selected resource id
     * @param request
     *            - ResourceRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ResourceMapping("doListProjects")
    public String projects(Long selectedResourceId, ResourceRequest request, Model model) {
        List<Project> projects = new ArrayList<Project>();
        if (selectedResourceId != null && selectedResourceId > 0) {
            projects = getProjectService().listProjectByResource(selectedResourceId);
        } else {
            projects = getProjectService().getProjectList();
        }
        model.addAttribute("projectList", projects);
        model.addAttribute("isFilter", true);
        return "common/projects";
    }

    /**
     * Search overtime by filter.
     * 
     * @param sort
     *            - sort order
     * @param dir
     *            - column to sort
     * @param startIndex
     *            - offset index
     * @param results
     *            - number of rows per page
     * @param selectedResource
     *            - filter value for resource
     * @param projectId
     *            - filter value for project
     * @param typeId
     *            - filter value for overtime type
     * @param startdate
     *            - filter value for start date
     * @param enddate
     *            - filter value for end date
     * @param request
     *            - ResourceRequest
     * @param model
     *            - Model
     * @return the view's name
     * @throws IOException
     *             - if something wrong happens.
     */
    @ResourceMapping("doOvertimeTable")
    public String table(String sort, String dir, Long startIndex, Long results, Long selectedResource,
            Long projectId, Long typeId, Date startdate, Date enddate, ResourceRequest request, Model model)
            throws IOException {
        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_ALL)) {
            createDataTableResult(sort, dir, startIndex, results, selectedResource, projectId, typeId,
                    startdate, enddate, model);
        } else if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_MANAGER)) {
            createDataTableResultForProjectLeader(sort, dir, startIndex, results, selectedResource, projectId, typeId,
                    startdate, enddate, model, request);
        }


        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * Shows insert overtime form.
     * 
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @RenderMapping(params = "ctx=doAddOvertime")
    public String doAddOvertime(PortletRequest request, Model model) {

        if(((BindingAwareModelMap) model).get("overtimeView") == null) {
            OvertimeView overtimeView = new OvertimeView();
            overtimeView.setType(-1L);
            model.addAttribute("overtimeView", overtimeView);
        }

        List<Resource> resources = getResourceService().getResourceList();
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resources);
        return "allovertime/add";
    }

    @RenderMapping(params = "ctx=showAllAddOverTime")
    public String showAllAddOverTime(PortletRequest request, Model model) {
        return doAddOvertime(request, model);
    }


    /**
     * Save a new overtime.
     * 
     * @param overtimeView
     *            - overtime to save
     * @param result
     *            - BindingResult
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ActionMapping("doSaveAddOvertime")
    public void doAdd(@ModelAttribute("overtimeView") @Valid OvertimeView overtimeView, BindingResult result,
            PortletRequest request, Model model, ActionResponse response) {

        overtimeViewValidator.validate(overtimeView, result);
        if (!result.hasErrors()) {
            overtimeView.setType(-1L);
            addOvertime(overtimeView, result, request);
            model.addAttribute("successMessage", "saved");
            model.addAttribute("saved", true);
            //This shows the default render view of this controller after saving
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doCancel");
        } else {
            List<Resource> resources = getResourceService().getResourceList();
            model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resources);
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showAllAddOverTime");
        }

    }

    /**
     * Shows send delete request form for selected overtime.
     * 
     * @param overtimePk
     *            - selected overtime id
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ResourceMapping("doRequestDelete")
    public String doRequestDelete(Long overtimePk, PortletRequest request, Model model) {

        Overtime overtime = getActivityService().getOvertime(overtimePk);
        DeleteRequest deleteRequest = new DeleteRequest(overtime);

        model.addAttribute("deleteRequest", deleteRequest);
        return "common/delete";
    }

    /**
     * Insert delete request.
     * 
     * @param objectId
     *            - delete request object id
     * @param description
     *            - delete request description
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ActionMapping("doSaveDeleteRequest")
    public void doSaveDeleteRequest(Long objectId, String description, PortletRequest request, Model model, ActionResponse response) {

        Overtime overtime = getActivityService().getOvertime(objectId);
        DeleteRequest deleteRequest = new DeleteRequest(overtime);

        if (getResourceService().getRequestByTypeAndId("overtime", objectId) != null) {
            model.addAttribute("errorMessage", "delete_request_already_exist");
            model.addAttribute("deleteRequest", deleteRequest);
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showCommonDeleteView");
        }

        deleteRequest.setController(RequestUtil.getLoggedUser(request));
        deleteRequest.setDescription(description);
        deleteRequest.setCrDate(new Date());
        deleteRequest.setCrUser(RequestUtil.getLoggedUser(request).getFirstname());
        getResourceService().addRequest(deleteRequest);
        
		request.setAttribute("successMessage", "saved");
    }

    /**
     * Cancel send delete request form.
     * 
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @ResourceMapping("doCancelDeleteRequest")
    public String doCancelDeleteRequest(PortletRequest request, Model model) {
        return ajaxView(model, request);
    }

    /**
     * Cancel add overtime form.
     * 
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     */
    @RenderMapping(params = "ctx=doCancel")
    public String doCancel(PortletRequest request, Model model) {
        return ajaxView(model, request);

    }

}
