/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.permissions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.service.PermissionService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.PermissionView;
import de.tarent.activity.web.domain.RoleView;
import de.tarent.activity.web.domain.UserView;
import de.tarent.activity.web.util.DataTableResult;

/**
 * Controller handling the 'Permissions' action.
 * 
 */
@Controller("PermissionsController")
@RequestMapping(value = "view")
public class PermissionsController {

    private static final Map<String, String> ROLE_SORT_ALIAS = new HashMap<String, String>();
    static {
        ROLE_SORT_ALIAS.put("id", "role.pk");
        ROLE_SORT_ALIAS.put("name", "role.name");
        ROLE_SORT_ALIAS.put("description", "role.description");
    }

    private static final Map<String, String> PERMISSION_SORT_ALIAS = new HashMap<String, String>();
    static {
        PERMISSION_SORT_ALIAS.put("id", "perm.pk");
        PERMISSION_SORT_ALIAS.put("actionId", "perm.actionId");
        PERMISSION_SORT_ALIAS.put("description", "perm.description");
    }

    private static final Map<String, String> USERS_SORT_ALIAS = new HashMap<String, String>();
    static {
        USERS_SORT_ALIAS.put("id", "resource.pk");
        USERS_SORT_ALIAS.put("firstName", "resource.firstname");
        USERS_SORT_ALIAS.put("lastName", "resource.lastname");
        USERS_SORT_ALIAS.put("username", "resource.username");
    }

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ObjectMapper objectMapper;

    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return ajaxView(model, request);
    }

    @RenderMapping(params = "ctx=showPermissions")
    public String showPermissions(Model model, RenderRequest request) {
        return ajaxView(model, request);
    }

    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {
        return "permissions/view";
    }

    @ResourceMapping("rolesTable")
    public String rolesTable(String sort, String dir, Long startIndex, Long results, Model model) throws IOException {
        List<Role> roles = permissionService.getAllRoles();

        List<RoleView> list = new ArrayList<RoleView>();
        for (Role r : roles) {
            RoleView rv = new RoleView(r.getPk(), r.getName(), r.getDescription());
            list.add(rv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<RoleView>("role list", null,
                list, Long.valueOf(roles.size()))));
        return ActivityConstants.JSON_CONSTANT;
    }

    @ResourceMapping("assignPermissions")
    public String assignPermissions(Long roleId, Model model, PortletRequest request) throws IOException {
        Role role = permissionService.getRole(roleId);
        model.addAttribute("roleId", roleId);
        model.addAttribute("roleName", role.getName());
        return "permissions/assign-permissions";
    }

    @ActionMapping("savePermissions")
    public void savePermissions(@RequestParam Long roleId, @RequestParam(required = false) Long[] addPermIds,
            @RequestParam(required = false) Long[] removePermIds, ActionRequest request, ActionResponse response) throws IOException {
        try {
            if (addPermIds != null) {
                permissionService.addPermissionsToRole(roleId, addPermIds);
            }
            if (removePermIds != null) {
                permissionService.removePermissionsFromRole(roleId, removePermIds);
            }
            request.setAttribute("successMessage", "saved");
        } catch (Exception e) {
        	request.setAttribute("errorMessage", "saved_error");
        }
    }

    @ActionMapping("saveRoleUsers")
    public void saveUsers(@RequestParam Long roleId, @RequestParam(required = false) Long[] addUserIds,
            @RequestParam(required = false) Long[] removeUserIds, ActionRequest request, ActionResponse response) {
        try {
            if (addUserIds != null) {
                permissionService.addResourcesToRole(roleId, addUserIds);
            }
            if (removeUserIds != null) {
                permissionService.removeResourcesFromRole(roleId, removeUserIds);
            }
            request.setAttribute("successMessage", "saved");
        } catch (Exception e) {
        	request.setAttribute("errorMessage", "saved_error");
        }
    }

    @ResourceMapping("permissionsTable")
    public String permissionsTable(@RequestParam Long roleId,
            @RequestParam(required = false) Long[] preSelectedPermIds, String sort, String dir, Long startIndex,
            Long results, Model model) throws IOException {
        // search all permissions
        PermissionFilter filter = new PermissionFilter(null, null, startIndex, results,
                PERMISSION_SORT_ALIAS.get(sort), dir);

        FilterResult<Permission> permissionsResult = permissionService.searchPermissions(filter);
        List<Permission> permissions = permissionsResult.getRecords();
        Long totalRecords = permissionsResult.getTotalRecords();

        ArrayList<PermissionView> permissionViews = new ArrayList<PermissionView>();
        ArrayList<Long> allPermIds = new ArrayList<Long>();
        buildPermissionsView(permissions, permissionViews, allPermIds);

        getPermissionsAssignedToAGivenRole(roleId, sort, dir, permissionViews, allPermIds);

        updatePermissionViewsWithPreselectedPermissions(preSelectedPermIds, sort, dir, permissionViews);

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<PermissionView>(
                "permission list", null, permissionViews, totalRecords, startIndex, results, dir, sort)));
        return ActivityConstants.JSON_CONSTANT;
    }

    private void updatePermissionViewsWithPreselectedPermissions(Long[] preSelectedPermIds, String sort, String dir, ArrayList<PermissionView> permissionViews) {
        PermissionFilter filter;FilterResult<Permission> permissionsResult;List<Permission> permissions;
        if (preSelectedPermIds != null && preSelectedPermIds.length > 0) {
            for (Long id : preSelectedPermIds) {
                PermissionView view = new PermissionView(id);
                int index = permissionViews.indexOf(view);
                if (index > -1) {
                    permissionViews.get(index).setChecked(true);
                }
            }
            // check if negative ids are in id list, which means that a user defined permission was deselected
            List<Long> negIds = new ArrayList<Long>();
            for (Long id : preSelectedPermIds) {
                if (Long.signum(id) == -1) {
                    negIds.add(id * -1L);
                }
            }
            if (!negIds.isEmpty()) {
                filter = new PermissionFilter(null, null, negIds, PERMISSION_SORT_ALIAS.get(sort), dir);
                permissionsResult = permissionService.searchPermissions(filter);
                permissions = permissionsResult.getRecords();

                for (Permission perm : permissions) {
                    PermissionView view = new PermissionView(perm.getPk());
                    int index = permissionViews.indexOf(view);
                    if (index > -1) {
                        permissionViews.get(index).setChecked(false);
                    }
                }
            }
        }
    }

    private void getPermissionsAssignedToAGivenRole(Long roleId, String sort, String dir, ArrayList<PermissionView> permissionViews, ArrayList<Long> allPermIds) {
        PermissionFilter filter;FilterResult<Permission> permissionsResult;List<Permission> permissions;
        List<Long> roleIds = Arrays.asList(roleId);
        filter = new PermissionFilter(roleIds, null, allPermIds, PERMISSION_SORT_ALIAS.get(sort), dir);
        permissionsResult = permissionService.searchPermissions(filter);
        permissions = permissionsResult.getRecords();
        for (Permission p : permissions) {
            PermissionView view = new PermissionView(p, false, false);
            int index = permissionViews.indexOf(view);
            if (index > -1) {
                permissionViews.get(index).setChecked(true);
            }
        }
    }

    private void buildPermissionsView(List<Permission> permissions, ArrayList<PermissionView> permissionViews, ArrayList<Long> allPermIds) {
        for (Permission p : permissions) {
            permissionViews.add(new PermissionView(p, false, false));

            allPermIds.add(p.getPk());
        }
    }

    @ResourceMapping("assignUsers")
    public String assignUsers(Long roleId, Model model, PortletRequest request) throws IOException {
        Role role = permissionService.getRole(roleId);
        model.addAttribute("roleId", roleId);
        model.addAttribute("roleName", role.getName());
        return "permissions/assign-users";
    }

    @ResourceMapping("usersTable")
    public String usersTable(@RequestParam Long roleId, @RequestParam(required = false) Long[] preSelectedUserIds,
            String sort, String dir, Long startIndex, Long results, Model model) throws IOException {

        // search all resources (user) that are in range of current page
        ResourceFilter filter = new ResourceFilter(null, null, startIndex, results, USERS_SORT_ALIAS.get(sort), dir);
        FilterResult<Resource> result = resourceService.searchResource(filter);
        List<Resource> resources = result.getRecords();
        Long totalResources =  result.getTotalRecords();

        // search all resources (user) that are assigned to given role; we need this to decide if an UserView has
        // checkbox checked or not
        List<Long> resIds = getResourceIds(resources);

        List<Long> roleIds = Arrays.asList(new Long[] { roleId });

        FilterResult<Resource> roleResult = getResourcesAssignedToARole(sort, dir, resIds, roleIds);

        List<Resource> roleResources = roleResult.getRecords();

        // create an UserView for each resource, set checkboxes of UserView's to enabled when user is assigned to given
        // role, or when id of user is within preselected user id's
        ArrayList<UserView> userViews = prepareUserViewsForResources(preSelectedUserIds, resources, roleResources);

        // search all resources that are assigned to given role and that are within previous searched resources
        filter = new ResourceFilter(null, roleIds, resIds, USERS_SORT_ALIAS.get(sort), dir);
        result = resourceService.searchResource(filter);
        resources = result.getRecords();

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<UserView>("user list", null,
                userViews, totalResources, startIndex, results, dir, sort)));
        return ActivityConstants.JSON_CONSTANT;
    }

    private ArrayList<UserView> prepareUserViewsForResources(Long[] preSelectedUserIds, List<Resource> resources, List<Resource> roleResources) {
        ArrayList<UserView> userViews = new ArrayList<UserView>();
        for (Resource r : resources) {
            UserView uv = getDefaultUserView(r, roleResources);
            if (preSelectedUserIds != null && preSelectedUserIds.length > 0) {
                for (Long id : preSelectedUserIds) {
                    if (id.equals(r.getPk())) {
                        uv.setChecked(true);
                        break;
                    } else if (Long.signum(id) == -1 && new Long(id * -1).equals(r.getPk())) {
                        uv.setChecked(false);
                        break;
                    }
                }
            }
            userViews.add(uv);
        }
        return userViews;
    }

    private FilterResult<Resource> getResourcesAssignedToARole(String sort, String dir, List<Long> resIds, List<Long> roleIds) {
        ResourceFilter roleFilter = new ResourceFilter(null, roleIds, resIds, USERS_SORT_ALIAS.get(sort), dir);
        return resourceService.searchResource(roleFilter);
    }

    private List<Long> getResourceIds(List<Resource> resources) {
        List<Long> resIds = new ArrayList<Long>();
        for (Resource res : resources) {
            resIds.add(res.getPk());
        }
        return resIds;
    }

    private UserView getDefaultUserView(Resource r, List<Resource> roleUsers) {
        UserView uv = new UserView();
        uv.setId(r.getPk());
        uv.setUsername(r.getUsername());
        uv.setFirstName(r.getFirstname());
        uv.setLastName(r.getLastname());
        uv.setChecked(roleUsers.contains(r));

        return uv;
    }
}
