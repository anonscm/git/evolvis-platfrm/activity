/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myposition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;

/**
 * MyPosition portlet controller.
 * 
 */
@Controller("MyPositionController")
@RequestMapping(value = "view")
public class MyPositionController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("positionName", "position.name");
        SORT_ALIAS.put("jobName", "job.name");
        SORT_ALIAS.put("positionStatus", "position.positionStatus");
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return "myposition/view";
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping(params="ctx=getMyPositions")
    public String getMyPositions(Model model, RenderRequest request) {
        return view(model, request);
    }

    /**
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     * @throws IOException
     *             IOException
     */
    // TODO this is probably a code duplication of PositionTableController#table()
    @ResourceMapping("table")
    public String table(String sort, String dir, Long startIndex, Long results, ResourceRequest request, Model model)
            throws IOException {
        Long resId = RequestUtil.getLoggedUser(request).getPk();

        PositionFilter filter = new PositionFilter(resId, null, startIndex, results, SORT_ALIAS.get(sort), dir);

        FilterResult<Position> positions = projectService.searchMyPosition(filter);
        Long count = positions.getTotalRecords();
        List<PositionView> list = new ArrayList<PositionView>();
        for (Position p : positions.getRecords()) {
            PositionView pv = new PositionView(p);
            list.add(pv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT,
                objectMapper.writeValueAsString(new DataTableResult<PositionView>("position list", null, list, count)));

        return ActivityConstants.JSON_CONSTANT;
    }

}
