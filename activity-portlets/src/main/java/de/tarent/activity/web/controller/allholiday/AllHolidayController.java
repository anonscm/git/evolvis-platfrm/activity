/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allholiday;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.LossStatus;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.domain.util.TableModel;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ExportService;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.LossView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;
import de.tarent.activity.web.util.TimeHelper;
import net.fortuna.ical4j.data.ParserException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.*;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * AllHoliday portlet controller.
 *
 * TODO: refactor ResourceMappings
 */
@Controller("AllHolidayController")
@RequestMapping(value = "view")
public class AllHolidayController extends BaseController {

	private static final String PAGING_CONFIG = "pagingConfigAllHoliday";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AllHolidayController.class);

	@Autowired
	private ActivityService activityService;

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private MailService mailService;

	@Autowired
	@Qualifier("lossViewValidator")
	private Validator lossViewValidator;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ExportService exportService;

	private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
	static {
		SORT_ALIAS.put("id", "loss.pk");
		SORT_ALIAS.put("startDate", "loss.startDate");
		SORT_ALIAS.put("endDate", "loss.endDate");
		SORT_ALIAS.put("typeLabel", "loss.lossType.name");
		SORT_ALIAS.put("statusLabel", "loss.lossStatusByFkLossStatus.name");
		SORT_ALIAS.put("description", "loss.note");
		SORT_ALIAS.put("days", "loss.days");
		SORT_ALIAS.put("resourceName", "loss.resourceByFkResource.firstname");
	}

	/**
	 * Shows loss list.
	 *
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return view's name
	 */
	@RenderMapping
	public String view(Model model, RenderRequest request) {
		setSessionPagingConfig(model, request, PAGING_CONFIG);
        if(currentUserhasTheRequiredRole(model, request, "Controller")){
            model.addAttribute("currentLoggedInUserRole", "Controller");
        }
		model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.listActiveResources());
		model.addAttribute("lossTypeList", activityService.lossTypeList());
		model.addAttribute("lossStatusList", activityService.lossStatusList());

		return "allholiday/view";
	}


    @RenderMapping(params = "ctx=doCancel")
    public String doCancel(Model model, RenderRequest request) {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        return view(model,  request);
    }

	private List<LossView> getLossViewsList(FilterResult<Loss> loss) {
		List<LossView> list = new ArrayList<LossView>();
		for (Loss o : loss.getRecords()) {
			LossView ov = new LossView(o);
			list.add(ov);
		}
		return list;
	}

	/**
	 * Search loss by filter.
	 *
	 * @param sort
	 *            - sort order
	 * @param dir
	 *            - column to sort
	 * @param startIndex
	 *            - offset index
	 * @param results
	 *            - number of rows per page
	 * @param selectedResource
	 *            - resource id
	 * @param startdate
	 *            - start date
	 * @param enddate
	 *            - end date
	 * @param typeId
	 *            - loss type id
	 * @param statusId
	 *            - loss status id
	 * @param request
	 *            - ResourceRequest
	 * @param model
	 *            - Model
	 * @return view's name
	 * @throws IOException
	 *             - if something wrong happens
	 */
	@ResourceMapping("table")
	public String table(String sort, String dir, Long offset, Long limit,
			Long selectedResource, Date startdate, Date enddate, Long typeId,
			Long statusId, ResourceRequest request, Model model)
			throws IOException {
		PortletSession ps = request.getPortletSession();
        BaseFilter pagingConfig = new BaseFilter(offset, limit, sort, dir);
        ps.setAttribute(PAGING_CONFIG, pagingConfig, PortletSession.APPLICATION_SCOPE);

		LossFilter filter = new LossFilter(selectedResource, startdate,
				enddate, typeId, statusId, null, offset, limit,
				SORT_ALIAS.get(sort), dir);

		FilterResult<Loss> loss = activityService.searchLoss(filter);

		Long count = loss.getTotalRecords();
		List<LossView> list = new ArrayList<LossView>();
		for (Loss o : loss.getRecords()) {
			LossView ov = new LossView(o);
			list.add(ov);
		}

		model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper
				.writeValueAsString(new DataTableResult<LossView>("loss list",
						null, list, count)));

		return ActivityConstants.JSON_CONSTANT;
	}

	/**
	 * Shows send delete request form for selected loss.
	 *
	 * @param lossPk
	 *            - selected loss id
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ResourceMapping("doRequestDelete")
	public String doRequestDelete(Long lossPk, PortletRequest request,
			Model model) {

		Loss loss = activityService.getLoss(lossPk);
		DeleteRequest deleteRequest = new DeleteRequest(loss);

		model.addAttribute("deleteRequest", deleteRequest);
		return "common/delete";
	}

	/**
	 * Insert delete request.
	 *
	 * @param objectId
	 *            - delete request object id to insert
	 * @param description
	 *            - delete request description
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ActionMapping(value = "doSaveDeleteRequest", params = "ctx=deleteRequest")
	public void doSaveDeleteRequest(Long objectId, String description,
			ActionRequest request, ActionResponse response) {

		Loss loss = activityService.getLoss(objectId);
		DeleteRequest deleteRequest = new DeleteRequest(loss);

		if (resourceService.getRequestByTypeAndId("loss", objectId) == null) {
			deleteRequest.setController(RequestUtil.getLoggedUser(request));
			deleteRequest.setDescription(description);
			resourceService.addRequest(deleteRequest);
			request.setAttribute("successMessage", "delete_request_success");
		} else {
			request.setAttribute("errorMessage", "delete_request_already_exist");
		}

	}

	/**
	 * Cancel send delete request form.
	 *
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ResourceMapping("doCancelDeleteRequest")
	public String doCancelDeleteRequest(PortletRequest request, Model model) {

		model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.listActiveResources());
		model.addAttribute("lossTypeList", activityService.lossTypeList());
		model.addAttribute("lossStatusList", activityService.lossStatusList());

		return "allholiday/view";

	}

	/**
	 * Edit loss.
	 *
	 * @param lossPk
	 *            - loss id
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ResourceMapping("doEdit")
	public String doEdit(Long lossPk, PortletRequest request, Model model) {

		Loss loss = activityService.getLoss(lossPk);
		
		model.addAttribute("lossView", new LossView(loss));
		model.addAttribute("lossStatusList", activityService.lossStatusList());

		return "allholiday/edit";

	}

    @RenderMapping(params = "ctx=showEdit")
    public String showEdit(Long lossPk, PortletRequest request, Model model) {
        return doEdit(lossPk, request, model);
    }


    @ResourceMapping("doGetDaysValue")
	public String doGetDaysValue(Date startdate, Date enddate, Model model)
			throws IOException {

		Float lossdays = new Float(0);
		try {
			lossdays = TimeHelper.calculateDays(startdate, enddate);
		} catch (IOException e) {
            LOGGER.error("IOException " + e.getMessage());
		} catch (ParserException e) {
            LOGGER.error("ParserException " + e.getMessage());
		}

		model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(lossdays));

		return ActivityConstants.JSON_CONSTANT;
	}

	/**
	 * Save edited loss.
	 *
	 * @param lossView
	 *            - LossView
	 * @param result
	 *            - BindingResult
	 * @param confirmation
	 *            - save confirmation (in user exceeded the max holiday)
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ActionMapping("doSaveLoss")
	public void doSaveLoss(
			@ModelAttribute("lossView") @Valid LossView lossView,
			BindingResult result, String confirmation, PortletRequest request,
			Model model) {

		model.addAttribute("lossStatusList", activityService.lossStatusList());

		lossViewValidator.validate(lossView, result);

		if (!result.hasErrors()) {
			Loss loss = activityService.getLoss(lossView.getId());

			if (confirmation == null
					&& LossView.LOSS_STATUS_APPROVED.equals(lossView
							.getStatusId())) {
				Resource resource = loss.getResourceByFkResource();
				BigDecimal maxHoliday = new BigDecimal(
						LossView.DEFAULT_MAX_HOLIDAY);
				if (resource.getHoliday() != null) {
					maxHoliday = resource.getHoliday().add(
							resource.getRemainHoliday());
				}
				BigDecimal days = activityService.getLossDaysFromYear(
						resource.getPk(),
						TimeHelper.getYear(lossView.getStartDate())).add(
						lossView.getDays());

				if (days.compareTo(maxHoliday) > 0) {
					model.addAttribute("allHoliday", days);
					// return "allholiday/edit";
				}
			}

			loss.setStartDate(lossView.getStartDate());
			loss.setEndDate(lossView.getEndDate());
			loss.setDays(lossView.getDays());
			loss.setNote(lossView.getDescription());
			loss.setLossStatusByFkLossStatus(new LossStatus(lossView
					.getStatusId()));
			loss.setYear(TimeHelper.getYear(lossView.getStartDate())
					.longValue());

			try {
				if (LossView.LOSS_STATUS_APPROVED
						.equals(lossView.getStatusId())) {
					mailService.sendHolidayApprovedMail(loss);
					mailService
							.sendConfigurableApprovedByControllingLossMail(loss);
				}

				if (LossView.LOSS_STATUS_REJECTED
						.equals(lossView.getStatusId())) {
					mailService.sendHolidayRejectedMail(loss);
					mailService
							.sendConfigurableDeniedByControllingLossMail(loss);
				}
			} catch (Exception e) {
				LOGGER.error("Got an exception trying to send mail", e);
				//TODO: Mailversand noch nicht implementiert
				//request.setAttribute("warningMessage", "send_email_error");
			}

			activityService.updateLoss(loss);
			request.setAttribute("successMessage", "saved");
		}

		model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.listActiveResources());
		model.addAttribute("lossTypeList", activityService.lossTypeList());

	}

	private void populateExcelTableRows(List<LossView> lossViewList,
			List<Object[]> tableLossRows) {
		for (LossView aloss : lossViewList) {
			Object[] oneRow = new Object[8];
			oneRow[0] = aloss.getResourceName();
			oneRow[1] = aloss.getTypeLabel();
			oneRow[2] = aloss.getAnswers();
			oneRow[3] = aloss.getStartDate();
			oneRow[4] = aloss.getEndDate();
			oneRow[5] = aloss.getDays();
			oneRow[6] = aloss.getDescription() == null ? "" : aloss.getDescription();
			oneRow[7] = aloss.getStatusLabel();
			tableLossRows.add(oneRow);

		}
	}

	private String[] populateColumnHeadersForExcelTable(ResourceRequest request) {
		Locale locale = request.getLocale();
		String[] columnHeaders = {
				messageSource.getMessage("resource", null, locale),
				messageSource.getMessage("type", null, locale),
				messageSource.getMessage("answers", null, locale),
				messageSource.getMessage("start_date", null, locale),
				messageSource.getMessage("end_date", null, locale),
				messageSource.getMessage("days", null, locale),
				messageSource.getMessage("description", null, locale),
				messageSource.getMessage("status", null, locale) };
		return columnHeaders;
	}

	@ResourceMapping("getHolidaysExport")
	public void getActivityMyExport(
			@RequestParam(required = false) Long resourceId,
			@RequestParam(required = false) Long statusId,
			@RequestParam(required = false) Long typeId,
			@RequestParam(required = false) Date startDate,
			@RequestParam(required = false) Date endDate,
			ResourceRequest request, ResourceResponse response)
			throws IOException {

		LossFilter lossFilter = new LossFilter(resourceId, startDate, endDate,
				typeId, statusId, null, new Long(0), null,
				SORT_ALIAS.get("resourceName"), "asc");

		FilterResult<Loss> lossList = activityService.searchLoss(lossFilter);

		List<LossView> lossViewList = getLossViewsList(lossList);

		String[] columnHeaders = null;
		List<Object[]> tableLossRows = new ArrayList<Object[]>();

		columnHeaders = populateColumnHeadersForExcelTable(request);
		populateExcelTableRows(lossViewList, tableLossRows);

		response.setContentType("application/octet-stream");
		response.setProperty("Content-Disposition",
				"attachment;filename=Activity.xls");

		TableModel tableModel = new TableModel(columnHeaders, tableLossRows,
				"Holidays", null);
		byte[] export = exportService.doExportExcel(tableModel);
		OutputStream out = response.getPortletOutputStream();

		try {
			out.write(export);
			out.flush();
		} finally {
			out.close();
		}
	}


    @ActionMapping("doMassUpdate")
    public void doMassUpdate(@RequestParam(value = "massUpdateSelect", required = false) String[] massUpdateSelect,
                               @RequestParam(value = "statusId2", required = true) String updateStatus,
                               Model model, PortletRequest request){
        if( massUpdateSelect != null ){
            for ( String lossId : massUpdateSelect){
                Loss loss = activityService.getLoss(new Long(lossId));
                loss.setLossStatusByFkLossStatus(new LossStatus( new Long(updateStatus) ));

                activityService.updateLoss(loss);
            }
			request.setAttribute("successMessage", "saved");
        }
    }

}
