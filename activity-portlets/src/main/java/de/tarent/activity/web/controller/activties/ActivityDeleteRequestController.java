/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.RequestUtil;

//FIXME: Der Controller wird nicht von Spring erkannt
@Controller("ActivityDeleteRequestController")
@RequestMapping(value = "view", params="ctx=deleteRequest")
public class ActivityDeleteRequestController extends BaseController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ResourceService resourceService;
    
    @RenderMapping
    public String view(PortletRequest request, Model model, @RequestParam Long activityPk) {
        Activity activity = activityService.getActivity(activityPk);
        DeleteRequest deleteRequest = new DeleteRequest(activity);

        model.addAttribute("deleteRequest", deleteRequest);
        return "common/delete";
    }

    @ActionMapping("doSaveDeleteRequest")
    public void doSaveDeleteRequest(PortletRequest request, Model model,
    		@RequestParam Long objectId, @RequestParam String description) {

        Activity activity = activityService.getActivity(objectId);
        DeleteRequest deleteRequest = new DeleteRequest(activity);

        if (resourceService.getRequestByTypeAndId("activity", objectId) != null) {
            model.addAttribute("errorMessage", "delete_request_already_exist");
            model.addAttribute("deleteRequest", deleteRequest);
            return;
        }

        deleteRequest.setController(RequestUtil.getLoggedUser(request));
        deleteRequest.setDescription(description);
        deleteRequest.setCrDate(new Date());
        deleteRequest.setCrUser(RequestUtil.getLoggedUser(request).getFirstname());
        resourceService.addRequest(deleteRequest);

        List<Resource> resources = resourceService.listActiveResources();
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resources);
        
        request.setAttribute("successMessage", "delete_request_success");
    }
}
