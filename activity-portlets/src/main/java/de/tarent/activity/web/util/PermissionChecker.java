/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

import javax.portlet.PortletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import de.tarent.activity.service.PermissionService;

/**
 * PermissionChecker is intended to use within Controller code to check permissions of an logged user.
 */
public class PermissionChecker {

    @Autowired
    private PermissionService permissionService;

    /**
     * Checks if currently logged in user has the permission with given action identifier.
     * 
     * @param request
     *            portlet request
     * @param actionId
     *            action identifier of permission
     */
    public void checkAndThrowPermission(PortletRequest request, String... actionId) {
        // We act on the assumption that the AuthInterceptor pre-handled all actions
        // and has already checked that an user is logged in.
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        permissionService.checkAndThrowPermission(resPk, actionId);
    }

    public boolean checkPermission(PortletRequest request, String... actionId) {
        // We act on the assumption that the AuthInterceptor pre-handled all actions
        // and has already checked that an user is logged in.
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        return permissionService.checkPermission(resPk, actionId);
    }
}
