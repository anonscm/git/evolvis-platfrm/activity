/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import net.fortuna.ical4j.data.ParserException;
import de.tarent.kaliko.holidays.ICalHolidayCalendar;

public class TimeHelper {

    private static HashMap<Integer, ICalHolidayCalendar> holidayCalender = new HashMap<Integer, ICalHolidayCalendar>();

    /**
     * Create holiday calendar.
     * 
     * @param year
     *            - year
     * @return holiday calendar
     * @throws IOException
     * @throws ParserException
     */
    private static ICalHolidayCalendar getHolidayCalender(int year) throws IOException, ParserException {

        ICalHolidayCalendar cal = holidayCalender.get(year);

        if (cal == null) {
            cal = new ICalHolidayCalendar(year, TimeHelper.class.getResourceAsStream("feiertage.ics"));

            holidayCalender.put(year, cal);
        }

        return cal;

    }

    /**
     * Hours are rounded to half man-days.
     * 
     * @param hours
     *            - hours
     * @return days
     */
    public static double getWorkDays(double hours) {
        return Math.round(hours / 2d) / 4d;
    }

    /**
     * Hours are rounded to 15 minutes.
     * 
     * @param hours
     *            - hours
     * @return hours
     */
    public static double getHours(double hours) {
        Double rest = hours % 0.25;
        if (rest.doubleValue() != 0.0) {
            Double loss = 0.25 - rest;
            hours += loss;
        }
        return hours;
    }

    /**
     * This method calculates the working days between two dates.
     * 
     * @param startdate
     *            - start date
     * @param enddate
     *            - end date
     * @return countDays - number of working days
     * @throws ParserException
     * @throws IOException
     */
    public static Float calculateDays(Date startdate, Date enddate) throws IOException, ParserException {
        Calendar calstart = Calendar.getInstance();
        Calendar calend = Calendar.getInstance();

        calstart.setTimeZone(TimeZone.getTimeZone("ECT"));
        calend.setTimeZone(TimeZone.getTimeZone("ECT"));

        calstart.setTime(startdate);
        calend.setTime(enddate);

        int countDays = 0;

        ICalHolidayCalendar holidayCal = getHolidayCalender(calstart.get(Calendar.YEAR));
        int year = calstart.get(Calendar.YEAR);

        // Solange das Startdatum ungleich dem Enddatum ist Werktage zählen
        while (calstart.compareTo(calend) != 0 && calstart.compareTo(calend) < 0) {
            if (calstart.get(Calendar.YEAR) != year){
                holidayCal = getHolidayCalender(calstart.get(Calendar.YEAR));
            }
            int value = calstart.get(Calendar.DAY_OF_WEEK);
            if (value != Calendar.SUNDAY && value != Calendar.SATURDAY
                    && !holidayCal.isDateAnHoliday(calstart.getTime())) {
                countDays += 1;
            }
            calstart.add(Calendar.DATE, 1);
        }
        int value = calstart.get(Calendar.DAY_OF_WEEK);
        if (calstart.compareTo(calend) == 0 && !holidayCal.isDateAnHoliday(calstart.getTime())
                && value != Calendar.SUNDAY && value != Calendar.SATURDAY) {
            countDays++;
        }
        return new Float(countDays);
    }

    /**
     * This method checks if calculated on working days with the given value matches.
     * 
     * @param startdate
     *            - start date
     * @param enddate
     *            - end date
     * @param dbDays
     *            - number of days
     * @return working days
     * @throws ParserException
     * @throws IOException
     */
    public static Float checkDays(Date startdate, Date enddate, Float dbDays) throws IOException, ParserException {
        return dbDays - TimeHelper.calculateDays(startdate, enddate);
    }

    /**
     * Get the year for given date.
     * 
     * @param d
     *            - given date
     * @return year
     */
    public static Integer getYear(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("ECT"));
        cal.setTime(d);

        return Integer.valueOf(cal.get(Calendar.YEAR));
    }

    /**
     * Returns a List of workdays inside the given space of time.
     * 
     * @param startdate
     *            first day of space
     * @param enddate
     *            last day of space
     * @return work days list
     * @throws ParserException
     * @throws IOException
     */
    public static List<Date> getWorkdaysAsDateList(Date startdate, Date enddate) throws IOException, ParserException {
        ArrayList<Date> workdays = new ArrayList<Date>();
        do {
            if (calculateDays(startdate, startdate) == 1) {
                workdays.add(startdate);
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(startdate);
            int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
            dayOfMonth++;
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            startdate = cal.getTime();
        } while (startdate.compareTo(enddate) <= 0);

        return workdays;
    }

    /**
     * Calculate one day in ms.
     * 
     * @return one day in ms
     */
    public static long oneDay() {
        return 24l * 60l * 60l * 1000l;
    }

}
