/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.addovertime;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.validation.Valid;

import de.tarent.activity.domain.Overtime;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.web.controller.BaseOvertimeController;
import de.tarent.activity.web.domain.OvertimeView;
import de.tarent.activity.web.util.RequestUtil;

/**
 * AddOvertime portlet controller.
 * 
 */
@Controller("AddOvertimeController")
@RequestMapping(value = "view")
public class AddOvertimeController extends BaseOvertimeController {

    @Autowired
    @Qualifier("overtimeViewValidator")
    private Validator overtimeViewValidator;

    /**
     * Shows add overtime form.
     * 
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @RenderMapping
    public String view(Model model, PortletRequest request) {
        getProjectList(model, request);
        addANewOvertimeInModelButNotOverwriteExisting(model, request);
        return "addovertime/view";
    }

    private void addANewOvertimeInModelButNotOverwriteExisting(Model model, PortletRequest request) {
        OvertimeView overTimeView = getNewOvertimeViewWithResourceIdInitialized(request);
        if(((BindingAwareModelMap) model).get("overtimeView") == null) {
            model.addAttribute("overtimeView", overTimeView);
        }
    }

    private OvertimeView getNewOvertimeViewWithResourceIdInitialized(PortletRequest request) {
        OvertimeView overTimeView =  new OvertimeView();
        overTimeView.setResourceId(RequestUtil.getLoggedUser(request).getPk());
        return overTimeView;
    }

    @RenderMapping(params = "ctx=showAddOvertimeView")
    public String showAddOvertimeView(Model model, PortletRequest request) {
        return view(model, request);
    }

    /**
     * Save a new overtime.
     * 
     * @param overtimeView
     *            - overtime to save
     * @param result
     *            - BindingResult
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     * @throws Exception
     */
    @ActionMapping("doAdd")
    public void doAdd(@ModelAttribute("overtimeView") @Valid OvertimeView overtimeView, BindingResult result,
                      @RequestParam(value = "overtimeViewId", required = false) Long overTimeViewId,
                      ActionRequest request, ActionResponse response, Model model)
            throws Exception {

        overtimeViewValidator.validate(overtimeView, result);

        Overtime o = null;
        if( overTimeViewId != null){
            o = getActivityService().getOvertime( overTimeViewId );
        }

        if (!result.hasErrors()) {
            overtimeView.setResourceId(RequestUtil.getLoggedUser(request).getPk());
            if( o == null){
                addOvertime(overtimeView, result, request);
            }else{
                updateOvertime(overtimeView, o);
            }
            request.setAttribute("successMessage", "saved");
            model.addAttribute("overtimeView", getNewOvertimeViewWithResourceIdInitialized(request));
        }else{
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showAddOvertimeView");
        }
    }

}
