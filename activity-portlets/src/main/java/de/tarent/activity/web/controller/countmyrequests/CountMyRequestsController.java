/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.countmyrequests;

import javax.portlet.PortletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseRequestController;
import de.tarent.activity.web.util.RequestUtil;

/**
 * CountMyRequestsController.
 *
 */
@Controller("CountMyRequestsController")
@RequestMapping(value = "view")
public class CountMyRequestsController extends BaseRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountMyRequestsController.class);

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    /**
     *
     * @param model
     *            Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @RenderMapping
    public String ajaxView(Model model, PortletRequest request) {

        LOGGER.debug("Delete request count");
        DeleteRequestFilter delFilter = new DeleteRequestFilter(RequestUtil.getLoggedUser(request).getPk(), null,
                ResourceService.DELETE_REQUEST_STATUS_OPEN, null, null, null, null);

        // Löschanfragen
        FilterResult<DeleteRequest> result = getResourceService().searchRequest(delFilter);
        model.addAttribute("numberOfRequests", result.getTotalRecords());

        if (result != null && result.getTotalRecords() > 0) {
            model.addAttribute("linkToDeleteRequestsPage", menuPropertiesService.getMyRequestsPageLink());
        }

        // Urlaubsanfragen
        LossFilter filter = new LossFilter(null, null, null, null, null, RequestUtil.getLoggedUser(request).getPk(),
                null, null, null, null);

        FilterResult<Loss> loss = getActivityService().searchLoss(filter);
        model.addAttribute("numberOfLoss", loss.getTotalRecords());

        if ( userHasTheRightToSeeHolidayResponses(model, request) && loss != null && loss.getTotalRecords() > 0) {
            model.addAttribute("linkToLossListPage", menuPropertiesService.getHolidayResponsesPageLink());
        }

        return "countmyrequests/view";
    }

    private boolean userHasTheRightToSeeHolidayResponses(Model model, PortletRequest request) {
        boolean isController = currentUserhasTheRequiredRole(model, request, "Controller");
        boolean isProjectLeader = currentUserhasTheRequiredRole(model, request, "Projektleiter");
        return isController || isProjectLeader;
    }

}
