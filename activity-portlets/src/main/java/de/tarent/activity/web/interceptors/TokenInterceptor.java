/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.interceptors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

import de.tarent.activity.web.util.TokenUtil;

/**
 * 
 * Interceptor - handles CSRF , verifies that a POST request is correct
 * 
 */
public class TokenInterceptor extends HandlerInterceptorAdapter {

    /**
     * This method verifies there is a token in the request and if its value is correct
     * 
     * returns : - true if there is a token in the request and its value is the same as the session stored token value.
     * - false if no token in the request or its value is different from the session stored token value.
     */
    @Override
    public boolean preHandleAction(ActionRequest request, ActionResponse response, Object handler) throws Exception {

        String reqToken = request.getParameter(TokenUtil.REQUEST_TOKEN_KEY);
        if (reqToken != null) {
            return reqToken.equals(request.getPortletSession().getAttribute(TokenUtil.SESSION_TOKEN_KEY));
        }
        return false;

    }

    /**
     * This method generates a new token stored in the request and portletSession
     * 
     * returns true
     */
    @Override
    public boolean preHandleRender(RenderRequest request, RenderResponse response, Object handler) throws Exception {

        // setting token value
        String newToken = TokenUtil.generateToken();
        request.setAttribute(TokenUtil.REQUEST_TOKEN_KEY, newToken);
        request.getPortletSession().setAttribute(TokenUtil.SESSION_TOKEN_KEY, newToken);

        return true;
    }

}
