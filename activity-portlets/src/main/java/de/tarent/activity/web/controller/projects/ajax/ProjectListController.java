/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.ProjectFilter;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.ProjectView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RequestUtil;

@Controller("ProjectListController")
@RequestMapping(value = "view")
public class ProjectListController extends BaseController{
	
	public static String PAGING_CONFIG = "pagingConfigProjects";

	@Autowired
    private ProjectService projectService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PermissionChecker permissionChecker;
    
	private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("description", "project.note");
        SORT_ALIAS.put("idTable", "project.pk");
        SORT_ALIAS.put("projectName", "project.name");
        SORT_ALIAS.put("customerName", "project.customer.pk");
    }

	/**
     * Create project list by filter.
     */
    @ResourceMapping("getProjectTable")
    public String getProjectTable(Long status, Long type, Long clientId, Long offset, Long limit, String sort, String dir,
            ResourceRequest request, Model model) throws IOException {

    	PortletSession ps = request.getPortletSession();
        BaseFilter pagingConfig = new BaseFilter(offset, limit, sort, dir);
        ps.setAttribute(PAGING_CONFIG, pagingConfig, PortletSession.APPLICATION_SCOPE);

        // dependent on permissions of current user we have to list all projects, or only these for that current user is
        // project manager
        ProjectFilter filter = new ProjectFilter(status, type, null, null, clientId, offset, limit,
                SORT_ALIAS.get(sort), dir);

        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_ALL)) {
            filter.setViewBy(ProjectFilter.VIEW_ALL);
        } else if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_MANAGER)) {
            filter.setViewBy(ProjectFilter.VIEW_BY_MANAGER);
            filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
        } else if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW)) {
            filter.setViewBy(ProjectFilter.VIEW_OWN);
            filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
        } else {
            throw new PermissionException("You do not have the permission to view projects.");
        }

        FilterResult<Project> projects = projectService.searchProject(filter);

        Long count = projects.getTotalRecords();
        List<ProjectView> list = new ArrayList<ProjectView>();
        for (Project o : projects.getRecords()) {
            ProjectView ov = new ProjectView(o);
            list.add(ov);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT,
                objectMapper.writeValueAsString(new DataTableResult<ProjectView>("project list", null, list, count)));

        return ActivityConstants.JSON_CONSTANT;
    }
}
