/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.ResourceFTypeMapping;

public class PosResourceMappingView implements Serializable {

    private static final long serialVersionUID = -147848594383837561L;

    private Long id;

    private Long projectId;

    @NotNull(message = "{validate_job_not_empty}")
    private Long jobId;

    private String jobName;

    @NotNull(message = "{validate_position_not_empty}")
    private Long positionId;

    private String positionName;

    @NotNull(message = "{validate_resource_not_empty}")
    private Long resourceId;

    private String resourceName;

    @NotNull(message = "{validate_status_not_empty}")
    private Long statusId;

    private String statusName;

    @DateTimeFormat
    private Date startDate;

    @DateTimeFormat
    private Date endDate;

    @NumberFormat
    @Min(value = 0)
    private Long percent;

    private String role;

    public PosResourceMappingView() {

    }

    public PosResourceMappingView(Long id, Long positionId, Long resourceId, Long statusId, Date startDate,
            Date endDate, Long percent) {
        this.id = id;
        this.positionId = positionId;
        this.resourceId = resourceId;
        this.statusId = statusId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.percent = percent;

    }

    public PosResourceMappingView(Long id, Long positionId, Long resourceId, Long statusId, Date startDate,
            Date endDate, Long percent, Long projectId, Long jobId, String jobName, String positionName,
            String resourceName, String statusName) {
        this(id, positionId, resourceId, statusId, startDate, endDate, percent);
        this.projectId = projectId;
        this.jobId = jobId;
        this.jobName = jobName;
        this.positionName = positionName;
        this.resourceName = resourceName;
        this.statusName = statusName;

    }

    public PosResourceMappingView(PosResourceMapping pr) {
        this(pr.getPk(), pr.getPosition().getPk(), pr.getResource().getPk(), pr.getPosResourceStatus().getPk(), pr
                .getStartDate(), pr.getEndDate(), pr.getPercent(), pr.getPosition().getJob().getProject().getPk(), pr
                .getPosition().getJob().getPk(), "(" + pr.getPosition().getJob().getProject().getName() + ") "
                + pr.getPosition().getJob().getName(), pr.getPosition().getName(), pr.getResource().getLastname()
                + ", " + pr.getResource().getFirstname(), pr.getPosResourceStatus().getName());

        StringBuffer sb = new StringBuffer();

        for (Iterator<ResourceFTypeMapping> iterator = pr.getResource().getResourceFTypeMappings().iterator(); iterator
                .hasNext();) {
            ResourceFTypeMapping item = iterator.next();
            sb.append(item.getFunctionType().getName());
            sb.append(iterator.hasNext() ? ", " : "");
        }
        this.role = sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getPercent() {
        return percent;
    }

    public void setPercent(Long percent) {
        this.percent = percent;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
