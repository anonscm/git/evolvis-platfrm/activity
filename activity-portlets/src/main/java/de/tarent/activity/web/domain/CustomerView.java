/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.web.util.EscapeDescriptionUtil;

/**
 * CustomerView.
 * 
 */
public class CustomerView implements Serializable {

    private static final long serialVersionUID = 2005048264023602404L;

    private Long id;

    private Long idTable;

    @NotEmpty(message = "{validate_name_not_empty}")
    @Size(max = 255)
    private String name;

    @Size(max = 4000)
    private String description;

    @NumberFormat
    private BigDecimal dayrate;

    @DateTimeFormat
    private Date paymentDate;

    public CustomerView() {
    }

    public CustomerView(Long id, String name, String description, BigDecimal dayrate, Date paymentDate,
            boolean escapeDescription) {
        setIdTable(id);
        setId(id);
        setName(name);
        setDescription(description, escapeDescription);
        setDayrate(dayrate);
        setPaymentDate(paymentDate);
    }

    public CustomerView(Customer c, boolean escapeDescription) {
        this(c.getPk(), c.getName(), c.getNote(), c.getDayRate(), c.getPaymentTarget(), escapeDescription);
    }
    
    public Customer getNewCustomerFromView(){
        Customer c = new Customer();
        c.setPk(this.id);
        c.setName(this.name);
        c.setNote(this.description);
        c.setDayRate(this.dayrate);
        c.setPaymentTarget(this.paymentDate);
        
        return c;
    }
    
    public Customer getUpatedCustomerFromView(Customer update){
        update.setName(this.name);
        update.setNote(this.description);
        update.setDayRate(this.dayrate);
        update.setPaymentTarget(this.paymentDate);
        
        return update;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDescription(String description, boolean escapeHtml) {
        this.description = escapeHtml ? EscapeDescriptionUtil.escapeDescriptionWithoutBr(description) : description;
    }

    public BigDecimal getDayrate() {
        return dayrate;
    }

    public void setDayrate(BigDecimal dayrate) {
        this.dayrate = dayrate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

}
