/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.holidayresponses;

import java.io.IOException;
import java.util.*;

import javax.portlet.*;

import de.tarent.activity.domain.Role;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.MailService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.controller.myholiday.MyHolidayController;
import de.tarent.activity.web.domain.LossView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;
import de.tarent.activity.web.util.RequestUtil;

import de.tarent.activity.domain.Resource;

/**
 * HolidayResponses portlet controller.
 *
 */
@Controller("HolidayResponsesController")
@RequestMapping(value = "view")
public class HolidayResponsesController extends BaseController{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MyHolidayController.class);

	@Autowired
	@Qualifier("jndiActivityService")
	private ActivityService activityService;

   	@Autowired
	private MailService mailService;

	private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

	private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
	static {
		SORT_ALIAS.put("id", "loss.pk");
		SORT_ALIAS.put("startDate", "loss.startDate");
		SORT_ALIAS.put("endDate", "loss.endDate");
		SORT_ALIAS.put("typeLabel", "loss.lossType.name");
		SORT_ALIAS.put("statusLabel", "loss.lossStatusByFkLossStatus.name");
		SORT_ALIAS.put("description", "loss.note");
		SORT_ALIAS.put("days", "loss.days");
		SORT_ALIAS.put("resourceName", "loss.resourceByFkResource.firstname");
	}

	/**
	 * Shows loss where current user must answer.
	 *
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return the view's name
	 */
	@RenderMapping
	public String view(Model model, RenderRequest request) {
		LOGGER.info("HolidayResponsesController view ");

        if(currentUserhasTheRequiredRole(model, request, "Projektleiter")){
            model.addAttribute("currentLoggedInUserRole", "Projektleiter");
        }else if(currentUserhasTheRequiredRole(model, request, "Controller")){
            model.addAttribute("currentLoggedInUserRole", "Controller");
        }

        return "holidayresponses/view";
	}

    @RenderMapping(params = "ctx=showHolidayResponses")
    public String showHolidayResponses(Model model, RenderRequest request) {
        return view(model, request);
    }


    /**
	 * Search loss by filter.
	 *
	 * @param sort
	 *            - sort order
	 * @param dir
	 *            - column to sort
	 * @param startIndex
	 *            - offset index
	 * @param results
	 *            - number of rows per page
	 * @param request
	 *            - ResourceRequest
	 * @param model
	 *            - Model
	 * @return view's name
	 * @throws IOException
	 *             - if something wrong happens
	 */
	@ResourceMapping("table")
	public String table(String sort, String dir, Long startIndex, Long results,
			ResourceRequest request, Model model) throws IOException {

		LossFilter filter = new LossFilter(null, null, null, null, null,
				RequestUtil.getLoggedUser(request).getPk(), startIndex,
				results, SORT_ALIAS.get(sort), dir);

		FilterResult<Loss> loss = activityService.searchLoss(filter);

		Long count = loss.getTotalRecords();
		List<LossView> list = new ArrayList<LossView>();
		for (Loss o : loss.getRecords()) {
			LossView ov = new LossView(o);
			list.add(ov);
		}

		model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper
				.writeValueAsString(new DataTableResult<LossView>("loss list",
						null, list, count)));

		return ActivityConstants.JSON_CONSTANT;
	}

	/**
	 * Edit loss.
	 *
	 * @param lossPk
	 *            - loss id
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@RenderMapping(params = "ctx=doEdit")
	public String doEdit(Long lossPk, PortletRequest request, Model model) {

		Loss loss = activityService.getLoss(lossPk);
		model.addAttribute("lossView", new LossView(loss));

		return "holidayresponses/edit";

	}

	/**
	 * Save updated loss.
	 *
	 * @param answerStatusId
	 *            - answer id
	 * @param lossId
	 *            - loss id
	 * @param request
	 *            - PortletRequest
	 * @param model
	 *            - Model
	 * @return the view's name
	 */
	@ActionMapping("doSaveAnswer")
	public void doSaveAnswer(Long answerStatusId, Long lossId,
			PortletRequest request, ActionResponse response, Model model) {
		// fail fast
		if (answerStatusId == null) {
			request.setAttribute("errorMessage", "ui.error.invalidChoice");
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doEdit");
            response.setRenderParameter("lossPk", lossId.toString());
			return;
		}

		Loss loss = activityService.getLoss(lossId);

		if (loss.getResourceByFkPl1() != null
				&& RequestUtil.getLoggedUser(request).getPk()
						.equals(loss.getResourceByFkPl1().getPk())) {
			loss.setLossStatusByFkPl1Status(activityService
					.getLossStatus(answerStatusId));
		}
		if (loss.getResourceByFkPl2() != null
				&& RequestUtil.getLoggedUser(request).getPk()
						.equals(loss.getResourceByFkPl2().getPk())) {
			loss.setLossStatusByFkPl2Status(activityService
					.getLossStatus(answerStatusId));
		}
		if (loss.getResourceByFkPl3() != null
				&& RequestUtil.getLoggedUser(request).getPk()
						.equals(loss.getResourceByFkPl3().getPk())) {
			loss.setLossStatusByFkPl3Status(activityService
					.getLossStatus(answerStatusId));
		}
		if (loss.getResourceByFkPl4() != null
				&& RequestUtil.getLoggedUser(request).getPk()
						.equals(loss.getResourceByFkPl4().getPk())) {
			loss.setLossStatusByFkPl4Status(activityService
					.getLossStatus(answerStatusId));
		}
		if (loss.getResourceByFkPl5() != null
				&& RequestUtil.getLoggedUser(request).getPk()
						.equals(loss.getResourceByFkPl5().getPk())) {
			loss.setLossStatusByFkPl5Status(activityService
					.getLossStatus(answerStatusId));
		}

		activityService.updateLoss(loss);

		try {
			if (LossView.LOSS_STATUS_APPROVED.equals(answerStatusId)) {
				mailService.sendConfigurableApprovedByManagerLossMail(loss);
			}

			if (LossView.LOSS_STATUS_REJECTED.equals(answerStatusId)) {
				mailService.sendConfigurableDeniedByManagerLossMail(loss);
			}
		} catch (Exception e) {
			LOGGER.error("Got an exception trying to send mail", e);
		}

		request.setAttribute("successMessage", "saved");
        response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doEdit");
        response.setRenderParameter("lossPk", lossId.toString());

    }
}
