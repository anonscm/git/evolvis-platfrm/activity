/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.util.HtmlUtils;

import de.tarent.activity.domain.Loss;

public class LossView implements Serializable {

    public static final Long LOSS_STATUS_BOOKED = 4L;

    public static final Long LOSS_STATUS_REJECTED = 3L;

    public static final Long LOSS_STATUS_APPROVED = 2L;

    public static final Long DEFAULT_MAX_HOLIDAY = 28L;

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long resourceId;

    private String resourceName;

    @NotNull(message = "{validate_date_not_empty}")
    @DateTimeFormat
    private Date startDate;

    @NotNull(message = "{validate_date_not_empty}")
    @DateTimeFormat
    private Date endDate;

    private String typeLabel;

    @NotNull(message = "{validate_type_not_empty}")
    private Long typeId;

    private String statusLabel;

    private Long statusId;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    private BigDecimal days;

    private String answers;

    private Long answerStatusId;

    public LossView() {

    }

    public LossView(Long id, Long resourceId, String resourceName, Date startDate, Date endDate, String typeLabel,
            Long typeId, String statusLabel, Long statusId, String description, BigDecimal days, String answers) {
        super();
        this.id = id;
        this.resourceId = resourceId;
        this.resourceName = resourceName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.typeLabel = typeLabel;
        this.typeId = typeId;
        this.statusLabel = statusLabel;
        this.statusId = statusId;
        this.description = description;
        this.days = days;
        this.answers = answers;
    }

    public LossView(Loss loss) {
        this(loss.getPk(), loss.getResourceByFkResource().getPk(), loss.getResourceByFkResource().getFirstname() + " "
                + loss.getResourceByFkResource().getLastname(), loss.getStartDate(), loss.getEndDate(), loss
                .getLossType().getName(), loss.getLossType().getPk(), loss.getLossStatusByFkLossStatus().getName(),
                loss.getLossStatusByFkLossStatus().getPk(), loss.getNote(), loss.getDays(), getString(loss));
    }

    private static String getString(Loss loss) {
        StringBuffer sb = new StringBuffer();

        if (loss.getResourceByFkPl1() != null) {
            String name = loss.getResourceByFkPl1().getFirstname() + " " + loss.getResourceByFkPl1().getLastname();
            String status = loss.getLossStatusByFkPl1Status() != null ? loss.getLossStatusByFkPl1Status().getName()
                    : "";

            sb.append(HtmlUtils.htmlEscape(name) + " : " + status + "<br>");
        }

        if (loss.getResourceByFkPl2() != null) {
            String name = loss.getResourceByFkPl2().getFirstname() + " " + loss.getResourceByFkPl2().getLastname();
            String status = loss.getLossStatusByFkPl2Status() != null ? loss.getLossStatusByFkPl2Status().getName()
                    : "";
            sb.append(HtmlUtils.htmlEscape(name) + " : " + status + "<br>");
        }

        if (loss.getResourceByFkPl3() != null) {
            String name = loss.getResourceByFkPl3().getFirstname() + " " + loss.getResourceByFkPl3().getLastname();
            String status = loss.getLossStatusByFkPl3Status() != null ? loss.getLossStatusByFkPl3Status().getName()
                    : "";
            sb.append(HtmlUtils.htmlEscape(name) + " : " + status + "<br>");
        }

        if (loss.getResourceByFkPl4() != null) {
            String name = loss.getResourceByFkPl4().getFirstname() + " " + loss.getResourceByFkPl4().getLastname();
            String status = loss.getLossStatusByFkPl4Status() != null ? loss.getLossStatusByFkPl4Status().getName()
                    : "";
            sb.append(HtmlUtils.htmlEscape(name) + " : " + status + "<br>");
        }

        if (loss.getResourceByFkPl5() != null) {
            String name = loss.getResourceByFkPl5().getFirstname() + " " + loss.getResourceByFkPl5().getLastname();
            String status = loss.getLossStatusByFkPl5Status() != null ? loss.getLossStatusByFkPl5Status().getName()
                    : "";
            sb.append(HtmlUtils.htmlEscape(name) + " : " + status + "<br>");
        }

        return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public BigDecimal getDays() {
        return days;
    }

    public void setDays(BigDecimal days) {
        this.days = days;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Long getAnswerStatusId() {
        return answerStatusId;
    }

    public void setAnswerStatusId(Long answerStatusId) {
        this.answerStatusId = answerStatusId;
    }

}
