/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.invoices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.service.CostService;
import de.tarent.activity.web.domain.InvoiceView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;

/**
 * InvoiceTableController provides request mappings to handle ajax calls that are related to invoice tables.
 */
@Controller(value = "InvoiceTableController")
@RequestMapping(value = "view")
public class InvoiceTableController {

    /** Sort keys for data table. */
    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("idTable", "invoice.pk");
        SORT_ALIAS.put("jobName", "job.name");
        SORT_ALIAS.put("name", "invoice.name");
        SORT_ALIAS.put("number", "invoice.number");
        SORT_ALIAS.put("amount", "invoice.amount");
        SORT_ALIAS.put("invoiceDate", "invoice.invoiced");
        SORT_ALIAS.put("payDate", "invoice.payed");
    }

    @Autowired
    private CostService costService;

    @Autowired
    private ObjectMapper objectMapper;

    @ResourceMapping("getInvoiceTable")
    public String getInvoiceTable(Long clientId, Long projectId, Long jobId, Integer typeId,
    		String sort, String dir, Long offset, Long limit, ResourceRequest request, Model model)
            throws IOException {

    	PortletSession ps = request.getPortletSession();
        ps.setAttribute(InvoiceController.PAGING_CONFIG, new BaseFilter(offset, limit, sort, dir), PortletSession.APPLICATION_SCOPE);
    	
    	String sortAlias = SORT_ALIAS.get(sort);
        InvoiceFilter invoiceFilter = new InvoiceFilter(clientId, projectId, jobId, typeId,
                offset, limit, sortAlias, dir);
        
        boolean frontendSort = "customerName".equalsIgnoreCase(sort) || "projectName".equalsIgnoreCase(sort);
        
        if(frontendSort){
        	invoiceFilter.setOffset(null);
        	invoiceFilter.setPageItems(null);
        }

        FilterResult<Invoice> result = costService.searchInvoice(invoiceFilter);
        
        List<Invoice> invoices = result.getRecords();
        
        if(frontendSort){
        	invoices = sortList(invoices, sort, dir);
        	int fromIndex = offset.intValue();
        	int toIndex = (int) ((fromIndex + limit) > result.getTotalRecords() ? result.getTotalRecords() : fromIndex + limit); 
        	invoices = invoices.subList(fromIndex, toIndex);
        }
        
        ArrayList<InvoiceView> list = new ArrayList<InvoiceView>();
        for (Invoice invoice : invoices) {
            list.add(new InvoiceView(invoice));
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<InvoiceView>("invoice list",
                null, list, result.getTotalRecords())));

        return ActivityConstants.JSON_CONSTANT;
    }
    
    
    private List<Invoice> sortList(List<Invoice> invoices, String sort, String dir) {
    	Comparator<Invoice> sortInvoiceByCustomerName = new Comparator<Invoice>() {
			@Override
			public int compare(Invoice i1, Invoice i2){
            	String i1Customer = "";
            	String i2Customer = "";
            	
            	if(i1.getJob() != null){
            		i1Customer = i1.getJob().getProject().getCustomer().getName();
            	}
            	else if(i1.getProject() != null){
            		i1Customer = i1.getProject().getCustomer().getName();
            	}
            	
            	if(i2.getJob() != null){
            		i2Customer = i2.getJob().getProject().getCustomer().getName();
            	}
            	else if(i2.getProject() != null){
            		i2Customer = i2.getProject().getCustomer().getName();
            	}
            	
            	return i1Customer.compareToIgnoreCase(i2Customer);
            	
            }
		};
		
		Comparator<Invoice> sortInvoiceByProjectName = new Comparator<Invoice>() {
			@Override
			public int compare(Invoice i1, Invoice i2){
            	String i1ProjectName = "";
            	String i2ProjectName = "";
            	
            	if(i1.getJob() != null){
            		i1ProjectName = i1.getJob().getProject().getName();
            	}
            	else if(i1.getProject() != null){
            		i1ProjectName = i1.getProject().getName();
            	}
            	
            	if(i2.getJob() != null){
            		i2ProjectName = i2.getJob().getProject().getName();
            	}
            	else if(i2.getProject() != null){
            		i2ProjectName = i2.getProject().getName();
            	}
            	
            	return i1ProjectName.compareToIgnoreCase(i2ProjectName);
            }
		};
		
    	if("customerName".equalsIgnoreCase(sort)){
    		Collections.sort(invoices, sortInvoiceByCustomerName);
    	} else if("projectName".equalsIgnoreCase(sort)){
    		Collections.sort(invoices, sortInvoiceByProjectName);
    	}
		
    	if("desc".equalsIgnoreCase(dir)){
			Collections.reverse(invoices);
		}
    	
		return invoices;
	}
}
