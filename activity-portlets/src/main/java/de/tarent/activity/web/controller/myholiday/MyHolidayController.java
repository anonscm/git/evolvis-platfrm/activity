/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myholiday;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import de.tarent.activity.web.util.*;
import net.fortuna.ical4j.data.ParserException;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.LossFilter;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.LossView;

/**
 * MyHoliday portlet controller.
 *
 */
@Controller("MyHolidayController")
@RequestMapping(value = "view")
public class MyHolidayController extends BaseController {

	private static final String PAGING_CONFIG = "pagingConfigMyHoliday";
	
    private static final Logger LOGGER = LoggerFactory.getLogger(MyHolidayController.class);

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private MailService mailService;

    @Autowired
    @Qualifier("lossViewValidator")
    private Validator lossViewValidator;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "loss.pk");
        SORT_ALIAS.put("startDate", "loss.startDate");
        SORT_ALIAS.put("endDate", "loss.endDate");
        SORT_ALIAS.put("typeLabel", "loss.lossType.name");
        SORT_ALIAS.put("statusLabel", "loss.lossStatusByFkLossStatus.name");
        SORT_ALIAS.put("description", "loss.note");
        SORT_ALIAS.put("days", "loss.days");
    }

    /**
     * Shows loss list.
     *
     * @param model
     *            - Model
     * @param request
     *            - RenderRequest
     * @return view's name
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        getLossDetails(model, request);
        setSessionPagingConfig(model, request, PAGING_CONFIG);
        return "myholiday/view";
    }
    
    /**
     * Cancel edit/add loss form.
     *
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return view's name
     */
    @RenderMapping(params = "ctx=doCancel")
    public String doCancel(Model model, RenderRequest request) {
        getLossDetails(model, request);
        setSessionPagingConfig(model, request, PAGING_CONFIG);
        return "myholiday/view";
    }

    /**
     * Get loss details.
     *
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     */
    private void getLossDetails(Model model, PortletRequest request) {
        Resource resource = RequestUtil.getLoggedUser(request);

        BigDecimal contractLossDays = resource.getHoliday();
        BigDecimal lossDaysThisYear = activityService.getLossDaysFromYear(resource.getPk(),
                TimeHelper.getYear(new Date()));
        BigDecimal remainingLossDaysFromLastYear = resource.getRemainHoliday();

        contractLossDays = contractLossDays == null ? BigDecimal.ZERO : contractLossDays;
        lossDaysThisYear = lossDaysThisYear == null ? BigDecimal.ZERO : lossDaysThisYear;
        BigDecimal remainingLossDaysFromThisYear = contractLossDays.subtract(lossDaysThisYear);
        
        BigDecimal remainingLossDaysSum = (remainingLossDaysFromLastYear.add(contractLossDays)).subtract(lossDaysThisYear);
        model.addAttribute("remainingLossDaysSum", remainingLossDaysSum);
        model.addAttribute("contractLossDays", contractLossDays);
        model.addAttribute("lossDaysThisYear", lossDaysThisYear);
        model.addAttribute("remainingLossDaysFromLastYear", remainingLossDaysFromLastYear == null ? 0L
                : remainingLossDaysFromLastYear);
        model.addAttribute("remainingLossDaysFromThisYear", remainingLossDaysFromThisYear);
    }

    /**
     * Search loss by filter.
     *
     * @param sort
     *            - sort order
     * @param dir
     *            - column to sort
     * @param startIndex
     *            - offset index
     * @param results
     *            - number of rows per page
     * @param request
     *            - ResourceRequest
     * @param model
     *            - Model
     * @return view's name
     * @throws IOException
     *             - if something wrong happens
     */
    @ResourceMapping("table")
    public String table(String sort, String dir, Long offset, Long limit, ResourceRequest request, Model model)
            throws IOException {
    	PortletSession ps = request.getPortletSession();
        ps.setAttribute(PAGING_CONFIG, new BaseFilter(offset, limit, sort, dir), PortletSession.APPLICATION_SCOPE);
            LossFilter filter = new LossFilter(RequestUtil.getLoggedUser(request).getPk(), null, null, null, null, null,
                offset, limit, SORT_ALIAS.get(sort), dir);
        FilterResult<Loss> loss = activityService.searchLoss(filter);

        Long count = loss.getTotalRecords();
        List<LossView> list = new ArrayList<LossView>();
        for (Loss o : loss.getRecords()) {
            LossView ov = new LossView(o);
            //Todo: Need a schema change to allow null values in loss entity.
            if(ov.getTypeId() == 2){
                ov.setStatusLabel("");
            }
            list.add(ov);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT,
                objectMapper.writeValueAsString(new DataTableResult<LossView>("loss list", null, list, count)));

        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * Add loss.
     *
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return view's name
     */
    @RenderMapping(params= "ctx=doAddLoss")
    public String doAddLoss(PortletRequest request, Model model) {

    	Resource resource = RequestUtil.getLoggedUser(request);
    	List<Project> projectByResource = projectService.listProjectByResource(resource.getPk());
    	List<Resource> responsibleManager = new ArrayList<Resource>();
    	for (Project p : projectByResource) {
        	if (projectService.isProjectActive(p.getPk())) {
        		Long fkResource = p.getFkResource();
        		if (fkResource != null) {
        			Resource res = resourceService.getResource(fkResource, true, true);
            		if(!responsibleManager.contains(res)){
            			responsibleManager.add(res);
            		}	
        		}        		
        	}
    	}
    	
    	model.addAttribute("responsibleManagerList", sortResourceByLastName(responsibleManager));
        model.addAttribute("lossTypeList", activityService.lossTypeList());
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.getAllProjectManager());

        if(((BindingAwareModelMap) model).get("lossView") == null) {
            model.addAttribute("lossView", new LossView());
        }

        return "myholiday/edit";
    }

    private List<Resource> sortResourceByLastName(List<Resource> resourceList) {
        Collections.sort(resourceList, new Comparator<Resource>()
        {
            public int compare(Resource r1, Resource r2){
                String r1LastName = r1.getLastname()!= null ? r1.getLastname() : "";
                String r2LastName = r2.getLastname() != null ? r2.getLastname(): "";
                if(r2LastName.compareToIgnoreCase(r1LastName)==0 ){
                	String r1FirstName = r1.getFirstname() != null ? r1.getFirstname() : "";
                    String r2FirstName = r2.getFirstname() != null ? r2.getFirstname() : "";
                    return r1FirstName.compareToIgnoreCase(r2FirstName);
                }else{
                	return r1LastName.compareToIgnoreCase(r2LastName);
                }
            }
        });
        return resourceList;
    }
    
    /**
     * Edit loss by given id.
     *
     * @param lossPk
     *            - loss id
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return view's name
     */
    @RenderMapping(params= "ctx=doEdit")
    public String doEdit(Long lossPk, PortletRequest request, Model model) {
    	
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        model.addAttribute("lossTypeList", activityService.lossTypeList());
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.getAllProjectManager());

        Loss loss = activityService.getLoss(lossPk);

        if(((BindingAwareModelMap) model).get("lossView") == null) {
            model.addAttribute("lossView", new LossView(loss));
        }

        model.addAttribute("answersList", getAnswersList((loss)));

        return "myholiday/edit";
    }

    /**
     * Create list of resource for given loss.
     *
     * @param loss
     *            - loss
     * @return list of resource
     */
    private List<Resource> getAnswersList(Loss loss) {
        List<Resource> list = new ArrayList<Resource>();

        if (loss.getResourceByFkPl1() != null) {
            list.add(loss.getResourceByFkPl1());
        }

        if (loss.getResourceByFkPl2() != null) {
            list.add(loss.getResourceByFkPl2());
        }

        if (loss.getResourceByFkPl3() != null) {
            list.add(loss.getResourceByFkPl3());
        }

        if (loss.getResourceByFkPl4() != null) {
            list.add(loss.getResourceByFkPl4());
        }

        if (loss.getResourceByFkPl5() != null) {
            list.add(loss.getResourceByFkPl5());
        }
        return list;
    }

    /**
     * Save loss.
     *
     * @param lossView
     *            LossView
     * @param result
     *            -
     * @param selectedAnswersList
     *            - selected resource
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return view's name
     * @throws ParserException
     * @throws IOException
     */
    @ActionMapping("doSave")
    public void doSave(@ModelAttribute("lossView") @Valid LossView lossView, BindingResult result,
            Long[] selectedAnswersList, PortletRequest request, Model model, ActionResponse response) {

        lossViewValidator.validate(lossView, result);

        if (!result.hasErrors()) {
            if (lossView.getId() != null && lossView.getId() > 0) {
                Loss loss = updateExistingLoss(lossView, request);
                activityService.updateLoss(loss);
            } else {
            	
            	Integer startYear = TimeHelper.getYear(lossView.getStartDate());
                Integer endYear = TimeHelper.getYear(lossView.getEndDate());
                
                //set loss for next year
            	if (!startYear.equals(endYear)) {
            		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            		Date startDate = lossView.getStartDate();
            		try {
						lossView.setStartDate(dateFormat.parse(endYear + "-01-01"));
					} catch (ParseException e) {
						LOGGER.error("Got a ParseException trying to call setStartDate for Loss", e);
					}
            		
            		Loss lossNextYear = addANewLoss(lossView, selectedAnswersList, request);
                    sendEmailNotification(lossNextYear);
                    
                    //set Date for this year
                    lossView.setStartDate(startDate);
                    try {
						lossView.setEndDate(dateFormat.parse(startYear + "-12-31"));
					} catch (ParseException e) {
						LOGGER.error("Got a ParseException trying to call setEndDate for Loss", e);
					}
            	}
            	
                Loss loss = addANewLoss(lossView, selectedAnswersList, request);
                sendEmailNotification(loss);
            }
           	request.setAttribute("successMessage", "saved");
        } else {
            model.addAttribute("lossTypeList", activityService.lossTypeList());
            model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceService.getAllProjectManager());
            model.addAttribute("selectedAnswersList", (selectedAnswersList != null ? getSelectedAnswersListAsString(selectedAnswersList) : null) );
            if( lossView.getId() != null){
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doEdit");
                response.setRenderParameter("lossPk", lossView.getId().toString());
            }else{
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doAddLoss");
            }
        }

    }

    private String getSelectedAnswersListAsString(Long[] selectedAnswersList) {
        StringBuffer returnList = new StringBuffer();
        for(Long item : selectedAnswersList){
            returnList.append (item.toString());
            if( item != selectedAnswersList[selectedAnswersList.length-1]){ returnList.append(","); }
        }
        return returnList.toString();
    }

    private void sendEmailNotification(Loss loss) {
        try {
            mailService.sendHolidayMail(loss);
            if (loss.getLossType().getPk().equals(1L)) {
                mailService.sendConfigurableAddHolidayMail(loss);
            } else {
                mailService.sendConfigurableAddAbsenceMail(loss);
            }
        } catch (Exception e) {
            LOGGER.error("Got an exception trying to send mail", e);
        }
    }

    private Loss addANewLoss(LossView lossView, Long[] selectedAnswersList, PortletRequest request) {
        Loss loss = new Loss();
        setLossValuesFromView(loss, lossView, request);
        loss.setLossStatusByFkLossStatus(activityService.getLossStatus(1L));
        if (selectedAnswersList != null) {
            setLossAnswers(loss, Arrays.asList(selectedAnswersList));
        }

        activityService.addLoss(loss);

        return loss;
    }

    private Loss updateExistingLoss(LossView lossView, PortletRequest request) {
        Loss loss = activityService.getLoss(lossView.getId());
        setLossValuesFromView(loss, lossView, request);
        loss.setLossStatusByFkLossStatus(activityService.getLossStatus(1L));
        loss.setLossStatusByFkPl1Status(null);
        loss.setLossStatusByFkPl2Status(null);
        loss.setLossStatusByFkPl3Status(null);
        loss.setLossStatusByFkPl4Status(null);
        loss.setLossStatusByFkPl5Status(null);
        return loss;
    }

    /**
     * Set loss values from LossView.
     *
     * @param loss
     *            - loss
     * @param lossView
     *            - lossView
     * @param request
     *            - PortletRequest
     * @throws ParserException
     * @throws IOException
     */
    private void setLossValuesFromView(Loss loss, LossView lossView, PortletRequest request) {

        loss.setResourceByFkResource(RequestUtil.getLoggedUser(request));
        loss.setLossType(activityService.getLossType(lossView.getTypeId()));
        loss.setStartDate(lossView.getStartDate());
        loss.setEndDate(lossView.getEndDate());
        loss.setNote(lossView.getDescription());
        try {
            loss.setDays(new BigDecimal(TimeHelper.calculateDays(lossView.getStartDate(), lossView.getEndDate())));
        } catch (IOException e) {
            LOGGER.error("Got a IOException trying to call setDays for Loss", e);
        } catch (ParserException e) {
            LOGGER.error("Got a ParseException trying to call setDays for Loss", e);
        }
        loss.setYear(TimeHelper.getYear(lossView.getStartDate()).longValue());

    }

    /**
     * Set answers for loss.
     *
     * @param loss
     *            - loss
     * @param selectedAnswersList
     *            - list of answers
     */
    private void setLossAnswers(Loss loss, List<Long> selectedAnswersList) {

        // TODO: Refactor me: Liste mit Ressourcen/lossStatus in Loss
    	loss.setResourceByFkPl1(null);
    	loss.setResourceByFkPl2(null);
    	loss.setResourceByFkPl3(null);
    	loss.setResourceByFkPl4(null);
    	loss.setResourceByFkPl5(null);
    	
    	if (selectedAnswersList != null && selectedAnswersList.size() > 0) {
    		switch(selectedAnswersList.size()){
    		case (1): 
    			loss.setResourceByFkPl1(resourceService.getResource(selectedAnswersList.get(0), false, false));
    			break;
    		case 2:
    			loss.setResourceByFkPl1(resourceService.getResource(selectedAnswersList.get(0), false, false));
    			loss.setResourceByFkPl2(resourceService.getResource(selectedAnswersList.get(1), false, false));
    			break;
    		case 3:
    			loss.setResourceByFkPl1(resourceService.getResource(selectedAnswersList.get(0), false, false));
    			loss.setResourceByFkPl2(resourceService.getResource(selectedAnswersList.get(1), false, false));
    			loss.setResourceByFkPl3(resourceService.getResource(selectedAnswersList.get(2), false, false));
    			break;
    		case 4:
    			loss.setResourceByFkPl1(resourceService.getResource(selectedAnswersList.get(0), false, false));
    			loss.setResourceByFkPl2(resourceService.getResource(selectedAnswersList.get(1), false, false));
    			loss.setResourceByFkPl3(resourceService.getResource(selectedAnswersList.get(2), false, false));
    			loss.setResourceByFkPl4(resourceService.getResource(selectedAnswersList.get(3), false, false));
    			break;
    		case 5: 
    			loss.setResourceByFkPl1(resourceService.getResource(selectedAnswersList.get(0), false, false));
    			loss.setResourceByFkPl2(resourceService.getResource(selectedAnswersList.get(1), false, false));
    			loss.setResourceByFkPl3(resourceService.getResource(selectedAnswersList.get(2), false, false));
    			loss.setResourceByFkPl4(resourceService.getResource(selectedAnswersList.get(3), false, false));
    			loss.setResourceByFkPl5(resourceService.getResource(selectedAnswersList.get(4), false, false));
    			break;
    		}
    	}
    }
}
