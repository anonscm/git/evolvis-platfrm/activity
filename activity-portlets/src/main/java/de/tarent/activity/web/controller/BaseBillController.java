/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.InvoiceFilter;
import de.tarent.activity.service.CostService;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.domain.InvoiceView;
import de.tarent.activity.web.util.DataTableResult;

/**
 * BaseBillController.
 * 
 */
public abstract class BaseBillController extends BaseController{

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseBillController.class);

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected CostService costService;

    @Autowired
    protected MailService mailService;

    @Autowired
    protected MenuPropertiesService menuPropertiesService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();

    static {
        SORT_ALIAS.put("idTable", "invoice.pk");
        SORT_ALIAS.put("customerName", "project.customer.name");
        SORT_ALIAS.put("projectName", "project.name");
        SORT_ALIAS.put("jobName", "job.name");
        SORT_ALIAS.put("name", "invoice.name");
        SORT_ALIAS.put("number", "invoice.number");
        SORT_ALIAS.put("amount", "invoice.amount");
        SORT_ALIAS.put("invoiceDate", "invoice.invoiced");
        SORT_ALIAS.put("payDate", "invoice.payed");
    }

    /**
     * @param clientId
     *            clientId
     * @param projectId
     *            projectId
     * @param jobId
     *            jobId
     * @param typeId
     *            typeId
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param model
     *            model
     * @throws IOException
     *             IOException
     */
    protected void createDataTableResult(Long clientId, Long projectId, Long jobId,
            Integer typeId, Long startIndex, Long results, String sort, String dir, Model model)
            throws IOException {
        LOGGER.debug("customer:" + clientId + "\nproject:" + projectId + "\njob" + jobId + "\ntype"
                + typeId + "\nsort:" + sort + "\ndir:" + dir + "\nstartIndex:" + startIndex + "\nresults:"
                + results);

        InvoiceFilter invoiceFilter = new InvoiceFilter(clientId, projectId, jobId, typeId,
                startIndex, results, SORT_ALIAS.get(sort), dir);

        FilterResult<Invoice> result = costService.searchInvoice(invoiceFilter);

        ArrayList<InvoiceView> list = new ArrayList<InvoiceView>();
        for (Invoice invoice : result.getRecords()) {
            list.add(new InvoiceView(invoice));
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<InvoiceView>("invoice list",
                null, list, result.getTotalRecords())));
    }

    /**
     * Save invoice.
     * 
     * @param invoiceView
     *            invoiceView
     * @param model
     *            model
     */
    protected void doSaveInvoice(InvoiceView invoiceView, Model model) {
        Long projectId = invoiceView.getProjectId();
        Long jobId = invoiceView.getJobId();
        Long invoiceId = invoiceView.getId();

        Invoice invoice;

        if (invoiceId != null) {
            invoice = getCostService().getInvoice(invoiceId);

        } else {
            invoice = new Invoice();
        }

        invoiceView.updateInvoiceFromView(invoice);

        if (jobId != null) {
            invoice.setJob(projectService.getJob(jobId));
        } else {
            invoice.setJob(null);
        }
        invoice.setProject(projectService.getProject(projectId));

        // if InvoiceView has set an id we have to update the invoice and send email
        if (invoiceId != null) {
            getCostService().updateInvoice(invoice);

            try {
                mailService.sendConfigurableEditJobInvoiceMail(invoice);
            } catch (Exception e) {
                LOGGER.error("Got an exception trying to send mail", e);
            }
        }
        // otherwise add a new invoice
        else {
            getCostService().addInvoice(invoice);

            try {
                mailService.sendConfigurableAddJobInvoiceMail(invoice);
            } catch (Exception e) {
                LOGGER.error("Got an exception trying to send mail", e);
            }
        }
        model.addAttribute("invoiceView", new InvoiceView());
    }

    /**
     * @return CostService
     */
    protected CostService getCostService() {
        return costService;
    }

    /**
     * @param projectIdId
     *            projectIdId
     * @param isFilter
     *            isFilter
     * @param model
     *            model
     */
    protected void getJobList(Long projectIdId, Boolean isFilter, Model model) {
        model.addAttribute("jobList", projectService.getJobsByProject(projectIdId));
        model.addAttribute("isFilter", isFilter);
    }

    /**
     * @param clientId
     *            clientId
     * @param isFilter
     *            isFilter
     * @param model
     *            model
     */
    protected void getProjectList(Long clientId, Boolean isFilter, Model model) {
        model.addAttribute("projectList", projectService.getProjectsByCustomer(clientId));
        model.addAttribute("isFilter", isFilter);
    }

    /**
     * @return ProjectService
     */
    protected ProjectService getProjectService() {
        return projectService;
    }

    /**
     * @param model
     *            model
     */
    protected void populateLists(Model model) {
        model.addAttribute("customerList", projectService.getCustomerList());
        model.addAttribute("projectList", projectService.getProjectList());
        model.addAttribute("jobList", projectService.getJobList());
    }

    /**
     * @param invoicePk
     *            invoicePk
     * @param model
     *            model
     */
    protected void populateModel(Long invoicePk, Model model) {
        Invoice invoice = costService.getInvoice(invoicePk);

        InvoiceView invoiceView = new InvoiceView(invoice);
        if ((invoice.getJob() != null) && (invoice.getJob().getProject() != null)) {
            invoiceView.setProjectId(invoice.getJob().getProject().getPk());
        }
        model.addAttribute("invoiceView", invoiceView);
    }

    /**
     * @param invoicePk
     *            invoicePk
     * @param model
     *            model
     */
    protected void populateModelForProjectInvoice(Long invoicePk, Model model) {
        Invoice invoice = costService.getProjectInvoice(invoicePk);
        InvoiceView invoiceView = new InvoiceView(invoice);
        model.addAttribute("invoiceView", invoiceView);
    }

    public MenuPropertiesService getMenuPropertiesService() {
        return menuPropertiesService;
    }

}
