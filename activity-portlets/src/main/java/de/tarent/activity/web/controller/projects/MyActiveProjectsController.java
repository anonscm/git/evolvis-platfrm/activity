/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Project;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RenderParametersUtil;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Controller handling the 'My Active Projects' action.
 * 
 */
@Controller("MyActiveProjectsController")
@RequestMapping(value = "view")
public class MyActiveProjectsController  extends BaseController{

    @Autowired
    private ProjectService projectService;

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    @Autowired
    private PermissionChecker permissionChecker;

    @RenderMapping
    public String view(Model model, RenderRequest request) {
    	setSessionPagingConfig(model, request, AllProjectsController.PAGING_CONFIG);
        return ajaxView(model, request);
    }

    private String ajaxView(Model model, PortletRequest request) {
        Long pk = RequestUtil.getLoggedUser(request).getPk();

        List<Project> projects = projectService.listProjectByResource(pk);
        model.addAttribute("projects", projects);

        List<Project> responsibleProjects = projectService.listProjectByResponsibleResource(pk);
        model.addAttribute("responsibleProjects", responsibleProjects);

        // manual check if current user has permission to view all projects
        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_VIEW_ALL)) {
            List<Project> allActiveProjects = projectService.getAllActiveProjectList();
            model.addAttribute("allActiveProjects", allActiveProjects);
        }

        String link = RenderParametersUtil.getPublicPortletUrl(menuPropertiesService.getPublicProjectsPageLink(),
                "publicProjectId", "");
        model.addAttribute("linkToProjectDetailsPage", link);

        String myProject = (String) request.getPortletSession().getAttribute("MY_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("myProject", myProject);

        String responsibleProject = (String) request.getPortletSession().getAttribute("RESPONSIBLE_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("responsibleProject", responsibleProject);

        String allProject = (String) request.getPortletSession().getAttribute("ALL_PROJECT_LIST",
                PortletSession.APPLICATION_SCOPE);
        model.addAttribute("allProject", allProject);

        return "myactiveprojects/view";
    }

    /**
     * Save menu (collapsed or extended).
     * 
     * @param myProject
     *            - settings for my project list
     * @param responsibleProject
     *            - settings for responsible project
     * @param allProject
     *            - setting for all active projects
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("doSaveDetails")
    public String doSaveDetails(String myProject, String responsibleProject, String allProject, Model model,
            PortletRequest request) {
        if (myProject != null) {
            request.getPortletSession().setAttribute("MY_PROJECT_LIST", myProject, PortletSession.APPLICATION_SCOPE);
        }

        if (responsibleProject != null) {
            request.getPortletSession().setAttribute("RESPONSIBLE_PROJECT_LIST", responsibleProject,
                    PortletSession.APPLICATION_SCOPE);
        }

        if (allProject != null) {
            request.getPortletSession().setAttribute("ALL_PROJECT_LIST", allProject, PortletSession.APPLICATION_SCOPE);
        }

        return ajaxView(model, request);
    }

}
