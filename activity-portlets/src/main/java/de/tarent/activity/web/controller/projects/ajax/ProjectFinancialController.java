/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Project;
import de.tarent.activity.service.CostService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseAjaxController;

@Controller("ProjectFinancialController")
@RequestMapping(value = "view")
public class ProjectFinancialController extends BaseAjaxController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CostService costService;

    @ModelAttribute
    public Project populateProject(@RequestParam Long projectId) {
        return projectService.getProject(projectId);
    }

    /**
     * Shows financial details for selected project.
     */
    @ResourceMapping("getProjectFinancialDetails")
    public String getProjectFinancialDetails(@ModelAttribute Project project, Model model, PortletRequest request) {
        List<Invoice> list = costService.getProjectInvoices(project.getPk());

        double sum = 0;
        double payed = 0;
        for (Iterator<Invoice> it = list.iterator(); it.hasNext();) {
            Invoice invoice = it.next();
            if (invoice.getAmount() != null) {
                sum += invoice.getAmount().doubleValue();
                if (invoice.getPayed() != null) {
                    payed += invoice.getAmount().doubleValue();
                }
            }
        }

        model.addAttribute("invoicesum", sum);
        model.addAttribute("invoicesumpayed", payed);

        return "projects/financial";
    }
}
