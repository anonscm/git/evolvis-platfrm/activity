/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;

import de.tarent.activity.domain.Role;

/**
 * Class RolesView acts as a command object and View representation for one {@link Role} entity.
 * 
 */
public class RoleView implements Serializable {

    private static final long serialVersionUID = 611821055490417452L;

    private Long id;

    private String name;

    private String description;

    public RoleView() {
    }

    public RoleView(Long id) {
        this.id = id;
    }

    public RoleView(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public RoleView(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.setDescription(description);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RoleView && ((RoleView) obj).getId().equals(id);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = (int) (31 * result + id);
        return result;
    }
}
