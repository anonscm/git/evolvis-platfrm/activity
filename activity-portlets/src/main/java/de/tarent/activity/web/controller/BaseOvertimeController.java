/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.OvertimeStatus;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.OvertimeFilter;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.OvertimeView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;
import de.tarent.activity.web.util.TimeHelper;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Base controller class for holding common methods used by Overtime portlets
 * controller.
 *
 */
public abstract class BaseOvertimeController extends BaseController{

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "overtime.pk");
        SORT_ALIAS.put("resourceName", "overtime.resource.firstname");
        SORT_ALIAS.put("projectName", "overtime.project.name");
        SORT_ALIAS.put("description", "overtime.note");
        SORT_ALIAS.put("hours", "overtime.hours");
        SORT_ALIAS.put("date", "overtime.date");
    }

    /**
     * Get project list.
     *
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     */
    public void getProjectList(Model model, PortletRequest request) {
        List<Project> projects = projectService.listProjectByResource(RequestUtil.getLoggedUser(request).getPk());
        model.addAttribute("projectList", projects);
    }

    /**
     * Insert an overtime.
     *
     * @param overtimeView
     *            - overtime to insert
     * @param result
     *            - BindingResult
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     */
    public void addOvertime(OvertimeView overtimeView, BindingResult result, PortletRequest request) {

        Resource resource = getResourceService().getResource(overtimeView.getResourceId(), false, false);

        Overtime overtime = new Overtime();
        setOvertimeFromOvertimeView(overtimeView, resource, overtime);

        getActivityService().addOvertime(overtime);
    }

    private void setOvertimeFromOvertimeView(OvertimeView overtimeView, Resource resource, Overtime o) {
        o.setResource(resource);
        o.setHours(overtimeView.getHours().multiply(new BigDecimal(overtimeView.getType())));
        o.setNote(overtimeView.getDescription());
        o.setDate(overtimeView.getDate());
        o.setOvertimeStatus(new OvertimeStatus(0L));
        o.setYear(TimeHelper.getYear(overtimeView.getDate()) != null ? TimeHelper.getYear(overtimeView.getDate())
                .longValue() : 0L);
        if (overtimeView.getType() != 1) {
            overtimeView.setProjectId(null);
        } else if (overtimeView.getProjectId() != null) {
            o.setProject(new Project(overtimeView.getProjectId()));
        }
    }

    /**
     * Updates an Overtime.
     *
     * @param overtimeView
     * @param dbOvertimeToUpdate
     */
    public void updateOvertime(OvertimeView overtimeView, Overtime dbOvertimeToUpdate) {

        Resource resource = getResourceService().getResource(overtimeView.getResourceId(), false, false);

        setOvertimeFromOvertimeView(overtimeView, resource, dbOvertimeToUpdate);

        getActivityService().updateOvertime(dbOvertimeToUpdate);

    }


    /**
     * Create DataTableResult<OvertimeView> for display overtime list.
     *
     * @param sort
     *            - sort order
     * @param dir
     *            - column to sort
     * @param startIndex
     *            - offset index
     * @param results
     *            - number of rows per page
     * @param resourceId
     *            - filter value for resource
     * @param projectId
     *            - filter value for project
     * @param typeId
     *            - filter value for overtime type
     * @param startdate
     *            - filter value for start date
     * @param enddate
     *            - filter value for end date
     * @param model
     *            - Model
     * @throws IOException
     *             - if something wrong happens.
     */
    public void createDataTableResult(String sort, String dir, Long startIndex, Long results, Long resourceId,
            Long projectId, Long typeId, Date startdate, Date enddate, Model model) throws IOException {

        OvertimeFilter filter = new OvertimeFilter(projectId, resourceId, typeId, startdate, enddate,
                startIndex, results, SORT_ALIAS.get(sort), dir);
        FilterResult<Overtime> overtime = getActivityService().searchOvertimes(filter);

        List<OvertimeView> overtimeViewList = convertOvertimeToView(overtime);

        BigDecimal overtimeSum = getActivityService().getOvertimeHoursSumByFilter(filter);

        addOvertimeTableInModel(model, overtime.getTotalRecords(), overtimeViewList, overtimeSum);

    }

    private void addOvertimeTableInModel(Model model, Long totalRecords, List<OvertimeView> overtimeViewList,
                                         BigDecimal overtimeSum) throws IOException {
        model.addAttribute(ActivityConstants.JSON_CONSTANT,
                objectMapper.writeValueAsString(
                        new DataTableResult<OvertimeView>("overtime list", overtimeSum,
                                overtimeViewList,  totalRecords)));
    }

    private  List<OvertimeView> convertOvertimeToView(FilterResult<Overtime> overtime) {
        List<OvertimeView> overtimeViewList = new ArrayList<OvertimeView>();
        for (Overtime o : overtime.getRecords()) {
            OvertimeView ov = new OvertimeView(o);
            overtimeViewList.add(ov);
        }
        return overtimeViewList;
    }

    private  BigDecimal caclucateOvertimeSum(List<OvertimeView> overtimeViewList ) {
        BigDecimal overtimeSum = new BigDecimal(0);
        for (OvertimeView overtimeView : overtimeViewList) {
            overtimeSum = overtimeSum.add( overtimeView.getHours() );
        }
        return overtimeSum;
    }

    public void createDataTableResultForProjectLeader(String sort, String dir, Long startIndex, Long results, Long resourceId,
                                                      Long projectId, Long typeId, Date startdate, Date enddate, Model model, ResourceRequest request) throws IOException {

        FilterResult<Overtime> overtime = null;

        if( projectId != null) {
            createDataTableResult(sort, dir, startIndex, results, resourceId, projectId, typeId, startdate, enddate, model);
            return;
        }else{
            overtime = getOvertimeForOnlyResponsibleProjects(sort, dir, resourceId, typeId, startdate, enddate, request);
        }

        List<OvertimeView> overtimeViewList = convertOvertimeToView(overtime);
        BigDecimal overtimeSum = caclucateOvertimeSum(overtimeViewList);

        addOvertimeTableInModel(model, overtime.getTotalRecords(), overtimeViewList, overtimeSum);

    }

    private FilterResult<Overtime> getOvertimeForOnlyResponsibleProjects(String sort, String dir, Long resourceId, Long typeId, Date startdate, Date enddate, ResourceRequest request) {
        OvertimeFilter filter;
        FilterResult<Overtime> overtime;List<Project> projectsResponsible = getProjectService().listProjectByResponsibleResource(RequestUtil.getLoggedUser(request).getPk());
        List<Overtime> overtimeForAllResponsibleProjects = new ArrayList<Overtime>();
        for( Project project : projectsResponsible){
            filter = new OvertimeFilter(project.getPk(), resourceId, typeId, startdate, enddate,
                    null, null, SORT_ALIAS.get(sort), dir);
            overtimeForAllResponsibleProjects.addAll(getActivityService().searchOvertimes(filter).getRecords());
        }
        overtime = new FilterResult<Overtime>(overtimeForAllResponsibleProjects, new Long(overtimeForAllResponsibleProjects.size()));
        return overtime;
    }

    public ActivityService getActivityService() {
        return activityService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }

    protected List<Resource> sortResourceListByLastName(List<Resource> resourceList) {
        Collections.sort(resourceList, new Comparator<Resource>() {
            public int compare(Resource resource1, Resource resource2) {
                String r1LastName = resource1.getLastname() != null ? resource1.getLastname() : "";
                String r2LastName = resource2.getLastname() != null ? resource2.getLastname() : "";
                return r1LastName.compareTo(r2LastName.toString());
            }
        });
        return resourceList;
    }

}
