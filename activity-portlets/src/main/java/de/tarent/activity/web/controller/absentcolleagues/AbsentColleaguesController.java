/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.absentcolleagues;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.util.RequestUtil;

/**
 * AbsentColleaguesController .
 * 
 */
@Controller("AbsentColleaguesController")
@RequestMapping(value = "view")
public class AbsentColleaguesController extends BaseController{

    private static final Logger LOGGER = LoggerFactory.getLogger(AbsentColleaguesController.class);

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ResourceService resourceService;

    /**
     * Shows absent colleagues from diferent location.
     * 
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @RenderMapping
    public String view(Model model, PortletRequest request) {
        LOGGER.debug("AbsentColleaguesController view");

        Long pkBranch = null;
        try {
            pkBranch = RequestUtil.getLoggedUser(request).getBranchOffice().getPk();
        } catch (Exception e) {
            pkBranch = null;
        }

        String[] locations = this.getLocations(model, pkBranch);

        if (locations != null) {
            model.addAttribute("currentLossList", activityService.getCurrentLossByLocation(new Date(), locations));
        }

        return "absentcolleagues/view";
    }

    @ResourceMapping("doShowAll")
    public String doShowAll(Model model, PortletRequest request) {
        LOGGER.debug("AbsentColleaguesController doShowAll");

        String[] locations = this.getLocations(model, null);

        if (locations != null) {
            model.addAttribute("showAll", ActivityConstants.ALL_CONSTANT);
            model.addAttribute("currentLossList", activityService.getCurrentLossByLocation(new Date(), locations));
        }

        return "absentcolleagues/view";
    }

    private String[] getLocations(Model model, Long pkBranch) {

        String[] locations = null;

        if (pkBranch == null) { // The user can see all employees of all branch offices
            List<BranchOffice> branchOfficeList = resourceService.getBranchOfficeList();
            locations = new String[branchOfficeList.size()];
            for (int i = 0; i < branchOfficeList.size(); i++) {
                locations[i] = branchOfficeList.get(i).getPk().toString();
            }
        } else { // The user can see only the absence employee of the own branch office
            locations = new String[] { pkBranch.toString() };
        }

        return locations;
    }

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    public void setResourceService(ResourceService resourceService) {
       this.resourceService = resourceService;
    }
}
