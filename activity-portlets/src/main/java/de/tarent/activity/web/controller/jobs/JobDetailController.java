/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.jobs;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceResponse;

import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.JobStats;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.util.TableModel;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ExportService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.JobView;

@Controller("JobDetailController")
@RequestMapping(value = "view")
public class JobDetailController extends BaseController {

    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private MessageSource messageSource;
	
	@Autowired
    private ActivityService activityService;
	
	@Autowired
    private ExportService exportService;

    @ModelAttribute("jobView")
    public JobView populateJobView(@RequestParam Long jobId){
        Job job = projectService.getJob(jobId);
        return new JobView(job);
    }

    @ModelAttribute("isDeletableJob")
    public boolean populateIsDeletable(@RequestParam Long jobId) {
        return projectService.isDeletableJob(jobId);
    }

    @RenderMapping(params = "ctx=jobDetails")
    public String details(@ModelAttribute JobView jobView, Model model) throws IOException {
    	JobStats jobStats = projectService.getJobStats(jobView.getId());
        model.addAttribute("jobStats", jobStats);
        model.addAttribute("jobView", jobView);

        return "jobs/details";
    }

    @RenderMapping(params = "ctx=jobInvoices")
    public String viewInvoiceDetails(@RequestParam Long jobId, Model model, PortletRequest request) {
        // need job for breadcrumb navigation and bill-table
        Job job = projectService.getJob(jobId);
        model.addAttribute("paramJob", job);

        return "jobs/invoices";
    }

    @RenderMapping(params = "ctx=jobCosts")
    public String doCosts(@RequestParam Long jobId, Model model, PortletRequest request) {
        // need job for breadcrumb navigation and bill-table
        Job job = projectService.getJob(jobId);
        model.addAttribute("paramJob", job);

        return "jobs/costs";
    }

    @RenderMapping(params = "ctx=jobPositions")
    public String doPositions(@RequestParam Long jobId, Model model, PortletRequest request) {
        // need job for breadcrumb navigation and position-table
        Job job = projectService.getJob(jobId);
        model.addAttribute("paramJob", job);

        return "jobs/positions";
    }

    @RenderMapping(params = "ctx=jobActivities")
    public String doActivities(@ModelAttribute JobView jobView, Model model) {
        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, getPositionViewList(projectService.getPositionsByJob(jobView.getId(), null, null)));
        model.addAttribute("resourcesList", null);
        return "jobs/activities";
    }

    private List<PositionView> getPositionViewList(List<Position> list){
        List<PositionView> positionViewList = new ArrayList<PositionView>();
        for(Position position : list){
            positionViewList.add( new PositionView(position));
        }
        return positionViewList;
    }

    @RenderMapping(params = "ctx=jobChanges")
    public String showChanges(@RequestParam Long jobId, Model model) {
        Job job = projectService.getJob(jobId);

        model.addAttribute("jobView", new JobView(job));
        model.addAttribute("jobChanges", projectService.getJobChangeInfo(jobId));

        return "jobs/changes";
    }
    
    /**
     * Export activities.
     */
    @ResourceMapping("getJobExport")
    public void getProjectExport(Date fromDate, Date toDate, Long jobId, Model model, PortletRequest request,
            ResourceResponse response) throws Exception {

        ActivityFilter filter = new ActivityFilter(null, jobId, null, fromDate, toDate, null, null, null, null,
                null);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        Locale locale = request.getLocale();
        String[] columnHeaders = {
                messageSource.getMessage("id", null, locale),
                messageSource.getMessage("resource", null, locale),
                messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale),
                messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                messageSource.getMessage("description", null, locale),
                messageSource.getMessage("hours", null, locale),
                messageSource.getMessage("date", null, locale) };

        List<Object[]> items = new ArrayList<Object[]>();

        for (Activity activity : activities.getRecords()) {
            Object[] item = new Object[7];
            item[0] = activity.getPk();
            item[1] = activity.getResource() == null ? null : activity.getResource().getName();
            item[2] = activity.getPosition() == null || activity.getPosition().getJob() == null ? null : activity.getPosition().getJob().getName();
            item[3] = activity.getPosition() == null ? null : activity.getPosition().getName();
            item[4] = activity.getName();
            item[5] = activity.getHours();
            item[6] = activity.getDate();

            items.add(item);
        }

        // tell browser program going to return an application file
        // instead of html page
        response.setContentType("application/octet-stream");
        response.setProperty("Content-Disposition", "attachment;filename=Activity.xls");

        OutputStream out = response.getPortletOutputStream();

        try {
            out.write(exportService.doExportExcel(new TableModel(columnHeaders, items, "Activity", null)));
            out.flush();
        } finally {
            out.close();
        }
    }
    
}
