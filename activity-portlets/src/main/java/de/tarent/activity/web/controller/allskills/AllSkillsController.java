/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allskills;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.filter.AllSkillsFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.AllSkillsView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;

/**
 * Controller handling the 'My Skills' action.
 * 
 */
@Controller("AllSkillsController")
@RequestMapping(value = "view")
public class AllSkillsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllSkillsController.class);

    @Autowired
    private ResourceService resourceService;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("resourceName", "skill.resource.firstname");
        SORT_ALIAS.put("skillsDefName", "skill.skillsDef.name");
        SORT_ALIAS.put("value", "skill.value");

    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {

        return "allskills/view";
    }

    /**
     * @param resourceName
     *            resourceName
     * @param skillsDefName
     *            skillsDefName
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param active
     *            active
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(String resourceName, String skillsDefName, String sort, String dir, Long startIndex,
            Long results, String active, Model model, ResourceRequest request) throws IOException {

        LOGGER.debug("sort:" + sort + "\ndir:" + dir + "\nstartIndex:" + startIndex + "\nresults:" + results
                + "\nactive: " + active);

        AllSkillsFilter filter = new AllSkillsFilter(resourceName, skillsDefName, startIndex, results,
                SORT_ALIAS.get(sort), dir);

        FilterResult<Skills> skills = resourceService.searchAllSkills(filter);

        LOGGER.info("Number of skills: " + skills.getTotalRecords());
        LOGGER.info("Number of filtered skills: " + skills.getRecords().size());

        List<AllSkillsView> list = new ArrayList<AllSkillsView>();
        for (Skills s : skills.getRecords()) {
            AllSkillsView sv = new AllSkillsView(s);
            list.add(sv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<AllSkillsView>("skills list",
                null, list, skills.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;
    }

}
