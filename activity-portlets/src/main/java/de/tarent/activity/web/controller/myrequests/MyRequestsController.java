/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myrequests;

import java.io.IOException;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseRequestController;
import de.tarent.activity.web.domain.DeleteRequestView;
import de.tarent.activity.web.util.RequestUtil;

/**
 * MyRequestsController.
 * 
 */
@Controller("MyRequestsController")
@RequestMapping(value = "view")
public class MyRequestsController extends BaseRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyRequestsController.class);

    @Autowired
    private ActivityService activityService;

    /**
     * @param requestId
     *            requestId
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("agree")
    public String agree(Long requestId, Model model, PortletRequest request) {
        DeleteRequest dr = getResourceService().getRequest(requestId);

        Boolean deleted = false;

        if (ResourceService.DELETE_REQUEST_TYPE_ACTIVITY.equalsIgnoreCase(dr.getType())) {
            activityService.deleteActivity(Long.valueOf(dr.getId()));
            deleted = true;
        }

        if (ResourceService.DELETE_REQUEST_TYPE_OVERTIME.equalsIgnoreCase(dr.getType())) {
            activityService.deleteOvertime(Long.valueOf(dr.getId()));
            deleted = true;
        }

        if (ResourceService.DELETE_REQUEST_TYPE_LOSS.equalsIgnoreCase(dr.getType())) {
            activityService.deleteLoss(Long.valueOf(dr.getId()));
            deleted = true;
        }

        if (deleted) {
            dr.setStatus(ResourceService.DELETE_REQUEST_STATUS_DELETED);
            getResourceService().updateRequest(dr);
            LOGGER.debug("Deleted " + dr.getType() + " with id: " + dr.getId());
        }

        return ajaxView(model, request);
    }

    /**
     * 
     * @param model
     *            Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {

        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, getResourceService().getResourceList());

        return "myrequests/view";
    }

    /**
     * Show details page.
     * 
     * @param requestPk
     *            DeleteRequest id
     * @param model
     *            Model
     * @param request
     *            - PortletRequest
     * @return ajaxView
     * 
     */
    @ResourceMapping("details")
    public String details(Long requestPk, Model model, PortletRequest request) {

        LOGGER.debug("Fetching DeleteRequest with id: " + requestPk);
        DeleteRequest dr = getResourceService().getRequest(requestPk);
        DeleteRequestView requestView = new DeleteRequestView(dr);

        model.addAttribute("drView", requestView);

        return "myrequests/details";
    }

    /**
     * @param requestId
     *            requestId
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("disagree")
    public String disagree(Long requestId, Model model, PortletRequest request) {

        DeleteRequest dr = getResourceService().getRequest(requestId);
        dr.setStatus(ResourceService.DELETE_REQUEST_STATUS_CANCELED);
        getResourceService().updateRequest(dr);

        LOGGER.debug("Delete request with id " + requestId + " was not accepted.");

        return ajaxView(model, request);
    }

    /**
     * 
     * @param startIndex
     *            Offset
     * @param results
     *            Number of items per page
     * @param sort
     *            Sorted column
     * @param dir
     *            Sort direction
     * @param request
     *            request
     * @param model
     *            model
     * @return Result location
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(Long startIndex, Long results, String sort, String dir, ResourceRequest request, Model model)
            throws IOException {

        createDataTableResult(RequestUtil.getLoggedUser(request).getPk(), null,
                ResourceService.DELETE_REQUEST_STATUS_OPEN, startIndex, results, sort, dir, model);

        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return ajaxView(model, request);
    }

    @RenderMapping(params = "ctx=getMyRequests")
    public String getMyRequests(Model model, RenderRequest request) {
        return view(model, request);
    }
}
