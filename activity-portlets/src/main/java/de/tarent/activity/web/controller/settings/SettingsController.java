/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.settings;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Settings;
import de.tarent.activity.service.SettingsService;
import de.tarent.activity.web.domain.SettingsView;

/**
 * 
 * SettingsController.
 * 
 */
@Controller("SettingsController")
@RequestMapping(value = "view")
public class SettingsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsController.class);

    @Autowired
    private SettingsService settingsService;

    @RenderMapping
    public String view(Model model, RenderRequest request) {
        LOGGER.info("SettingsController view ");
        setModel(model);
        return "settings/view";
    }

    @ResourceMapping("doDetailsGeneralAddresses")
    public String detailsGeneralAddresses(Model model, PortletRequest request) {
        setModel(model);
        return "settings/view";
    }

    @ResourceMapping("doNews")
    public String news(Model model, PortletRequest request) {
        setModel(model);
        return "settings/news";
    }

    private void setModel(Model model) {
        model.addAttribute("settingsView", new SettingsView(settingsService.getSettings()));
    }

    @ResourceMapping("doSave")
    public String doSave(@ModelAttribute("settingsView") SettingsView settingsView, Model model, PortletRequest request) {

        Settings resourceManagementMail = settingsService.getSettingsByKey(SettingsView.RESOURCE_MANAGEMENT_MAIL);
        if (resourceManagementMail != null) {
            resourceManagementMail.setValue(settingsView.getResourceManagementMail());
        } else {
            resourceManagementMail = new Settings(null, SettingsView.RESOURCE_MANAGEMENT_MAIL,
                    settingsView.getResourceManagementMail());
        }
        settingsService.saveSetting(resourceManagementMail);

        Settings personnelOfficeMail = settingsService.getSettingsByKey(SettingsView.PERSONNEL_OFFICE_MAIL);
        if (personnelOfficeMail != null) {
            personnelOfficeMail.setValue(settingsView.getPersonnelOfficeMail());
        } else {
            personnelOfficeMail = new Settings(null, SettingsView.PERSONNEL_OFFICE_MAIL,
                    settingsView.getPersonnelOfficeMail());
        }
        settingsService.saveSetting(personnelOfficeMail);

        request.setAttribute("successMessage", "saved");
        setModel(model);
        return "settings/view";
    }

    @ResourceMapping("doSaveNews")
    public String doSaveNews(@ModelAttribute("settingsView") SettingsView settingsView, Model model,
            PortletRequest request) {
        Settings welcomeMessage = settingsService.getSettingsByKey(SettingsView.WELCOME_MESSAGE);
        if (welcomeMessage != null) {
            welcomeMessage.setValue(settingsView.getWelcomeMessage());
        } else {
            welcomeMessage = new Settings(null, SettingsView.WELCOME_MESSAGE, settingsView.getWelcomeMessage());
        }
        settingsService.saveSetting(welcomeMessage);

        setModel(model);
        return "settings/news";
    }

}
