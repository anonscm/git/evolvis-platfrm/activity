/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.workedhours;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.portlet.RenderRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.service.ActivityService;
import de.tarent.activity.web.util.RequestUtil;

/**
 * WorkedHours portlet controller.
 *
 */
@Controller("WorkedHoursController")
@RequestMapping(value = "view")
public class WorkedHoursController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkedHoursController.class);

    @Autowired
    private ActivityService activityService;

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        LOGGER.info("WorkedHoursController view ");

        final long oneDay = (24l * 60l * 60l * 1000l);

        Calendar cal = new GregorianCalendar(Locale.GERMANY);
        Date now = cal.getTime();
        int daysSinceStart = cal.get(Calendar.DAY_OF_WEEK) - 1;
        Date startOfThisWeek = new Date(now.getTime() - (daysSinceStart * oneDay));
        Date startOfNextWeek = new Date(startOfThisWeek.getTime() + 7 * oneDay);
        Date endDate = new Date(now.getTime());

        BigDecimal hoursThisWeek = activityService.getHoursFromInterval(RequestUtil.getLoggedUser(request).getPk(),
                startOfThisWeek, startOfNextWeek);
        model.addAttribute("hoursThisWeek", hoursThisWeek);

        BigDecimal hourscomplete = activityService.getHoursFromInterval(RequestUtil.getLoggedUser(request).getPk(), null,
                endDate);
        model.addAttribute("hourscomplete", hourscomplete);

        Date startOfLastWeek = new Date(startOfThisWeek.getTime() - 7 * oneDay);

        BigDecimal hoursLastWeek = activityService.getHoursFromInterval(RequestUtil.getLoggedUser(request).getPk(),
                startOfLastWeek, startOfThisWeek);
        model.addAttribute("hoursLastWeek", hoursLastWeek);

        BigDecimal overtimeHours = activityService.getAllOvertimeSum(RequestUtil.getLoggedUser(request).getPk());
        model.addAttribute("overtimeHours", overtimeHours);

        return "workedhours/view";
    }

}
