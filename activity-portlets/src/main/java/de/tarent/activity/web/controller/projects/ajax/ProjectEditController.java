/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.ProjectView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import java.util.List;

/**
 * ProjectEditController provides request mappings for portlets to add or edit projects.
 * 
 * <p>
 * All request mappings are @ResourceMapping's so that they are only usable for ajax calls.
 * 
 * <p>
 * Recurrent model attributes are added to model by the @ModelAttribute annotated methods before any request method will
 * be invoked. These attributes are <code>customerList</code> and <code>resourceList</code>.
 */
@Controller("ProjectEditController")
@RequestMapping(value = "view")
public class ProjectEditController extends BaseAjaxController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private PermissionChecker permissionChecker;

    /**
     * Populate list of all customers to model, for drop-down select box.
     */
    @ModelAttribute("customerList")
    public List<Customer> populateCustomer() {
        return projectService.getCustomerList();
    }

    /**
     * Populate list of all resources to model, for drop-down select box.
     */
    @ModelAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT)
    public List<Resource> populateResources() {
        return resourceService.listActiveResources();
    }

    @ModelAttribute("projectView")
    public ProjectView populateProjectView(@RequestParam(required = false) Long projectId,
            @RequestParam(required = false) Long clientId, Model model) {

        if(((BindingAwareModelMap) model).get("projectView") == null) {

            return getProjectViewEitherymptyOrFromDb(projectId, clientId);

        }else{
            return (ProjectView) ((BindingAwareModelMap) model).get("projectView");
        }
    }

    private ProjectView getProjectViewEitherymptyOrFromDb(Long projectId, Long clientId) {
        ProjectView projectView;
        if (projectId != null) {
            Project p = projectService.getProject(projectId);
            projectView = new ProjectView(p);
        } else {
            projectView = new ProjectView();
        }

        if (clientId != null) {
            projectView.setClientId(clientId);
        }
        return projectView;
    }

    @ResourceMapping("getProjectEdit")
    public String getProjectEdit(@RequestParam Long projectId, Model model, PortletRequest request) {
        Long resId = RequestUtil.getLoggedUser(request).getPk();
        Project project = null;

        if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_EDIT_ALL)) {
            project = projectService.getProject(projectId);
        } else if (permissionChecker.checkPermission(request, PermissionIds.PROJECT_EDIT)) {
            project = projectService.getProject(projectId);
            if (!project.getFkResource().equals(resId)) {
                throw new PermissionException("You don't have the permission to edit this project.");
            }
        } else {
            throw new PermissionException("You don't have the permission to edit this project.");
        }

        return "projects/edit";
    }

    @RenderMapping(params = "ctx=getProjectEdit")
    public String getProjectRenderEdit(@RequestParam Long projectId, Model model, PortletRequest request) {
        return getProjectEdit(projectId,  model, request);
    }

    @RenderMapping(params = "ctx=getProjectAdd")
    public String getProjectAdd(Model model) {
        return "projects/edit";
    }

    @RenderMapping(params = "ctx=getProjectEditCancel")
    public String getProjectEditCancel(PortletRequest request, Model model) {
    	setSessionPagingConfig(model, request, ProjectListController.PAGING_CONFIG);
        return "projects/view";
    }

}
