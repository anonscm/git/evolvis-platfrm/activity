/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.myovertime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import de.tarent.activity.domain.Overtime;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.web.domain.OvertimeView;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.web.controller.BaseOvertimeController;
import de.tarent.activity.web.util.RequestUtil;

/**
 * MyOvertime portlet controller.
 * 
 */
@Controller("MyOvertimeController")
@RequestMapping(value = "view")
public class MyOvertimeController extends BaseOvertimeController {

	private static final String PAGING_CONFIG = "pagingConfigMyOvertime";
	
    /**
     * Shows user overtime list.
     * 
     * @param model
     *            - Model
     * @param request
     *            - RenderRequest
     * @return the view's name
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        getProjectList(model, request);
        model.addAttribute("allHours",
                getActivityService().getAllOvertimeSum(RequestUtil.getLoggedUser(request).getPk()));
        setSessionPagingConfig(model, request, PAGING_CONFIG);
        return "myovertime/view";
    }

    @RenderMapping(params = "ctx=getMyOvertimeTable")
    public String showMyOvertimeTable(Model model, RenderRequest request){
        return view(model, request);
    }

    @RenderMapping( params = "ctx=showEdit")
    public String showEdit(Long overtimePk, Model model, RenderRequest request) {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
    	getProjectList(model, request);
        OvertimeView overtimeView = getOvertimeViewFromPersistence(overtimePk);
        model.addAttribute("overtimeView", overtimeView);
        return "addovertime/view";
    }

    private OvertimeView getOvertimeViewFromPersistence(Long overtimePk) {
        Overtime overtime = getActivityService().getOvertime(overtimePk);
        OvertimeView overtimeView = new OvertimeView(overtime);
        overtimeView.setId( overtime.getPk());
        if(overtimeView.getType() == -1){ //if it is leave hours then give absolute value to the view
            overtimeView.setHours( new BigDecimal( Math.abs(overtimeView.getHours().doubleValue()) ));
        }
        return overtimeView;
    }


    /**
     * Search overtime by filter.
     * 
     * @param sort
     *            - sort order
     * @param dir
     *            - column to sort
     * @param startIndex
     *            - offset index
     * @param results
     *            - number of rows per page
     * @param projectId
     *            - filter value for project
     * @param typeId
     *            - filter value for overtime type
     * @param startdate
     *            - filter value for start date
     * @param enddate
     *            - filter value for end date
     * @param request
     *            - ResourceRequest
     * @param model
     *            - Model
     * @return the view's name
     * @throws IOException
     *             - if something wrong happens.
     */
    @ResourceMapping("table")
    public String table(String sort, String dir, Long offset, Long limit, Long projectId,
            Long typeId, Date startdate, Date enddate, ResourceRequest request, Model model) throws IOException {
    	PortletSession ps = request.getPortletSession();
        ps.setAttribute(PAGING_CONFIG, new BaseFilter(offset, limit, sort, dir), PortletSession.APPLICATION_SCOPE);
    	
        createDataTableResult(sort, dir, offset, limit, RequestUtil.getLoggedUser(request).getPk(),
                projectId, typeId, startdate, enddate, model);
        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * Delete selected overtime.
     * 
     * @param overtimePk
     *            - overtime id
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return the view's name
     * @throws IOException
     *             IOException
     */
    @ActionMapping("doDelete")
    public void doDelete(Long overtimePk, PortletRequest request, Model model) throws IOException {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        getActivityService().deleteOvertimeAndRequest(overtimePk);
        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString("success"));

        request.setAttribute("successMessage", "deleted");
    }
}
