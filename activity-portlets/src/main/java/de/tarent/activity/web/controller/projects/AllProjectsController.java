/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects;

import de.tarent.activity.domain.*;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.domain.ProjectView;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Projects portal controller.
 * 
 */
@Controller("AllProjectsController")
@RequestMapping(value = "view")
public class AllProjectsController extends BaseController{
	
	public static String PAGING_CONFIG = "pagingConfigProjects";

    @Autowired
    private ProjectService projectService;

    @ModelAttribute(ActivityConstants.STATUS_LIST_CONSTANT)
    public List<JobStatus> populateStatusList() {
        return projectService.getJobStatusList();
    }

    @ModelAttribute("customerList")
    public List<Customer> populateCustomer() {
        return projectService.getCustomerList();
    }

    /**
     * Shows project list.
     */
    @RenderMapping(params = { "!publicProjectId" })
    public String view(PortletRequest request, Model model) {
        return ajaxView(request, model);
    }

    @RenderMapping(params = "ctx=getAllProjectsTable")
    public String showProjectTable(PortletRequest request, Model model){
        return ajaxView(request, model);
    }

    @RenderMapping(params = "ctx=getProjectDetailsCancel")
    public String getProjectDetailsCancel(PortletRequest request, Model model){
        return ajaxView(request, model);
    }

    @ResourceMapping("ajaxView")
    public String ajaxView(PortletRequest request, Model model) {
        setSessionPagingConfig(model, request, PAGING_CONFIG);
        return "projects/view";
    }

    //Deletes Positions 
    @ActionMapping("doDelete")
    public void doDelete(Long id, Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, new PositionView());
        projectService.deletePosition(id);

        request.setAttribute("successMessage", "deleted");
    }
    
    @ResourceMapping("doProjectDelete")
    public String doProjectDelete(Long projectId, Model model, PortletRequest request) {
        projectService.deleteProject(projectId);

        request.setAttribute("successMessage", "deleted");
        return ajaxView(request, model);
    }

    /**
     * Cancel add/edit project form.
     */
    @ResourceMapping("getProjectCancel")
    public String getProjectCancel(@RequestParam(required = false) Long projectId, Model model, PortletRequest request) {
        return ajaxView(request, model);
    }

    @ResourceMapping("showChanges")
    public String showChanges(@RequestParam Long projectId, Model model, PortletRequest request) throws IOException {
        Project project = projectService.getProject(projectId);
        ProjectView projectView = new ProjectView(project);
        model.addAttribute("projectView", projectView);
        List<ChangeInfo> changeInfo = projectService.getProjectChangeInfo(projectId);
        model.addAttribute("projectChanges", changeInfo);

        return "projects/changes";
    }
}
