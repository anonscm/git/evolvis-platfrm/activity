/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.allusers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.domain.Employment;
import de.tarent.activity.domain.FunctionType;
import de.tarent.activity.domain.Permission;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceFTypeMapping;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PermissionFilter;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.PermissionService;
import de.tarent.activity.service.PropertiesService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.PermissionView;
import de.tarent.activity.web.domain.RoleView;
import de.tarent.activity.web.domain.RolesView;
import de.tarent.activity.web.domain.UserDetailsView;
import de.tarent.activity.web.domain.UserView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Controller handling the 'My Activities' action.
 *
 */
@Controller("AllUsersController")
@RequestMapping(value = "view")
public class AllUsersController extends BaseController{

    @Autowired
    PropertiesService propertiesService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AllUsersController.class);

    private static final String PAGING_CONFIG = "pagingConfigAllUsers";
    
    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("idTable", "resource.pk");
        SORT_ALIAS.put("firstName", "resource.firstname");
        SORT_ALIAS.put("lastName", "resource.lastname");
        SORT_ALIAS.put("username", "resource.username");
        SORT_ALIAS.put("id", "perm.pk");
    }

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    @Qualifier("userViewValidator")
    private Validator userViewValidator;

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Default render method.
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return ajaxView(model, null, request);
    }

    /**
     * Initial resource permissions view rendering.
     *
     */
    @RenderMapping(params = "ctx=editPermissions")
    public String editPermissions(@RequestParam(required = true, value = "resId") Long resId, Model model,
                                  PortletRequest request) {
        Resource resource = resourceService.getResource(resId, false, false);
        model.addAttribute("resId", resId);
        model.addAttribute("resName", resource.getFirstname() + " " + resource.getLastname());
        return "allusers/permissions";
    }


    @RenderMapping(params="ctx=showAllUsersList")
    public String showAllUsersList(Model model, RenderRequest request) {
        return ajaxView(model, null, request);
    }

    /**
     * @param model
     *            model
     * @param id
     *            id
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, Long id, PortletRequest request) {
        
    	String linkToUserList = menuPropertiesService.getResourcesPageLink();
        model.addAttribute("linkToUserList", linkToUserList);

        setAdvancedPagingConfig(model, request);

        if (id != null) {
            return "allusers/edit";
        }
        return "allusers/view";
    }

    /**
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param active
     *            active
     * @param model
     *            model
     * @return String
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(String sort, String dir, Long offset, Long limit, Character active, 
    		PortletRequest request, Model model)
            throws IOException {

        PortletSession ps = request.getPortletSession();
        ps.setAttribute(PAGING_CONFIG, new BaseFilter(offset, limit, sort, dir), PortletSession.APPLICATION_SCOPE);
        
        ps.setAttribute(ACTIVE, active, PortletSession.APPLICATION_SCOPE);
        
        ResourceFilter dbFilter = new ResourceFilter(active, null, offset, limit, SORT_ALIAS.get(sort), dir);
        FilterResult<Resource> resources = resourceService.searchResource(dbFilter);

        LOGGER.info("Number of resources: " + resources.getTotalRecords());
        LOGGER.info("Number of filtered resources: " + resources.getRecords().size());

        List<UserView> list = new ArrayList<UserView>();
        for (Resource r : resources.getRecords()) {
            UserView uv = new UserView(r);
            list.add(uv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<UserView>("resource list", null,
        		list, resources.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * @param id
     *            id
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @RenderMapping(params = "ctx=edit")
    public String edit(Long id, Model model, PortletRequest request) throws IOException {
        Resource resource = resourceService.getResource(id, false, false);
        // check if logged user tries to edit him self
        if (id.equals(RequestUtil.getLoggedUser(request).getPk())) {
            model.addAttribute("canEdit", "f");
            setAdvancedPagingConfig(model, request);
            return "allusers/view";
        } else {
            prepareModelForEdit(model, id, true);

            if(((BindingAwareModelMap) model).get(ActivityConstants.USER_VIEW_CONSTANT) == null) {
                UserDetailsView userView = new UserDetailsView(resource);
                model.addAttribute(ActivityConstants.USER_VIEW_CONSTANT, userView);
            }

            return setModelAttributesAndReturnUserEditView(model);
        }
    }

    private String setModelAttributesAndReturnUserEditView(Model model) {
        String linkToUserList = menuPropertiesService.getResourcesPageLink();
        model.addAttribute("linkToUserList", linkToUserList);
        model.addAttribute("activityAuthMechanism", propertiesService.getActivityAuthenticationMechanism());
        return "allusers/edit";
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @RenderMapping(params = "ctx=add")
    public String add(Model model, PortletRequest request) throws IOException {
        prepareModelForEdit(model, null, true);
        if(((BindingAwareModelMap) model).get(ActivityConstants.USER_VIEW_CONSTANT) == null) {
            UserDetailsView userView = new UserDetailsView();
            model.addAttribute(ActivityConstants.USER_VIEW_CONSTANT, userView);
        }
        return setModelAttributesAndReturnUserEditView(model);
    }

    private void getCommonLists(Model model) {
        List<ResourceType> resourceTypeList = resourceService.getResourceTypeList();
        List<BranchOffice> branchOfficeList = resourceService.getBranchOfficeList();
        List<Employment> emplymentList = resourceService.getEmploymentList();

        model.addAttribute("resourceTypeList", resourceTypeList);
        model.addAttribute("branchOfficeList", branchOfficeList);
        model.addAttribute("employmentList", emplymentList);
    }

    /**
     * @param userView
     *            userView
     * @param result
     *            result
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ActionMapping("doSave")
    public void doSave(@ModelAttribute(ActivityConstants.USER_VIEW_CONSTANT) @Valid UserDetailsView userView, BindingResult result,
            Model model, PortletRequest request, ActionResponse response) {

        userViewValidator.validate(userView, result);

        String linkToUserList = menuPropertiesService.getResourcesPageLink();
        model.addAttribute("linkToUserList", linkToUserList);

        if (result.hasErrors()) {
            prepareModelForEdit(model, userView.getId(), false);
            prepareModelAddFunctionList(model, userView, null);

            selectAddOrEditUserView(userView, response);
            return;
        }

        Resource resource = new Resource();
        try {
            if (userView.getId() != null) {
                resource = resourceService.getResource(userView.getId(), false, false);
                setResourceValuesFromUserView(resource, userView);
                resourceService.updateResource(resource);
            } else {
                setResourceValuesFromUserView(resource, userView);
                Long resourceId = resourceService.addResource(resource);
                resource.setPk(resourceId);
            }
            request.setAttribute("successMessage", "saved");
        } catch (UniqueValidationException e) {
            LOGGER.debug("Insert error: resource.user_name_exist");
            model.addAttribute("errors", "validate_unique_constraint");
            prepareModelForEdit(model, userView.getId(), false);
            prepareModelAddFunctionList(model, userView, resource.getPk());
            selectAddOrEditUserView(userView, response);
        }

        List<ResourceFTypeMapping> resourceFunctionList = resourceService.getResourceFTypeMapping(resource.getPk());
        for (ResourceFTypeMapping resourceFunction : resourceFunctionList) {
            LOGGER.debug(resourceFunction.getResource().getPk().toString());
            resourceService.deleteResourceFTypeMappingByResource(resource.getPk());
        }

        if (userView.getFunctiontype() != null) {
            for (Long functionId : userView.getFunctiontype()) {
                ResourceFTypeMapping resourceFunction = new ResourceFTypeMapping();
                resourceFunction.setFunctionType(new FunctionType(functionId));
                resourceFunction.setResource(new Resource(resource.getPk()));
                resourceService.addResourceFTypeMapping(resourceFunction);
            }
        }

        selectAddOrEditUserView(userView, response);
    }

    private void selectAddOrEditUserView(UserDetailsView userView, ActionResponse response) {
        if(userView.getId() == null){
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "add");
            //model.addAttribute(ActivityConstants.USER_VIEW_CONSTANT, new UserDetailsView());
        }else{
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "edit");
            response.setRenderParameter("id", userView.getId().toString());
        }
    }

    /**
     * Prepare model for edit resource.
     *
     * @param model
     *            - Model
     * @param id
     *            - resource id
     */
    private void prepareModelForEdit(Model model, Long id, boolean getOldType) {
        getCommonLists(model);

        List<FunctionType> functionList = resourceService.getFunctionTypeList();
        model.addAttribute("functionList", functionList);

        if (getOldType) {
            List<ResourceFTypeMapping> resourceFunctionList = new ArrayList<ResourceFTypeMapping>();
            if (id != null) {
                resourceFunctionList = resourceService.getResourceFTypeMapping(id);
            }
            model.addAttribute("resourceFunctionList", resourceFunctionList);
        }
    }

    private void prepareModelAddFunctionList(Model model, UserDetailsView userView, Long resourcePk) {
        List<ResourceFTypeMapping> resourceFunctionList = new ArrayList<ResourceFTypeMapping>();
        if (userView.getFunctiontype() != null) {
            for (Long functionId : userView.getFunctiontype()) {
                ResourceFTypeMapping resourceFunction = new ResourceFTypeMapping();
                resourceFunction.setFunctionType(new FunctionType(functionId));
                resourceFunction.setResource(new Resource(resourcePk));
                resourceFunctionList.add(resourceFunction);
            }
        }
        
        model.addAttribute("resourceFunctionList", resourceFunctionList);
    }

    /**
     * Set resource values form userView object.
     *
     * @param resource
     *            - resource to update
     * @param userView
     *            - userView object
     */
    private void setResourceValuesFromUserView(Resource resource, UserDetailsView userView) {
        resource.setFirstname(userView.getFirstName());
        resource.setLastname(userView.getLastName());
        resource.setNote(userView.getDescription());
        resource.setUsername(userView.getUsername());
        // Two cases are supported here:
        // 1. Ldap: then password cannot be saved in the db
        // 2. Database: the password would be saved in the database and can be changed by a controller role.
        // Case 2 is required for example when installing locally or for manual tests.
        if ( propertiesService.getActivityAuthenticationMechanism().equals("DATABASE")
                && userView.getPassword() != null){
            resource.setPassword(userView.getPassword());
        }
        resource.setMail(userView.getMail());
        resource.setResourceType(resourceService.getResourceType(userView.getResourceTypeId()));
        resource.setBranchOffice(resourceService.getBranchOffice(userView.getBranchOfficeId()));
        //DELETEME
        //resource.setBirth(userView.getBirthDate());
        char caracter = userView.getActive() == null || "".equals(userView.getActive()) ? 'f' : 't';
        resource.setActive(caracter);
        resource.setEmployment(resourceService.getEmployment(userView.getEmploymentId()));
        resource.setEntered(userView.getJoinDate());
        resource.setExit(userView.getLeavingDate());
        resource.setHoliday(userView.getHolidays());
        resource.setSalery(userView.getSalary());
        resource.setCostPerHour(userView.getCost());
        resource.setAvailableHours(userView.getAvailableHours());
        
        BigDecimal remainingDays = userView.getRemainingDays(); 
        resource.setRemainHoliday(remainingDays != null ? remainingDays : new BigDecimal(0));
        // resource.setOvertimeHours(userView.getOvertimeHours());
    }

    /**
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doCancel")
    public String doCancel(Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.USER_VIEW_CONSTANT, new UserDetailsView());
        setAdvancedPagingConfig(model, request);
        return "allusers/view";
    }

    @ActionMapping("doDeleteResource")
    public void doDeleteResource(@RequestParam(value = "id", required = true) Long id,
    		PortletRequest request){
        resourceService.deleteRessource(id);
        request.setAttribute("successMessage", "deleted");
    }

    /**
     * Initial resource roles view rendering.
     *
     */
    @RenderMapping(params = "ctx=editRoles")
    public String editRoles(@RequestParam("resId") Long resourceId, Model model, PortletRequest request)
            throws IOException {
        RolesView rolesView = new RolesView();

        if (resourceId != null) {
            rolesView.setResourceId(resourceId);

            // retrieve resource with associated roles
            Resource resource = resourceService.getResource(resourceId, true, false);
            Set<Role> userRoles = resource.getRoles();

            // convert resource's role entities to command objects
            List<RoleView> userRoleViews = new ArrayList<RoleView>(userRoles.size());
            rolesView.setUserRoles(userRoleViews);

            for (Role role : userRoles) {
                userRoleViews.add(new RoleView(role.getPk(), role.getName()));
            }

            // now get all existing roles and convert all of them which are not yet in 'userRoleViews' to command
            // objects
            List<Role> allRoles = permissionService.getAllRoles();
            List<RoleView> allRolesExceptUserRoles = new ArrayList<RoleView>(allRoles.size());
            rolesView.setAllRolesExceptUserRoles(allRolesExceptUserRoles);

            for (Role role : allRoles) {
                int index = userRoleViews.indexOf(new RoleView(role.getPk()));
                if (index == -1) {
                    allRolesExceptUserRoles.add(new RoleView(role.getPk(), role.getName()));
                }
            }

            model.addAttribute("resourceName", resource.getFirstname() + " " + resource.getLastname());
            model.addAttribute("resourceId", resourceId);
        }
        model.addAttribute("rolesView", rolesView);

        String linkToUserList = menuPropertiesService.getResourcesPageLink();
        model.addAttribute("linkToUserList", linkToUserList);
        return "allusers/roles";
    }

    @ResourceMapping("rolePermissionsTable")
    public String rolePermissionsTable(Long[] roleIds, String sort, String dir, Long startIndex, Long results,
                                       Model model) throws IOException {
        // first search 'all permissions'
        PermissionFilter allPermFilter = new PermissionFilter(null, null, startIndex, results, SORT_ALIAS.get(sort),
                dir);
        FilterResult<Permission> allPermResult = permissionService.searchPermissions(allPermFilter);

        List<Permission> allPermRecords = allPermResult.getRecords();
        int allPermSize = allPermRecords.size();

        // prepare table list of PermissionView for copying result of
        // permissions
        ArrayList<PermissionView> permissionViews = new ArrayList<PermissionView>(allPermSize);

        // also copy primary keys of resulted permissions
        ArrayList<Long> allPermIds = new ArrayList<Long>(allPermSize);

        for (int i = 0; i < allPermSize; i++) {
            Permission perm = allPermRecords.get(i);
            PermissionView permView = getDefaultPermissionViewObject(perm);
            permissionViews.add(permView);

            allPermIds.add(perm.getPk());
        }

        // next search all 'role permissions' that are within result of first
        // search ('allPermResult') and merge them to table list
        if (roleIds != null && roleIds.length > 0) {
            List<Long> roleIdList = Arrays.asList(roleIds);
            PermissionFilter filter = new PermissionFilter(roleIdList, null, allPermIds, SORT_ALIAS.get(sort), dir);
            FilterResult<Permission> result = permissionService.searchPermissions(filter);
            List<Permission> records = result.getRecords();

            for (Permission perm : records) {
                PermissionView permView = getDefaultPermissionViewObject(perm);
                int index = permissionViews.indexOf(permView);
                if (index > -1) {
                    permissionViews.get(index).setChecked(true);
                }
            }
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<PermissionView>(
                "permission list", null, permissionViews, allPermResult.getTotalRecords())));

        return ActivityConstants.JSON_CONSTANT;
    }

    @ResourceMapping("doAddRole")
    public String doAddRole(@RequestParam("id") Long roleId, @RequestParam("resId") Long resourceId, Model model,
                            PortletRequest request) throws IOException {
        permissionService.addResourceToRole(roleId, resourceId);

        return editRoles(resourceId, model, request);
    }

    @ResourceMapping("doRemoveRole")
    public String doRemoveRole(@RequestParam("id") Long roleId, @RequestParam("resId") Long resourceId, Model model,
                               PortletRequest request) throws IOException {
        permissionService.removeResourceFromRole(roleId, resourceId);

        return editRoles(resourceId, model, request);
    }

    private PermissionView getDefaultPermissionViewObject(Permission perm) {
        return new PermissionView(perm.getPk(), "", perm.getDescription(), false, true);
    }

    @ResourceMapping("userPermissionsTable")
    public String userPermissionsTable(@RequestParam(required = true, value = "resId") Long resourceId,
                                       Long[] preSelectedPermIds, String sort, String dir, Long startIndex, Long results, Model model)
            throws IOException {
        // first search 'all permissions'
        PermissionFilter allPermFilter = new PermissionFilter(null, null, startIndex, results, SORT_ALIAS.get(sort),
                dir);
        FilterResult<Permission> allPermResult = permissionService.searchPermissions(allPermFilter);

        List<Permission> allPermRecords = allPermResult.getRecords();
        int allPermSize = allPermRecords.size();

        ArrayList<PermissionView> permissionViews = new ArrayList<PermissionView>(allPermSize);
        ArrayList<Long> allPermIds = new ArrayList<Long>(allPermSize);

        for (int i = 0; i < allPermSize; i++) {
            Permission perm = allPermRecords.get(i);
            PermissionView permView = getDefaultPermissionViewObject(perm);
            permissionViews.add(permView);

            allPermIds.add(perm.getPk());
        }

        Resource res = resourceService.getResource(resourceId, true, false);
        Set<Role> userRoles = res.getRoles();
        Long[] roleIds = new Long[userRoles.size()];
        int i = 0;
        for (Role r : userRoles) {
            roleIds[i] = r.getPk();
            i += 1;
        }

        // next search for all 'user permission' that are within result of
        // first search ('allPermResult') and merge them to table list
        ArrayList<Long> resourceIds = new ArrayList<Long>();
        resourceIds.add(resourceId);

        PermissionFilter filter = new PermissionFilter(null, resourceIds, allPermIds, SORT_ALIAS.get(sort), dir);
        FilterResult<Permission> result = permissionService.searchPermissions(filter);
        List<Permission> records = result.getRecords();

        for (Permission perm : records) {
            PermissionView permView = getDefaultPermissionViewObject(perm);
            int index = permissionViews.indexOf(permView);
            if (index > -1) {
                permissionViews.get(index).setChecked(true);
                permissionViews.get(index).setDisabled(false);
            }
        }

        // next search all 'role permissions' that are within result of first
        // search ('allPermResult') and merge them to table list
        if (roleIds != null && roleIds.length > 0) {
            List<Long> ids = Arrays.asList(roleIds);
            filter = new PermissionFilter(ids, null, allPermIds, SORT_ALIAS.get(sort), dir);
            result = permissionService.searchPermissions(filter);
            records = result.getRecords();

            for (Permission perm : records) {
                PermissionView permView = getDefaultPermissionViewObject(perm);
                int index = permissionViews.indexOf(permView);
                if (index > -1) {
                    permissionViews.get(index).setChecked(true);
                    permissionViews.get(index).setDisabled(true);
                }
            }
        }

        // last merge the hand-selected permissions
        if (preSelectedPermIds != null && preSelectedPermIds.length > 0 && preSelectedPermIds[0] != null) {
            List<Long> ids = Arrays.asList(preSelectedPermIds);
            filter = new PermissionFilter(null, null, ids, SORT_ALIAS.get(sort), dir);
            result = permissionService.searchPermissions(filter);
            records = result.getRecords();

            for (Permission perm : records) {
                PermissionView permView = getDefaultPermissionViewObject(perm);
                int index = permissionViews.indexOf(permView);
                if (index > -1) {
                    permissionViews.get(index).setChecked(true);
                    permissionViews.get(index).setDisabled(false);
                }
            }
            // check if negative ids are in id list, which means that a user defined permission was deselected
            List<Long> negIds = new ArrayList<Long>();
            for (Long id : ids) {
                if (Long.signum(id) == -1) {
                    negIds.add(id * -1L);
                }
            }
            if (!negIds.isEmpty()) {
                filter = new PermissionFilter(null, null, negIds, SORT_ALIAS.get(sort), dir);
                result = permissionService.searchPermissions(filter);
                records = result.getRecords();

                for (Permission perm : records) {
                    PermissionView permView = getDefaultPermissionViewObject(perm);
                    int index = permissionViews.indexOf(permView);
                    if (index > -1) {
                        permissionViews.get(index).setChecked(false);
                        permissionViews.get(index).setDisabled(false);
                    }
                }
            }
        }

        // generate JSON Object from permission views
        DataTableResult<PermissionView> table = new DataTableResult<PermissionView>("permission list", null,
                permissionViews, allPermResult.getTotalRecords(), startIndex, results, dir, sort);
        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(table));

        return ActivityConstants.JSON_CONSTANT;
    }

    @ActionMapping("doSavePermission")
    public void doSavePermission(PortletRequest request, ActionResponse response,
                                 @RequestParam(required = true) Long resId, Long[] addPermIds, Long[] removePermIds) {
        // add user permissions if requested
        if (addPermIds != null) {
            permissionService.addPermissionsToResource(resId, addPermIds);
        }

        // remove user permissions if requested
        if (removePermIds != null) {
            permissionService.removePermissionsFromResource(resId, removePermIds);
        }

        request.setAttribute("successMessage", "saved");
        response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "editPermissions");
        response.setRenderParameter("resId", resId.toString());
    }

    
    private void setAdvancedPagingConfig(Model model, PortletRequest request){
    	
    	setSessionPagingConfig(model, request, PAGING_CONFIG);

    	PortletSession ps = request.getPortletSession();
    	model.addAttribute(ACTIVE, ps.getAttribute(ACTIVE, PortletSession.APPLICATION_SCOPE));
    }
    
}
