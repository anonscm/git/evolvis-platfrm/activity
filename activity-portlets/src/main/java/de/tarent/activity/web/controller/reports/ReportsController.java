/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.reports;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Report;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ReportService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.ReportView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;
import de.tarent.activity.web.util.RequestUtil;

/**
 * 
 * Controller handling reports.
 * 
 */
@Controller("ReportsController")
@RequestMapping(value = "view")
public class ReportsController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportsController.class);

    @Autowired
    private ReportService reportService;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "pk");
        SORT_ALIAS.put("idTable", "pk");
        SORT_ALIAS.put("name", "name");
    }

    /**
     * 
     * @param model
     *            model
     * @param request
     *            request
     * @return view render for reports
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        LOGGER.info("ReportsController view ");

        return "reports/view";
    }

    /**
     * 
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param request
     *            request
     * @param model
     *            model
     * @return json Object containing DataTableResult<Report> - the list of reports(paginated)
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(String sort, String dir, Long startIndex, Long results, ResourceRequest request, Model model)
            throws IOException {

        BaseFilter filter = new BaseFilter(startIndex, results, SORT_ALIAS.get(sort), dir);
        FilterResult<Report> reports = reportService.searchReports(filter);
        Long count = reports.getTotalRecords();
        List<ReportView> list = new ArrayList<ReportView>();
        for (Report r : reports.getRecords()) {
            ReportView rv = new ReportView(r);
            list.add(rv);
        }
        String jsonObj = objectMapper.writeValueAsString(new DataTableResult<ReportView>("report list", null, list,
                count));
        model.addAttribute(ActivityConstants.JSON_CONSTANT, jsonObj);
        return ActivityConstants.JSON_CONSTANT;
    }

    /**
     * 
     * @param reportPk
     *            - pk for the report editted
     * @param model
     *            model
     * @param request
     *            request
     * @return render for edit report
     */
    @RenderMapping(params = { "action=doEdit" })
    public String doEdit(Long reportPk, ModelMap model, PortletRequest request) {
        LOGGER.debug("ReportsController - reportPk=" + reportPk);

        if (model.get(BindingResult.MODEL_KEY_PREFIX + "reportView") == null && reportPk != null
                && reportPk.longValue() > 0) {

            Report report = reportService.getReport(reportPk);
            ReportView reportView = new ReportView(report);
            model.addAttribute("reportView", reportView);

        }
        return "reports/edit";

    }

    /**
     * Action for adding/updating the report - with validation.
     * 
     * @param reportView
     *            reportView
     * @param result
     *            result
     * @param model
     *            model
     * @param request
     *            request
     * @param response
     *            response
     */
    @ActionMapping(params = "doSaveReport")
    public void doSaveReport(@ModelAttribute("reportView") @Valid ReportView reportView, BindingResult result,
            Model model, ActionRequest request, ActionResponse response) {

        CommonsMultipartFile f = reportView.getFile();

        if ((f == null || f.isEmpty()) && (reportView == null || reportView.getId() == null)) {
            result.rejectValue("file", "choose_file");
        }
        Resource currentUser = RequestUtil.getLoggedUser(request);

        if (!result.hasErrors()) {

            byte[] data = getByteArray(f);
            Report report = getReportFromView(reportView);
            if ((f == null || f.isEmpty()) && reportView != null && reportView.getId() != null
                    && reportView.getId().longValue() > 0) {
                Report oldReport = reportService.getReport(reportView.getId());
                report.setData(oldReport.getData());
            } else {
                report.setData(new String(data));
            }

            // TODO: resourceFk = currentUser or ...(only at add etc)
            report.setFkResource(currentUser.getPk());
            if (reportView.getId() != null && reportView.getId().longValue() > 0) {
                try {
                    reportService.updateReport(report);
                } catch (UniqueValidationException e) {
                    LOGGER.warn("UniqueValidationException: " + e.getMessage());
                }
            } else {
                try {
                    reportService.addReport(report);
                } catch (UniqueValidationException e) {
                    LOGGER.warn("UniqueValidationException: " + e.getMessage());
                }
            }

            request.setAttribute("successMessage", "saved");
        } else {
            for (ObjectError error : result.getAllErrors()) {
                LOGGER.error(error.toString());
            }
            response.setRenderParameter("action", "doEdit");
            return;
        }

    }

    /*
     * This method creates a Report given a ReportView
     */
    private Report getReportFromView(ReportView reportView) {
        Report report = new Report();
        report.setPk(reportView.getId());
        report.setName(reportView.getName());
        report.setPermission(reportView.getPermission());
        report.setNote(reportView.getNote());
        return report;
    }

    /**
     * Cancel edit loss form.
     * 
     * @param request
     *            - PortletRequest
     * @param model
     *            - Model
     * @return he view's name
     */
    @ResourceMapping("doCancelReport")
    public String doCancelReport(PortletRequest request, Model model) {

        return "reports/view";

    }

    /**
     * 
     * @param reportId
     *            reportId
     * @param model
     *            model
     * @param request
     *            request
     * @return render for report as pdf
     */
    @RenderMapping(params = { "action=resultPdf" })
    public String resultPdf(Long reportId, ModelMap model, PortletRequest request) {
        LOGGER.debug("ReportsController resultPdf - reportId=" + reportId);

        return "reports/resultpdf";
    }

    /**
     * 
     * @param reportId
     *            reportId
     * @param model
     *            model
     * @param request
     *            request
     * @return render for report as html
     */
    @RenderMapping(params = { "action=resultHtml" })
    public String resultHtml(Long reportId, ModelMap model, PortletRequest request) {
        LOGGER.debug("ReportsController resultHtml - reportId=" + reportId);

        return "reports/resulthtml";
    }

    /**
     * Delete report with the given id.
     * 
     * @param reportId
     *            reportId
     * @param request
     *            request
     * @param response
     *            response
     * @throws IOException
     *             IOException
     */
    @ActionMapping(params = { "action=doDeleteReport" })
    public void doDeleteReport(Long reportId, ActionRequest request, ActionResponse response) {

        if (reportId != null && reportId.longValue() > 0) {
            reportService.deleteReport(reportId);
        }

    }

    /*
     * This method returns byteArray for the given file
     */
    private byte[] getByteArray(CommonsMultipartFile file) {

        InputStream bs;
        byte[] ret;
        try {
            bs = file.getInputStream();
            ret = new byte[bs.available()];
            bs.read(ret, 0, bs.available());
            return ret;
        } catch (IOException e) {
            LOGGER.error("NamingException " + e.getMessage());
            return null;
        }

    }

}
