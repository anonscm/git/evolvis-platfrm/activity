/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.util.Date;

import de.tarent.activity.domain.DeleteRequest;

/**
 * DeleteRequestView.
 * 
 */
public class DeleteRequestView {
    private Long id;
    private Long idTable;
    private String resourceName;
    private String dataType;
    private Long dataTypeId;
    private String description;
    private String status;
    private String controllerName;
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public DeleteRequestView(DeleteRequest request) {
        this(request.getPk(), (request.getResource() == null) ? "" : request.getResource().getLastname() + " "
                + request.getResource().getFirstname(), request.getType(), request.getId(), request.getDescription(),
                request.getStatus(), (request.getController() == null) ? "" : request.getController().getLastname()
                        + " " + request.getController().getFirstname(), request.getCrDate());
    }

    public DeleteRequestView(Long id, String resourceName, String dataType, Long dataTypeId, String description,
            String status, String controllerName, Date crDate) {
        super();
        this.id = id;
        this.idTable = id;
        this.resourceName = resourceName;
        this.dataType = dataType;
        this.dataTypeId = dataTypeId;
        this.description = description;
        this.status = status;
        this.controllerName = controllerName;
        this.date = crDate;
    }

    public String getControllerName() {
        return controllerName;
    }

    public String getDataType() {
        return dataType;
    }

    public Long getDataTypeId() {
        return dataTypeId;
    }

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getStatus() {
        return status;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setDataTypeId(Long dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }
}
