/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.validator.constraints.Email;

/**
 * Basic information about the user (what is typically needed in a table / list of users)
 */
public class UserView implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 620745636598939474L;

    private Long id;

    private Long idTable;

    @NotEmpty(message = "{validate_firstname_not_empty}")
    private String firstName;

    @NotEmpty(message = "{validate_lastname_not_empty}")
    private String lastName;

    @NotEmpty(message = "{validate_username_not_empty}")
    @Length(max = 10, message = "{validate_username_length}")
    private String username;

    @Email
    private String mail;

    @DateTimeFormat
    private Date birthDate;

    private Boolean checked;

    @Length(max = 4000, message = "{validate_description_length}")
    private String description;

    private String active;

    public UserView() {

    }

    public UserView(Resource r) {
        this(r.getPk(), r.getFirstname(), r.getLastname(), r.getUsername(), r.getMail(), r.getBirth(), null, r
                .getNote(), r.getActive() == null ? "" : r.getActive().toString());
    }

    public UserView(Long id, String firstName, String lastName, String username, String mail, Date birthDate,
            Boolean checked, String description, String active) {
        super();
        this.id = id;
        this.idTable = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.mail = mail;
        this.birthDate = birthDate;
        this.checked = checked;
        this.description = description;
        this.active = active;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
        result = prime * result + ((checked == null) ? 0 : checked.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((mail == null) ? 0 : mail.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }

        UserView other = (UserView) obj;
        if (active == null) {
            if (other.active != null){
                return false;
            }
        } else if (!active.equals(other.active)){
            return false;
        }
        if (birthDate == null) {
            if (other.birthDate != null){
                return false;
            }
        } else if (!birthDate.equals(other.birthDate)){
            return false;
        }
        if (checked == null) {
            if (other.checked != null){
                return false;
            }
        } else if (!checked.equals(other.checked)){
            return false;
        }
        if (description == null) {
            if (other.description != null){
                return false;
            }
        } else if (!description.equals(other.description)){
            return false;
        }
        if (firstName == null) {
            if (other.firstName != null){
                return false;
            }
        } else if (!firstName.equals(other.firstName)){
            return false;
        }
        if (id == null) {
            if (other.id != null){
                return false;
            }
        } else if (!id.equals(other.id)){
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null){
                return false;
            }
        } else if (!lastName.equals(other.lastName)){
            return false;
        }
        if (mail == null) {
            if (other.mail != null){
                return false;
            }
        } else if (!mail.equals(other.mail)){
            return false;
        }
        if (username == null) {
            if (other.username != null){
                return false;
            }
        } else if (!username.equals(other.username)){
            return false;
        }

        return true;
    }

}
