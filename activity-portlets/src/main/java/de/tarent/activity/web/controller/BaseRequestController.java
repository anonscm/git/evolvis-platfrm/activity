/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.DeleteRequestView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;

/**
 * Base controller class for holding common methods used by requests portlets controllers.
 * 
 */
public abstract class BaseRequestController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseRequestController.class);

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ActivityService activityService;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();

    static {
        SORT_ALIAS.put("idTable", "dr.pk");
        SORT_ALIAS.put("resourceName", "dr.resource.lastname");
        SORT_ALIAS.put("dataType", "dr.type");
        SORT_ALIAS.put("dataTypeId", "dr.id");
        SORT_ALIAS.put("description", "dr.description");
        SORT_ALIAS.put("status", "dr.status");
        SORT_ALIAS.put("controllerName", "dr.controller.lastname");
    }

    /**
     * @param selectedResource
     *            selectedResource
     * @param selectedController
     *            selectedController
     * @param selectedStatus
     *            selectedStatus
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param model
     *            model
     * @throws IOException
     *             IOException
     */
    protected void createDataTableResult(Long selectedResource, Long selectedController, String selectedStatus,
            Long startIndex, Long results, String sort, String dir, Model model) throws IOException {
        LOGGER.debug("resource:" + selectedResource + "\ncontroller" + selectedController + "\nsort:" + sort + "\ndir:"
                + dir + "\nstartIndex:" + startIndex + "\nresults:" + results);

        DeleteRequestFilter delFilter = new DeleteRequestFilter(selectedResource, selectedController, selectedStatus,
                startIndex, results, SORT_ALIAS.get(sort), dir);

        FilterResult<DeleteRequest> result = resourceService.searchRequest(delFilter);

        ArrayList<DeleteRequestView> list = new ArrayList<DeleteRequestView>();
        for (DeleteRequest deleteRequest : result.getRecords()) {
            list.add(new DeleteRequestView(deleteRequest));
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<DeleteRequestView>(
                "request list", null, list, result.getTotalRecords())));
    }

    protected ResourceService getResourceService() {
        return resourceService;
    }

    protected ActivityService getActivityService() {
        return activityService;
    }
}
