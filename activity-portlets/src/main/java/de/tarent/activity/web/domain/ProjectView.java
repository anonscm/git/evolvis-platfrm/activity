/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Project;
import de.tarent.activity.validator.constraints.Url;
import de.tarent.activity.validator.constraints.Webdav;

public class ProjectView implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long idTable;

    @NotEmpty(message = "{validate_project_name_not_empty}")
    @Size(max = 255, message = "{validate_name_255}")
    private String projectName;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    @NotNull(message = "{validate_date_not_empty}")
    @DateTimeFormat
    private Date startDate;

    @DateTimeFormat
    private Date endDate;

    @NotNull(message = "{validate_customer_not_empty}")
    private Long clientId;

    private String customerName;

    private String contactPerson;

    @Max(value = 99999999)
    private Long costCenter;

    @NotNull(message = "{validate_project_responsible_not_empty}")
    private Long responsibleId;

    @NumberFormat
    private BigDecimal dailyRate;

    private Date paymentDate;

    @Url
    private String wiki;

    @Url
    private String testServer;

    @Url
    private String productBacklog;

    @Url
    private String sprintBacklog;

    @Url
    private String specification;

    @Url
    private String sourceControl;

    @Url
    private String protocols;

    @Url
    private String evolvisProjectPage;

    @Url
    private String projectPlan;

    @Url
    private String projectBlog;

    @Url
    private String issueTracking;

    @Url
    private String mailingLists;

    @Url
    private String offer;

    @Webdav
    private String dms;

    private String crm;

    @Url
    private String riskAnalysis;

    @Url
    private String sonar;

    @Url
    private String order;

    @Url
    private String dnsTestProtocol;

    @Url
    private String wikiProcess;

    public ProjectView() {
    }

    public ProjectView(Project p) {
        super();
        this.id = p.getPk();
        this.idTable = p.getPk();
        this.projectName = p.getName();
        this.description = p.getNote();
        this.startDate = p.getStartDate();
        this.endDate = p.getEndDate();
        this.clientId = p.getCustomer().getPk();
        this.contactPerson = p.getContactPerson();
        this.costCenter = p.getAccounts();
        this.responsibleId = p.getFkResource();
        this.dailyRate = p.getDayRate();
        this.paymentDate = p.getPaymentTarget();
        this.wiki = p.getWiki();
        this.testServer = p.getTestServer();
        this.productBacklog = p.getProductBacklog();
        this.sprintBacklog = p.getSprintBacklog();
        this.specification = p.getSpecification();
        this.sourceControl = p.getSourceControl();
        this.protocols = p.getProtocols();
        this.evolvisProjectPage = p.getProjectSite();
        this.projectPlan = p.getProjectPlan();
        this.projectBlog = p.getProjectBlog();
        this.issueTracking = p.getIssueTracking();
        this.mailingLists = p.getMailingLists();
        this.offer = p.getOffer();
        this.dms = p.getDms();
        this.order = p.getOffer();
        this.dnsTestProtocol = p.getTestProtokoll();
        this.wikiProcess = p.getProcessWiki();
        this.customerName = p.getCustomer().getName();
        this.crm = p.getCrm();
        this.riskAnalysis = p.getRiskAnalysis();
        this.sonar = p.getSonar();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Long getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(Long costCenter) {
        this.costCenter = costCenter;
    }

    public Long getResponsibleId() {
        return responsibleId;
    }

    public void setResponsibleId(Long responsibleId) {
        this.responsibleId = responsibleId;
    }

    public BigDecimal getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(BigDecimal dailyRate) {
        this.dailyRate = dailyRate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getWiki() {
        return wiki;
    }

    public void setWiki(String wiki) {
        this.wiki = wiki;
    }

    public String getTestServer() {
        return testServer;
    }

    public void setTestServer(String testServer) {
        this.testServer = testServer;
    }

    public String getProductBacklog() {
        return productBacklog;
    }

    public void setProductBacklog(String productBacklog) {
        this.productBacklog = productBacklog;
    }

    public String getSprintBacklog() {
        return sprintBacklog;
    }

    public void setSprintBacklog(String sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getSourceControl() {
        return sourceControl;
    }

    public void setSourceControl(String sourceControl) {
        this.sourceControl = sourceControl;
    }

    public String getProtocols() {
        return protocols;
    }

    public void setProtocols(String protocols) {
        this.protocols = protocols;
    }

    public String getEvolvisProjectPage() {
        return evolvisProjectPage;
    }

    public void setEvolvisProjectPage(String evolvisProjectPage) {
        this.evolvisProjectPage = evolvisProjectPage;
    }

    public String getProjectPlan() {
        return projectPlan;
    }

    public void setProjectPlan(String projectPlan) {
        this.projectPlan = projectPlan;
    }

    public String getProjectBlog() {
        return projectBlog;
    }

    public void setProjectBlog(String projectBlog) {
        this.projectBlog = projectBlog;
    }

    public String getIssueTracking() {
        return issueTracking;
    }

    public void setIssueTracking(String issueTracking) {
        this.issueTracking = issueTracking;
    }

    public String getMailingLists() {
        return mailingLists;
    }

    public void setMailingLists(String mailingLists) {
        this.mailingLists = mailingLists;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getDms() {
        return dms;
    }

    public void setDms(String dms) {
        this.dms = dms;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getRiskAnalysis() {
        return riskAnalysis;
    }

    public void setRiskAnalysis(String riskAnalysis) {
        this.riskAnalysis = riskAnalysis;
    }

    public String getSonar() {
        return sonar;
    }

    public void setSonar(String sonar) {
        this.sonar = sonar;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDnsTestProtocol() {
        return dnsTestProtocol;
    }

    public void setDnsTestProtocol(String dnsTestProtocol) {
        this.dnsTestProtocol = dnsTestProtocol;
    }

    public String getWikiProcess() {
        return wikiProcess;
    }

    public void setWikiProcess(String wikiProcess) {
        this.wikiProcess = wikiProcess;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

}
