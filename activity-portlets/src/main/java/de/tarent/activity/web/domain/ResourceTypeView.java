/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

/**
 * @author This class stores all ResourceType properties.
 */
public class ResourceTypeView implements java.io.Serializable {

    private static final long serialVersionUID = -95791787475838124L;

    private Long pk;

    private String name;

    public ResourceTypeView() {
    }

    public ResourceTypeView(Long pk, String name) {
        this.pk = pk;
        this.name = name;
    }

    public Long getPk() {
        return pk;
    }

    public void setPk(Long pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
