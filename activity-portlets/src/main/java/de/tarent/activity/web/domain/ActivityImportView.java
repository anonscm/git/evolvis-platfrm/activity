/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * ActivityImportView.
 * 
 */
public class ActivityImportView implements Serializable {

    private static final long serialVersionUID = -7686257943515193987L;

    @NotNull(message = "{validate_job_not_empty}")
    private Long jobId;

    @NotNull(message = "{validate_position_not_empty}")
    private Long positionId;

    @NotNull(message = "{validate_resource_not_empty}")
    private Long resourceId;

    @NotNull(message = "{validate_import_file_not_empty}")
    private CommonsMultipartFile file;

    /**
     * Constructor.
     */
    public ActivityImportView() {

    }

    /**
     * Constructor.
     * 
     * @param jobId
     *            jobId
     * @param positionId
     *            positionId
     * @param resourceId
     *            resourceId
     * @param file
     *            file
     */
    public ActivityImportView(Long jobId, Long positionId, Long resourceId, CommonsMultipartFile file) {
        this.jobId = jobId;
        this.positionId = positionId;
        this.resourceId = resourceId;
        this.file = file;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public CommonsMultipartFile getFile() {
        return file;
    }

    public void setFile(CommonsMultipartFile file) {
        this.file = file;
    }

}
