/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.Role;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.util.RequestUtil;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.PortletRequestDataBinder;

public abstract class BaseController {
	
	public static final String ACTIVE = "activeUser";
	
	public static final String PAGING_CONFIG_MODEL = "config";

	@Autowired
	private PropertyEditorRegistrar customPropertyEditorRegistrar;

    @Autowired
    private ResourceService resourceService;


    @ModelAttribute("doCancel")
    public String populatecancelUrl(@RequestParam(required = false) String cancel) {
        if(cancel != null){
            try {
                return URLDecoder.decode(cancel, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // will never occur
            }
        }
        return null;
    }

    @ModelAttribute("redirect")
    public String populateRedirectUrl(@RequestParam(required = false) String redirect) {
        if(redirect != null){
            try {
                return URLDecoder.decode(redirect, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // will never occur
            }
        }
        return null;
    }

    @ModelAttribute("message")
    public String populateMessage(PortletRequest request){
        String message = (String) request.getPortletSession().getAttribute("message");
        if(message != null){
            request.getPortletSession().removeAttribute("message");
        } else {
            message = (String) request.getAttribute("message");
        }

        return message;
    }

    @InitBinder
    public void initBinder(PortletRequestDataBinder binder, PortletPreferences preferences) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        customPropertyEditorRegistrar.registerCustomEditors(binder);
    }
    
    //TODO Set paging configurations for all tables
    public void setSessionPagingConfig(Model model, PortletRequest request, String configName){
        
        PortletSession ps = request.getPortletSession();
        BaseFilter pagingConfig = (BaseFilter)ps.getAttribute(configName, PortletSession.APPLICATION_SCOPE);
        if(pagingConfig == null || pagingConfig.getOffset() == null || pagingConfig.getPageItems() == null){
            pagingConfig = new BaseFilter();
        }
        model.addAttribute(PAGING_CONFIG_MODEL, pagingConfig);

    }
    
    //TODO: replace this with setSessionPagingConfig in all tables
    protected void setUserPreferencesInSession(Model model, PortletSession ps, String liferaySessionToken, String tokenToPassToJsp) {
    	
    	if( ps.getAttribute(liferaySessionToken, PortletSession.APPLICATION_SCOPE) == null ){
            ps.setAttribute(liferaySessionToken, 10, PortletSession.APPLICATION_SCOPE);
            model.addAttribute(tokenToPassToJsp, 10);
        }else{
            model.addAttribute(tokenToPassToJsp, ps.getAttribute(liferaySessionToken, PortletSession.APPLICATION_SCOPE));
        }
    }

    protected boolean currentUserhasTheRequiredRole(Model model, PortletRequest portletRequest, String checkRole) {

        Long resourceId = null;
        Resource res = null;
        if (portletRequest != null) {
            resourceId = RequestUtil.getLoggedUser(portletRequest).getPk();
            res = resourceService.getResource(resourceId, true, false);
        }

        Set<Role> userRoles = res.getRoles();
        for (Role r : userRoles) {
            if(r.getName().equalsIgnoreCase(checkRole)){
                return true;
            }
        }
        return false;

    }


}
