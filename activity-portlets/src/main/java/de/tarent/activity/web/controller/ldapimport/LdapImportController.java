/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.ldapimport;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.BranchOffice;
import de.tarent.activity.domain.Employment;
import de.tarent.activity.domain.LdapConfig;
import de.tarent.activity.domain.LdapUser;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.service.LdapService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.BranchOfficeView;
import de.tarent.activity.web.domain.EmploymentView;
import de.tarent.activity.web.domain.ResourceTypeView;
import de.tarent.activity.web.domain.UserExtendedView;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.NullToEmptyStringObjectMapper;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Controller for login portlet. It's used to create an authentication context, to simplify development. Authentication
 * context will be used to authorize service calls. This portlet is temporary.
 */
@Controller("LdapImportController")
@RequestMapping(value = "view")
public class LdapImportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LdapImportController.class);

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private LdapConfig ldapConfig;

    @Autowired
    private LdapService ldapService;

    private List<Resource> resourceList;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private int startIndex = 0;

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("sortAttrib", "uid");
    }

    private List<Resource> getMappedLdapResource(List<LdapUser> listLdapUser) {

        List<Resource> listResource = new ArrayList<Resource>();
        Resource resource = null;
        Resource dbResource = null;

        for (int i = 0; i < listLdapUser.size(); i++) {

            dbResource = resourceService.loadResource(listLdapUser.get(i).getUsername());

            resource = new Resource();

            if (dbResource != null) {
                resource = dbResource;
                dbResource = null;
            }

            Date birth = null;
            try {
                birth = new SimpleDateFormat("yyyy-MM-dd").parse(listLdapUser.get(i).getBirth().toString());
            } catch (ParseException e) {
                LOGGER.error("", e);
            }

            resource.setActive('t');
            resource.setBirth(birth);
            resource.setGroups(listLdapUser.get(i).getGroups());
            resource.setFirstname(listLdapUser.get(i).getFirstname());
            resource.setLastname(listLdapUser.get(i).getLastname());
            resource.setMail(listLdapUser.get(i).getMail());
            resource.setUsername(listLdapUser.get(i).getUsername());

            listResource.add(resource);
        }

        Collections.sort(listResource);
        return listResource;
    }

    /**
     * Default view for portlet.
     * 
     * @param model
     *            Model
     * @param request
     *            RenderRequest
     * @return String path for jsp file.
     * 
     */
    @RenderMapping
    public String view(Model model, PortletRequest request) {
        model.addAttribute("loggeduser", RequestUtil.getLoggedUser(request));
        ajaxView(model, request);
        return "ldapimport/view";
    }

    /**
     * @param model
     *            - Model
     * @param request
     *            - PortletRequest
     * @return the view's name
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, PortletRequest request) {
        List<LdapUser> ldapUser = ldapService.getAllUserAndGroups(ldapConfig, SORT_ALIAS);
        List<Resource> resource = this.getMappedLdapResource(ldapUser);
        this.resourceList = resource;

        List<BranchOffice> branchOfficeList = resourceService.getBranchOfficeList();
        List<ResourceType> resourceTypeList = resourceService.getResourceTypeList();
        List<Employment> employmentList = resourceService.getEmploymentList();

        List<BranchOfficeView> branchOfficeViewList = new ArrayList<BranchOfficeView>();
        List<EmploymentView> employmentViewList = new ArrayList<EmploymentView>();
        List<ResourceTypeView> resourceTypeViewList = new ArrayList<ResourceTypeView>();

        for (int i = 0; i < branchOfficeList.size(); i++) {
            branchOfficeViewList.add(new BranchOfficeView(branchOfficeList.get(i).getPk(), branchOfficeList.get(i)
                    .getName()));
        }

        for (int i = 0; i < employmentList.size(); i++) {
            employmentViewList.add(new EmploymentView(employmentList.get(i).getPk(), employmentList.get(i).getName()));
        }

        for (int i = 0; i < resourceTypeList.size(); i++) {
            resourceTypeViewList.add(new ResourceTypeView(resourceTypeList.get(i).getPk(), resourceTypeList.get(i)
                    .getName()));
        }

        try {
            model.addAttribute("branchOffice", objectMapper.writeValueAsString(branchOfficeViewList));
            model.addAttribute("employment", objectMapper.writeValueAsString(employmentViewList));
            model.addAttribute("resourceType", objectMapper.writeValueAsString(resourceTypeViewList));
        } catch (JsonGenerationException e) {
            LOGGER.error("", e);
        } catch (JsonMappingException e) {
            LOGGER.error("", e);
        } catch (IOException e) {
            LOGGER.error("", e);
        }
        return "ldapimport/view";
    }

    @ResourceMapping("table")
    public String table(String sort, String dir, Long startIndex, Long results, String active, Model model)
            throws IOException {

        int intStartIndex = Integer.valueOf(startIndex.toString());
        this.startIndex = intStartIndex;
        int intResults = Integer.valueOf(results.toString());

        List<UserExtendedView> extendedUserViewList = new ArrayList<UserExtendedView>();

        int intResourceSize = this.resourceList.size();
        int intTargetResults = (intStartIndex + intResults);

        if ((intResourceSize - startIndex) < results) {
            intTargetResults = (intResourceSize - intStartIndex) + intStartIndex;
        }

        for (int i = intStartIndex; i < intTargetResults; i++) {
            Resource r = resourceList.get(i);
            LdapUser l = new LdapUser(Long.valueOf(String.valueOf(i)), r.getUsername());
            UserExtendedView uv = new UserExtendedView(r, l);
            extendedUserViewList.add(uv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<UserExtendedView>(
                "resource list", null, extendedUserViewList, (long) intResourceSize)));
        return ActivityConstants.JSON_CONSTANT;
    }

    @ResourceMapping("doLdapimport")
    public String actionForm(PortletRequest request, Model model, String[] selectedForImportId, String[] branchOffice,
            String[] employmentType, String[] resourceType, String[] holidays, String[] remainingDays, Long startIndex,
            Long results) throws Exception {

        for (int j = 0; j < selectedForImportId.length; j++) {
            int id = Integer.valueOf(selectedForImportId[j]);
            int thisIndex = (id - this.startIndex);
            Resource r = this.resourceList.get(id);
            r.setBranchOffice(resourceService.getBranchOffice(Long.valueOf(branchOffice[thisIndex].toString())));
            r.setEmployment(resourceService.getEmployment(Long.valueOf(employmentType[thisIndex])));
            r.setResourceType(resourceService.getResourceType(Long.valueOf(resourceType[thisIndex])));

            r.setHoliday(BigDecimal.valueOf(Long.valueOf(holidays[thisIndex])));
            r.setRemainHoliday(BigDecimal.valueOf(Long.valueOf(remainingDays[thisIndex])));
            if (resourceService.loadResource(r.getUsername()) == null) {
                try {
                    resourceService.addResource(r);
                    model.addAttribute("successMessage", "Import successful");
                } catch (Exception e) {
                    LOGGER.error("", e);
                    model.addAttribute("errorMessage", "One or more imports went wrong");
                }

            } else {
                try {
                    resourceService.updateResource(r);
                    model.addAttribute("successMessage", "Update successful");
                } catch (Exception e) {
                    LOGGER.error("", e);
                    model.addAttribute("errorMessage", "One or more updates went wrong");
                }
            }
        }

        return ajaxView(model, request);
    }
}
