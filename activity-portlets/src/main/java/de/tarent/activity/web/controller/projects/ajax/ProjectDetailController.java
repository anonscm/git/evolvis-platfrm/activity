/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.util.TableModel;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ExportService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.EscapeDescriptionUtil;
import de.tarent.activity.web.util.RequestUtil;

/**
 * ProjectDetailController provides request mappings for displaying project details.
 * 
 */
@Controller("ProjectDetailController")
@RequestMapping(value = "view")
public class ProjectDetailController extends BaseAjaxController {

	@Autowired
    private MessageSource messageSource;
	
	@Autowired
    private ActivityService activityService;
	
	@Autowired
    private ExportService exportService;
	
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @ModelAttribute("project")
    public Project populateProject(@RequestParam Long projectId) {
        return projectService.getProject(projectId);
    }
    
    @RenderMapping(params = "ctx=showProjectDetails")
    public String getRenderProjectDetails(@ModelAttribute Project project, Model model, 
    		RenderRequest request, RenderResponse response) throws PortletModeException {

        prepareProjectDetailsView(project, model, request);

        PortletURL jobDetailLink = response.createRenderURL();
        jobDetailLink.setPortletMode(PortletMode.VIEW);
        jobDetailLink.setParameter(ActivityConstants.CONTEXT_CONSTANT, "jobDetails");
        jobDetailLink.setParameter("isDeletableJob", "false");
        model.addAttribute("publicJobIdLink", jobDetailLink);

        return "projects/details";
    }

    /**
     * Export activities.
     */
    @ResourceMapping("getProjectExport")
    public void getProjectExport(Date fromDate, Date toDate, Long projectId, Model model, PortletRequest request,
            ResourceResponse response) throws Exception {

        ActivityFilter filter = new ActivityFilter(null, null, null, fromDate, toDate, projectId, null, null, null,
                null);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        Locale locale = request.getLocale();
        String[] columnHeaders = {
                messageSource.getMessage("id", null, locale),
                messageSource.getMessage("resource", null, locale),
                messageSource.getMessage(ActivityConstants.JOB_CONSTANT, null, locale),
                messageSource.getMessage(ActivityConstants.POSITION_CONSTANT, null, locale),
                messageSource.getMessage("description", null, locale),
                messageSource.getMessage("hours", null, locale),
                messageSource.getMessage("date", null, locale) };

        List<Object[]> items = new ArrayList<Object[]>();

        for (Activity activity : activities.getRecords()) {
            Object[] item = new Object[7];
            item[0] = activity.getPk();
            item[1] = activity.getResource() == null ? null : activity.getResource().getName();
            item[2] = activity.getPosition() == null || activity.getPosition().getJob() == null ? null : activity.getPosition().getJob().getName();
            item[3] = activity.getPosition() == null ? null : activity.getPosition().getName();
            item[4] = activity.getName();
            item[5] = activity.getHours();
            item[6] = activity.getDate();

            items.add(item);
        }

        // tell browser program going to return an application file
        // instead of html page
        response.setContentType("application/octet-stream");
        response.setProperty("Content-Disposition", "attachment;filename=Activity.xls");

        OutputStream out = response.getPortletOutputStream();

        try {
            out.write(exportService.doExportExcel(new TableModel(columnHeaders, items, "Activity", null)));
            out.flush();
        } finally {
            out.close();
        }
    }

    private void prepareProjectDetailsView(Project project, Model model, RenderRequest request) {
        if (project != null && project.getNote() != null) {
            project.setNote(EscapeDescriptionUtil.escapeDescriptionWithoutBr(project.getNote()));
        }

        boolean isProjectActive = projectService.isProjectActive(project.getPk());
        int projectStatus = isProjectActive ? 1 : 0;

        model.addAttribute("projectStatus", projectStatus);
        model.addAttribute("isDeletableProject", projectService.isDeletableProject(project.getPk()));
        model.addAttribute("projectManager", resourceService.getResource(project.getFkResource(), false, false));

        Resource currentUser = RequestUtil.getLoggedUser(request);
        model.addAttribute("isOwner", currentUser.getPk().equals(project.getFkResource()));
    }


}
