/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.validation.Valid;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.ProjectView;

@Controller("ProjectSaveController")
@RequestMapping(value = "view")
public class ProjectSaveController extends BaseController{

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectSaveController.class);

    private static final String UNIQUE_MESSAGE_KEY = "validate_unique_constraint";

    @Autowired
    @Qualifier("projectViewValidator")
    private Validator projectViewValidator;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MailService mailService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    /**
     * Populate list of all customers to model, for drop-down select box.
     */
    @ModelAttribute("customerList")
    public List<Customer> populateCustomer() {
        return projectService.getCustomerList();
    }

    /**
     * Populate list of all resources to model, for drop-down select box.
     */
    @ModelAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT)
    public List<Resource> populateResources() {
        return resourceService.listActiveResources();
    }

    @ActionMapping("doProjectSave")
    public void doProjectSave(@ModelAttribute("projectView") @Valid ProjectView projectView, BindingResult result,
                              @RequestParam(value = "backUrl", required = false) String backUrl,
    		                  ActionRequest request, ActionResponse actionResponse, Locale locale, Model model) {
        // run custom validator to validate start- and end-date of project
        projectViewValidator.validate(projectView, result);
        if(backUrl != null && !backUrl.equals("")){
            model.addAttribute("backUrl", backUrl);
        }

        if (!result.hasErrors()) {
            saveOrUpdateProjectAndSetFrontendStatusMessage(projectView, result, locale, request, actionResponse);
        }
        selectAddOrEditRenderView(projectView, actionResponse);

    }

    private void selectAddOrEditRenderView(ProjectView projectView, ActionResponse actionResponse) {
        if(projectView.getId() != null){
            actionResponse.setRenderParameter("projectId", projectView.getId().toString());
            actionResponse.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getProjectEdit");
        }else{
            actionResponse.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getProjectAdd");
        }
    }

    private void saveOrUpdateProjectAndSetFrontendStatusMessage(ProjectView projectView, BindingResult result, 
    		Locale locale, ActionRequest request, ActionResponse response) {
        Project project = getProjectFromView(projectView);
        try {
            // new or existing project?
            if (projectView.getId() != null) {
                projectService.updateProject(project);
            } else {
                projectService.addProject(project);

                // send mail notification that a new project was created
                try {
                    mailService.sendProjectMail(project);
                    mailService.sendConfigurableAddProjectMail(project);
                } catch (Exception e) {
                    LOGGER.error("Got an exception trying to send mail", e);
                    //TODO: mailservice noch nicht implementiert, deswegen noch keine Nachricht
                    //request.setAttribute("warningMessage", "Could not send mail.");
                }
            }
            request.setAttribute("successMessage", "saved");
        } catch (UniqueValidationException e) {
            LOGGER.error("Cannot save project, unique constraint violation.");
            String arg = messageSource.getMessage("project", null, "a project", locale);
            Object[] errorArgs = { arg, e.getConstraint().toString() };
            result.reject(UNIQUE_MESSAGE_KEY, errorArgs, "Unique constraint violated");

            selectAddOrEditRenderView(projectView, response);

        }
    }

    /**
     * Set project values from projectView.
     */
    private Project getProjectFromView(ProjectView projectView) {
        Project project = new Project();
        project.setPk(projectView.getId());
        project.setName(projectView.getProjectName());
        project.setNote(projectView.getDescription());
        project.setStartDate(projectView.getStartDate());
        project.setEndDate(projectView.getEndDate());
        project.setCustomer(projectService.getCustomer(projectView.getClientId()));
        project.setContactPerson(projectView.getContactPerson());
        project.setAccounts(projectView.getCostCenter());
        project.setFkResource(projectView.getResponsibleId());
        project.setDayRate(projectView.getDailyRate());
        project.setPaymentTarget(projectView.getPaymentDate());
        project.setWiki(projectView.getWiki());
        project.setTestServer(projectView.getTestServer());
        project.setProductBacklog(projectView.getProductBacklog());
        project.setSprintBacklog(projectView.getSprintBacklog());
        project.setSpecification(projectView.getSpecification());
        project.setSourceControl(projectView.getSourceControl());
        project.setProtocols(projectView.getProtocols());
        project.setProjectSite(projectView.getEvolvisProjectPage());
        project.setProjectPlan(projectView.getProjectPlan());
        project.setProjectBlog(projectView.getProjectBlog());
        project.setIssueTracking(projectView.getIssueTracking());
        project.setMailingLists(projectView.getMailingLists());
        project.setOffer(projectView.getOffer());
        project.setDms(projectView.getDms());
        project.setPageAuftrag(projectView.getOrder());
        project.setTestProtokoll(projectView.getDnsTestProtocol());
        project.setProcessWiki(projectView.getWikiProcess());
        project.setRiskAnalysis(projectView.getRiskAnalysis());
        project.setSonar(projectView.getSonar());

        return project;
    }
}
