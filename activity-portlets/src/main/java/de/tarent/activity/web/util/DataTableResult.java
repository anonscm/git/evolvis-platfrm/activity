/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

import java.math.BigDecimal;
import java.util.List;

import de.tarent.activity.domain.filter.FilterResult;

/**
 * Helper class for handling YUI datatable.
 *
 */
public class DataTableResult<T> extends FilterResult<T> {

    private static final long serialVersionUID = 3258731540880205767L;

    private String status;

    private BigDecimal columnSum;

    private Long startIndex;
    
    private Long itemCount;

    private String dir;

    private String sort;
    
    /**
     * Constructor using all fields.
     *
     * @param status
     *            String
     * @param records
     *            List<?> holding records to be displayed in yui table.
     * @param totalRecords
     *            long used in pagination.
     */
    public DataTableResult(String status, BigDecimal sum, List<T> records, Long totalRecords) {
        super(records, totalRecords);
        this.status = status;
        this.columnSum = sum;
    }

    public DataTableResult(String status, BigDecimal columnSum, List<T> records, Long totalRecords, Long startIndex,
            Long itemCount, String dir, String sort) {
        this(status, columnSum, records, totalRecords);
        this.startIndex = startIndex;
        this.itemCount = itemCount;
        this.dir = dir;
        this.sort = sort;
    }

    /**
     * Constructor using status and a FilterResult.
     *
     * @param status
     * @param filterResults
     */
    public DataTableResult(String status, BigDecimal columnSum, FilterResult<T> filterResults) {
        super(filterResults);
        this.status = status;
        this.columnSum = columnSum;
    }

    public String getStatus() {
        return status;
    }

    public BigDecimal getColumnSum() {
        return columnSum;
    }

    public Long getStartIndex() {
        return startIndex;
    }

    public Long getItemCount() {
        return itemCount;
    }

    public String getDir() {
        return dir;
    }

    public String getSort() {
        return sort;
    }
}
