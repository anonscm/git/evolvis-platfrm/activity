/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Project;
import de.tarent.activity.service.CostService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseBillController;
import de.tarent.activity.web.domain.InvoiceView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller("ProjectInvoiceController")
@RequestMapping(value = "view")
public class ProjectInvoiceController extends BaseBillController {

    @Autowired
    private CostService costService;

    @Autowired
    private ProjectService projectService;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();

    static {
        SORT_ALIAS.put("idTable", "invoice.pk");
        SORT_ALIAS.put("jobName", "job.name");
        SORT_ALIAS.put("name", "invoice.name");
        SORT_ALIAS.put("number", "invoice.number");
        SORT_ALIAS.put("amount", "invoice.amount");
        SORT_ALIAS.put("invoiceDate", "invoice.invoiced");
        SORT_ALIAS.put("payDate", "invoice.payed");
    }

    @ModelAttribute
    public Project populateProject(@RequestParam Long projectId) {
        return projectService.getProject(projectId);
    }

    @RenderMapping(params = "ctx=projectInvoices")
    public String viewInvoiceDetails(@RequestParam Long projectId, Model model, PortletRequest request) {
        return "projects/invoice";
    }

    @ResourceMapping("getJobListForProject")
    public String jobs(Long projectIdId, Boolean isFilter,
                       ResourceRequest request, Model model) {

        model.addAttribute("jobsList",
                projectService.getJobsByProject(projectIdId));
        model.addAttribute("isFilter", isFilter);

        return "common/jobs";
    }


    @ResourceMapping("getProjectInvoiceDetails")
    public String getProjectInvoiceDetails() {
        return "projects/invoice";
    }

    @ActionMapping("doProjectInvoiceDelete")
    public void doProjectInvoiceDelete(@RequestParam Long invoicePk,@RequestParam Long projectId, ActionResponse response) {
        costService.deleteInvoice(invoicePk);
        //response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "showProjectDetails");
        //response.setRenderParameter("projectId", projectId.toString());
    }

    @RenderMapping(params = "ctx=getProjectInvoiceEdit")
    public String getProjectInvoiceEdit(@RequestParam Long invoicePk, Model model) throws IOException {
        Invoice invoice = costService.getInvoice(invoicePk);

        InvoiceView invoiceView = new InvoiceView(invoice);

        model.addAttribute("invoiceView", invoiceView);
        model.addAttribute("customerList", projectService.getCustomerList());
        model.addAttribute("projectList", projectService.getProjectList());
        if (invoiceView.getProjectId() != null) {
            model.addAttribute("jobList", projectService.getJobsByProject(invoiceView.getProjectId()));
        } else {
            model.addAttribute("jobList", projectService.getJobList());
        }

        model.addAttribute("backToProjectInvoices", true);

        return "invoices/edit";
    }

    @RenderMapping(params = "ctx=getProjectInvoiceDetails")
    public String getProjectInvoiceDetails(@RequestParam Long invoicePk, Model model) throws IOException {
        
    	Invoice invoice = costService.getInvoice(invoicePk);

        InvoiceView invoiceView = new InvoiceView(invoice);
        model.addAttribute("invoiceView", invoiceView);

        return "invoices/details";
    }
    
    @ResourceMapping("doInvoiceSave")
    public String save(@ModelAttribute("invoiceView") @Valid InvoiceView invoiceView, BindingResult result, Model model)
            throws IOException {
        populateLists(model);
        if (!result.hasErrors()) {
            doSaveInvoice(invoiceView, model);
            return getProjectInvoiceDetails(invoiceView.getId(), model);
        } else {
            return getProjectInvoiceEdit(invoiceView.getId(), model);
        }
    }

}
