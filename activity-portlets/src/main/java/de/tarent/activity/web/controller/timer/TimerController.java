/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.timer;

import de.tarent.activity.domain.*;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.ActivityView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Controller handling the 'Timer' action.
 *
 */
@Controller("TimerController")
@RequestMapping(value = "view")
public class TimerController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimerController.class);

    private static final String UNIQUE_MESSAGE_KEY = "validate_unique_constraint";

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ProjectService projectService;

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {

        return ajaxView(model, null, request);
    }

    /**
     * @param model
     *            model
     * @param jobId
     *            jobId
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, String jobId, PortletRequest request) {

        model.addAttribute("timerLink", request.getPreferences().getValue("timerLink", "#"));

        Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());
        if (dbTimer == null) {
            getActiveJobList(model, request);
            if (jobId != null && jobId != "") {
                model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, retrievePositionsList(Long.valueOf(jobId), request));
            }
            return "timer/view";
        } else {
            long startDiff = 0;
            String status;
            if (dbTimer.getTimeDiff() != null) {
                status = "paused";
                startDiff = dbTimer.getTimeDiff().getTime() - dbTimer.getTime().getTime();
            } else {
                startDiff = new Date().getTime() - dbTimer.getTime().getTime();
                status = "running";
            }
            model.addAttribute("startDiff", startDiff);
            model.addAttribute("status", status);
            return "timer/timer";
        }
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @param jobId
     *            jobId
     * @return String
     */
    @ResourceMapping("doRecord")
    public String doRecord(Model model, PortletRequest request, String jobId) {

        model.addAttribute("timerLink", request.getPreferences().getValue("timerLink", "#"));
        Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());

        if (dbTimer != null) {
            getActiveJobList(model, request);

            ActivityView activityView = new ActivityView();

            Position poz = projectService.getPosition(dbTimer.getPosition().getPk());

            Job job = projectService.getJob(poz.getJob().getPk());

            if (job.getPk() != null) {
                model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, retrievePositionsList(job.getPk(), request));
            }

            activityView.setResourceId(RequestUtil.getLoggedUser(request).getPk());
            activityView.setJobId(job.getPk());
            activityView.setJobName(job.getName());
            activityView.setPositionId(poz.getPk());
            activityView.setPositionName(poz.getName());

            long timeDiff;
            if (dbTimer.getTimeDiff() == null) {
                timeDiff = new Date().getTime() - dbTimer.getTime().getTime();
            } else {
                timeDiff = dbTimer.getTimeDiff().getTime() - dbTimer.getTime().getTime();
            }

            double passedHours = timeDiff / (1000d * 60d * 60d);
            passedHours = getHours(passedHours);
            activityView.setHours(new BigDecimal(passedHours));
            activityView.setDate(new Date());

            model.addAttribute("activityView", activityView);
            model.addAttribute("showCancel", "show");
            return "addactivities/view";
        }

        return "timer/view";

    }
    
    /**
     * This method gives a list with active jobs for a logged user.
     *
     * @param model
     *            Model
     * @param request
     *            RenderRequest
     */
    public void getActiveJobList(Model model, PortletRequest request) {
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        List<Job> jobs = projectService.getActiveJobsByResource(resPk);
        LOGGER.info("Number of jobs active for User id " + resPk + " is: " + jobs.size());
        model.addAttribute("jobsList", jobs);
    }

    /**
     * @param positionId
     *            positionId
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doStart")
    public String doStart(Long positionId, PortletRequest request, Model model) {

        if (positionId != null && !positionId.equals(0)) {

            Timer newTimer = new Timer();
            newTimer.setResource(new Resource(RequestUtil.getLoggedUser(request).getPk()));
            newTimer.setTime(new Date());
            newTimer.setPosition(new Position(positionId));
            activityService.addTimer(newTimer);

        }
        return ajaxView(model, null, request);

    }

    /**
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doPause")
    public String doPause(PortletRequest request, Model model) {

        Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());
        dbTimer.setTimeDiff(new Date());
        activityService.updateTimer(dbTimer);

        return ajaxView(model, null, request);

    }

    /**
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doReset")
    public String doReset(PortletRequest request, Model model) {

        Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());
        activityService.deleteTimer(dbTimer.getPk());
        return ajaxView(model, null, request);
    }

    /**
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doContinue")
    public String doContinue(PortletRequest request, Model model) {

        Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());

        Date originalStart = dbTimer.getTime();
        Date paused = dbTimer.getTimeDiff();

        if (paused != null) {
            long newStartTime = originalStart.getTime() + (new Date().getTime() - paused.getTime());
            dbTimer.setTime(new Date(newStartTime));
            dbTimer.setTimeDiff(null);
            activityService.updateTimer(dbTimer);
        }
        return ajaxView(model, null, request);
    }

    /**
     * @param activityView
     *            activityView
     * @param result
     *            result
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("doAdd")
    public String doAdd(@ModelAttribute("activityView") @Valid ActivityView activityView, BindingResult result,
            PortletRequest request, Model model) {

        model.addAttribute("timerLink", request.getPreferences().getValue("timerLink", "#"));

        if (!result.hasErrors()) {

            Activity a = new Activity();
            a.setResource(new Resource(RequestUtil.getLoggedUser(request).getPk()));
            a.setHours(activityView.getHours());
            a.setName(activityView.getDescription());
            a.setPosition(new Position(activityView.getPositionId()));
            a.setDate(activityView.getDate());
            try {            	
            	a.setEvolvisTaskId(Long.valueOf(activityView.getEvolvisTaskId()));
            } catch (NumberFormatException e) {
            	// do nothing
            }
            LOGGER.debug("Selected activity: " + activityView);

            try {
                activityService.addActivity(a);
                Timer dbTimer = activityService.getTimerByResource(RequestUtil.getLoggedUser(request).getPk());
                activityService.deleteTimer(dbTimer.getPk());

                model.addAttribute("activityView", new ActivityView());
                return ajaxView(model, null, request);
            } catch (UniqueValidationException e) {
                LOGGER.error("Cannot save activity, unique constraint violation.");
                Object[] errorArgs = { "activity", e.getConstraint().toString() };
                result.reject(UNIQUE_MESSAGE_KEY, errorArgs, "Unique constraint violated");
            }
        }

        getActiveJobList(model, request);

        if (activityView.getJobId() != null) {
            model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, retrievePositionsList(activityView.getJobId(), request));
        }

        return "addactivities/view";

    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("doCancel")
    public String doCancel(Model model, PortletRequest request) {
        return ajaxView(model, null, request);
    }
    
    /**
     * This method gives a position list by a jobId and current logged user.
     *
     * @param jobId
     *            Job ID
     * @param request
     *            PortletRequest
     * @return List<Position>
     */
    public List<Position> retrievePositionsList(Long jobId, PortletRequest request) {
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        return projectService.getPositionsByJob(jobId, null, resPk);
    }

    /**
     * Calculates scaled hours.
     *
     * @param hours
     *            difference in hours
     * @return scaled hours
     */
    private static double getHours(double hours) {
        Double rest = hours % 0.25;
        if (rest.doubleValue() != 0.0) {
            Double loss = 0.25 - rest;
            hours += loss;
        }
        return hours;
    }

}
