/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activitymenu;

import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.web.util.PermissionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import java.io.IOException;

import static de.tarent.activity.service.util.PermissionIds.*;

/**
 * Controller to handle menu portlet actions.
 */
@Controller("ActivityMenuController")
@RequestMapping(value = "view")
public class ActivityMenuController {

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    @Autowired
    private PermissionChecker permissionChecker;

    /**
     * Default render method.
     */
    @RenderMapping
    public String view() {
        return "activitymenu/view";
    }

    @ActionMapping("redirect")
    public void clearPublicRenderParameterAndRedirect(@RequestParam String redirect, ActionResponse response)
            throws IOException {
        response.sendRedirect(redirect);
    }

    @ModelAttribute("myActivitiesPageLink")
    public String populateMyActivitiesPage(PortletRequest request) {
        if (permissionChecker.checkPermission(request, ACTIVITY_VIEW)) {
            return menuPropertiesService.getMyActivitiesPageLink();
        }
        return null;
    }

    @ModelAttribute("myOvertimesPageLink")
    public String populateMyOvertimePage(PortletRequest request) {
        if (permissionChecker.checkPermission(request, OVERTIME_VIEW)) {
            return menuPropertiesService.getMyOvertimesPageLink();
        }
        return null;
    }

    @ModelAttribute("myHolidayPageLink")
    public String populateMyHolidayPage(PortletRequest request) {
        if (permissionChecker.checkPermission(request, LOSS_VIEW)) {
            return menuPropertiesService.getMyHolidayPageLink();
        }
        return null;
    }

    @ModelAttribute("myPositionsPageLink")
    public String populateMyPositionsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, POSITION_VIEW)) {
            return menuPropertiesService.getMyPositionsPageLink();
        }
        return null;
    }

    /*@ModelAttribute("mySkillsPageLink")
    public String populateMyskillsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, SKILL_VIEW)) {
            return menuPropertiesService.getMySkillsPageLink();
        }
        return null;
    }*/

    @ModelAttribute("customersPageLink")
    public String populateCustomersPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, CUSTOMER_VIEW_ALL)) {
            return menuPropertiesService.getCustomersPageLink();
        }
        return null;
    }

    @ModelAttribute("projectsPageLink")
    public String populateProjectsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, PROJECT_VIEW_MANAGER, PROJECT_VIEW_ALL, PROJECT_VIEW_BY_DIVISION)) {
            return menuPropertiesService.getProjectsPageLink();
        }
        return null;
    }

    @ModelAttribute("jobsPageLink")
    public String populateJobsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, JOB_VIEW_MANAGER, JOB_VIEW_ALL, JOB_VIEW_BY_DIVISION)) {
            return menuPropertiesService.getJobsPageLink();
        }
        return null;
    }

    @ModelAttribute("positionsPageLink")
    public String populatePositionsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, POSITION_VIEW_MANAGER, POSITION_VIEW_ALL, POSITION_VIEW_BY_DIVISION)) {
            return menuPropertiesService.getPositionsPageLink();
        }
        return null;
    }

    @ModelAttribute("resourceAllocationPageLink")
    public String populateResourceAllocationPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, RESOURCE_APPEND_ALL, RESOURCE_DETACH_ALL)) {
            return menuPropertiesService.getResourceAllocationPageLink();
        }
        return null;
    }

    @ModelAttribute("skillsPageLink")
    public String populateSkillsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, SKILL_VIEW_ALL)) {
            return menuPropertiesService.getSkillsPageLink();
        }
        return null;
    }

    @ModelAttribute("deleteRequestsPageLink")
    public String populateDeleteRequestsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, LOSS_DELETE_ALL, ACTIVITY_DELETE_ALL, OVERTIME_DELETE_ALL)) {
            return menuPropertiesService.getDeleteRequestsPageLink();
        }
        return null;
    }

    @ModelAttribute("activitiesPageLink")
    public String populateActivitiesPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, ACTIVITY_VIEW_ALL, ACTIVITY_VIEW_BY_DIVISION)) {
            return menuPropertiesService.getActivitiesPageLink();
        }
        return null;
    }

    @ModelAttribute("invoicesPageLink")
    public String populateInvoicesPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, INVOICE_VIEW_ALL)) {
            return menuPropertiesService.getInvoicesPageLink();
        }
        return null;
    }

    @ModelAttribute("overtimesPageLink")
    public String populateOvertimesPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, OVERTIME_VIEW_ALL, OVERTIME_VIEW_BY_PROJECT, OVERTIME_VIEW_BY_DIVISION)) {
            return menuPropertiesService.getOvertimesPageLink();
        }
        return null;
    }

    @ModelAttribute("ldapImportPageLink")
    public String populateLdapImportPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, ADMIN_IMPORT_LDAP)) {
            return menuPropertiesService.getLdapImportPageLink();
        }
        return null;
    }

    @ModelAttribute("resourcesPageLink")
    public String populateResourcesPageLink(PortletRequest request) {
    	if (permissionChecker.checkPermission(request, RESOURCE_EDIT_ALL)) {
            return menuPropertiesService.getResourcesPageLink();
        }
        return null;
    }

    @ModelAttribute("permissionsPageLink")
    public String populatePermissionsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, ADMIN_EDIT_PERMISSIONS)) {
            return menuPropertiesService.getPermissionsPageLink();
        }
        return null;
    }

    @ModelAttribute("holidayPageLink")
    public String populateHolidayPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, LOSS_VIEW_ALL)) {
            return menuPropertiesService.getHolidayPageLink();
        }
        return null;
    }

    @ModelAttribute("holidayResponsesPageLink")
    public String populateHolidayResponsesPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, LOSS_VIEW_ALL, LOSS_VIEW)) {
            return menuPropertiesService.getHolidayResponsesPageLink();
        }
        return null;
    }

    @ModelAttribute("myRequestsPageLink")
    public String populateMyRequestsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, OVERTIME_VIEW)) {
            return menuPropertiesService.getMyRequestsPageLink();
        }
        return null;
    }

    @ModelAttribute("myActiveProjectsPageLink")
    public String populateMyActiveProjectsPageLink(PortletRequest request) {
        if (permissionChecker.checkPermission(request, OVERTIME_VIEW)) {
            return menuPropertiesService.getMyActiveProjectsPageLink();        }
        return null;
    }

}
