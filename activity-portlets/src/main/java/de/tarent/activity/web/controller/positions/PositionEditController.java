/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.positions;

import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PosResourceMappingFilter;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.PosResourceMappingView;
import de.tarent.activity.web.domain.PosResourceStatusView;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("PositionEditController")
@RequestMapping(value = "view", params = "ctx=positionEdit")
public class PositionEditController extends BaseAjaxController {

	private static final Map<String, String> SORT_ALIAS_RESOURCE = new HashMap<String, String>();
	static {
		SORT_ALIAS_RESOURCE.put("resourceName",
				"posResMapping.resource.firstname");
		SORT_ALIAS_RESOURCE.put("startDate", "posResMapping.startDate");
		SORT_ALIAS_RESOURCE.put("endDate", "posResMapping.endDate");
	}

	@Autowired
	private ProjectService projectService;

	@Autowired
	@Qualifier("positionViewValidator")
	private Validator positionViewValidator;

	@Autowired
	private MenuPropertiesService menuPropertiesService;

	@Autowired
	private ResourceService resourceService;

	@Autowired
	private PermissionChecker permissionChecker;

    @Autowired
    @Qualifier("positionResourceViewValidator")
    private Validator positionResourceViewValidator;

    private ObjectMapper objectMapper = new NullToEmptyStringObjectMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionController.class);

    @ModelAttribute("jobsList")
	public List<Job> populateJobList() {
		return projectService.getJobListToManage();
	}

	@ModelAttribute(ActivityConstants.STATUS_LIST_CONSTANT)
	public List<PositionStatus> populateStatusList() {
		return projectService.positionStatusList();
	}

	@RenderMapping
	public String doEdit(@RequestParam(required = true) Long positionId,
			Model model) {

		PositionView positionView = new PositionView(
				projectService.getPosition(positionId));
		if (((BindingAwareModelMap) model).get(ActivityConstants.POSITION_VIEW_CONSTANT) == null) {
			model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, positionView);
		}

		return "positions/edit";
	}

	@RenderMapping(params = "action=getPositionCancel")
	public String getPositionCancel() {
		return "positions/view";
	}

	@ActionMapping("doPositionSave")
	public void doPositionSave(
			@ModelAttribute(ActivityConstants.POSITION_VIEW_CONSTANT) @Valid PositionView positionView,
			BindingResult result, Model model, PortletRequest request,
			ActionResponse response) {
		positionViewValidator.validate(positionView, result);
		
		if ( !positionIsUniqueWithinTheJob(positionView) ){
            model.addAttribute("errorMessage", "validate_unique_constraint");
            showAppropriateReturnView(positionView, response);
            return;
    	}
		
		Position position = new Position();
		boolean isNewPosition = positionView.getId() == null ? true : false; 
        
		if (!isNewPosition) {
            position = projectService.getPosition(positionView.getId());
        }
        if (!result.hasErrors()) {
            position.setName(positionView.getPositionName());
            position.setNote(positionView.getDescription());
            position.setJob(projectService.getJob(positionView.getJobId()));
            position.setPositionStatus(projectService
                    .getPositionStatus(positionView.getPositionStatusId()));
            position.setExpectedWork(positionView.getExpectedWork());
            position.setCommunicatedWork(positionView.getCommunicatedWork());
            position.setFixedPrice(positionView.getFixedPrice());
            position.setEvolvisurl(positionView.getEvolvisURL());
            position.setEvolvisprojektid(positionView.getEvolvisProjectId());

            position.setPositionStartDate(positionView.getBeginDate());
            position.setPositionEndDate(positionView.getEndDate());
            if (isNewPosition) {
            	projectService.addPosition(position).toString();
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getPositionAdd");
                
            } else {
                projectService.updatePosition(position);
                response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "positionEdit");
                response.setRenderParameter("positionId", positionView.getId()
                        .toString());
            }

            // Show success message for added position
            model.addAttribute("successMessage", "saved");
        } else {
            // Unsuccessfull show postion edit view
            showAppropriateReturnView(positionView, response);
        }
	}

    /*Todo: This is a frontend check for unique case, should be done in backend db on table
            but right now there are existing duplicate records from old activity dump which needs to
            be sorted out first.*/
    private boolean positionIsUniqueWithinTheJob(PositionView positionView) {
        List<Position> positions = projectService.getPositionsByJob(positionView.getJobId(), null, null);
        for(Position pos : positions){
        	if(pos.getName().trim().equals(positionView.getPositionName().trim())){
        		if(positionView.getId() == null){
        			return false;
        		} else if(!pos.getPk().equals(positionView.getId())){
        			return false;
        		}
        	}
        }
        return true;
    }

    private void showAppropriateReturnView(PositionView positionView, ActionResponse response) {
        if (positionView.getId() != null) {
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "positionEdit");
            response.setRenderParameter("positionId", positionView.getId()
                    .toString());
        } else {
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getPositionAdd");
        }
    }

    @ActionMapping("removeResource")
	public void removeResource(Long id, Long positionId, Model model,
			PortletRequest request, ActionResponse response) throws IOException {

		projectService.deletePosResourceMapping(id);
        setRenderMethodToDoAssign(positionId.toString(), response);
        request.setAttribute("successMessage", "deleted");

	}

	@ResourceMapping("changeStatus")
	public String changeStatus(Long statusId, Long resourceId, Long positionId,
			Model model, PortletRequest request) throws IOException {

		PosResourceMapping res = projectService
				.getPosResourceMappingsByPositionAndResource(positionId,
						resourceId);
		res.setPosResourceStatus(new PosResourceStatus(statusId));
		projectService.updatePosResourceMapping(res);

		return assign(model, positionId, request);
	}

	/**
	 * @param model
	 *            model
	 * @param id
	 *            id
	 * @param request
	 *            request
	 * @return String
	 * @throws IOException
	 *             IOException
	 */
	@RenderMapping(params = "action=doAssign")
	public String assign(Model model, Long id, PortletRequest request)
			throws IOException {
		Position position = projectService.getPosition(id);
		PosResourceMappingView posResourceMappingView = new PosResourceMappingView();
		posResourceMappingView.setJobId(position.getJob().getPk());
		posResourceMappingView.setJobName(position.getJob().getName());
		posResourceMappingView.setPositionId(id);
		posResourceMappingView.setPositionName(position.getName());
		model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);
		List<PosResourceStatus> statusList = resourceService
				.posResourceStatusList();
		model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);

		List<Resource> resourceList = new ArrayList<Resource>();
		if (permissionChecker.checkPermission(request,
				PermissionIds.RESOURCE_VIEW_ALL, PermissionIds.RESOURCE_DETACH_ALL)) {
			resourceList = resourceService.listActiveResources();
		} else {
			resourceList.add(RequestUtil.getLoggedUser(request));
		}
		model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);

		String linkToJobPage = menuPropertiesService.getJobsPageLink();
		if (linkToJobPage != null) {
			model.addAttribute("linkToJobPage", linkToJobPage);
		}

		String linkToCurrentJobPage = RenderParametersUtil.getPublicPortletUrl(
				menuPropertiesService.getJobsPageLink(), "publicJobId",
				position.getJob().getPk().toString());
		if (linkToCurrentJobPage != null) {
			model.addAttribute("linkToCurrentJobPage", linkToCurrentJobPage);
		}

		String linkToPositionPage = RenderParametersUtil.getPublicPortletUrl(
				menuPropertiesService.getPositionsPageLink(), "publicJobId",
				position.getJob().getPk().toString());
		if (linkToPositionPage != null) {
			model.addAttribute("linkToPositionPage", linkToPositionPage);
		}

		String linkToCurrentPositionPage = RenderParametersUtil
				.getPublicPortletUrl(
						menuPropertiesService.getMyPositionsPageLink(),
						"publicPositionId", position.getPk().toString());
		if (linkToCurrentPositionPage != null) {
			model.addAttribute("linkToCurrentPositionPage",
					linkToCurrentPositionPage);
		}

		List<PosResourceStatusView> statusViews = new ArrayList<PosResourceStatusView>();
		for (PosResourceStatus s : statusList) {
			PosResourceStatusView sv = new PosResourceStatusView(s.getPk(),
					s.getName());
			statusViews.add(sv);
		}

		model.addAttribute("statusViews",
				objectMapper.writeValueAsString(statusViews));
		return "positions/editResource";
	}

	@ResourceMapping("tableResources")
	public String tableResources(String sort, String dir, Long startIndex,
			Long results, Long selectedPositionId, Model model,
			ResourceRequest request) throws IOException {
		PosResourceMappingFilter filter = new PosResourceMappingFilter(null,
				null, selectedPositionId, startIndex, null,
				SORT_ALIAS_RESOURCE.get(sort), dir);

		FilterResult<PosResourceMapping> posResourceMapping = projectService
				.searchPosResourceMapping(filter);
		List<PosResourceMappingView> list = new ArrayList<PosResourceMappingView>();

		for (PosResourceMapping r : posResourceMapping.getRecords()) {
			PosResourceMappingView pv = new PosResourceMappingView(r);
			list.add(pv);
		}

		DataTableResult<PosResourceMappingView> dataTableResult = new DataTableResult<PosResourceMappingView>(
				"resource list", null, list, (long) list.size());

		model.addAttribute(ActivityConstants.JSON_CONSTANT,
				objectMapper.writeValueAsString(dataTableResult));
		return ActivityConstants.JSON_CONSTANT;
	}



    @ActionMapping("doDelete")
    public void doDelete(Long id, Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, new PositionView());
        projectService.deletePosition(id);

        request.setAttribute("successMessage", "deleted");
    }

    /**
     * @param posResourceMappingView
     *            posResourceMappingView
     * @param result
     *            result
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     */
    @ActionMapping("doSave")
    public void doSave(
            @ModelAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT) @Valid PosResourceMappingView posResourceMappingView,
            BindingResult result, Model model, PortletRequest request, ActionResponse response) throws IOException {
        positionResourceViewValidator.validate(posResourceMappingView, result);

        Position position = projectService.getPosition(posResourceMappingView.getPositionId());

        String linkToJobPage = menuPropertiesService.getJobsPageLink();
        if (linkToJobPage != null) {
            model.addAttribute("linkToJobPage", linkToJobPage);
        }

        String linkToCurrentJobPage = RenderParametersUtil.getPublicPortletUrl(menuPropertiesService.getJobsPageLink(),
                "publicJobId", position.getJob().getPk().toString());
        if (linkToCurrentJobPage != null) {
            model.addAttribute("linkToCurrentJobPage", linkToCurrentJobPage);
        }

        String linkToPositionPage = menuPropertiesService.getPositionsPageLink();
        if (linkToPositionPage != null) {
            model.addAttribute("linkToPositionPage", linkToPositionPage);
        }

        String linkToCurrentPositionPage = RenderParametersUtil.getPublicPortletUrl(
                menuPropertiesService.getPositionsPageLink(), "publicPositionId", position.getJob().getPk().toString());
        if (linkToCurrentPositionPage != null) {
            model.addAttribute("linkToCurrentPositionPage", linkToCurrentPositionPage);
        }

        if (!result.hasErrors()) {

            PosResourceMapping posResourceMapping;
            PosResourceMapping posResMapp = projectService.getPosResourceMappingsByPositionAndResource(
                    posResourceMappingView.getPositionId(), posResourceMappingView.getResourceId());
            if (posResMapp != null) {
                model.addAttribute("exist", "exist");
                List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
                model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
                List<Resource> resourceList = resourceService.listActiveResources();
                model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);

                List<PosResourceStatusView> statusViews = new ArrayList<PosResourceStatusView>();
                for (PosResourceStatus s : statusList) {
                    PosResourceStatusView sv = new PosResourceStatusView(s.getPk(), s.getName());
                    statusViews.add(sv);
                }

                model.addAttribute("statusViews", objectMapper.writeValueAsString(statusViews));

                model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);

                response.setRenderParameter("id", posResourceMappingView.getPositionId().toString());
            } else {

                posResourceMapping = new PosResourceMapping();
                posResourceMapping.setPosition(new Position(posResourceMappingView.getPositionId()));
                posResourceMapping.setResource(new Resource(posResourceMappingView.getResourceId()));
                posResourceMapping.setPosResourceStatus(new PosResourceStatus(posResourceMappingView.getStatusId()));
                posResourceMapping.setStartDate(posResourceMappingView.getStartDate());
                posResourceMapping.setEndDate(posResourceMappingView.getEndDate());
                posResourceMapping.setPercent(posResourceMappingView.getPercent());
                projectService.addPosResourceMapping(posResourceMapping);

                List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
                model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
                List<Resource> resourceList = resourceService.listActiveResources();
                model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);

                List<PosResourceStatusView> statusViews = new ArrayList<PosResourceStatusView>();
                for (PosResourceStatus s : statusList) {
                    PosResourceStatusView sv = new PosResourceStatusView(s.getPk(), s.getName());
                    statusViews.add(sv);
                }

                model.addAttribute("statusViews", objectMapper.writeValueAsString(statusViews));

                model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);

            }

            request.setAttribute("successMessage", "saved");
        } else {
            for (ObjectError error : result.getAllErrors()) {
                LOGGER.error(error.toString());
                model.addAttribute("errors", "validate_all");
            }

            List<Resource> resourceList = resourceService.listActiveResources();
            model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);
            List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
            model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);

            model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);

            List<PosResourceStatusView> statusViews = new ArrayList<PosResourceStatusView>();
            for (PosResourceStatus s : statusList) {
                PosResourceStatusView sv = new PosResourceStatusView(s.getPk(), s.getName());
                statusViews.add(sv);
            }

            model.addAttribute("statusViews", objectMapper.writeValueAsString(statusViews));

        }
        setRenderMethodToDoAssign(posResourceMappingView.getPositionId().toString(), response);
    }

    private void setRenderMethodToDoAssign(String positionId, ActionResponse response) {
        response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "positionEdit");
        response.setRenderParameter("action", "doAssign");
        response.setRenderParameter("id", positionId);
    }
}
