/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.positions;

import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.PositionStatus;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.EscapeDescriptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.PortletRequest;
import java.io.IOException;
import java.util.List;

@Controller("PositionDetailController")
@RequestMapping(value = "view", params = "ctx=positionDetails")
public class PositionDetailController extends BaseAjaxController{
    
    @Autowired
    private ProjectService projectService;

    @ModelAttribute(ActivityConstants.POSITION_VIEW_CONSTANT)
    public PositionView populatePositionView(@RequestParam Long positionId){
        Position position = projectService.getPosition(positionId);
        PositionView positionView = new PositionView(position);
        if (positionView.getDescription() != null){
            positionView
            .setDescription(EscapeDescriptionUtil.escapeDescriptionWithoutBr(positionView.getDescription()));
        }
        positionView.setStats(projectService.getPositionStats(positionId));
        return positionView;
    }
    
    @ModelAttribute("isDeletablePosition")
    public boolean populateIsDeletable(@RequestParam Long positionId){
        return projectService.isDeletablePosition(positionId);
    }
    
    @ActionMapping("doDelete")
    public void doDelete(Long id, Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, new PositionView());
        projectService.deletePosition(id);

        request.setAttribute("successMessage", "deleted");
    }
    
    @RenderMapping
    public String getPositionDetail() {
        return "positions/details";
    }

    // TODO own permission? at the moment we use VIEW_ALL
    @RenderMapping(params = "action=showChanges")
    public String showChanges(Long positionId, Model model, PortletRequest request) throws IOException {
        getCommonLists(model);

        Position position = projectService.getPosition(positionId);
        PositionView positionView = new PositionView(position);
        model.addAttribute(ActivityConstants.POSITION_VIEW_CONSTANT, positionView);

        List<ChangeInfo> changeInfo = projectService.getPositionChangeInfo(positionId);
        model.addAttribute("positionChanges", changeInfo);

        return "positions/changes";
    }

    /**
     * @param model
     *            model
     */
    private void getCommonLists(Model model) {
        List<Job> jobs = projectService.getJobListToManage();
        List<PositionStatus> statusList = projectService.positionStatusList();
        model.addAttribute("jobsList", jobs);
        model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
    }

}
