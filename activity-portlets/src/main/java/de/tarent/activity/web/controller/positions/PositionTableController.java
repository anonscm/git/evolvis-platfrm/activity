/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.positions;

import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.util.PermissionIds;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.PermissionChecker;
import de.tarent.activity.web.util.RequestUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("PositionTableController")
@RequestMapping(value = "view")
public class PositionTableController {

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
    	SORT_ALIAS.put("positionId", "position.id");
        SORT_ALIAS.put("positionName", "position.name");
        SORT_ALIAS.put("description", "position.note");
        SORT_ALIAS.put("positionStatusName", "positionStatus.name");
        SORT_ALIAS.put("expectedWork", "position.expectedWork");
        SORT_ALIAS.put("communicatedWork", "position.communicatedWork");
        SORT_ALIAS.put("projectName", "project.name");
        SORT_ALIAS.put("jobName", "job.name");
    }
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private PermissionChecker permissionChecker;
    
    @Autowired
    private ObjectMapper objectMapper;

    @ResourceMapping("getPositionTable")
    public String table(String sort, String dir, Long startIndex, Long results, Long jobId, Model model,
            ResourceRequest request) throws IOException {

        PortletSession ps = request.getPortletSession();
        ps.setAttribute("LIFERAY_SHARED_positionsRowsPerPage", results, PortletSession.APPLICATION_SCOPE);

        PositionFilter filter = new PositionFilter(null, jobId, startIndex, results, SORT_ALIAS.get(sort), dir);

        if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW_ALL)) {
            filter.setViewBy(PositionFilter.VIEW_ALL);
        } else if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW_MANAGER)) {
            filter.setViewBy(PositionFilter.VIEW_BY_MANAGER);
            filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
        } else if (permissionChecker.checkPermission(request, PermissionIds.POSITION_VIEW)) {
            filter.setViewBy(PositionFilter.VIEW_OWN);
            filter.setResourceId(RequestUtil.getLoggedUser(request).getPk());
        } else {
            throw new PermissionException("You do not have the permission to view positions.");
        }
        
        FilterResult<Position> positions = projectService.searchPosition(filter);

        List<PositionView> list = new ArrayList<PositionView>();
        for (Position p : positions.getRecords()) {
            PositionView pv = new PositionView(p);
            pv.setStats(projectService.getPositionStats(p.getPk()));
            list.add(pv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<PositionView>("position list",
                null, list, positions.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;
    }
}
