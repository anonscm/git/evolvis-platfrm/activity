/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import de.tarent.activity.domain.Skills;

public class SkillsView implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long skillsDefId;

    @NotEmpty(message = "{validate_name_not_empty}")
    private String skillsDefName;

    @NotNull(message = "{validate_type_not_empty}")
    private Integer value;

    public SkillsView() {

    }

    public SkillsView(Long id, Long skillsDefId, String skillsDefName, Integer value) {
        this.id = id;
        this.skillsDefId = skillsDefId;
        this.skillsDefName = skillsDefName;
        this.value = value;
    }

    public SkillsView(Skills skill) {
        this(skill.getPk(), skill.getSkillsDef().getPk(), skill.getSkillsDef().getName(), skill.getValue());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkillsDefName() {
        return skillsDefName;
    }

    public void setSkillsDefName(String skillsDefName) {
        this.skillsDefName = skillsDefName;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Long getSkillsDefId() {
        return skillsDefId;
    }

    public void setSkillsDefId(Long skillsDefId) {
        this.skillsDefId = skillsDefId;
    }

}
