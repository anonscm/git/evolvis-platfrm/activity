/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.projects.ajax;

import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.PosResourceMapping;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.PosResourceMappingFilter;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.PosResourceMappingView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import java.io.IOException;
import java.util.*;

/**
 * Projects portal controller.
 * 
 */
@Controller("ProjectTeamController")
@RequestMapping(value = "view")
public class ProjectTeamController extends BaseAjaxController {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS_RESOURCE = new HashMap<String, String>();
    static {
        SORT_ALIAS_RESOURCE.put("id", "resource.pk");
        SORT_ALIAS_RESOURCE.put("resourceName", "posResMapping.resource.lastname");
        SORT_ALIAS_RESOURCE.put("startDate", "posResMapping.startDate");
        SORT_ALIAS_RESOURCE.put("endDate", "posResMapping.endDate");
    }


    @ModelAttribute
    public Project populateProject(@RequestParam Long projectId) {
        return projectService.getProject(projectId);
    }

    /**
     * Projects team details tab.
     * 
     * @param projectId
     * @param model
     * @param request
     * @return
     */
    @ResourceMapping("getProjectTeamDetails")
    public String getProjectTeamDetails(@ModelAttribute Project project, Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, sortResourceByLastName(resourceService.getResourceFromProject(project.getPk())));
        model.addAttribute("currentLossList", sorByLastName(activityService.getCurrentLossByProject(new Date(), project.getPk())));
        model.addAttribute("futureLossList", sorByLastName(activityService.getFutureLossByProject(new Date(), project.getPk())));

        return "projects/team";
    }

    private List<Resource> sortResourceByLastName(List<Resource> resourceList) {
        Collections.sort(resourceList, new Comparator<Resource>()
        {
            public int compare(Resource r1, Resource r2){
                String r1LastName = r1.getLastname() != null ? r1.getLastname() : "";
                String r2LastName = r2.getLastname() != null ? r2.getLastname() : "";
                return r1LastName.compareTo(r2LastName.toString());
            }
        });
        return resourceList;
    }

    private List<Loss> sorByLastName(List<Loss> lossList) {
        Collections.sort(lossList, new Comparator<Loss>()
        {
            public int compare(Loss l1, Loss l2){
                String r1LastName = l1.getResourceByFkResource().getLastname() != null ? l1.getResourceByFkResource().getLastname() : "";
                String r2LastName = l2.getResourceByFkResource().getLastname() != null ? l2.getResourceByFkResource().getLastname() : "";
                return r1LastName.compareTo(r2LastName.toString());
            }
        });
        return lossList;
    }

    @ResourceMapping("getTeamMembersTable")
    public String tableResources(@RequestParam String projectId, String sort, String dir, Long startIndex, Long results, Long selectedPositionId,
                                 Model model, ResourceRequest request) throws IOException {


        PosResourceMappingFilter filter = new PosResourceMappingFilter(new Long(projectId), null, null, startIndex,
                null, SORT_ALIAS_RESOURCE.get(sort), dir);

        FilterResult<PosResourceMapping> posResourceMapping = projectService.searchPosResourceMapping(filter);
        List<PosResourceMappingView> list = new ArrayList<PosResourceMappingView>();

        for (PosResourceMapping r : posResourceMapping.getRecords()) {
            PosResourceMappingView pv = new PosResourceMappingView(r);
            list.add(pv);
        }

        DataTableResult<PosResourceMappingView> dataTableResult = new DataTableResult<PosResourceMappingView>(
                "resource list", null, list, (long) list.size());

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(dataTableResult));
        return ActivityConstants.JSON_CONSTANT;
    }

}
