/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.clients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.*;

import javax.portlet.MimeResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.ResourceResponse;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.ChangeInfo;
import de.tarent.activity.domain.Project;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.*;
import de.tarent.activity.web.domain.ActivityView;
import de.tarent.activity.web.domain.ProjectView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.controller.clients.ajax.ClientListController;

/**
 * ClientController is the initial Controller for the clients portlet.
 * 
 * The default render method only returns an initial view of the portlet which mostly contains drop-down lists to filter
 * content of a client data table. The data table itself is added to view by ajax calls to {@link ClientListController}.
 */
@Controller("ClientController")
@RequestMapping(value = "view")
public class ClientController extends BaseController {


    @Autowired
    private ActivityService activityService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "activity.pk");
        SORT_ALIAS.put("jobName", "activity.position.job.name");
        SORT_ALIAS.put("positionName", "activity.position.name");
        SORT_ALIAS.put("resourceName", "activity.resource.username");
        SORT_ALIAS.put("description", "activity.name");
        SORT_ALIAS.put("hours", "activity.hours");
        SORT_ALIAS.put("date", "activity.date");
        SORT_ALIAS.put("evolvisTaskId", "activity.evolvisTaskId");
    }

    /**
     * Default view of clients portlet.
     * 
     * @throws UnsupportedEncodingException
     */
    @RenderMapping
    public String defaultView(Model model, MimeResponse response, PortletRequest request) {
        PortletSession ps = request.getPortletSession();
        setUserPreferencesInSession(model, ps, "LIFERAY_SHARED_clientsRowsPerPage", "clientsRowsPerPage");
        return "clients/view";
    }
    
    @RenderMapping(params = "ctx=cancelCustomerView")
    public String cancelRenderView(Model model, MimeResponse response, PortletRequest request) {

        return defaultView(model, response, request);
    }

    /**
     * Default view of clients portlet (ajax-able version).
     */
    @ResourceMapping
    public String defaultAjaxView(Model model, ResourceResponse response, PortletRequest request) {
        return defaultView(model, response, request);
    }

    /**
     * Default view of clients portlet (ajax-able version). This ResourceMapping has an unique id and is used to
     * register initial browser history of this portlet.
     */
    @ResourceMapping("getClientList")
    public String defaultResourceView(Model model, ResourceResponse response, PortletRequest request) {
        return defaultAjaxView(model, response, request);
    }

    @ResourceMapping("getActivityTable")
    public String getActivityTable(String sort, String dir, Long startIndex, Long results, Long selectedPosition,
                                   Long jobId, Long selectedResource, Date startdate, Date enddate, Model model, PortletRequest request)
            throws IOException {
        ActivityFilter filter = new ActivityFilter(selectedResource, jobId, selectedPosition, startdate, enddate,
                null, startIndex, results, SORT_ALIAS.get(sort), dir);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        List<ActivityView> list = new ArrayList<ActivityView>();
        for (Activity a : activities.getRecords()) {
            ActivityView av = new ActivityView(a);
            list.add(av);
        }

        BigDecimal sum = activityService.getActivitiesHoursSum(filter);

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<ActivityView>("activity list",
                sum, list, activities.getTotalRecords())));

        return ActivityConstants.JSON_CONSTANT;
    }

    @ResourceMapping("showChanges")
    public String showChanges(@RequestParam Long projectId, Model model, PortletRequest request) throws IOException {
        Project project = projectService.getProject(projectId);
        ProjectView projectView = new ProjectView(project);
        model.addAttribute("projectView", projectView);
        List<ChangeInfo> changeInfo = projectService.getProjectChangeInfo(projectId);
        model.addAttribute("projectChanges", changeInfo);

        return "projects/changes";
    }

}
