/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.jobs;

import de.tarent.activity.domain.*;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.JobView;
import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller("JobUpdateController")
@RequestMapping(value = "view")
public class JobUpdateController extends BaseAjaxController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobUpdateController.class);

    private static final String UNIQUE_MESSAGE_KEY = "validate_unique_constraint";    
    private static final String ATTRIBUTE_JOB_ID = "jobId";

    private static final Character BILLABLE = 't';
    private static final Character NON_BILLABLE = 'f';


    @Autowired
    private MailService mailService;

    @Autowired
    @Qualifier("jobViewValidator")
    private Validator jobViewValidator;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @ModelAttribute
    public JobView populateJobView(@RequestParam(required = false) Long jobId) {
        JobView jobView = null;
        if (jobId != null) {
            Job job = projectService.getJob(jobId);
            jobView = new JobView(job);
        } else {
            jobView = new JobView();
        }
        return jobView;
    }

    @RenderMapping(params = "ctx=jobUpdate")
    public String edit(Model model,
                       @RequestParam(required = false, value = ATTRIBUTE_JOB_ID) Long jobId,
                       @RequestParam(required = false, value = "projectId" ) Long projectId,
                       @RequestParam(required = false, value = "projectBeginDate" ) Date projectBeginDate,
                       @RequestParam(required = false, value = "projectOwner" ) Long projectOwner,
                       @RequestParam(required = false, value = "projectCostCenter" ) Long projectCostCenter) {

        JobView jobView = getJobViewFromModle(model);

        if(jobView != null){
            populateJobViewWithProjectValues(projectId, projectBeginDate, projectOwner, projectCostCenter, jobView);
        }else{
            if(jobId == null){
                model.addAttribute("jobView", new JobView());
            }else{
                model.addAttribute("jobView", new JobView(projectService.getJob(jobId)));
            }
        }

        model.addAttribute("managerList", resourceService.listActiveResources());
        model.addAttribute("projectList", projectService.getProjectList());
        model.addAttribute("jobTypes", projectService.getJobTypeList());
        model.addAttribute("jobStatuses", projectService.getJobStatusList());

        return "jobs/edit";
    }

    private void populateJobViewWithProjectValues(Long projectId, Date projectBeginDate, Long projectOwner, Long projectCostCenter, JobView jobView) {
        if( projectId != null ) {
            jobView.setProjectId(projectId);
        }
        if( projectBeginDate != null ) {
            jobView.setJobStartDate(projectBeginDate);
        }
        if( projectOwner != null ) {
            jobView.setManagerId(projectOwner);
        }
        if( projectCostCenter != null ) {
            jobView.setAccounts(projectCostCenter);
        }
    }

    private JobView getJobViewFromModle(Model model) {
        return ((JobView)((BindingAwareModelMap) model).get("jobView"));
    }

    @RenderMapping(params = "ctx=jobAdd")
    public String edit(Model model,
                       @RequestParam(required = false, value = "projectId" ) Long projectId ) {

        model.addAttribute("jobView", new JobView());

        model.addAttribute("managerList", resourceService.listActiveResources());
        model.addAttribute("projectList", projectService.getProjectList());
        model.addAttribute("jobTypes", projectService.getJobTypeList());
        model.addAttribute("jobStatuses", projectService.getJobStatusList());

        return "jobs/edit";
    }


    @ActionMapping("doJobSave")
    public void doSave(@RequestParam(required = false) String redirect,
            @Valid JobView jobView, BindingResult result, ActionRequest request, ActionResponse response) {

        jobViewValidator.validate(jobView, result);
        if (!result.hasErrors()) {
            Job job = new Job();
            job.setPk(jobView.getId());
            job.setAccounts(jobView.getAccounts());
            job.setCrm(jobView.getCrm());
            job.setExpectedBilling(jobView.getExpectedBilling());
            job.setJobEndDate(jobView.getJobEndDate());
            job.setJobStartDate(jobView.getJobStartDate());
            job.setJobStatus(new JobStatus(jobView.getJobStatusId()));
            job.setJobType(new JobType(jobView.getJobTypeId()));
            job.setManager(new Resource(jobView.getManagerId()));
            job.setName(jobView.getName());
            job.setNote(jobView.getDescription());
            job.setNr(jobView.getNr());
            job.setOffer(jobView.getOffer());
            job.setIsMaintenance("f");
            /*
             * The method job.setIsMaintenance sets nowhere the value true for the maintenance. But in the database a
             * new job requires the value false to prevent a bug.
             */
            job.setProject(projectService.getProject(jobView.getProjectId()));
            job.setRequest(jobView.getRequest());

            if (job.getJobType().getPk().equals(ProjectService.JOBTYPE_INTERNALJOB)) {
                job.setBillable(NON_BILLABLE);
            } else {
                job.setBillable(BILLABLE);
            }

            // TODO: this code must be removed later because the status "abgerechnet" is changed to "fertiggestellt" now
            // If Job status is charged, the complete billing date is set.
            if (job.getJobStatus().getPk().equals(ProjectService.JOBSTATUS_CHARGED)) {
                job.setPayed(new Date());
            }

            try {
                if (job.getPk() != null) {
                    // If updatePosition is true update state of dependent
                    // position
                    if (jobView.getUpdatePosition() != null && jobView.getUpdatePosition()) {
                        updatePositionStatus(job);
                    }

                    projectService.updateJob(job);                    
                    
                    setJobUpdateAsTheNextView(jobView, response);
                } else {
                    // TODO where is the createDefaultPositions flag in the
                    // layout ?
                    projectService.addJob(job);

                    response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobAdd");
                    // send mail notification that a new job was created
                    try {
                        mailService.sendJobMail(job);
                        mailService.sendConfigurableAddJobMail(job);
                    } catch (Exception e) {
                        LOGGER.error("Got an exception trying to send mail", e);
                        //TODO: mailversand noch nicht implementiert, bis dahin keine Nachrichten
                        //request.setAttribute("warningMessage", "send_email_error");
                    }
                }
                request.setAttribute("successMessage", "saved");
            } catch (UniqueValidationException e) {
                LOGGER.error("Cannot save job, unique constraint violation.");
                Object[] errorArgs = { ActivityConstants.JOB_CONSTANT, e.getConstraint().toString() };
                result.reject(UNIQUE_MESSAGE_KEY, errorArgs, "Unique constraint violated");
                setJobUpdateAsTheNextView(jobView, response);
            } 
        } else {
            //There are errors in the form
            setJobUpdateAsTheNextView(jobView, response);
        }
    }

    private void setJobUpdateAsTheNextView(JobView jobView, ActionResponse response) {
        response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobUpdate");
        if( jobView.getId() != null){
            response.setRenderParameter(ATTRIBUTE_JOB_ID, jobView.getId().toString());
        }
    }

    @ActionMapping("doJobCopy")
    public void copyJob(Model model, Long jobId, ActionRequest request, ActionResponse response) {
        Job job = projectService.getJob(jobId);
        Job newJob;
        try {
            newJob = projectService.duplicateJob(job);
            request.setAttribute("successMessage", "job_duplicated");
            request.setAttribute("messageArgs", newJob.getName());
            response.setRenderParameter(ATTRIBUTE_JOB_ID, newJob.getPk().toString());
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobUpdate");
            model.addAttribute("jobView", null);
        } catch (UniqueValidationException e) {
            String[] errorArgs = { ActivityConstants.JOB_CONSTANT, e.getConstraint().toString() };
        	request.setAttribute("errorMessage", "job_duplicated_error");
            request.setAttribute("messageArgs", errorArgs);
        	response.setRenderParameter(ATTRIBUTE_JOB_ID, job.getPk().toString());
        	response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobDetails");
            LOGGER.error("cannot duplicate job", e);
        }
    }

    @ActionMapping("doJobDelete")
    public void doDelete(@RequestParam Long jobId, ActionRequest request) {
        projectService.deleteJob(jobId);
        request.setAttribute("successMessage", "deleted");
    }

    private void updatePositionStatus(Job job) {
        List<Position> positions = projectService.getAllPositionsByJob(job.getPk());

        for (Position position : positions) {
            if (ProjectService.JOBSTATUS_ORDERED.equals(job.getJobStatus().getPk())) {
                position.setPositionStatus(new PositionStatus(ProjectService.POSITIONSTATUS_ATWORK));
            } else if (ProjectService.JOBSTATUS_FINISHED.equals(job.getJobStatus().getPk())) {
                position.setPositionStatus(new PositionStatus(ProjectService.POSITIONSTATUS_FINISHED));
            }

            LOGGER.debug("Updating status for position id:" + position.getPk() + " name:" + position.getName());
            projectService.updatePosition(position);
        }
    }
}
