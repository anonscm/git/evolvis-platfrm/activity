/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.login;

import de.tarent.activity.auth.AuthCallbackHandler;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.service.SettingsService;
import de.tarent.activity.web.domain.LoginView;
import de.tarent.activity.web.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.validation.Valid;

/**
 * Controller for login portlet. It's used to create an authentication context, to simplify development. Authentication
 * context will be used to authorize service calls. This portlet is temporary.
 */
@Controller("LoginController")
@RequestMapping(value = "view")
//@SecurityDomain(value = "ActivityDomain")
public class LoginController extends de.tarent.activity.auth.AuthInterceptor {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    @Autowired
    private SettingsService settingsService;

    /**
     * Default view for portlet.
     * 
     * @param model
     *            Model
     * @param request
     *            RenderRequest
     * @return String path for jsp file.
     * 
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        if (!"de_DE.utf8".equals(settingsService.getDBCollation())) {
            model.addAttribute("warningMessage", "db_wrong_collation");
        }

        model.addAttribute("loggeduser", RequestUtil.getLoggedUser(request));

        model.addAttribute("errorMessage", request.getAttribute("errorMessage"));
        return "login/view";
    }

    /**
     * This method performs authentication. This portlet exists temporary!
     * 
     * @param resource
     *            ResourceView
     * @param request
     *            ResourceRequest
     * @param model
     *            Model
     * @return String json with message related to login success.
     * @throws Exception
     */
    @ActionMapping("doLogin")
    public void actionForm(@ModelAttribute("loginView") @Valid LoginView resource, ActionRequest request,
            ActionResponse response) throws Exception {

        String username = resource.getUsername();
        String password = resource.getPassword();
        boolean authenticated = false;

        LoginContext lctx = new LoginContext("ActivityDomain", new AuthCallbackHandler(username, password));
        try {
            lctx.login();
            authenticated = true;
            //HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
            //httpServletRequest.login(username, password);
            //boolean result = httpServletRequest.authenticate(PortalUtil.getHttpServletResponse(response));

        } catch (LoginException e) {
            authenticated = false;
        }
        /* catch (ServletException se){
            authenticated = false;
        }*/

        if (authenticated) {
            Resource res = resourceService.loadResource(resource.getUsername());

            if (res == null) {
                RequestUtil.setLoggedUser(request, null);
                request.setAttribute("errorMessage", "login_ldap_error");
            } else {
                // put on session real password not md5 password, so we can
                // login on each request
                res.setPassword(resource.getPassword());
                RequestUtil.setLoggedUser(request, res);
                response.sendRedirect(menuPropertiesService.getMyMainPageLink());
            }
        } else {
            RequestUtil.setLoggedUser(request, null);
            request.setAttribute("errorMessage", "login_error");
        }
    }

    /**
     * @param request
     *            - ResourceRequest
     * @param model
     *            - Model
     * @return the view's name
     * @throws Exception
     */
    @ActionMapping("doLogout")
    public void doLogout(ActionRequest request) throws Exception {
        RequestUtil.setLoggedUser(request, null);
        request.setAttribute("logoutRedirectUrl", menuPropertiesService.getLoginPageLink());
    }

}
