/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.resourceallocation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.*;
import javax.validation.Valid;

import de.tarent.activity.domain.*;
import de.tarent.activity.domain.filter.PositionFilter;
import de.tarent.activity.web.controller.BaseController;
import de.tarent.activity.web.domain.PositionView;
import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.portlet.bind.PortletRequestDataBinder;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.service.MenuPropertiesService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.domain.PosResourceMappingView;
import de.tarent.activity.web.domain.ResourceMappingView;
import de.tarent.activity.web.util.DataTableResult;

/**
 * Controller class to handle actions of resource-allocation portlet.
 *
 */
@Controller("ResourceAllocationController")
@SessionAttributes(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT)
@RequestMapping(value = "view")
public class ResourceAllocationController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceAllocationController.class);

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "resource.pk");
        SORT_ALIAS.put("firstName", "resource.firstname");
        SORT_ALIAS.put("lastName", "resource.lastname");
    }

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    @Qualifier("positionResourceViewValidator")
    private Validator positionResourceViewValidator;

    @Autowired
    private MenuPropertiesService menuPropertiesService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Default render method.
     */
    @RenderMapping
    public String view(Model model, RenderRequest request) {
        return ajaxView(model, null, null, request);

    }

    @RenderMapping(params="ctx=showResourceAllocation")
    public String showResourceAllocation(Model model, RenderRequest request) {
        return ajaxView(model, null, null, request);

    }

    /**
     * Initial view of resource allocation portlet.
     *
     * @param model
     * @param projectId
     * @param jobId
     * @param request
     * @return
     */
    @ResourceMapping("view")
    public String ajaxView(Model model, String projectId, String jobId, PortletRequest request) {
        List<Resource> resources = resourceService.listActiveResources();
        List<Job> jobs = projectService.getJobList();

        PortletSession ps = request.getPortletSession();
        setUserPreferencesInSession(model, ps, "LIFERAY_SHARED_resourceAllocationRowsPerPage", "resourceAllocationRowsPerPage");

        // add lists for filter
        model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resources);
        model.addAttribute("jobsList", jobs);
        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, getAllPositionViews()); //get All positions
        return "resourceallocation/view";
    }

    private List<PositionView> getAllPositionViews(){
        List<Position> positions = getAllPositions();

        return convertPositionsListToViews(positions);
    }

    private List<PositionView> convertPositionsListToViews(List<Position> positions) {
        List<PositionView> list = new ArrayList<PositionView>();
        for (Position p : positions) {
            PositionView pv = new PositionView(p);
            list.add(pv);
        }
        return list;
    }

    private List<Position> getAllPositions() {
        PositionFilter positionFilter = new PositionFilter(null, null, null, null, null, null);
        return projectService.searchPosition(positionFilter).getRecords();
    }

    @ResourceMapping("getAllPositions")
    public String getAllPositions(Long jobId, Boolean isFilter, ResourceRequest request, Model model) {

        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, getAllPositions());
        model.addAttribute("isFilter", isFilter);

        return "common/positions";
    }


    /**
     * @param id
     *            id
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("doEdit")
    public String doEdit(Long id, Model model, PortletRequest request) {
        PosResourceMapping posResourceMapping = projectService.getPosResourceMapping(id);
        PosResourceMappingView posResourceMappingView = new PosResourceMappingView(posResourceMapping);
        model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);
        model.addAttribute("action", "edit");
        List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
        model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
        return "resourceallocation/edit";
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @RenderMapping(params = "ctx=doAssign")
    public String doAssign(Model model, PortletRequest request) {

        if(!model.containsAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT)){
            PosResourceMappingView posResourceMappingView = new PosResourceMappingView();
            model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, posResourceMappingView);
        }

        if(!model.containsAttribute("jobsList")){
            addJobListToModel(model, request);
        }

        if(!model.containsAttribute(ActivityConstants.STATUS_LIST_CONSTANT)){
            List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
            model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
        }

        if(!model.containsAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT)){
            List<Resource> resourceList = resourceService.listActiveResources();
            model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);
        }

        return "resourceallocation/edit";

    }

    private void setCommonProperties(PosResourceMappingView posResourceMappingView,
            PosResourceMapping posResourceMapping) {
        posResourceMapping.setPosResourceStatus(new PosResourceStatus(posResourceMappingView.getStatusId()));
        posResourceMapping.setStartDate(posResourceMappingView.getStartDate());
        posResourceMapping.setEndDate(posResourceMappingView.getEndDate());
        posResourceMapping.setPercent(posResourceMappingView.getPercent());
    }

    /**
     * @param posResourceMappingView
     *            posResourceMappingView
     * @param result
     *            result
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ActionMapping("doSave")
    public void doSave(
            @ModelAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT) @Valid PosResourceMappingView posResourceMappingView,
            BindingResult result, Model model, PortletRequest request, ActionResponse response) {

        positionResourceViewValidator.validate(posResourceMappingView, result);

        if (!result.hasErrors()) {
            Long posResourceMappingId = posResourceMappingView.getId();
            PosResourceMapping posResourceMapping;
            if (posResourceMappingId != null) {
                posResourceMapping = projectService.getPosResourceMapping(posResourceMappingId);
                setCommonProperties(posResourceMappingView, posResourceMapping);
                projectService.updatePosResourceMapping(posResourceMapping);
            } else {
                PosResourceMapping posResMapp = projectService.getPosResourceMappingsByPositionAndResource(
                        posResourceMappingView.getPositionId(), posResourceMappingView.getResourceId());
                if (posResMapp != null) {
                    model.addAttribute("exist", "exist");
                    model.addAttribute("action", "add");
                    addJobListToModel(model, request);
                    List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
                    model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);
                    List<Resource> resourceList = resourceService.listActiveResources();
                    model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);
                } else {
                    posResourceMapping = new PosResourceMapping();
                    posResourceMapping.setPosition(new Position(posResourceMappingView.getPositionId()));
                    posResourceMapping.setResource(new Resource(posResourceMappingView.getResourceId()));
                    setCommonProperties(posResourceMappingView, posResourceMapping);
                    projectService.addPosResourceMapping(posResourceMapping);
                }
            }

            model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, new PosResourceMappingView());

            request.setAttribute("successMessage", "saved");
        } else {
            for (ObjectError error : result.getAllErrors()) {
                LOGGER.error(error.toString());
            }
            if (posResourceMappingView.getId() != null) {
                model.addAttribute("action", "edit");

            } else {
                addJobListToModel(model, request);
                List<Resource> resourceList = resourceService.listActiveResources();
                model.addAttribute(ActivityConstants.RESOURCE_LIST_CONSTANT, resourceList);
            }

            List<PosResourceStatus> statusList = resourceService.posResourceStatusList();
            model.addAttribute(ActivityConstants.STATUS_LIST_CONSTANT, statusList);

            model.addAttribute("action", "add");
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "doAssign");
        }
    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     */
    private void addJobListToModel(Model model, PortletRequest request) {
        List<Job> jobs = projectService.getJobList();
        model.addAttribute("jobsList", jobs);
    }

    /**
     * @param projectIdId
     *            projectIdId
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("jobs")
    public String jobs(Long projectIdId, ResourceRequest request, Model model) {

        model.addAttribute("jobList", retrieveJobsList(projectIdId));
        model.addAttribute("isFilter", true);

        return "common/jobs";
    }

    /**
     * @param jobId
     *            jobId
     * @param request
     *            request
     * @param model
     *            model
     * @return String
     */
    @ResourceMapping("getPositionsByJobAndResource")
    public String positions(Long jobId, Boolean isFilter, ResourceRequest request, Model model) {

        if(isFilter.booleanValue()){
            model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, retrievePositionsList(jobId));
        }else{
            model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, getAllPositions());
        }
        model.addAttribute("isFilter", isFilter);

        return "common/positions";
    }

    /**
     * @param sort
     *            sort
     * @param dir
     *            dir
     * @param startIndex
     *            startIndex
     * @param results
     *            results
     * @param projectId
     *            projectId
     * @param jobId
     *            jobId
     * @param selectedPosition
     *            selectedPosition
     * @param model
     *            model
     * @return String
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("table")
    public String table(Long selectedResource, Long jobId, Long selectedPosition, String sort, String dir,
            Long startIndex, Long results, Model model, PortletRequest request) throws IOException {

        PortletSession ps = request.getPortletSession();
        ps.setAttribute("LIFERAY_SHARED_resourceAllocationRowsPerPage", results, PortletSession.APPLICATION_SCOPE);

        ResourceFilter filter = new ResourceFilter(startIndex, results, SORT_ALIAS.get(sort), dir);

        if (selectedResource != null) {
            filter.setResourceIds(Arrays.asList(new Long[] { selectedResource }));
            filter.setOffset(null);
            filter.setPageItems(null);
        }
        filter.setJobId(jobId);
        filter.setPositionId(selectedPosition);

        FilterResult<Resource> resources = resourceService.searchResource(filter);

        LOGGER.info("Number of resources: " + resources.getTotalRecords());
        LOGGER.info("Number of filtered resources: " + resources.getRecords().size());

        String projectLink = menuPropertiesService.getPublicProjectsPageLink();

        List<ResourceMappingView> list = new ArrayList<ResourceMappingView>();
        for (Resource r : resources.getRecords()) {
            ResourceMappingView uv = new ResourceMappingView(r);
            uv.setActiveProjects(uv.getProjects(resourceService.getResourceProjectByStatus(r.getPk(), new Long(1)),
                    projectLink));
            uv.setInactiveProjects(uv.getProjects(resourceService.getResourceProjectByStatus(r.getPk(), new Long(0)),
                    projectLink));
            uv.setPlannedProjects(uv.getProjects(resourceService.getResourceProjectByStatus(r.getPk(), new Long(2)),
                    projectLink));
            list.add(uv);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<ResourceMappingView>(
                "resource list", null, list, resources.getTotalRecords())));
        return ActivityConstants.JSON_CONSTANT;

    }

    /**
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     */
    @ResourceMapping("doCancel")
    public String doCancel(Model model, PortletRequest request) {
        model.addAttribute(ActivityConstants.POS_RESOURCE_MAPPING_VIEW_CONSTANT, new PosResourceMappingView());
        return ajaxView(model, null, null, request);
    }

    /**
     * @param id
     *            id
     * @param model
     *            model
     * @param request
     *            request
     * @return String
     * @throws IOException
     *             IOException
     */
    @ResourceMapping("doDelete")
    public String doDelete(Long id, Model model, PortletRequest request) throws IOException {
        LOGGER.info("PosResourceMapping with id " + id + " was removed");
        projectService.deletePosResourceMapping(id);
        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString("success"));
        return ActivityConstants.JSON_CONSTANT;
    }

    private List<Job> retrieveJobsList(Long projectId) {
        return projectService.getJobsByProject(projectId);
    }

    private List<Position> retrievePositionsList(Long jobId) {

        return projectService.getAllPositionsByJob(jobId);
    }

}
