/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.util.List;

import de.tarent.activity.domain.Settings;

public class SettingsView implements Serializable {

    private static final long serialVersionUID = 3287988258481453806L;

    public static final String RESOURCE_MANAGEMENT_MAIL = "resourceManagementMail";
    public static final String PERSONNEL_OFFICE_MAIL = "personnelOfficeMail";
    public static final String WELCOME_MESSAGE = "welcomeMessage";

    private String resourceManagementMail;

    private String personnelOfficeMail;

    private String welcomeMessage;

    public SettingsView() {
        super();
    }

    public SettingsView(List<Settings> settings) {

        for (Settings sett : settings) {
            if (sett.getKey().equals(RESOURCE_MANAGEMENT_MAIL)) {
                this.resourceManagementMail = sett.getValue();
            } else if (sett.getKey().equals(PERSONNEL_OFFICE_MAIL)) {
                this.personnelOfficeMail = sett.getValue();
            } else if (sett.getKey().equals(WELCOME_MESSAGE)) {
                this.welcomeMessage = sett.getValue();
            }
        }
    }

    public String getResourceManagementMail() {
        return resourceManagementMail;
    }

    public void setResourceManagementMail(String resourceManagementMail) {
        this.resourceManagementMail = resourceManagementMail;
    }

    public String getPersonnelOfficeMail() {
        return personnelOfficeMail;
    }

    public void setPersonnelOfficeMail(String personnelOfficeMail) {
        this.personnelOfficeMail = personnelOfficeMail;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

}
