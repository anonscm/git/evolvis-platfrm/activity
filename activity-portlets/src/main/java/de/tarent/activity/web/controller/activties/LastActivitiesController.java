/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.web.domain.ActivityView;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Controller handling the 'Last Benefits' action.
 * 
 */
@Controller("LastActivitiesController")
@RequestMapping(value = "view")
public class LastActivitiesController {

    @Autowired
    private ActivityService activityService;

    @RenderMapping
    public String view(Model model, RenderRequest request) {
        ActivityFilter filter = new ActivityFilter(RequestUtil.getLoggedUser(request).getPk(), null, null, null, null,
                null, null, null, "activity.date", "desc");

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        // TODO FIXME es werden alle activities geholt und dann nur 5 benutzt? bitte über filter regeln, sodass nur 5
        // geholt werden!!
        List<ActivityView> list = new ArrayList<ActivityView>();
        int i = 0;
        for (Activity activity : activities.getRecords()) {
            if (i < 5) {
                ActivityView av = new ActivityView(activity);
                list.add(av);
                i++;
            } else {
                break;
            }

        }
        model.addAttribute("myList", list);

        return "activities/lastActivities";
    }
}
