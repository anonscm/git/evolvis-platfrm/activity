/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.invoices;

import java.util.List;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Project;
import de.tarent.activity.service.CostService;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.InvoiceView;
import de.tarent.activity.web.util.ActivityConstants;

/**
 * InvoiceController.
 *
 */
@Controller(value = "InvoiceUpdateController")
@RequestMapping(value = "view", params = "ctx=invoiceUpdate")
public class InvoiceUpdateController extends BaseAjaxController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(InvoiceUpdateController.class);

	@Autowired
	private ProjectService projectService;

	@Autowired
	private CostService costService;

	@Autowired
	private MailService mailService;

	@ModelAttribute("customerList")
	public List<Customer> populateCustomerList() {
		return projectService.getCustomerList();
	}

	@ModelAttribute("projectList")
	public List<Project> populateProjectList() {
		return projectService.getProjectList();
	}

	@ModelAttribute("jobList")
	public List<Job> populateJobList(
			@RequestParam(required = false) Long invoicePk) {
		if (invoicePk != null) {
            Invoice invoice = costService.getInvoice(invoicePk);
            //FIXME: Rechnung gehört zum Job(Auftrag). Sie hat
            //scheinbar ENTWEDER ein Project ODER einen Job zugewiesen. Führt das
            //zu Problemen, beim Anlegen der Rechnung eines Jobs auch das
            //passende Projekt zuzuweisen? (oder wird sie dann 
            //vom Code als Projektrechnung verstanden?)
            if(invoice.getProject()== null){
            	return projectService.getJobsByProject(invoice.getJob().getProject().getPk());
            }
			return projectService.getJobsByProject(invoice.getProject().getPk());
		}
		return projectService.getJobList();
	}

	@ModelAttribute("invoiceView")
	public InvoiceView populateInvoiceView(
			@RequestParam(required = false) Long invoicePk) {
		if (invoicePk != null) {
			Invoice invoice = costService.getInvoice(invoicePk);
			return new InvoiceView(invoice);
		}
		return new InvoiceView();
	}

	@RenderMapping
	public String getInvoiceUpdate(Model model) {
		return "invoices/edit";
	}

    @RenderMapping(params = "mode=addInvoice")
    public String getInvoiceAdd(Model model, @RequestParam("projectId") Long projectId,
    		@RequestParam(value="jobId", required=false) Long jobId) {
        InvoiceView invoiceView = new InvoiceView();
        invoiceView.setProjectId(projectId);
        invoiceView.setJobId(jobId);

        model.addAttribute("invoiceView", invoiceView);
        return "invoices/edit";
    }

	@ActionMapping("doInvoiceSave")
	public void save(@ModelAttribute @Valid InvoiceView invoiceView,
			BindingResult result, PortletRequest request,
			ActionResponse response) {
		if (!result.hasErrors()) {
			Long projectId = invoiceView.getProjectId();
			Long jobId = invoiceView.getJobId();
			Long invoiceId = invoiceView.getId();

			Invoice invoice;

			if (invoiceId != null) {
				invoice = costService.getInvoice(invoiceId);

			} else {
				invoice = new Invoice();
			}

			invoiceView.updateInvoiceFromView(invoice);

			if (jobId != null) {
				invoice.setJob(projectService.getJob(jobId));
			} else {
				invoice.setJob(null);
			}
			invoice.setProject(projectService.getProject(projectId));

			// if InvoiceView has set an id we have to update the invoice and
			// send email
			if (invoiceId != null) {
				costService.updateInvoice(invoice);

				try {
					mailService.sendConfigurableEditJobInvoiceMail(invoice);
				} catch (Exception e) {
					LOGGER.error("Got an exception trying to send mail", e);
				}
			}
			// otherwise add a new invoice
			else {
				costService.addInvoice(invoice);

				try {
					mailService.sendConfigurableAddJobInvoiceMail(invoice);
				} catch (Exception e) {
					LOGGER.error("Got an exception trying to send mail", e);
				}
			}
            //Success: go back to the job list page and show success message
			request.setAttribute("successMessage", "saved");
            response.setRenderParameter("jobId", jobId.toString());
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "jobInvoices");
		}else{
            //Error in input: we must remain on the same page to correct it
            if(invoiceView.getJobId() != null){
                response.setRenderParameter("jobId", invoiceView.getJobId().toString());
            }
            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "invoiceUpdate");
        }

	}

	/**
	 * This method adds to the model a jobs list.
	 *
	 * @param projectId
	 *            project ID
	 * @param model
	 *            Model
	 * @return string path for jsp file
	 */
	@ResourceMapping("jobs")
	public String jobs(Long projectId, Boolean isFilter,
			ResourceRequest request, Model model) {

		model.addAttribute("jobsList",
				projectService.getJobsByProject(projectId));
		model.addAttribute("isFilter", isFilter);

		return "common/jobs";
	}
}
