/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.clients.ajax;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.CustomerView;
import de.tarent.activity.web.util.ActivityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Controller("ClientDetailController")
@RequestMapping(value = "view")
public class ClientDetailController extends BaseAjaxController {

    @Autowired
    private ProjectService projectService;

    @ModelAttribute
    public CustomerView populateCustomerView(@RequestParam Long clientPk) {
        Customer customer = projectService.getCustomer(clientPk);
        return new CustomerView(customer, true);
    }

    @RenderMapping(params = "ctx=getClientDetails")
    public String renderDetails(@ModelAttribute CustomerView customerView, Model model, RenderRequest request,
                          RenderResponse response) throws PortletModeException {

        PortletURL renderUrl = response.createRenderURL();
        renderUrl.setParameter(ActivityConstants.CONTEXT_CONSTANT, "getClientDetails");
        renderUrl.setParameter("clientPk", customerView.getId().toString());
        model.addAttribute("backToClientDetailsRenderUrl", renderUrl.toString());

        return "clients/details";
    }
    
    @RenderMapping(params = "ctx=getClientList")
       public String getClientList(@RequestParam Long clientPk,Model model, PortletRequest request) {
       		return "clients/view";
       }
    
}
