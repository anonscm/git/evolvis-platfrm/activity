/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import de.tarent.activity.domain.Resource;

/**
 * 
 * Helper class that retrieves information from PortletRequest.
 */
public final class RequestUtil {

    private static final String ACTIVITY_LOGGED_USER = "ACTIVITY_LOGGED_USER";

    public static final String ACTIVITY_ACTION_ID = "action";

    public static final String ACTIVITY_ACTION_RESOURCE = "permActRes";

    /**
     * Return the logged user.
     * 
     * @param request
     *            PortletRequest
     * @return Resource (the user of activity)
     */
    public static Resource getLoggedUser(PortletRequest request) {
        return (Resource) request.getPortletSession().getAttribute(ACTIVITY_LOGGED_USER,
                PortletSession.APPLICATION_SCOPE);
    }

    /**
     * Saves a user (Resource) in the session.
     * 
     * @param request
     *            PortletRequest
     * @param user
     *            Resource
     */
    public static void setLoggedUser(PortletRequest request, Resource user) {
        request.getPortletSession().setAttribute(ACTIVITY_LOGGED_USER, user, PortletSession.APPLICATION_SCOPE);
    }

    public static String getActionName(PortletRequest request) {
        String actionName = request.getParameter(ACTIVITY_ACTION_ID);
        if (actionName == null) {
            actionName = request.getParameter("javax.portlet.action");
        }
        if (actionName == null) {
            actionName = request.getParameter(ActivityConstants.CONTEXT_CONSTANT);
        }
        return actionName;
    }

    public static String getActionResource(PortletRequest request) {
        return request.getParameter(ACTIVITY_ACTION_RESOURCE);
    }

    // /**
    // * Workaround Liferay Bug: http://issues.liferay.com/browse/LPS-3022 .
    // *
    // * Method will be removed when migrating to Liferay 6.1 or above.
    // *
    // * @param responseURL
    // * @throws ClassNotFoundException
    // * @throws NoSuchMethodException
    // * @throws IllegalAccessException
    // * @throws InvocationTargetException
    // */
    // public static void fixResourceURL(ResourceURL responseURL)
    // throws ClassNotFoundException, NoSuchMethodException,
    // IllegalAccessException, InvocationTargetException {
    // Class<?> clazz = Class.forName("com.liferay.portlet.PortletURLImpl",
    // true, PortalClassLoaderUtil.getClassLoader());
    //
    // // responseURL.setCopyCurrentRenderParameters(false);
    // Method setCopyCurrentRenderParametersMethod = clazz.getMethod(
    // "setCopyCurrentRenderParameters",
    // new Class<?>[] { Boolean.TYPE });
    // setCopyCurrentRenderParametersMethod.invoke(responseURL,
    // new Object[] { false });
    // }

    /**
     * Private constructor: Utility classes should not have a public or default constructor.
     * 
     */
    private RequestUtil() {
        // does nothing
    }

}
