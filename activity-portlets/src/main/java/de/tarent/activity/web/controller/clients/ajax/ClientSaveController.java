/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.clients.ajax;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.validation.Valid;

import de.tarent.activity.web.util.ActivityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import java.util.List;
import de.tarent.activity.domain.Customer;
import de.tarent.activity.service.MailService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.web.controller.BaseAjaxController;
import de.tarent.activity.web.domain.CustomerView;

@Controller("ClientSaveController")
@RequestMapping(value = "view")
public class ClientSaveController extends BaseAjaxController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAddController.class);

    @Autowired
    private MailService mailService;

    @Autowired
    private ProjectService projectService;

    @ActionMapping("doClientSave")
    public void doClientSave(@ModelAttribute @Valid CustomerView customerView, BindingResult result, 
    		ActionRequest request, ActionResponse response) {
        if (!result.hasErrors()) {
            saveOrUpdateClient(customerView);
            request.setAttribute("successMessage", "saved");
        }
    }

    @ActionMapping("getClientSave")
    public void getClientSave(@ModelAttribute @Valid CustomerView customerView, BindingResult result, 
    		ActionRequest request, ActionResponse response) {
    	if(!customerIsUnique(customerView)){
    		request.setAttribute("errorMessage", "validate_unique_constraint");
    		response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getClientAdd");
    	}else{
	        if (!result.hasErrors()) {
	            saveOrUpdateClient(customerView);
	            request.setAttribute("successMessage", "saved");
	        }
	        if(customerView.getId() != null){
	            response.setRenderParameter("clientPk", customerView.getId().toString());
	            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getClientEdit");
	        }else{
	            response.setRenderParameter(ActivityConstants.CONTEXT_CONSTANT, "getClientAdd");
	        }
    	}
    }

    private boolean customerIsUnique(CustomerView customerView){
    	List<Customer> customer = projectService.getCustomerList();
    	for(Customer cust : customer){
    		if(cust.getName().trim().equalsIgnoreCase(customerView.getName().trim())){
    			if(customerView.getId() == null){
    				return false;
    			} else if(!cust.getPk().equals(customerView.getId())){
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
    @ActionMapping("doClientDelete")
    public void doClientDelete(@RequestParam(value = "clientPk", required = true) Long clientId, 
    		ActionRequest request, ActionResponse response) {
        projectService.deleteCustomer( clientId );
        request.setAttribute("successMessage", "deleted");
    }


    private void saveOrUpdateClient(CustomerView customerView){
    	Customer customer;
        if (customerView.getId() != null) {
            customer = projectService.getCustomer(customerView.getId());
            customer = customerView.getUpatedCustomerFromView(customer);
            projectService.updateCustomer(customer);
        } else {
            customer = customerView.getNewCustomerFromView();
            projectService.addCustomer(customer);

            // send mail notification that a new customer was added
            try {
                mailService.sendCustomerMail(customer);
                mailService.sendConfigurableAddCustomerMail(customer);
            } catch (Exception e) {
                LOGGER.error("Got an exception trying to send mail", e);
            }
        }
    }
}
