/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.util;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;

/**
 * {@link PropertyEditorRegistrar} for property editors.
 *
 * @author T. Kudla <t.kudla@tarent.de>, tarent solutions GmbH
 *
 */
public class CustomPropertyEditorRegistrar implements PropertyEditorRegistrar {

	@Override
	public void registerCustomEditors(PropertyEditorRegistry registry) {
		registry.registerCustomEditor(BigDecimal.class,
				new CustomBigDecimalPropertyEditor());
		registry.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * Custom property editor class to parse value to big decimals, such that
	 * decimal separator can either be '.' or ','. Additionally, a big decimal
	 * will be formated given the pattern '##0.00', e.g. '-1234.45', i.e. with
	 * two fractional digits. This property editor should be registered
	 * globally.
	 */
	private static final class CustomBigDecimalPropertyEditor extends
			PropertyEditorSupport {

		private static final String BIG_DECIMAL_FORMAT_PATTERN = "##0.00";

		private DecimalFormat df;

		public CustomBigDecimalPropertyEditor() {
			final NumberFormat nf = NumberFormat.getNumberInstance();
			df = (DecimalFormat) nf;
			df.applyPattern(BIG_DECIMAL_FORMAT_PATTERN);
		}

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			BigDecimal parsedValue = null;
			
			if(text != null && !text.trim().isEmpty()){
				parsedValue = new BigDecimal(
					text.replace(',', '.'), MathContext.DECIMAL64);
			}
			setValue(parsedValue);
		}

		@Override
		public String getAsText() {
			if(getValue() == null){
				return null;
			}
			final BigDecimal valueToFormat = (BigDecimal) getValue();
			return df.format(valueToFormat);
		}


	}

}
