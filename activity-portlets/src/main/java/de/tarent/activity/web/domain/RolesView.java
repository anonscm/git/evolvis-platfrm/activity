/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.util.List;

import de.tarent.activity.domain.Role;

/**
 * Class RolesView acts as a command object and View representation for {@link Role}'s.
 * 
 */
public class RolesView implements Serializable {

    private static final long serialVersionUID = 611821055490417452L;

    private Long resourceId;

    /**
     * List of all existing roles.
     */
    private List<RoleView> allRoles;

    /**
     * List of all roles that are related to a specific user. Only useful when in view with a selected resource.
     */
    private List<RoleView> userRoles;

    /**
     * List of all roles that are not related to a specific user. Only useful when in view with a selected resource.
     */
    private List<RoleView> allRolesExceptUserRoles;

    public RolesView() {
    }

    public RolesView(List<RoleView> allRoles, List<RoleView> userRoles, List<RoleView> allRolesExceptUserRoles) {
        this.allRoles = allRoles;
        this.userRoles = userRoles;
        this.allRolesExceptUserRoles = allRolesExceptUserRoles;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public List<RoleView> getAllRoles() {
        return allRoles;
    }

    public void setAllRoles(List<RoleView> allRoles) {
        this.allRoles = allRoles;
    }

    public List<RoleView> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<RoleView> userRoles) {
        this.userRoles = userRoles;
    }

    public List<RoleView> getAllRolesExceptUserRoles() {
        return allRolesExceptUserRoles;
    }

    public void setAllRolesExceptUserRoles(List<RoleView> allRolesExceptUserRoles) {
        this.allRolesExceptUserRoles = allRolesExceptUserRoles;
    }

}
