/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.tarent.activity.web.util.ActivityConstants;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.service.CostService;
import de.tarent.activity.web.domain.CostView;
import de.tarent.activity.web.util.DataTableResult;

public abstract class BaseCostController extends BaseController {

    @Autowired
    protected CostService costService;

    @Autowired
    protected ObjectMapper objectMapper;

    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("idTable", "cost.pk");
        SORT_ALIAS.put("jobName", "job.name");
        SORT_ALIAS.put("name", "cost.name");
        SORT_ALIAS.put("description", "cost.note");
    }

    protected void createDataTableResult(Long projectId, Long jobId, Long startIndex, Long results,
            String sort, String dir, Model model) throws IOException, JsonGenerationException, JsonMappingException {
        CostFilter costFilter = new CostFilter(projectId, jobId, startIndex, results, SORT_ALIAS.get(sort),
                dir);

        FilterResult<Cost> result = costService.searchCost(costFilter);

        ArrayList<CostView> list = new ArrayList<CostView>();
        for (Cost cost : result.getRecords()) {
            CostView costView = new CostView(cost);
            list.add(costView);
        }

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<CostView>("cost list", null,
                list, result.getTotalRecords())));
    }
}
