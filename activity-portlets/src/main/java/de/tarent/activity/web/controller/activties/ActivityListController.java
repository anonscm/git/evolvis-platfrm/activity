/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller.activties;

import de.tarent.activity.domain.Activity;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Position;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.ActivityFilter;
import de.tarent.activity.domain.filter.BaseFilter;
import de.tarent.activity.domain.filter.FilterResult;
import de.tarent.activity.exception.UniqueValidationException;
import de.tarent.activity.service.ActivityService;
import de.tarent.activity.service.ProjectService;
import de.tarent.activity.service.ResourceService;
import de.tarent.activity.web.controller.BaseActivityController;
import de.tarent.activity.web.domain.ActivityView;
import de.tarent.activity.web.util.ActivityConstants;
import de.tarent.activity.web.util.DataTableResult;
import de.tarent.activity.web.util.RequestUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * ActivityListController provides request mappings for either render a complete activity-list view or handle ajax
 * requests that are related to
 *
 */
@Controller("ActivityListController")
@RequestMapping(value = "view")
public class ActivityListController extends BaseActivityController {
	
	private static final String PAGING_CONFIG = "pagingConfigActivities";
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityListController.class);
    private static final String UNIQUE_MESSAGE_KEY = "validate_unique_constraint";
	
    @Autowired
    private ActivityService activityService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Sort alias map. Used to map between java objects and YUI table.
     */
    private static final Map<String, String> SORT_ALIAS = new HashMap<String, String>();
    static {
        SORT_ALIAS.put("id", "activity.pk");
        SORT_ALIAS.put("jobName", "activity.position.job.name");
        SORT_ALIAS.put("positionName", "activity.position.name");
        SORT_ALIAS.put("resourceName", "activity.resource.username");
        SORT_ALIAS.put("description", "activity.name");
        SORT_ALIAS.put("hours", "activity.hours");
        SORT_ALIAS.put("date", "activity.date");
        SORT_ALIAS.put("evolvisTaskId", "activity.evolvisTaskId");
    }

   /**
     * Default render view that shows either the view for all activities or the view for my activities dependent on what
     * is set by {@link ActivityListController#setAllActivities(Boolean)}.
     */
    @RenderMapping
    public String defaultView(Model model, RenderRequest request,
    		@RequestParam(required = false) Long activityPk) {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        return defaultAjaxView(model, request, activityPk);
    }

    @RenderMapping(params = "ctx=doCancel")
    public String doCancel(Model model, RenderRequest request,
    		@RequestParam(required = false) Long activityPk) {
    	setSessionPagingConfig(model, request, PAGING_CONFIG);
        return defaultAjaxView(model, request, activityPk);
    }

    @ResourceMapping("getActivityListView")
    public String defaultAjaxView(Model model, PortletRequest request,
    		@RequestParam(required = false) Long activityPk) {

        setSessionPagingConfig(model, request, PAGING_CONFIG);
        ActivityView activityView = new ActivityView();
        boolean isNewActivity = false;
        if(!model.containsAttribute("activityView")){
        	isNewActivity = true;
        }
        Long jobId = null;
        
        // Vorselektion der Selectboxen / Ermittlung der editActivityPositionsList
        if(!isNewActivity){
        	if(request.getParameter("editJobId") != null)
        		jobId = Long.valueOf(request.getParameter("editJobId"));
        	if(activityPk == null){
	        	if(jobId != null){
	        		activityView.setJobId(jobId);
	        	}
	        	if(request.getParameter("editPositionId") != null){
	        		activityView.setPositionId(Long.valueOf(request.getParameter("editPositionId")));
	        	}
	        	
	        	//Setze activityView mit Vorseletionen nur, wenn vorher erfolgreich gespeichert wurde
	        	if(request.getParameter("hasErrors") != null && !(Boolean.parseBoolean(request.getParameter("hasErrors")))){
		        	model.addAttribute("activityView", activityView);
	        	}
        	}
        } // Leistung editieren 
        else if(activityPk != null){
        	activityView = new ActivityView(activityService.getActivity(activityPk));
        	jobId = activityView.getJobId();
        	model.addAttribute("activityView", new ActivityView(activityService.getActivity(activityPk)));
        } // neue Leistung 
        else {
        	model.addAttribute("activityView", activityView);
        }
        
        Long resPk = RequestUtil.getLoggedUser(request).getPk();
        Long activityResPk = activityView.getResourceId() != null ? activityView.getResourceId() : resPk;
        
        //Selects für Leistung neu/editieren ermitteln
        model.addAttribute("editActivityJobsList", projectService.getActiveJobsByResource(activityResPk));
        if(jobId != null){
        	model.addAttribute("editActivityPositionsList", projectService.getPositionsByJob(jobId, 
				null, activityResPk));
        } else{
        	model.addAttribute("editActivityPositionsList", null);
        }
        
		//Selects für Sicht der nicht eigenen Leistungen
        if (allActivities) {
            model.addAttribute("projectList", projectService.getProjectList());
            model.addAttribute("resourcesList", resourceService.listActiveResources());

            return "activities/view";
        }

        //Selects für Übersicht
        List<Job> jobsList = projectService.getActiveJobsByResource(resPk);
        model.addAttribute("jobsList", jobsList);
        model.addAttribute(ActivityConstants.POSITIONS_LIST_CONSTANT, null);
        return "activities/viewMyActivities";
    }

    @ResourceMapping("getCancelDeleteRequest")
    public String doCancelDeleteRequest(Model model, PortletRequest request,
    		@RequestParam(required = false) Long activityPk) {
        return defaultAjaxView(model, request, activityPk);
    }

    @ResourceMapping("getActivityTable")
    public String getActivityTable(String sort, String dir, Long offset, Long limit, Long projectId,
            Long selectedPosition, Long jobId, Long selectedResource, Date startdate, Date enddate, Model model, PortletRequest request)
            throws IOException {

        //Save the actual user preferences of rowCount in PortletSession
    	PortletSession ps = request.getPortletSession();
        BaseFilter pagingConfig = new BaseFilter(offset, limit, sort, dir);
        ps.setAttribute(PAGING_CONFIG, pagingConfig, PortletSession.APPLICATION_SCOPE);

        if (!allActivities) {
            // my activities
            selectedResource = RequestUtil.getLoggedUser(request).getPk();
        }
        
        ActivityFilter filter = new ActivityFilter(selectedResource, jobId, selectedPosition, startdate, enddate,
                projectId, offset, limit, SORT_ALIAS.get(sort), dir);

        FilterResult<Activity> activities = activityService.searchActivities(filter);

        List<ActivityView> list = new ArrayList<ActivityView>();
        for (Activity a : activities.getRecords()) {
            ActivityView av = new ActivityView(a);
            list.add(av);
        }

        BigDecimal sum = activityService.getActivitiesHoursSum(filter);

        model.addAttribute(ActivityConstants.JSON_CONSTANT, objectMapper.writeValueAsString(new DataTableResult<ActivityView>("activity list",
                sum, list, activities.getTotalRecords())));

        return ActivityConstants.JSON_CONSTANT;
    }
    

    @ActionMapping("doActivitySave")
    public void doActivitySave(@ModelAttribute @Valid ActivityView activityView, BindingResult result,
            @RequestParam String redirect, @RequestParam(required = false) Long activityPk, ActionRequest request, 
            ActionResponse response, Model model) {

    	boolean newActivity = activityView.getId() == null ? true : false;
    	Boolean hasErrors = result.hasErrors();
    	response.setRenderParameter("hasErrors", hasErrors.toString());
    	Activity activity = new Activity();
    	
        if (!hasErrors) {
        	
        	Resource user = resourceService.getResource(RequestUtil.getLoggedUser(request).getPk(), false, false);
        	
        	if(newActivity){
        		activity.setResource(user);
        	}
        	else{
        		activity = activityService.getActivity(activityView.getId());
        	}
            activity.setHours(activityView.getHours());
            activity.setName(activityView.getDescription());
            activity.setPosition(new Position(activityView.getPositionId()));
            activity.setDate(activityView.getDate());
            try{
            	activity.setEvolvisTaskId(Long.valueOf(activityView.getEvolvisTaskId()));
            } catch (NumberFormatException e){
            	// wurde schon in validierung ausgeschlossen
            }
            
            if (activity.getResource() != null && activity.getResource().getCostPerHour() != null) {
                activity.setCostPerHour(activity.getResource().getCostPerHour());
            }
            try {
            	if(newActivity){
            		activityService.addActivity(activity);
            	} else {
            		activityService.updateActivity(activity);
            	}
                request.setAttribute("successMessage", "saved");
                
            } catch (UniqueValidationException e) {
                LOGGER.error("Cannot save activity, unique constraint violation.");
                Object[] errorArgs = { "activity", e.getConstraint().toString() };
                result.reject(UNIQUE_MESSAGE_KEY, errorArgs, "Unique constraint violated");
                request.setAttribute("errorMessage", "validate_unique_constraint");
            }
        }else{
            if(!newActivity){
            	response.setRenderParameter("activityPk", activityView.getId().toString());
            }
        }
        
        //Auftrag u. Position bleiben für den nächsten Eintrag vorselektiert
        if(activityView.getJobId()!= null){
        response.setRenderParameter("editJobId", activityView.getJobId().toString());
        }
        if(activityView.getPositionId()!= null){
        response.setRenderParameter("editPositionId", activityView.getPositionId().toString());
        }
    }


    @ActionMapping("doActivityDelete")
    public void doActivityDelete(PortletRequest request, Model model, @RequestParam Long activityPk) {
        activityService.deleteActivity(activityPk);
        model.addAttribute("successMessage", "deleted");

    }
}
