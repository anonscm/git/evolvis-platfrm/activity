/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.portlet.RenderResponse;
import javax.portlet.ResourceResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * By extending BaseAjaxController you ensure that your request mapping always require a request parameter
 * <code>backUrl</code> and passes it to model. This is useful for views that provide a back button to go back to last
 * view.
 * 
 * The <code>backUrl</code> should be a ResourceUrl and is best created with a {@link ResourceResponse} or
 * {@link RenderResponse} within appropriate request method.
 * 
 */
public abstract class BaseAjaxController extends BaseController {

    /**
     * Passes given <code>backUrl</code> to model.
     * 
     * @param backUrl
     *            encoded URL string
     * @return decoded URL string
     */
    @ModelAttribute("backUrl")
    public String populateBackButtonUrl(@RequestParam(required = false) String backUrl) {
       if(backUrl != null){
           if( backUrl.endsWith(",")){
               backUrl = backUrl.replace(',', ' ');
           }
    		try {
    			return URLDecoder.decode(backUrl, "UTF-8");
    		} catch (UnsupportedEncodingException e) {
    			// will never occur
    		}
    	}
        return null;
    }
}
