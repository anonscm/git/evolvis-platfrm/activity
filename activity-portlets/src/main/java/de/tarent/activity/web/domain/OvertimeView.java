/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import de.tarent.activity.domain.Overtime;
import de.tarent.activity.validator.constraints.ActivityHours;

/**
 * Helper class, acting as a proxy for resource.
 *
 */
public class OvertimeView implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long projectId;

    private String projectName;

    @NotNull(message = "{validate_resource_not_empty}")
    private Long resourceId;

    private String resourceName;

    @NotNull(message = "{validate_type_not_empty}")
    private Long type;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    @NotNull(message = "{validate_hours_not_empty}")
    @NumberFormat
    @Range(min = 0, max = 24, message = "{validate_overtime}")
    @ActivityHours
    private BigDecimal hours;

    @DateTimeFormat
    @NotNull(message = "{validate_date_not_empty}")
    private Date date;

    /**
     * Default and null constructor.
     */
    public OvertimeView() {
    }

    /**
     * Constructor using fields.
     *
     * @param id
     *            - overtime id
     * @param projectId
     *            - project id
     * @param projectName
     *            - project name
     * @param resourceId
     *            - resource id
     * @param resourceName
     *            - resource name
     * @param type
     *            - overtime type
     * @param description
     *            - overtime description
     * @param hours
     *            - overtime hours
     * @param date
     *            - overtime date
     */
    public OvertimeView(Long id, Long projectId, String projectName, Long resourceId, String resourceName, Long type,
            String description, BigDecimal hours, Date date) {
        super();
        this.id = id;
        this.projectId = projectId;
        this.projectName = projectName;
        this.resourceId = resourceId;
        this.resourceName = resourceName;
        this.type = type;
        this.description = description;
        this.hours = hours;
        this.date = date;
    }

    /**
     * Constructor using an Overtime object.
     *
     * @param o
     *            Overtime
     */
    public OvertimeView(Overtime o) {
        this(o.getPk(), (o.getProject() == null) ? 0L : o.getProject().getPk(), (o.getProject() == null) ? "" : o
                .getProject().getName(), (o.getResource() == null) ? 0L : o.getResource().getPk(),
                (o.getResource() == null) ? "" : o.getResource().getFirstname() + " " + o.getResource().getLastname(),
                (o.getHours().compareTo(BigDecimal.ZERO) > 0) ? 1L : -1L, o.getNote(), o.getHours(), o.getDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

}
