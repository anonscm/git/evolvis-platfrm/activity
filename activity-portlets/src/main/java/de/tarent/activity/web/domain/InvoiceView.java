/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.web.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Project;
import de.tarent.activity.web.util.EscapeDescriptionUtil;

public class InvoiceView {

    @NotNull(message = "{validate_amount_not_empty}")
    @NumberFormat(style = Style.CURRENCY)
    private BigDecimal amount;

    private String customerName;

    @Size(max = 4000, message = "{validate_description_length}")
    private String description;

    private Long id;
    private Long idTable;

    @DateTimeFormat
    private Date invoiceDate;

    private Long jobId;

    private String jobName;

    @NotEmpty(message = "{validate_name_not_empty}")
    @Size(max = 255)
    private String name;

    private String number;

    @DateTimeFormat
    private Date payDate;

    @NotNull(message = "{validate_project_not_empty}")
    private Long projectId;

    private String projectName;

    public InvoiceView() {
    }

    public InvoiceView(Invoice invoice) {
        this(invoice.getPk(), invoice.getJob() == null ? null : invoice.getJob().getPk(), invoice.getJob() == null ? ""
                : invoice.getJob().getName(), invoice.getName(), invoice.getNote(), invoice.getNumber(), invoice
                .getAmount(), invoice.getInvoiced(), invoice.getPayed());

        Project project = invoice.getProject() != null ? invoice.getProject() : (invoice.getJob() != null ? invoice
                .getJob().getProject() : new Project());
        this.projectName = project.getName();
        this.customerName = project.getCustomer().getName();
        this.projectId = project.getPk();

        this.jobName = invoice.getJob() == null ? null : invoice.getJob().getName();
        this.jobId = invoice.getJob() == null ? null : invoice.getJob().getPk();
        if(this.description != null){
            setDescription(EscapeDescriptionUtil.escapeDescriptionWithoutBr(getDescription()));
        }
    }

    public InvoiceView(Long id, Long jobId, String jobName, String name, String description, String number,
            BigDecimal amount, Date invoiceDate, Date payDate) {
        super();
        this.id = id;
        this.idTable = id;
        this.jobId = jobId;
        this.jobName = jobName;
        this.name = name;
        this.description = description;
        this.number = number;
        this.amount = amount;
        this.invoiceDate = invoiceDate;
        this.payDate = payDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public Long getIdTable() {
        return idTable;
    }

    public void updateInvoiceFromView(Invoice invoice) {
        invoice.setAmount(this.amount);
        invoice.setInvoiced(this.invoiceDate);
        invoice.setName(this.name);
        invoice.setNote(this.description);
        invoice.setNumber(this.number);
        invoice.setPayed(this.payDate);
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public Long getJobId() {
        return jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Date getPayDate() {
        return payDate;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdTable(Long idTable) {
        this.idTable = idTable;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
