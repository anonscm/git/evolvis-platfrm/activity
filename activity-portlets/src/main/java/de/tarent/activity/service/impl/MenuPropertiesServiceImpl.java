/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service.impl;

import de.tarent.activity.service.MenuPropertiesService;

public class MenuPropertiesServiceImpl implements MenuPropertiesService {

    private String myMainPageLink;
    private String myActivitiesPageLink;
    private String myOvertimesPageLink;
    private String myHolidayPageLink;
    private String myPositionsPageLink;
    private String mySkillsPageLink;
    private String myRequestsPageLink;

    private String myActiveProjectsPageLink;

    private String customersPageLink;
    private String projectsPageLink;
    private String jobsPageLink;
    private String positionsPageLink;
    private String resourceAllocationPageLink;
    private String skillsPageLink;

    private String deleteRequestsPageLink;
    private String activitiesPageLink;
    private String invoicesPageLink;
    private String holidayPageLink;
    private String overtimesPageLink;

    private String resourcesPageLink;
    private String ldapImportPageLink;
    private String permissionsPageLink;

    private String userInfoPageLink;

    private String loginPageLink;

    private String publicProjectsPageLink;
    private String publicPositionsPageLink;
    private String publicActivitiesPageLink;
    private String publicJobsPageLink;
    private String publicCustomersPageLink;
    private String holidayResponsesPageLink;

    @Override
    public String getMyMainPageLink() {
        return myMainPageLink;
    }

    public void setMyMainPageLink(String myMainPageLink) {
        this.myMainPageLink = myMainPageLink;
    }

    @Override
    public String getMyActivitiesPageLink() {
        return myActivitiesPageLink;
    }

    public void setMyActivitiesPageLink(String myActivitiesPageLink) {
        this.myActivitiesPageLink = myActivitiesPageLink;
    }

    @Override
    public String getMyOvertimesPageLink() {
        return myOvertimesPageLink;
    }

    public void setMyOvertimesPageLink(String myOvertimesPageLink) {
        this.myOvertimesPageLink = myOvertimesPageLink;
    }

    @Override
    public String getMyHolidayPageLink() {
        return myHolidayPageLink;
    }

    public void setMyHolidayPageLink(String myHolidayPageLink) {
        this.myHolidayPageLink = myHolidayPageLink;
    }

    @Override
    public String getMyPositionsPageLink() {
        return myPositionsPageLink;
    }

    public void setMyPositionsPageLink(String myPositionsPageLink) {
        this.myPositionsPageLink = myPositionsPageLink;
    }

    @Override
    public String getMySkillsPageLink() {
        return mySkillsPageLink;
    }

    public void setMySkillsPageLink(String mySkillsPageLink) {
        this.mySkillsPageLink = mySkillsPageLink;
    }

    @Override
    public String getMyRequestsPageLink() {
        return myRequestsPageLink;
    }

    public void setMyRequestsPageLink(String myRequestsPageLink) {
        this.myRequestsPageLink = myRequestsPageLink;
    }

    @Override
    public String getCustomersPageLink() {
        return customersPageLink;
    }

    public void setCustomersPageLink(String customersPageLink) {
        this.customersPageLink = customersPageLink;
    }

    @Override
    public String getProjectsPageLink() {
        return projectsPageLink;
    }

    public void setProjectsPageLink(String projectsPageLink) {
        this.projectsPageLink = projectsPageLink;
    }

    @Override
    public String getJobsPageLink() {
        return jobsPageLink;
    }

    public void setJobsPageLink(String jobsPageLink) {
        this.jobsPageLink = jobsPageLink;
    }

    @Override
    public String getPositionsPageLink() {
        return positionsPageLink;
    }

    public void setPositionsPageLink(String positionsPageLink) {
        this.positionsPageLink = positionsPageLink;
    }

    @Override
    public String getResourcesPageLink() {
        return resourcesPageLink;
    }

    public void setResourcesPageLink(String resourcesPageLink) {
        this.resourcesPageLink = resourcesPageLink;
    }

    @Override
    public String getResourceAllocationPageLink() {
        return resourceAllocationPageLink;
    }

    public void setResourceAllocationPageLink(String resourceAllocationPageLink) {
        this.resourceAllocationPageLink = resourceAllocationPageLink;
    }

    @Override
    public String getSkillsPageLink() {
        return skillsPageLink;
    }

    public void setSkillsPageLink(String skillsPageLink) {
        this.skillsPageLink = skillsPageLink;
    }

    @Override
    public String getDeleteRequestsPageLink() {
        return deleteRequestsPageLink;
    }

    public void setDeleteRequestsPageLink(String deleteRequestsPageLink) {
        this.deleteRequestsPageLink = deleteRequestsPageLink;
    }

    @Override
    public String getActivitiesPageLink() {
        return activitiesPageLink;
    }

    public void setActivitiesPageLink(String activitiesPageLink) {
        this.activitiesPageLink = activitiesPageLink;
    }

    @Override
    public String getInvoicesPageLink() {
        return invoicesPageLink;
    }

    public void setInvoicesPageLink(String invoicesPageLink) {
        this.invoicesPageLink = invoicesPageLink;
    }

    @Override
    public String getHolidayPageLink() {
        return holidayPageLink;
    }

    public void setHolidayPageLink(String holidayPageLink) {
        this.holidayPageLink = holidayPageLink;
    }

    @Override
    public String getOvertimesPageLink() {
        return overtimesPageLink;
    }

    public void setOvertimesPageLink(String overtimesPageLink) {
        this.overtimesPageLink = overtimesPageLink;
    }

    @Override
    public String getLdapImportPageLink() {
        return this.ldapImportPageLink;
    }

    public void setLdapImportPageLink(String ldapImportPageLink) {
        this.ldapImportPageLink = ldapImportPageLink;
    }

    @Override
    public String getPermissionsPageLink() {
        return permissionsPageLink;
    }

    public void setPermissionsPageLink(String permissionsPageLink) {
        this.permissionsPageLink = permissionsPageLink;
    }

    @Override
    public String getUserInfoPageLink() {
        return userInfoPageLink;
    }

    public void setUserInfoPageLink(String userInfoPageLink) {
        this.userInfoPageLink = userInfoPageLink;
    }

    @Override
    public String getLoginPageLink() {
        return loginPageLink;
    }

    public void setLoginPageLink(String loginPageLink) {
        this.loginPageLink = loginPageLink;
    }

    @Override
    public String getPublicProjectsPageLink() {
        return publicProjectsPageLink;
    }

    public void setPublicProjectsPageLink(String publicProjectsPageLink) {
        this.publicProjectsPageLink = publicProjectsPageLink;
    }

    @Override
    public String getPublicPositionsPageLink() {
        return publicPositionsPageLink;
    }

    public void setPublicPositionsPageLink(String publicPositionsPageLink) {
        this.publicPositionsPageLink = publicPositionsPageLink;
    }

    @Override
    public String getPublicActivitiesPageLink() {
        return publicActivitiesPageLink;
    }

    public void setPublicActivitiesPageLink(String publicActivitiesPageLink) {
        this.publicActivitiesPageLink = publicActivitiesPageLink;
    }

    @Override
    public String getPublicJobsPageLink() {
        return publicJobsPageLink;
    }

    public void setPublicJobsPageLink(String publicJobsPageLink) {
        this.publicJobsPageLink = publicJobsPageLink;
    }

    @Override
    public String getPublicCustomersPageLink() {
        return publicCustomersPageLink;
    }

    @Override
    public String getHolidayResponsesPageLink() {
        return holidayResponsesPageLink;
    }

    public void setPublicCustomersPageLink(String publicCustomersPageLink) {
        this.publicCustomersPageLink = publicCustomersPageLink;
    }

    public String getMyActiveProjectsPageLink() {
        return myActiveProjectsPageLink;
    }

    public void setMyActiveProjectsPageLink(String myActiveProjectsPageLink) {
        this.myActiveProjectsPageLink = myActiveProjectsPageLink;
    }

    public void setHolidayResponsesPageLink(String holidayResponsesPageLink) {
        this.holidayResponsesPageLink = holidayResponsesPageLink;
    }

}
