/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.auth;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletSession;
import javax.security.auth.login.LoginContext;

import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.exception.LoginException;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Authentication interceptor.
 * 
 */
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static final String ACTIVITY_LOGIN_CONTEXT = "ACTIVITY_LOGIN_CONTEXT";

    @Override
    protected boolean preHandle(PortletRequest request, PortletResponse response, Object handler) throws Exception {

        Resource loggedUser = RequestUtil.getLoggedUser(request);

        if (loggedUser == null) {
            throw new LoginException("User not logged in.");
        }
        LoginContext lctx = getLoginContext(request);

        if (lctx != null) {
            lctx.logout();
        }

        lctx = new LoginContext("ActivityDomain", new AuthCallbackHandler(loggedUser.getUsername(), loggedUser.getPassword()));
        lctx.login();
        lctx.getSubject();

        setLoginContext(request, lctx);

        return true;
    }

    @Override
    protected void afterCompletion(PortletRequest request, PortletResponse response, Object handler, Exception ex)
            throws Exception {

        LoginContext lctx = getLoginContext(request);

        if (lctx != null) {
            lctx.logout();
            setLoginContext(request, null);
        }
    }

    /**
     * Saves the user login context in the session.
     * 
     * @param request
     *            PortletRequest
     * @param lctx
     *            LoginContext
     */
    private static void setLoginContext(PortletRequest request, LoginContext lctx) {
        request.getPortletSession().setAttribute(ACTIVITY_LOGIN_CONTEXT, lctx, PortletSession.APPLICATION_SCOPE);
    }

    /**
     * Return the logged user login context.
     * 
     * @param request
     *            PortletRequest
     * @return LoginContext
     */
    private static LoginContext getLoginContext(PortletRequest request) {
        return (LoginContext) request.getPortletSession().getAttribute(ACTIVITY_LOGIN_CONTEXT,
                PortletSession.APPLICATION_SCOPE);
    }
}
