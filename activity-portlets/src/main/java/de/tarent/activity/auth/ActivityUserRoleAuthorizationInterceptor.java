/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.auth;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletSecurityException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

import de.tarent.activity.domain.Resource;
import de.tarent.activity.exception.PermissionException;
import de.tarent.activity.service.PermissionService;
import de.tarent.activity.web.util.RequestUtil;

/**
 * Interceptor for Authorization.
 *
 */
public class ActivityUserRoleAuthorizationInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityUserRoleAuthorizationInterceptor.class);

    private static final String DEFAULT_ACTION = "defaultRender";

    private Map<String, List<String>> permissionMapping;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void checkPermissionMapping() {
        if (permissionMapping == null) {
            throw new IllegalArgumentException(
                    "You have not declared the permissionMapping property for ActivityUserRoleAuthorizationInterceptor within \""
                            + context.getDisplayName() + "\"");
        }
    }

    /**
     * Handle a request that is not authorized according to this interceptor. Default implementation throws a new
     * PortletSecurityException.
     * <p>
     * This method can be overridden to write a custom message, forward or redirect to some error page or login page, or
     * throw a PortletException.
     *
     * @param request
     *            current portlet request
     * @param response
     *            current portlet response
     * @param handler
     *            chosen handler to execute, for type and/or instance evaluation
     * @throws javax.portlet.PortletSecurityException
     *             if there is an internal error
     */
    protected void handleNotAuthorized(PortletRequest request, PortletResponse response, Object handler)
            throws PortletSecurityException {

        throw new PortletSecurityException("Logged user does not have required permissions");
    }

    @Override
    public boolean preHandleRender(RenderRequest request, RenderResponse response, Object handler) throws Exception {
        // check if map with permission <> action mapping is set by spring

        Resource loggedUser = RequestUtil.getLoggedUser(request);

        String renderAction = RequestUtil.getActionName(request);
        renderAction = renderAction != null ? renderAction : DEFAULT_ACTION;

        List<String> permissionIds = permissionMapping.get(renderAction);

        if (permissionIds != null) {
            permissionService.checkAndThrowPermission(loggedUser.getPk(), permissionIds.toArray(new String[]{}));
        } else {
            LOGGER.warn("No permissions set for render method: " + renderAction);
            throw new PermissionException("User '" + loggedUser.getPk() + "' does not have the permission '" + renderAction + "'");
        }

        return true;
    }

    @Override
    public boolean preHandleAction(ActionRequest request, ActionResponse response, Object handler) throws Exception {
        Resource loggedUser = RequestUtil.getLoggedUser(request);

        String actionId = RequestUtil.getActionName(request);
        if (actionId == null) {
            throw new IllegalArgumentException("No parameter 'action' present in action request.");
        }

        List<String> permissionIds = permissionMapping.get(actionId);

        if (permissionIds != null) {
            permissionService.checkAndThrowPermission(loggedUser.getPk(), permissionIds.toArray(new String[]{}));
        } else {
            LOGGER.warn("No permissions set for action method: " + actionId);
            throw new PermissionException("User '" + loggedUser.getPk() + "' does not have the permission '" + actionId + "'");

        }

        return true;
    }

    @Override
    public boolean preHandleResource(ResourceRequest request, ResourceResponse response, Object handler)
            throws Exception {
        Resource loggedUser = RequestUtil.getLoggedUser(request);

        String resourceId = request.getResourceID();

        List<String> permissionIds = permissionMapping.get(resourceId);

        if (permissionIds != null) {
            permissionService.checkAndThrowPermission(loggedUser.getPk(), permissionIds.toArray(new String[]{}));
        } else {
            LOGGER.warn("No permissions set for resource method: " + resourceId);
            throw new PermissionException("User '" + loggedUser.getPk() + "' does not have the permission '" + resourceId + "'");

        }

        return true;
    }

    public void setPermissionMapping(Map<String, List<String>> permissionMapping) {
        this.permissionMapping = permissionMapping;
    }
}
