/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.validator.constraints.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.tarent.activity.validator.constraints.Webdav;

/**
 * Check that a given string is a well-formed URL and WEBDAV.
 * 
 */
public class WebdavValidator implements ConstraintValidator<Webdav, String> {

    private static final String URL_PATTERN = "^(http|https|ftp|webdav|webdavs)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*"
            + "((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\."
            + "(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|"
            + "([\\p{L}0-9\\-]+\\.)*[\\p{L}0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*"
            + "(/($|[\\p{L}0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$";

    private java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(URL_PATTERN, Pattern.CASE_INSENSITIVE);

    @Override
    public void initialize(Webdav constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if ((value == null) || (value.length() == 0)) {
            return true;
        }
        Matcher m = pattern.matcher(value);
        return m.matches();
    }

}
