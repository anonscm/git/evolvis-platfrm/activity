/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.validator.constraints.impl;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import de.tarent.activity.web.domain.OvertimeView;

@Component("overtimeViewValidator")
public class OvertimeViewValidator implements Validator {

    @Override
    public boolean supports(Class<?> klass) {
        return OvertimeView.class.isAssignableFrom(klass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        OvertimeView loss = (OvertimeView) target;

        if (loss.getType() != null && loss.getType().equals(1L) && loss.getProjectId() == null) {
            errors.rejectValue("projectId", "validate_project_not_empty");
        }

        if (loss.getDate() != null && loss.getDate().after(new Date())) {
            errors.rejectValue("date", "validate_date_future");
        }

    }
}
