/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.validator.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import de.tarent.activity.validator.constraints.impl.ActivityHoursValidator;

/**
 * Validate annotation that activity hours must match a given pattern, or the
 * default pattern.
 *
 * @author T. Kudla <t.kudla@tarent.de> tarent solutions GmbH
 *
 */
@Documented
@Constraint(validatedBy = ActivityHoursValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface ActivityHours {
	public static final String ACTIVITY_HOUR_DEFAULT_PATTERN = "^(:?1?[0-9]|2[0-4])(:?[.,](:?00?|25|50?|75))?$";

	/**
	 * @return Class<?>[]
	 */
	Class<?>[] groups() default {};

	/**
	 * @return String
	 */
	String message() default "{validate_activity_hours}";

	/**
	 * @return Class<? extends Payload>[]
	 */
	Class<? extends Payload>[] payload() default {};

	String regexp() default ActivityHours.ACTIVITY_HOUR_DEFAULT_PATTERN;
}
