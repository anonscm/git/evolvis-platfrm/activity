/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import de.tarent.activity.debug.XMLDebug;
import de.tarent.activity.domain.filter.JobFilter;

@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
public class ServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    ProjectService projectService;

    @Autowired
    ResourceService userService;

    @Autowired
    ActivityService activityService;

    @Autowired
    CostService costService;

    @Autowired
    SettingsService settingService;

    @Test
    public void testProjectService() {
        JobFilter filter = new JobFilter("f", 2l, null, null, null, null, null, null, null);
        XMLDebug.printXML(projectService.searchJob(filter));
    }

    @Test
    public void testUserService() {
        XMLDebug.printXML(userService.getResource(2L, false, false));
    }

    @Test
    public void testActivityService() {
        XMLDebug.printXML(activityService.lossTypeList());
    }

    @Test
    public void testCostService() {
        XMLDebug.printXML(costService.getCost(1L));
    }

    @Test
    public void testSettingService() {
        XMLDebug.printXML(settingService.getSettings());
    }
}
