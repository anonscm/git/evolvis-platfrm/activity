/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.math.BigDecimal;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.tarent.activity.domain.DeleteRequest;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.ResourceType;
import de.tarent.activity.domain.Skills;
import de.tarent.activity.domain.SkillsDef;
import de.tarent.activity.domain.filter.DeleteRequestFilter;
import de.tarent.activity.domain.filter.ResourceFilter;
import de.tarent.activity.domain.filter.SkillsFilter;
import de.tarent.activity.exception.UniqueValidationException;

@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
public class ResourceServiceTest extends TestCase {

    ResourceService userService;

    @Before
    public void setUp() throws Exception {
        InitialContext ctx = new InitialContext();
        userService = (ResourceService) ctx.lookup("activity-backend-ear/ResourceService/remote");
    }

    @Test
    public void testGetResourceWithRoles() {
        Resource resource = userService.getResource(4L, true, false);

        assertNotNull(resource);
    }

    @Test
    public void testGetUser() throws NamingException {
        // Resource resource = userService.getResource(1L, false, false);
        // List<String> roleNames = UserPermissions.getUserRoles(resource);
        // List<Integer> permissions = UserPermissions.getUserPermissions(resource);
        //
        // XMLDebug.printXML(roleNames);
        // XMLDebug.printXML(permissions);
    }

    @Test
    public void testManagerList() {
        userService.managerList(1L);
    }

    @Test
    public void testSearchResource() {
        userService.searchResource(new ResourceFilter('t', null, null, null, null, null));
    }

    @Test
    public void testposResStatusList() {
        userService.posResourceStatusList();
    }

    @Test
    public void testAddUser() {

        Resource resource = new Resource();
        resource.setCrDate(new Date());
        resource.setFirstname("new resource");
        resource.setResourceType(new ResourceType(1L, "int. Person"));
        resource.setRemainHoliday(new BigDecimal(1));
        resource.setHoliday(new BigDecimal(1));
        resource.setPassword("password");

        try {
            userService.addResource(resource);
        } catch (UniqueValidationException e) {
            e.printStackTrace();
        }
        assertEquals("5f4dcc3b5aa765d61d8327deb882cf99", resource.getPassword());
    }

    @Test
    public void testUpdateUser() {

        Resource resource = new Resource();
        resource.setCrDate(new Date());
        resource.setFirstname("new resource");
        resource.setResourceType(new ResourceType(1L, "int. Person"));
        resource.setRemainHoliday(new BigDecimal(1));
        resource.setHoliday(new BigDecimal(1));
        resource.setPassword("password");

        try {
            userService.updateResource(resource);
        } catch (UniqueValidationException e) {
            e.printStackTrace();
        }
        assertEquals("5f4dcc3b5aa765d61d8327deb882cf99", resource.getPassword());
    }

    @Test
    public void testSearchSkill() {
        userService.searchSkills(new SkillsFilter(null, null, null, null, null, null));
    }

    @Test
    public void testSkillDefList() {
        userService.skillsDefList();
    }

    @Test
    public void testAddSkillDef() {
        SkillsDef def = new SkillsDef();
        def.setName("skill def");
        userService.addSkillDef(def);
    }

    @Test
    public void testAddSkill() {
        Skills skills = new Skills();
        skills.setResource(new Resource(1L));
        skills.setSkillsDef(new SkillsDef(1L));
        userService.addSkill(skills);
    }

    @Test
    public void testUpdateSkill() {
        Skills skills = new Skills();
        skills.setResource(new Resource(1L));
        skills.setSkillsDef(new SkillsDef(1L));
        userService.updateSkill(skills);
    }

    @Test
    public void testDeleteSkill() {
        Skills skills = new Skills();
        skills.setResource(new Resource(1L));
        skills.setSkillsDef(new SkillsDef(1L));
        userService.deleteSkill(userService.addSkill(skills));
    }

    @Test
    public void testSearchRequest() {
        userService.searchRequest(new DeleteRequestFilter(null, null, null, null, null, null, null));
    }

    @Test
    public void testUpdateRequest() {
        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setResource(new Resource(1L));
        deleteRequest.setController(new Resource(2L));
        deleteRequest.setDescription("delete request");
        deleteRequest.setId(1L);
        deleteRequest.setType("Loss");
        deleteRequest.setStatus("active");
        userService.updateRequest(deleteRequest);
    }

    @Test
    public void testGetRequest() {
        userService.getRequest(1L);
    }

    @Test
    public void testDeleteRequest() {
        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setResource(new Resource(1L));
        deleteRequest.setController(new Resource(2L));
        deleteRequest.setDescription("delete request");
        deleteRequest.setId(1L);
        deleteRequest.setType("Loss");
        deleteRequest.setStatus("active");
        userService.deleteRequest(userService.addRequest(deleteRequest));
    }

    @Test
    public void testAddRequest() {
        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setResource(new Resource(1L));
        deleteRequest.setController(new Resource(2L));
        deleteRequest.setDescription("delete request");
        deleteRequest.setId(1L);
        deleteRequest.setType("Loss");
        deleteRequest.setStatus("active");
        userService.addRequest(deleteRequest);
    }

}
