/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.security.auth.login.LoginContext;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import de.tarent.activity.auth.AuthCallbackHandler;
import de.tarent.activity.debug.XMLDebug;
import de.tarent.activity.domain.filter.JobFilter;

/**
 * Test using standard JAAS.
 * 
 * @author miron
 * 
 */
@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
public class ServiceJaasTest extends AbstractJUnit4SpringContextTests {

    @Test
    public void testJaas() throws Exception {

        Properties env = new Properties();
        env.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
        env.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
        env.setProperty(Context.PROVIDER_URL, "jnp://localhost:1099/");

        InitialContext ctx = new InitialContext(env);
        // ActivityService activityService = (ActivityService) ctx.lookup("activity/ActivityService/remote");
        ProjectService projectService = (ProjectService) ctx.lookup("activity/ProjectService/remote");

        // LoginContext lctx = new LoginContext("Activity", new AuthCallbackHandler("iurie", "iurie"));
        LoginContext lctx = new LoginContext("ActivityDomain", new AuthCallbackHandler("admin", "admin"));
        lctx.login();

        JobFilter filter = new JobFilter("f", 2l, null, null, null, null, null, null, null);
        XMLDebug.printXML(projectService.searchJob(filter));
        // XMLDebug.printXML(activityService.getJobsByResource(2l));

        lctx.logout();

        lctx = new LoginContext("ActivityDomain", new AuthCallbackHandler("miron", "miron"));
        lctx.login();
        XMLDebug.printXML(projectService.searchJob(filter));
        // XMLDebug.printXML(activityService.getJobsByResource(2l));

        lctx.logout();
    }
}
