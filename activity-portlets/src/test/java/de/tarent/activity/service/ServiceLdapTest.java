/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import junit.framework.TestCase;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.tarent.activity.web.controller.login.LoginController;

@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
public class ServiceLdapTest extends TestCase {

    private String LDAP_URL;
    private String LDAP_BASE_DN;
    private boolean LDAP_ENABLED;
    private String USER_FILTER;
    private String ROLE_FILTER;
    private String LINK_FILTER;
    private int LDAP_PORT;
    private boolean SSL;
    private String USER_GROUP_CN;
    private String GROUP_DN;

    private DirContext anonCtx = null;

    @Before
    public void setUp() {
        if (LDAP_URL == null) {
            Properties props = new Properties();
            InputStream inStream = LoginController.class.getResourceAsStream("/ldap.properties");
            try {
                props.load(inStream);
                LDAP_URL = props.getProperty("activity.ldap.url");
                LDAP_BASE_DN = props.getProperty("activity.ldap.baseDn");
                LDAP_ENABLED = Boolean.parseBoolean(props.getProperty("activity.ldap.enable"));

                USER_FILTER = props.getProperty("activity.ldap.userFilter");
                ROLE_FILTER = props.getProperty("activity.ldap.roleFilter");
                LINK_FILTER = props.getProperty("activity.ldap.linkFilter");

                SSL = Boolean.parseBoolean(props.getProperty("activity.ldap.ssl"));
                LDAP_PORT = Integer.valueOf(props.getProperty("activity.ldap.port"));

                USER_GROUP_CN = props.getProperty("activity.ldap.userGroupCn");
                GROUP_DN = props.getProperty("activity.ldap.groupDn");

                assertNotNull("Property LDAP_URL is NULL", LDAP_URL);
                assertNotNull("Property LDAP_BASE_DN is NULL", LDAP_BASE_DN);
                assertNotNull("Property LDAP_ENABLED is NULL", LDAP_ENABLED);
                assertNotNull("Property USER_FILER is NULL", USER_FILTER);
                assertNotNull("Property ROLE_FILTER is NULL", ROLE_FILTER);
                assertNotNull("Property USER_ROLE_FILTER is NULL, USER_ROLE_FILTER");
                assertNotNull("Property SSL is Null", SSL);
                assertNotNull("Property LDAP_PORT is NULL", LDAP_PORT);
                assertNotNull("Property USER_GROUP_CN is Null, USER_GROUP_CN");

                String LDAP_URL_PREFIX = "ldap";

                if (SSL)
                    LDAP_URL_PREFIX += "s";

                LDAP_URL_PREFIX += "://";
                LDAP_URL = LDAP_URL_PREFIX + LDAP_URL + ":" + LDAP_PORT;

                Hashtable<String, Object> anonimousEnv = new Hashtable<String, Object>(11);
                anonimousEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                anonimousEnv.put(Context.PROVIDER_URL, LDAP_URL);
                anonimousEnv.put(Context.SECURITY_AUTHENTICATION, "none"); // Anonymous bind

                try {
                    anonCtx = new InitialDirContext(anonimousEnv);

                } catch (NamingException e) {
                    Assume.assumeNoException(e);
                } catch (NullPointerException e) {
                    Assume.assumeNoException(e);
                } catch (Exception e) {
                    Assume.assumeNoException(e);
                }

            } catch (Exception e) {
                Assume.assumeNoException(e);
            }
        }
    }

    @Test
    public void testGetAllUsers() {

        SearchControls searchControls = new SearchControls();
        searchControls.setReturningAttributes(new String[] { USER_FILTER });
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        String searchFilter = "(" + USER_FILTER + "=*)";
        NamingEnumeration<SearchResult> results = null;

        try {
            results = anonCtx.search(LDAP_BASE_DN, searchFilter, searchControls);

            if (results != null) {

                while (results.hasMore()) {

                    SearchResult searchResult = (SearchResult) results.next();
                    Attributes attributes = searchResult.getAttributes();
                    Attribute attribute = (Attribute) attributes.get(USER_FILTER);
                    String userUid = attribute.get(0).toString();
                    assertNotNull(userUid);
                }
            }

        } catch (NamingException e) {
            Assume.assumeNoException(e);
        }
    }

    @Test
    public void testGetNestedEmployeeGroups() {

        SearchControls searchControls = new SearchControls();
        searchControls.setReturningAttributes(new String[] { LINK_FILTER });
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        String searchFilter = "(&(objectClass=posixGroup)(" + ROLE_FILTER + "=" + USER_GROUP_CN + "))";
        NamingEnumeration<SearchResult> results = null;

        try {
            results = anonCtx.search(GROUP_DN, searchFilter, searchControls);

            if (results != null) {

                while (results.hasMore()) {

                    SearchResult searchResult = (SearchResult) results.next();
                    Attributes attributes = searchResult.getAttributes();
                    Attribute attribute = (Attribute) attributes.get(LINK_FILTER);

                    for (int i = 0; i < attribute.size(); i++) {
                        String roleCn = attribute.get(i).toString();
                        if (roleCn.startsWith(ROLE_FILTER)) {
                            assertNotNull(roleCn);
                        }
                    }
                }
            }

        } catch (NamingException e) {
            Assume.assumeNoException(e);
        }
    }

    @Test
    public void testGetGroupsOfUser() {

        SearchControls searchControls = new SearchControls();
        searchControls.setReturningAttributes(new String[] { ROLE_FILTER });
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        String searchFilter = "(&(objectClass=posixGroup)(" + LINK_FILTER + "=" + USER_FILTER + "=papel,"
                + LDAP_BASE_DN + "))";
        NamingEnumeration<SearchResult> results = null;

        try {
            results = anonCtx.search(GROUP_DN, searchFilter, searchControls);

            if (results != null) {

                while (results.hasMore()) {

                    SearchResult searchResult = (SearchResult) results.next();
                    Attributes attributes = searchResult.getAttributes();
                    Attribute attribute = (Attribute) attributes.get(ROLE_FILTER);
                    String roleCn = attribute.get(0).toString();
                    assertNotNull(roleCn);
                }
            }

        } catch (NamingException e) {
            Assume.assumeNoException(e);
        }
    }
}
