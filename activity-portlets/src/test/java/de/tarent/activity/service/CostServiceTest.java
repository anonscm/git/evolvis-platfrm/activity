/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import java.math.BigDecimal;
import java.util.Date;

import javax.naming.InitialContext;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.tarent.activity.debug.XMLDebug;
import de.tarent.activity.domain.Cost;
import de.tarent.activity.domain.CostType;
import de.tarent.activity.domain.Invoice;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Resource;
import de.tarent.activity.domain.filter.CostFilter;
import de.tarent.activity.domain.filter.InvoiceFilter;

@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
public class CostServiceTest extends TestCase {
    CostService costService;

    @Before
    public void setUp() throws Exception {
        InitialContext ctx = new InitialContext();
        costService = (CostService) ctx
                .lookup("ejb:activity-backend-ear/activity-ejb-0.0.5-SNAPSHOT//CostService!de.tarent.activity.service.CostService");
    }

    @Test
    public void testGetCost() throws Exception {
        // Cost cost = costService.getCost(1L);
        System.out.println(costService.getInvoice(1L));
    }

    @Test
    public void testSearchCost() {
        costService.searchCost(new CostFilter(null, null, null, null, null, null));
    }

    @Test
    public void testCostTypeList() {
        XMLDebug.printXML(costService.getCostTypeList());
    }

    @Test
    public void testUpdateCost() {
        Cost cost = new Cost();
        cost.setJob(new Job(1L));
        cost.setCostType(new CostType(1L));
        cost.setResource(new Resource(1L));
        cost.setCost(new BigDecimal(10));
        cost.setCrDate(new Date());
        cost.setName("New Cost");

        costService.updateCost(cost);
    }

    @Test
    public void testAddCost() {
        Cost cost = new Cost();
        cost.setJob(new Job(1L));
        cost.setCostType(new CostType(1L));
        cost.setResource(new Resource(1L));
        cost.setCost(new BigDecimal(10));
        cost.setCrDate(new Date());
        cost.setName("New Cost");

        XMLDebug.printXML(costService.addCost(cost));
    }

    @Test
    public void testSerachInvoice() {
        XMLDebug.printXML(costService.searchInvoice(new InvoiceFilter(null, null, null, null, null, null, null, null)));
    }

    @Test
    public void testGetInvoice() {
        XMLDebug.printXML(costService.getInvoice(1L));
    }

    @Test
    public void testUpdateInvoice() {
        Invoice invoice = new Invoice();
        invoice.setJob(new Job(1L));
        invoice.setAmount(new BigDecimal(10));
        invoice.setCrDate(new Date());
        invoice.setName("New invoice");
        costService.updateInvoice(invoice);
    }

    @Test
    public void testAddInvoice() {
        Invoice invoice = new Invoice();
        invoice.setJob(new Job(1L));
        invoice.setAmount(new BigDecimal(10));
        invoice.setCrDate(new Date());
        invoice.setName("New invoice");
        XMLDebug.printXML(costService.addInvoice(invoice));

    }

}
