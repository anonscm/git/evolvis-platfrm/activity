/**
 * tarent-activity ERP is
 * Copyright © 2005-2014 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version
 * 3 as published by the Free Software Foundation.
 *
 * As an additional permit under Section 7 of the AGPLv3, tarent-activity
 * and any derivate thereof may be linked against Dependencies, as defined
 * below, without requiring Dependencies to be licenced under the AGPLv3,
 * as long as the respective licences of Dependencies permit. You must
 * comply with the AGPLv3 in all respects for all code other than such
 * Dependencies, which are hereby defined as the set of build, test and
 * runtime dependencies of tarent-activity as published by tarent itself
 * that do not come under an otherwise AGPLv3 compatible licence.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.tarent.activity.service;

import javax.naming.InitialContext;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.tarent.activity.domain.Customer;
import de.tarent.activity.domain.Job;
import de.tarent.activity.domain.Loss;
import de.tarent.activity.domain.Project;

@Ignore("Ignore tests until they are ready for JBoss 7.0.1")
public class MailServiceTest extends TestCase {
    private MailService mailService;
    private ProjectService projectService;
    private ActivityService activityService;

    @Override
    @Before
    public void setUp() throws Exception {
        InitialContext ctx = new InitialContext();
        mailService = (MailService) ctx.lookup("activity-backend-ear/MailService/remote");
        projectService = (ProjectService) ctx.lookup("activity-backend-ear/ProjectService/remote");
        activityService = (ActivityService) ctx.lookup("activity-backend-ear/ActivityService/remote");
    }

    @Test
    public void testSendCustomerMail() throws Exception {
        Customer customer = projectService.getCustomer(1l);
        mailService.sendCustomerMail(customer);
    }

    @Test
    public void testSendHolidayApprovedMail() throws Exception {
        Loss loss = activityService.getLoss(1l);
        mailService.sendHolidayApprovedMail(loss);
    }

    @Test
    public void testSendHolidayMail() throws Exception {
        Loss loss = activityService.getLoss(1l);
        mailService.sendHolidayMail(loss);
    }

    @Test
    public void testSendHolidayRejectedMail() throws Exception {
        Loss loss = activityService.getLoss(1l);
        mailService.sendHolidayRejectedMail(loss);
    }

    @Test
    public void testSendJobMail() throws Exception {
        Job job = projectService.getJob(1l);
        mailService.sendJobMail(job);
    }

    @Test
    public void testSendProjectMail() throws Exception {
        Project project = projectService.getProject(1l);
        mailService.sendProjectMail(project);
    }
}
